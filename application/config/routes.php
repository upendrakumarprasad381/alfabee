<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'Frontend';
$route['404_override'] = 'Login/ERROR_404';
$route['translate_uri_dashes'] = FALSE;
// $route['register'] = 'Frontend/vendorRegister';
$route['vendor_dashboard'] = 'vendor/index';
$route['vendorlogout'] = 'vendor/vendorLoginout';

$route['add_your_food'] = 'Register/addYourFoodVendor';

$route['login_signup'] = 'Frontend/signUp';
$route['login_signup/(:any)'] = "Frontend/signUp";
$route['set_lang'] = 'Frontend/set_lang';
$route['most_selling'] = 'Frontend/mostSelling';
$route['homely_foods'] = 'Frontend/homelyFoods';
$route['search_result'] = 'Frontend/searchResult';
$route['search_details/(:any)'] = 'Frontend/search_details/$1';
$route['my_profile'] = 'Frontend/myProfile';
$route['change_password'] = 'Frontend/changePassword';
$route['forgot_password'] = 'Frontend/forgotPassword';
$route['resetpassword/(:any)'] = 'Frontend/resetpassword/$1';
$route['details/(:any)'] = 'Frontend/homelyFoodsDetails/$1';
$route['checkout'] = 'Frontend/checkout';
$route['payment'] = 'Frontend/payment';
$route['thankyou'] = 'Frontend/thankyou';
$route['thank_you'] = 'Frontend/thankyou_partyorder';
$route['paymentresponse'] = 'Frontend/paymentresponse';
$route['my_orders'] = 'Frontend/myOrders';
$route['my_party_orders'] = 'Frontend/myPartyOrders';
$route['order_history/(:any)'] = 'Frontend/orderHistory/$1';
$route['party_order'] = 'Frontend/PartyOrder';
$route['payment_invoice/(:any)'] = 'Frontend/PaymentInvocie/$1';
$route['party_orders'] = 'Frontend/partyOrdersList';
$route['offers'] = 'Frontend/offers';
$route['contact_us'] = 'Frontend/contact_us';
$route['suggest_homebusiness'] = 'Frontend/suggestHomeBusiness';
$route['saved_address'] = 'Frontend/savedAddress';
$route['store_category/(:any)'] = 'Frontend/restaurant/$1';
$route['about_us'] = 'Frontend/about_us';
$route['terms_condition'] = 'Frontend/terms_condition';
$route['privacy_policy'] = 'Frontend/privacy_policy';
$route['partner_registration'] = 'Frontend/partner_registration';
$route['rider_registration'] = 'Frontend/rider_registration';
$route['terms-and-conditions'] = 'Frontend/termsAndConditions';
$route['privacy-policy'] = 'Frontend/privacypolicy';
$route['faq'] = 'Frontend/faq';

$route['loyalty-program'] = 'Frontend/loyaltyProgram';
$route['guidelines-for-vendors'] = 'Frontend/guidelinesforVendors';

$route['rider-live-track'] = 'Frontend/rider_live_track';
$route['live-tracking'] = 'Frontend/livetracking';

$route['logout'] = 'Frontend/logout';
