<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    private $segment2;
    private $segment3;
    private $segment4;

    public function __construct() {

        parent::__construct();
        date_default_timezone_set(TIME_ZONE_GET);
        error_reporting(0);
        IsLogedAdmin();
        $this->load->Model('Database');
        $this->segment2 = $this->uri->segment(2);
        $this->segment3 = $this->uri->segment(3);
        $this->segment4 = $this->uri->segment(4);
        if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'en' || empty($this->session->userdata(ADMIN_LANGUAGE_ID))) {
            $this->load->language("common", "english");
        } else {
            $this->load->language("common", "arabic");
        }
    }

    public function Helper() {
        $status = $_REQUEST['function'];
        switch ($status) {
            default :
                $_REQUEST['function']($_REQUEST);
        }
    }

    public function index() {

        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'index');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function home_slider() {
        $segment3 = $this->segment3;
        $HomeBannerId = base64_decode($this->segment4);
        $data = array();


        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);
                $json['type'] = 1;
                $json['lastupdate'] = date('Y-m-d H:i:s');
                $HomeBannerId = $json['HomeBannerId'];
                unset($json['HomeBannerId']);
                if ($HomeBannerId == '') {
                    $Result = $this->Database->insert('featured_deals_image', $json);
                } else if ($HomeBannerId > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $HomeBannerId);
                    $this->Database->update('featured_deals_image', $json, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['HomeBannerId'] = '';
            if (is_numeric($HomeBannerId)) {
                $CondutaionArray = array('id' => $HomeBannerId);
                $Qry = "SELECT * FROM `featured_deals_image` WHERE id=:id and type=1";
                $CategoryArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
                if (count($CategoryArray) > 0) {
                    $data['HomeBannerId'] = $HomeBannerId;
                    $data['CategoryDetails'] = $CategoryArray[0];
                }
            }
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'AddHomeSlider', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `featured_deals_image` WHERE archive=:archive and type=1";
            $CategoryArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $data['Categories'] = $CategoryArray;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'home_slider', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function sub_slider() {
        $segment3 = $this->segment3;
        $HomeBannerId = base64_decode($this->segment4);
        $data = array();


        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);
                $json['type'] = 2;
                $json['lastupdate'] = date('Y-m-d H:i:s');
                $HomeBannerId = $json['HomeBannerId'];
                unset($json['HomeBannerId']);
                if ($HomeBannerId == '') {
                    $Result = $this->Database->insert('featured_deals_image', $json);
                } else if ($HomeBannerId > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $HomeBannerId);
                    $this->Database->update('featured_deals_image', $json, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['HomeBannerId'] = '';
            if (is_numeric($HomeBannerId)) {
                $CondutaionArray = array('id' => $HomeBannerId);
                $Qry = "SELECT * FROM `featured_deals_image` WHERE id=:id and type=2";
                $CategoryArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
                if (count($CategoryArray) > 0) {
                    $data['HomeBannerId'] = $HomeBannerId;
                    $data['CategoryDetails'] = $CategoryArray[0];
                }
            }
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'AddSubSlider', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `featured_deals_image` WHERE archive=:archive and type=2";
            $CategoryArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $data['Categories'] = $CategoryArray;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'sub_slider', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function side_banner() {

        $segment3 = $this->segment3;
        $HomeBannerId = base64_decode($this->segment4);
        $data = array();


        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);
                $json['type'] = 3;
                $json['lastupdate'] = date('Y-m-d H:i:s');
                $HomeBannerId = $json['HomeBannerId'];
                unset($json['HomeBannerId']);
                if ($HomeBannerId == '') {
                    $Result = $this->Database->insert('featured_deals_image', $json);
                } else if ($HomeBannerId > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $HomeBannerId);
                    $this->Database->update('featured_deals_image', $json, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['HomeBannerId'] = '';
            if (is_numeric($HomeBannerId)) {
                $CondutaionArray = array('id' => $HomeBannerId);
                $Qry = "SELECT * FROM `featured_deals_image` WHERE id=:id and type=3";
                $CategoryArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
                if (count($CategoryArray) > 0) {
                    $data['HomeBannerId'] = $HomeBannerId;
                    $data['CategoryDetails'] = $CategoryArray[0];
                }
            }
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'AddSideBanner', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `featured_deals_image` WHERE archive=:archive and type=3";
            $CategoryArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $data['Categories'] = $CategoryArray;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'side_banner', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function sponsored_ad() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();


        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);


                $Id = $json['Id'];
                unset($json['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('sponsored_ad', $json);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);

                    $this->Database->update('sponsored_ad', $json, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['HomeBannerId'] = '';
            if (is_numeric($Id)) {
                $CondutaionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `sponsored_ad` WHERE id=:id";
                $CategoryArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
                if (count($CategoryArray) > 0) {
                    $data['HomeBannerId'] = $Id;
                    $data['CategoryDetails'] = $CategoryArray[0];
                }
            }
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'addSponsored', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `sponsored_ad` WHERE archive=:archive";
            $CategoryArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $data['Categories'] = $CategoryArray;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'sponsored', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function ads() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();


        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);


                $Id = $json['Id'];
                unset($json['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('ads', $json);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    $this->Database->update('ads', $json, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['HomeBannerId'] = '';
            if (is_numeric($Id)) {
                $CondutaionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `ads` WHERE id=:id";
                $CategoryArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
                if (count($CategoryArray) > 0) {
                    $data['HomeBannerId'] = $Id;
                    $data['CategoryDetails'] = $CategoryArray[0];
                }
            }
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'addAds', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `ads` WHERE archive=:archive";
            $CategoryArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $data['Categories'] = $CategoryArray;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'ads', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function home_page() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();


        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);

                $json['lastupdate'] = date('Y-m-d H:i:s');
                $Id = $json['Id'];
                unset($json['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('home_banner', $json);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    $this->Database->update('home_banner', $json, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $CondutaionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `home_banner` WHERE id=:id";
                $CategoryArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
                if (count($CategoryArray) > 0) {
                    $data['Id'] = $Id;
                    $data['CategoryDetails'] = $CategoryArray[0];
                }
            }
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'addhomebanner', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `home_banner` WHERE archive=:archive";
            $CategoryArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $data['Categories'] = $CategoryArray;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'home_page', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function category() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);

                $json['lastupdate'] = date('Y-m-d H:i:s');
                $Id = $json['Id'];
                unset($json['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('category_details', $json);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    $this->Database->update('category_details', $json, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $ConditionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `category_details` WHERE id=:id ORDER BY id DESC";
                $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
                if (count($Array) > 0) {
                    $data['Id'] = $Id;
                    $data['Categories'] = $Array[0];
                }
            }

            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'addCategory', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $ConditionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `category_details` WHERE archive=:archive order by id DESC";
            $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
            $data['Categories'] = $Array;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'Category', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function store_type() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {

                $json = json_decode($_REQUEST['json'], true);
                $json['description'] = $_REQUEST['description'];
                $json['lastupdate'] = date('Y-m-d H:i:s');
                $Id = $json['Id'];
                unset($json['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('store_type', $json);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    $this->Database->update('store_type', $json, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $ConditionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `store_type` WHERE id=:id ORDER BY id DESC";
                $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
                if (count($Array) > 0) {
                    $data['Id'] = $Id;
                    $data['Categories'] = $Array[0];
                }
            }


            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'addStoreType', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $ConditionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `store_type` WHERE archive=:archive";
            $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
            $data['Categories'] = $Array;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'store_type', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function occasion() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);
                $json['lastupdate'] = date('Y-m-d H:i:s');
                $Id = $json['Id'];
                unset($json['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('occasion', $json);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    $this->Database->update('occasion', $json, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $ConditionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `occasion` WHERE id=:id ORDER BY id DESC";
                $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
                if (count($Array) > 0) {
                    $data['Id'] = $Id;
                    $data['Occasion'] = $Array[0];
                }
            }

            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'addOccasion', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $ConditionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `occasion` WHERE archive=:archive";
            $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
            $data['Occasion'] = $Array;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'Occasion', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function cuisine() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);
                $json['lastupdate'] = date('Y-m-d H:i:s');
                $Id = $json['Id'];
                unset($json['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('cuisine', $json);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('cuisine_id' => $Id);
                    $this->Database->update('cuisine', $json, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $ConditionArray = array('cuisine_id' => $Id);
                $Qry = "SELECT * FROM `cuisine` WHERE cuisine_id=:cuisine_id ORDER BY cuisine_id DESC";
                $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
                if (count($Array) > 0) {
                    $data['Id'] = $Id;
                    $data['Cuisine'] = $Array[0];
                }
            }

            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'addCuisine', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $ConditionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `cuisine` WHERE archive=:archive";
            $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
            $data['Cuisine'] = $Array;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'Cuisine', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function vendor() {
        $ConditionArray = array('is_approved' => 0);
        $qry = "SELECT users.*,restaurant_details.restaurant_name FROM `users` join restaurant_details ON restaurant_details.vendor_id=users.id WHERE is_approved =:is_approved AND archive=0 order by users.id desc";
        $array = $this->Database->select_qry_array($qry, $ConditionArray);
        $data['Vendor'] = $array;
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'new_vendor', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function approved_vendor() {
        $qry = "SELECT users.*,restaurant_details.restaurant_name FROM `users` join restaurant_details ON restaurant_details.vendor_id=users.id WHERE is_approved != 0 AND user_type=2 order by users.id desc";
        $array = $this->Database->select_qry_array($qry);
        $data['Vendor'] = $array;
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'approved_vendor', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function customers() {
        $ConditionArray = array('user_type' => 3);
        $qry = "SELECT * FROM `users` WHERE user_type =:user_type order by id desc";
        $array = $this->Database->select_qry_array($qry, $ConditionArray);
        $data['customer'] = $array;
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'customer_list', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function contact_us() {
        $qry = "SELECT * FROM `contact_us` order by id desc";
        $array = $this->Database->select_qry_array($qry);
        $data['customer'] = $array;
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'contact_us', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function suggest_homebusiness() {
        $qry = "SELECT * FROM `suggest_restaurant` order by id desc";
        $array = $this->Database->select_qry_array($qry);
        $data['customer'] = $array;
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'suggest_homebusiness', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function Orders() {
        $data['Filter'] = '';
        if (isset($_POST['Filter'])) {
            $data['Filter'] = $_POST;
        }
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'orders', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function OrderDetails($orderid = '') {
        $data['orderid'] = base64_decode($orderid);
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'order_details', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function party_orders() {
        $data['Filter'] = '';
        if (isset($_POST['Filter'])) {
            $data['Filter'] = $_POST;
        }

        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'party_orders', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function party_orderDetails($orderid = '') {
        $data['orderid'] = base64_decode($orderid);
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'party_order_details', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function promocode() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $Session = GetSessionArrayAdmin();
        if ($segment3 == 'add-new') {
            if (!empty($_REQUEST)) {
                $array = $_REQUEST;

                $json['promo_code'] = !empty($array['promo_code']) ? $array['promo_code'] : '';
                $json['title'] = !empty($array['offer_title']) ? $array['offer_title'] : '';
                $json['title_ar'] = !empty($array['offer_title_ar']) ? $array['offer_title_ar'] : '';
                $json['purchase_amount'] = !empty($array['purchase_amount']) ? $array['purchase_amount'] : 0;
                $json['discount'] = !empty($array['discount']) ? $array['discount'] : '';
                $json['date_to'] = !empty($array['date_to']) ? date('Y-m-d', strtotime($array['date_to'])) : '0000-00-00';
                $json['date_from'] = !empty($array['date_from']) ? date('Y-m-d', strtotime($array['date_from'])) : '0000-00-00';
                $json['user_type'] = 1;
                $json['offer_type'] = 2;
                $json['store_type'] = !empty($array['store_type']) ? $array['store_type'] : '';
                $json['discount_type'] = !empty($array['discount_type']) ? $array['discount_type'] : '';
                $json['discount_from'] = !empty($array['discount_from']) ? $array['discount_from'] : '';
                $json['offer_addedby'] = $Session->id;
                $json['restaurant_id'] = $array['rest_id'];
                $json['maximum_discount'] = !empty($array['maximum_discount']) ? $array['maximum_discount'] : '';
                if (empty($array['Id'])) {
                    $this->Database->insert('promo_code', $json);
                } else {
                    $this->Database->update('promo_code', $json, array('promocode_id' => $array['Id']));
                }
                die(json_encode(array('status' => true, 'message' => 'success')));
            }
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'addpromocode');
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'promocode');
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function Reviews() {
        $Qry = "SELECT user_feedback.id as feedback_id,user_feedback.status as review_status,user_feedback.*,users.*, restaurant_details.* FROM `user_feedback` LEFT JOIN users ON users.id = user_feedback.user_id
                LEFT JOIN restaurant_details ON restaurant_details.vendor_id=user_feedback.vendor_id WHERE  user_feedback.archive=0 ORDER BY user_feedback.id desc
                ";
        $Array = $this->Database->select_qry_array($Qry);

        $data['Reviews'] = $Array;
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'reviews', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function user_info() {
        $UserId = base64_decode($this->segment3);
        $qry = "SELECT UD.* FROM `users` UD WHERE UD.id=:id";
        $CondutaionArray = array('id' => $UserId);
        $UserArray = $this->Database->select_qry_array($qry, $CondutaionArray);
        if (count($UserArray) > 0) {
            $data['UserId'] = $UserId;
            $qry = "SELECT OD.*,OS.* FROM `orders` OD LEFT JOIN order_status OS ON OS.status_id=OD.order_status WHERE OD.user_id=:user_id ORDER BY OD.id DESC";
            $CondutaionArray = array('user_id' => $UserId);
            $order = $this->Database->select_qry_array($qry, $CondutaionArray);
            $data['order'] = $order;
            $data['UserInfo'] = $UserArray[0];

            $Qry = "SELECT * FROM `user_order_address` WHERE `user_id` =:user_id";
            $AddressArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $data['AddressArray'] = $AddressArray;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'user_info', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function vendor_info() {
        $UserId = base64_decode($this->segment3);
        $qry = "SELECT UD.* FROM `users` UD WHERE UD.id=:id";
        $CondutaionArray = array('id' => $UserId);
        $UserArray = $this->Database->select_qry_array($qry, $CondutaionArray);
        if (count($UserArray) > 0) {
            $data['UserId'] = $UserId;
            $CondutaionArray = array('vendor_id' => $UserId);
            $qry = "SELECT RD.* FROM `restaurant_details` RD WHERE RD.vendor_id=:vendor_id ORDER BY RD.id DESC";
            $rest = $this->Database->select_qry_array($qry, $CondutaionArray);

            $data['restauarnt'] = $rest;
            $data['UserInfo'] = $UserArray[0];


            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'vendor_info', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    // public function commission() {
    //     $qry = "SELECT * FROM `commission`";
    //     $commission = $this->Database->select_qry_array($qry);
    //     $data['commission'] = $commission;
    //     $this->load->view(ADMIN_DIR . 'includes/header');
    //     $this->load->view(ADMIN_DIR . 'commission',$data);
    //     $this->load->view(ADMIN_DIR . 'includes/footer');
    // }
    public function commission() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);

                $Id = $json['Id'];
                unset($json['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('commission', $json);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    $this->Database->update('commission', $json, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $ConditionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `commission` WHERE id=:id ORDER BY id DESC";
                $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
                if (count($Array) > 0) {
                    $data['Id'] = $Id;
                    $data['commission'] = $Array[0];
                }
            }

            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'manage_commission', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $qry = "SELECT * FROM `commission`";
            $commission = $this->Database->select_qry_array($qry);
            $data['commission'] = $commission;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'commission', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function transactions() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'transactions');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    // public function cms_page() {
    //     $this->load->view(ADMIN_DIR . 'includes/header');
    //     $this->load->view(ADMIN_DIR . 'cms_page');
    //     $this->load->view(ADMIN_DIR . 'includes/footer');
    // }   
    public function about_us() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'about_us');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function terms_condition() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'terms_condition');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function privacy() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'privacy');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function filter() {
        $segment3 = $this->segment3;
        $FilterId = base64_decode($this->segment4);
        $data = array();


        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);
                $json['lastupdate'] = date('Y-m-d H:i:s');
                $FilterId = $json['FilterId'];
                unset($json['FilterId']);
                if ($FilterId == '') {
                    $Result = $this->Database->insert('mobile_filter', $json);
                } else if ($FilterId > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $FilterId);
                    $this->Database->update('mobile_filter', $json, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['FilterId'] = '';
            if (is_numeric($FilterId)) {
                $CondutaionArray = array('id' => $FilterId);
                $Qry = "SELECT * FROM `mobile_filter` WHERE id=:id";
                $CategoryArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
                if (count($CategoryArray) > 0) {
                    $data['FilterId'] = $FilterId;
                    $data['CategoryDetails'] = $CategoryArray[0];
                }
            }
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'addfilter', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `mobile_filter` WHERE archive=:archive";
            $CategoryArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $data['Categories'] = $CategoryArray;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'filter', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function user_management() {
        //  $CondutaionArray = array('id' => 1);
        $ADMIN_PERMISSION_ARRAY = ADMIN_PERMISSION_ARRAY;
        $Session = $this->session->userdata('Admin');
        if (in_array($Session->id, $ADMIN_PERMISSION_ARRAY)) {
            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `admin_login` WHERE archive=:archive AND id NOT IN (1)";
        } else {
            $CondutaionArray_dept = array('archive' => 0, 'id' => $Session->id);
            $Qry_dept_id = "SELECT * FROM `admin_login` WHERE archive=:archive and id=:id";
            $manager_data = $this->Database->select_qry_array($Qry_dept_id, $CondutaionArray_dept);
            $manager_dept = $manager_data[0]->department;
            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `admin_login` WHERE archive=:archive";
        }
        $UserArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
        // print_r($Qry);die();
        $data['UserArray'] = $UserArray;
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'user_management', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function AddNewUser() {
        if (isset($_REQUEST['json'])) {
            //print_r('hiii');
            $Json = json_decode($_REQUEST['json'], true);
            $UserId = $Json['UserId'];
            $email = $Json['email'];

            $CondutaionArray_Email = array('email' => $email);
            if ($UserId) {
                $Qry_email = "SELECT * FROM `admin_login` WHERE email=:email AND id!=$UserId";
            } else {
                $Qry_email = "SELECT * FROM `admin_login` WHERE email=:email ";
            }
            $email_exist = $this->Database->select_qry_array($Qry_email, $CondutaionArray_Email);
            if ($email_exist) {
                echo json_encode(array('status' => 'error', 'message' => 'This Email Or Username Already Exist'));
                exit;
            }
            //email validation

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {

                echo json_encode(array('status' => 'error', 'message' => 'Invalid email format'));
                exit;
            }

            unset($Json['UserId']);
            $Json['lastupdate'] = date('Y-m-d H:i:s');
            if (isset($Json['password'])) {
                $Json['password'] = md5($Json['password']);
            }
            if ($UserId > 0) {
                $Result = 1;
                $CondArray = array('id' => $UserId);
                $this->Database->update('admin_login', $Json, $CondArray);
            } else {
                $Result = $this->Database->insert('admin_login', $Json);
            }
            if ($Result) {
                echo json_encode(array('status' => 'success', 'message' => 'Successfully'));
            }
            exit;
        }

        $UserId = base64_decode($this->segment3);
        $data['UserArray'] = '';
        $data['UserId'] = '';
        if (is_numeric($UserId)) {
            $CondutaionArray = array('id' => $UserId);
            $Qry = "SELECT * FROM `admin_login` WHERE id=:id";
            $UserArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $data['UserArray'] = $UserArray[0];
            $data['UserId'] = $UserId;
        }
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'AddNewUser', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function userpermission() {
        if (isset($_POST['SetUserId'])) {
            $permission = !empty($_POST['permission']) && is_array($_POST['permission']) ? $_POST['permission'] : array();
            $SetUserId = !empty($_POST['SetUserId']) ? $_POST['SetUserId'] : '';
            $cond = array('user_id' => $SetUserId);
            $this->Database->delete('user_permission', $cond);
            if (!empty($permission)) {
                $date = date('Y-m-d H:i:s');
                for ($i = 0; $i < count($permission); $i++) {
                    $d = $permission[$i];
                    $addPer = array(
                        'user_id' => $SetUserId,
                        'menu_id' => $d['MenuId'],
                        'additional_json' => !empty($d['actionpermission']) ? json_encode($d['actionpermission']) : '',
                        'timestamp' => $date,
                    );
                    $this->Database->insert('user_permission', $addPer);
                }
            }
            die(json_encode(array('status' => true, 'message' => 'successfully')));
        }
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'userpermission');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function delivery_boy() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'deliveryboy');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function deliveryboydetails() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'deliveryboydetails');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function add_deliveryboy() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'add_deliveryboy');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function loyality_points() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);


                $json['redm_point'] = !empty($_POST['redm_point']) ? $_POST['redm_point'] : '';
                $json['redm_amt'] = !empty($_POST['redm_amt']) ? $_POST['redm_amt'] : '';

                $json['lastupdate'] = date('Y-m-d H:i:s');
                $Id = $json['Id'];
                unset($json['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('XXXXloyality_points', $json);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    $this->Database->update('loyality_points', $json, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $ConditionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `loyality_points` WHERE id=:id ORDER BY id DESC";
                $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
                if (count($Array) > 0) {
                    $data['Id'] = $Id;
                    $data['loyality'] = $Array[0];
                }
            }

            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'addloyality_points', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $ConditionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `loyality_points` WHERE archive=:archive order by id DESC";
            $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
            $data['loyality'] = $Array;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'loyality_points', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function getBusinessName() {
        try {
            if ($this->input->is_ajax_request()) {
                $getData = $this->input->post();
                $store_type = $getData['store_type'];

                if (is_numeric($store_type)) {
                    $CondutaionArray = array('store_type' => $store_type);
                    $Qry = "SELECT restaurant_details.vendor_id,restaurant_details.restaurant_name FROM `users` LEFT JOIN restaurant_details ON restaurant_details.vendor_id=users.id WHERE store_type=:store_type and archive='0' AND status=1 
                    GROUP BY users.id,restaurant_details.restaurant_name  ORDER BY users.id DESC";
                    $StoreArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
                }
                echo json_encode($StoreArray);
            }
        } catch (Exception $e) {
            echo json_encode($e->getTraceAsString());
        }

        // $store_type = $_POST['store_type'];
        // $store_details = "SELECT restaurant_details.vendor_id,restaurant_details.restaurant_name FROM `users` LEFT JOIN restaurant_details ON restaurant_details.vendor_id=users.id WHERE store_type='$store_type' AND archive=0 AND status=1  ORDER BY users.id DESC";
        // $store_data = $this->Database->select_qry_array($store_details);
        // echo json_encode($store_data);
    }

    public function set_lang() {

        $lang = $_REQUEST['choose_lang'];

        $this->session->set_userdata("language", $lang);

        $url = $_REQUEST['url'];
        redirect(base_url() . $url);
    }

    public function rider_commission() {
        $segment3 = $this->segment3;
        $data = array();
        $vendor_data = $this->session->userdata('Vendor');
        if ($segment3 == 'update') {
            if (!empty($_REQUEST)) {
                $array = $_REQUEST;
                $arrays['restaurant_name'] = $array['restaurant_name'];
                $arrays['restaurant_name_ar'] = $array['restaurant_name_ar'];
                $arrays['opening_time'] = date("H:i", strtotime($array['opening_time']));
                $arrays['closing_time'] = date("H:i", strtotime($array['closing_time']));
                $arrays['delivery_time'] = $array['delivery_time'];
                $arrays['service_charge'] = $array['service_charge'];
                $arrays['min_amount'] = $array['min_amount'];
                $arrays['about'] = $array['about'];
                $arrays['about_ar'] = $array['about_ar'];
                $arrays['isDelivery	'] = $array['isDelivery'];

                // $arrays['pre_order'] = $array['pre_order'];
                $arrays['party_order'] = !empty($array['party_order']) ? $array['party_order'] : 0;
                $arrays['busy_status'] = $array['busy_status'];
                $arrays['delivery_time_start'] = !empty($array['delivery_time_start']) ? date("H:i", strtotime($array['delivery_time_start'])) : '';
                $arrays['delivery_time_ends'] = !empty($array['delivery_time_ends']) ? date("H:i", strtotime($array['delivery_time_ends'])) : '';
                $Id = $_REQUEST['Id'];
                $CondArray = array('vendor_id' => $Id);
                $Result = $this->Database->update('restaurant_details', $arrays, $CondArray);

                if (!empty($_REQUEST['LogoImages']) || !empty($_REQUEST['LicenseImages'])) {
                    if (!empty($_REQUEST['LogoImages'])) {
                        $ImageName = preg_replace('/\s+/', '', $vendor_data->name) . uniqid() . '.jpeg';
                        $UploadPath = HOME_DIR . 'uploads/vendor_images/' . $ImageName;
                        if (file_put_contents($UploadPath, file_get_contents($_REQUEST['LogoImages']))) {
                            $oldFile = HOME_DIR . (!empty($_REQUEST['LogoOldImage']) ? $_REQUEST['LogoOldImage'] : '');
                            if (is_file($oldFile)) {
                                unlink($oldFile);
                            }
                            $array1['image'] = $ImageName;
                        }
                    }

                    if (!empty($_REQUEST['LicenseImages'])) {
                        $ImageName1 = preg_replace('/\s+/', '', $vendor_data->name) . uniqid() . '.jpeg';
                        $UploadPath1 = HOME_DIR . 'uploads/trade_license/' . $ImageName1;
                        if (file_put_contents($UploadPath1, file_get_contents($_REQUEST['LicenseImages']))) {
                            $oldFile1 = HOME_DIR . (!empty($_REQUEST['LicenseOldImage']) ? $_REQUEST['LicenseOldImage'] : '');
                            if (is_file($oldFile1)) {
                                unlink($oldFile1);
                            }
                            $array1['trade_license'] = $ImageName1;
                        }
                    }
                    unset($array1['LogoImages']);
                    unset($array1['LogoOldImage']);
                    unset($array1['LicenseImages']);
                    unset($array1['LicenseOldImage']);
                }
                $array1['delivery_radius'] = $array['delivery_radius'];
                $CondArray1 = array('id' => $Id);
                $this->Database->update('users', $array1, $CondArray1);
                $cuisine_details = array('vendor_id' => $Id);
                $this->Database->delete('restaurant_cuisine_type', $cuisine_details);

                if (!empty($array["cuisine_id"])) {
                    foreach ($array["cuisine_id"] as $categoryId) {
                        $storeCuisine = [
                            "vendor_id" => $Id,
                            "cuisine_id" => $categoryId,
                            "lastupdate" => date('Y-m-d H:i:s'),
                        ];
                        $this->Database->insert('restaurant_cuisine_type', $storeCuisine);
                    }
                }
                if (!empty($_REQUEST['delivery_from'])) {
                    $CondArray = array('vendor_id' => $Id);
                    $this->Database->delete('delivery_timings', $CondArray);
                    foreach ($_REQUEST['delivery_from'] as $key => $product_item) {

                        $insertItems = [
                            "vendor_id" => $Id,
                            "delivery_from" => date('H:i', strtotime($_REQUEST['delivery_from'][$key])),
                            "delivery_to" => date('H:i', strtotime($_REQUEST['delivery_to'][$key])),
                            // "cities" => $_REQUEST['emirate'][$key],
                            "cities" => !empty($_REQUEST['emirate'][$key]['value']) ? implode(',', $_REQUEST['emirate'][$key]['value']) : '',
                        ];


                        if ($_REQUEST['delivery_from'][$key] != '') {
                            $this->Database->insert('delivery_timings', $insertItems);
                        }
                    }
                }
            }
            if ($Result) {
                $return = array('status' => 'success');
            }
            exit(json_encode($return));
        } else {
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'rider_commission');
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function testttt() {



        getMenudiscountArray($menuId = '4017', $menuPrice = '110', $vendorId = '256');
//        $html='<p>Dear Rider,</p>';
//      echo  emailTemplate($html);
//      exit;
        // send_mail('upendra@alwafaagroup.com', 'Test', 'Message');
        //   SendNotifactionOrderStatus($orderId = '4');
        //  updateRidercommissionByOrder($orderId = '112');
        //echo GetLoyalityPointsAmountByuserId('199');
    }

    public function features() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'features');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function rider_performance() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'rider_performance');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function chat() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'chat');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function orderlocation() {
        $this->load->view(ADMIN_DIR . 'orderlocation');
    }

    public function firebasejsnotify() {
        $this->load->view(ADMIN_DIR . 'notify.html');
    }

    public function TestTime() {
        $inser['message'] = 'A';
        $inser['sender_id'] = '33';
        $inser['receiver_id'] = '555';
        $this->Database->insert('chat_history', $inser);
    }

    public function citybyorder() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'citybyorder');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function vendor_position() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'vendor_position');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

   
    public function notification() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'notification');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }
    public function testt() {
//        send_mail('upendra@alwafaagroup.com', $subject='tes', $messages='tesss');
//         $vendorAr = GetusersById(256);
//          $addresssAr = GetuseraddressBy(502);
//         $distance_ord = distance($vendorAr->latitude, $vendorAr->longitude, $addresssAr->latitude, $addresssAr->longitude, $unit = 'K');
//echo $distance_ord;exit; 
////$distanceTotal = distance('32.43564580', '73.57406720','32.4292775377731', '73.57533666414031', $unit = 'K');
//     //   echo $distanceTotal;exit;
        
         SendpushNotifactionTorider(3966);
    }

}
