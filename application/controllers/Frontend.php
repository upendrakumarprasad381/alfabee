<?php

use LinkedIn\Client;
use LinkedIn\Scope;
use LinkedIn\AccessToken;

defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends CI_Controller {

    private $segment2;
    private $segment3;
    private $segment4;

    public function __construct() {

        parent::__construct();
        date_default_timezone_set(TIME_ZONE_GET);
        error_reporting(0);
        $this->load->Model('Database');
        $this->segment2 = $this->uri->segment(2);
        $this->segment3 = $this->uri->segment(3);
        $this->segment4 = $this->uri->segment(4);
        $this->load->library('form_validation');
        $this->load->helper('url');
        if ($this->session->userdata('language') == 'en' || empty($this->session->userdata('language'))) {
            $this->load->language("common", "english");
        } else {
            $this->load->language("common", "arabic");
        }
    }

    public function Helper() {
        $status = $_REQUEST['status'];
        switch ($status) {
            default :
                $_REQUEST['status']($_REQUEST);
        }
    }

    public function index() {
        // is_logged_user();
        $this->load->view(FRONTED_DIR . 'includes/header');
        $this->load->view(FRONTED_DIR . 'index');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function signUp() {
        // is_logged_user();
        is_not_logged_user();
        $segment3 = $this->segment3;
        $data = array();
        if ($segment3 == 'register') {
            if (isset($_REQUEST['json'])) {
                $json = $_REQUEST['json'];
                $array = json_decode($json, true);

                $check_email = "SELECT * FROM `users` WHERE  `email` LIKE '" . $array['email'] . "'";
                $sql = $this->Database->select_qry_array($check_email);
                if (count($sql) <= 0) {
                    // $array['reg_no'] = $code;
                    $array['lastupdate'] = date('Y-m-d H:i:s');
                    $array['password'] = md5($array['password']);
                    $array['user_type'] = '3';
                    $array['is_approved'] = '1';
                    // $array['original_pwd'] = $array['password'];
                    $Result = $this->Database->insert('users', $array);
                    if ($Result) {
                        $array['id'] = $Result;
                        $this->session->set_userdata('UserLogin', (object) $array);


                        $return = array('status' => 'success', 'message' => 'Your registration is successful');
                        $admin_data = "SELECT id FROM `admin_login` WHERE  `LoginType` =1";
                        $qry = $this->Database->select_qry_array($admin_data);

                        $update['sender_id'] = $Result;
                        $update['receiver_id'] = $qry[0]->id;
                        $update['notification_type'] = NEW_USER;
                        $update['notification_message'] = 'New user registered';
                        $update['redirect_url'] = 'admin/customers';

                        $this->Database->insert('notification', $update);
                        send_email_for_registration($array);
                    } else {
                        $return = array('status' => 'error', 'message' => 'Something went wrong');
                    }
                } else {
                    $return = array('status' => 'error', 'message' => 'Email already exists');
                }
                exit(json_encode($return));
            }
        } else if ($segment3 == 'login') {
            if (isset($_REQUEST['email']) && $_REQUEST['password']) {
                $email = $_REQUEST['email'];
                $password = $_REQUEST['password'];
                $md5_password = md5($password);
                $qry = "SELECT * FROM `users` WHERE `email` LIKE '$email' AND `password` LIKE '$md5_password' AND user_type=3 AND archive =0";
                $login_array = $this->Database->select_qry_array($qry);
                if (count($login_array) > 0 && $login_array[0]->user_type == 3) {
                    if ($login_array[0]->is_approved == "1") {
                        $this->session->unset_userdata('UserLogin');
                        $this->session->set_userdata('UserLogin', $login_array[0]);
                        $json = '{"status":"success","message":"Login Successfully"}';
                    } else {
                        $json = '{"status":"error","message":"Your account is not activated."}';
                    }
                } else {
                    $json = '{"status":"error","message":"Invalid email or password"}';
                }
            } else {
                $json = '{"status":"error","message":"Email and password is required"}';
            }
            echo $json;
        } else {
            $this->load->view(FRONTED_DIR . 'includes/header_inner');
            $this->load->view(FRONTED_DIR . 'sign_up');
            $this->load->view(FRONTED_DIR . 'includes/footer');
        }
    }

    // }
    public function myProfile() {
        is_logged_user();
        $segment3 = $this->segment3;
        if ($segment3 == 'update') {
            if (isset($_REQUEST)) {
                $array = $_REQUEST;
                $id = $array['id'];
                $CondArray = array('Id' => $id);
                $result = $this->Database->update('users', $array, $CondArray);
                if ($result) {
                    $json = '{"status":"success","message":"Updated Successfully"}';
                } else {
                    $json = '{"status":"error","message":"Something went wrong"}';
                }
                echo $json;
                exit;
            }
        } else {
            $this->load->view(FRONTED_DIR . 'includes/header_inner');
            $this->load->view(FRONTED_DIR . 'my_profile');
            $this->load->view(FRONTED_DIR . 'includes/footer');
        }
    }

    public function forgotPassword() {
        $segment3 = $this->segment3;
        if ($segment3 == 'update') {
            if ($_REQUEST['email'] != '') {
                $qry = "SELECT * FROM `users` WHERE `email` = '" . $_REQUEST['email'] . "' and archive=0 and status=1";
                $arr = $this->Database->select_qry_array($qry);
                if (!empty($arr)) {
                    send_email_for_forgot_password($arr[0]->id, $_REQUEST['email']);
                    $json = '{"status":"success","message":"Please check your mail to reset your password"}';
                } else {
                    $json = '{"status":"error","message":"User Not Found"}';
                }
            }
            echo $json;
            exit;
        } else {
            $this->load->view(FRONTED_DIR . 'includes/header_inner');
            $this->load->view(FRONTED_DIR . 'forgot_password');
            $this->load->view(FRONTED_DIR . 'includes/footer');
        }
    }

    public function resetpassword($id = '') {
        $segment3 = $this->segment3;
        if ($segment3 == 'update') {
            if ($_REQUEST['id'] != '' && $_REQUEST['password']) {
                $CondArray = array('id' => $_REQUEST['id']);
                $update_array = array('password' => md5($_REQUEST['password']));
                $result = $this->Database->update('users', $update_array, $CondArray);
                if ($result) {
                    $json = '{"status":"success","message":"Password updated successfully"}';
                } else {
                    $json = '{"status":"error","message":"Something went wrong"}';
                }
            }
            echo $json;
            exit;
        } else {
            $id = base64_decode($id);
            $data['id'] = $id;
            $this->load->view(FRONTED_DIR . 'includes/header_inner');
            $this->load->view(FRONTED_DIR . 'resetpassword', $data);
            $this->load->view(FRONTED_DIR . 'includes/footer');
        }
    }

    public function changePassword() {
        is_logged_user();
        $segment3 = $this->segment3;
        if ($segment3 == 'update') {
            if (isset($_REQUEST['json'])) {
                $json = $_REQUEST['json'];
                $array = json_decode($json, true);
                $id = $array['id'];
                $CondArray = array('Id' => $id);

                $slct_qry = "SELECT * FROM `users` WHERE password = '" . md5($array['password']) . "' AND id = '" . $id . "'";
                $user_array = $this->Database->select_qry_array($slct_qry);

                if (count($user_array) > 0) {
                    $json = '{"status":"error","message":"New password Cannot Same as old password"}';
                    echo $json;
                    exit;
                }
                if ($array['password'] != '') {
                    $array['Password'] = md5($array['password']);
                    // $array['original_pwd'] = $array['password'];
                } else {
                    unset($array['password']);
                }
                unset($array['id']);
                $result = $this->Database->update('users', $array, $CondArray);
                if ($result) {
                    $json = '{"status":"success","message":"Password Updated Successfully"}';
                    if ($array['password'] != '') {
                        $qry = "SELECT * FROM `users` WHERE `id`='" . $id . "'";
                        $user_array = $this->Database->select_qry_array($qry);

                        $mail_array['email'] = $user_array[0]->email;
                        $mail_array['name'] = $user_array[0]->name;
                        send_email_for_password_change($mail_array);
                    }
                } else {
                    $json = '{"status":"error","message":"Something went wrong"}';
                }
                echo $json;
                exit;
            }
        } else {
            $this->load->view(FRONTED_DIR . 'includes/header_inner');
            $this->load->view(FRONTED_DIR . 'change_password');
            $this->load->view(FRONTED_DIR . 'includes/footer');
        }
    }

    public function offers() {
        $this->load->view(FRONTED_DIR . 'includes/header');
        $this->load->view(FRONTED_DIR . 'offers');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function mostSelling() {
        $this->load->view(FRONTED_DIR . 'includes/header');
        $this->load->view(FRONTED_DIR . 'most_selling');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function homelyFoods() {

        $Qry = "SELECT users.id AS user_id,restaurant_details.vendor_id as id,users.image,restaurant_details.restaurant_name,restaurant_details.restaurant_name_ar,GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ', ')AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount,users.store_type 
        FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
        left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
        WHERE users.user_type=2 AND users.archive=0 AND users.is_approved=1 AND users.status=1
        GROUP BY users.id ORDER BY FIELD(restaurant_details.isPremium ,0, 1) DESC";
        // print_r($Qry);
        $Array = $this->Database->select_qry_array($Qry);
        $data['homely_food_users'] = $Array;

        $this->load->view(FRONTED_DIR . 'includes/header');
        $this->load->view(FRONTED_DIR . 'homely_foods', $data);
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function restaurant($id) {

        $Id = base64_decode($id);
        $selectTime = ",restaurant_details.opening_time,restaurant_details.closing_time,restaurant_details.delivery_hours_st,restaurant_details.delivery_hours_et";
        $Qry = "SELECT users.id AS user_id $selectTime ,restaurant_details.vendor_id as id,users.image,restaurant_details.restaurant_name,restaurant_details.restaurant_name_ar,GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ', ')AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount,users.store_type 
        FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
        left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
        WHERE users.user_type=2 AND store_type='$Id' AND users.archive=0 AND users.status=1 AND is_approved=1
        GROUP BY users.id,restaurant_details.restaurant_name,restaurant_details.restaurant_name_ar,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount,users.store_type,restaurant_details.isPremium  
        ORDER BY users.position ASC";

        $Array = $this->Database->select_qry_array($Qry);
        $data['homely_food_users'] = $Array;

        $this->load->view(FRONTED_DIR . 'includes/header');
        $this->load->view(FRONTED_DIR . 'store_category', $data);
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function homelyFoodsDetails($id = '') {

        $data['rest_id'] = base64_decode($id);
        $this->load->view(FRONTED_DIR . 'includes/header');
        $this->load->view(FRONTED_DIR . 'homelyfood_details', $data);
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function partyOrdersList() {

        $Qry = "SELECT users.id,users.image,restaurant_details.restaurant_name,GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ', ')AS cuisine_name,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount 
        FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
        left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
        WHERE users.user_type=2 AND party_order=1
        GROUP BY users.id ORDER BY FIELD(restaurant_details.isPremium ,0, 1) DESC";
        $Array = $this->Database->select_qry_array($Qry);
        $data['homely_food_users'] = $Array;
        $this->load->view(FRONTED_DIR . 'includes/header');
        $this->load->view(FRONTED_DIR . 'party_orders', $data);
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function searchResult() {
        if (!empty($this->session->userdata('location')) & !isset($_REQUEST['get_location'])) {
            $loc1 = $this->session->userdata('location');
            $loc2 = $this->session->userdata('sub_locality');
            $loc3 = $this->session->userdata('locality');
            $latitude = $this->session->userdata('latitude');
            $longitude = $this->session->userdata('longitude');
            $store_type = $this->session->userdata('store_type');
            $searchtype = $this->session->userdata('searchtype');
        } else {

            if (isset($_REQUEST['get_location']) && !empty($_REQUEST['latitude']) && !empty($_REQUEST['longitude'])) {
                $loc2 = !empty($_REQUEST['sublocality_level_1']) ? $_REQUEST['sublocality_level_1'] : '';
                $latitude = !empty($_REQUEST['latitude']) ? $_REQUEST['latitude'] : '';
                $longitude = !empty($_REQUEST['longitude']) ? $_REQUEST['longitude'] : '';

                $this->session->set_userdata('location', $loc2);
                $this->session->set_userdata('latitude', $latitude);
                $this->session->set_userdata('longitude', $longitude);
            }
            $loc1 = !empty($_REQUEST['get_location']) ? $_REQUEST['get_location'] : '';
            $loc2 = !empty($_REQUEST['sublocality_level_1']) ? $_REQUEST['sublocality_level_1'] : '';
            $loc3 = !empty($_REQUEST['locality']) ? $_REQUEST['locality'] : '';
            $loc4 = !empty($_REQUEST['administrative_area_level_1']) ? $_REQUEST['administrative_area_level_1'] : '';

            $latitude = !empty($_REQUEST['latitude']) ? $_REQUEST['latitude'] : '';
            $longitude = !empty($_REQUEST['longitude']) ? $_REQUEST['longitude'] : '';
            $store_type = !empty($_REQUEST['store_type']) ? $_REQUEST['store_type'] : 0;
            $searchtype = !empty($_REQUEST['searchtype']) ? $_REQUEST['searchtype'] : '';




            //  $this->session->set_userdata('location', $loc2);
            $this->session->set_userdata('sub_locality', $loc2);
            //  $this->session->set_userdata('latitude', $latitude);
            //  $this->session->set_userdata('longitude', $longitude);
            // $this->session->set_userdata('locality', $dat);
            $this->session->set_userdata('store_type', $store_type);
            $this->session->set_userdata('searchtype', $searchtype);
        }
        $filter = !empty($_REQUEST['filter']) ? $_REQUEST['filter'] : "";
        $bannerType = !empty($_REQUEST['bannerType']) ? $_REQUEST['bannerType'] : '';
        // if($loc2!='' && (!preg_match('/[^A-Za-z0-9]/', $loc2)))



        if (empty($latitude) || empty($longitude) || empty($store_type)) {
            redirect(base_url());
        }
        $having = '';
        $promoCode = "";
        if ($bannerType == 'offers') {
            $promoCode = ",(SELECT promocode_id FROM `promo_code`  WHERE date_from<=CURDATE() AND date_to>=CURDATE() AND user_type=1 AND status=0 AND archive=0 AND FIND_IN_SET(users.id,restaurant_id) LIMIT 1) AS promoCode";
            $having = " HAVING promoCode > 0";
        }

        $distnc = ",IFNULL(ROUND((6371 * acos( cos( radians($latitude) ) * cos( radians(users.latitude) ) * cos( radians(users.longitude) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians(users.latitude) ) ) ),2),0) AS distnc";
        $Qry1 = "SELECT restaurant_details.vendor_id,users.delivery_radius $distnc $promoCode
                FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
                
                WHERE user_type=2 AND store_type='$store_type' AND archive=0 AND is_approved=1 AND status=1 $having ORDER BY distnc ASC"; //AND latitude LIKE CONCAT('%', '" . $latitude . "' ,'%')

        $Array1 = $this->Database->select_qry_array($Qry1);




        $ids = [];
        for ($i = 0; $i < count($Array1); $i++) {
            $d = $Array1[$i];
            if (!empty($d->delivery_radius) && $d->distnc <= $d->delivery_radius) {
                array_push($ids, $d->vendor_id);
            } else if ($d->distnc <= 4) {
                array_push($ids, $d->vendor_id);
            }
        }



        $arr = implode("','", $ids);

        $cond = '';
        if ($searchtype == '2') {
            $cond = $cond . " AND restaurant_details.self_pickup='1'";
        }
        if ($bannerType == 'fast-delivery') {
            $cond = $cond . " AND restaurant_details.fastdelvry='1'";
        }if ($bannerType == 'party-orders') {
            $cond = $cond . " AND restaurant_details.party_order='1'";
        }
        $orderBy = 'users.position ASC';
        if ($filter == '1') {
            $orderBy = 'users.timestamp DESC';
        }
        $Qry1 = "SELECT users.id,users.image,users.position,restaurant_details.restaurant_name,restaurant_details.restaurant_name_ar,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar,restaurant_details.vendor_id,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount,restaurant_details.opening_time,restaurant_details.closing_time,restaurant_details.busy_status
        ,restaurant_details.isDelivery,restaurant_details.delivery_time_start,restaurant_details.delivery_time_ends,isPremium,users.store_type," .
                " '' as distance" .
                " FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
        left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
        WHERE restaurant_details.vendor_id IN('" . $arr . "')   $cond
        GROUP BY users.id,restaurant_details.restaurant_name,restaurant_details.restaurant_name_ar,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount,restaurant_details.opening_time,restaurant_details.closing_time,restaurant_details.busy_status,restaurant_details.isDelivery,restaurant_details.delivery_time_start,restaurant_details.delivery_time_ends,isPremium,users.store_type 
        ORDER BY $orderBy";
        // print_r($Qry1);die();
        $Array_data = $this->Database->select_qry_array($Qry1);
        $data['rest_details'] = $Array_data;
        $data['location'] = $this->session->userdata('location');
        $data['latitude'] = $this->session->userdata('latitude');
        $data['longitude'] = $this->session->userdata('longitude');
        $data['locality'] = $this->session->userdata('locality');
        $data['store_type'] = $this->session->userdata('store_type');
        // $this->session->unset_userdata('location');
        // $this->session->unset_userdata('latitude');
        // $this->session->unset_userdata('longitude');
        $this->load->view(FRONTED_DIR . 'includes/header');
        $this->load->view(FRONTED_DIR . 'search_result', $data);
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function search_details() {
        $this->load->view(FRONTED_DIR . 'includes/header');
        $this->load->view(FRONTED_DIR . 'search_details');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function check_cart() {
        $session_cart = $this->session->userdata('CartData');
        if (!empty($session_cart)) {
            $count = 0;
            $total = 0;
            foreach ($session_cart as $item) {
                $count += $item['quantity'];
            }
            $result = array('count' => $count);
            echo json_encode($result);
            exit;
        } else {
            echo "0";
        }
    }

    public function UpdateQuantity() {

        $session_cart = $this->session->userdata('CartData');
        if (isset($_POST)) {
            $quantityUpdated = '';
            $currentMenuId = trim($_POST['menu_id']);
            $cartIdPost = trim($_POST['cart_id']);
            for ($i = 0; $i < count($session_cart); $i++) {
                $sessionMenuid = trim($session_cart[$i]['menu_id']);
                $sessionCartId = trim($session_cart[$i]['cart_id']);
                if ($sessionMenuid == $currentMenuId) {

                    if ($_POST['store_type'] != 1) {
                        $stock = GetProductCountByProductId($sessionMenuid);
                        $menudetails = GetMenuById($sessionMenuid);
                        $menudetails = !empty($menudetails) ? $menudetails[0] : '';
                        $quantityee = !empty($_POST['quantity']) ? $_POST['quantity'] + 1 : '1';

                        if (!empty($menudetails->max_purchase_qty)) {
                            if ($_POST['type'] == 'plus' && $quantityee > $menudetails->max_purchase_qty) {
                                die(json_encode(array('status' => 'error', 'message' => "You can purchase only $menudetails->max_purchase_qty quantity.")));
                            }
                        }
                        if ($stock->stock < (int) $quantityee && $_POST['type'] == 'plus') {
                            die(json_encode(array('status' => 'error', 'message' => "Only $stock->stock  quantity available.")));
                        } else {
                            if ($currentMenuId == $sessionMenuid && $_POST['type'] == 'plus') {
                                $session_cart[$i]['quantity'] += 1;
                                $session_cart[$i]['subtotal'] = $session_cart[$i]['price'] * $session_cart[$i]['quantity'];
                                $quantityUpdated = $session_cart[$i]['quantity'];
                            } else if ($currentMenuId == $sessionMenuid && $_POST['type'] == 'minus' && $_POST['quantity'] > 1) {
                                if ($_POST['quantity'] > 0) {
                                    $session_cart[$i]['quantity'] -= 1;
                                    $session_cart[$i]['subtotal'] = $session_cart[$i]['price'] * $session_cart[$i]['quantity'];
                                    $quantityUpdated = $session_cart[$i]['quantity'];
                                }
                            }
                        }
                    } else {
                        if ($currentMenuId == $sessionMenuid && $_POST['type'] == 'plus') {
                            $session_cart[$i]['quantity'] += 1;
                            $session_cart[$i]['subtotal'] = $session_cart[$i]['price'] * $session_cart[$i]['quantity'];
                            $quantityUpdated = $session_cart[$i]['quantity'];
                        } else if ($currentMenuId == $sessionMenuid && $_POST['type'] == 'minus' && $_POST['quantity'] > 1) {
                            if ($_POST['quantity'] > 0) {
                                $session_cart[$i]['quantity'] -= 1;
                                $session_cart[$i]['subtotal'] = $session_cart[$i]['price'] * $session_cart[$i]['quantity'];
                                $quantityUpdated = $session_cart[$i]['quantity'];
                            }
                        }
                    }
                }
            }

            $session_cart = array_values(array_filter($session_cart));
            // print_r($session_cart);die();
            $this->session->set_userdata('CartData', $session_cart);
            $count = 0;
            $total_vat = 0;
            foreach ($session_cart as $item) {
                $count += $item['quantity'];
                // $total_vat += $item['vat_amount'];
            }
            $this->session->set_userdata('cart_count', $count);
            // $this->session->set_userdata('total_vat', $total_vat);
            die(json_encode(array('status' => 'success', 'quantityUpdated' => $quantityUpdated)));
        }
    }

    // public function UpdateQuantity() {
    //     $session_cart = $this->session->userdata('CartData');
    //     if (isset($_POST)) {
    //         for ($i = 0; $i < count($session_cart); $i++) {
    //             if ($_POST['menu_id'] == $session_cart[$i]['menu_id'] && $_POST['cart_id'] == $session_cart[$i]['cart_id'] && $_POST['type']=='plus') {
    //                 $session_cart[$i]['quantity'] += 1;
    //                 $session_cart[$i]['subtotal'] = $session_cart[$i]['price'] * $session_cart[$i]['quantity'];
    //             }else if ($_POST['menu_id'] == $session_cart[$i]['menu_id'] && $_POST['cart_id'] == $session_cart[$i]['cart_id'] && $_POST['type']=='minus' && $_POST['quantity']>1) {
    //                 if($_POST['quantity']>0)
    //                 {
    //                     $session_cart[$i]['quantity'] -= 1;
    //                     $session_cart[$i]['subtotal'] = $session_cart[$i]['price'] * $session_cart[$i]['quantity'];
    //                 }
    //             }
    //         }
    //         $session_cart = array_values(array_filter($session_cart));
    //         $this->session->set_userdata('CartData', $session_cart);
    //         $count = 0;
    //         $total_vat = 0;
    //         foreach ($session_cart as $item) {
    //             $count += $item['quantity'];
    //             // $total_vat += $item['vat_amount'];
    //         }
    //         $this->session->set_userdata('cart_count', $count);
    //         // $this->session->set_userdata('total_vat', $total_vat);
    //         echo 'success';
    //         exit;
    //     }
    // }
    public function checkout() {
        is_logged_user();
        $this->load->view(FRONTED_DIR . 'includes/header_inner');
        $this->load->view(FRONTED_DIR . 'checkout');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function cartProductCheck() {
        $this->session->unset_userdata('CartData');
    }

    public function payment() {
        $order = array();
        $price = 0;
        $discount = 0;
        $vat = 0;
        $session_cart = $this->session->userdata('CartData');
        $LoyalityPoints = GetSessionLoyalityPoints();
        $pointlolyted = GetSessionLoyalityPointsadded();
        $distance_ord = 0;
        if (empty($session_cart)) {
            redirect(base_url());
        } else {

            $cond = array('vendor_id' => $session_cart[0]['vendor_id']);
            $Qry = "SELECT * FROM  restaurant_details WHERE vendor_id=:vendor_id";
            $RestArray = $this->Database->select_qry_array($Qry, $cond);
            $rest345 = !empty($RestArray) ? $RestArray[0] : '';

            $vendor43623 = GetUserDetails($session_cart[0]['vendor_id']);
            $vendor43623 = !empty($vendor43623) ? $vendor43623[0] : '';
            $free_delivery = !empty($vendor43623->free_delivery) ? $vendor43623->free_delivery : '0';

            if (!empty($vendor43623->archive)) {
                die(json_encode(array('status' => false, 'message' => 'Shop is inactive')));
            }
            if ($vendor43623->status != '1') {
                die(json_encode(array('status' => false, 'message' => 'Shop is inactive')));
            }
 

            $delivery_type = !empty($_REQUEST['delivery_type']) ? $_REQUEST['delivery_type'] : '';
            $dlcharges = !empty($_POST['dlcharges']) ? $_POST['dlcharges'] : '0';
            if ($delivery_type == '2') {
                $dlcharges = 0;
            }
            if ($delivery_type == '1' && $free_delivery > 0) {
                if ($total > $free_delivery) {
                    $dlcharges = '0';
                }
            }



            foreach ($session_cart as $item) {
                $price += $item['subtotal'];
            }
            if (isset($session_cart[0]['discount_amt']) && $session_cart[0]['discount_amt'] != '') {

                if (empty($session_cart[0]['discount_from'])) {
                    $tempFordis = $price;
                } else {
                    $tempFordis = $dlcharges;
                }

                if (isset($session_cart[0]['discount_type']) && $session_cart[0]['discount_type'] == '1') {
                    $discount = $session_cart[0]['discount_amt'];
                } else {
                    $discount = $tempFordis * $session_cart[0]['discount_amt'];
                }
            }
            if (!empty($discount)) {
                if ($session_cart[0]['maximum_discount'] > 0) {
                    if ($discount > $session_cart[0]['maximum_discount']) {
                        $discount = $session_cart[0]['maximum_discount'];
                    }
                }
            }
            // $total = ($price - $discount)+$vat+$RestArray[0]->service_charge;
            $total = ($price - $discount) + $vat + 0;
            if (!empty($LoyalityPoints)) {
                $total = $total - $LoyalityPoints;
            }
            $user_data = $this->session->userdata('UserLogin');
            if ($session_cart[0]['party'] != 1) {
                $location = trim($session_cart[0]['location']);
                $saved_address = "SELECT id FROM `user_order_address` WHERE user_id=$user_data->id  AND isDefault=1 AND address  LIKE '%$location%' OR street LIKE '%$location%' ORDER BY id DESC";
                $address_details = $this->Database->select_qry_array($saved_address);
                $start_time = date('H:i', strtotime($RestArray[0]->delivery_time_start));
                $end_time = date('H:i', strtotime($RestArray[0]->delivery_time_ends));
                $currentTime = date('H:i', time());


                $delivery_addressId = !empty($session_cart[0]['delivery_address']) ? $session_cart[0]['delivery_address'] : $_REQUEST['address_id'];
                $addresssAr = GetuseraddressBy($delivery_addressId);
                $vendorAr = GetusersById($session_cart[0]['vendor_id']);

                if (!empty($addresssAr) && !empty($vendorAr)) {
                    $distance_ord = distance($vendorAr->latitude, $vendorAr->longitude, $addresssAr->latitude, $addresssAr->longitude, $unit = 'K');
                }
                if (!empty($rest345->busy_status)) {
                    die(json_encode(array("status" => false, 'type' => '1', "message" => "shop is closed.")));
                } else if (date('H:i', strtotime($rest345->opening_time)) >= date('H:i')) {
                    die(json_encode(array("status" => false, 'type' => '1', "message" => "shop is open only " . date('h:i A', strtotime($rest345->opening_time)) . ' - ' . date('h:i A', strtotime($rest345->closing_time)) . ".")));
                } else if (date('H:i', strtotime($rest345->closing_time)) <= date('H:i')) {
                    die(json_encode(array("status" => false, 'type' => '1', 'endTimeError' => true, "message" => "shop is open only " . date('h:i A', strtotime($rest345->opening_time)) . ' - ' . date('h:i A', strtotime($rest345->closing_time)) . ".")));
                }




                if ($start_time != '00:00' && $end_time != '00:00') {

                    if ($currentTime > $start_time && $currentTime < $end_time) {
                        $delivery = 1;
                    } elseif ($currentTime < $start_time && $currentTime < $end_time) {
                        $delivery = 1;
                    } else {
                        $delivery = 0;
                    }
                } else {
                    $delivery = 1;
                }
                $date_tmrw = date('Y-m-d', strtotime(' +1 day'));
                $time_tmrw = '09:00:00';
                if ($delivery == 1) {
                    $date = date('Y-m-d H:i:s');
                } else {
                    $date = date('Y-m-d H:i:s', strtotime("$date_tmrw $time_tmrw"));
                }


                $order = array('user_id' => $user_data->id,
                    'vendor_id' => $session_cart[0]['vendor_id'],
                    'delivery_address' => $delivery_addressId,
                    'price' => $price,
                    'discount' => $discount,
                    'loyality_discount' => $LoyalityPoints,
                    'loyality_point_discount' => !empty($LoyalityPoints) ? $pointlolyted : '0',
                    'total_price' => $total,
                    'vat' => $vat,
                    'service_charge' => $dlcharges,
                    'mode_of_payment' => 3,
                    'store_type' => !empty($_REQUEST['store_type']) ? $_REQUEST['store_type'] : '',
                    'delivery_type' => $delivery_type,
                    'special_request' => !empty($_REQUEST['special_request']) ? $_REQUEST['special_request'] : '',
                    'scheduled_delivery_date' => !empty($_REQUEST['schedule_delivery_date']) ? $_REQUEST['schedule_delivery_date'] : '',
                    'scheduled_delivery_time' => !empty($_REQUEST['schedule_delivery_time']) ? $_REQUEST['schedule_delivery_time'] : '',
                    //'pre_order' => $pre_order,
                    //'delivery_time' =>$deliv_time,
                    'status' => 1,
                    'distance_ord' => !empty($distance_ord) ? $distance_ord : '0',
                    'payment_date' => date('Y-m-d H:i:s'),
                    'date' => $date
                );

                $order['scheduled_delivery_date'] = !empty($_POST['schedule_date']) ? date('Y-m-d', strtotime($_POST['schedule_date'])) : '';
                $order['scheduled_delivery_time'] = !empty($_POST['schedule_date_time']) ? date('H:i:s', strtotime($_POST['schedule_date_time'])) : '';
                $order['scheduled_deliveryend_time'] = !empty($_POST['scheduled_deliveryend_time']) ? date('H:i:s', strtotime($_POST['scheduled_deliveryend_time'])) : '';

                $order['upload_prescription'] = !empty($_POST['upload_prescription']) ? $_POST['upload_prescription'] : '';


                $Result = $this->Database->insert('orders', $order);

                addloyalitypointcreditAmt($user_data->id, $Result, $order['total_price']);

                if (!empty($LoyalityPoints)) {

                    $this->session->set_userdata('CSLoyalityPoints', '');
                    $this->session->set_userdata('CSLoyalityINPoints', '');
                }
                $CondArray = array('id' => $Result);
                $orderno = date('dmy') . rand(10, 100) . $Result;
                $update_array = array('orderno' => $orderno);
                $this->Database->update('orders', $update_array, $CondArray);
                NeworderSendVendorNotifaction($Result);
                foreach ($session_cart as $cart) {
                    if (isset($cart['size'])) {
                        $array = array_column($cart['size'], 'id');
                        $array = implode(',', $array);
                    } else {
                        $array = '';
                    }
                    if (isset($cart['addon'])) {
                        $array1 = array_column($cart['addon'], 'id');
                        $array1 = implode(',', $array1);
                    } else {
                        $array1 = '';
                    }
                    if (isset($cart['topping'])) {
                        $array2 = array_column($cart['topping'], 'id');
                        $array2 = implode(',', $array2);
                    } else {
                        $array2 = '';
                    }
                    if (isset($cart['drink'])) {

                        $array3 = array_column($cart['drink'], 'id');
                        $array3 = implode(',', $array3);
                    } else {
                        $array3 = '';
                    }
                    if (isset($cart['dip'])) {

                        $array4 = array_column($cart['dip'], 'id');
                        $array4 = implode(',', $array4);
                    } else {
                        $array4 = '';
                    }
                    if (isset($cart['side'])) {
                        $array5 = array_column($cart['side'], 'id');
                        $array5 = implode(',', $array5);
                    } else {
                        $array5 = '';
                    }

                    $order_details = array(
                        'order_id' => $Result,
                        'category_id' => $cart['category'],
                        'price' => $cart['price'],
                        'product_id' => $cart['menu_id'],
                        'quantity' => $cart['quantity'],
                        'product_name' => $cart['menu_name'],
                        'subtotal' => $cart['subtotal'],
                        'vat' => 0,
                        'size' => $array,
                        'add_on' => $array1,
                        'topping' => $array2,
                        'drink' => $array3,
                        'dip' => $array4,
                        'side' => $array5,
                    );
                    $Result1 = $this->Database->insert('order_details', $order_details);
                    $menu_details = GetMenudetailsById($cart['menu_id']);
                    if ($menu_details[0]->store_type != '1') {
                        $CondP = array('id' => $cart['menu_id']);
                        $UpdateQtry['stock'] = $menu_details[0]->stock - $cart['quantity'];
                        $this->Database->update('menu_list', $UpdateQtry, $CondP);
                    }
                }
                order_pdf($Result);
                send_email_for_order($Result);
                send_email_for_order_to_Vendor($Result);

                $loyality_admin = "SELECT * FROM  loyality_points WHERE status=0 AND archive=0 ORDER BY id desc LIMIT 1";
                $loyality_Array = $this->Database->select_qry_array($loyality_admin);

                $Cond = array('user_id' => $user_data->id);
                $loyality = "SELECT * FROM  loyality_point_history WHERE user_id=:user_id  ORDER BY id desc LIMIT 1";
                $loyalityArray = $this->Database->select_qry_array($loyality, $Cond);

                $loy_amt = $price / $loyality_Array[0]->point_amt;
                if ($loyalityArray[0]->available_balance != 0) {
                    $avail_blnc = $loyalityArray[0]->available_balance + $loy_amt;
                } else {
                    $avail_blnc = $loy_amt;
                }

                $loyality_array['user_id'] = $user_data->id;
                $loyality_array['order_id'] = $Result;
                $loyality_array['amt_spent'] = $price;
                $loyality_array['reward_points'] = $loy_amt;
                $loyality_array['available_balance'] = $avail_blnc;
                $loyality_array['lastupdate'] = date('Y-m-d H:i:s');

                //$this->Database->insert('loyality_point_historyXXXX', $loyality_array);


                $session_user = $this->session->userdata('UserLogin');
                $Qry = "SELECT * FROM  orders WHERE id=:id";
                $RestArray = $this->Database->select_qry_array($Qry, $CondArray);

                $admin_data = "SELECT id FROM `admin_login` WHERE  `LoginType` =1";
                $qry = $this->Database->select_qry_array($admin_data);

                $commission = 'SELECT * FROM restaurant_details WHERE vendor_id=' . $session_cart[0]['vendor_id'];
                $commission_qry = $this->Database->select_qry_array($commission);

                $home_earning = ($total * $commission_qry[0]->commission) / 100;
                $rest_earning = $total - $home_earning;
                $comm_data = array(
                    'user_id' => $session_user->id,
                    'vendor_id' => $RestArray[0]->vendor_id,
                    'order_id' => $Result,
                    'amount' => $total,
                    'restaurant_earning' => $rest_earning,
                    'homeeats_earning' => $home_earning,
                    'transaction_type' => 2
                );
                $this->Database->insert('user_transactions', $comm_data);

                $vendor_array['sender_id'] = $session_user->id;
                $vendor_array['receiver_id'] = $RestArray[0]->vendor_id;
                $vendor_array['notification_type'] = NEW_ORDER;
                $vendor_array['notification_message'] = 'New Order Received';
                $vendor_array['redirect_url'] = 'vendor/OrderDetails/' . base64_encode($Result);
                $vendor_array['order_id'] = $Result;
                $vendor_array['inserted_on'] = date('Y-m-d H:i:s');

                $this->Database->insert('notification', $vendor_array);

                $admin_array['sender_id'] = $session_user->id;
                $admin_array['receiver_id'] = $qry[0]->id;
                $admin_array['notification_type'] = NEW_ORDER;
                $admin_array['notification_message'] = 'New Order Received';
                $admin_array['redirect_url'] = 'admin/OrderDetails/' . base64_encode($Result);
                ;
                $admin_array['order_id'] = $Result;
                $admin_array['inserted_on'] = date('Y-m-d H:i:s');

                $this->Database->insert('notification', $admin_array);
                die(json_encode(array('status' => 'success', 'redirect_url' => 'thankyou?orderId=' . $Result)));
            } else {
                $party_time = !empty($_REQUEST['party_time']) ? $_REQUEST['party_time'] : '';
                $party_time_end = !empty($_REQUEST['party_time_end']) ? $_REQUEST['party_time_end'] : '';


                if (!empty($rest345->busy_status)) {
                    die(json_encode(array("status" => false, 'type' => '1', "message" => "shop is closed.")));
                } else if (date('H:i', strtotime($rest345->opening_time)) >= date('H:i', strtotime($party_time))) {
                    die(json_encode(array("status" => false, 'type' => '1', "message" => "shop is open only " . date('h:i A', strtotime($rest345->opening_time)) . ' - ' . date('h:i A', strtotime($rest345->closing_time)) . ".")));
                } else if (date('H:i', strtotime($rest345->closing_time)) <= date('H:i', strtotime($party_time_end))) {
                    die(json_encode(array("status" => false, 'type' => '1', 'endTimeError' => true, "message" => "shop is open only " . date('h:i A', strtotime($rest345->opening_time)) . ' - ' . date('h:i A', strtotime($rest345->closing_time)) . ".")));
                }


                $order = array('user_id' => $user_data->id,
                    'vendor_id' => $session_cart[0]['vendor_id'],
                    'price' => $price,
                    'discount' => $discount,
                    'total_price' => $total,
                    'vat' => $vat,
                    'mode_of_payment' => 3,
                    'no_of_people' => $_REQUEST['no_of_people'],
                    'special_request' => !empty($_REQUEST['special_request']) ? $_REQUEST['special_request'] : '',
                    'party_date' => !empty($_REQUEST['party_date']) ? $_REQUEST['party_date'] : '',
                    'party_time' => $party_time,
                    'party_time_end' => $party_time_end,
                    'status' => 1,
                );

                $Result = $this->Database->insert('party_order', $order);
                $CondArray = array('id' => $Result);
                $orderno = 'PO-' . date('dmy') . rand(10, 100) . $Result;
                $update_array = array('party_orderno' => $orderno);
                $this->Database->update('party_order', $update_array, $CondArray);

                NewPartyorderSendVendorNotifaction($Result);
                foreach ($session_cart as $cart) {
                    $order_details = array(
                        'party_order_id' => $Result,
                        'category_id' => $cart['category'],
                        'price' => $cart['price'],
                        'product_id' => $cart['menu_id'],
                        'quantity' => $cart['quantity'],
                        'product_name' => $cart['menu_name'],
                        'subtotal' => $cart['subtotal'],
                        'vat' => 0,
                    );
                    $Result1 = $this->Database->insert('party_order_details', $order_details);
                }
                // order_pdf($Result);
                // send_email_for_order($Result);
                // send_email_for_order_to_Vendor($Result);

                $session_user = $this->session->userdata('UserLogin');
                $Qry = "SELECT * FROM  orders WHERE id=:id";
                $RestArray = $this->Database->select_qry_array($Qry, $CondArray);

                $admin_data = "SELECT id FROM `admin_login` WHERE  `LoginType` =1";
                $qry = $this->Database->select_qry_array($admin_data);

                $vendor_array['sender_id'] = $session_user->id;
                $vendor_array['receiver_id'] = $RestArray[0]->vendor_id;
                $vendor_array['notification_type'] = PARTY_ORDER;
                $vendor_array['notification_message'] = 'New Party Order Received';
                $vendor_array['redirect_url'] = 'vendor/partyorders';
                $vendor_array['order_id'] = $Result;

                $this->Database->insert('notification', $vendor_array);

                $admin_array['sender_id'] = $session_user->id;
                $admin_array['receiver_id'] = $qry[0]->id;
                $admin_array['notification_type'] = PARTY_ORDER;
                $admin_array['notification_message'] = 'New Party Order Received';
                $admin_array['redirect_url'] = 'admin/party_orders';
                $admin_array['order_id'] = $Result;

                $this->Database->insert('notification', $admin_array);
                die(json_encode(array('status' => 'success', 'redirect_url' => 'thank_you')));
            }
        }
    }

    public function thankyou() {
        $this->load->view(FRONTED_DIR . 'includes/header_inner');
        $this->load->view(FRONTED_DIR . 'payment');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function thankyou_partyorder() {
        $this->load->view(FRONTED_DIR . 'includes/header_inner');
        $this->load->view(FRONTED_DIR . 'payment_party');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function paymentresponse() {
        if (isset($_POST['pay'])) {
            $order_id = $this->session->userdata('order_id');
            $CondArray = array('id' => $order_id);
            $update_array = array('status' => 1, 'payment_date' => date('Y-m-d H:i:s'));
            $result = $this->Database->update('orders', $update_array, $CondArray);
            order_pdf($order_id);
            send_email_for_order($order_id);
            send_email_for_order_to_Vendor($order_id);
            $session_user = $this->session->userdata('UserLogin');
            $Qry = "SELECT * FROM  orders WHERE id=:id";
            $RestArray = $this->Database->select_qry_array($Qry, $CondArray);

            $admin_data = "SELECT id FROM `admin_login` WHERE  `LoginType` =1";
            $qry = $this->Database->select_qry_array($admin_data);


            $vendor_array['sender_id'] = $session_user->id;
            $vendor_array['receiver_id'] = $RestArray[0]->vendor_id;
            $vendor_array['notification_type'] = NEW_ORDER;
            $vendor_array['notification_message'] = 'New Order Received';
            $vendor_array['redirect_url'] = 'vendor/orders';
            $vendor_array['order_id'] = $order_id;

            $this->Database->insert('notification', $vendor_array);

            $admin_array['sender_id'] = $session_user->id;
            $admin_array['receiver_id'] = $qry[0]->id;
            $admin_array['notification_type'] = NEW_ORDER;
            $admin_array['notification_message'] = 'New Order Received';
            $admin_array['redirect_url'] = 'admin/orders';
            $admin_array['order_id'] = $order_id;

            $this->Database->insert('notification', $admin_array);

            if ($result) {
                // order_pdf($order_id);
                $this->session->unset_userdata('order_id');
                $this->session->unset_userdata('ordered');
                $this->session->unset_userdata('CartData');
                $this->session->unset_userdata('cart_count');
                $data['message'] = 'Your payment is successful';
            }
            $this->load->view(FRONTED_DIR . 'includes/header_inner');
            $this->load->view(FRONTED_DIR . 'paymentresponse', $data);
            $this->load->view(FRONTED_DIR . 'includes/footer');
        }
    }

    public function myOrders() {
        is_logged_user();
        $this->load->view(FRONTED_DIR . 'includes/header_inner');
        $this->load->view(FRONTED_DIR . 'my_orders');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function orderHistory() {
        $this->load->view(FRONTED_DIR . 'includes/header_inner');
        $this->load->view(FRONTED_DIR . 'order_product_history');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function myPartyOrders() {
        is_logged_user();
        $this->load->view(FRONTED_DIR . 'includes/header_inner');
        $this->load->view(FRONTED_DIR . 'my_party_orders');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function saveAddress() {
        if (isset($_REQUEST['json'])) {
            $json = $_REQUEST['json'];
            $array = json_decode($json, true);
            $user_data = $this->session->userdata('UserLogin');
            $array['user_id'] = $user_data->id;
            $array['isDefault'] = 1;
            if ($_REQUEST['content'] == '') {
                $Result = $this->Database->insert('user_order_address', $array);
            } else {
                $CondArray = array('id' => $_REQUEST['address_id']);
                $this->Database->update('user_order_address', $array, $CondArray);
                $Result = $_REQUEST['address_id'];
            }
            $session_cart = $this->session->userdata('CartData');
            if (!empty($session_cart)) {
                for ($i = 0; $i < count($session_cart); $i++) {
                    $session_cart[$i]['delivery_address'] = $Result;
                }
                $cart = $session_cart;
                $cart = array_values(array_filter($cart));
                $this->session->set_userdata('CartData', $cart);
            }
            if ($Result) {
                $return = array('status' => 'success', 'message' => 'Address saved successfully');
            } else {
                $return = array('status' => 'error', 'message' => 'Something went wrong');
            }
            exit(json_encode($return));
        }
    }

    public function partyOrder() {
        $session_arr = $this->session->userdata('UserLogin');
        if (empty($session_arr)) {
            $return = array('status' => 'error', 'redirecturl' => 'login_signup');
        } else {

            if (isset($_REQUEST)) {
                $array = $_REQUEST;
                $array['user_id'] = $session_arr->id;
                $Result = $this->Database->insert('partyorder_request', $array);

                $CondArray = array('id' => $Result);
                $orderno = 'PO-' . date('dm') . rand(10, 100) . $Result;
                $update_array = array('party_orderno' => $orderno);
                $result = $this->Database->update('partyorder_request', $update_array, $CondArray);

                $vendor_array['sender_id'] = $array['user_id'];
                $vendor_array['receiver_id'] = $array['vendor_id'];
                $vendor_array['notification_type'] = PARTY_ORDER;
                $vendor_array['notification_message'] = 'New Party Order Received';
                $vendor_array['redirect_url'] = 'vendor/partyorders';
                $vendor_array['party_order_id'] = $Result;

                $this->Database->insert('notification', $vendor_array);

                $return = array('status' => 'success', 'message' => 'Request send successfully');
            } else {
                $return = array('status' => 'error', 'message' => 'Something went wrong');
            }
        }
        exit(json_encode($return));
    }

    public function PaymentInvocie() {
        $this->load->view(FRONTED_DIR . 'includes/header_inner');
        $this->load->view(FRONTED_DIR . 'request_payment_invoice');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function userFeedback() {
        $session_arr = $this->session->userdata('UserLogin');
        if (empty($session_arr)) {
            $return = array('status' => 'error', 'redirecturl' => 'login_signup');
        } else {
            $session_arr = $this->session->userdata('UserLogin');
            if (isset($_REQUEST)) {
                $array = $_REQUEST;
                $array['user_id'] = $session_arr->id;
                $array['vendor_id'] = $array['vendor_id'];
                // $array['order_id'] = $array['order_id'];
                $array['rating'] = $array['rating'];
                $array['comments'] = $array['comments'];
                $Result = $this->Database->insert('user_feedback', $array);
                $return = array('status' => 'success', 'message' => 'Rated Successfully');
            } else {
                $return = array('status' => 'error', 'message' => 'Something went wrong');
            }
        }
        exit(json_encode($return));
    }

    public function contact_us() {
        $data = array();

        if (isset($_REQUEST) && !empty($_REQUEST)) {
            $Insert = array(
                'name' => $_REQUEST['name'],
                'email' => $_REQUEST['email'],
                'message' => $_REQUEST['message'],
                'mobile_number' => $_REQUEST['mobile_number'],
            );
            $result = $this->Database->insert('contact_us', $Insert);
            sendContactusEmail($result);
            // SendMialContactUs($Contactid);
            if ($result) {
                $json = '{"status":"success","message":"Send Successfully"}';
            } else {
                $json = '{"status":"error","message":"Something went wrong"}';
            }
            echo $json;
            exit;
        }
        $this->load->view(FRONTED_DIR . 'includes/header');
        $this->load->view(FRONTED_DIR . 'contact_us');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function suggestHomeBusiness() {
        $data = array();

        if (isset($_REQUEST) && !empty($_REQUEST)) {
            $Insert = array(
                'name' => $_REQUEST['name'],
                'location' => $_REQUEST['location'],
                'contact_details' => $_REQUEST['contact_details'],
                'additional_comments' => $_REQUEST['additional_comments'],
            );
            $result = $this->Database->insert('suggest_restaurant', $Insert);
            // SendMialContactUs($Contactid);
            if ($result) {
                $json = '{"status":"success","message":"Send Successfully"}';
            } else {
                $json = '{"status":"error","message":"Something went wrong"}';
            }
            echo $json;
            exit;
        }
        $this->load->view(FRONTED_DIR . 'includes/header_inner');
        $this->load->view(FRONTED_DIR . 'suggest_homebusiness');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function savedAddress() {
        $this->load->view(FRONTED_DIR . 'includes/header_inner');
        $this->load->view(FRONTED_DIR . 'saved_address');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function about_us() {
        $this->load->view(FRONTED_DIR . 'includes/header_inner');
        $this->load->view(FRONTED_DIR . 'about_us');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function terms_condition() {
        $this->load->view(FRONTED_DIR . 'includes/header_inner');
        $this->load->view(FRONTED_DIR . 'terms_condition');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function privacy_policy() {
        $this->load->view(FRONTED_DIR . 'includes/header_inner');
        $this->load->view(FRONTED_DIR . 'privacy_policy');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function partner_registration() {
        $this->load->view(FRONTED_DIR . 'includes/header');
        $this->load->view(FRONTED_DIR . 'partner_registration');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function rider_registration() {
        $this->load->view(FRONTED_DIR . 'includes/header');
        $this->load->view(FRONTED_DIR . 'rider_registration');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function set_lang() {

        $lang = $_REQUEST['choose_lang'];

        $this->session->set_userdata("language", $lang);

        $url = $_REQUEST['url'];
        die(json_encode(array('status' => true, 'message' => '')));
    }

    public function termsAndConditions() {
        $this->load->view(FRONTED_DIR . 'includes/header');
        $this->load->view(FRONTED_DIR . 'termsAndConditions');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function faq() {
        $this->load->view(FRONTED_DIR . 'includes/header');
        $this->load->view(FRONTED_DIR . 'faq');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function privacypolicy() {
        $this->load->view(FRONTED_DIR . 'includes/header');
        $this->load->view(FRONTED_DIR . 'privacypolicy');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function loyaltyProgram() {
        $this->load->view(FRONTED_DIR . 'includes/header');
        $this->load->view(FRONTED_DIR . 'loyaltyProgram');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function guidelinesforVendors() {
        $this->load->view(FRONTED_DIR . 'includes/header');
        $this->load->view(FRONTED_DIR . 'guidelinesforVendors');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function reservations() {
        $session_arr = $this->session->userdata('UserLogin');
        //  if(empty($session_arr))
        //  {
        //      $return = array('status' => 'error', 'redirecturl' => 'login_signup/callback');
        //  }else{
        $session_arr = $this->session->userdata('UserLogin');
        if (isset($_REQUEST)) {
            $array = $_REQUEST;
            $today_date = date('Y-m-d');
            $current_time = date('H:i');
            if ($array['reservation_date'] == $today_date && $array['reservation_time'] < $current_time) {

                $return = array('status' => false, 'message' => 'At the moment, there’s no table availability');
            } else {
                $Result = $this->Database->insert('book_table', $array);
                $return = array('status' => 'success', 'message' => 'Reservation Successfull');
            }
        } else {
            $return = array('status' => 'error', 'message' => 'Something went wrong');
        }
        //  }
        exit(json_encode($return));
    }

    public function rider_live_track() {
        $this->load->view(ADMIN_DIR . 'rider_live_track');
    }

    public function livetracking() {
        $this->load->view(FRONTED_DIR . 'livetracking_1');
    }

    public function testlocation() {
        $this->load->view(FRONTED_DIR . 'testlocation');
    }

    public function logout() {
        $this->session->unset_userdata('UserLogin');
        session_destroy();
        redirect(base_url('/'));
    }

}
