<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    private $segment2;
    private $segment3;
    private $segment4;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set(TIME_ZONE_GET);
        $this->load->Model('Database');
        $this->segment2 = $this->uri->segment(2);
        $this->segment3 = $this->uri->segment(3);
        $this->segment4 = $this->uri->segment(4);
    }

    public function Admin() {
        $this->load->view(ADMIN_DIR . 'Login');
    }

    public function CheckAdminLogin() {
        $json = array();
        if (isset($_REQUEST['UserName']) && !empty($_REQUEST['UserName'])) {
            $UserName = $_REQUEST['UserName'];
            $Password = $_REQUEST['Password'];
            $Md5Password = md5($Password);
            $Qry = "SELECT * FROM admin_login WHERE email=:email AND password=:password";
            $CondutaionArray = array('email' => $UserName, 'password' => $Md5Password);
            $UserArray = $this->Database->select_qry_array($Qry, $CondutaionArray);


            if (!empty($UserArray)) {
                $UserArray = $UserArray[0];
                if ($UserArray->archive == 0 && $UserArray->status == 0) {
                    $this->session->set_userdata('Admin', $UserArray);
                    $json = array('status' => 'success', 'RedirectURL' => base_url('Admin'), 'msg' => 'Login successfully');
                } else {
                    $json = array('status' => 'error', 'msg' => 'Your account has been blocked.');
                }
            } else {
                $json = array('status' => 'error', 'msg' => 'Invalid user name and password.'
                );
            }
        } else {
            $json = array('status' => 'error', 'msg' => 'Invalid credentials.'
            );
        }
        echo json_encode($json);
    }

    public function AdminLoginout() {
        $this->session->unset_userdata('Admin');
        redirect(base_url('Login/Admin'));
    }

    public function Vendor() {
        $this->load->view(VENDOR_DIR . 'Login');
    }

    public function CheckVendorLogin() {
        $json = array();
        if (isset($_REQUEST['UserName']) && !empty($_REQUEST['UserName'])) {
            $UserName = $_REQUEST['UserName'];
            $Password = $_REQUEST['Password'];
            $Md5Password = md5($Password); 
            $Qry = "SELECT * FROM users WHERE email='$UserName' AND password='$Md5Password'  AND user_type=2 AND status=1";  
            $CondutaionArray = array('email' => $UserName, 'password' => $Md5Password);
            $UserArray = $this->Database->select_qry_array($Qry);
            if (!empty($UserArray)) {
                $UserArray = $UserArray[0];
                if ($UserArray->archive == 0 && $UserArray->is_approved == 1) {
                    $this->session->set_userdata('Vendor', $UserArray);
                    $json = array('status' => 'success', 'RedirectURL' => base_url('vendor_dashboard'), 'msg' => 'Login successfully');
                } elseif ($UserArray->user_type == 2 && $UserArray->is_approved == 0) {
                    $json = array('status' => 'error', 'msg' => 'Your approval is pending, please check after sometime');
                } else {
                    $json = array('status' => 'error', 'msg' => 'Your account has been blocked.');
                }
            } else {
                $json = array('status' => 'error', 'msg' => 'Invalid user name and password.');
            }
        } else {
            $json = array('status' => 'error', 'msg' => 'Invalid credentials.'
            );
        }
        echo json_encode($json);
    }

    public function VendorRegistration() {
        $this->load->view(VENDOR_DIR . 'Register');
    }

    public function ERROR_404() {
        $data = array();
        $data['URL'] = base_url();
        $CurrentController = $this->router->fetch_class();
        if ($CurrentController == 'Admin') {
            $data['URL'] = base_url('Admin');
        }
        $this->load->view(ADMIN_DIR . '404', $data);
    }

    public function Google() {
        $this->load->library('Google');
        $this->google->Login();
    }

    public function GoogleLoginDetails() {
        if (isset($_GET['code'])) {
            $Code = $_GET['code'];
            $this->load->library('Google');
            $Array = $this->google->UserInfo($Code);
            if (!empty($Array)) {
                $this->CheckSocialMedia($Array);
            }
        } else {
            $this->Google();
        }
    }

    public function Facebook() {
        $this->load->library('Facebook');
        $this->facebook->Login();
    }

    public function FacebookLoginDetails() {
        if (isset($_GET['code'])) {
            $this->load->library('Facebook');
            $state = '';
            if (isset($_GET['state'])) {
                $state = $_GET['state'];
            }
            $UserInfo = $this->facebook->UserInfo($state);
            if (is_array($UserInfo)) {
                $this->CheckSocialMedia($UserInfo);
            }
        }
    }

    public function CheckSocialMedia($Array) {

        if (isset($Array['login_type']) && isset($Array['social_id'])) {
            $Qry = "SELECT * FROM `users` WHERE `social_id` =:social_id AND login_type=:login_type";
            $CondutaionArray = array('social_id' => $Array['social_id'], 'login_type' => $Array['login_type']);
            $CheckUser = $this->Database->select_qry_array($Qry, $CondutaionArray);
            if ($Array['image'] != '') {
                $ImageName = $Array['social_id'] . '.jpg';
                $ProfilePic = HOME_DIR . 'uploads/user_images/' . $ImageName;
                if (is_file($ProfilePic)) {
                    unlink($ProfilePic);
                }
                if (count($CheckUser) > 0) {
                    $OldPic = HOME_DIR . 'uploads/user_images/' . $CheckUser[0]->image;
                    if (is_file($OldPic)) {
                        unlink($OldPic);
                    }
                }
                if (file_put_contents($ProfilePic, file_get_contents($Array['image']))) {
                    
                }
            }
            if (count($CheckUser) == 0) {
                if ($Array['email'] == '') {
                    die(json_encode(array('status' => 'error', 'message' => 'Email id not exists')));
                }
                $Qry = "SELECT * FROM `users` WHERE `email` =:email";
                $CondutaionArray = array('email' => $Array['email']);
                $CheckEmail = $this->Database->select_qry_array($Qry, $CondutaionArray);
                if (count($CheckEmail) == 0) {
                    $InsertArray = array(
                        'name' => $Array['name'],
                        'email' => $Array['email'],
                        'social_id' => $Array['social_id'],
                        'login_type' => $Array['login_type'],
                        // 'birthday' => date('Y-m-d', strtotime($Array['Birthday'])),
                        // 'gender' => ucfirst($Array['Gender']) == 'Male' ? 1 : 2,
                        'mobile_number' => 0,
                        // 'mobile_code' => 0,
                        'user_type' => 3,
                        'is_approved' => 1,
                        'lastupdate' => date('Y-m-d H:i:s'),
                    );
                    $UserId = $this->Database->insert('users', $InsertArray);
                } else {
                    $UserId = $CheckEmail[0]->id;
                }
            } else {
                $UserId = $CheckUser[0]->id;
            }
            $InsertArray = array(
                'social_id' => $Array['social_id'],
                'login_type' => $Array['login_type'],
            );
            // if (count($CheckUser) > 0) {
            //     if ($Array['Birthday'] != '') {
            //         $InsertArray['birthday'] = date('Y-m-d', strtotime($Array['Birthday']));
            //     }if ($Array['Gender'] != '' && $CheckUser[0]->gender != '') {
            //         $InsertArray['gender'] = ucfirst($Array['Gender']) == 'Male' ? 1 : 2;
            //     }
            // }
            if (isset($ImageName)) {
                $InsertArray['image'] = $ImageName;
            }
            $CondArray = array('id' => $UserId);
            $this->Database->update('users', $InsertArray, $CondArray);
            if (!isset($UserId) || !is_numeric($UserId)) {
                die(json_encode(array('status' => 'error', 'message' => 'User Id Issue')));
            }
            $Qry = "SELECT * FROM `users` WHERE `id` =:id";
            $CondutaionArray = array('id' => $UserId);
            $CheckUser = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $CheckUser = $CheckUser[0];
            if ($CheckUser->archive == 0) {
                $this->session->set_userdata('UserLogin', $CheckUser);
                $AdditionalSession = GetAdditionalSession();
                if (isset($AdditionalSession['CallBackURL']) && $AdditionalSession['CallBackURL'] != '') {
                    $Unnset = ['CallBackURL'];
                    UnsetAdditionalSession($Unnset);
                    header('Location: ' . filter_var($AdditionalSession['CallBackURL'], FILTER_SANITIZE_URL));
                } else {
                    header('Location: ' . filter_var(base_url(), FILTER_SANITIZE_URL));
                }
            } else if ($CheckUser->archive == 1) {
                die(json_encode(array('status' => false, 'message' => 'Your account has been blocked by HomeEats admin. contact with admin')));
            }
        }
    }
    public function invoice() {
           $this->load->view(ADMIN_DIR . 'invoice_print');
    }
    public function vendor_forgot_password() {
        $this->load->view(VENDOR_DIR . 'vendor_forgot_password');
    }

}
