<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    private $segment2;
    private $segment3;
    private $segment4;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set(TIME_ZONE_GET);
        $this->load->Model('Database');
        $this->segment2 = $this->uri->segment(2);
        $this->segment3 = $this->uri->segment(3);
        $this->segment4 = $this->uri->segment(4);
        $this->load->library('form_validation');

        if ($this->session->userdata('language') == 'en' || empty($this->session->userdata('language'))) {
            $this->load->language("common", "english");
        } else {
            $this->load->language("common", "arabic");
        }
    }

    public function addYourFoodVendor() {
        $this->load->view(FRONTED_DIR . 'includes/header_login');
        $this->load->view(FRONTED_DIR . 'add_your_food');
        $this->load->view(FRONTED_DIR . 'includes/footer');
    }

    public function loginregistration() {

        // is_not_logged_user();
        $segment3 = $this->segment3;
        $data = array();
        if ($segment3 == 'vendor_register') {
            if (isset($_REQUEST['json'])) {
                $json_data = $_REQUEST['json'];

                $array = json_decode($json_data, true);

                $check_email = "SELECT * FROM `users` WHERE  `email` LIKE '" . $array['email'] . "' AND user_type=2";
                $sql = $this->Database->select_qry_array($check_email);
                if (count($sql) <= 0) {
                    $arrays['name'] = $array['name'];
                    // $arrays['last_name'] = $array['last_name'];
                    $arrays['email'] = $array['email'];
                    $arrays['mobile_number'] = $array['mobile_number'];
                    $arrays['mobile_code'] = '92';

                    //$arrays['city'] = $array['city'];
                    $arrays['area'] = $array['area'];
                    //$arrays['approx_location'] = $array['approx_location'];
                    //$arrays['location'] = $array['location'];
                    //$arrays['street'] = $array['street'];
                    $arrays['latitude'] = $array['latitude'];
                    $arrays['longitude'] = $array['longitude'];
                    $arrays['lastupdate'] = date('Y-m-d H:i:s');
                    $arrays['password'] = isset($array['password']) ? md5($array['password']) : '';
                    $arrays['user_type'] = '2';
                    $arrays['store_type'] = $array['store_type'];
                    $arrays['delivery_radius'] = !empty($array['delivery_radius']) ? $array['delivery_radius'] : 0;
                    if (!empty($array['LogoImages'])) {
                        $ImageName = preg_replace('/\s+/', '', $array['name']) . uniqid() . '.jpeg';
                        $UploadPath = HOME_DIR . 'uploads/vendor_images/' . $ImageName;
                        if (file_put_contents($UploadPath, file_get_contents($array['LogoImages']))) {

                            $arrays['image'] = $ImageName;
                        }
                    }
                    if (!empty($array['LicenseImages'])) {
                        $ImageNameLicense = preg_replace('/\s+/', '', $array['name']) . uniqid() . '.jpeg';
                        $UploadPathLicense = HOME_DIR . 'uploads/trade_license/' . $ImageNameLicense;
                        if (file_put_contents($UploadPathLicense, file_get_contents($array['LicenseImages']))) {
                            $arrays['trade_license'] = $ImageNameLicense;
                        }
                    }


                    $result = $this->Database->insert('users', $arrays);
                    if ($result) {
                        $array['id'] = $result;
                        $this->session->set_userdata('vendorlogin', (object) $array);
                        $this->session->unset_userdata('UserLogin');

                        send_email_for_vendor_registration($array);

                        // if($array['isDelivery']=='on')
                        // {
                        //     $delivery = 1;
                        // }else{
                        //     $delivery = 0;
                        // }
                        // if($array['owner']=='on')
                        // {
                        //     $owner = 1;
                        // }else{
                        //     $owner = 0;
                        // }
                        $restaurant['vendor_id'] = $result;
                        $restaurant['restaurant_name'] = $array['restaurant_name'];
                        if (isset($array['isDelivery']) && $array['isDelivery'] != 'undefined') {
                            $del = $array['isDelivery'];
                        } else {
                            $del = 0;
                        }
                        $restaurant['isDelivery'] = $del;
                        $restaurant['service_charge'] = !empty($array['service_charge']) ? $array['service_charge'] : 0;

                        // $restaurant['owner'] = $owner;
                        // $restaurant['about'] = $array['about'];

                        $this->Database->insert('restaurant_details', $restaurant);


                        $dat['cuisine_id'] = $array['primary_cuisine_type'];

                        if (!empty($dat['cuisine_id']) && $dat['cuisine_id'] != 'null') {
                            $cus = explode(',', $dat['cuisine_id']);
                            foreach ($cus as $c) {
                                $datas['vendor_id'] = $result;
                                $datas['cuisine_id'] = $c;
                                $datas['lastupdate'] = date('Y-m-d H:i:s');
                                $this->Database->insert('restaurant_cuisine_type', $datas);
                            }
                        }

                        if (!empty($_REQUEST['delivery_from'])) {
                            foreach ($_REQUEST['delivery_from'] as $key => $product_item) {

                                $insertItems = [
                                    "vendor_id" => $result,
                                    "delivery_from" => !empty($_REQUEST['delivery_from'][$key]) ? date('H:i', strtotime($_REQUEST['delivery_from'][$key])) : '',
                                    "delivery_to" => !empty($_REQUEST['delivery_to'][$key]) ? date('H:i', strtotime($_REQUEST['delivery_to'][$key])) : '',
                                    // "cities" => $_REQUEST['emirate'][$key],
                                    "cities" => implode(',', $_REQUEST['emirate'][$key]['value']),
                                ];

                                if ($_REQUEST['delivery_from'][$key] != '') {
                                    $this->Database->insert('delivery_timings', $insertItems);
                                }
                            }
                        }

                        $admin_data = "SELECT id FROM `admin_login` WHERE  `LoginType` =1";
                        $qry = $this->Database->select_qry_array($admin_data);

                        $update['sender_id'] = $result;
                        $update['receiver_id'] = $qry[0]->id;
                        $update['notification_type'] = NEW_RESTAURANT;
                        $update['notification_message'] = 'New Vendor Request';
                        $update['redirect_url'] = 'admin/vendor';

                        $this->Database->insert('notification', $update);

                        $return = array('status' => 'success', 'message' => 'Your registration is successful');
                    } else {
                        $return = array('status' => 'error', 'message' => 'Something went wrong');
                    }
                } else {
                    $return = array('status' => 'error', 'message' => 'Email already exists');
                }

                exit(json_encode($return));
            }
        } elseif ($segment3 == 'rider_register') {
            if (isset($_REQUEST)) {
                // $json_data = $_REQUEST['json'];

                $array = $_REQUEST;
//                echo '<pre>';
//                print_r($array);
//                exit;
                $check_email = "SELECT * FROM `users` WHERE  `email` LIKE '" . $array['email_id'] . "' AND user_type=4";
                $sql = $this->Database->select_qry_array($check_email);
                if (count($sql) <= 0) {
                    $arrays['name'] = $array['rider_name'];
                    $arrays['father_name'] = $array['father_name'];
                    $arrays['area'] = $array['area'];
                    $arrays['latitude'] = $array['latitude'];
                    $arrays['longitude'] = $array['longitude'];
                    $arrays['nic'] = $array['nic'];

                    $arrays['mobile_number'] = $array['mobile_number'];
                      $arrays['mobile_code'] = '92';
                    $arrays['bike_type'] = $array['bike_type'];
                    $arrays['18years_old'] = $array['age'];
                    $arrays['license'] = $array['license'];
                    $arrays['email'] = $array['email_id'];
                    $arrays['password'] = isset($array['password']) ? md5($array['password']) : '';
                    $arrays['user_type'] = '4';
                    $arrays['delivery_radius'] = !empty($array['delivery_radius']) ? $array['delivery_radius'] : 0;
                    $arrays['deliveryboy_added_by'] = '1';
                    $arrays['lastupdate'] = date('Y-m-d H:i:s');

                    $result = $this->Database->insert('users', $arrays);
                    if ($result) {
                        $array['id'] = $result;

                        
                        $admin_data = "SELECT id FROM `admin_login` WHERE  `LoginType` =1";
                        $qry = $this->Database->select_qry_array($admin_data);

                        $update['sender_id'] = $result;
                        $update['receiver_id'] = $qry[0]->id;
                        $update['notification_type'] = NEW_RIDER;
                        $update['notification_message'] = 'New Rider Registered';
                        $update['redirect_url'] = 'admin/delivery_boy';

                        $this->Database->insert('notification', $update);
                        
                        
                        

                        send_email_for_registration_rider($array);

                        $return = array('status' => 'success', 'message' => 'Your registration is successful');
                    } else {
                        $return = array('status' => 'error', 'message' => 'Something went wrong');
                    }
                } else {
                    $return = array('status' => 'error', 'message' => 'Email already exists');
                }

                exit(json_encode($return));
            }
        } else {

            // $this->load->view(VENDOR_DIR . 'includes/header');
            $this->load->view(VENDOR_DIR . 'Register');
            // $this->load->view(VENDOR_DIR . 'includes/footer');
        }
    }

}
