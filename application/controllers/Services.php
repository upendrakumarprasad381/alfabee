<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {

    public $success = 200;
    public $error = 401;
    public $language = 'UR';

    public function __construct() {
        parent::__construct();
        $Header = $this->input->request_headers();
        date_default_timezone_set(TIME_ZONE_GET);
        $this->load->Model('Database');
        $User = isset($Header['User-Agents']) ? $Header['User-Agents'] : '';
        $Authtoken = isset($Header['Authtoken']) ? $Header['Authtoken'] : '';

        $LanguageCode = isset($Header['Language-Code']) ? $Header['Language-Code'] : '';
        $this->language = strtolower($LanguageCode) == 'ur' ? 'UR' : 'EN';

        $_REQUEST['languagecode'] = $this->language;
        error_reporting(0);
        $json = file_get_contents('php://input');
        $Method = $this->router->fetch_method();
        // send_mail('upendra@alwafaagroup.com', 'AlfaBee - Method-' . $Method, json_encode($_POST));
        if ($User != 'com.alfabee.com' || $Authtoken != 'Alwafaa123') {
            header('Content-Type: application/json');
            die(json_encode(array("response" => array("status" => false, "message" => "Auth Failed"))));
        }

        if ($this->language == 'UR') {
            $this->load->language("common", "app_arabic");
        } else {
            $this->load->language("common", "app_englist");
        }

        $this->login_type = 3;
        header('Content-Type: application/json');
    }

    public function register() {
        // $json = file_get_contents('php://input');
        // $json = json_decode($json, TRUE);
        $json = $_POST;

        $today = date('Y-m-d H:i:s');
        $mobile_number = !empty($json['mobile_number']) ? $json['mobile_number'] : '';
        $mobile_code = !empty($json['mobile_code']) ? $json['mobile_code'] : '';
        $invitation_code = !empty($json['invitation_code']) ? $json['invitation_code'] : '';
        $email = !empty($json['email']) ? $json['email'] : '';
        $full_name = !empty($json['full_name']) ? $json['full_name'] : '';
        $isValidate = !empty($json['is_validate']) ? $json['is_validate'] : '';


        $qry = "SELECT * FROM `users` WHERE (CONCAT(mobile_code,mobile_number) LIKE '$mobile_code$mobile_number') AND user_type='$this->login_type'";
        $Array = $this->Database->select_qry_array($qry);
        if (!empty($Array)) {

            die(json_encode(array("code" => $this->error, "response" => array("status" => false, "message" => $this->lang->line("already_registered_mobile")))));
        }
        $qry = "SELECT * FROM `users` WHERE email LIKE '$email'  AND user_type='$this->login_type'";
        $Array = $this->Database->select_qry_array($qry);
        if (!empty($Array)) {

            die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $this->lang->line("already_register_this_email")))));
        }
        if (empty($isValidate)) {
            $result444['otp'] = mt_rand(1000, 9999);
            $message = "";
            $message = $message . "<p>Dear $full_name,</p>";
            $message = $message . "<p>You have one new otp code.</p>";
            $message = $message . "<p>OTP code: " . $result444['otp'] . "</p>";
            $subject = "New OTP Code";
            send_mail($email, $subject, $message);
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("otp_send_successfully")), 'result' => $result444)));
        }
//  return new RegExp('(?=.*[a-zA-Z])(?=.*\\d).{8,}').test(password);
        // $regex = '/^(?=.*[a-zA-Z])(?=.*\\d).{8,}$/';
        // if (!preg_match($regex,$json['password']))
        // {
        //     die(json_encode(array("code" => $this->error, "response" => array("status" => false, "message" => "Password conatin min 8 Characters,atleast one letter,atleast one number"))));
        // }
        $Inst['name'] = $full_name;
        $Inst['email'] = $email;
        $Inst['password'] = !empty($json['password']) ? md5($json['password']) : '';
        $Inst['mobile_code'] = $mobile_code;
        $Inst['mobile_number'] = $mobile_number;
        $Inst['device_id'] = !empty($json['device_id']) ? $json['device_id'] : '';
        $Inst['user_type'] = $this->login_type;
        $Inst['is_approved'] = 1;
        $Inst['lastupdate'] = $today;
        $Inst['timestamp'] = $today;


        $pdpic2 = empty($_FILES['profile_image']['error']) && !empty($_FILES['profile_image']) ? $_FILES['profile_image'] : '';
        $six_digit = mt_rand(100000, 999999);
        if (!empty($pdpic2)) {
            $extension = pathinfo($pdpic2['name'], PATHINFO_EXTENSION);
            if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png') {
                $FileName = $six_digit . '_' . $pdpic2['name'];
                $basedir = HOME_DIR . 'uploads/user_images/' . $six_digit . '_' . $pdpic2['name'];
                if (move_uploaded_file($pdpic2['tmp_name'], $basedir)) {

                    $Inst['image'] = $FileName;
                }
            }
        }

        $customerId = $this->Database->insert('users', $Inst);

        send_email_for_registration($Inst);
        $cArray = GetCustomerArrayById($customerId);
        $cArray['address_list'] = getUserAddressByuserId383($customerId);

        $cArray->loyalty_points = DecimalAmount(getloyalityTotalCreditPoint($customerId));

        $result['user_details'] = $cArray;


        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("successfully_registered")), 'result' => $result)));
    }

    public function login() {
        $json = file_get_contents('php://input');
        //send_mail('upendra@alwafaagroup.com', 'login', $json);
        $json = json_decode($json, TRUE);
        $today = date('Y-m-d H:i:s');
        $email = !empty($json['email']) ? $json['email'] : '';
        $password = !empty($json['password']) ? $json['password'] : '';
        $user_type = !empty($json['user_type']) ? $json['user_type'] : '';
        if (empty($user_type)) {
            $user_type = '3';
        }
        $password = md5($password);
        if (empty($email)) {
            die(json_encode(array("code" => $this->error, "response" => array("status" => false, "message" => $this->lang->line("Please_enter_a_valid_email")))));
        }
        if ($user_type == '2') {
            $qry = "SELECT * FROM `users` WHERE email LIKE '$email' AND password='$password'  AND user_type='2' AND status=1 AND is_approved=1 AND archive=0";
            $Array = $this->Database->select_qry_array($qry);
            if (count($Array) > 1 || empty($Array)) {
                $result['user_details'] = new stdClass();
                die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $this->lang->line("Invalid_username_and_pswrd")), 'result' => $result)));
            }
            $InsertArray = array(
                // 'device_id' => $json['device_id'],
                'lastupdate' => $today,
            );
            $Array = $Array[0];
            $customerId = $Array->id;
            $cond = array('id' => $customerId);
            $this->Database->update('users', $InsertArray, $cond);
            $cArray = GetCustomerArrayById($customerId);
            $cArray['address_list'] = getUserAddressByuserId383($customerId);

            $result['user_details'] = empty($cArray) ? new stdClass() : $cArray;

            updatedeveiceToken($customerId, $json['device_id']);
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("successfully_Login")), 'result' => $result)));

            die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $this->lang->line("Invalid_username_and_pswrd")))));
        } else if ($user_type == '4') {
            $qry2 = "SELECT * FROM `users` WHERE email LIKE '$email' AND password='$password'  AND user_type='4' "; // AND status=0 AND archive=0
            $Array2 = $this->Database->select_qry_array($qry2);
            if (count($Array2) > 1 || empty($Array2)) {
                $result['user_details'] = new stdClass();
                die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $this->lang->line("Invalid_username_and_pswrd")), 'result' => $result)));
            }
            $Array3 = $Array2[0];
            $InsertArray2 = array(
                // 'device_id' => $json['device_id'],
                'lastupdate' => $today,
            );
            if (!empty($Array3->status) || !empty($Array3->archive) || !empty($Array3->is_deleted)) {
                $result['user_details'] = new stdClass();
                die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $this->lang->line("username_and_pswrd_disabled")), 'result' => $result)));
            }
            $Id1 = $Array3->id;
            $cond = array('Id' => $Id1);
            $this->Database->update('users', $InsertArray2, $cond);
            $cArray = GetCustomerArrayById($Id1);
            $cArray['address_list'] = getUserAddressByuserId383($Id1);
            $result['user_details'] = empty($cArray) ? new stdClass() : $cArray;

            updatedeveiceToken($Id1, $json['device_id']);
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("successfully_Login")), 'result' => $result)));
        } else if ($user_type == '3') {
            $qry = "SELECT * FROM `users` WHERE email LIKE '$email' AND password='$password'  AND user_type='3'  AND archive=0";
            // send_mail('upendra@alwafaagroup.com', 'login-qry', $qry);
            $Array = $this->Database->select_qry_array($qry);
            if (count($Array) > 1 || empty($Array)) {
                $result['user_details'] = new stdClass();
                die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $this->lang->line("Invalid_username_and_pswrd")), 'result' => $result)));
            }
            $InsertArray = array(
                //  'device_id' => $json['device_id'],
                'lastupdate' => $today,
            );
            $Array = $Array[0];
            $customerId = $Array->id;
            $cond = array('id' => $customerId);
            $this->Database->update('users', $InsertArray, $cond);
            $cArray = GetCustomerArrayById($customerId);

            $cArray->loyalty_points = DecimalAmount(getloyalityTotalCreditPoint($customerId));


            $cArray['address_list'] = getUserAddressByuserId383($customerId);
            $result['user_details'] = empty($cArray) ? new stdClass() : $cArray;

            updatedeveiceToken($customerId, $json['device_id']);
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("successfully_Login")), 'result' => $result)));
        }
        $result['user_details'] = new stdClass();
        die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $this->lang->line("Invalid_username_and_pswrd")), 'result' => $result)));
    }

    public function user_details() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $customerId = !empty($json['user_id']) ? $json['user_id'] : '';
        $cArray = GetCustomerArrayById($customerId);
        $cArray['address_list'] = getUserAddressByuserId383($customerId);
        $cArray['loyalty_points'] = DecimalAmount(getloyalityTotalCreditPoint($customerId));
        $result['user_details'] = empty($cArray) ? new stdClass() : $cArray;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("successfully_Login")), 'result' => $result)));
    }

    public function logout() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $customerId = !empty($json['user_id']) ? $json['user_id'] : '';
        $device_token = !empty($json['device_token']) ? $json['device_token'] : '';
        removeDeviceId($customerId, $deviceId = $device_token);

//        $InsertArray = array(
//            'device_id' => '',
//            'lastupdate' => date('Y-m-d H:i:s'),
//        );
//        $cond = array('id' => $customerId);
//        $this->Database->update('users', $InsertArray, $cond);
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("Logout_successfully")))));
    }

    public function social_login() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        //  $json = $_POST;
        $today = date('Y-m-d H:i:s');
        $email = $json['email'] ? $json['email'] : '';
        $qry = "SELECT * FROM `users` WHERE (email LIKE '$email') AND user_type='3'";
        $Array = $this->Database->select_qry_array($qry);
        $is_new = false;
        if (empty($Array)) {
            $InsertArray = array(
                'name' => !empty($json['full_name']) ? $json['full_name'] : '',
                'email' => $email,
                'mobile_code' => '',
                'mobile_number' => '',
                'password' => '',
                'user_type' => 3,
                'login_type' => !empty($json['login_type']) ? $json['login_type'] : '', //1-fb,2-google
//                'device_id' => !empty($json['device_id']) ? $json['device_id'] : '',
                'is_approved' => 1,
                'social_id' => !empty($json['social_id']) ? $json['social_id'] : '',
                'image' => !empty($json['profile_image']) ? $json['profile_image'] : '',
                'lastupdate' => $today,
                'timestamp' => $today,
            );
            $is_new = true;
            $customerId = $this->Database->insert('users', $InsertArray);
        } else {
            $customerId = $Array[0]->id;

//            $updateArray = [];
//            $updateArray['lastupdate'] = date('Y-m-d H:i:s');
//            if (!empty($json['device_id'])) {
//                $updateArray['device_id'] = $json['device_id'];
//            }
//            $condrr = array('id' => $customerId);
//            $this->Database->update('users', $updateArray, $condrr);
        }

        updatedeveiceToken($customerId, $json['device_id']);

        $cArray = GetCustomerArrayById($customerId);
        $cArray['address_list'] = getUserAddressByuserId383($customerId);
        $result['customer'] = $cArray;
        $result['is_new'] = $is_new;
        die(json_encode(array("response" => array("status" => true, "message" => $this->lang->line("successfully_registered")), 'result' => $result)));
    }

    public function get_otp() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $today = date('Y-m-d H:i:s');
        $phone = !empty($json['mobile_number']) ? $json['mobile_number'] : '';
        $phonecode = !empty($json['mobile_code']) ? str_replace('+', '', $json['mobile_code']) : '';
        if (empty($phone)) {

            die(json_encode(array("code" => $this->error, "response" => array("status" => false, "message" => $this->lang->line("Please_enter_valid_mobile")))));
        }
        $isvalidate = !empty($json['isvalidate']) ? $json['isvalidate'] : '';
        $qry = "SELECT * FROM `users` WHERE (CONCAT(mobile_code,mobile_number) LIKE '$phonecode$phone') AND user_type='$this->login_type'";
        $Array = $this->Database->select_qry_array($qry);
        if (!empty($Array)) {

            die(json_encode(array("code" => $this->error, "response" => array("status" => false, "message" => $this->lang->line("already_registered_mobile")))));
        }
        $otp = rand(1000, 9999);
        $otp = '0000';
        $result = array('OTP' => $otp);
        // $messagqwe = "your verification code is " . $otp;
        // send_sms($phonecode . $phone, $messagqwe);
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("Please_verify_your")), 'result' => $result)));
    }

    public function forgot_password() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $customerId = !empty($json['user_id']) ? $json['user_id'] : '';
        $email_id = !empty($json['email_id']) ? $json['email_id'] : '';

        $mobile_code = !empty($json['mobile_code']) ? $json['mobile_code'] : '';
        $mobile_number = !empty($json['mobile_number']) ? $json['mobile_number'] : '';


        $user_type = !empty($json['user_type']) ? $json['user_type'] : '';
        $message = $this->lang->line("Invalid_email");
        if ($user_type == '3') {
            $customerId = !empty($json['customer_id']) ? $json['customer_id'] : '';
            $message = $this->lang->line("Invalid_mobile");
        }
        $cArray = GetCustomerArrayById($customerId);
        if (empty($cArray)) {
            //if ($user_type == '3') {
            if ($user_type == 'FFF') {
                $qry = "SELECT * FROM `users` WHERE CONCAT(mobile_code,mobile_number) LIKE '$mobile_code$mobile_number' AND user_type='$user_type'";
            } else {
                $qry = "SELECT * FROM `users` WHERE email LIKE '$email_id' AND user_type='$user_type'";
            }
            $rArray = $this->Database->select_qry_array($qry);
            $rArray = !empty($rArray[0]) ? $rArray[0] : '';
            if (empty($rArray)) {
                $result['OTP'] = 0;
                $result['user_id'] = '';
                die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $message), 'result' => $result)));
            }
            $result['OTP'] = mt_rand(1000, 9999);

            $result['user_id'] = $rArray->id;
            send_email_forgotpassord($rArray->name, $result['OTP'], $rArray->email);

            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("Please_check_your_mail_otpcode")), 'result' => $result)));
        } else {
            $Inst['password'] = md5($json['password']);
            $this->Database->update('users', $Inst, array('id' => $customerId));

            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("successfully_updated")), 'result' => new stdClass())));
        }
    }

    public function cart_count() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $sql = "SELECT * FROM add_to_cart WHERE user_id='$user_id'";
        $cart_data = $this->Database->select_qry_array($sql);

        $count = 0;
        $total = 0;
        foreach ($cart_data as $item) {

            $count += $item->quantity;
        }

        $result['cart_count'] = !empty($count) ? (string) $count : '0';
        $_REQUEST['returncartType'] = true;
        $cartList = $this->CartList();
        $pricearray = !empty($cartList['pricearray']) ? $cartList['pricearray'] : [];
        $result['cart_amount'] = $pricearray['totalprice'];
        die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $result)));
    }

    public function update_password() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $customer_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $old_password = !empty($json['old_password']) ? md5($json['old_password']) : '';
        $qry = "SELECT * FROM `users` WHERE id LIKE '$customer_id' AND password='$old_password' AND user_type='$this->login_type'";
        $Array = $this->Database->select_qry_array($qry);
        if (empty($Array)) {

            die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $this->lang->line("invalid_old_password")))));
        }
        if (empty($json['new_password'])) {

            die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $this->lang->line("invalid_new_password")))));
        }
        $Inst['password'] = md5($json['new_password']);
        $this->Database->update('users', $Inst, array('id' => $customer_id));
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("successfully_updated")), 'result' => [])));
    }

    public function update_profile() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);

        $customer_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $password = !empty($json['password']) ? $json['password'] : '';
        $image = !empty($customer->image) ? $customer->image : '';
        $Inst['name'] = !empty($json['fullName']) ? $json['fullName'] : '';

        if (!empty($password)) {
            $Inst['password'] = md5($password);
        }
        if (!empty($json['mobile_number'])) {
            $Inst['mobile_number'] = $json['mobile_number'];
        }
//        $pdpic2 = empty($_FILES['profile_image']['error']) && !empty($_FILES['profile_image']) ? $_FILES['profile_image'] : '';
//        $six_digit = mt_rand(100000, 999999);
//        if (!empty($pdpic2)) {
//            $extension = pathinfo($pdpic2['name'], PATHINFO_EXTENSION);
//            if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png') {
//
//                $FileName = $six_digit . '_' . $pdpic2['name'];
//
//                $basedir = HOME_DIR . 'uploads/user_images/' . $six_digit . '_' . $pdpic2['name'];
//                if (move_uploaded_file($pdpic2['tmp_name'], $basedir)) {
//                    $oldFile = HOME_DIR . "uploads/user_images/" . $image;
//                    if (is_file($oldFile)) {
//                        unlink($oldFile);
//                    }
//                    $Inst['image'] = $FileName;
//                }
//            } else {
//                die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => "Invalid file format"))));
//            }
//        }


        $cond = array('id' => $customer_id);
        $this->Database->update('users', $Inst, $cond);
        $cArray = GetCustomerArrayById($customer_id);
        $cArray['address_list'] = getUserAddressByuserId383($customer_id);
        $result['customer'] = $cArray;

        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("successfully_updated")), 'result' => $result)));
    }

    public function home_content() {
        //  $dat[0]['homelyfood_id'] = '1';
        //  $dat[0]['image_url'] = base_url('images/box1.jpg');
        //  $dat[1]['homelyfood_id'] = '2';
        //  $dat[1]['image_url'] = base_url('images/box2.jpg');
        //  $dat[2]['homelyfood_id'] = '3';
        //  $dat[2]['image_url'] = base_url('images/box3.jpg');
        // $basePath = base_url().'uploads/homeslider/';
        // $qry = "SELECT id as homelyfood_id,CONCAT('$basePath',home_image) as image_url FROM `featured_deals_image` WHERE type=2 AND archive=0 AND status=0 ORDER BY id";
        // $cArray = $this->Database->select_qry_array($qry);
        // $result['special_offers'] = $cArray;
        $cat = Get_AllStoreType();

        $basePath = base_url() . 'uploads/store_type/';
        $data = array();
        foreach ($cat as $category) {
            $data[] = array('id' => $category->id,
                'category' => $this->language == 'EN' ? $category->store_type : $category->store_type_ar,
                'image_url' => !empty($category->image) ? $basePath . $category->image : '',
                'description' => strip_tags($category->description));
        }
        $slider = GetHomeSlider();
        $slider_data = array();
        foreach ($slider as $slide) {
            if (!empty($slide->image)) {
                $img = base_url() . 'uploads/home_banner/' . $slide->image;
            } else {
                $img = base_url() . 'images/bnr1.jpg';
            }

            $slider_data[] = array('id' => $slide->id,
                'image_url' => $img,
            );
        }
        $result['category'] = $data;
        $result['home_banner'] = $slider_data;
        //  $result['homely_foods'] = $this->get_features(); // for enable the all restunt need to show here
        $result['homely_foods'] = [];
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
    }

    public function store_by_category() {

        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $category_id = !empty($json['category_id']) ? $json['category_id'] : '';
        $search_text = !empty($json['search_text']) ? trim($json['search_text']) : '';
        $cond = '';
        if (!empty($search_text)) {
            $cond = "AND restaurant_name LIKE '%" . $search_text . "%'";
        }

        $Qry = "SELECT restaurant_details.vendor_id,restaurant_details.restaurant_name,users.image,GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ', ')AS cuisine_name,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount,restaurant_details.opening_time,restaurant_details.closing_time,restaurant_details.busy_status,restaurant_details.party_order,restaurant_details.table_booking,restaurant_banner.image as banner_image 
        FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
        left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
        left join restaurant_banner ON restaurant_banner.vendor_id=restaurant_details.vendor_id
        WHERE users.user_type=2 AND store_type='$category_id' $cond AND users.archive=0 AND users.status=1 AND is_approved=1
        GROUP BY restaurant_details.vendor_id  
        ORDER BY users.position = 0, users.position";

        $Array = $this->Database->select_qry_array($Qry);
        $list = array();
        foreach ($Array as $dat) {

            if (!empty($dat->image)) {
                $img = base_url() . 'uploads/vendor_images/' . $dat->image;
            } else {
                $img = "";
            }
            if ($category_id == '1') {
                $cus = $dat->cuisine_name;
            } else {
                $cus = GetNameById($category_id, 'store_type', 'store_type');
            }
            $delivery_time = '';
            $qry = "SELECT count(id) as total_reviews,AVG(rating) as rating FROM user_feedback WHERE vendor_id=" . $dat->vendor_id;
            $Array = $this->Database->select_qry_array($qry);
            $rating = !empty($Array[0]->rating) ? number_format($Array[0]->rating, 1) : '0';

            $opening_time = date('H:i', strtotime($dat->opening_time));
            $closing_time = date('H:i', strtotime($dat->closing_time));
            $currentTime = date('H:i', time());

            if ($currentTime > $opening_time && $dat->busy_status == 1) {
                $isBusy = '1';
            } else {
                $isBusy = '0';
            }
            if (($closing_time < $opening_time && ($currentTime <= $opening_time && $currentTime >= $closing_time)) ||
                    ($closing_time > $opening_time && ($currentTime <= $opening_time || $currentTime >= $closing_time))) {
                $close = '1';
            } else {
                $close = '0';
            }
            $shop_opening_time = date('h:i a ', strtotime($dat->opening_time));
            $shop_closing_time = date('h:i a ', strtotime($dat->closing_time));
            if (!empty($shop_opening_time) && !empty($shop_closing_time)) {
                $wrk_hrs = $shop_opening_time . '-' . $shop_closing_time;
                $wrking_hrs = trim($wrk_hrs);
            } else {
                $wrking_hrs = '';
            }
            $homely_id = $dat->vendor_id;
            $Qry_Promo = "SELECT * FROM promo_code WHERE promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE() AND promo_code.user_type=1 AND promo_code.status=0 AND promo_code.archive=0 AND FIND_IN_SET($homely_id,promo_code.restaurant_id)";

            $sql = $this->Database->select_qry_array($Qry_Promo);
            if (!empty($sql)) {
                $rest_id = explode(',', $sql[0]->restaurant_id);

                if (in_array($homely_id, $rest_id)) {
                    foreach ($sql as $s) {
                        $offer_title = 'Super Saver - Get ' . $s->discount . '% off. use code ' . $s->promo_code;
                        $discount = $s->discount;
                    }
                }
            } else {
                $offer_title = "";
                $discount = "";
            }

            if (!empty($dat->banner_image)) {
                $banner_img = explode(',', $dat->banner_image);
                for ($i = 0; $i < count($banner_img); $i++) {
                    //  array_push($bannerimage,base_url().'uploads/banner_images/'.$banner_img[$i]);
                    $bannerimage[] = array('image_url' => base_url() . 'uploads/banner_images/' . $banner_img[$i]);
                }
            } else {
                $bannerimage = array(array('image_url' => base_url('images/poimg.jpg')));
            }
            $list[] = array(
                'homelyfood_id' => $dat->vendor_id,
                'homelyfood_name' => $dat->restaurant_name,
                'image_url' => $img,
                'cuisine' => $cus,
                'min_amount' => $dat->min_amount,
                'service_charge' => $dat->service_charge,
                'delivery_time' => $dat->delivery_time,
                'rating' => $rating,
                'reviews' => $Array[0]->total_reviews,
                'delivery' => isset($delivery_time) ? $delivery_time : '',
                'isClosed' => $close,
                'closed_message' => 'shop is closed at the moment',
                'isBusy' => $isBusy,
                'isParty' => $dat->party_order,
                'isTableBooking' => $dat->table_booking,
                'discount' => $discount,
                'promocode_title' => $offer_title,
                'working_hours' => $wrking_hrs,
                'banner_image' => $bannerimage
            );
        }
        $result['store_list'] = $list;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
    }

    public function get_features() {
        $json = file_get_contents('php://input');
        // send_mail('upendra@alwafaagroup.com', 'get_features', $json);
        $json = json_decode($json, TRUE);
        $location = !empty($json['location']) ? $json['location'] : '';
        $latitude = !empty($json['latitude']) ? $json['latitude'] : '';
        $longitude = !empty($json['longitude']) ? $json['longitude'] : '';
        $order_type = !empty($json['order_type']) ? $json['order_type'] : '';
        $sort_by = !empty($json['sort_id']) ? $json['sort_id'] : '';
        $cuisine_id = !empty($json['cuisine_id']) ? $json['cuisine_id'] : '';

        $category_id = !empty($json['category_id']) ? $json['category_id'] : '';


        $filter = !empty($json['filter']) ? $json['filter'] : '';
        $_REQUEST['order_type'] = $order_type;
        $_REQUEST['get_features'] = '1';
        $hArray = GetRestaurantByLocation($location, $latitude, $longitude, $sort_by, $cuisine_id, $filter);

        $result_data = [];
        $filter = [];
        $bannerimage = [];
        foreach ($hArray as $d) {
            // $banner = !empty($d->banner_image)?$d->banner_image:'';
            if (!empty($d->banner_image)) {
                $banner_img = explode(',', $d->banner_image);
                for ($i = 0; $i < count($banner_img); $i++) {
                    //  array_push($bannerimage,base_url().'uploads/banner_images/'.$banner_img[$i]);
                    $bannerimage[] = array('image_url' => base_url() . 'uploads/banner_images/' . $banner_img[$i]);
                }
            } else {
                $bannerimage = array(array('image_url' => base_url('images/ft2.jpg')));
            }
            if (!empty($d->image)) {
                $image = base_url() . 'uploads/vendor_images/' . $d->image;
            } else {
                $image = base_url(DEFAULT_LOGO_RESTAURANT);
            }
            if ($d->discount != '') {
                $offer_title = 'Super Saver - Get ' . $d->discount . '% off. use code ' . $d->promo_code;
            } else {
                $offer_title = '';
            }




            $discount = '';
            $homely_id = $d->id;
            $Qry_Promo = "SELECT * FROM promo_code WHERE promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE() AND promo_code.user_type=1 AND promo_code.status=0 AND promo_code.archive=0 AND FIND_IN_SET($homely_id,promo_code.restaurant_id)";

            $sql = $this->Database->select_qry_array($Qry_Promo);
            if (!empty($sql)) {
                $rest_id = explode(',', $sql[0]->restaurant_id);

                if (in_array($homely_id, $rest_id)) {
                    foreach ($sql as $s) {
                        $offer_title = 'Super Saver - Get ' . $s->discount . '% off. use code ' . $s->promo_code;
                        $discount = $s->discount;
                    }
                }
            } else {
                $offer_title = "";
                $discount = "";
            }






            $opening_time = date('H:i', strtotime($d->opening_time));
            $closing_time = date('H:i', strtotime($d->closing_time));
            $currentTime = date('H:i', time());
            if ($currentTime > $opening_time && $d->busy_status == 1) {
                $isBusy = '1';
            } else {
                $isBusy = '0';
            }
            $start_time = date('H:i', strtotime($d->delivery_time_start));
            $end_time = date('H:i', strtotime($d->delivery_time_ends));
            $currentTime = date('H:i', time());

            if ($start_time != '00:00:00' && $end_time != '00:00:00') {
                if ($currentTime > $start_time && $currentTime < $end_time) {
                    $delivery = 1;
                } elseif ($currentTime < $start_time && $currentTime < $end_time) {
                    $delivery = 1;
                } else {
                    $delivery = 0;
                }
            }
            if ($d->isDelivery == 1) {
                if ($delivery == 1) {
                    $delivery_time = "Same day";
                } else {
                    $delivery_time = "Next day";
                }
            } else {
                $delivery_time = '';
            }
            $qry = "SELECT count(id) as total_reviews,AVG(rating) as rating FROM user_feedback WHERE vendor_id=" . $d->vendor_id;
            $Array = $this->Database->select_qry_array($qry);
            $rating = !empty($Array[0]->rating) ? number_format($Array[0]->rating, 1) : '0';
            if (($closing_time < $opening_time && ($currentTime <= $opening_time && $currentTime >= $closing_time)) ||
                    ($closing_time > $opening_time && ($currentTime <= $opening_time || $currentTime >= $closing_time))) {
                $close = '1';
            } else {
                $close = '0';
            }
            if (empty($cuisine_id)) {
                $result_data[] = array(
                    'homelyfood_id' => $d->vendor_id,
                    'homelyfood_name' => $d->restaurant_name,
                    'cuisine' => $d->cuisine_name,
                    'service_charge' => $d->service_charge,
                    'min_amount' => $d->min_amount,
                    'delivery_time' => $d->delivery_time,
                    'rating' => $rating,
                    'reviews' => $Array[0]->total_reviews,
                    'delivery' => $delivery_time,
                    'image_url' => $image,
                    'isClosed' => $close,
                    'closed_message' => 'shop is closed at the moment',
                    'isBusy' => $isBusy,
                    'isParty' => $d->party_order,
                    'discount' => $discount,
                    'promocode_title' => $offer_title,
                    'banner_image' => $bannerimage
                );
            } else {
                $ConditionArray = array('vendor_id' => $d->vendor_id);
                $Qry2 = "SELECT IFNULL(GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ','), '') as cuisine_name FROM `restaurant_cuisine_type` LEFT JOIN `cuisine` ON cuisine.cuisine_id=restaurant_cuisine_type.cuisine_id  WHERE vendor_id=:vendor_id";
                $Array2 = $this->Database->select_qry_array($Qry2, $ConditionArray);
                $result_data[] = array(
                    'homelyfood_id' => $d->vendor_id,
                    'homelyfood_name' => $d->restaurant_name,
                    'cuisine' => $Array2[0]->cuisine_name,
                    'service_charge' => $d->service_charge,
                    'min_amount' => $d->min_amount,
                    'delivery_time' => $d->delivery_time,
                    'rating' => $rating,
                    'reviews' => $Array[0]->total_reviews,
                    'delivery' => $delivery_time,
                    'image_url' => $image,
                    'isClosed' => $close,
                    'closed_message' => 'shop is closed at the moment',
                    'isBusy' => $isBusy,
                    'isParty' => $d->party_order,
                    'discount' => $discount,
                    'promocode_title' => $offer_title,
                    'banner_image' => $bannerimage
                );
            }
        }
        return $result_data;

        $qry1 = "SELECT * FROM mobile_filter WHERE archive=0 AND status=0";
        $Array1 = $this->Database->select_qry_array($qry1);
        $result['total_count'] = (string) count($hArray);



        $result['homely_foods'] = $result_data;
        $result['sorting'] = array(
            array(
                "id" => "1",
                "name" => "Recommended"
            ),
            array(
                "id" => "2",
                "name" => "Newest"
            ),
            array(
                "id" => "3",
                "name" => "A to Z"
            ),
            array(
                "id" => "4",
                "name" => "Min.Order Amount"
            ),
                // array(
                //     "id" => "4",
                //     "name" => "Fastest Delivery"
                // ),
                // array(
                //     "id" => "5",
                //     "name" => "Rating"
                // )
        );
        $result['cuisine'] = GetCuisineName();

        foreach ($Array1 as $a) {
            $filter[] = array(
                'id' => $a->id,
                'name' => $a->name,
                'image_url' => base_url() . 'uploads/filter/' . $a->image
            );
        }
        $result['filter'] = $filter;
        if (empty($hArray)) {
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
        } else {
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
        }
    }

    public function get_homelyfood() {
        $json = file_get_contents('php://input');
        // send_mail('upendra@alwafaagroup.com', 'Homelyfoodddd', $json); 
        $json = json_decode($json, TRUE);
        $location = !empty($json['location']) ? $json['location'] : '';
        $latitude = !empty($json['latitude']) ? $json['latitude'] : '';
        $longitude = !empty($json['longitude']) ? $json['longitude'] : '';
        $order_type = !empty($json['order_type']) ? $json['order_type'] : '';
        $sort_by = !empty($json['sort_id']) ? $json['sort_id'] : '';
        $cuisine_id = !empty($json['cuisine_id']) ? $json['cuisine_id'] : '';

        $category_id = !empty($json['category_id']) ? $json['category_id'] : '';
        $store_type = $category_id;


        $store56 = GetstoretypeBy($store_type);



        $noresuleMsg = $this->lang->line("no_results_found");
        $shopisclose = 'shop is closed at the moment';
        if (!empty($store56)) {
            $noresuleMsg = "No $store56->store_type available  in your area";
            if ($this->language == 'UR') {
                $noresuleMsg = $store56->if_nodatafound;
                $shopisclose = $store56->if_nodatafound;
            }
        }

        $filter = !empty($json['filter']) ? $json['filter'] : '';
        $_REQUEST['order_type'] = $order_type;
        $hArray = GetRestaurantByLocation($location, $latitude, $longitude, $sort_by, $cuisine_id, $filter);

        $result_data = [];
        $filter = [];

        foreach ($hArray as $d) {
            // $banner = !empty($d->banner_image)?$d->banner_image:'';

            $bannerimage = [];
            $qry = "SELECT * FROM `restaurant_banner` WHERE vendor_id='$d->id'";
            $image444 = $this->Database->select_qry_array($qry);

            if (!empty($image444)) {
                for ($i = 0; $i < count($image444); $i++) {
                    $d4555 = $image444[$i];
                    $filenff = 'uploads/banner_images/' . $d4555->image;
                    if (is_file(HOME_DIR . $filenff)) {
                        $bannerimage[] = array('image_url' => base_url($filenff));
                    }
                }
            } if (empty($bannerimage)) {
                $bannerimage = array(array('image_url' => base_url('images/ft2.jpg')));
            }
            if (!empty($d->image)) {
                $image = base_url() . 'uploads/vendor_images/' . $d->image;
            } else {
                $image = base_url(DEFAULT_LOGO_RESTAURANT);
            }
            if ($d->discount != '') {
                $offer_title = 'Super Saver - Get ' . $d->discount . '% off. use code ' . $d->promo_code;
            } else {
                $offer_title = '';
            }




            $discount = '';
            $homely_id = $d->id;
            $Qry_Promo = "SELECT * FROM promo_code WHERE promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE() AND promo_code.user_type=1 AND promo_code.status=0 AND promo_code.archive=0 AND FIND_IN_SET($homely_id,promo_code.restaurant_id)";

            $sql = $this->Database->select_qry_array($Qry_Promo);
            if (!empty($sql)) {
                $rest_id = explode(',', $sql[0]->restaurant_id);

                if (in_array($homely_id, $rest_id)) {
                    foreach ($sql as $s) {
                        $offer_title = 'Super Saver - Get ' . $s->discount . '% off. use code ' . $s->promo_code;
                        $discount = $s->discount;
                    }
                }
            } else {
                $offer_title = "";
                $discount = "";
            }

            if ($discount == '100') {
                $discount = '';
            }




            $opening_time = date('H:i', strtotime($d->opening_time));
            $closing_time = date('H:i', strtotime($d->closing_time));
            $currentTime = date('H:i', time());

            $isBusy = '0';
            if ($d->busy_status == 1) {
                $isBusy = '1';
            }

            $closed = "0";
            if ($store_type == '1') {
                $opening_time = date('H:i', strtotime($d->opening_time));
                $closing_time = date('H:i', strtotime($d->closing_time));
                $currentTime = date('H:i', time());

                if (empty($order_type)) {
                    $opening_time = date('H:i', strtotime($d->delivery_hours_st));
                    $closing_time = date('H:i', strtotime($d->delivery_hours_et));
                }
                if ($currentTime < $opening_time) {
                    $closed = "1";
                } else {
                    if ($currentTime > $closing_time) {
                        if (date('A', strtotime($closing_time)) == 'AM' && date('A') == 'PM') {
                            
                        } else {
                            $closed = "1";
                        }
                    }
                }
            }


            if ($d->isDelivery == 1) {
                if ($delivery == 1) {
                    $delivery_time = "Same day";
                } else {
                    $delivery_time = "Next day";
                }
            } else {
                $delivery_time = '';
            }
            $qry = "SELECT count(id) as total_reviews,AVG(rating) as rating FROM user_feedback WHERE vendor_id='$d->vendor_id' AND archive=0";
            $Array = $this->Database->select_qry_array($qry);
            $rating = !empty($Array[0]->rating) ? number_format($Array[0]->rating, 1) : '0';



            if ($this->language == 'UR' && !empty($d->restaurant_name_ar)) {
                $d->restaurant_name = $d->restaurant_name_ar;
            }

            if (empty($cuisine_id)) {
                $gentest63 = array(
                    'homelyfood_id' => $d->vendor_id,
                    'homelyfood_name' => $d->restaurant_name,
                    'cuisine' => !empty($d->cuisine_name) ? $d->cuisine_name : '',
                    'service_charge' => $d->service_charge,
                    'min_amount' => $d->min_amount,
                    'delivery_time' => $d->delivery_time,
                    'address' => !empty($d->area) ? $d->area : '',
                    'rating' => $rating,
                    'reviews' => $Array[0]->total_reviews,
                    'delivery' => $delivery_time,
                    'image_url' => $image,
                    'isClosed' => $closed,
                    'closed_message' => $shopisclose,
                    'isBusy' => $isBusy,
                    'isParty' => $d->party_order,
                    'discount' => $discount,
                    'promocode_title' => $offer_title,
                    'banner_image' => $bannerimage,
                    'description' => !empty($d->about) ? strip_tags($d->about) : '',
                );
            } else {
                $ConditionArray = array('vendor_id' => $d->vendor_id);
                $Qry2 = "SELECT IFNULL(GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ','), '') as cuisine_name FROM `restaurant_cuisine_type` LEFT JOIN `cuisine` ON cuisine.cuisine_id=restaurant_cuisine_type.cuisine_id  WHERE vendor_id=:vendor_id";
                $Array2 = $this->Database->select_qry_array($Qry2, $ConditionArray);
                $gentest63 = array(
                    'homelyfood_id' => $d->vendor_id,
                    'homelyfood_name' => $d->restaurant_name,
                    'cuisine' => !empty($Array2[0]->cuisine_name) ? $Array2[0]->cuisine_name : '',
                    'service_charge' => $d->service_charge,
                    'min_amount' => $d->min_amount,
                    'delivery_time' => $d->delivery_time,
                    'rating' => $rating,
                    'reviews' => $Array[0]->total_reviews,
                    'delivery' => $delivery_time,
                    'image_url' => $image,
                    'isClosed' => $closed,
                    'closed_message' => $shopisclose,
                    'address' => !empty($d->area) ? $d->area : '',
                    'isBusy' => $isBusy,
                    'isParty' => $d->party_order,
                    'discount' => $discount,
                    'promocode_title' => $offer_title,
                    'banner_image' => $bannerimage,
                    'description' => !empty($d->about) ? strip_tags($d->about) : '',
                );
            }

            $gentest63['working_hrs'] = '';
            $gentest63['delivery_hrs'] = '';
            if ($d->opening_time != '00:00:00') {
                $gentest63['working_hrs'] = date('h:i A', strtotime($d->opening_time)) . ' - ' . date('h:i A', strtotime($d->closing_time));
            }
            if ($d->delivery_hours_et != '00:00:00') {
                $gentest63['delivery_hrs'] = date('h:i A', strtotime($d->delivery_hours_st)) . '-' . date('h:i A', strtotime($d->delivery_hours_et));
            }


            $result_data[] = $gentest63;
        }
        $qry1 = "SELECT * FROM mobile_filter WHERE archive=0 AND status=0";
        $Array1 = $this->Database->select_qry_array($qry1);
        $result['total_count'] = (string) count($hArray);
        $result['homely_foods'] = $result_data;
        $result['sorting'] = array(
            array(
                "id" => "1",
                "name" => ($this->language == 'UR' ? 'تجویز کردہ' : "Recommended")
            ),
            array(
                "id" => "2",
                "name" => ($this->language == 'UR' ? 'تازہ ترین' : "Newest")
            ),
//            array(
//                "id" => "3",
//                "name" => "A to Z"
//            ),
//            array(
//                "id" => "4",
//                "name" => "Min.Order Amount"
//            ),
                // array(
                //     "id" => "4",
                //     "name" => "Fastest Delivery"
                // ),
                // array(
                //     "id" => "5",
                //     "name" => "Rating"
                // )
        );
        $result['cuisine'] = GetCuisineName();
        if ($category_id != '1') {
            $result['cuisine'] = [];
        }
        foreach ($Array1 as $a) {
            $filter[] = array(
                'id' => $a->id,
                'name' => $a->name,
                'image_url' => base_url() . 'uploads/filter/' . $a->image
            );
        }
        $result['filter'] = $filter;
        if (empty($hArray)) {
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $noresuleMsg), 'result' => $result)));
        } else {
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
        }
    }

    public function feedback_list() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $vendor_id = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $qry = "SELECT UF.*,U.name AS user_name FROM `user_feedback` UF LEFT JOIN users U ON U.id=UF.user_id WHERE UF.vendor_id='$vendor_id' AND UF.status='0'";
        $cuisine_data = $this->Database->select_qry_array($qry);
        $result['feedback'] = $cuisine_data;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
    }

    public function common_search() {
        $json = file_get_contents('php://input');
        //  send_mail('upendra@alwafaagroup.com', 'AlfaBee - Method-' . 'common_search', $json);
        $json = json_decode($json, TRUE);
        $search_text = !empty($json['search_text']) ? $json['search_text'] : '';
        $location = !empty($json['location']) ? $json['location'] : '';
        $latitude = !empty($json['latitude']) ? $json['latitude'] : '';
        $longitude = !empty($json['longitude']) ? $json['longitude'] : '';
        $hArray = searchRestaurant($search_text, $location, $latitude, $longitude);

        if (!empty($hArray)) {
            foreach ($hArray as $d) {
                if (!empty($d->image)) {
                    $image = base_url() . 'uploads/vendor_images/' . $d->image;
                } else {
                    $image = base_url(DEFAULT_LOGO_RESTAURANT);
                }

                $Qry1 = "SELECT * FROM `restaurant_banner` WHERE vendor_id='$d->vendor_id'";
                $ArrayBanner = $this->Database->select_qry_array($Qry1);
                if (!empty($ArrayBanner)) {
                    for ($i = 0; $i < count($ArrayBanner); $i++) {
                        $dRTTTT = $ArrayBanner[$i];
                        $filenff = 'uploads/banner_images/' . $dRTTTT->image;
                        if (is_file(HOME_DIR . $filenff)) {
                            $bannerimage[] = array('image_url' => base_url($filenff));
                        }
                    }
                }
                if (empty($bannerimage)) {
                    $bannerimage = array(array('image_url' => base_url('images/ft2.jpg')));
                }

                $opening_time = date('H:i', strtotime($d->opening_time));
                $closing_time = date('H:i', strtotime($d->closing_time));
                $currentTime = date('H:i', time());
                if ($currentTime > $opening_time && $d->busy_status == 1) {
                    $isBusy = '1';
                } else {
                    $isBusy = '0';
                }
                $qry = "SELECT count(id) as total_reviews,AVG(rating) as rating FROM user_feedback WHERE vendor_id='$d->vendor_id'";

                $Array = $this->Database->select_qry_array($qry);
                $rating = !empty($Array[0]->rating) ? number_format($Array[0]->rating, 1) : '0';


                if ($this->language == 'UR' && !empty($d->restaurant_name_ar)) {
                    $d->restaurant_name = $d->restaurant_name_ar;
                }

                $rest463633 = array(
                    'homelyfood_id' => $d->vendor_id,
                    'homelyfood_name' => $d->restaurant_name,
                    'cuisine' => $d->cuisine_name,
                    'service_charge' => $d->service_charge,
                    'min_amount' => $d->min_amount,
                    'delivery_time' => $d->delivery_time,
                    'address' => !empty($d->area) ? $d->area : '',
                    'rating' => $rating,
                    'reviews' => $Array[0]->total_reviews,
                    'delivery' => '',
                    'image_url' => $image,
                    'isClosed' => $currentTime < $opening_time ? '1' : '0',
                    'closed_message' => 'shop is closed at the moment',
                    'isBusy' => $isBusy,
                    'isParty' => $d->party_order,
                    'discount' => '',
                    'promocode_title' => '',
                    'banner_image' => $bannerimage,
                    'description' => !empty($d->about) ? strip_tags($d->about) : '',
                );

                $rest463633['working_hrs'] = '';
                $rest463633['delivery_hrs'] = '';
                if ($d->opening_time != '00:00:00') {
                    $rest463633['working_hrs'] = date('h:i A', strtotime($d->opening_time)) . ' - ' . date('h:i A', strtotime($d->closing_time));
                }
                if ($d->delivery_hours_et != '00:00:00') {
                    $rest463633['delivery_hrs'] = date('h:i A', strtotime($d->delivery_hours_st)) . '-' . date('h:i A', strtotime($d->delivery_hours_et));
                }


                $result_data[] = $rest463633;
            }
        } else {
            $result_data = [];
        }
        $result['total_count'] = (string) count($hArray);
        $result['homely_foods'] = $result_data;
        if (empty($hArray)) {
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
        } else {
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
        }
    }

    public function homelyfood_details() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $homelyfood_id = !empty($json['homelyfood_id']) ? $json['homelyfood_id'] : '';
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $data = getRestaurantDetailsAPI($homelyfood_id);


        $user637 = GetUserDetails($homelyfood_id);
        $user637 = !empty($user637) ? $user637[0] : '';
        $todays = date('Y-m-d');
        if (!empty($data->banner_image)) {
            $banner_img = explode(',', $data->banner_image);
            for ($i = 0; $i < count($banner_img); $i++) {
                //  array_push($bannerimage,base_url().'uploads/banner_images/'.$banner_img[$i]);
                $bannerimage[] = array('image_url' => base_url() . 'uploads/banner_images/' . $banner_img[$i]);
            }
        } else {
            $bannerimage = array(array('image_url' => base_url('images/ft2.jpg')));
        }
        if (!empty($data->image)) {
            $image = base_url() . 'uploads/vendor_images/' . $data->image;
        } else {
            $image = base_url(DEFAULT_LOGO_RESTAURANT);
        }
        if (!empty($data->discount)) {
            $offer_title = 'Super Saver - Get ' . $data->discount . '% off. use code ' . $data->promo_code;
        } else {
            $offer_title = '';
        }

        $opening_time = date('H:i', strtotime($data->opening_time));
        $closing_time = date('H:i', strtotime($data->closing_time));
        $currentTime = date('H:i', time());
        if ($currentTime > $opening_time && $data->busy_status == 1) {
            $isBusy = '1';
        } else {
            $isBusy = '0';
        }
        $qry = "SELECT count(id) as total_reviews,AVG(rating) as rating FROM user_feedback WHERE vendor_id='$data->vendor_id' AND archive=0";
        $Array = $this->Database->select_qry_array($qry);
        $rating = !empty($Array[0]->rating) ? number_format($Array[0]->rating, 1) : '0';
        $rest_details = array(
            'homelyfood_id' => $data->vendor_id,
            'homelyfood_name' => $data->restaurant_name,
            'cuisine' => $data->cuisine_name,
            'service_charge' => $data->service_charge,
            'min_amount' => $data->min_amount,
            'delivery_time' => $data->delivery_time,
            'rating' => $rating,
            'reviews' => $Array[0]->total_reviews,
            'image_url' => $image,
            'isClosed' => $currentTime < $opening_time ? '1' : '0',
            'closed_message' => 'shop is closed at the moment',
            'isBusy' => $isBusy,
            'isParty' => $data->party_order,
            'promocode_title' => $offer_title,
            'banner_image' => $bannerimage,
            'address' => !empty($user637->area) ? $user637->area : '',
            'description' => !empty($data->about) ? strip_tags($data->about) : '',
        );
        $rest_details['working_hrs'] = '';
        $rest_details['delivery_hrs'] = '';
        if ($data->opening_time != '00:00:00') {
            $rest_details['working_hrs'] = date('h:i A', strtotime($data->opening_time)) . ' - ' . date('h:i A', strtotime($data->closing_time));
        }
        if ($data->delivery_hours_et != '00:00:00') {
            $rest_details['delivery_hrs'] = date('h:i A', strtotime($data->delivery_hours_st)) . '-' . date('h:i A', strtotime($data->delivery_hours_et));
        }
        $categories = GetAllCategories($homelyfood_id);

        $return = [];
        for ($i = 0; $i < count($categories); $i++) {
            $dft = $categories[$i];
            if ($this->language == 'UR' && !empty($dft->category_name_ar)) {
                $dft->category_name = $dft->category_name_ar;
            }



            // $array_sub = GetMenuByCatid($dft->category_id,$homelyfood_id,$user_id);
            $array_sub = listMenuByRestaurant($homelyfood_id, $dft->category_id, $user_id);
            if (!empty($array_sub)) {
                $return[$i] = array(
                    'cat_id' => $dft->category_id,
                    'category_name' => $dft->category_name,
                );
                for ($j = 0; $j < count($array_sub); $j++) {

//                $admin_data = "SELECT discount,offer_addedby,menu_id FROM `promo_code` WHERE  `offer_addedby` ='$homelyfood_id' AND status=0";
//                $qry = $this->Database->select_qry_array($admin_data);
//                if (!empty($qry)) {
//                    $offer_menu = explode(',', $qry[0]->menu_id);
//                    if (in_array($array_sub[$j]->id, $offer_menu)) {
//                        $discount = $qry[0]->discount;
//                    } else {
//                        $discount = '0';
//                    }
//                } else {
//                    $discount = '0';
//                }



                    $array_sub1 = GetMenuSize($array_sub[$j]->id);
                    $array_sub2 = GetMenuAddon($array_sub[$j]->id);
                    $array_sub3 = GetMenuTopping($array_sub[$j]->id);
                    $array_sub4 = GetMenuDrink($array_sub[$j]->id);
                    $array_sub5 = GetMenuDip($array_sub[$j]->id);
                    $array_sub6 = GetMenuSide($array_sub[$j]->id);

                    $is_cart = false;
                    $cartQuantity = '0';
                    if (!empty($user_id)) {
                        $rttir8 = "SELECT cart_id,quantity FROM `add_to_cart` WHERE menu_id='" . $array_sub[$j]->id . "' AND user_id='$user_id'";

                        $cartt5444 = $this->Database->select_qry_array($rttir8);
                        if (!empty($cartt5444)) {
                            $is_cart = true;
                            $cartQuantity = !empty($cartt5444[0]->quantity) ? $cartt5444[0]->quantity : '0';
                        }
                    }

//                $offerTitle = '';
//                $qry444 = "SELECT * FROM `promo_code` WHERE '$todays' BETWEEN date_from AND date_to AND FIND_IN_SET('" . $array_sub[$j]->id . "',menu_id) AND archive=0 and status=0";
//                $offteredd = $this->Database->select_qry_array($qry444);
//
//                if (!empty($offteredd)) {
//                    $offteredd = $offteredd[0];
//                    $offerTitle = $array_sub[$j]->price;
//                    $calculatedPrice = $array_sub[$j]->price * $offteredd->discount / 100;
//                    $calculatedPrice = $offerTitle - $calculatedPrice;
//                    $array_sub[$j]->price = $calculatedPrice;
//                }

                    $prc = getMenudiscountArray($menuId = $array_sub[$j]->id, $array_sub[$j]->price, $homelyfood_id);

                    if ($this->language == 'UR' && !empty($array_sub[$j]->menu_name_ar)) {
                        $array_sub[$j]->menu_name = $array_sub[$j]->menu_name_ar;
                    }
                    $yeyef = array(
                        'menu_id' => $array_sub[$j]->id,
                        'menu_name' => $array_sub[$j]->menu_name,
                        'description' => str_replace("&nbsp;", "", strip_tags($array_sub[$j]->description)),
                        'price' => DecimalAmount($prc['final_price']),
                        'discount_percentage' => !empty($prc['total_discount']) ? DecimalAmount($prc['total_discount']) : '',
                        'image_url' => !empty($array_sub[$j]->image) ? base_url() . "uploads/menu/" . $array_sub[$j]->image : base_url() . DEFAULT_LOGO_MENU,
                        'is_cart' => $is_cart,
                        'special_price' => !empty($prc['cross_price']) ? DecimalAmount($prc['cross_price']) : '',
                        'cart_quantity' => $cartQuantity,
                        'is_customizable' => !empty($array_sub1) ? true : false,
                        'is_stock' => !empty($array_sub[$j]->stock) ? true : false,
                    );
                    if ($data->store_type == '1') {
                        $yeyef['is_stock'] = true;
                    }
                    $return[$i]['menu_list'][$j] = $yeyef;
                    if (!empty($array_sub1)) {
                        for ($k = 0; $k < count($array_sub1); $k++) {
                            $Sql = "SELECT *  FROM `promo_code` WHERE `offer_addedby` = '$homelyfood_id' AND '" . date('Y-m-d') . "' BETWEEN date_from AND date_to AND status=0 AND archive=0 AND FIND_IN_SET('" . $array_sub[$j]->id . "',menu_id)";
                            $offer73645 = $this->Database->select_qry_array($Sql);
                            $offer73645 = !empty($offer73645) ? $offer73645[0] : '';
                            $isdcoe8 = '';
                            if (!empty($offer73645)) {
                                $isdcoe8 = $array_sub1[$k]->menu_price;
                                if (empty($offer73645->discount_type)) {
                                    $disue38 = $array_sub1[$k]->menu_price * $offer73645->discount / 100;
                                    $array_sub1[$k]->menu_price = $array_sub1[$k]->menu_price - $disue38;
                                } else {
                                    $array_sub1[$k]->menu_price = $array_sub1[$k]->menu_price - $offer73645->discount;
                                }
                            }
                            $return[$i]['menu_list'][$j]['size'][$k] = array(
                                'size_id' => $array_sub1[$k]->menu_size_id,
                                'size_name' => $array_sub1[$k]->menu_size,
                                'size_price' => DecimalAmount($array_sub1[$k]->menu_price),
                                'cross_price' => !empty($isdcoe8) ? DecimalAmount($isdcoe8) : '',
                            );
                        }
                    } else {
                        $return[$i]['menu_list'][$j]['size'] = [];
                    }
                    if (!empty($array_sub2)) {
                        for ($a = 0; $a < count($array_sub2); $a++) {


                            $return[$i]['menu_list'][$j]['addons'][$a] = array(
                                'addon_id' => $array_sub2[$a]->menu_addon_id,
                                'addon_name' => $array_sub2[$a]->add_on,
                                'addon_price' => $array_sub2[$a]->addon_price,
                            );
                        }
                    } else {
                        $return[$i]['menu_list'][$j]['addons'] = [];
                    }
                    if (!empty($array_sub3)) {
                        for ($b = 0; $b < count($array_sub3); $b++) {
                            $return[$i]['menu_list'][$j]['topping'][$b] = array(
                                'topping_id' => $array_sub3[$b]->menu_topping_id,
                                'topping_name' => $array_sub3[$b]->topping,
                                'topping_price' => $array_sub3[$b]->topping_price,
                            );
                        }
                    } else {
                        $return[$i]['menu_list'][$j]['topping'] = [];
                    }

                    if (!empty($array_sub4)) {
                        for ($c = 0; $c < count($array_sub4); $c++) {
                            $return[$i]['menu_list'][$j]['drink'][$c] = array(
                                'drink_id' => $array_sub4[$c]->menu_drink_id,
                                'drink_name' => $array_sub4[$c]->drink_name,
                                'drink_price' => $array_sub4[$c]->drink_price,
                            );
                        }
                    } else {
                        $return[$i]['menu_list'][$j]['drink'] = [];
                    }

                    if (!empty($array_sub5)) {
                        for ($d = 0; $d < count($array_sub5); $d++) {
                            $return[$i]['menu_list'][$j]['dip'][$d] = array(
                                'dip_id' => $array_sub5[$d]->menu_dip_id,
                                'dip_name' => $array_sub5[$d]->dips,
                                'dip_price' => $array_sub5[$d]->dip_price,
                            );
                        }
                    } else {
                        $return[$i]['menu_list'][$j]['dip'] = [];
                    }

                    if (!empty($array_sub6)) {
                        for ($e = 0; $e < count($array_sub6); $e++) {
                            $return[$i]['menu_list'][$j]['side'][$e] = array(
                                'side_id' => $array_sub6[$e]->menu_side_id,
                                'side_name' => $array_sub6[$e]->side_dish,
                                'side_price' => $array_sub6[$e]->side_dish_price,
                            );
                        }
                    } else {
                        $return[$i]['menu_list'][$j]['side'] = [];
                    }
                }
            }
        }

        $return = array_values($return);

        $categories1 = GetRestaurantPartyCategory($homelyfood_id);

        $return1 = [];
        for ($n = 0; $n < count($categories1); $n++) {
            $dft = $categories1[$n];
            $return1[$n] = array(
                'cat_id' => $dft->category_id,
                'category_name' => $dft->category_name,
            );

            $array_sub1 = listPartyMenuByRestaurant($homelyfood_id, $dft->category_id, $user_id);

            for ($k = 0; $k < count($array_sub1); $k++) {
                $admin_data1 = "SELECT discount,offer_addedby,menu_id FROM `promo_code` WHERE  `offer_addedby` ='$homelyfood_id'";
                $qry1 = $this->Database->select_qry_array($admin_data1);
                if (!empty($qry1)) {
                    $offer_menu1 = explode(',', $qry1[0]->menu_id);
                    if (in_array($array_sub1[$k]->id, $offer_menu1)) {
                        $discount1 = $qry[0]->discount;
                    } else {
                        $discount1 = '0';
                    }
                } else {
                    $discount1 = '0';
                }

                if (!empty($array_sub1[$k]->cart_id)) {
                    $is_cart = true;
                } else {
                    $is_cart = false;
                }
                $return1[$n]['menu_list'][$k] = array(
                    'menu_id' => $array_sub1[$k]->id,
                    'menu_name' => $array_sub1[$k]->menu_name,
                    'description' => strip_tags($array_sub1[$k]->description),
                    'price' => DecimalAmount($array_sub1[$k]->price),
                    'discount_percentage' => !empty($discount) ? DecimalAmount($discount) : '',
                    'image_url' => !empty($array_sub1[$k]->image) ? base_url() . "uploads/party_menu/" . $array_sub1[$k]->image : base_url() . DEFAULT_LOGO_MENU,
                    'is_cart' => $is_cart
                );
            }
        }


        $result['homelyfood_details'] = $rest_details;
        //$result['party_order'] = $return1;
        $result['party_order'] = [];
        $result['category'] = $return;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
    }

    public function search_menu() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $homelyfood_id = !empty($json['homelyfood_id']) ? $json['homelyfood_id'] : '';
        $search_text = !empty($json['search_text']) ? $json['search_text'] : '';
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $_REQUEST = $json;

        if (empty($search_text)) {
            $result['menu_list'] = [];
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
        }

        $data = getRestaurantDetailsAPI($homelyfood_id);

        if (!empty($data->banner_image)) {
            $banner_img = explode(',', $data->banner_image);
            for ($i = 0; $i < count($banner_img); $i++) {
                //  array_push($bannerimage,base_url().'uploads/banner_images/'.$banner_img[$i]);
                $bannerimage[] = array('image_url' => base_url() . 'uploads/banner_images/' . $banner_img[$i]);
            }
        } else {
            $bannerimage = array(array('image_url' => base_url('images/ft2.jpg')));
        }
        if (!empty($data->image)) {
            $image = base_url() . 'uploads/vendor_images/' . $data->image;
        } else {
            $image = base_url(DEFAULT_LOGO_RESTAURANT);
        }
        if (!empty($data->discount)) {
            $offer_title = 'Super Saver - Get ' . $data->discount . '% off. use code ' . $data->promo_code;
        } else {
            $offer_title = '';
        }

        $opening_time = date('H:i', strtotime($data->opening_time));
        $closing_time = date('H:i', strtotime($data->closing_time));
        $currentTime = date('H:i', time());
        if ($currentTime > $opening_time && $data->busy_status == 1) {
            $isBusy = '1';
        } else {
            $isBusy = '0';
        }
        $qry = "SELECT count(id) as total_reviews,AVG(rating) as rating FROM user_feedback WHERE vendor_id=" . $data->vendor_id;
        $Array = $this->Database->select_qry_array($qry);
        $rating = !empty($Array[0]->rating) ? number_format($Array[0]->rating, 1) : '0';
        $rest_details = array(
            'homelyfood_id' => $data->vendor_id,
            'homelyfood_name' => $data->restaurant_name,
            'cuisine' => $data->cuisine_name,
            'service_charge' => $data->service_charge,
            'min_amount' => $data->min_amount,
            'delivery_time' => $data->delivery_time,
            'rating' => $rating,
            'reviews' => $Array[0]->total_reviews,
            'image_url' => $image,
            'isClosed' => $currentTime < $opening_time ? '1' : '0',
            'closed_message' => 'shop is closed at the moment',
            'isBusy' => $isBusy,
            'isParty' => $data->party_order,
            'promocode_title' => $offer_title,
            'banner_image' => $bannerimage,
            'address' => !empty($user637->area) ? $user637->area : '',
            'description' => !empty($data->about) ? strip_tags($data->about) : '',
        );
        $categories = GetAllCategories($homelyfood_id);
        $return = [];

        for ($i = 0; $i < count($categories); $i++) {
            $dft = $categories[$i];
            $return[$i] = array(
                'cat_id' => $dft->category_id,
                'category_name' => $dft->category_name,
            );

            // $array_sub = GetMenuByCatid($dft->category_id,$homelyfood_id,$user_id);
            $array_sub = listMenuByRestaurantAPISearch($homelyfood_id, $dft->category_id, $user_id);

            for ($j = 0; $j < count($array_sub); $j++) {
//                $admin_data = "SELECT discount,offer_addedby,menu_id FROM `promo_code` WHERE  `offer_addedby` ='$homelyfood_id' AND status=0 AND archive=0";
//                $qry = $this->Database->select_qry_array($admin_data);
//                $offerTitle='';
//                if (!empty($qry)) {
//                    $offer_menu = explode(',', $qry[0]->menu_id);
//                    if (in_array($array_sub[$j]->id, $offer_menu)) {
//                        $offteredd = $qry;
//                        $offteredd = $offteredd[0];
//                        $offerTitle = $array_sub[$j]->price;
//                        $calculatedPrice = $array_sub[$j]->price * $offteredd->discount / 100;
//                        $calculatedPrice = $offerTitle - $calculatedPrice;
//                        $array_sub[$j]->price = $calculatedPrice;
//
//                        // echo 'FFF'.$array_sub[$j]->price.'NEW';
//                        $discount = $qry[0]->discount;
//                    } else {
//                        $discount = '0';
//                    }
//                } else {
//                    $discount = '0';
//                }

                $prc = getMenudiscountArray($menuId = $array_sub[$j]->id, $array_sub[$j]->price, $homelyfood_id);

                $array_sub1 = GetMenuSize($array_sub[$j]->id);
                $array_sub2 = GetMenuAddon($array_sub[$j]->id);
                $array_sub3 = GetMenuTopping($array_sub[$j]->id);
                $array_sub4 = GetMenuDrink($array_sub[$j]->id);
                $array_sub5 = GetMenuDip($array_sub[$j]->id);
                $array_sub6 = GetMenuSide($array_sub[$j]->id);
                if (!empty($array_sub[$j]->cart_id)) {
                    $is_cart = true;
                } else {
                    $is_cart = false;
                }

                if ($this->language == 'UR' && !empty($array_sub[$j]->menu_name_ar)) {
                    $array_sub[$j]->menu_name = $array_sub[$j]->menu_name_ar;
                }

                $rywtr = array(
                    'menu_id' => $array_sub[$j]->id,
                    'menu_name' => $array_sub[$j]->menu_name,
                    'description' => str_replace('&nbsp;', '', strip_tags($array_sub[$j]->description)),
                    'price' => DecimalAmount($prc['final_price']),
                    'special_price' => !empty($prc['cross_price']) ? DecimalAmount($prc['cross_price']) : '',
                    'discount_percentage' => !empty($prc['total_discount']) ? DecimalAmount($prc['total_discount']) : '',
                    'image_url' => !empty($array_sub[$j]->image) ? base_url() . "uploads/menu/" . $array_sub[$j]->image : base_url() . DEFAULT_LOGO_MENU,
                    'is_cart' => $is_cart,
                    'is_stock' => !empty($array_sub[$j]->stock) ? true : false,
                );
                if ($data->store_type == '1') {
                    $rywtr['is_stock'] = true;
                }
                $return[$i]['menu_list'][$j] = $rywtr;
                if (!empty($array_sub1)) {
                    for ($k = 0; $k < count($array_sub1); $k++) {

                        $return[$i]['menu_list'][$j]['size'][$k] = array(
                            'size_id' => $array_sub1[$k]->menu_size_id,
                            'size_name' => $array_sub1[$k]->menu_size,
                            'size_price' => $array_sub1[$k]->menu_price,
                        );
                    }
                } else {
                    $return[$i]['menu_list'][$j]['size'] = [];
                }
                if (!empty($array_sub2)) {
                    for ($a = 0; $a < count($array_sub2); $a++) {


                        $return[$i]['menu_list'][$j]['addons'][$a] = array(
                            'addon_id' => $array_sub2[$a]->menu_addon_id,
                            'addon_name' => $array_sub2[$a]->add_on,
                            'addon_price' => $array_sub2[$a]->addon_price,
                        );
                    }
                } else {
                    $return[$i]['menu_list'][$j]['addons'] = [];
                }
                if (!empty($array_sub3)) {
                    for ($b = 0; $b < count($array_sub3); $b++) {
                        $return[$i]['menu_list'][$j]['topping'][$b] = array(
                            'topping_id' => $array_sub3[$b]->menu_topping_id,
                            'topping_name' => $array_sub3[$b]->topping,
                            'topping_price' => $array_sub3[$b]->topping_price,
                        );
                    }
                } else {
                    $return[$i]['menu_list'][$j]['topping'] = [];
                }

                if (!empty($array_sub4)) {
                    for ($c = 0; $c < count($array_sub4); $c++) {
                        $return[$i]['menu_list'][$j]['drink'][$c] = array(
                            'drink_id' => $array_sub4[$c]->menu_drink_id,
                            'drink_name' => $array_sub4[$c]->drink_name,
                            'drink_price' => $array_sub4[$c]->drink_price,
                        );
                    }
                } else {
                    $return[$i]['menu_list'][$j]['drink'] = [];
                }

                if (!empty($array_sub5)) {
                    for ($d = 0; $d < count($array_sub5); $d++) {
                        $return[$i]['menu_list'][$j]['dip'][$d] = array(
                            'dip_id' => $array_sub5[$d]->menu_dip_id,
                            'dip_name' => $array_sub5[$d]->dips,
                            'dip_price' => $array_sub5[$d]->dip_price,
                        );
                    }
                } else {
                    $return[$i]['menu_list'][$j]['dip'] = [];
                }

                if (!empty($array_sub6)) {
                    for ($e = 0; $e < count($array_sub6); $e++) {
                        $return[$i]['menu_list'][$j]['side'][$e] = array(
                            'side_id' => $array_sub6[$e]->menu_side_id,
                            'side_name' => $array_sub6[$e]->side_dish,
                            'side_price' => $array_sub6[$e]->side_dish_price,
                        );
                    }
                } else {
                    $return[$i]['menu_list'][$j]['side'] = [];
                }
            }
        }
        //   $result['homelyfood_details'] = $rest_details;
        $newMenu = [];
        for ($i = 0; $i < count($return); $i++) {
            $d = $return[$i];

            $mllist = !empty($d['menu_list']) ? $d['menu_list'] : [];
            for ($kk = 0; $kk < count($mllist); $kk++) {
                $ghju = $mllist[$kk];
                $ghju['cat_id'] = $d['cat_id'];
                $ghju['category_name'] = $d['category_name'];

                $newMenu[] = $ghju;
            }
        }
        $result['menu_list'] = $newMenu;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
    }

    public function AddToCart() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $UserId = !empty($json['user_id']) ? $json['user_id'] : '';
        $homelyfood_id = !empty($json['homelyfood_id']) ? $json['homelyfood_id'] : '';
        $quantity = !empty($json['quantity']) ? $json['quantity'] : '';
        $menu_id = !empty($json['menu_id']) ? $json['menu_id'] : '';
        $category_id = !empty($json['category_id']) ? $json['category_id'] : '';
        $product_price = !empty($json['product_price']) ? str_replace(',', '', $json['product_price']) : '0'; //total_price
        $size = !empty($json['size']) ? $json['size'] : '';
        $addon = !empty($json['addon']) ? $json['addon'] : ''; //multiple
        $topping = !empty($json['topping']) ? $json['topping'] : ''; //multiple
        $drink = !empty($json['drink']) ? $json['drink'] : '';
        $dip = !empty($json['dip']) ? $json['dip'] : ''; //multiple
        $side = !empty($json['side']) ? $json['side'] : ''; //multiple
        $location = !empty($json['location']) ? $json['location'] : '';
        $CondutaionArray = array('id' => $menu_id, 'category_id' => $category_id);
        $Qry = "SELECT * FROM  menu_list WHERE id=:id AND category_id=:category_id";
        $CategoryArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
        $array['user_id'] = $UserId;
        $array['menu_id'] = $menu_id;
        $CondutaionArray1 = array('category_id' => $category_id);
        $Qry1 = "SELECT * FROM  most_selling WHERE category_id=:category_id";
        $CategoryArray1 = $this->Database->select_qry_array($Qry1, $CondutaionArray1);



        $mArray = "SELECT * FROM `add_to_cart` WHERE `user_id` = '$UserId' AND menu_id='$menu_id'";
        $mArray = $this->Database->select_qry_array($mArray);

        if (!empty($mArray)) {
            // $quantity = $quantity + $mArray[0]->quantity;
        }

        $vendor = GetusersById($homelyfood_id);
        if (count($CategoryArray1) == 0) {
            $array['category_id'] = $category_id;
        } else {
            $CondutaionArray2 = array('id' => $menu_id);
            $Qry2 = "SELECT * FROM menu_list WHERE id=:id";
            $CategoryArray2 = $this->Database->select_qry_array($Qry2, $CondutaionArray2);
            $array['category_id'] = $CategoryArray2[0]->category_id;
        }
        // $array['category_id'] = $category_id;
        // $array['menu_name'] = $CategoryArray[0]->menu_name;
        $product_price = $product_price / $quantity; // by mistake from app side they are sending including price* qty thaty why again we removeing


        $array['product_price'] = $product_price;
        $array['quantity'] = $quantity;
        $subtotal = $array['quantity'] * $array['product_price'];
        $array['subtotal'] = (string) $subtotal;
        $array['vendor_id'] = $homelyfood_id;
        $array['location'] = trim($location);
        $array['addon'] = !empty($addon) ? implode(',', $addon) : '';
        $array['size'] = !empty($size) ? $size : '';
        $array['topping'] = !empty($topping) ? implode(',', $topping) : '';
        $array['drink'] = !empty($drink) ? $drink : '';
        $array['dip'] = !empty($dip) ? implode(',', $dip) : '';
        $array['side'] = !empty($side) ? implode(',', $side) : '';
        $Menu = GetMenuById($menu_id);

        if (empty($Menu)) {
            die(json_encode(array('response' => array('status' => false, 'message' => $this->lang->line("Invalid_menu_id")))));
        }
        $menudetails = $Menu[0];
        if ($vendor->store_type != '1' && !empty($menudetails->max_purchase_qty)) {
            if ($quantity > $menudetails->max_purchase_qty) {
                die(json_encode(array('response' => array('status' => false, 'message' => "You can purchase only $menudetails->max_purchase_qty quantity."))));
            }
        }
        $Condutaion = array('user_id' => $UserId);
        $Qry1 = "SELECT * FROM  add_to_cart WHERE user_id=:user_id";
        $itemArray = $this->Database->select_qry_array($Qry1, $Condutaion);
        if (count($itemArray) == 0 || $itemArray[0]->vendor_id == $homelyfood_id) {
            $Condition = array('user_id' => $UserId, 'menu_id' => $menu_id);
            $Qry2 = "SELECT * FROM  add_to_cart WHERE user_id=:user_id AND menu_id=:menu_id";
            $itemArray1 = $this->Database->select_qry_array($Qry2, $Condition);
            if (count($itemArray1) > 0) {
                if ($menu_id == $itemArray1[0]->menu_id) {
                    $data = array('quantity' => $quantity,
                        'subtotal' => $itemArray1[0]->product_price * $quantity);
                    $UpD = $this->Database->update('add_to_cart', $data, $Condition);

                    $result4747['in_cart'] = false;
                    $result4747['cart_count'] = $this->getcartcountAPI();

                    $_REQUEST['returncartType'] = true;
                    $cartList = $this->CartList();
                    $pricearray = !empty($cartList['pricearray']) ? $cartList['pricearray'] : [];
                    $result4747['cart_amount'] = $pricearray['totalprice'];

                    die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("Successfully_addedto_cart")), 'result' => $result4747)));
                }
            }
            $cart[0] = $array;
            $cart = array_values(array_filter($cart));
            $this->Database->insert('add_to_cart', $cart[0]);

            $result4747['in_cart'] = false;
            $result4747['cart_count'] = $this->getcartcountAPI();

            $_REQUEST['returncartType'] = true;
            $cartList = $this->CartList();
            $pricearray = !empty($cartList['pricearray']) ? $cartList['pricearray'] : [];
            $result4747['cart_amount'] = $pricearray['totalprice'];
            die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("Successfully_addedto_cart")), 'result' => $result4747)));
        } else {
            $Condutaion1 = array('vendor_id' => $itemArray[0]->vendor_id);
            $Qry2 = "SELECT restaurant_name FROM restaurant_details WHERE vendor_id=:vendor_id";
            $itemArray1 = $this->Database->select_qry_array($Qry2, $Condutaion1);
            $msg = 'There are items in your cart from ' . $itemArray1[0]->restaurant_name . '. Do you want to clear your cart?';
            die(json_encode(array('response' => array('status' => true, 'message' => $msg), 'result' => array('in_cart' => true))));
        }
    }

    public function clear_cart() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);

        $UserId = !empty($json['user_id']) ? $json['user_id'] : '';
        $CondArray = array('user_id' => $UserId);
        $this->Database->delete('add_to_cart', $CondArray);

        $result4747['cart_count'] = $this->getcartcountAPI();

        $_REQUEST['returncartType'] = true;
        $cartList = $this->CartList();
        $pricearray = !empty($cartList['pricearray']) ? $cartList['pricearray'] : [];
        $result4747['cart_amount'] = $pricearray['totalprice'];
        die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("Successfully_cleared_cart")), 'result' => $result4747)));
    }

    public function getcartcountAPI() {
        $_REQUEST['returncartType'] = true;
        $cartList = $this->CartList();
        $listarray = !empty($cartList['listarray']) ? $cartList['listarray'] : [];
        $qty = 0;
        for ($j = 0; $j < count($listarray); $j++) {
            $d = $listarray[$j];
            $qty = $qty + $d['quantity'];
        }
        return $qty;
    }

    public function CartList() {
        $json = file_get_contents('php://input');
        //send_mail('upendra@alwafaagroup.com', 'CartList', $json);
        $json = json_decode($json, TRUE);
        $UserId = !empty($json['user_id']) ? $json['user_id'] : '';
        $returncartType = !empty($_REQUEST['returncartType']) ? $_REQUEST['returncartType'] : '';
        $Qry = "SELECT * FROM  add_to_cart WHERE user_id='$UserId'";
        $CartArray = $this->Database->select_qry_array($Qry);

        if (empty($CartArray)) {
            $redyfyf['listarray'] = [];
            $redyfyf['cart_amount'] = '0';
            $redyfyf['cart_count'] = '0';
            die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("cart_is_empty")), 'result' => $redyfyf)));
        }

        $catArray = GetCategory($CartArray[0]->category_id);
        $catArray = !empty($catArray) ? $catArray[0] : '';
        $vendorId = !empty($CartArray[0]->vendor_id) ? $CartArray[0]->vendor_id : '';
        $min_amount = GetRestaurantDetails($vendorId);



        $CartArrayNew = [];
        $array = [];
        $options = [];
        $subtotal = 0;
        $sub_total = 0;

        $isprecriptionUpload = false;
        for ($i = 0; $i < count($CartArray); $i++) {
            $d = $CartArray[$i];

            $Qry = "SELECT * FROM  menu_list WHERE id='$d->menu_id'";
            $MenuArray = $this->Database->select_qry_array($Qry);


            $subtotal = $d->quantity * $d->product_price;

            if (!empty($MenuArray[0]->prescriptionreq)) {
                $isprecriptionUpload = true;
            }

            if (!empty($MenuArray[0]->image)) {
                $image = base_url() . 'uploads/menu/' . $MenuArray[0]->image;
            } else {
                $image = base_url(DEFAULT_LOGO_MENU);
            }

            if (isset($d->addon) && !empty($d->addon)) {
                $addon = explode(',', $d->addon);
                foreach ($addon as $a) {
                    $Qry = "SELECT menu_addon_id as id,add_on as name FROM  menu_addons WHERE menu_addon_id='$a'";
                    $CategoryArray = $this->Database->select_qry_array($Qry);

                    $array['addon'][] = $CategoryArray[0];
                }
            } else {
                $array['addon'] = [];
            }

            if (isset($d->drink) && !empty($d->drink)) {
                $drink = explode(',', $d->drink);
                foreach ($drink as $t) {
                    $Qry = "SELECT menu_drink_id as id,drink_name as name FROM  menu_drink WHERE menu_drink_id='$t'";
                    $CategoryArray = $this->Database->select_qry_array($Qry);
                    $array['drink'][] = $CategoryArray[0];
                }
            } else {
                $array['drink'] = [];
            }
            if (isset($d->topping) && !empty($d->topping)) {
                $topping = explode(',', $d->topping);
                foreach ($topping as $t) {
                    $Qry = "SELECT menu_topping_id as id,topping as name FROM  menu_topping WHERE menu_topping_id='$t'";
                    $CategoryArray = $this->Database->select_qry_array($Qry);
                    $array['topping'][] = $CategoryArray[0];
                }
            } else {
                $array['topping'] = [];
            }
            if (isset($d->dip) && !empty($d->dip)) {
                $dip = explode(',', $d->dip);
                foreach ($dip as $di) {
                    $Qry = "SELECT menu_dip_id as id,dips as name FROM  menu_dips WHERE menu_dip_id='$di'";
                    $CategoryArray = $this->Database->select_qry_array($Qry);

                    $array['dip'][] = $CategoryArray[0];
                }
            } else {
                $array['dip'] = [];
            }
            if (isset($d->side) && !empty($d->side)) {
                $side = explode(',', $d->side);
                foreach ($side as $s) {
                    $Qry = "SELECT menu_side_id as id,side_dish as name FROM  menu_side WHERE menu_side_id='$s'";
                    $CategoryArray = $this->Database->select_qry_array($Qry);
                    $array['side'][] = $CategoryArray[0];
                }
            } else {
                $array['side'] = [];
            }
            if (isset($d->size) && !empty($d->size)) {
                $size = explode(',', $d->size);
                foreach ($size as $si) {
                    $Qry = "SELECT menu_size_id as id,menu_size as name FROM  menu_sizes WHERE menu_size_id='$si'";
                    $CategoryArray = $this->Database->select_qry_array($Qry);
                    $array['size'][] = $CategoryArray[0];
                }
            } else {
                $array['size'] = [];
            }

            if ($this->language == 'UR' && !empty($MenuArray[0]->menu_name_ar)) {
                $MenuArray[0]->menu_name = $MenuArray[0]->menu_name_ar;
            }

            $CartArrayNew[$i] = array(
                'menu_id' => $d->menu_id,
                'homelyfood_id' => $d->vendor_id,
                'category_id' => $d->category_id,
                'quantity' => $d->quantity,
                'product_price' => $d->product_price,
                'menu_name' => $MenuArray[0]->menu_name,
                'image_url' => $image,
                'subtotal' => (string) $subtotal,
                'options' => !empty($array) ? array($array) : ''
            );
            $sub_total += $d->product_price * $d->quantity;
        }
//print_r($CartArrayNew);die();
        //     $session_cart = $this->session->userdata('CartData');
        //     $subtotal = 0;
        //     foreach($session_cart as $cart)
        //     {
        //         $subtotal += $cart['price'] * $cart['quantity'];
        //     }
        //     $get_restaurant_details = GetRestaurantDetails($session_cart[0]['vendor_id']);
        //     $return['subtotal'] = number_format($subtotal,2);
        //     $sub_tot = number_format($subtotal,2);
        // 	$serv_charge = number_format($get_restaurant_details[0]->service_charge,2);
        // 	$total_amount = $sub_tot + $serv_charge;
        //     $return['delivery_fee'] = number_format($get_restaurant_details[0]->service_charge,2);
        //     $return['total_amount'] = number_format($total_amount,2);
        //     $return['product_details'] = $session_cart;
//            $qry = "SELECT * FROM `promo_code` WHERE promocode_id='".$CartArray[0]->promocode_id."'";
//            $pArray = $this->Database->select_qry_array($qry);
//            $pArray=!empty($pArray) ? $pArray[0] : '';
//           
//           if (empty($pArray->discount_type)) {
//                 $discount = DecimalAmount($sub_total) * $CartArray[0]->discount / 100;
//           }else{
//               $discount=$CartArray[0]->discount;
//           }

        $discount = 0;

        $get_restaurant_details = GetRestaurantDetails($CartArray[0]->vendor_id);
        // $total_amount = (number_format($sub_total, 2) + number_format($get_restaurant_details[0]->service_charge, 2)) - $discount;
        $total_amount = (DecimalAmount($sub_total)) - $discount;
        $Return = array(
            'listarray' => $CartArrayNew,
            'pricearray' => array(
                'sub_total' => (string) DecimalAmount($sub_total),
                'discount' => (string) $discount,
                'delivery_fee' => 0,
                'totalprice' => (string) DecimalAmount($total_amount),
            ),
        );

        $Return['order_type'] = !empty($get_restaurant_details[0]->self_pickup) ? $get_restaurant_details[0]->self_pickup : '0';
        $Return['is_scheduled'] = !empty($catArray->store_type) && $catArray->store_type != '1' ? true : false;
        $Return['store_type'] = !empty($catArray->store_type) ? $catArray->store_type : '';

        $Return['is_upload_prescription'] = $Return['store_type'] == '3' && !empty($isprecriptionUpload) ? true : false;
        $Return['is_required_prescription'] = $Return['is_upload_prescription'];
        $Return['is_prescription_required'] = $Return['is_upload_prescription'];


        $Return['is_checkout'] = true;
        $Return['checkout_message'] = '';


        $adrs = "SELECT * FROM `user_order_address` WHERE archive=0 AND user_id='$UserId' AND isDefault=1";
        $ad = $this->Database->select_qry_array($adrs);

        if (!empty($ad)) {
            $ad = $ad[0];
            $delAdres = array(
                'address_id' => $ad->id,
                'address_label' => $ad->address_label,
                'address' => $ad->address,
                'latitude' => $ad->latitude,
                'longitude' => $ad->longitude,
                'street' => $ad->street,
                'additional_direction' => $ad->additional_direction,
                'mobile_code' => $ad->mobile_code,
                'mobile_number' => $ad->mobile_number,
                'landphone_no' => $ad->landphone_no,
                'building' => $ad->building,
                'office' => $ad->office,
                'house' => $ad->house,
                'apartment_no' => $ad->apartment_no,
                'floor' => $ad->floor,
                'address_type' => $ad->address_label,
            );
            $Return['delivery_address'] = $delAdres;
        }
        $Return['loyality_point'] = DecimalAmount(getloyalityTotalCreditPoint($UserId));
        $Return['is_special_request'] = true;
        if (!empty($returncartType)) {
            return $Return;
        } else {

            if (!empty($min_amount[0]->min_amount)) {
                if ($sub_total < $min_amount[0]->min_amount) {
                    $Return['is_checkout'] = false;
                    $rem_amt = $min_amount[0]->min_amount - $sub_total;
                    $Return['checkout_message'] = "Add PKR " . DecimalAmount($rem_amt) . " to Checkout";
                }
            }
        }
        die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $Return)));
    }

    public function apply_promo_code() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $promo_code = !empty($json['promo_code']) ? $json['promo_code'] : '';
        $homelyfood_id = !empty($json['homelyfood_id']) ? $json['homelyfood_id'] : '';
        $address_id = !empty($json['address_id']) ? $json['address_id'] : '';
        $user_id = !empty($json['user_id']) ? $json['user_id'] : [];
        $total_price = !empty($json['total_price']) ? $json['total_price'] : '';
        $price_list = !empty($json['price_list']) ? $json['price_list'] : '';

        $delivery_fee = !empty($price_list['delivery_fee']) ? $price_list['delivery_fee'] : '0';

        $_REQUEST['returncartType'] = true;
        $cartList = $this->CartList();
        $pricearray = !empty($cartList['pricearray']) ? $cartList['pricearray'] : [];
        $total_price = $pricearray['sub_total'];


        $qry = "SELECT * FROM `promo_code` WHERE promo_code='$promo_code' AND archive=0 AND status=0 AND date_to >=DATE(NOW())";
        $pArray = $this->Database->select_qry_array($qry);
        if (empty($pArray)) {
            $qry = "SELECT * FROM `promo_code` WHERE promo_code='$promo_code' AND archive=0 AND status=0";
            $pArray = $this->Database->select_qry_array($qry);
            if (!empty($pArray)) {
                $result4049['message'] = $this->lang->line("fill_the_form") . 'Promo code has expired.';
                die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $this->lang->line("Promo_code_has_expired")), 'result' => $result4049)));
            }
            $result4049['message'] = 'Invalid promo code.';
            die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $this->lang->line("Invalid_promo")), 'result' => $result4049)));
        }
        $pArray = $pArray[0];
        $discountedAmt = 0;
        if (!empty($pArray->restaurant_id)) {

            $matchedBox = explode(',', $pArray->restaurant_id);
            $matchedBoxFound = [];
            $promocode_id = $pArray->promocode_id;
            $ProductExists = false;

            if (in_array($homelyfood_id, $matchedBox)) {
                $ProductExists = true;
            }

            if (empty($ProductExists)) {
                $result4049['message'] = $this->lang->line("Promo_code_is_not_available_this_produc");
                die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => "Promo code is not available for this product"), 'result' => $result4049)));
            }
            if ($total_price < $pArray->purchase_amount) {
                $result4049['message'] = "This code is applicable only on orders above PKR $pArray->purchase_amount";
                die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $result4049['message']), 'result' => $result4049)));
            }
        }


        $cartList = $this->CartList();
        $pricearray = !empty($cartList['pricearray']) ? $cartList['pricearray'] : [];

        if (!empty($pArray->discount_from)) {
            if (empty($delivery_fee)) {
                die(json_encode(array("status" => 'error', "message" => "This code is applicable only for delivery charges")));
            }
            $total_price = $delivery_fee;
        }



        if (empty($pArray->discount_type)) {
            $discountedAmt = $total_price * $pArray->discount / 100;
        } else {
            $discountedAmt = $pArray->discount;
        }

        $result['promo_code_discount'] = $discountedAmt;


        $pricearray['discount'] = $discountedAmt;
        $pricearray['delivery_fee'] = DecimalAmount(getdeliverychargesBy($address_id, $user_id));

        $totalFee = $pricearray['delivery_fee'] + $pricearray['totalprice'];
        $pricearray['totalprice'] = DecimalAmount($totalFee - $pricearray['discount']);

        $result['pricearray'] = $pricearray;
        $result['promo_code_discount'] = $discountedAmt;


        $condCart = array('user_id' => $user_id, 'vendor_id' => $homelyfood_id);
        $updateUser = array('discount' => $discountedAmt, 'promocode_id' => $pArray->promocode_id);
        $this->Database->update('add_to_cart', $updateUser, $condCart);

        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => "$discountedAmt PKR  has been discounted."), 'result' => $result)));
    }

    public function get_delivery_charges() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $UserId = !empty($json['user_id']) ? $json['user_id'] : '';
        $addressId = !empty($json['address_id']) ? $json['address_id'] : '';
        $_REQUEST['returncartType'] = true;
        $cartList = $this->CartList();
        $address = GetuseraddressBy($addressId);
        if (empty($address)) {
            die(json_encode(array('response' => array('status' => false, 'message' => $this->lang->line("invalid_address_id"), 'result' => []))));
        }
        $Qry = "SELECT * FROM  add_to_cart WHERE user_id='$UserId'";
        $CartArray = $this->Database->select_qry_array($Qry);


        $condCart = array('user_id' => $UserId);
        $updateUser = array('discount' => '0', 'promocode_id' => '0');
        $this->Database->update('add_to_cart', $updateUser, $condCart);








        $vendor_id = !empty($CartArray) ? $CartArray['0']->vendor_id : '';
        $vendor = GetUserDetails($vendor_id);
        $vendor = !empty($vendor) ? $vendor[0] : '';
        $free_delivery = !empty($vendor->free_delivery) ? $vendor->free_delivery : '0';


        $Qry = "SELECT * FROM `restaurant_details` LEFT JOIN users ON users.id=restaurant_details.vendor_id WHERE vendor_id='$vendor_id'";
        $rest_details = $this->Database->select_qry_array($Qry);

        $rest_details1 = GetusersById($rest_details[0]->vendor_id);

        $distanceTotal = distance($rest_details[0]->latitude, $rest_details[0]->longitude, $address->latitude, $address->longitude, $unit = 'K');
        $delivery_radius = !empty($rest_details1->delivery_radius) ? $rest_details1->delivery_radius : '0';

        if (!empty($delivery_radius) && $distanceTotal > $delivery_radius) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => "Sorry we will deliver only inside  $delivery_radius  kilometre."))));
        }

        $dlCharges = getdeleverychagesfeecal35($rest_details, $address->latitude, $address->longitude);


        $pricearray = !empty($cartList['pricearray']) ? $cartList['pricearray'] : [];
        if ($free_delivery > 0) {
            if ($pricearray['sub_total'] > $free_delivery) {
                $dlCharges = '0';
            }
        }
        $pricearray['delivery_fee'] = DecimalAmount($dlCharges);
        $pricearray['totalprice'] = DecimalAmount($pricearray['sub_total'] + $pricearray['delivery_fee']);

        $result['pricearray'] = $pricearray;


        $cond3232 = array('user_id' => $UserId);
        $update3333 = array('isDefault' => '0');
        $this->Database->update('user_order_address', $update3333, $cond3232);

        $cond3232 = array('id' => $addressId);
        $update3333 = array('isDefault' => '1');
        $this->Database->update('user_order_address', $update3333, $cond3232);


        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => ''), 'result' => $result)));
    }

    public function remove_promocode() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $promo_code = !empty($json['promo_code']) ? $json['promo_code'] : '';
        $homelyfood_id = !empty($json['homelyfood_id']) ? $json['homelyfood_id'] : '';
        $user_id = !empty($json['user_id']) ? $json['user_id'] : [];
        $condCart = array('user_id' => $user_id, 'vendor_id' => $homelyfood_id);
        $updateUser = array('discount' => '', 'promocode_id' => '');
        $this->Database->update('add_to_cart', $updateUser, $condCart);

        die(json_encode(array("code" => $this->success, 'response' => array('status' => true, 'message' => $this->lang->line("Promo_code_has_been_removed")))));
    }

    public function Update_Cart() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $menu_id = !empty($json['menu_id']) ? $json['menu_id'] : '';
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $quantity = !empty($json['quantity']) ? $json['quantity'] : '';

        $menudetails = GetMenuById($menu_id);
        $vendor = GetusersById($menudetails->vendor_id);
        $menudetails = !empty($menudetails) ? $menudetails[0] : '';
        if ($vendor->store_type != '1' && !empty($menudetails->max_purchase_qty)) {
            if ($quantity > $menudetails->max_purchase_qty) {
                die(json_encode(array('response' => array('status' => false, 'message' => "You can purchase only $menudetails->max_purchase_qty quantity."), 'result' => [])));
            }
        }
        // $condCart = array('user_id' => $user_id, 'product_id' => $product_id);
        // $session_cart = $this->session->userdata('CartData');
        // for ($i = 0; $i < count($session_cart); $i++) {
        //     if ($menu_id == $session_cart[$i]['menu_id'])
        //     {
        //     $session_cart[$i]['quantity'] = $quantity;
        //     $session_cart[$i]['subtotal'] = $session_cart[$i]['price'] * $session_cart[$i]['quantity'];
        //     }
        // }
        //  $session_cart = array_values(array_filter($session_cart));
        //     $this->session->set_userdata('CartData', $session_cart);
        $condCart = array('user_id' => $user_id, 'menu_id' => $menu_id);
        $updateUser = array('quantity' => $quantity);
        $UpD = $this->Database->update('add_to_cart', $updateUser, $condCart);
        if ($UpD) {
            $result['cart_count'] = $this->getcartcountAPI();

            $_REQUEST['returncartType'] = true;
            $cartList = $this->CartList();
            $pricearray = !empty($cartList['pricearray']) ? $cartList['pricearray'] : [];
            $result['cart_amount'] = $pricearray['totalprice'];
            die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("Quantity_updated_successfully")), 'result' => $result)));
        }
    }

    public function RemoveFromCart() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $UserId = !empty($json['user_id']) ? $json['user_id'] : '';
        $menuId = !empty($json['menu_id']) ? $json['menu_id'] : '';
        // $UserArray = GetUserArrayByUserId($UserId);
        // $Products = GetProductDetailByProductId($ProductId);
        // if (empty($UserArray)) {
        //     die(json_encode(array('response' => array('status' => false, 'message' => 'Invalid user id.'))));
        // }
        RemoveFromCartAPI($UserId, $menuId);
        // $CartArray = GetCartDetailsAPI($UserId, $this->device_id);
        $result['cart_count'] = $this->getcartcountAPI();

        $_REQUEST['returncartType'] = true;
        $cartList = $this->CartList();
        $pricearray = !empty($cartList['pricearray']) ? $cartList['pricearray'] : [];
        $result['cart_amount'] = $pricearray['totalprice'];
        die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("Successfully_removed_from_cart")), 'result' => $result)));
    }

    public function MyOrders() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $UserId = !empty($json['user_id']) ? $json['user_id'] : '';
        $UserArray = GetCustomerArrayById($UserId);
        if (empty($UserArray)) {
            die(json_encode(array('response' => array('status' => false, 'message' => $this->lang->line("Invalid_user_id")))));
        }
        $OrderArray = GetOrderList($UserId);
        for ($i = 0; $i < count($OrderArray); $i++) {
            $d = $OrderArray[$i];
            // $d->MedArray = GetSingleMediaVideo($d->product_id);
            $d->StatusName = GetOrderStatusName($d->order_status);

            $d->restaurant_name = $this->language == 'UR' && !empty($d->restaurant_name_ar) ? $d->restaurant_name_ar : $d->restaurant_name;
            if (empty($d->image_url)) {
                $d->image_url = base_url(DEFAULT_LOGO_RESTAURANT);
            }
            unset($d->apiJson, $d->restaurant_name_ar);
        }
        die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => array('orders' => $OrderArray))));
    }

    public function OrderDetails() {

        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $UserId = !empty($json['user_id']) ? $json['user_id'] : '';

        $OrderId = !empty($json['order_id']) ? $json['order_id'] : '';

        $UserArray = GetCustomerArrayById($UserId);
        if (empty($UserArray)) {
            // die(json_encode(array('response' => array('status' => false, 'message' => 'Invalid user id.'))));
        }


        $OrderArray = GetOrderById($OrderId);
        $address = GetOrderAddressById($OrderArray[0]->delivery_address);
        $restuRent = GetRestaurantDetails($OrderArray[0]->vendor_id);
        $restuRent = !empty($restuRent) ? $restuRent[0] : '';

        $vendor353 = GetusersById($OrderArray[0]->vendor_id);
        $rider = GetusersById($OrderArray[0]->assign_rider);
        $subTotal = 0;

        $responc = GetOrderSubmitResponce($OrderId);
        $return['delivered_time'] = !empty($responc['msgOrderDelv']) ? $responc['msgOrderDelv'] : '';




        $return['discount'] = DecimalAmount($OrderArray[0]->discount);
        $return['delivery_fee'] = DecimalAmount($OrderArray[0]->service_charge);
        $return['total_amount'] = DecimalAmount($OrderArray[0]->total_price + $OrderArray[0]->service_charge);
        for ($i = 0; $i < count($OrderArray); $i++) {
            $ord = $OrderArray[$i];
            $prdSubTotal = $ord->ord_del_price * $ord->quantity;

            if ($this->language == 'UR' && !empty($ord->menu_name_ar)) {
                $ord->product_name = $ord->menu_name_ar;
            }

            $subTotal = $subTotal + $prdSubTotal;
            $return['product_details'][$i] = array(
                'product_name' => $ord->product_name,
                'quantity' => $ord->quantity,
                'price' => DecimalAmount($prdSubTotal),
            );
            $size = explode(',', $ord->size);
            $addon = explode(',', $ord->add_on);
            $topping = explode(',', $ord->topping);
            $drink = explode(',', $ord->drink);
            $dip = explode(',', $ord->dip);
            $side = explode(',', $ord->side);
            if (isset($size) && $size[0] != '') {
                foreach ($size as $s) {
                    $size_qry = "SELECT menu_size FROM `menu_sizes` WHERE `menu_size_id`=" . $s;
                    $size_data = $this->Database->select_qry_array($size_qry);
                    $return['product_details'][$i]['options'][] = $size_data[0]->menu_size;
                }
            }
            if (isset($addon) && $addon[0] != '') {
                foreach ($addon as $a) {
                    $addon_qry = "SELECT add_on FROM `menu_addons` WHERE `menu_addon_id`=" . $a;
                    $addon_data = $this->Database->select_qry_array($addon_qry);
                    $return['product_details'][$i]['options'][] = $addon_data[0]->add_on;
                }
            }
            if (isset($topping) && $topping[0] != '') {
                foreach ($topping as $t) {
                    $topping_qry = "SELECT topping FROM `menu_topping` WHERE `menu_topping_id`=" . $t;
                    $topping_data = $this->Database->select_qry_array($topping_qry);
                    $return['product_details'][$i]['options'][] = $topping_data[0]->topping;
                }
            }
            if (isset($drink) && $drink[0] != '') {
                foreach ($drink as $d) {
                    $drink_qry = "SELECT drink_name FROM `menu_drink` WHERE `menu_drink_id`=" . $d;
                    $drink_data = $this->Database->select_qry_array($drink_qry);
                    $return['product_details'][$i]['options'][] = $drink_data[0]->drink_name;
                }
            }
            if (isset($dip) && $dip[0] != '') {
                foreach ($dip as $di) {
                    $dip_qry = "SELECT dips FROM `menu_dips` WHERE `menu_dip_id`=" . $di;
                    $dip_data = $this->Database->select_qry_array($dip_qry);
                    $return['product_details'][$i]['options'][] = $dip_data[0]->dips;
                }
            }
            if (isset($side) && $side[0] != '') {
                foreach ($side as $s) {
                    $side_qry = "SELECT side_dish FROM `menu_side` WHERE `menu_side_id`=" . $s;
                    $side_data = $this->Database->select_qry_array($side_qry);
                    $return['product_details'][$i]['options'][] = $side_date[0]->side_dish;
                }
            }
        }
        $return['subtotal'] = DecimalAmount($subTotal);


        if (!empty($address)) {
            if ($address[0]->address_label == 1) {
                $label = 'Apartment';
            } else if ($address[0]->address_label == 2) {
                $label = 'House';
            } else if ($address[0]->address_label == 2) {
                $label = 'Office';
            } else {
                $label = '';
            }
            $return['delivery_address'] = array(
                'address_id' => $address[0]->id,
                'address_label' => $address[0]->address_label,
                'address_label_name' => $label,
                'address' => $address[0]->address,
                'mobile_code' => $address[0]->mobile_code,
                'mobile_number' => $address[0]->mobile_number,
                'latitude' => $address[0]->latitude,
                'longitude' => $address[0]->longitude,
                'street' => $address[0]->street,
                'building' => $address[0]->building,
                'office' => $address[0]->office,
                'house' => $address[0]->house,
                'apartment_no' => $address[0]->apartment_no,
                'floor' => $address[0]->floor,
            );
        }
        $return['pickup_address'] = array(
            'address' => $vendor353->area,
            'mobile_code' => $vendor353->mobile_code,
            'mobile_number' => $vendor353->mobile_number,
        );
        $delivery_type = !empty($OrderArray[0]->delivery_type) ? $OrderArray[0]->delivery_type : '1';
        $delivery_typeVal = '0';
        if ($delivery_type == '2') {
            $delivery_typeVal = '1';
        }
        $is_tracking = false;
        $allowedTrack = [5];
//        $return['rider_details']=[];
        if (!empty($OrderArray[0]->assign_rider) && $delivery_type == '1') {
            $riderAr['name'] = $rider->name;
            $riderAr['email'] = $rider->email;
            $riderAr['mobile'] = $rider->mobile_code . $rider->mobile_number;
            $return['rider_details'] = $riderAr;
            if (in_array($OrderArray[0]->order_status, $allowedTrack)) {
                $is_tracking = true;
            }
        }


        $statusId = GetorderstatusBy($OrderArray[0]->order_status);
        if ($this->language == 'UR') {
            $statusId->status_name = GetorderstatusByAr($OrderArray[0]->order_status);
        }
        $return['payment_type'] = '0';
        $return['order_type'] = $delivery_typeVal;

        if ($this->language == 'UR' && !empty($restuRent->restaurant_name_ar)) {
            $restuRent->restaurant_name = $restuRent->restaurant_name_ar;
        }

        $return['order_details'] = array(
            'order_id' => $OrderId,
            'homeleyfood_id' => $OrderArray[0]->vendor_id,
            'restaurant_name' => $restuRent->restaurant_name,
            'order_date' => date('Y-m-d H:i:s', strtotime($OrderArray[0]->date)),
            'orderno' => $OrderArray[0]->orderno,
            'image_url' => $restuRent->resImage,
            'order_status' => $OrderArray[0]->order_status,
            'StatusName' => !empty($statusId->status_name) ? $statusId->status_name : '',
            'status_color' => !empty($statusId->status_color) ? $statusId->status_color : '',
            'is_tracking' => $is_tracking,
        );

        $return['cancel_order_list'] = [
            array('name' => 'The order took too long', 'value' => $this->lang->line("order_took_to_long")),
            array('name' => 'I changed my mind', 'value' => $this->lang->line("changed_my_mind")),
            array('name' => 'The price is too high', 'value' => $this->lang->line("price_high")),
            array('name' => 'I found it cheaper somewhere else', 'value' => $this->lang->line("found_somewhere_else")),
            array('name' => 'Other', 'value' => $this->lang->line("other"))
        ];
        $result = $return;
        // print_r($result);die();

        if (!empty($result)) {
            die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => array('orders' => $result))));
        } else {
            die(json_encode(array('response' => array('status' => false, 'message' => $this->lang->line("Order_deatils_not_found")), 'result' => array('orders' => $result))));
        }
    }

    public function add_address() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $address_id = !empty($json['address_id']) ? $json['address_id'] : '';
        $address_label = !empty($json['address_type']) ? $json['address_type'] : '';
        $address = !empty($json['address']) ? $json['address'] : '';
        $latitude = !empty($json['latitude']) ? $json['latitude'] : '';
        $longitude = !empty($json['longitude']) ? $json['longitude'] : '';
        $street = !empty($json['street']) ? $json['street'] : '';
        $additional_direction = !empty($json['additional_direction']) ? $json['additional_direction'] : '';
        $mobile_code = !empty($json['mobile_code']) ? $json['mobile_code'] : '';
        $mobile_number = !empty($json['mobile_number']) ? $json['mobile_number'] : '';
        $landphone_no = !empty($json['landphone_no']) ? $json['landphone_no'] : '';
        $building = !empty($json['building']) ? $json['building'] : '';
        $office = !empty($json['office']) ? $json['office'] : '';
        $house = !empty($json['house']) ? $json['house'] : '';
        $apartment_no = !empty($json['apartment_no']) ? $json['apartment_no'] : '';
        $floor = !empty($json['floor']) ? $json['floor'] : '';


        $mcount = strlen($mobile_number); // 6
        if ($mcount < 10) {
            die(json_encode(array("code" => $this->error, "response" => array("status" => false, "message" => $this->lang->line("Please_enter_valid_mobile")))));
        }
        $insert = array(
            'user_id' => $user_id,
            'address_label' => $address_label,
            'address' => $address,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'street' => $street,
            'additional_direction' => $additional_direction,
            'mobile_code' => $mobile_code,
            'mobile_number' => $mobile_number,
            'landphone_no' => $landphone_no,
            'building' => $building,
            'office' => $office,
            'house' => $house,
            'apartment_no' => $apartment_no,
            'address_label' => !empty($json['address_type']) ? $json['address_type'] : '',
            'floor' => $floor,
        );
        if (!empty($address_id)) {
            $this->Database->update('user_order_address', $insert, array('id' => $address_id));
        } else {
            $this->Database->insert('user_order_address', $insert);
        }

        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")))));
    }

    public function delete_address() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $address_id = !empty($json['address_id']) ? $json['address_id'] : '';
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $insert = array(
            'archive' => '1',
        );
        $this->Database->update('user_order_address', $insert, array('id' => $address_id, 'user_id' => $user_id));

        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("successfully_deleted")))));
    }

    public function saved_address() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';

        $result['address'] = getUserAddressByuserId383($user_id);
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
    }

    public function place_order_new() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $json = $_POST;

        $fileName = '';
        $UploadExcel = empty($_FILES['prescription_image']['error']) ? $_FILES['prescription_image'] : [];
        if (!empty($UploadExcel['name'])) {
            $fileName = uniqid() . '.' . pathinfo($UploadExcel['name'], PATHINFO_EXTENSION);
            $baseDir = HOME_DIR . "uploads/prescription/" . $fileName;
            if (!move_uploaded_file($UploadExcel['tmp_name'], $baseDir)) {
                $fileName = '';
            }
        }



        // send_mail('upendra@alwafaagroup.com', 'place_order_new', json_encode($json) . '--' . json_encode($_FILES));
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $address_id = !empty($json['address_id']) ? $json['address_id'] : '';
        $payment_mode = !empty($json['payment_mode']) ? $json['payment_mode'] : [];
        $delivery_type = !empty($json['order_type']) ? $json['order_type'] : [];
        $store_type = !empty($json['store_type']) ? $json['store_type'] : '';
        $distance_ord = 0;


        $Qry = "SELECT * FROM  add_to_cart WHERE user_id='$user_id'";

        $CartArray = $this->Database->select_qry_array($Qry);

//echo $CartArray[0]->vendor_id;exit;
        $rest_details = GetRestaurantById($CartArray[0]->vendor_id);
        $rest_details = !empty($rest_details) ? $rest_details[0] : '';
        if ($store_type == '1' && !empty($rest_details)) {
            $opening_time = date('H:i', strtotime($rest_details->opening_time));
            $closing_time = date('H:i', strtotime($rest_details->closing_time));
            $currentTime = date('H:i', time());

            if (empty($delivery_type)) {

                $opening_time = date('H:i', strtotime($rest_details->delivery_hours_st));
                $closing_time = date('H:i', strtotime($rest_details->delivery_hours_et));
            }
            if ($currentTime < $opening_time) {
                die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => 'shop is closed'))));
            } else {
                if ($currentTime > $closing_time) {
                    if (date('A', strtotime($closing_time)) == 'AM' && date('A') == 'PM') {
                        
                    } else {
                        die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => 'shop is closed'))));
                    }
                }
            }
        }
        if (!empty($rest_details->busy_status)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => 'shop is closed'))));
        }
//echo $closing_time;exit;

        $addresssAr = GetuseraddressBy($address_id);
        $vendorAr = GetusersById($CartArray[0]->vendor_id);
        if (empty($delivery_type)) {
            $delivery_type = '1';
        } else if ($delivery_type == '1') {
            $delivery_type = '2';
        }
        if (!empty($addresssAr) && !empty($vendorAr)) {
            $distance_ord = distance($vendorAr->latitude, $vendorAr->longitude, $addresssAr->latitude, $addresssAr->longitude, $unit = 'K');
        }
        if (!empty($vendorAr->archive)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => 'Shop is inactive'))));
        }
        if ($vendorAr->status != '1') {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => 'Shop is inactive'))));
        }
        $price = 0;
        if (!empty($CartArray)) {
            $cond = array('vendor_id' => $CartArray[0]->vendor_id);
            $Qry = "SELECT * FROM  restaurant_details WHERE vendor_id=:vendor_id";
            $RestArray = $this->Database->select_qry_array($Qry, $cond);
            foreach ($CartArray as $item) {
                $price = $price + ($item->product_price * $item->quantity);
            }
            $serviceCharge33 = 0;
            if ($delivery_type == '1') {
                $serviceCharge33 = !empty($json['delivery_fee']) ? $json['delivery_fee'] : '';
            }
            $discount = $CartArray[0]->discount;
            $total = ($price) - $discount;
            $order = array('user_id' => $user_id,
                'vendor_id' => $CartArray[0]->vendor_id,
                'delivery_address' => $address_id,
                'price' => $price,
                'discount' => $discount,
                'total_price' => $total,
                'vat' => 0,
                'service_charge' => $serviceCharge33,
                'mode_of_payment' => 3,
                'special_request' => !empty($json['special_request']) ? $json['special_request'] : '',
                //'pre_order' => $pre_order,
                //'delivery_time' =>$deliv_time,
                'status' => 1,
                'delivery_type' => $delivery_type,
                'store_type' => $store_type,
                'upload_prescription' => $fileName,
                'payment_date' => date('Y-m-d H:i:s'),
                'distance_ord' => !empty($distance_ord) ? $distance_ord : '0',
                'app_order' => '1',
                'date' => date('Y-m-d H:i:s')
            );

            $order['scheduled_delivery_date'] = !empty($json['delivery_date']) ? date('Y-m-d', strtotime($json['delivery_date'])) : '';
            $order['scheduled_delivery_time'] = !empty($json['delivery_from_time']) ? date('H:i:s', strtotime($json['delivery_from_time'])) : '';
            $order['scheduled_deliveryend_time'] = !empty($json['delivery_to_time']) ? date('H:i:s', strtotime($json['delivery_to_time'])) : '';

            $todays = date('Y-m-d');
            $date_live = !empty($json['delivery_date']) ? date('Y-m-d', strtotime($json['delivery_date'])) : '';
            $delivery_tiime = !empty($json['delivery_from_time']) ? date('H:i', strtotime($json['delivery_from_time'])) : '';
            $currentTime = date('H:i');
            if ($date_live == $todays) {
                if ($delivery_tiime < $currentTime) {
                    die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => 'Please schedule your order with availble time'))));
                }
            }
            $Result = $this->Database->insert('orders', $order);
            addloyalitypointcreditAmt($user_id, $Result, $order['total_price']);
            addNewOrderNotifaction($userId = $user_id, $vendorId = $order['vendor_id'], $orderId = $Result);

            $CondArray = array('id' => $Result);
            $orderno = date('dmy') . rand(10, 100) . $Result;
            $update_array = array('orderno' => $orderno);

            $this->Database->update('orders', $update_array, $CondArray);

            foreach ($CartArray as $cart) {
                $CondutaionArray = array('id' => $cart->menu_id);
                $Qry1 = "SELECT * FROM  menu_list WHERE id=:id";
                $CategoryArray1 = $this->Database->select_qry_array($Qry1, $CondutaionArray);

                $order_details = array(
                    'order_id' => $Result,
                    'category_id' => $cart->category_id,
                    'price' => $cart->product_price,
                    'product_id' => $cart->menu_id,
                    'quantity' => $cart->quantity,
                    'product_name' => !empty($CategoryArray1[0]->menu_name) ? $CategoryArray1[0]->menu_name : '',
                    'subtotal' => $cart->subtotal,
                    'vat' => 0,
                    'size' => $cart->size,
                    'add_on' => $cart->addon,
                    'topping' => $cart->topping,
                    'drink' => $cart->drink,
                    'dip' => $cart->dip,
                    'side' => $cart->side,
                );

                $Result1 = $this->Database->insert('order_details', $order_details);
                RemoveFromCartAPI($user_id, $cart->menu_id);
            }
            NeworderSendVendorNotifaction($Result);
            order_pdf($Result);
            send_email_for_order($Result);
            send_email_for_order_to_Vendor($Result);

            $res = GetOrderSubmitResponce($Result);
            $resultRes['delivered_time'] = $res['msgOrderDelv'];
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("Order_placed_successfully")), 'result' => $resultRes)));
        } else {
            die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $this->lang->line("cart_is_empty")))));
        }
    }

    public function place_order() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $address_id = !empty($json['address_id']) ? $json['address_id'] : '';
        $payment_mode = !empty($json['payment_mode']) ? $json['payment_mode'] : [];
        $delivery_type = !empty($json['order_type']) ? $json['order_type'] : [];
        $store_type = !empty($json['store_type']) ? $json['store_type'] : '';
        if (empty($delivery_type)) {
            $delivery_type = '1';
        } else if ($delivery_type == '1') {
            $delivery_type = '2';
        }
        $Qry = "SELECT * FROM  add_to_cart WHERE user_id='$user_id'";
        $CartArray = $this->Database->select_qry_array($Qry);
        $price = 0;
        if (!empty($CartArray)) {
            $cond = array('vendor_id' => $CartArray[0]->vendor_id);
            $Qry = "SELECT * FROM  restaurant_details WHERE vendor_id=:vendor_id";
            $RestArray = $this->Database->select_qry_array($Qry, $cond);
            foreach ($CartArray as $item) {
                $price = $price + ($item->product_price * $item->quantity);
            }
            $serviceCharge33 = 0;
            if ($delivery_type == '1') {
                $serviceCharge33 = !empty($json['delivery_fee']) ? $json['delivery_fee'] : '';
            }

            $qrrty = "SELECT *  FROM `promo_code` WHERE `promocode_id` = '" . $CartArray[0]->promocode_id . "'";
            $prmArray = $this->Database->select_qry_array($qrrty);
            $prmArray = !empty($prmArray) ? $prmArray[0] : '';
            if (empty($prmArray->discount_type)) {
                $discount = $price * $CartArray[0]->discount / 100;
            } else {
                $discount = $CartArray[0]->discount;
            }

            $total = ($price) - $discount;
            $order = array('user_id' => $user_id,
                'vendor_id' => $CartArray[0]->vendor_id,
                'delivery_address' => $address_id,
                'price' => $price,
                'discount' => $discount,
                'total_price' => $total,
                'vat' => 0,
                'service_charge' => $serviceCharge33,
                'mode_of_payment' => 3,
                'special_request' => '',
                //'pre_order' => $pre_order,
                //'delivery_time' =>$deliv_time,
                'status' => 1,
                'delivery_type' => $delivery_type,
                'store_type' => $store_type,
                'payment_date' => date('Y-m-d H:i:s'),
                'app_order' => '1',
                'date' => date('Y-m-d H:i:s')
            );

            $order['scheduled_delivery_date'] = !empty($json['delivery_date']) ? date('Y-m-d', strtotime($json['delivery_date'])) : '';
            $order['scheduled_delivery_time'] = !empty($json['delivery_from_time']) ? date('H:i:s', strtotime($json['delivery_from_time'])) : '';
            $order['scheduled_deliveryend_time'] = !empty($json['delivery_to_time']) ? date('H:i:s', strtotime($json['delivery_to_time'])) : '';


            $Result = $this->Database->insert('orders', $order);
            addloyalitypointcreditAmt($user_id, $Result, $order['total_price']);
            addNewOrderNotifaction($userId = $user_id, $vendorId = $order['vendor_id'], $orderId = $Result);

            $CondArray = array('id' => $Result);
            $orderno = date('dmy') . rand(10, 100) . $Result;
            $update_array = array('orderno' => $orderno);

            $this->Database->update('orders', $update_array, $CondArray);

            foreach ($CartArray as $cart) {
                $CondutaionArray = array('id' => $cart->menu_id);
                $Qry1 = "SELECT * FROM  menu_list WHERE id=:id";
                $CategoryArray1 = $this->Database->select_qry_array($Qry1, $CondutaionArray);

                $order_details = array(
                    'order_id' => $Result,
                    'category_id' => $cart->category_id,
                    'price' => $cart->product_price,
                    'product_id' => $cart->menu_id,
                    'quantity' => $cart->quantity,
                    'product_name' => !empty($CategoryArray1[0]->menu_name) ? $CategoryArray1[0]->menu_name : '',
                    'subtotal' => $cart->subtotal,
                    'vat' => 0,
                    'size' => $cart->size,
                    'add_on' => $cart->addon,
                    'topping' => $cart->topping,
                    'drink' => $cart->drink,
                    'dip' => $cart->dip,
                    'side' => $cart->side,
                );

                $Result1 = $this->Database->insert('order_details', $order_details);
                RemoveFromCartAPI($user_id, $cart->menu_id);
            }
            NeworderSendVendorNotifaction($Result);
            order_pdf($Result);
            send_email_for_order($Result);
            send_email_for_order_to_Vendor($Result);
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("Order_placed_successfully")))));
        } else {
            die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $this->lang->line("cart_is_empty")))));
        }
    }

    public function notifications() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $nArray = GetNotifications($user_id);
        $result['notifications'] = $nArray;

        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
    }

    public function feedback() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $insert = array(
            'user_id' => !empty($json['user_id']) ? $json['user_id'] : '',
            'vendor_id' => !empty($json['vendor_id']) ? $json['vendor_id'] : '',
            'rating' => !empty($json['rating']) ? $json['rating'] : '',
            'comments' => !empty($json['comments']) ? $json['comments'] : '',
            'inserted_on' => date('Y-m-d H:i:s'),
        );
        $customerId = $this->Database->insert('user_feedback', $insert);
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("successfully_Sent")))));
    }

    public function party_order_details() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $homelyfood_id = !empty($json['homelyfood_id']) ? $json['homelyfood_id'] : '';
        $occasion = GetOccasion();
        foreach ($occasion as $occ) {
            $occ_data[] = array('occasion_id' => $occ->id,
                'occasion_name' => $occ->occasion_name);
        }
        $cuisine = GetCuisineByRestaurant($homelyfood_id);
        foreach ($cuisine as $cus) {
            $cuisine_data[] = array('cuisine_id' => $cus->cuisine_id,
                'cuisine_name' => $cus->cuisine_name);
        }
        $result['occasion'] = $occ_data;
        $result['cuisine'] = $cuisine_data;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
    }

    public function party_order_request() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $homelyfood_id = !empty($json['homelyfood_id']) ? $json['homelyfood_id'] : '';
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $insert = array(
            'user_id' => $user_id,
            'vendor_id' => $homelyfood_id,
            'occasion_id' => !empty($json['occasion_id']) ? $json['occasion_id'] : '',
            'party_date' => !empty($json['party_date']) ? $json['party_date'] : '',
            'party_time' => !empty($json['party_time']) ? $json['party_time'] : '',
            'no_of_adult' => !empty($json['adult']) ? $json['adult'] : '',
            'no_of_children' => !empty($json['children']) ? $json['children'] : '',
            'party_venue' => !empty($json['party_venue']) ? $json['party_venue'] : '',
            'latitude' => !empty($json['latitude']) ? $json['latitude'] : '',
            'longitude' => !empty($json['longitude']) ? $json['longitude'] : '',
            'cuisine_id' => !empty($json['cuisine_id']) ? implode(',', $json['cuisine_id']) : '',
        );

        $Result = $this->Database->insert('partyorder_request', $insert);

        $CondArray = array('id' => $Result);
        $orderno = 'PO-' . date('dm') . rand(10, 100) . $Result;
        $update_array = array('party_orderno' => $orderno);
        $result = $this->Database->update('partyorder_request', $update_array, $CondArray);

        $vendor_array['sender_id'] = $user_id;
        $vendor_array['receiver_id'] = $homelyfood_id;
        $vendor_array['notification_type'] = PARTY_ORDER;
        $vendor_array['notification_message'] = 'New Party Order Received';
        $vendor_array['redirect_url'] = 'vendor/party_order';
        $vendor_array['party_order_id'] = $Result;

        $this->Database->insert('notification', $vendor_array);
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")))));
    }

    public function my_party_orders() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $UserId = !empty($json['user_id']) ? $json['user_id'] : '';
        $UserArray = GetCustomerArrayById($UserId);
        if (empty($UserArray)) {
            die(json_encode(array('response' => array('status' => false, 'message' => $this->lang->line("Invalid_user_id")))));
        }
        $OrderArray = GetPartyOrderByUserId($UserId);
        for ($i = 0; $i < count($OrderArray); $i++) {
            $d = $OrderArray[$i];
            $rest_name = GetRestaurantById($d->vendor_id);

            $party_order[] = array(
                'party_order_id' => $d->id,
                'user_id' => $d->user_id,
                'homelyfood_id' => $d->vendor_id,
                'restaurant_name' => $rest_name[0]->restaurant_name,
                'orderno' => $d->party_orderno,
                'order_date' => $d->inserted_on,
                'price' => $d->price
            );
        }
        $result['party_order'] = $party_order;
        die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $result)));
    }

    public function partyorder_invoice() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $UserId = !empty($json['user_id']) ? $json['user_id'] : '';
        $party_order_id = !empty($json['party_order_id']) ? $json['party_order_id'] : '';
        $UserArray = GetCustomerArrayById($UserId);
        if (empty($UserArray)) {
            die(json_encode(array('response' => array('status' => false, 'message' => $this->lang->line("Invalid_user_id")))));
        }
        $OrderArray = GetPartyOrderById($party_order_id);

        $dat['occasion'] = GetNameById($OrderArray[0]->occasion_id, 'occasion', 'occasion_name');
        $dat['party_date'] = date('D, d M-Y', strtotime($OrderArray[0]->party_date));
        $dat['party_time'] = date('h:i A', strtotime($OrderArray[0]->party_time));
        $dat['no_of_guests'] = array('adult' => $OrderArray[0]->no_of_adult, 'children' => $OrderArray[0]->no_of_children);
        $dat['party_venue'] = $OrderArray[0]->party_venue;
        $cuisine = explode(',', $OrderArray[0]->cuisine_id);
        foreach ($cuisine as $cus) {
            $Qry = "SELECT * FROM `cuisine` WHERE cuisine_id='$cus' ";
            $CuisineArray = $this->Database->select_qry_array($Qry);
            $cuisine_name[] = $CuisineArray[0]->cuisine_name;
        }
        $dat['cuisines'] = implode(',', $cuisine_name);

        $result['order_details'] = $dat;

        $data['sub_total'] = DecimalAmount($OrderArray[0]->price);
        $data['tax'] = '0.00';
        $data['delivery_charge'] = '0.00';
        $data['total_amount'] = DecimalAmount($OrderArray[0]->price);
        if ($OrderArray[0]->price != 0) {
            $result['payment_details'] = $data;
        }
        // else{
        //     $result['payment_details'] = [];
        // }
        die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $result)));
    }

    public function partyorder_payment() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $UserId = !empty($json['user_id']) ? $json['user_id'] : '';
        $party_order_id = !empty($json['party_order_id']) ? $json['party_order_id'] : '';
        $updatedata = array('payment_status' => 2, 'mode_of_payment' => 3);
        $where = array('id' => $party_order_id);
        $this->Database->update('partyorder_request', $updatedata, $where);
        die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("success")))));
    }

    public function offers() {
        $Qry = "SELECT users.id,users.image,restaurant_details.restaurant_name,GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ', ')AS cuisine_name,promo_code.menu_id,promo_code.restaurant_id,promo_code.discount,promo_code.title 
            FROM promo_code LEFT JOIN restaurant_details ON restaurant_details.vendor_id=promo_code.offer_addedby 
            LEFT JOIN users ON users.id=restaurant_details.vendor_id 
            LEFT JOIN restaurant_cuisine_type as rct on rct.vendor_id=users.id 
            LEFT JOIN cuisine on cuisine.cuisine_id=rct.cuisine_id 
            WHERE promo_code.user_type=2
            AND promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE()
            GROUP BY users.id";

        $Array = $this->Database->select_qry_array($Qry);

        $Qry1 = "SELECT users.id,users.image,restaurant_details.restaurant_name,GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ', ')AS cuisine_name,promo_code.menu_id,promo_code.restaurant_id,promo_code.discount,promo_code.title 
            FROM promo_code LEFT JOIN restaurant_details ON restaurant_details.vendor_id=promo_code.restaurant_id 
            LEFT JOIN users ON users.id=restaurant_details.vendor_id 
            LEFT JOIN restaurant_cuisine_type as rct on rct.vendor_id=users.id 
            LEFT JOIN cuisine on cuisine.cuisine_id=rct.cuisine_id 
            WHERE promo_code.user_type=1
            AND promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE()
            GROUP BY users.id";
        $Array1 = $this->Database->select_qry_array($Qry1);

        $Arrays = array_merge($Array, $Array1);
        //print_r($Arrays);die();
        foreach ($Arrays as $dat) {
            if (!empty($dat->menu_id) && empty($dat->restaurant_id)) {
                $sql = "SELECT id,vendor_id,menu_name,description,price,image FROM menu_list WHERE id IN ($dat->menu_id) GROUP BY id ORDER BY id DESC";
                $Array1 = $this->Database->select_qry_array($sql);
            } else {
                $sql = "SELECT users.id as vendor_id,users.image as image FROM restaurant_details LEFT JOIN users ON users.id=restaurant_details.id WHERE restaurant_details.id IN ($dat->restaurant_id) GROUP BY restaurant_details.id ORDER BY restaurant_details.id DESC";
                $Array1 = $this->Database->select_qry_array($sql);
            }
            foreach ($Array1 as $d) {
                if ($d->image != '') {
                    $image = base_url('uploads/vendor_images/' . $d->image);
                } else {
                    $image = base_url(DEFAULT_LOGO_RESTAURANT);
                }
                if (isset($d->price)) {
                    if ($d->price != 0) {
                        $offer = $d->price * $dat->discount / 100;
                        $discount = $d->price - $offer;
                        $price = DecimalAmount($d->price);
                        $discount_percentage = $dat->discount;
                    } else {
                        $price = '0';
                        $discount_percentage = $dat->discount;
                    }
                } else {
                    $price = '0';
                    $discount_percentage = $dat->discount;
                }
                $array[] = array(
                    'homelyfood_id' => $d->vendor_id,
                    'homelyfood_name' => $dat->restaurant_name,
                    'menu_id' => !empty($d->id) ? $d->id : '',
                    'menu_name' => !empty($d->menu_name) ? $d->menu_name : '',
                    'description' => !empty($d->description) ? $d->description : '',
                    'deal_title' => $dat->title,
                    'price' => $price,
                    'discount_percentage' => !empty($discount_percentage) ? DecimalAmount($discount_percentage) : '',
                    'image_url' => $image,
                );
            }
            //  }
            //         else{
            //             $sql = "SELECT * FROM restaurant_details WHERE id IN ($dat->restaurant_id) GROUP BY id ORDER BY id DESC";
            //   $Array1 = $this->Database->select_qry_array($sql);
            //             $array[] = array(
            //                                 'homelyfood_id' => $d->vendor_id,
            //                                 'homelyfood_name'=> $dat->restaurant_name,
            //                                 'menu_id' => '',
            //                                 'menu_name' => '',
            //                                 'description' => '',
            //                                 'price' => '0',
            //                                 'discount_percentage' => '',
            //                                 'image_url' => $image,
            //                                 );
            //         }
        }
        die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $array)));
    }

    //vendor
    public function vendor_orders() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $UserArray = GetCustomerArrayById($VendorId);
        if (empty($UserArray)) {
            die(json_encode(array('response' => array('status' => false, 'message' => $this->lang->line("Invalid_vendor_id")))));
        }
        $order_status = '4';
        $OrderArray = GetVendorOrderList($VendorId, $order_status);
        if (!empty($OrderArray)) {
            for ($i = 0; $i < count($OrderArray); $i++) {
                $d = $OrderArray[$i];
                // $d->MedArray = GetSingleMediaVideo($d->product_id);
                $d->StatusName = GetOrderStatusName($d->order_status);
                unset($d->apiJson);
            }
            die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => array('orders' => $OrderArray))));
        } else {
            die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => array('orders' => $OrderArray))));
        }
    }

    public function vendor_order_detailsXXX() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $OrderId = !empty($json['order_id']) ? $json['order_id'] : '';
        $UserArray = GetCustomerArrayById($VendorId);
        if (empty($UserArray)) {
            die(json_encode(array('response' => array('status' => false, 'message' => $this->lang->line("Invalid_vendor_id")))));
        }
        $order_status = '0';
        $OrderArray = GetVendorOrderList($VendorId, $order_status);

        if (!empty($OrderArray)) {
            for ($i = 0; $i < count($OrderArray); $i++) {
                $d = $OrderArray[$i];
                // $d->MedArray = GetSingleMediaVideo($d->product_id);
                $d->StatusName = GetOrderStatusName($d->order_status);
                unset($d->apiJson);
            }
            die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => array('orders' => $OrderArray))));
        } else {
            die(json_encode(array('response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => array('orders' => $OrderArray))));
        }
    }

    public function rider_active_status() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $userArrayd = GetusersById($VendorId);
        if (!empty($userArrayd->archive)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => 'Your account is inactive.'))));
        } else {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => true, 'message' => 'Active'))));
        }
    }

    public function vendor_completed_orders() {
        $json = file_get_contents('php://input');
        // send_mail('upendra@alwafaagroup.com', 'vendor_completed_orders', $json);
        $json = json_decode($json, TRUE);
        $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $offset = !empty($json['offset']) ? $json['offset'] : '';
        $UserArray = GetCustomerArrayById($VendorId);
        if (empty($UserArray)) {
            die(json_encode(array('response' => array('status' => false, 'message' => $this->lang->line("Invalid_vendor_id")))));
        }
        $userArrayd = GetusersById($VendorId);
        $_REQUEST['userAr'] = $userArrayd;
        $_REQUEST['customerAr'] = $UserArray;

        $restuRent = GetRestaurantById($VendorId);
        $restuRent = !empty($restuRent) ? $restuRent[0] : '';

        if (!empty($userArrayd->archive)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => 'Your account is inactive.'))));
        }


        if ($userArrayd->user_type == '4') {

            $_REQUEST['viewType'] = 'completed';
            $_REQUEST['deliveryboy_added_by'] = $userArrayd->deliveryboy_added_by;
            $OrderArray = GetVendorOrderListForRider($VendorId, $offset);
        } else {
            $OrderArray = GetVendorCompletedOrderList($VendorId, $order_status);
            for ($i = 0; $i < count($OrderArray); $i++) {
                $d = $OrderArray[$i];
                unset($d->apiJson);
            }
        }


        $total_ongoing = total_ongoing_order($VendorId);
        $total_completed = total_completed_order($VendorId);
        $total_new_order = total_new_order($VendorId);



        $result['total_neworder_count'] = !empty($total_new_order) ? $total_new_order[0]->TotalRows : 0;
        $result['total_ongoing_count'] = !empty($total_ongoing) ? $total_ongoing[0]->TotalRows : 0;
        $result['total_completed_count'] = !empty($total_completed) ? $total_completed[0]->TotalRows : 0;
        $result['order'] = $OrderArray;
        $result['online_status'] = $userArrayd->online_status;
        if ($userArrayd->user_type == '2') {
            $result['online_status'] = $restuRent->busy_status;
        }
        $result['notification_count'] = count(APINotifactionCount());

        die(json_encode(array("code" => $this->success, 'response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $result)));
    }

    //vendor
    public function vendor_new_orders() {
        $json = file_get_contents('php://input');
        // send_mail('upendra@alwafaagroup.com', 'vendor_new_orders', $json);
        $json = json_decode($json, TRUE);
        $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $offset = !empty($json['offset']) && is_numeric($json['offset']) ? $json['offset'] : 0;
        $UserArray = GetCustomerArrayById($VendorId);
        if (empty($UserArray)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => $this->lang->line("Invalid_vendor_id")))));
        }
        $restuRent = GetRestaurantById($VendorId);
        $restuRent = !empty($restuRent) ? $restuRent[0] : '';

        $userArrayd = GetusersById($VendorId);
        $_REQUEST['customerAr'] = $UserArray;
        $_REQUEST['userAr'] = $userArrayd;
        $order_status = '0';



        if (!empty($userArrayd->archive)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => 'Your account is inactive.'))));
        }
        if ($userArrayd->user_type == '4') {
            $_REQUEST['deliveryboy_added_by'] = $userArrayd->deliveryboy_added_by;
            $_REQUEST['viewType'] = 'new_orders';
            $OrderArray = GetVendorOrderListForRider($VendorId, $offset);
        } else {
            $OrderArray = GetVendorOrderList($VendorId, $order_status, $offset);
        }

        $total_ongoing = total_ongoing_order($VendorId);
        $total_completed = total_completed_order($VendorId);
        $total_new_order = total_new_order($VendorId);



        $result['total_neworder_count'] = !empty($total_new_order) ? $total_new_order[0]->TotalRows : 0;
        $result['total_ongoing_count'] = !empty($total_ongoing) ? $total_ongoing[0]->TotalRows : 0;
        $result['total_completed_count'] = !empty($total_completed) ? $total_completed[0]->TotalRows : 0;
        $result['order'] = $OrderArray;
        $result['notification_count'] = count(APINotifactionCount());
        $result['online_status'] = $userArrayd->online_status;
        if ($userArrayd->user_type == '2') {
            $result['online_status'] = $restuRent->busy_status;
        }
        if (!empty($OrderArray)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $result)));
        } else {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $result)));
        }
    }

    public function vendor_ongoing_orders() {
        $json = file_get_contents('php://input');
        // send_mail('upendra@alwafaagroup.com', 'vendor_ongoing_orders', $json);
        $json = json_decode($json, TRUE);
        $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $offset = !empty($json['offset']) && is_numeric($json['offset']) ? $json['offset'] : 0;
        $UserArray = GetCustomerArrayById($VendorId);
        if (empty($UserArray)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => $this->lang->line("Invalid_vendor_id")))));
        }
        $userArrayd = GetusersById($VendorId);
        $restuRent = GetRestaurantById($VendorId);
        if (!empty($userArrayd->archive)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => 'Your account is inactive.'))));
        }
        $restuRent = !empty($restuRent) ? $restuRent[0] : '';
        $_REQUEST['userAr'] = $userArrayd;
        $_REQUEST['customerAr'] = $UserArray;
        $order_status = '0';
        $OrderArray = GetVendorOngoingOrderList($VendorId, $offset);



        if ($userArrayd->user_type == '4') {
            $_REQUEST['deliveryboy_added_by'] = $userArrayd->deliveryboy_added_by;
            $_REQUEST['viewType'] = 'ongoing';
            $OrderArray = GetVendorOrderListForRider($VendorId, $offset);
        }


        $total_completed = total_completed_order($VendorId);
        $total_ongoing = total_ongoing_order($VendorId);
        $total_new = total_new_order($VendorId);

        $result['total_neworder_count'] = !empty($total_new) ? $total_new[0]->TotalRows : 0;
        $result['total_ongoing_count'] = !empty($total_ongoing) ? $total_ongoing[0]->TotalRows : 0;
        $result['total_completed_count'] = !empty($total_completed) ? $total_completed[0]->TotalRows : 0;
        $result['order'] = $OrderArray;
        $result['notification_count'] = count(APINotifactionCount());
        $result['online_status'] = $userArrayd->online_status;
        if ($userArrayd->user_type == '2') {
            $result['online_status'] = $restuRent->busy_status;
        }
        if (!empty($OrderArray)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $result)));
        } else {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $result)));
        }
    }

    public function vendor_order_details() {
        $json = file_get_contents('php://input');
        //send_mail('upendra@alwafaagroup.com', 'vendor_order_details', $json);
        $json = json_decode($json, TRUE);
        $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $OrderId = !empty($json['order_id']) ? $json['order_id'] : '';
        $UserArray = GetCustomerArrayById($VendorId);



        $userArrayd = GetusersById($VendorId);
        if (empty($UserArray)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => $this->lang->line("Invalid_vendor_id")))));
        }

        $store_type = GetStoreType($VendorId);

        $OrderArray = GetVendorOrderDetails($VendorId, $OrderId);

        $order566 = GetOrderByOrderId($OrderId);
        $order566 = $order566[0];

        $uArray = GetUserDetails($order566->vendor_id);
        $uArray = !empty($uArray) ? $uArray[0] : '';

        $restuRent = GetRestaurantById($order566->vendor_id);
        $restuRent = !empty($restuRent) ? $restuRent[0] : '';



        $customer = GetUserDetails($order566->user_id);
        $customer = !empty($customer) ? $customer[0] : '';



        $OrderArray->is_self_pickup = $order566->delivery_type == '2' ? true : false;
        if (!empty($OrderArray->is_self_pickup)) {
            $OrderArray->customer_mobile_number = '+' . $customer->mobile_code . '' . $customer->mobile_number;
        }
        $OrderArray->scheduled_delivery_date = '';
        if ($order566->store_type != '1') {
            $OrderArray->scheduled_delivery_date = $order566->scheduled_delivery_date != '0000-00-00' ? date('Y-m-d', strtotime($order566->scheduled_delivery_date)) . ' ' . date('h:i A', strtotime($order566->scheduled_delivery_time)) : '';
        }

        if (!empty($OrderArray->scheduled_delivery_date) && $OrderArray->scheduled_deliveryend_time != '00:00:00') {
            $OrderArray->scheduled_delivery_date = $OrderArray->scheduled_delivery_date . ' - ' . date('h:i A', strtotime($OrderArray->scheduled_deliveryend_time));
        }
        unset($OrderArray->scheduled_deliveryend_time);



        $OrderArray->currency_code = 'PKR';
        $OrderArray->isRiderAccept = false;
        if ($OrderArray->order_status == 0) {
            $OrderArray->next_status_id = (string) 1;
            $OrderArray->next_status_name = 'Order Confirmed';
        } else {


            if ($userArrayd->user_type == '4' && $userArrayd->deliveryboy_added_by == '1' && empty($order566->assign_rider)) {
                $OrderArray->isRiderAccept = true;
                //  $OrderArray->next_status_name = 'Accept & On the way';
            } else if ($OrderArray->order_status == 1) {
                $OrderArray->next_status_id = (string) 2;
                $OrderArray->next_status_name = 'Preparing Order';
            } elseif ($OrderArray->order_status == 2) {

                if ($order566->delivery_type == 2) {
                    $OrderArray->next_status_id = (string) 8;
                    $OrderArray->next_status_name = 'Ready for pickup';
                } else {
                    $OrderArray->next_status_id = (string) 14;
                    $OrderArray->next_status_name = 'Ready for delivery';
                }
            } elseif ($OrderArray->order_status == 14) {
                $OrderArray->next_status_id = (string) 5;
                $OrderArray->next_status_name = 'Order on the way';
            } elseif ($OrderArray->order_status == 5 || $OrderArray->order_status == 8) {
                $OrderArray->next_status_id = (string) 6;
                $OrderArray->next_status_name = 'Order Delivered';
            }
        }
        if ($OrderArray->order_status == 7) {
            $OrderArray->next_status_id = 0;
            $OrderArray->next_status_name = '';
            $OrderArray->isRiderAccept = false;
        }



        $OrderArray->is_clickable = false;
        if (!empty($OrderArray->next_status_id)) {
            if ($userArrayd->user_type == '4') {
                //  if ($userArrayd->deliveryboy_added_by == '1') {
                $allwordRiderstaus = [5, 6, 14];
                if (in_array($OrderArray->next_status_id, $allwordRiderstaus)) {
                    $OrderArray->is_clickable = true;
                }
                // }
                if ($restuRent->isDelivery == '1') {
                    $OrderArray->is_clickable = true;
                }
            } else if ($userArrayd->user_type == '2') {
                $allwordVendorstaus = [0, 1, 2, 6, 14, 8];
                if (empty($restuRent->isDelivery)) {
                    if (in_array($OrderArray->next_status_id, $allwordVendorstaus)) {
                        $OrderArray->is_clickable = true;
                    }
                } else if ($restuRent->isDelivery == '1') {
                    $OrderArray->is_clickable = true;
                }
            }
        }
        if (!empty($OrderArray->isRiderAccept)) {
            $OrderArray->is_clickable = true;
        }


        $history = GetorderstatushistoryBy($OrderId);
        $history1 = json_decode(json_encode($history), true);
        $order_history = [];

        foreach ($history1 as $key => $item) {

            $order_history[] = array(
                'statusId' => $item['status_id'],
                'statusName' => $item['status_name'],
                'status' => 1,
            );
        }

        $product = GetOrderDetailsByOrderIdApi($OrderId);
        for ($i = 0; $i < count($product); $i++) {
            $ord = $product[$i];
            $prdSubTotal = $ord->ord_del_price * $ord->quantity;
            $return[$i] = array(
                'product_name' => $ord->product_name,
                'quantity' => $ord->quantity,
                'price' => DecimalAmount($prdSubTotal),
                'product_image' => !empty($ord->image) ? base_url() . 'uploads/menu/' . $ord->image : base_url(DEFAULT_LOGO_MENU),
            );
            $size = explode(',', $ord->size);
            $addon = explode(',', $ord->add_on);
            $topping = explode(',', $ord->topping);
            $drink = explode(',', $ord->drink);
            $dip = explode(',', $ord->dip);
            $side = explode(',', $ord->side);
            if (isset($size) && $size[0] != '') {
                foreach ($size as $s) {
                    $size_qry = "SELECT menu_size FROM `menu_sizes` WHERE `menu_size_id`=" . $s;
                    $size_data = $this->Database->select_qry_array($size_qry);
                    $return[$i]['size'][] = $size_data[0]->menu_size;
                }
            }
            if (isset($addon) && $addon[0] != '') {
                foreach ($addon as $a) {
                    $addon_qry = "SELECT add_on FROM `menu_addons` WHERE `menu_addon_id`=" . $a;
                    $addon_data = $this->Database->select_qry_array($addon_qry);
                    $return[$i]['addon'][] = $addon_data[0]->add_on;
                }
            }
            if (isset($topping) && $topping[0] != '') {
                foreach ($topping as $t) {
                    $topping_qry = "SELECT topping FROM `menu_topping` WHERE `menu_topping_id`=" . $t;
                    $topping_data = $this->Database->select_qry_array($topping_qry);
                    $return[$i]['topping'][] = $topping_data[0]->topping;
                }
            }
            //   if(isset($drink) && $drink[0]!='')
            //   {
            //       foreach($drink as $d)
            //       {
            //           $drink_qry = "SELECT drink_name FROM `menu_drink` WHERE `menu_drink_id`=".$d;
            //           $drink_data = $this->Database->select_qry_array($drink_qry);
            //           $return['product_details'][$i]['options'][] = $drink_data[0]->drink_name;
            //       }
            //   }
            if (isset($dip) && $dip[0] != '') {
                foreach ($dip as $di) {
                    $dip_qry = "SELECT dips FROM `menu_dips` WHERE `menu_dip_id`=" . $di;
                    $dip_data = $this->Database->select_qry_array($dip_qry);
                    $return[$i]['dip'][] = $dip_data[0]->dips;
                }
            }
            if (isset($side) && $side[0] != '') {
                foreach ($side as $s) {
                    $side_qry = "SELECT side_dish FROM `menu_side` WHERE `menu_side_id`=" . $s;
                    $side_data = $this->Database->select_qry_array($side_qry);
                    $return[$i]['side'][] = $side_data[0]->side_dish;
                }
            }
        }
        $address = GetOrderAddressById($OrderArray->delivery_address);

        if ($address[0]->address_label == 1) {
            $label = 'Apartment';
        } else if ($address[0]->address_label == 2) {
            $label = 'House';
        } else if ($address[0]->address_label == 2) {
            $label = 'Office';
        } else {
            $label = '';
        }
        $pick_address = array(
            'address' => !empty($uArray->area) ? $uArray->area : '',
            'latitude' => !empty($uArray->latitude) ? $uArray->latitude : '',
            'longitude' => !empty($uArray->longitude) ? $uArray->longitude : '',
            'mobile_no' => !empty($uArray->mobile_number) ? $uArray->mobile_code . $uArray->mobile_number : '',
        );
        $delivery_address = array(
            'address_id' => !empty($address[0]->id) ? $address[0]->id : '',
            'address_label' => !empty($address[0]->address_label) ? $address[0]->address_label : '',
            'address_label_name' => $label,
            'address' => !empty($address[0]->address) ? $address[0]->address : '',
            'mobile_code' => !empty($address[0]->mobile_code) ? $address[0]->mobile_code : '',
            'mobile_number' => !empty($address[0]->mobile_number) ? $address[0]->mobile_number : '',
            'latitude' => !empty($address[0]->latitude) ? $address[0]->latitude : '',
            'longitude' => !empty($address[0]->longitude) ? $address[0]->longitude : '',
            'street' => !empty($address[0]->street) ? $address[0]->street : '',
            'building' => !empty($address[0]->building) ? $address[0]->building : '',
            'office' => !empty($address[0]->office) ? $address[0]->office : '',
            'house' => !empty($address[0]->house) ? $address[0]->house : '',
            'apartment_no' => !empty($address[0]->apartment_no) ? $address[0]->apartment_no : '',
            'floor' => !empty($address[0]->floor) ? $address[0]->floor : '',
        );
        $OrderArray->product_details = $return;
        $OrderArray->delivery_address = $delivery_address;
        $OrderArray->order_history = $order_history;
        $OrderArray->pickup_address = $pick_address;
        $OrderArray->invoice_pdf_url = base_url('login/invoice?orderId=' . base64_encode($OrderId));
        if (!empty($OrderArray)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => array('orders' => $OrderArray))));
        } else {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => array('orders' => $OrderArray))));
        }
    }

    public function party_order_list() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $offset = !empty($json['offset']) && is_numeric($json['offset']) ? $json['offset'] : 0;

        $select = ",CONCAT(U.name) AS user_name,CONCAT(U.mobile_code,U.mobile_number) AS user_phone";
        $join = "LEFT JOIN users U ON U.id=PO.user_id";
        $qry = "SELECT PO.id,PO.party_orderno,PO.date,PO.order_status $select FROM `party_order` PO $join WHERE PO.`vendor_id`='$VendorId' ORDER BY date DESC LIMIT $offset,10";
        $data = $this->Database->select_qry_array($qry);
        for ($i = 0; $i < count($data); $i++) {
            $d = $data[$i];
            $d->time = date('H:i', strtotime($d->date));
            $d->date = date('d-m-Y', strtotime($d->date));
            $d->order_status_name = GetPartyOrderStatusName($d->order_status);
        }
        $calRow = "SELECT FOUND_ROWS() AS TotalRows";
        $calRowArray = $this->Database->select_qry_array($calRow);

        $result['total_count'] = !empty($calRowArray) ? $calRowArray[0]->TotalRows : 0;
        $result['list'] = $data;
        die(json_encode(array("code" => $this->success, 'response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $result)));
    }

    public function party_order_detail() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $orderId = !empty($json['order_id']) ? $json['order_id'] : '';
        $data = GetpartyorderBy($orderId);
        $data->order_status_name = GetPartyOrderStatusName($data->order_status);
        $data->products = GetpartyorderdetailsBy($orderId);
        $his = GetpartyorderstatushistoryBy($orderId);
        $order_history = [];
        for ($i = 0; $i < count($his); $i++) {
            $d = $his[$i];
            $order_history[] = array(
                'statusName' => GetPartyOrderStatusName($d->status_id),
                'statusId' => $d->status_id,
                'status' => '1',
            );
        }
        $data->party_date = date('Y-m-d', strtotime($data->party_date));
        $data->party_time = date('h:i A', strtotime($data->party_time));
        $data->party_time_end = date('h:i A', strtotime($data->party_time_end));


        $data->is_new = !empty($data->order_status) ? false : true;
        $data->next_status_id = 0;
        $data->next_status_name = '';
        $data->currency_code = 'PKR';
        if (empty($data->order_status)) {
            $data->next_status_id = '1';
            $data->next_status_name = 'Order Confirm';
        } else if ($data->order_status == '1') {
            $data->next_status_id = '2';
            $data->next_status_name = 'Party Finish';
        }
        $data->order_history = $order_history;

        $data->invoice = !empty($data->invoice) ? $data->invoice : '';
        $data->invoice_no = !empty($data->invoice_no) ? $data->invoice_no : '';
        $data->request_json = !empty($data->request_json) ? $data->request_json : '';


        $data->time = date('H:i', strtotime($data->date));
        $data->date = date('d-m-Y', strtotime($data->date));


        $result['orders'] = $data;
        die(json_encode(array("code" => $this->success, 'response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $result)));
    }

    public function change_party_order_status() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);

        $order_id = !empty($json['order_id']) ? $json['order_id'] : '';
        $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $status_id = !empty($json['status_id']) ? $json['status_id'] : '';

        $UserArray = GetCustomerArrayById($VendorId);
        if (empty($UserArray)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => $this->lang->line("Invalid_vendor_id")))));
        }

        $orderUp['order_status'] = $status_id;
        $this->Database->update('party_order', $orderUp, array('id' => $order_id));


        $updatehis['order_id'] = $order_id;
        $updatehis['status_id'] = $status_id;
        $updatehis['isPartyOrder'] = '1';
        $updatehis['notes'] = "api name change_party_order_status, status Id-$status_id, order Id-$order_id";
        if (canUpdateorderStatus($updatehis['order_id'], $updatehis['status_id'])) {
            $this->Database->insert('order_status_history', $updatehis);
        }
        // SendnotificationorderStatus($order_id);
        //$result = [];

        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("Update_successfully")))));
    }

    public function notifaction_list_vendor() {
        $json = file_get_contents('php://input');
        //send_mail('upendra@alwafaagroup.com', 'response', $json);
        $json = json_decode($json, TRUE);
        $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $UserArray = GetCustomerArrayById($VendorId);

        $datebefore3min = date("Y-m-d H:i:s", strtotime('-3 minutes', strtotime(date('Y-m-d H:i:s'))));
        $cond = '';
        $isRead = '';
        if ($UserArray['user_type'] == '2') {
            $cond = " AND `vendor_id` = '$VendorId' ";
            $isRead = 'is_read';
        } else if ($UserArray['user_type'] == '4') {
            $cond = " AND `assign_rider` = '$VendorId' ";
            $isRead = 'is_read_rider AS is_read';
        }


        $sql = '';
        $sql = $sql . "SELECT id AS order_id,CONCAT(party_orderno,' New order received.') AS title,date,$isRead,'1' AS type  FROM `party_order` WHERE 1 AND archive=0  $cond UNION ALL ";

        $sql = $sql . "SELECT id AS order_id,CONCAT(orderno,' New order received.') AS title,date,$isRead,'2' AS type  FROM `orders` WHERE 1 AND archive=0  AND date < '$datebefore3min' $cond UNION ALL ";

        $sql = substr($sql, 0, -11);
        $select = "MN.order_id,MN.title,MN.date,MN.is_read,MN.type";
        $join = " ";
        $mainSql = "SELECT $select FROM ($sql) MN $join ORDER BY MN.date DESC";
        $nArray = $this->Database->select_qry_array($mainSql);


        for ($i = 0; $i < count($nArray); $i++) {
            $d = $nArray[$i];
            $d->time = date('H:i', strtotime($d->date));
            $d->date = date('d-m-Y', strtotime($d->date));
        }
        $result['list'] = $nArray;
        die(json_encode(array("code" => $this->success, 'response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $result)));
    }

    public function notifaction_list_vendor_read() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $id = !empty($json['order_id']) ? $json['order_id'] : '';
        $type = !empty($json['type']) ? $json['type'] : '';
        $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $UserArray = GetCustomerArrayById($VendorId);
        if (empty($UserArray)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => $this->lang->line("Invalid_vendor_id")))));
        }

        $isRead = '';
        if ($UserArray['user_type'] == '2') {
            $isRead = 'is_read';
        } else if ($UserArray['user_type'] == '4') {
            $isRead = 'is_read_rider';
        }


        if ($type == '1') {
            $orderUp[$isRead] = '1';
            $this->Database->update('party_order', $orderUp, array('id' => $id));
        } else if ($type == '2') {
            $orderUp[$isRead] = '1';
            $this->Database->update('orders', $orderUp, array('id' => $id));
        }
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("Update_successfully")))));
    }

    public function rider_accept_order() {
        $json = file_get_contents('php://input');
        //  send_mail('upendra@alwafaagroup.com', 'response', $json);
        $json = json_decode($json, TRUE);
        $order_id = !empty($json['order_id']) ? $json['order_id'] : '';
        $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $UserArray = GetCustomerArrayById($VendorId);
        $cusArray = GetusersById($VendorId);
        if (empty($UserArray)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => $this->lang->line("Invalid_vendor_id")))));
        }


        if (!empty($cusArray->archive)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => 'Your account is inactive'))));
        }
        if (!empty($cusArray->status)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => 'Your account is inactive'))));
        }

        if ($cusArray->user_type == '4' && $cusArray->deliveryboy_added_by == '1') {
            if (!empty($order->assign_rider) && $order->assign_rider != $VendorId) {
                die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => $this->lang->line("Already_booked_by_some_other_one")))));
            }
            $orderUp['assign_rider'] = $VendorId;
            $this->Database->update('orders', $orderUp, array('id' => $order_id));
            updateRidercommissionByOrder($order_id);
        }
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("Update_successfully")))));
    }

    public function vendor_online_status_update() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $status = !empty($json['status']) ? $json['status'] : '';
        $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $orderUp['online_status'] = $status;
        if (empty($status)) {
            $cond['vendor_id'] = $VendorId;
            $cond['end_time'] = '0000-00-00 00-00-00';
            $order44Up['end_time'] = date('Y-m-d H:i:s');
            $this->Database->update('vendor_online_history', $order44Up, $cond);
        } else {

            $cond['vendor_id'] = $VendorId;
            $cond['end_time'] = '0000-00-00 00-00-00';
            $order44Up['end_time'] = date('Y-m-d H:i:s');
            $this->Database->update('vendor_online_history', $order44Up, $cond);



            $inserOnline['vendor_id'] = $VendorId;
            $inserOnline['start_date'] = date('Y-m-d H:i:s');
            $inserOnline['end_time'] = '0000-00-00 00-00-00';
            $inserOnline['timestamp'] = $inserOnline['start_date'];
            $this->Database->insert('vendor_online_history', $inserOnline);
        }
        $this->Database->update('users', $orderUp, array('id' => $VendorId));
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("Update_successfully")))));
    }

    public function update_rider_live_location() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';

        $orderUp['liv_lat'] = !empty($json['latitude']) ? $json['latitude'] : '';
        $orderUp['liv_long'] = !empty($json['longitude']) ? $json['longitude'] : '';
        $orderUp['liv_address'] = !empty($json['address']) ? $json['address'] : '';
        $orderUp['lastupdate'] = date('Y-m-d H:i:s');

        $this->Database->update('users', $orderUp, array('id' => $VendorId));
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("Update_successfully")))));
    }

    public function vendor_busy_status_update() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $status = !empty($json['status']) ? $json['status'] : '';
        $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $orderUp['busy_status'] = $status;
        $this->Database->update('restaurant_details', $orderUp, array('vendor_id' => $VendorId));
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("Update_successfully")))));
    }

    public function get_rider_live_location() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $orderId = !empty($json['order_id']) ? $json['order_id'] : '';
        $order = GetOrderById($orderId);


        if (empty($order)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => $this->lang->line("Invalid_request_id")))));
        }
        $order = $order[0];

        if ($order->order_status != '5') {
            $status = GetOrderStatusName($order->order_status);
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => "$status"))));
        }
        $resturent = GetusersById($order->vendor_id);

        if ($order->delivery_type != '1') {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => 'Not allowed for self pickup.'))));
        }
        $rider = GetusersById($order->assign_rider);
        if (empty($rider)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => 'Rider not assign.'))));
        }
        $address = GetOrderAddressById($order->delivery_address);
        $address = !empty($address) ? $address[0] : '';

        $result['vendor_lat'] = !empty($resturent->latitude) ? $resturent->latitude : '';
        $result['vendor_long'] = !empty($resturent->longitude) ? $resturent->longitude : '';
        $result['vendor_address'] = !empty($resturent->area) ? $resturent->area : '';


        $result['user_lat'] = !empty($address->latitude) ? $address->latitude : '';
        $result['user_long'] = !empty($address->longitude) ? $address->longitude : '';
        $result['user_address'] = !empty($address->address) ? $address->address : '';
        $result['rider_lat'] = !empty($rider->liv_lat) ? $rider->liv_lat : '';
        $result['rider_long'] = !empty($rider->liv_long) ? $rider->liv_long : '';
        $result['rider_address'] = !empty($rider->liv_address) ? $rider->liv_address : '';
        $result['rider_lastupdate'] = !empty($rider->lastupdate) ? date('Y-m-d h:i A', strtotime($rider->lastupdate)) : '';
        die(json_encode(array("code" => $this->success, 'response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $result)));
    }

    public function about_us_content() {
        $data = $this->load->view(ADMIN_DIR . 'about_usAPI', false, true);
        $result['content'] = $data;
        die(json_encode(array("code" => $this->success, 'response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $result)));
    }

    public function change_order_status() {
        $json = file_get_contents('php://input');
        // send_mail('upendra@alwafaagroup.com', 'response', $json);
        $json = json_decode($json, TRUE);

        $order_id = !empty($json['order_id']) ? $json['order_id'] : '';
        $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $status_id = !empty($json['status_id']) ? $json['status_id'] : '';
        $order = GetOrderByOrderId($order_id);
        $order = !empty($order[0]) ? $order[0] : '';


        $UserArray = GetCustomerArrayById($VendorId);
        if (empty($UserArray)) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => $this->lang->line("Invalid_vendor_id")))));
        }
        $cusArray = GetusersById($VendorId);
        if ($cusArray->user_type == '4' && $cusArray->deliveryboy_added_by == '1') {
            if (!empty($order->assign_rider) && $order->assign_rider != $VendorId) {
                die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => $this->lang->line("Already_booked_by_some_other_one")))));
            }
            $orderUp['assign_rider'] = $VendorId;
        }

        $orderUp['order_status'] = $status_id;
        if ($orderUp['order_status'] == '7') {
            $orderUp['cancellation_msg'] = (!empty($json['cancellation_msg']) ? $json['cancellation_msg'] : '') . " (cancelled by user)";
            $orderUp['cancellation_reason'] = !empty($json['cancellation_reason']) ? $json['cancellation_reason'] : '';
        }
        $this->Database->update('orders', $orderUp, array('id' => $order_id));

        if (!empty($orderUp['assign_rider'])) {
            updateRidercommissionByOrder($order_id);
        }
        $updatehis['order_id'] = $order_id;
        $updatehis['status_id'] = $status_id;
        $updatehis['notes'] = "api name change_order_status, status Id-$status_id, order Id-$order_id";
        if ($status_id == '1') {
            SendpushNotifactionTorider($order_id);
        }
        if (canUpdateorderStatus($updatehis['order_id'], $updatehis['status_id'])) {
            $this->Database->insert('order_status_history', $updatehis);
        }
        // SendnotificationorderStatus($order_id);
        //$result = [];
        SendNotifactionOrderStatus($order_id);
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("Update_successfully")))));
    }

    public function apply_loyalty_points() {
        $json = file_get_contents('php://input');
        // send_mail('upendra@alwafaagroup.com', 'response', $json);
        $json = json_decode($json, TRUE);

        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $price_list = !empty($json['price_list']) ? $json['price_list'] : '';



        $address_id = !empty($price_list['address_id']) ? $price_list['address_id'] : '';
        $loyalty_points = !empty($json['loyalty_points']) ? $json['loyalty_points'] : '';
        $homelyfood_id = !empty($price_list['homelyfood_id']) ? $price_list['homelyfood_id'] : '';
        $order_type = !empty($price_list['order_type']) ? $price_list['order_type'] : '';

        $sub_total = !empty($price_list['sub_total']) ? $price_list['sub_total'] : '';




        $return = applyloyalityPoints($loyalty_points, $user_id, $sub_total); // this api using also for webiste
        $price_list['loyality_point_discount'] = $return['loyality_point_discount'];
        $price_list['loyality_discount'] = DecimalAmount($return['loyality_discount']);

        $returnVal['price_list'] = $price_list;
        die(json_encode(array("code" => 200, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $returnVal)));
    }

    /*     * ********Pickup/Delivery*********************************
      1 - Delivery, 2 - pickup
     */

    //  public function vendor_order_details()
    // {
    //     $json = file_get_contents('php://input');
    //     $json = json_decode($json, TRUE);
    //     $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
    //     $OrderId = !empty($json['order_id']) ? $json['order_id'] : '';
    //     $UserArray = GetCustomerArrayById($VendorId);
    //     if (empty($UserArray)) {
    //         die(json_encode(array("code" => $this->success,'response' => array('status' => false, 'message' => 'Invalid vendor id.'))));
    //     }
    //     $store_type = GetStoreType($VendorId);
    //     $OrderArray = GetVendorOrderDetails($VendorId,$OrderId);
    //     if($OrderArray->order_status==0)
    //     {
    //         $OrderArray->next_status_id = (string)1;
    //         $OrderArray->next_status_name = 'Order Confirmed';
    //     }else{
    //       if($OrderArray->delivery_type==1)
    //       {
    //         if($store_type[0]->store_type==1)
    //         {
    //             if($OrderArray->order_status==1)
    //             {
    //                 $OrderArray->next_status_id = (string)2;
    //                 $OrderArray->next_status_name = 'Preparing Order';
    //             }
    //             elseif($OrderArray->order_status==2)
    //             {
    //                 $OrderArray->next_status_id = (string)5;
    //                 $OrderArray->next_status_name = 'Order on the way';
    //             }elseif($OrderArray->order_status==5)
    //             {
    //                 $OrderArray->next_status_id = (string)6;
    //                 $OrderArray->next_status_name = 'Order Delivered';
    //             }
    //         }else{
    //             if($OrderArray->order_status==1)
    //             {
    //                 $OrderArray->next_status_id = (string)5;
    //                 $OrderArray->next_status_name = 'Order on the way';
    //             }elseif($OrderArray->order_status==5)
    //             {
    //             $OrderArray->next_status_id = (string)6;
    //             $OrderArray->next_status_name = 'Order Delivered';
    //             }
    //         }
    //       }else{
    //           if($OrderArray->order_status==1)
    //             {
    //                 $OrderArray->next_status_id = (string)4;
    //                 $OrderArray->next_status_name = 'Ready for pickup';
    //             }
    //             elseif($OrderArray->order_status==4)
    //             {
    //                 $OrderArray->next_status_id = (string)9;
    //                 $OrderArray->next_status_name = 'Order Completed';
    //             }
    //       }
    //     }
    //     $history = GetorderstatushistoryBy($OrderId);
    //     $history1 = json_decode(json_encode($history), true);
    //     $order_history = [];
    //     foreach($history1 as $key=>$item)
    //     {
    //         if($item['status_id']==1)
    //         {
    //             $order_history[$key]['statusId'] =(string)$item['status_id'];
    //             $order_history[$key]['statusName'] = 'Order Confirmed';
    //             $order_history[$key]['status'] = '1';
    //         }
    //         elseif($item['status_id']==2)
    //         {
    //             $order_history[$key]['statusId'] =(string)$item['status_id'];
    //             $order_history[$key]['statusName'] = 'Preparing Order';
    //             $order_history[$key]['status'] = '1';
    //         }
    //         elseif($item['status_id']==5)
    //         {
    //             $order_history[$key]['statusId'] =(string)$item['status_id'];
    //             $order_history[$key]['statusName'] = 'Order on the way';
    //             $order_history[$key]['status'] = '1';
    //         }
    //         elseif($item['status_id']==6)
    //         {
    //             $order_history[$key]['statusId'] =(string)$item['status_id'];
    //             $order_history[$key]['statusName'] = 'Order Delivered';
    //             $order_history[$key]['status'] = '1';
    //         }
    //         elseif($item['status_id']==4)
    //         {
    //             $order_history[$key]['statusId'] =(string)$item['status_id'];
    //             $order_history[$key]['statusName'] = 'Ready for pickup';
    //             $order_history[$key]['status'] = '1';
    //         }
    //         elseif($item['status_id']==9)
    //         {
    //             $order_history[$key]['statusId'] =(string)$item['status_id'];
    //             $order_history[$key]['statusName'] = 'Order Completed';
    //             $order_history[$key]['status'] = '1';
    //         }
    //     }
    //     $product = GetOrderDetailsByOrderIdApi($OrderId);
    //     for ($i=0; $i < count($product); $i++) { 
    //         $ord = $product[$i];
    //         $return[$i] = array(
    //             'product_name' =>$ord->product_name,
    //             'quantity' =>$ord->quantity,
    //             'price' =>$ord->subtotal,
    //             'product_image' =>!empty($ord->image)?base_url().'uploads/menu/'.$ord->image:base_url(DEFAULT_LOGO_MENU),
    //           );
    //       $size = explode(',',$ord->size);
    //       $addon = explode(',',$ord->add_on);
    //       $topping = explode(',',$ord->topping);
    //       $drink = explode(',',$ord->drink);
    //       $dip = explode(',',$ord->dip);
    //       $side = explode(',',$ord->side);
    //       if(isset($size) && $size[0]!='')
    //       {
    //           foreach($size as $s)
    //           {
    //               $size_qry = "SELECT menu_size FROM `menu_sizes` WHERE `menu_size_id`=".$s;
    //               $size_data = $this->Database->select_qry_array($size_qry);
    //               $return[$i]['size'][]=  $size_data[0]->menu_size;
    //           }
    //       }
    //       if(isset($addon) && $addon[0]!='')
    //       {
    //           foreach($addon as $a)
    //           {
    //               $addon_qry = "SELECT add_on FROM `menu_addons` WHERE `menu_addon_id`=".$a;
    //               $addon_data = $this->Database->select_qry_array($addon_qry);
    //               $return[$i]['addon'][] =  $addon_data[0]->add_on;
    //           }
    //       }
    //       if(isset($topping) && $topping[0]!='')
    //       {
    //           foreach($topping as $t)
    //           {
    //               $topping_qry = "SELECT topping FROM `menu_topping` WHERE `menu_topping_id`=".$t;
    //               $topping_data = $this->Database->select_qry_array($topping_qry);
    //               $return[$i]['topping'][] =  $topping_data[0]->topping;
    //           }
    //       }
    //     //   if(isset($drink) && $drink[0]!='')
    //     //   {
    //     //       foreach($drink as $d)
    //     //       {
    //     //           $drink_qry = "SELECT drink_name FROM `menu_drink` WHERE `menu_drink_id`=".$d;
    //     //           $drink_data = $this->Database->select_qry_array($drink_qry);
    //     //           $return['product_details'][$i]['options'][] = $drink_data[0]->drink_name;
    //     //       }
    //     //   }
    //       if(isset($dip) && $dip[0]!='')
    //       {
    //           foreach($dip as $di)
    //           {
    //               $dip_qry = "SELECT dips FROM `menu_dips` WHERE `menu_dip_id`=".$di;
    //               $dip_data = $this->Database->select_qry_array($dip_qry);
    //               $return[$i]['dip'][] = $dip_data[0]->dips;
    //           }
    //       }
    //       if(isset($side) && $side[0]!='')
    //       {
    //           foreach($side as $s)
    //           {
    //               $side_qry = "SELECT side_dish FROM `menu_side` WHERE `menu_side_id`=".$s;
    //               $side_data = $this->Database->select_qry_array($side_qry);
    //               $return[$i]['side'][] =  $side_data[0]->side_dish;
    //           }
    //       }
    //     }
    //     $address = GetOrderAddressById($OrderArray->delivery_address);
    //     if($address[0]->address_label == 1)
    //     {
    //         $label = 'Apartment';
    //     }else if($address[0]->address_label == 2)
    //     {
    //          $label = 'House';
    //     }else if($address[0]->address_label == 2){
    //         $label = 'Office';
    //     }else{
    //         $label = '';
    //     }
    //     $delivery_address = array(
    //         'address_id' => !empty($address[0]->id)?$address[0]->id:'',
    //         'address_label' => !empty($address[0]->address_label)?$address[0]->address_label:'',
    //         'address_label_name' => $label,
    //         'address' => !empty($address[0]->address)?$address[0]->address:'',
    //         'mobile_code' => !empty($address[0]->mobile_code)?$address[0]->mobile_code:'',
    //         'mobile_number' => !empty($address[0]->mobile_number)?$address[0]->mobile_number:'',
    //         'latitude' => !empty($address[0]->latitude)?$address[0]->latitude:'',
    //         'longitude'=>!empty($address[0]->longitude)?$address[0]->longitude:'',
    //         'street' => !empty($address[0]->street)?$address[0]->street:'',
    //         'building' => !empty($address[0]->building)?$address[0]->building:'',
    //         'office' => !empty($address[0]->office)?$address[0]->office:'',
    //         'house' => !empty($address[0]->house)?$address[0]->house:'',
    //         'apartment_no' => !empty($address[0]->apartment_no)?$address[0]->apartment_no:'',
    //         'floor' => !empty($address[0]->floor)?$address[0]->floor:'',
    //         );
    //     $OrderArray->product_details = $return;
    //     $OrderArray->delivery_address = $delivery_address;
    //     $OrderArray->order_history = $order_history;
    //     if(!empty($OrderArray))
    //     {
    //         die(json_encode(array("code" => $this->success,'response' => array('status' => true, 'message' => 'Success'), 'result' => array('orders' => $OrderArray))));
    //     }else{
    //         die(json_encode(array("code" => $this->success,'response' => array('status' => true, 'message' => 'Success'), 'result' => array('orders' => $OrderArray))));
    //     }
    //     }
    // public function delivery_boy_list()
    // {
    //     $json = file_get_contents('php://input');
    //     $json = json_decode($json, TRUE);
    //     $vendor_id = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
    //     $qry = "SELECT id as user_id,name as full_name,email as email_id FROM users WHERE user_type=4 AND deliveryboy_added_by='$vendor_id' AND archive=0 ORDER BY id DESC";
    //     $cArray = $this->Database->select_qry_array($qry);
    //     $result['user_details'] = empty($cArray) ? [] : $cArray;
    //     die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => "success"), 'result' => $result)));
    // }
    public function chat_list() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $sender_id = !empty($json['sender_id']) ? $json['sender_id'] : '';
        $receiver_id = !empty($json['receiver_id']) ? $json['receiver_id'] : '';
        $chat_id = !empty($json['chat_id']) ? $json['chat_id'] : '0';
        $autorefresh = !empty($json['autorefresh']) ? $json['autorefresh'] : '';

        $cond = "";
        if (!empty($autorefresh)) {
            //    $cond = "  AND sender_id !='$sender_id' AND receiver_id !='$sender_id'";
        }
        $selectMessage = ",IF(message_type=1,CONCAT('" . base_url('voice_chat/') . "',message),message) AS message";
        $basePath = base_url('files/profile_images/');
        $defaultImg = base_url(DEFAULT_IMG_USER);
        $select = ",CONCAT('$basePath',SEN.image) AS sender_img,CONCAT('$basePath',REC.image) AS receiver_img,'$defaultImg' AS DEFAULT_IMG";
        $join = " LEFT JOIN users SEN ON SEN.id=CH.sender_id LEFT JOIN users REC ON REC.id=CH.receiver_id";
        $qry = "SELECT CH.* $selectMessage $select FROM `chat_history` CH $join WHERE (CH.sender_id IN ($sender_id,$receiver_id) AND CH.receiver_id IN ($sender_id,$receiver_id)) AND chat_id > '$chat_id' $cond ORDER BY CH.timestamp ASC";
        $chat_list = $this->Database->select_qry_array($qry);
        $result['chat_list'] = $chat_list;
        $result['last_chat_id'] = !empty($chat_list) ? $chat_list[count($chat_list) - 1]->chat_id : '0';
        die(json_encode(array("response" => array("status" => true, "message" => "successfully"), 'result' => $result)));
    }

    public function send_message() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        // $json = $_POST;
        if (empty($this->devicetype)) {
            //$json = formateJsonAndriod($json);
        }
        $sender_id = !empty($json['sender_id']) ? $json['sender_id'] : '';
        $receiver_id = !empty($json['receiver_id']) ? $json['receiver_id'] : '';
        $message = !empty($json['message']) ? $json['message'] : '';
        $todays = date('Y-m-d H:i:s');
        $qry = "SELECT REC.device_id,CONCAT(SEN.name) AS sender_name,SEN.image AS image_url FROM `users` REC LEFT JOIN users SEN ON SEN.id='$sender_id' WHERE REC.id='$receiver_id'";
        $dlist = $this->Database->select_qry_array($qry);
        $dlist = !empty($dlist) ? $dlist[0] : [];



        $ProfileImg = 'files/profile_images/' . $dlist->image_url;
        if (!is_file(HOME_DIR . $ProfileImg)) {
            $ProfileImg = 'files/profile_images/default_profile_image.png';
        }
        $argument['receiver_image'] = base_url($ProfileImg);
        $argument['receiver_id'] = $sender_id;
        $argument['receiver_name'] = $dlist->sender_name;
        send_pushnotification([$dlist->device_id], $dlist->sender_name, $message, '3', $argument);
        $insert = array(
            'message' => $message,
            'sender_id' => $sender_id,
            'receiver_id' => $receiver_id,
            'message_type' => !empty($json['message_type']) ? $json['message_type'] : '',
            'timestamp' => $todays,
        );
        $insertId = $this->Database->insert('chat_history', $insert);
        $result['last_chat_id'] = $insertId;
        die(json_encode(array("response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
    }

    public function send_message_new() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $json = $_POST;
        if (empty($this->devicetype)) {
            //$json = formateJsonAndriod($json);
        }
        $sender_id = !empty($json['sender_id']) ? $json['sender_id'] : '';
        $receiver_id = !empty($json['receiver_id']) ? $json['receiver_id'] : '';
        $message = !empty($json['message']) ? $json['message'] : '';
        $message_type = !empty($json['message_type']) ? $json['message_type'] : '';
        $todays = date('Y-m-d H:i:s');
        $qry = "SELECT REC.device_id,CONCAT(SEN.name) AS sender_name,SEN.image AS image_url FROM `users` REC LEFT JOIN users SEN ON SEN.id='$sender_id' WHERE REC.id='$receiver_id'";
        $dlist = $this->Database->select_qry_array($qry);
        $dlist = !empty($dlist) ? $dlist[0] : [];



        if ($message_type == '1') {
            $UploadExcel = empty($_FILES['voice_chat']['error']) ? $_FILES['voice_chat'] : [];
            if (!empty($UploadExcel['name'])) {
                $message = uniqid() . '.' . pathinfo($UploadExcel['name'], PATHINFO_EXTENSION);
                $baseDir = HOME_DIR . "voice_chat/" . $message;
                if (!move_uploaded_file($UploadExcel['tmp_name'], $baseDir)) {
                    $message = 'INVALID MESSAGE';
                }
            }
        }




        $ProfileImg = 'files/profile_images/' . $dlist->image_url;
        if (!is_file(HOME_DIR . $ProfileImg)) {
            $ProfileImg = 'files/profile_images/default_profile_image.png';
        }
        $argument['receiver_image'] = base_url($ProfileImg);
        $argument['receiver_id'] = $sender_id;
        $argument['receiver_name'] = $dlist->sender_name;
        //  send_pushnotifaction($dlist->device_id, $dlist->sender_name, $message, '3', $argument);
        $insert = array(
            'message' => $message,
            'sender_id' => $sender_id,
            'receiver_id' => $receiver_id,
            'message_type' => !empty($json['message_type']) ? $json['message_type'] : '',
            'file_duration' => !empty($json['file_duration']) ? $json['file_duration'] : '',
            'timestamp' => $todays,
        );
        $insertId = $this->Database->insert('chat_history', $insert);
        $result['last_chat_id'] = $insertId;
        die(json_encode(array("response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
    }

    public function get_time_slot() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $date = !empty($json['date']) ? date('Y-m-d', strtotime($json['date'])) : '';
        $vendor_id = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $order_type = !empty($json['order_type']) ? $json['order_type'] : '';
        $rest_details = GetRestaurantById($id = $vendor_id);

        $todays = date('Y-m-d');
        if ($todays > $date) {
            die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => $this->lang->line("invalid_date")))));
        }

        $start = "07:00";
        $end = "23:00";

        if (!empty($rest_details)) {
            if ($rest_details[0]->delivery_hours_st != '00:00:00') {
                $start = date('H:i', strtotime($rest_details[0]->delivery_hours_st));
            }
            if ($rest_details[0]->delivery_hours_et != '00:00:00') {
                $end = date('H:i', strtotime($rest_details[0]->delivery_hours_et));
            }
        }
        if ($order_type == '1') {
            if ($rest_details[0]->opening_time != '00:00:00') {
                $start = date('H:i', strtotime($rest_details[0]->opening_time));
            }
            if ($rest_details[0]->closing_time != '00:00:00') {
                $end = date('H:i', strtotime($rest_details[0]->closing_time));
            }
        }

        $currentTime = date('H:i');
        $openingTime = date('H:i', strtotime($start));


        if ($date == $todays) {

            if ($currentTime > '23:00') {
                die(json_encode(array("code" => $this->success, 'response' => array('status' => false, 'message' => 'Please schedule your order for next day.'))));
            }
            if ($currentTime > $openingTime) {
                $start = date('H:00', strtotime($currentTime . '+1 hour')) . ":00";
            }
        }

        $tStart = strtotime($start);
        $tEnd = strtotime($end);
        $tNow = $tStart;
        $return = [];

        while ($tNow <= $tEnd) {
            $endTime = strtotime('+60 minutes', $tNow);
            $endTime = date("h:i A", $endTime);

            if (date("H:i", $tNow) >= '23:00' && date('H:i:s', strtotime($endTime)) > '00:00') {
                $endTime = date('h:i A', strtotime('23:59'));
            }

            $tfrom = date("h:i A", $tNow);
            $tend = date("h:i A", $tEnd);

            if ($tfrom != $tend) {
                $return[] = array(
                    'date' => date('d-m-Y', strtotime($date)),
                    'time_from' => date("h:i A", $tNow),
                    'time_to' => $endTime,
                );
            }
            $tNow = strtotime('+60 minutes', $tNow);
        }
        $result['times'] = $return;
        die(json_encode(array("code" => $this->success, 'response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $result)));
    }

    public function rider_commission() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $vendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
        $select = ",(O.total_price+O.service_charge) AS order_amt,O.rider_commission";
        $join = "LEFT JOIN users U ON U.id=O.user_id";
        $qry = "SELECT O.orderno,O.date,U.name $select FROM `orders` O $join WHERE O.assign_rider='$vendorId' AND  O.archive=0"; //
        $dArray = $this->Database->select_qry_array($qry);
        $orderAmt = '0';
        $riderCom = '0';
        for ($i = 0; $i < count($dArray); $i++) {
            $d = $dArray[$i];
            $orderAmt = $orderAmt + $d->order_amt;
            $riderCom = $riderCom + $d->rider_commission;
            $d->time = date('h:i A', strtotime($d->date));
            $d->date = date('d/m/Y', strtotime($d->date));

            $d->order_amt = $d->order_amt . ' PKR';
            $d->rider_commission = $d->rider_commission . ' PKR';
        }
        $result['order_list'] = $dArray;
        $result['order_amount'] = DecimalAmount($orderAmt) . ' PKR';
        $result['rider_commission'] = DecimalAmount($riderCom) . ' PKR';
        die(json_encode(array("code" => $this->success, 'response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $result)));
    }

    public function show_social_login() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $version = !empty($json['version']) ? $json['version'] : '';
        $result['is_social_login'] = $version == '1.1' ? false : true;
        $result['is_social_login'] = true;
        die(json_encode(array("code" => $this->success, 'response' => array('status' => true, 'message' => $this->lang->line("success")), 'result' => $result)));
    }

    public function contact_us() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $Insert = array(
            'name' => !empty($json['name']) ? $json['name'] : '',
            'email' => !empty($json['email']) ? $json['email'] : '',
            'message' => !empty($json['message']) ? $json['message'] : '',
            'mobile_number' => !empty($json['mobile_number']) ? $json['mobile_number'] : '',
        );
        $result = $this->Database->insert('contact_us', $Insert);
        sendContactusEmail($result);
        die(json_encode(array("response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => [])));
    }

}
