<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends CI_Controller {

    private $segment2;
    private $segment3;
    private $segment4;

    public function __construct() {

        parent::__construct();
        IsLogedVendor();
        error_reporting(0);
        date_default_timezone_set(TIME_ZONE_GET);
        $this->load->Model('Database');
        $this->segment2 = $this->uri->segment(2);
        $this->segment3 = $this->uri->segment(3);
        $this->segment4 = $this->uri->segment(4);
        $this->load->library('form_validation');

        if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'en' || empty($this->session->userdata(ADMIN_LANGUAGE_ID))) {
            $this->load->language("common", "english");
        } else {
            $this->load->language("common", "arabic");
        }
    }

    public function helper() {
        $status = $_REQUEST['status'];
        switch ($status) {
            default :
                $_REQUEST['status']($_REQUEST);
        }
    }

    public function index() {
        $this->load->view(VENDOR_DIR . 'includes/header');
        $this->load->view(VENDOR_DIR . 'index');
        $this->load->view(VENDOR_DIR . 'includes/footer');
    }

    public function profile() {
        $segment3 = $this->segment3;
        $data = array();
        if ($segment3 == 'update') {
            if (!empty($_REQUEST)) {
                $array = $_REQUEST;
                $Id = $_REQUEST['Id'];
                // if (!empty($_REQUEST['VendorImage'])) {
                //     $ImageName = preg_replace('/\s+/', '', $array['name']) . uniqid() . '.jpeg';
                //     $UploadPath = HOME_DIR . 'uploads/vendor_images/' . $ImageName;
                //     if (file_put_contents($UploadPath, file_get_contents($_REQUEST['VendorImage']))) {
                //         $oldFile = HOME_DIR . (!empty($_REQUEST['OldImage']) ? $_REQUEST['OldImage'] : '');
                //         if (is_file($oldFile)) {
                //             unlink($oldFile);
                //         }
                //         $array['image'] = $ImageName;
                //     }
                // }
                // unset($array['VendorImage']);
                unset($array['Id']);
                // unset($array['OldImage']);
                if ($array['password'] != '') {
                    $array['password'] = md5($array['password']);
                } else {
                    unset($array['password']);
                }
                $CondArray = array('id' => $Id);
                $Result = $this->Database->update('users', $array, $CondArray);

                $vendor_data = $this->session->userdata('Vendor');
            }
            if ($Result) {
                $return = array('status' => 'success');
            }
            exit(json_encode($return));
        } else {
            $this->load->view(VENDOR_DIR . 'includes/header');
            $this->load->view(VENDOR_DIR . 'My_profile');
            $this->load->view(VENDOR_DIR . 'includes/footer');
        }
    }

    public function storelist() {

        $vendor_data = $this->session->userdata('Vendor');
        $vendor_id = $vendor_data->id;
        $ConditionArray = array('vendor_id' => $vendor_id);
        $Qry = "SELECT * FROM `restaurant_details` WHERE vendor_id=:vendor_id";
        $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
        $data['restaurant'] = $Array;
        $this->load->view(VENDOR_DIR . 'includes/header');
        $this->load->view(VENDOR_DIR . 'restaurant_details', $data);
        $this->load->view(VENDOR_DIR . 'includes/footer');
    }

    public function storedetails() {
        $segment3 = $this->segment3;
        $data = array();
        $vendor_data = $this->session->userdata('Vendor');
        if ($segment3 == 'update') {
            if (!empty($_REQUEST)) {
                $array = $_REQUEST;
                $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
                $arrays['restaurant_name'] = $array['restaurant_name'];
                $arrays['restaurant_name_ar'] = $array['restaurant_name_ar'];
                $arrays['opening_time'] = date("H:i", strtotime($array['opening_time']));
                $arrays['closing_time'] = date("H:i", strtotime($array['closing_time']));
                $arrays['delivery_time'] = $array['delivery_time'];
                //$arrays['service_charge'] = $array['service_charge'];
                $arrays['min_amount'] = $array['min_amount'];
                $arrays['about'] = $array['about'];
                $arrays['about_ar'] = $array['about_ar'];
                $arrays['isDelivery'] = $array['isDelivery'];
                $arrays['self_pickup'] = $array['self_pickup'];
                $arrays['prescriptionreq'] = !empty($array['prescriptionreq']) ? $array['prescriptionreq'] : 0;


                $arrays['delivery_hours_st'] = !empty($array['delivery_hours_st']) ? date("H:i", strtotime($array['delivery_hours_st'])) : '00:00:00';
                $arrays['delivery_hours_et'] = !empty($array['delivery_hours_et']) ? date("H:i", strtotime($array['delivery_hours_et'])) : '00:00:00';




                // $arrays['pre_order'] = $array['pre_order'];
                $arrays['party_order'] = !empty($array['party_order']) ? $array['party_order'] : 0;
                $arrays['busy_status'] = $array['busy_status'];
                $arrays['delivery_time_start'] = !empty($array['delivery_time_start']) ? date("H:i", strtotime($array['delivery_time_start'])) : '';
                $arrays['delivery_time_ends'] = !empty($array['delivery_time_ends']) ? date("H:i", strtotime($array['delivery_time_ends'])) : '';
                $arrays['table_booking'] = !empty($array['table_booking']) ? $array['table_booking'] : 0;
                $arrays['table_capacity'] = !empty($array['table_capacity']) ? $array['table_capacity'] : 0;
                $arrays['table_booking_opening_time'] = !empty($array['table_booking_opening_time']) ? date("H:i", strtotime($array['table_booking_opening_time'])) : '';
                $arrays['table_booking_closing_time'] = !empty($array['table_booking_closing_time']) ? date("H:i", strtotime($array['table_booking_closing_time'])) : '';

                $arrays = array_merge($arrays, $json);

                $Id = $_REQUEST['Id'];

                $CondArray = array('vendor_id' => $Id);
                $Result = $this->Database->update('restaurant_details', $arrays, $CondArray);

                if (!empty($_REQUEST['LogoImages']) || !empty($_REQUEST['LicenseImages'])) {
                    if (!empty($_REQUEST['LogoImages'])) {
                        $ImageName = preg_replace('/\s+/', '', $vendor_data->name) . uniqid() . '.jpeg';
                        $UploadPath = HOME_DIR . 'uploads/vendor_images/' . $ImageName;
                        if (file_put_contents($UploadPath, file_get_contents($_REQUEST['LogoImages']))) {
                            $oldFile = HOME_DIR . (!empty($_REQUEST['LogoOldImage']) ? $_REQUEST['LogoOldImage'] : '');
                            if (is_file($oldFile)) {
                                unlink($oldFile);
                            }
                            $array1['image'] = $ImageName;
                        }
                    }

                    if (!empty($_REQUEST['LicenseImages'])) {
                        $ImageName1 = preg_replace('/\s+/', '', $vendor_data->name) . uniqid() . '.jpeg';
                        $UploadPath1 = HOME_DIR . 'uploads/trade_license/' . $ImageName1;
                        if (file_put_contents($UploadPath1, file_get_contents($_REQUEST['LicenseImages']))) {
                            $oldFile1 = HOME_DIR . (!empty($_REQUEST['LicenseOldImage']) ? $_REQUEST['LicenseOldImage'] : '');
                            if (is_file($oldFile1)) {
                                unlink($oldFile1);
                            }
                            $array1['trade_license'] = $ImageName1;
                        }
                    }
                    unset($array1['LogoImages']);
                    unset($array1['LogoOldImage']);
                    unset($array1['LicenseImages']);
                    unset($array1['LicenseOldImage']);
                }
                $array1['delivery_radius'] = $array['delivery_radius'];
                $CondArray1 = array('id' => $Id);
                $this->Database->update('users', $array1, $CondArray1);
                $cuisine_details = array('vendor_id' => $Id);
                $this->Database->delete('restaurant_cuisine_type', $cuisine_details);

                if (!empty($array["cuisine_id"])) {
                    foreach ($array["cuisine_id"] as $categoryId) {
                        $storeCuisine = [
                            "vendor_id" => $Id,
                            "cuisine_id" => $categoryId,
                            "lastupdate" => date('Y-m-d H:i:s'),
                        ];
                        $this->Database->insert('restaurant_cuisine_type', $storeCuisine);
                    }
                }


                $updateLog = [$arrays, $array1];
                $log['vendor_id'] = $Id;
                $log['updated_by'] = $Id;
                $log['update_type'] = 'VENDOR_PROFILE_UPDATED_BY_VENDOR';
                $log['data'] = json_encode($updateLog);
                $log['timestamp'] = date('Y-m-d H:i:s');
                $this->Database->insert('vendor_log', $log);




                if (!empty($_REQUEST['delivery_from'])) {
                    $CondArray = array('vendor_id' => $Id);
                    $this->Database->delete('delivery_timings', $CondArray);
                    foreach ($_REQUEST['delivery_from'] as $key => $product_item) {

                        $insertItems = [
                            "vendor_id" => $Id,
                            "delivery_from" => date('H:i', strtotime($_REQUEST['delivery_from'][$key])),
                            "delivery_to" => date('H:i', strtotime($_REQUEST['delivery_to'][$key])),
                            // "cities" => $_REQUEST['emirate'][$key],
                            "cities" => !empty($_REQUEST['emirate'][$key]['value']) ? implode(',', $_REQUEST['emirate'][$key]['value']) : '',
                        ];


                        if ($_REQUEST['delivery_from'][$key] != '') {
                            $this->Database->insert('delivery_timings', $insertItems);
                        }
                    }
                }
            }
            if ($Result) {
                $return = array('status' => 'success');
            }
            exit(json_encode($return));
        } else {
            $this->load->view(VENDOR_DIR . 'includes/header');
            $this->load->view(VENDOR_DIR . 'store_details');
            $this->load->view(VENDOR_DIR . 'includes/footer');
        }
    }

    // public function menu() {
    //     $segment3 = $this->segment3;
    //     $Id = base64_decode($this->segment4);
    //     $data = array();
    //     if ($segment3 == 'add-new') {
    //         if (isset($_REQUEST['json'])) {
    //             $json = json_decode($_REQUEST['json'], true);
    //             $json['lastupdate'] = date('Y-m-d H:i:s');
    //             $Id = $json['Id'];
    //             unset($json['Id']);
    //             if (isset($_REQUEST['Images']) && !empty($_REQUEST['Images']) && $_REQUEST['Images'] != "undefined") {
    //                 $ImageName = preg_replace('/\s+/', '', substr($json['menu_name'], 0, 3)) . uniqid() . '.jpeg';
    //                 $UploadPath = HOME_DIR . 'uploads/menu/' . $ImageName;
    //                 if (file_put_contents($UploadPath, file_get_contents($_REQUEST['Images']))) {
    //                     $oldFile = HOME_DIR . (!empty($_REQUEST['OldImage']) ? $_REQUEST['OldImage'] : '');
    //                     if (is_file($oldFile)) {
    //                         unlink($oldFile);
    //                     }
    //                     $json['image'] = $ImageName;
    //                 }
    //             }
    //             if (!empty($_REQUEST['NewImages'])) {
    //                 for ($i = 0; $i < count($_REQUEST['NewImages']); $i++) {
    //                     if ($_REQUEST['NewImages'][$i] != "") {
    //                         $ImageName = preg_replace('/\s+/', '', substr($json['menu_name'], 0, 3)) . uniqid() . '.jpeg';
    //                         $UploadPath = HOME_DIR . 'uploads/product_images/' . $ImageName;
    //                         if (file_put_contents($UploadPath, file_get_contents($_REQUEST['NewImages'][$i]))) {
    //                             $oldFile = HOME_DIR . 'uploads/product_images/' . (!empty($_REQUEST['Old_Image'][$i]) ? $_REQUEST['Old_Image'][$i] : '');
    //                             if (is_file($oldFile)) {
    //                                 unlink($oldFile);
    //                             }
    //                             $json['product_images'][] = $ImageName;
    //                         }
    //                     } else {
    //                         if ($_REQUEST['Old_Image'][$i] != '') {
    //                             $json['product_images'][] = $_REQUEST['Old_Image'][$i];
    //                         }
    //                     }
    //                 }
    //             }
    //             if (!empty($json['product_images'])) {
    //                 $json['product_images'] = implode(",", $json['product_images']);
    //             }
    //             unset($json['Images']);
    //             unset($json['OldImage']);
    //             unset($json['Id']);
    //             unset($json['NewImages']);
    //             unset($json['Old_Image']);
    //             $vendor_data = $this->session->userdata('Vendor');
    //             $json['vendor_id'] = $vendor_data->id;
    //             $json['choice'] = $_REQUEST['choice'];
    //             $json['price'] = !empty($_REQUEST['product_price']) ? $_REQUEST['product_price'] : '0';
    //             if ($Id == '') {
    //                 if (isset($json['category_name']) && $json['category_name'] != '') {
    //                     if (!empty($json['stock']) && $json['stock'] != 'undefined') {
    //                         $stock = $json['stock'];
    //                     } elseif ($json['stock'] == 'undefined') {
    //                         $stock = 0;
    //                     } else {
    //                         $stock = 0;
    //                     }
    //                     $cat = array('store_type' => $json['store_type'], 'category_name' => $json['category_name'], 'lastupdate' => $json['lastupdate']);
    //                     $Results = $this->Database->insert('category_details', $cat);
    //                     $jsons['vendor_id'] = $vendor_data->id;
    //                     $jsons['category_id'] = $Results;
    //                     $jsons['store_type'] = $json['store_type'];
    //                     $jsons['menu_name'] = $json['menu_name'];
    //                     $jsons['menu_name_ar'] = $json['menu_name_ar'];
    //                     $jsons['description'] = $json['description'];
    //                     $jsons['description_ar'] = $json['description_ar'];
    //                     $jsons['price'] = $json['price'];
    //                     $jsons['stock'] = !empty($stock) ? $stock : 0;
    //                     $jsons['image'] = !empty($json['image']) ? $json['image'] : '';
    //                     $jsons['product_images'] = !empty($json['product_images']) ? $json['product_images'] : '';
    //                     $jsons['choice'] = $json['choice'];
    //                     // $jsons['deal_id'] = $json['deal_id'];
    //                     $jsons['status'] = $json['status'];
    //                     $jsons['lastupdate'] = $json['lastupdate'];
    //                     // print_r($jsons);die();
    //                     $Result = $this->Database->insert('menu_list', $jsons);
    //                 } else {
    //                     $Result = $this->Database->insert('menu_list', $json);
    //                 }
    //                 if (!empty($_REQUEST['size'])) {
    //                     foreach ($_REQUEST['size'] as $key => $product_item) {
    //                         $insertItems = [
    //                             "menu_id" => $Result,
    //                             "menu_size" => $_REQUEST['size'][$key],
    //                             "menu_price" => !empty($_REQUEST['price']) ? $_REQUEST['price'][$key] : 0,
    //                             "lastupdate" => $json['lastupdate'],
    //                         ];
    //                         if ($_REQUEST['size'][$key] != '') {
    //                             $this->Database->insert('menu_sizes', $insertItems);
    //                         }
    //                     }
    //                 }
    //                 if (!empty($_REQUEST['addon'])) {
    //                     foreach ($_REQUEST['addon'] as $key => $product_item) {
    //                         $insertItems = [
    //                             "menu_id" => $Result,
    //                             "add_on" => $_REQUEST['addon'][$key],
    //                             "add_on_ar" => $_REQUEST['addon_ar'][$key],
    //                             "addon_price" => !empty($_REQUEST['addon_price']) ? $_REQUEST['addon_price'][$key] : 0,
    //                             "lastupdate" => $json['lastupdate'],
    //                         ];
    //                         if ($_REQUEST['addon'][$key] != '') {
    //                             $this->Database->insert('menu_addons', $insertItems);
    //                         }
    //                     }
    //                 }
    //                 if (!empty($_REQUEST['topping'])) {
    //                     foreach ($_REQUEST['topping'] as $key => $product_item) {
    //                         $insertItems = [
    //                             "menu_id" => $Result,
    //                             "topping" => $_REQUEST['topping'][$key],
    //                             "topping_ar" => $_REQUEST['topping_ar'][$key],
    //                             "topping_price" => !empty($_REQUEST['topping_price']) ? $_REQUEST['topping_price'][$key] : 0,
    //                             "lastupdate" => $json['lastupdate'],
    //                         ];
    //                         if ($_REQUEST['topping'][$key] != '') {
    //                             $this->Database->insert('menu_topping', $insertItems);
    //                         }
    //                     }
    //                 }
    //                 if (!empty($_REQUEST['drink_name'])) {
    //                     foreach ($_REQUEST['drink_name'] as $key => $product_item) {
    //                         $insertItems = [
    //                             "menu_id" => $Result,
    //                             "drink_name" => $_REQUEST['drink_name'][$key],
    //                             "drink_name_ar" => $_REQUEST['drink_name_ar'][$key],
    //                             "drink_price" => !empty($_REQUEST['drink_price']) ? $_REQUEST['drink_price'][$key] : 0,
    //                             "lastupdate" => $json['lastupdate'],
    //                         ];
    //                         if ($_REQUEST['drink_name'][$key] != '') {
    //                             $this->Database->insert('menu_drink', $insertItems);
    //                         }
    //                     }
    //                 }
    //                 if (!empty($_REQUEST['dip'])) {
    //                     foreach ($_REQUEST['dip'] as $key => $product_item) {
    //                         $insertItems = [
    //                             "menu_id" => $Result,
    //                             "dips" => $_REQUEST['dip'][$key],
    //                             "dips_ar" => $_REQUEST['dip_ar'][$key],
    //                             "dip_price" => !empty($_REQUEST['dip_price']) ? $_REQUEST['dip_price'][$key] : 0,
    //                             "lastupdate" => $json['lastupdate'],
    //                         ];
    //                         if ($_REQUEST['dip'][$key] != '') {
    //                             $this->Database->insert('menu_dips', $insertItems);
    //                         }
    //                     }
    //                 }
    //                 if (isset($_REQUEST['side_dish']) && !empty($_REQUEST['side_dish'])) {
    //                     foreach ($_REQUEST['side_dish'] as $key => $product_item) {
    //                         $insertItems = [
    //                             "menu_id" => $Result,
    //                             "side_dish" => $_REQUEST['side_dish'][$key],
    //                             "side_dish_ar" => $_REQUEST['side_dish_ar'][$key],
    //                             "side_dish_price" => !empty($_REQUEST['side_dish_price']) ? $_REQUEST['side_dish_price'][$key] : 0,
    //                             "lastupdate" => $json['lastupdate'],
    //                         ];
    //                         if ($_REQUEST['side_dish'][$key] != '') {
    //                             $this->Database->insert('menu_side', $insertItems);
    //                         }
    //                     }
    //                 }
    //             } else if ($Id > 0) {
    //                 $Result = 1;
    //                 $CondArray = array('id' => $Id);
    //                 if (!empty($json['stock']) && $json['stock'] != 'undefined') {
    //                     $stock = $json['stock'];
    //                 } elseif ($json['stock'] == 'undefined') {
    //                     $stock = 0;
    //                 } else {
    //                     $stock = 0;
    //                 }
    //                 if (isset($json['category_name']) && $json['category_name'] != '') {
    //                     $cat = array('category_name' => $json['category_name']);
    //                     $Results = $this->Database->insert('category_details', $cat);
    //                     $jsons['vendor_id'] = $vendor_data->id;
    //                     $jsons['category_id'] = $Results;
    //                     $jsons['menu_name'] = $json['menu_name'];
    //                     $jsons['menu_name_ar'] = $json['menu_name_ar'];
    //                     $jsons['description'] = $json['description'];
    //                     $jsons['description_ar'] = $json['description_ar'];
    //                     $jsons['price'] = $json['price'];
    //                     $jsons['stock'] = $stock;
    //                     $jsons['image'] = $json['image'];
    //                     $jsons['product_images'] = !empty($json['product_images']) ? $json['product_images'] : '';
    //                     $jsons['choice'] = $json['choice'];
    //                     // $jsons['deal_id'] = $json['deal_id'];
    //                     $jsons['status'] = $json['status'];
    //                     $jsons['lastupdate'] = $json['lastupdate'];
    //                     $this->Database->update('menu_list', $jsons, $CondArray);
    //                 } else {
    //                     $this->Database->update('menu_list', $json, $CondArray);
    //                 }
    //                 if ($json['choice'] == 0) {
    //                     $menu_details = array('menu_id' => $Id);
    //                     $Res = $this->Database->delete('menu_sizes', $menu_details);
    //                     $menu_addon = array('menu_id' => $Id);
    //                     $Res = $this->Database->delete('menu_addons', $menu_addon);
    //                     $menu_topping = array('menu_id' => $Id);
    //                     $Res = $this->Database->delete('menu_topping', $menu_topping);
    //                     $menu_drink = array('menu_id' => $Id);
    //                     $Res = $this->Database->delete('menu_drink', $menu_drink);
    //                     $menu_dips = array('menu_id' => $Id);
    //                     $Res = $this->Database->delete('menu_dips', $menu_dips);
    //                 } else {
    //                     if (!empty($_REQUEST['size'])) {
    //                         // $menu_details = array('menu_id' => $Id);
    //                         // $Res = $this->Database->delete('menu_sizes', $menu_details);
    //                         foreach ($_REQUEST['size'] as $key => $product_item) {
    //                             $insertItems = [
    //                                 "menu_id" => $Id,
    //                                 "menu_size" => $_REQUEST['size'][$key],
    //                                 "menu_price" => !empty($_REQUEST['price'][$key]) ? $_REQUEST['price'][$key] : 0,
    //                                 "lastupdate" => $json['lastupdate'],
    //                             ];
    //                             if (!empty($_REQUEST['size_id'][$key])) {
    //                                 $menu_details = array('menu_id' => $Id, 'menu_size_id' => $_REQUEST['size_id'][$key]);
    //                                 if ($_REQUEST['size'][$key] != '') {
    //                                     $this->Database->update('menu_sizes', $insertItems, $menu_details);
    //                                     // $this->Database->insert('menu_sizes', $insertItems);
    //                                 }
    //                             } else {
    //                                 if ($_REQUEST['size'][$key] != '') {
    //                                     $this->Database->insert('menu_sizes', $insertItems);
    //                                 }
    //                             }
    //                         }
    //                     }
    //                     if (!empty($_REQUEST['addon'])) {
    //                         // $menu_addon = array('menu_id' => $Id);
    //                         // $Res = $this->Database->delete('menu_addons', $menu_addon);
    //                         foreach ($_REQUEST['addon'] as $key => $product_item) {
    //                             $insertItems = [
    //                                 "menu_id" => $Id,
    //                                 "add_on" => $_REQUEST['addon'][$key],
    //                                 "add_on_ar" => !empty($_REQUEST['addon_ar'][$key]) ? $_REQUEST['addon_ar'][$key] : '',
    //                                 "addon_price" => !empty($_REQUEST['addon_price'][$key]) ? $_REQUEST['addon_price'][$key] : 0,
    //                                 "lastupdate" => $json['lastupdate'],
    //                             ];
    //                             // if($_REQUEST['addon'][$key]!='')
    //                             // {
    //                             //     $this->Database->insert('menu_addons', $insertItems);
    //                             // }
    //                             if (!empty($_REQUEST['addon_id'][$key])) {
    //                                 $menu_details = array('menu_id' => $Id, 'menu_addon_id' => $_REQUEST['addon_id'][$key]);
    //                                 if ($_REQUEST['addon'][$key] != '') {
    //                                     $this->Database->update('menu_addons', $insertItems, $menu_details);
    //                                     // $this->Database->insert('menu_addons', $insertItems);
    //                                 }
    //                             } else {
    //                                 if ($_REQUEST['addon'][$key] != '') {
    //                                     $this->Database->insert('menu_addons', $insertItems);
    //                                 }
    //                             }
    //                         }
    //                     }
    //                     if (!empty($_REQUEST['topping'])) {
    //                         // $menu_topping = array('menu_id' => $Id);
    //                         // $Res = $this->Database->delete('menu_topping', $menu_topping);
    //                         foreach ($_REQUEST['topping'] as $key => $product_item) {
    //                             $insertItems = [
    //                                 "menu_id" => $Id,
    //                                 "topping" => $_REQUEST['topping'][$key],
    //                                 "topping_ar" => !empty($_REQUEST['topping_ar'][$key]) ? $_REQUEST['topping_ar'][$key] : '',
    //                                 "topping_price" => !empty($_REQUEST['topping_price'][$key]) ? $_REQUEST['topping_price'][$key] : 0,
    //                                 "lastupdate" => $json['lastupdate'],
    //                             ];
    //                             if (!empty($_REQUEST['topping_id'][$key])) {
    //                                 $menu_details = array('menu_id' => $Id, 'menu_topping_id' => $_REQUEST['topping_id'][$key]);
    //                                 if ($_REQUEST['topping'][$key] != '') {
    //                                     $this->Database->update('menu_topping', $insertItems, $menu_details);
    //                                     // $this->Database->insert('menu_addons', $insertItems);
    //                                 }
    //                             } else {
    //                                 if ($_REQUEST['topping'][$key] != '') {
    //                                     $this->Database->insert('menu_topping', $insertItems);
    //                                 }
    //                             }
    //                             // if($_REQUEST['topping'][$key]!='')
    //                             // {
    //                             //     $this->Database->insert('menu_topping', $insertItems);
    //                             // }
    //                         }
    //                     }
    //                     // if(!empty($_REQUEST['drink_name']))
    //                     // {
    //                     //     $menu_drink = array('menu_id' => $Id);
    //                     //     $Res = $this->Database->delete('menu_drink', $menu_drink);
    //                     //     foreach ($_REQUEST['drink_name'] as $key => $product_item) {
    //                     //         $insertItems = [
    //                     //             "menu_id" => $Id,
    //                     //             "drink_name" => $_REQUEST['drink_name'][$key],
    //                     //             "drink_name_ar" => !empty($_REQUEST['drink_name_ar'][$key])?$_REQUEST['drink_name_ar'][$key]:'',
    //                     //             "drink_price" => $_REQUEST['drink_price'][$key],
    //                     //             "lastupdate" => $json['lastupdate'],
    //                     //         ];
    //                     //         if($_REQUEST['drink_name'][$key]!='')
    //                     //         {
    //                     //             $this->Database->insert('menu_drink', $insertItems);
    //                     //         }
    //                     //     }
    //                     // }
    //                     if (!empty($_REQUEST['dip'])) {
    //                         // $menu_dip = array('menu_id' => $Id);
    //                         // $Res = $this->Database->delete('menu_dips', $menu_dip);
    //                         foreach ($_REQUEST['dip'] as $key => $product_item) {
    //                             $insertItems = [
    //                                 "menu_id" => $Id,
    //                                 "dips" => $_REQUEST['dip'][$key],
    //                                 "dips_ar" => !empty($_REQUEST['dip_ar'][$key]) ? $_REQUEST['dip_ar'][$key] : '',
    //                                 "dip_price" => !empty($_REQUEST['dip_price'][$key]) ? $_REQUEST['dip_price'][$key] : 0,
    //                                 "lastupdate" => $json['lastupdate'],
    //                             ];
    //                             if (!empty($_REQUEST['dip_id'][$key])) {
    //                                 $menu_details = array('menu_id' => $Id, 'menu_dip_id' => $_REQUEST['dip_id'][$key]);
    //                                 if ($_REQUEST['dip'][$key] != '') {
    //                                     $this->Database->update('menu_dips', $insertItems, $menu_details);
    //                                     // $this->Database->insert('menu_addons', $insertItems);
    //                                 }
    //                             } else {
    //                                 if ($_REQUEST['dip'][$key] != '') {
    //                                     $this->Database->insert('menu_dips', $insertItems);
    //                                 }
    //                             }
    //                             // if($_REQUEST['dip'][$key]!='')
    //                             // {
    //                             //     $this->Database->insert('menu_dips', $insertItems);
    //                             // }
    //                         }
    //                     }
    //                     if (!empty($_REQUEST['side_dish'])) {
    //                         // $menu_side = array('menu_id' => $Id);
    //                         // $Res = $this->Database->delete('menu_side', $menu_side);
    //                         foreach ($_REQUEST['side_dish'] as $key => $product_item) {
    //                             $insertItems = [
    //                                 "menu_id" => $Id,
    //                                 "side_dish" => $_REQUEST['side_dish'][$key],
    //                                 "side_dish_ar" => !empty($_REQUEST['side_dish_ar'][$key]) ? $_REQUEST['side_dish_ar'][$key] : '',
    //                                 "side_dish_price" => !empty($_REQUEST['side_dish_price'][$key]) ? $_REQUEST['side_dish_price'][$key] : 0,
    //                                 "lastupdate" => $json['lastupdate'],
    //                             ];
    //                             if (!empty($_REQUEST['side_dish_id'][$key])) {
    //                                 $menu_details = array('menu_id' => $Id, 'menu_side_id' => $_REQUEST['side_dish_id'][$key]);
    //                                 if ($_REQUEST['side_dish'][$key] != '') {
    //                                     $this->Database->update('menu_side', $insertItems, $menu_details);
    //                                     // $this->Database->insert('menu_addons', $insertItems);
    //                                 }
    //                             } else {
    //                                 if ($_REQUEST['side_dish'][$key] != '') {
    //                                     $this->Database->insert('menu_side', $insertItems);
    //                                 }
    //                             }
    //                         }
    //                     }
    //                 }
    //             }
    //             if ($Result) {
    //                 $return = array('status' => 'success');
    //             }
    //             exit(json_encode($return));
    //         }
    //         $data['Id'] = '';
    //         if (is_numeric($Id)) {
    //             $ConditionArray = array('id' => $Id);
    //             $Qry = "SELECT * FROM `menu_list` WHERE id=:id ORDER BY id DESC";
    //             $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
    //             $Qry1 = "SELECT * FROM `menu_sizes` WHERE menu_id=:id ORDER BY menu_size_id DESC";
    //             $Array1 = $this->Database->select_qry_array($Qry1, $ConditionArray);
    //             $Qry2 = "SELECT * FROM `menu_addons` WHERE menu_id=:id ORDER BY menu_addon_id DESC";
    //             $Array2 = $this->Database->select_qry_array($Qry2, $ConditionArray);
    //             $Qry3 = "SELECT * FROM `menu_topping` WHERE menu_id=:id ORDER BY menu_topping_id DESC";
    //             $Array3 = $this->Database->select_qry_array($Qry3, $ConditionArray);
    //             $Qry4 = "SELECT * FROM `menu_drink` WHERE menu_id=:id ORDER BY menu_drink_id DESC";
    //             $Array4 = $this->Database->select_qry_array($Qry4, $ConditionArray);
    //             $Qry5 = "SELECT * FROM `menu_dips` WHERE menu_id=:id ORDER BY menu_dip_id DESC";
    //             $Array5 = $this->Database->select_qry_array($Qry5, $ConditionArray);
    //             $Qry6 = "SELECT * FROM `menu_side` WHERE menu_id=:id ORDER BY menu_side_id DESC";
    //             $Array6 = $this->Database->select_qry_array($Qry6, $ConditionArray);
    //             if (count($Array) > 0) {
    //                 $data['Id'] = $Id;
    //                 $data['Categories'] = $Array[0];
    //                 $data['Menu'] = $Array1;
    //                 $data['Addon'] = $Array2;
    //                 $data['Topping'] = $Array3;
    //                 $data['Drink'] = $Array4;
    //                 $data['Dips'] = $Array5;
    //                 $data['Side'] = $Array6;
    //             }
    //         }
    //         $this->load->view(VENDOR_DIR . 'includes/header');
    //         $this->load->view(VENDOR_DIR . 'addMenu', $data);
    //         $this->load->view(VENDOR_DIR . 'includes/footer');
    //     } else {
    //         $vendor_data = $this->session->userdata('Vendor');
    //         $ConditionArray = array('archive' => 0, 'vendor_id' => $vendor_data->id);
    //         $Qry = "SELECT * FROM `menu_list` WHERE archive=:archive AND vendor_id=:vendor_id ORDER BY id DESC";
    //         $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
    //         $data['Categories'] = $Array;
    //         $this->load->view(VENDOR_DIR . 'includes/header');
    //         $this->load->view(VENDOR_DIR . 'menu', $data);
    //         $this->load->view(VENDOR_DIR . 'includes/footer');
    //     }
    // }
    public function menu() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {

            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);

                $json['lastupdate'] = date('Y-m-d H:i:s');
                $Id = $json['Id'];
                if (empty($Id)) {
                    $json['image'] = '';
                }
                unset($json['Id']);

                if (isset($_REQUEST['Images']) && !empty($_REQUEST['Images']) && $_REQUEST['Images'] != "undefined") {
                    $ImageName = preg_replace('/\s+/', '', substr($json['menu_name'], 0, 3)) . uniqid() . '.jpeg';
                    $UploadPath = HOME_DIR . 'uploads/menu/' . $ImageName;
                    if (file_put_contents($UploadPath, file_get_contents($_REQUEST['Images']))) {
                        $oldFile = HOME_DIR . (!empty($_REQUEST['OldImage']) ? $_REQUEST['OldImage'] : '');
                        if (is_file($oldFile)) {
                            unlink($oldFile);
                        }
                        $json['image'] = $ImageName;
                    }
                }
                unset($json['Images']);
                unset($json['OldImage']);
                unset($json['Id']);
                $vendor_data = $this->session->userdata('Vendor');
                $json['vendor_id'] = $vendor_data->id;
                $json['choice'] = $_REQUEST['choice'];
                $json['price'] = !empty($_REQUEST['product_price']) ? $_REQUEST['product_price'] : '0';
                $json['prescriptionreq'] = !empty($_REQUEST['prescriptionreq']) ? $_REQUEST['prescriptionreq'] : '0';
                $json['description'] = $_REQUEST['description'];
                $json['description_ar'] = $_REQUEST['description_ar'];
                $jsons['max_purchase_qty'] = !empty($json['max_purchase_qty']) ? $json['max_purchase_qty'] : '0';
                $json['max_purchase_qty'] = !empty($json['max_purchase_qty']) ? $json['max_purchase_qty'] : '0';
                $json['order_view'] = '0';
                $json['excel_upload_id'] = '0';
                if(!empty($json['status']) && !empty($Id)){
                    $this->Database->delete('add_to_cart', ['menu_id'=>$Id]);
                   
                }
                if ($Id == '') {
                    $menuPrice = 0;
                    if (isset($json['category_name']) && $json['category_name'] != '') {
                        if (!empty($json['stock']) && $json['stock'] != 'undefined') {
                            $stock = $json['stock'];
                        } elseif ($json['stock'] == 'undefined') {
                            $stock = 0;
                        } else {
                            $stock = 0;
                        }

                        $cat = array('store_type' => $json['store_type'], 'category_name' => $json['category_name'], 'lastupdate' => $json['lastupdate']);
                        $Results = $this->Database->insert('category_details', $cat);
                        $jsons['vendor_id'] = $vendor_data->id;
                        $jsons['category_id'] = $Results;
                        $jsons['store_type'] = $json['store_type'];
                        $jsons['menu_name'] = $json['menu_name'];
                        $jsons['menu_name_ar'] = $json['menu_name_ar'];
                        $jsons['description'] = $_REQUEST['description'];
                        $jsons['description_ar'] = $_REQUEST['description_ar'];
                        $jsons['price'] = $json['price'];
                        $jsons['order_view'] = '0';
                        $jsons['excel_upload_id'] = '';
                        $jsons['prescriptionreq'] = !empty($_REQUEST['prescriptionreq']) ? $_REQUEST['prescriptionreq'] : '0';
                        $jsons['stock'] = !empty($stock) ? $stock : 0;
                        $jsons['image'] = !empty($json['image']) ? $json['image'] : '';
                        $jsons['max_purchase_qty'] = !empty($json['max_purchase_qty']) ? $json['max_purchase_qty'] : '0';
                        $jsons['choice'] = $json['choice'];
                        // $jsons['deal_id'] = $json['deal_id'];
                        $jsons['status'] = $json['status'];
                        $jsons['lastupdate'] = $json['lastupdate'];
                        $menuPrice = $jsons['price'];
                        $Result = $this->Database->insert('menu_list', $jsons);
                    } else {
                        $menuPrice = $json['price'];
                        $Result = $this->Database->insert('menu_list', $json);
                    }

                    $cartupdate['product_price'] = $menuPrice;
                    $this->Database->update('add_to_cart', $cartupdate, array('menu_id' => $Result));

                    if (!empty($_REQUEST['size'])) {
                        foreach ($_REQUEST['size'] as $key => $product_item) {
                            $insertItems = [
                                "menu_id" => $Result,
                                "menu_size" => $_REQUEST['size'][$key],
                                "menu_price" => !empty($_REQUEST['price']) ? $_REQUEST['price'][$key] : 0,
                                "lastupdate" => $json['lastupdate'],
                            ];
                            if ($_REQUEST['size'][$key] != '') {
                                $this->Database->insert('menu_sizes', $insertItems);
                            }
                        }
                    }

                    if (!empty($_REQUEST['addon'])) {
                        foreach ($_REQUEST['addon'] as $key => $product_item) {
                            $insertItems = [
                                "menu_id" => $Result,
                                "add_on" => $_REQUEST['addon'][$key],
                                "add_on_ar" => $_REQUEST['add_on_ar'][$key],
                                "addon_price" => !empty($_REQUEST['addon_price']) ? $_REQUEST['addon_price'][$key] : 0,
                                "lastupdate" => $json['lastupdate'],
                            ];
                            if ($_REQUEST['addon'][$key] != '') {
                                $this->Database->insert('menu_addons', $insertItems);
                            }
                        }
                    }
                    if (!empty($_REQUEST['topping'])) {
                        foreach ($_REQUEST['topping'] as $key => $product_item) {
                            $insertItems = [
                                "menu_id" => $Result,
                                "topping" => $_REQUEST['topping'][$key],
                                "topping_ar" => $_REQUEST['topping_ar'][$key],
                                "topping_price" => !empty($_REQUEST['topping_price']) ? $_REQUEST['topping_price'][$key] : 0,
                                "lastupdate" => $json['lastupdate'],
                            ];
                            if ($_REQUEST['topping'][$key] != '') {
                                $this->Database->insert('menu_topping', $insertItems);
                            }
                        }
                    }
                    if (!empty($_REQUEST['drink_name'])) {
                        foreach ($_REQUEST['drink_name'] as $key => $product_item) {
                            $insertItems = [
                                "menu_id" => $Result,
                                "drink_name" => $_REQUEST['drink_name'][$key],
                                "drink_name_ar" => $_REQUEST['drink_name_ar'][$key],
                                "drink_price" => !empty($_REQUEST['drink_price']) ? $_REQUEST['drink_price'][$key] : 0,
                                "lastupdate" => $json['lastupdate'],
                            ];
                            if ($_REQUEST['drink_name'][$key] != '') {
                                $this->Database->insert('menu_drink', $insertItems);
                            }
                        }
                    }
                    if (!empty($_REQUEST['dip'])) {
                        foreach ($_REQUEST['dip'] as $key => $product_item) {
                            $insertItems = [
                                "menu_id" => $Result,
                                "dips" => $_REQUEST['dip'][$key],
                                "dips_ar" => $_REQUEST['dip_ar'][$key],
                                "dip_price" => !empty($_REQUEST['dip_price']) ? $_REQUEST['dip_price'][$key] : 0,
                                "lastupdate" => $json['lastupdate'],
                            ];
                            if ($_REQUEST['dip'][$key] != '') {
                                $this->Database->insert('menu_dips', $insertItems);
                            }
                        }
                    }
                    if (isset($_REQUEST['side_dish']) && !empty($_REQUEST['side_dish'])) {
                        foreach ($_REQUEST['side_dish'] as $key => $product_item) {
                            $insertItems = [
                                "menu_id" => $Result,
                                "side_dish" => $_REQUEST['side_dish'][$key],
                                "side_dish_ar" => $_REQUEST['side_dish_ar'][$key],
                                "side_dish_price" => !empty($_REQUEST['side_dish_price']) ? $_REQUEST['side_dish_price'][$key] : 0,
                                "lastupdate" => $json['lastupdate'],
                            ];
                            if ($_REQUEST['side_dish'][$key] != '') {
                                $this->Database->insert('menu_side', $insertItems);
                            }
                        }
                    }
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    if (!empty($json['stock']) && $json['stock'] != 'undefined') {
                        $stock = $json['stock'];
                    } elseif ($json['stock'] == 'undefined') {
                        $stock = 0;
                    } else {
                        $stock = 0;
                    }
                    $priceMenu = 0;

                    if (isset($json['category_name']) && $json['category_name'] != '') {


                        $cat = array('category_name' => $json['category_name']);
                        $Results = $this->Database->insert('category_details', $cat);
                        $jsons['vendor_id'] = $vendor_data->id;
                        $jsons['category_id'] = $Results;
                        $jsons['menu_name'] = $json['menu_name'];
                        $jsons['menu_name_ar'] = $json['menu_name_ar'];
                        $jsons['description'] = $_REQUEST['description'];
                        $jsons['description_ar'] = $_REQUEST['description_ar'];
                        $jsons['price'] = $json['price'];
                        $jsons['stock'] = $stock;
                        $jsons['image'] = $json['image'];
                        $jsons['choice'] = $json['choice'];

                        $jsons['prescriptionreq'] = !empty($_REQUEST['prescriptionreq']) ? $_REQUEST['prescriptionreq'] : '0';
                        // $jsons['deal_id'] = $json['deal_id'];
                        $jsons['status'] = $json['status'];
                        $jsons['lastupdate'] = $json['lastupdate'];
                        $priceMenu = $jsons['price'];

                        $this->Database->update('menu_list', $jsons, $CondArray);
                    } else {

                        $priceMenu = $json['price'];
                        $this->Database->update('menu_list', $json, $CondArray);
                    }


                    $cartupdate['product_price'] = $priceMenu;
                    $this->Database->update('add_to_cart', $cartupdate, array('menu_id' => $Id));


                    if ($json['choice'] == 0) {
                        $menu_details = array('menu_id' => $Id);
                        $Res = $this->Database->delete('menu_sizes', $menu_details);

                        $menu_addon = array('menu_id' => $Id);
                        $Res = $this->Database->delete('menu_addons', $menu_addon);

                        $menu_topping = array('menu_id' => $Id);
                        $Res = $this->Database->delete('menu_topping', $menu_topping);

                        $menu_drink = array('menu_id' => $Id);
                        $Res = $this->Database->delete('menu_drink', $menu_drink);

                        $menu_dips = array('menu_id' => $Id);
                        $Res = $this->Database->delete('menu_dips', $menu_dips);
                    } else {
                        if (!empty($_REQUEST['size'])) {
                            // $menu_details = array('menu_id' => $Id);
                            // $Res = $this->Database->delete('menu_sizes', $menu_details);
                            foreach ($_REQUEST['size'] as $key => $product_item) {
                                $insertItems = [
                                    "menu_id" => $Id,
                                    "menu_size" => $_REQUEST['size'][$key],
                                    "menu_price" => !empty($_REQUEST['price'][$key]) ? $_REQUEST['price'][$key] : 0,
                                    "lastupdate" => $json['lastupdate'],
                                ];
                                if (!empty($_REQUEST['size_id'][$key])) {
                                    $menu_details = array('menu_id' => $Id, 'menu_size_id' => $_REQUEST['size_id'][$key]);

                                    if ($_REQUEST['size'][$key] != '') {
                                        $this->Database->update('menu_sizes', $insertItems, $menu_details);
                                        // $this->Database->insert('menu_sizes', $insertItems);
                                    }
                                } else {

                                    if ($_REQUEST['size'][$key] != '') {
                                        $this->Database->insert('menu_sizes', $insertItems);
                                    }
                                }
                            }
                        }


                        if (!empty($_REQUEST['addon'])) {
                            // $menu_addon = array('menu_id' => $Id);
                            // $Res = $this->Database->delete('menu_addons', $menu_addon);
                            foreach ($_REQUEST['addon'] as $key => $product_item) {
                                $insertItems = [
                                    "menu_id" => $Id,
                                    "add_on" => $_REQUEST['addon'][$key],
                                    "add_on_ar" => !empty($_REQUEST['addon_ar'][$key]) ? $_REQUEST['addon_ar'][$key] : '',
                                    "addon_price" => !empty($_REQUEST['addon_price'][$key]) ? $_REQUEST['addon_price'][$key] : 0,
                                    "lastupdate" => $json['lastupdate'],
                                ];
                                // if($_REQUEST['addon'][$key]!='')
                                // {
                                //     $this->Database->insert('menu_addons', $insertItems);
                                // }
                                if (!empty($_REQUEST['addon_id'][$key])) {
                                    $menu_details = array('menu_id' => $Id, 'menu_addon_id' => $_REQUEST['addon_id'][$key]);

                                    if ($_REQUEST['addon'][$key] != '') {
                                        $this->Database->update('menu_addons', $insertItems, $menu_details);
                                        // $this->Database->insert('menu_addons', $insertItems);
                                    }
                                } else {
                                    if ($_REQUEST['addon'][$key] != '') {
                                        $this->Database->insert('menu_addons', $insertItems);
                                    }
                                }
                            }
                        }

                        if (!empty($_REQUEST['topping'])) {
                            // $menu_topping = array('menu_id' => $Id);
                            // $Res = $this->Database->delete('menu_topping', $menu_topping);
                            foreach ($_REQUEST['topping'] as $key => $product_item) {
                                $insertItems = [
                                    "menu_id" => $Id,
                                    "topping" => $_REQUEST['topping'][$key],
                                    "topping_ar" => !empty($_REQUEST['topping_ar'][$key]) ? $_REQUEST['topping_ar'][$key] : '',
                                    "topping_price" => !empty($_REQUEST['topping_price'][$key]) ? $_REQUEST['topping_price'][$key] : 0,
                                    "lastupdate" => $json['lastupdate'],
                                ];
                                if (!empty($_REQUEST['topping_id'][$key])) {
                                    $menu_details = array('menu_id' => $Id, 'menu_topping_id' => $_REQUEST['topping_id'][$key]);

                                    if ($_REQUEST['topping'][$key] != '') {
                                        $this->Database->update('menu_topping', $insertItems, $menu_details);
                                        // $this->Database->insert('menu_addons', $insertItems);
                                    }
                                } else {

                                    if ($_REQUEST['topping'][$key] != '') {
                                        $this->Database->insert('menu_topping', $insertItems);
                                    }
                                }
                                // if($_REQUEST['topping'][$key]!='')
                                // {
                                //     $this->Database->insert('menu_topping', $insertItems);
                                // }
                            }
                        }
                        // if(!empty($_REQUEST['drink_name']))
                        // {
                        //     $menu_drink = array('menu_id' => $Id);
                        //     $Res = $this->Database->delete('menu_drink', $menu_drink);
                        //     foreach ($_REQUEST['drink_name'] as $key => $product_item) {
                        //         $insertItems = [
                        //             "menu_id" => $Id,
                        //             "drink_name" => $_REQUEST['drink_name'][$key],
                        //             "drink_name_ar" => !empty($_REQUEST['drink_name_ar'][$key])?$_REQUEST['drink_name_ar'][$key]:'',
                        //             "drink_price" => $_REQUEST['drink_price'][$key],
                        //             "lastupdate" => $json['lastupdate'],
                        //         ];
                        //         if($_REQUEST['drink_name'][$key]!='')
                        //         {
                        //             $this->Database->insert('menu_drink', $insertItems);
                        //         }
                        //     }
                        // }
                        if (!empty($_REQUEST['dip'])) {
                            // $menu_dip = array('menu_id' => $Id);
                            // $Res = $this->Database->delete('menu_dips', $menu_dip);
                            foreach ($_REQUEST['dip'] as $key => $product_item) {
                                $insertItems = [
                                    "menu_id" => $Id,
                                    "dips" => $_REQUEST['dip'][$key],
                                    "dips_ar" => !empty($_REQUEST['dip_ar'][$key]) ? $_REQUEST['dip_ar'][$key] : '',
                                    "dip_price" => !empty($_REQUEST['dip_price'][$key]) ? $_REQUEST['dip_price'][$key] : 0,
                                    "lastupdate" => $json['lastupdate'],
                                ];
                                if (!empty($_REQUEST['dip_id'][$key])) {
                                    $menu_details = array('menu_id' => $Id, 'menu_dip_id' => $_REQUEST['dip_id'][$key]);

                                    if ($_REQUEST['dip'][$key] != '') {
                                        $this->Database->update('menu_dips', $insertItems, $menu_details);
                                        // $this->Database->insert('menu_addons', $insertItems);
                                    }
                                } else {

                                    if ($_REQUEST['dip'][$key] != '') {
                                        $this->Database->insert('menu_dips', $insertItems);
                                    }
                                }
                                // if($_REQUEST['dip'][$key]!='')
                                // {
                                //     $this->Database->insert('menu_dips', $insertItems);
                                // }
                            }
                        }
                        if (!empty($_REQUEST['side_dish'])) {
                            // $menu_side = array('menu_id' => $Id);
                            // $Res = $this->Database->delete('menu_side', $menu_side);
                            foreach ($_REQUEST['side_dish'] as $key => $product_item) {
                                $insertItems = [
                                    "menu_id" => $Id,
                                    "side_dish" => $_REQUEST['side_dish'][$key],
                                    "side_dish_ar" => !empty($_REQUEST['side_dish_ar'][$key]) ? $_REQUEST['side_dish_ar'][$key] : '',
                                    "side_dish_price" => !empty($_REQUEST['side_dish_price'][$key]) ? $_REQUEST['side_dish_price'][$key] : 0,
                                    "lastupdate" => $json['lastupdate'],
                                ];
                                if (!empty($_REQUEST['side_dish_id'][$key])) {
                                    $menu_details = array('menu_id' => $Id, 'menu_side_id' => $_REQUEST['side_dish_id'][$key]);

                                    if ($_REQUEST['side_dish'][$key] != '') {
                                        $this->Database->update('menu_side', $insertItems, $menu_details);
                                        // $this->Database->insert('menu_addons', $insertItems);
                                    }
                                } else {

                                    if ($_REQUEST['side_dish'][$key] != '') {
                                        $this->Database->insert('menu_side', $insertItems);
                                    }
                                }
                            }
                        }
                    }
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $ConditionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `menu_list` WHERE id=:id ORDER BY id DESC";
                $Array = $this->Database->select_qry_array($Qry, $ConditionArray);

                $Qry1 = "SELECT * FROM `menu_sizes` WHERE menu_id=:id ORDER BY menu_size_id DESC";
                $Array1 = $this->Database->select_qry_array($Qry1, $ConditionArray);

                $Qry2 = "SELECT * FROM `menu_addons` WHERE menu_id=:id ORDER BY menu_addon_id DESC";
                $Array2 = $this->Database->select_qry_array($Qry2, $ConditionArray);

                $Qry3 = "SELECT * FROM `menu_topping` WHERE menu_id=:id ORDER BY menu_topping_id DESC";
                $Array3 = $this->Database->select_qry_array($Qry3, $ConditionArray);

                $Qry4 = "SELECT * FROM `menu_drink` WHERE menu_id=:id ORDER BY menu_drink_id DESC";
                $Array4 = $this->Database->select_qry_array($Qry4, $ConditionArray);

                $Qry5 = "SELECT * FROM `menu_dips` WHERE menu_id=:id ORDER BY menu_dip_id DESC";
                $Array5 = $this->Database->select_qry_array($Qry5, $ConditionArray);

                $Qry6 = "SELECT * FROM `menu_side` WHERE menu_id=:id ORDER BY menu_side_id DESC";
                $Array6 = $this->Database->select_qry_array($Qry6, $ConditionArray);

                if (count($Array) > 0) {
                    $data['Id'] = $Id;
                    $data['Categories'] = $Array[0];
                    $data['Menu'] = $Array1;
                    $data['Addon'] = $Array2;
                    $data['Topping'] = $Array3;
                    $data['Drink'] = $Array4;
                    $data['Dips'] = $Array5;
                    $data['Side'] = $Array6;
                }
            }

            $this->load->view(VENDOR_DIR . 'includes/header');
            $this->load->view(VENDOR_DIR . 'addMenu', $data);
            $this->load->view(VENDOR_DIR . 'includes/footer');
        } else {
            $vendor_data = $this->session->userdata('Vendor');
            $ConditionArray = array('archive' => 0, 'vendor_id' => $vendor_data->id);
            $Qry = "SELECT * FROM `menu_list` WHERE archive=:archive AND vendor_id=:vendor_id ORDER BY id DESC";
            $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
            $data['Categories'] = $Array;
            $this->load->view(VENDOR_DIR . 'includes/header');
            $this->load->view(VENDOR_DIR . 'menu', $data);
            $this->load->view(VENDOR_DIR . 'includes/footer');
        }
    }

    public function party_menu() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);

                $json['lastupdate'] = date('Y-m-d H:i:s');
                $Id = $json['Id'];
                unset($json['Id']);

                if (isset($_REQUEST['Images']) && !empty($_REQUEST['Images']) && $_REQUEST['Images'] != "undefined") {
                    $ImageName = preg_replace('/\s+/', '', substr($json['menu_name'], 0, 3)) . uniqid() . '.jpeg';
                    $UploadPath = HOME_DIR . 'uploads/party_menu/' . $ImageName;
                    if (file_put_contents($UploadPath, file_get_contents($_REQUEST['Images']))) {
                        $oldFile = HOME_DIR . (!empty($_REQUEST['OldImage']) ? $_REQUEST['OldImage'] : '');
                        if (is_file($oldFile)) {
                            unlink($oldFile);
                        }
                        $json['image'] = $ImageName;
                    }
                }
                unset($json['Images']);
                unset($json['OldImage']);
                unset($json['Id']);
                $vendor_data = $this->session->userdata('Vendor');
                $json['vendor_id'] = $vendor_data->id;
                $json['price'] = !empty($_REQUEST['product_price']) ? $_REQUEST['product_price'] : '0';


                if ($Id == '') {
                    if (isset($json['category_name']) && $json['category_name'] != '') {


                        $cat = array('store_type' => $json['store_type'], 'category_name' => $json['category_name'], 'lastupdate' => $json['lastupdate']);
                        $Results = $this->Database->insert('category_details', $cat);
                        $jsons['vendor_id'] = $vendor_data->id;
                        $jsons['category_id'] = $Results;
                        $jsons['store_type'] = $json['store_type'];
                        $jsons['menu_name'] = $json['menu_name'];
                        $jsons['menu_name_ar'] = $json['menu_name_ar'];
                        $jsons['description'] = $json['description'];
                        $jsons['description_ar'] = $json['description_ar'];
                        $jsons['price'] = $json['price'];
                        $jsons['image'] = !empty($json['image']) ? $json['image'] : '';
                        $jsons['status'] = $json['status'];
                        $jsons['lastupdate'] = $json['lastupdate'];
                        // print_r($jsons);die();
                        $Result = $this->Database->insert('party_order_menu', $jsons);
                    } else {
                        $Result = $this->Database->insert('party_order_menu', $json);
                    }
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);

                    if (isset($json['category_name']) && $json['category_name'] != '') {
                        $cat = array('category_name' => $json['category_name']);
                        $Results = $this->Database->insert('category_details', $cat);
                        $jsons['vendor_id'] = $vendor_data->id;
                        $jsons['category_id'] = $Results;
                        $jsons['menu_name'] = $json['menu_name'];
                        $jsons['menu_name_ar'] = $json['menu_name_ar'];
                        $jsons['description'] = $json['description'];
                        $jsons['description_ar'] = $json['description_ar'];
                        $jsons['price'] = $json['price'];
                        $jsons['image'] = $json['image'];
                        $jsons['status'] = $json['status'];
                        $jsons['lastupdate'] = $json['lastupdate'];

                        $this->Database->update('party_order_menu', $jsons, $CondArray);
                    } else {
                        $this->Database->update('party_order_menu', $json, $CondArray);
                    }
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $ConditionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `party_order_menu` WHERE id=:id ORDER BY id DESC";
                $Array = $this->Database->select_qry_array($Qry, $ConditionArray);



                if (count($Array) > 0) {
                    $data['Id'] = $Id;
                    $data['Categories'] = $Array[0];
                }
            }

            $this->load->view(VENDOR_DIR . 'includes/header');
            $this->load->view(VENDOR_DIR . 'addPartyMenu', $data);
            $this->load->view(VENDOR_DIR . 'includes/footer');
        } else {
            $vendor_data = $this->session->userdata('Vendor');
            $ConditionArray = array('archive' => 0, 'vendor_id' => $vendor_data->id);
            $Qry = "SELECT * FROM `party_order_menu` WHERE archive=:archive AND vendor_id=:vendor_id ORDER BY id DESC";
            $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
            $data['Categories'] = $Array;
            $this->load->view(VENDOR_DIR . 'includes/header');
            $this->load->view(VENDOR_DIR . 'party_menu', $data);
            $this->load->view(VENDOR_DIR . 'includes/footer');
        }
    }

    public function addon() {
        $this->load->view(VENDOR_DIR . 'includes/header');
        $this->load->view(VENDOR_DIR . 'addMenuOptions');
        $this->load->view(VENDOR_DIR . 'includes/footer');
    }

    public function Orders() {
        $this->load->view(VENDOR_DIR . 'includes/header');
        $this->load->view(VENDOR_DIR . 'orders');
        $this->load->view(VENDOR_DIR . 'includes/footer');
    }

    public function OrderDetails($orderid = '') {
        $data['orderid'] = base64_decode($orderid);
        $this->load->view(VENDOR_DIR . 'includes/header');
        $this->load->view(VENDOR_DIR . 'order_details', $data);
        $this->load->view(VENDOR_DIR . 'includes/footer');
    }

    public function partyOrders() {
        $this->load->view(VENDOR_DIR . 'includes/header');
        $this->load->view(VENDOR_DIR . 'party_order');
        $this->load->view(VENDOR_DIR . 'includes/footer');
    }

    public function partyOrderDetails($orderid = '') {
        $data['orderid'] = base64_decode($orderid);
        $this->load->view(VENDOR_DIR . 'includes/header');
        $this->load->view(VENDOR_DIR . 'party_orderdetails', $data);
        $this->load->view(VENDOR_DIR . 'includes/footer');
    }

    public function offers() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $vendor_data = $this->session->userdata('Vendor');
        if ($segment3 == 'add-new') {
            if (!empty($_REQUEST)) {
                $array = $_REQUEST;

                $OfferId = $_REQUEST['Id'];

                // if (!empty($_REQUEST['OfferImages'])) {
                //     // for ($i = 0; $i < count($_REQUEST['OfferImages']); $i++) {
                //     //     if ($_REQUEST['OfferImages'][$i] != "") {
                //             $ImageName = preg_replace('/\s+/', '', substr($array['title'], 0, 3)) . uniqid() . '.jpeg';
                //             $UploadPath = HOME_DIR . 'uploads/offer_images/' . $ImageName;
                //             if (file_put_contents($UploadPath, file_get_contents($_REQUEST['OfferImages']))) {
                //                 $oldFile = HOME_DIR . 'uploads/offer_images/' . (!empty($_REQUEST['OldImage']) ? $_REQUEST['OldImage'] : '');
                //                 if (is_file($oldFile)) {
                //                     unlink($oldFile);
                //                 }
                //                 $json['offer_image']= $ImageName;
                //             }
                //         // } else {
                //         //     if ($_REQUEST['OldImage'][$i] != '') {
                //         //         $json['offer_image'][] = $_REQUEST['OldImage'][$i];
                //         //     }
                //         // }
                //     // }
                // }
                // if (!empty($array['offer_image'])) {
                //     $json['offer_image'] = implode(",", $array['offer_image']);
                // }
                // unset($array['OfferImages']);
                // unset($array['Id']);
                // unset($array['OldImage']);
                $json['title'] = !empty($array['title']) ? $array['title'] : '';
                $json['title_ar'] = !empty($array['title_ar']) ? $array['title_ar'] : '';
                $json['discount'] = !empty($array['discount']) ? $array['discount'] : '';
                $json['date_to'] = !empty($array['date_to']) ? date('Y-m-d', strtotime($array['date_to'])) : '0000-00-00';
                $json['date_from'] = !empty($array['date_from']) ? date('Y-m-d', strtotime($array['date_from'])) : '0000-00-00';
                $json['user_type'] = 2;
                $json['offer_type'] = 1;
                $json['offer_addedby'] = $vendor_data->id;
                $json['status'] = $array['status'];
                $json['menu_id'] = $array['menu_id'];
                $json['discount_type'] = !empty($array['discount_type']) ? $array['discount_type'] : '0';

                if (empty($OfferId)) {
                    $this->Database->insert('promo_code', $json);
                } else {
                    $this->Database->update('promo_code', $json, array('promocode_id' => $OfferId));
                }
                die(json_encode(array('status' => true, 'message' => 'success')));
            }
            $this->load->view(VENDOR_DIR . 'includes/header');
            $this->load->view(VENDOR_DIR . 'addoffers');
            $this->load->view(VENDOR_DIR . 'includes/footer');
        } else {
            $this->load->view(VENDOR_DIR . 'includes/header');
            $this->load->view(VENDOR_DIR . 'offers');
            $this->load->view(VENDOR_DIR . 'includes/footer');
        }
    }

    public function most_selling() {
        $this->load->view(VENDOR_DIR . 'includes/header');
        $this->load->view(VENDOR_DIR . 'most_selling');
        $this->load->view(VENDOR_DIR . 'includes/footer');
    }

    public function feedback() {
        $vendor_data = $this->session->userdata('Vendor');
        $ConditionArray = array('archive' => 0, 'vendor_id' => $vendor_data->id);
        $Qry = "SELECT * FROM `user_feedback` WHERE archive=:archive AND vendor_id=:vendor_id ORDER BY id DESC";
        $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
        $data['feedback'] = $Array;
        $this->load->view(VENDOR_DIR . 'includes/header');
        $this->load->view(VENDOR_DIR . 'feedback', $data);
        $this->load->view(VENDOR_DIR . 'includes/footer');
    }

    public function banner_management() {
        if (!empty($_REQUEST['XXXX'])) {
            $array = $_REQUEST;
            $vendor_data = $this->session->userdata('Vendor');
            $array['vendor_id'] = $vendor_data->id;
            $Qry = "SELECT * FROM `restaurant_details` WHERE vendor_id=" . $vendor_data->id;
            $Array = $this->Database->select_qry_array($Qry);

            if (!empty($_REQUEST['Images'])) {
                for ($i = 0; $i < count($_REQUEST['Images']); $i++) {
                    if ($_REQUEST['Images'][$i] != "") {
                        $ImageName = preg_replace('/\s+/', '', substr($Array[0]->restaurant_name, 0, 3)) . uniqid() . '.jpeg';
                        $UploadPath = HOME_DIR . 'uploads/banner_images/' . $ImageName;
                        if (file_put_contents($UploadPath, file_get_contents($_REQUEST['Images'][$i]))) {
                            $oldFile = HOME_DIR . 'uploads/banner_images/' . (!empty($_REQUEST['OldImage'][$i]) ? $_REQUEST['OldImage'][$i] : '');
                            if (is_file($oldFile)) {
                                unlink($oldFile);
                            }
                            $array['image'][] = $ImageName;
                        }
                    } else {
                        if ($_REQUEST['OldImage'][$i] != '') {
                            $array['image'][] = $_REQUEST['OldImage'][$i];
                        }
                    }
                }
            }
            if (!empty($array['image'])) {
                $array['image'] = implode(",", $array['image']);
            }
            unset($array['Images']);
            unset($array['OldImage']);

            $Result = $this->Database->insert('restaurant_banner', $array);
            if ($Result) {
                $json = '{"status":"success","message":"Upload Successfully"}';
            } else {
                $json = '{"status":"error","message":"Something went wrong"}';
            }
            echo $json;
            exit;
        }
        $this->load->view(VENDOR_DIR . 'includes/header');
        $this->load->view(VENDOR_DIR . 'banner_management');
        $this->load->view(VENDOR_DIR . 'includes/footer');
    }

    // public function delivery_location()
    // {
    //     $this->load->view(VENDOR_DIR . 'includes/header');
    //     $this->load->view(VENDOR_DIR . 'delivery_location');
    //     $this->load->view(VENDOR_DIR . 'includes/footer');
    // }
    public function delivery_location() {
        $json = $this->input->post();
        if (!empty($json)) {
            $vendor_id = $json['vendor_id'];
            $array['delivery_location'] = $json['delivery_area1'];
            $this->Database->update('restaurant_details', $array, array('vendor_id' => $vendor_id));
            $this->load->view(VENDOR_DIR . 'includes/header');
            $this->load->view(VENDOR_DIR . 'delivery_location');
            $this->load->view(VENDOR_DIR . 'includes/footer');
        } else {

            $this->load->view(VENDOR_DIR . 'includes/header');
            $this->load->view(VENDOR_DIR . 'delivery_location');
            $this->load->view(VENDOR_DIR . 'includes/footer');
        }
    }

    public function transactions() {
        $this->load->view(VENDOR_DIR . 'includes/header');
        $this->load->view(VENDOR_DIR . 'transactions');
        $this->load->view(VENDOR_DIR . 'includes/footer');
    }

    public function commission() {
        $vendor_data = $this->session->userdata('Vendor');

        $qry = "SELECT * FROM `commission` WHERE store_type=" . $vendor_data->store_type;
        $commission = $this->Database->select_qry_array($qry);
        $data['commission'] = $commission;
        $this->load->view(VENDOR_DIR . 'includes/header');
        $this->load->view(VENDOR_DIR . 'commission', $data);
        $this->load->view(VENDOR_DIR . 'includes/footer');
    }

    function sendReqstToAirdtod() {
        $db = LoadDB();
        if (isset($_REQUEST)) {
            $user_details = GetUserDetails($_REQUEST['user_id']);
            $order = GetOrderById($_REQUEST['order_id']);
            $product_name = [];
            foreach ($order as $ord) {
                array_push($product_name, $ord->product_name);
            }
            $item_name = implode(',', $product_name);
            $vendor_details = GetUserDetails($_REQUEST['vendor_id']);
            $address_details = GetOrderAddressById($order[0]->delivery_address);
            // print_r($address[0]->house);die();
            if (isset($address_details[0]->building) && $address_details[0]->building != '') {
                $address = $address_details[0]->building;
            } elseif (isset($address_details[0]->building) && $address_details[0]->house != '') {
                $address = $address_details[0]->house;
            } elseif (isset($address_details[0]->building) && $address_details[0]->office != '') {
                $address = $address_details[0]->office;
            }
            $params = array(
                'receiver_name' => $user_details[0]->name,
                'receiver_phone_code' => '92',
                'receiver_phone' => $user_details[0]->mobile_number,
                'receiver_email' => $user_details[0]->email,
                'item_description' => $item_name,
                'pickup_address' => $vendor_details[0]->area,
                'pickup_lat' => $vendor_details[0]->latitude,
                'pickup_long' => $vendor_details[0]->longitude,
                'deliver_address' => $address . ',' . $address_details[0]->street . ',' . $address_details[0]->address,
                'deliver_lat' => $address_details[0]->latitude,
                'deliver_long' => $address_details[0]->longitude
            );
            $json = json_encode($params);
            $CondArray = array('id' => $_REQUEST['order_id']);
            $array['request_json'] = $json;
            $Result = $this->Database->update('orders', $array, $CondArray);
            $headers = array();

            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Authtoken: Alwafaa123';
            $headers[] = 'User-Agents: com.x-cargo';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://app.airdtod.com/Services/addReqiesthomeeats");
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $results = curl_exec($ch);
            print_r($results);
            die();
            curl_close($ch);
            $results = json_decode($results, true);
            print_r($results);
            die();
        }
    }

    public function delivery_boy() {
        $this->load->view(VENDOR_DIR . 'includes/header');
        $this->load->view(VENDOR_DIR . 'delivery_boy');
        $this->load->view(VENDOR_DIR . 'includes/footer');
    }

    public function add_deliveryboy() {
        $this->load->view(VENDOR_DIR . 'includes/header');
        $this->load->view(VENDOR_DIR . 'add_deliveryboy');
        $this->load->view(VENDOR_DIR . 'includes/footer');
    }

    public function reservations() {
        $this->load->view(VENDOR_DIR . 'includes/header');
        $this->load->view(VENDOR_DIR . 'reservations');
        $this->load->view(VENDOR_DIR . 'includes/footer');
    }

    public function menucategory() {
        $this->load->view(VENDOR_DIR . 'includes/header');
        $this->load->view(VENDOR_DIR . 'menucategory');
        $this->load->view(VENDOR_DIR . 'includes/footer');
    }

    public function menuexcelupload() {
        $this->load->view(VENDOR_DIR . 'includes/header');
        $this->load->view(VENDOR_DIR . 'menuexcelupload');
        $this->load->view(VENDOR_DIR . 'includes/footer');
    }

    public function exportmenu() {
        includePHPExcel();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel = PHPExcel_IOFactory::load(HOME_DIR . "images/menu-upload-excel-formate.xlsx");
        $sessvendor = GetSessionArrayVendor();

        $name = str_replace(' ', '-', $sessvendor->name);
        $name = $name . '-' . date('d-m-Y');

        $select = ",CD.category_name";
        $join = "LEFT JOIN category_positions CP ON CP.category_id=ML.category_id AND CP.vendor_id='$sessvendor->id' LEFT JOIN category_details CD ON CD.id=ML.category_id";
        $qry = "SELECT ML.* $select FROM `menu_list` ML $join WHERE ML.archive=0 AND ML.status=0 AND ML.vendor_id='$sessvendor->id'  ORDER BY CP.ordercat_view ASC,ML.order_view ASC";
        $dArray = $this->Database->select_qry_array($qry);
        $start = 2;
        for ($i = 0; $i < count($dArray); $i++) {
            $d = $dArray[$i];
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$start", $d->category_name)
                    ->setCellValue("B$start", $d->menu_name)
                    ->setCellValue("C$start", $d->menu_name_ar)
                    ->setCellValue("D$start", strip_tags($d->description))
                    ->setCellValue("E$start", strip_tags($d->description_ar))
                    ->setCellValue("F$start", $d->price)
                    ->setCellValue("G$start", $d->stock)
            ;
            $start++;
        }

        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.ms-excel; charset=UTF-8');
        header('Content-Disposition: attachment;filename="menu-list-' . $name . '.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public function set_lang() {

        $lang = $_REQUEST['choose_lang'];

        $this->session->set_userdata("language", $lang);

        $url = $_REQUEST['url'];
        redirect(base_url() . $url);
    }

    public function vendorLoginout() {
        $this->session->unset_userdata('Vendor');
        // session_destroy();
        redirect(base_url('Login/Vendor'));
    }

    public function testtt() {



        NewPartyorderSendVendorNotifaction($orderId = '14');
    }

}
