<?php

function GetRestaurantByLocation($location = '', $latitude = '', $longitude = '', $sort_by = '', $cuisine_id = '', $filter = '') {

    $db = LoadDB();
    $json = file_get_contents('php://input');
    $json = json_decode($json, TRUE);
    $category_id = !empty($json['category_id']) ? $json['category_id'] : '';
    $get_features = !empty($_REQUEST['get_features']) ? $_REQUEST['get_features'] : '';

    if (empty($latitude) || empty($longitude)) {
        die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => "Invalid location"))));
    }

    $order_type = !empty($_REQUEST['order_type']) ? $_REQUEST['order_type'] : '';


    $cond = '';
    $cond1 = '';
    if (!empty($cuisine_id)) {
        $cond1 = $cond1 . " AND rct.cuisine_id ='$cuisine_id'";
    }
    if (!empty($order_type)) {
        $cond1 = $cond1 . " AND restaurant_details.self_pickup ='1'";
    }if (!empty($category_id)) {
        $cond1 = $cond1 . " AND users.store_type ='$category_id'";
    }
    if ($sort_by == 2 || $filter == 3) {
        $cond = 'users.id DESC,';
    } elseif ($sort_by == 3) {
        $cond = 'restaurant_details.restaurant_name,';
    } elseif ($sort_by == 4) {
        $cond = 'restaurant_details.min_amount,';
    }

    // elseif($sort_by==4)
    // {
    //     $cond = 'restaurant_details.delivery_time,';
    // }


    $cond6 = '';
    $having = '';
    $promoCode = "";
    if ($filter == 1) {
        $cond6 = $cond6 . " AND restaurant_details.fastdelvry='1'";
    }if ($filter == 3) {
        $promoCode = ",(SELECT promocode_id FROM `promo_code`  WHERE date_from<=CURDATE() AND date_to>=CURDATE() AND user_type=1 AND status=0 AND archive=0 AND FIND_IN_SET(users.id,restaurant_id) LIMIT 1) AS promoCode";
        $having = " HAVING promoCode > 0";
    }


    if (!empty($get_features)) {
        $noteIn = "SELECT vendor_id FROM `features`";
        $Array = $db->Database->select_qry_array($noteIn);
    } else {





        $distnc = ",IFNULL(ROUND((6371 * acos( cos( radians($latitude) ) * cos( radians(users.latitude) ) * cos( radians(users.longitude) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians(users.latitude) ) ) ),2),0) AS distnc";
        $Qry = "SELECT restaurant_details.vendor_id,users.delivery_radius $distnc $promoCode
        FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
         WHERE user_type=2 AND archive=0 AND is_approved=1 AND status=1 $cond6  GROUP BY users.id $having ORDER BY distnc ASC  ";  // AND delivery_location !=''
        $Array = $db->Database->select_qry_array($Qry);
    }




    $ids = [];
    for ($i = 0; $i < count($Array); $i++) {
        $d = $Array[$i];
        if (!empty($d->delivery_radius) && $d->distnc <= $d->delivery_radius) {
            array_push($ids, $d->vendor_id);
        } else if ($d->distnc <= 4) {
            array_push($ids, $d->vendor_id);
        }
    }

    $arr = implode("','", $ids);


    $selectTime = ",restaurant_details.opening_time,restaurant_details.closing_time,restaurant_details.delivery_hours_st,restaurant_details.delivery_hours_et";
    $Qry = "SELECT users.id,users.image,users.area $selectTime ,restaurant_details.restaurant_name,restaurant_details.restaurant_name_ar,restaurant_details.about,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,restaurant_details.vendor_id,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount,restaurant_details.opening_time,restaurant_details.closing_time,restaurant_details.busy_status,restaurant_details.party_order
                    ,restaurant_details.isDelivery,restaurant_details.delivery_time_start,restaurant_details.delivery_time_ends,isPremium,restaurant_banner.image as banner_image,promo_code.promo_code,promo_code.discount," .
            " '' as distance" .
            " FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
                    left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
                    left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
                    left join restaurant_banner ON restaurant_banner.vendor_id=restaurant_details.vendor_id
                    left join promo_code on (restaurant_details.vendor_id=promo_code.restaurant_id AND promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE())
                    WHERE restaurant_details.vendor_id IN('" . $arr . "') $cond1  GROUP BY users.id ORDER BY $cond users.position ASC";

    // print_r($Qry);
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function getRestaurantDetailsAPI($rest_id) {
    $db = LoadDB();
    $selectTime = ",restaurant_details.opening_time,restaurant_details.closing_time,restaurant_details.delivery_hours_st,restaurant_details.delivery_hours_et";

    $Qry = "SELECT users.id,users.store_type,users.image,users.area $selectTime ,restaurant_details.restaurant_name,restaurant_details.about,restaurant_details.about,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,restaurant_details.vendor_id,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount,restaurant_details.opening_time,restaurant_details.closing_time,restaurant_details.busy_status,restaurant_details.party_order
                    ,restaurant_details.isDelivery,restaurant_details.delivery_time_start,restaurant_details.delivery_time_ends,isPremium,restaurant_banner.image as banner_image,promo_code.promo_code,promo_code.discount
                    FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
                    left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
                    left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
                    left join restaurant_banner ON restaurant_banner.vendor_id=restaurant_details.vendor_id
                    left join promo_code on (restaurant_details.vendor_id=promo_code.restaurant_id AND promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE())
                    WHERE users.id=" . $rest_id;
    // print_r($Qry);die();
    $Array = $db->Database->select_qry_array($Qry);
    return $Array[0];
}

function searchRestaurant($search_text = '', $location = '', $latitude = '', $longitude = '') {
    $json = file_get_contents('php://input');
    $json = json_decode($json, TRUE);
    $db = LoadDB();
    $search_text = trim($search_text);


    $conds = '';

    $store_id = !empty($json['store_id']) ? $json['store_id'] : '';
    if (!empty($store_id)) {
        $conds = $conds . " AND users.store_type='$store_id'";
    }


    $distnc = ",IFNULL(ROUND((6371 * acos( cos( radians($latitude) ) * cos( radians(users.latitude) ) * cos( radians(users.longitude) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians(users.latitude) ) ) ),2),0) AS distnc";

    $Qry = "SELECT restaurant_details.vendor_id,users.delivery_radius $distnc
        FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
         WHERE user_type=2 $conds AND archive=0 AND is_approved=1 AND status=1 GROUP BY users.id  ORDER BY distnc ASC";


    $Array = $db->Database->select_qry_array($Qry);

    $ids = [];
    for ($i = 0; $i < count($Array); $i++) {
        $d = $Array[$i];

        if (!empty($d->delivery_radius) && $d->distnc <= $d->delivery_radius) {
            array_push($ids, $d->vendor_id);
        } else if ($d->distnc <= 4) {
            array_push($ids, $d->vendor_id);
        }
    }

    $arr = implode("','", $ids);

    $selectTime = ",restaurant_details.opening_time,restaurant_details.closing_time,restaurant_details.delivery_hours_st,restaurant_details.delivery_hours_et";
    $Qry = "SELECT users.id,users.image,users.area $selectTime ,restaurant_details.restaurant_name,restaurant_details.restaurant_name_ar,restaurant_details.vendor_id,restaurant_details.about,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount,restaurant_details.isDelivery,restaurant_details.delivery_time_start,restaurant_details.delivery_time_ends,restaurant_details.party_order,restaurant_details.busy_status,restaurant_details.opening_time,restaurant_details.closing_time FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
            left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
            left join cuisine on cuisine.cuisine_id=rct.cuisine_id
        
            WHERE restaurant_details.vendor_id IN('" . $arr . "') AND (restaurant_name LIKE '%" . $search_text . "%' || BINARY(CONVERT(restaurant_name_ar USING latin1))  LIKE '%" . $search_text . "%')  GROUP BY users.id ORDER BY FIELD(restaurant_details.isPremium ,0, 1) DESC";

    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetCuisineName() {
    $db = LoadDB();
    $CondutaionArray = array('archive' => 0);
    $Qry = "SELECT cuisine_id,cuisine_name FROM `cuisine` WHERE archive=:archive";
    $CuisineArray = $db->Database->select_qry_array($Qry, $CondutaionArray);
    return $CuisineArray;
}

function GetMenuByCatid($catId = '', $vendor_id = '', $user_id = '') {

    $db = LoadDB();
    $qry = "SELECT * FROM `menu_list` LEFT JOIN add_to_cart ON add_to_cart.menu_id=menu_list.id AND add_to_cart.user_id='$user_id' WHERE menu_list.category_id='$catId' and menu_list.vendor_id='$vendor_id' and archive='0'";
    $dArray = $db->Database->select_qry_array($qry);

    return $dArray;
}

function GetAllCategories($restaurantId = '') {
    $db = LoadDB();

    $Qry = "SELECT category_details.id as category_id,category_details.category_name,category_details.category_name_ar,position FROM `category_details` JOIN `menu_list` ON `menu_list`.`category_id`=`category_details`.`id` LEFT JOIN category_positions CP ON CP.category_id=menu_list.category_id AND CP.vendor_id='$restaurantId' WHERE menu_list.vendor_id='$restaurantId' AND category_details.archive=0 AND category_details.status=0 GROUP BY category_id ORDER BY CP.ordercat_view ASC";
    $CategoryArray = $db->Database->select_qry_array($Qry);

    $Qry1 = "SELECT category_details.id as category_id,category_details.category_name,category_details.category_name_ar,position FROM `category_details`  LEFT JOIN `most_selling` ON `most_selling`.`category_id`=`category_details`.`id` LEFT JOIN category_positions CP ON CP.category_id=most_selling.category_id AND CP.vendor_id='$restaurantId' WHERE most_selling.vendor_id='$restaurantId' AND category_details.archive=0 AND category_details.status=0 GROUP BY most_selling.category_id  ORDER BY CP.ordercat_view ASC";
    $CategoryArray1 = $db->Database->select_qry_array($Qry1);

    $result = array_merge($CategoryArray, $CategoryArray1);

    usort($result, 'sortByOrder');

    return $result;
}

function GetMenuById($menuId = '') {

    $db = LoadDB();
    $qry = "SELECT * FROM `menu_list` WHERE id='$menuId' and archive='0'";

    $dArray = $db->Database->select_qry_array($qry);

    return $dArray;
}

function GetMenuSize($menu_id) {
    $db = LoadDB();
    $Qry1 = "SELECT * FROM `menu_sizes` WHERE menu_id='$menu_id' ORDER BY menu_size_id DESC";
    $Array1 = $db->Database->select_qry_array($Qry1);
    return $Array1;
}

function GetMenuAddon($menu_id) {
    $db = LoadDB();
    $Qry1 = "SELECT * FROM `menu_addons` WHERE menu_id='$menu_id' ORDER BY menu_addon_id DESC";
    $Array1 = $db->Database->select_qry_array($Qry1);
    return $Array1;
}

function GetMenuTopping($menu_id) {
    $db = LoadDB();
    $Qry1 = "SELECT * FROM `menu_topping` WHERE menu_id='$menu_id' ORDER BY menu_topping_id DESC";
    $Array1 = $db->Database->select_qry_array($Qry1);
    return $Array1;
}

function GetMenuDrink($menu_id) {
    $db = LoadDB();
    $Qry1 = "SELECT * FROM `menu_drink` WHERE menu_id='$menu_id' ORDER BY menu_drink_id DESC";
    $Array1 = $db->Database->select_qry_array($Qry1);
    return $Array1;
}

function GetMenuDip($menu_id) {
    $db = LoadDB();
    $Qry1 = "SELECT * FROM `menu_dips` WHERE menu_id='$menu_id' ORDER BY menu_dip_id DESC";
    $Array1 = $db->Database->select_qry_array($Qry1);
    return $Array1;
}

function GetMenuSide($menu_id) {
    $db = LoadDB();
    $Qry1 = "SELECT * FROM `menu_side` WHERE menu_id='$menu_id' ORDER BY menu_side_id DESC";
    $Array1 = $db->Database->select_qry_array($Qry1);
    return $Array1;
}

function GetOrderList($userid) {
    $db = LoadDB();
    $basePath = base_url() . 'uploads/vendor_images/';
    $Qry = "SELECT orders.id as order_id,OS.status_color,orders.vendor_id as homeleyfood_id,restaurant_name,restaurant_name_ar,DATE_FORMAT(orders.date,'%d %b %Y %h:%i %p') AS order_date,orderno,IF(image!='',CONCAT('$basePath',image),'') AS image_url,order_status FROM `orders` LEFT JOIN restaurant_details ON restaurant_details.vendor_id=orders.vendor_id LEFT JOIN users ON users.id=restaurant_details.vendor_id LEFT JOIN order_status OS ON OS.status_id=orders.order_status  WHERE orders.archive=0 and user_id=" . $userid . " order by orders.id desc";

    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function RemoveFromCartAPI($UserId = '', $MenuId = '') {
    $db = LoadDB();
    $ArrayCond = array(
        'menu_id' => $MenuId,
        'user_id' => $UserId,
    );

    $db->Database->delete('add_to_cart', $ArrayCond);
    return true;
}

//vendor

function total_new_order($userid) {
    $db = LoadDB();

    $customerAr = !empty($_REQUEST['customerAr']) ? $_REQUEST['customerAr'] : '';
    $userAr = !empty($_REQUEST['userAr']) ? $_REQUEST['userAr'] : '';
    if ($userAr->user_type == '4') {
        if ($userAr->deliveryboy_added_by == '1') {
            $_REQUEST['isCount'] = true;
            $_REQUEST['viewType'] = 'new_orders';
            $total_new_order = GetVendorOrderListForRider($userAr->id, $offset = 0);
            return $total_new_order;
        } else {
            $cond = " and orders.assign_rider=" . $userid . " AND orders.order_status IN(0,1)";
        }
    } else if ($userAr->user_type == '2') {
        $cond = " and orders.vendor_id=" . $userid . " AND orders.order_status IN(0)";
    }

    $datebefore3min = date("Y-m-d H:i:s", strtotime('-3 minutes', strtotime(date('Y-m-d H:i:s'))));
    $Qry = "SELECT SQL_CALC_FOUND_ROWS orders.id as order_id 
    FROM `orders` 
    WHERE orders.archive=0 $cond  AND orders.date < '$datebefore3min' order by orders.id desc";
    $Array = $db->Database->select_qry_array($Qry);
    $calRow = "SELECT FOUND_ROWS() AS TotalRows";
    $calRowArray = $db->Database->select_qry_array($calRow);
    return $calRowArray;
}

function total_ongoing_order($userid) {
    $db = LoadDB();


    $customerAr = !empty($_REQUEST['customerAr']) ? $_REQUEST['customerAr'] : '';
    $userAr = !empty($_REQUEST['userAr']) ? $_REQUEST['userAr'] : '';
    $cond = " and orders.vendor_id=" . $userid . " ";
    $statusCond = " AND orders.order_status IN (1,2,3,4,5,8,14) ";
    if ($userAr->user_type == '4') {
        $cond = " and orders.assign_rider=" . $userid . " ";
        if ($userAr->deliveryboy_added_by != '1') {
            $statusCond = " AND orders.order_status IN (2,3,4,5,8,14) "; // 1,8,2,14,5
        }
    }


    $Qry = "SELECT SQL_CALC_FOUND_ROWS orders.id as order_id 
    FROM `orders` 
    WHERE orders.archive=0 $cond $statusCond order by orders.id desc";
    $Array = $db->Database->select_qry_array($Qry);
    $calRow = "SELECT FOUND_ROWS() AS TotalRows";
    $calRowArray = $db->Database->select_qry_array($calRow);
    return $calRowArray;
}

function total_Completed_order($userid) {
    $db = LoadDB();

    $customerAr = !empty($_REQUEST['customerAr']) ? $_REQUEST['customerAr'] : '';
    $cond = " and orders.vendor_id=" . $userid . " ";
    if ($customerAr['user_type'] == '4') {
        $cond = " and orders.assign_rider=" . $userid . " ";
    }

    $Qry = "SELECT SQL_CALC_FOUND_ROWS orders.id as order_id 
    FROM `orders` 
    WHERE orders.archive=0 $cond AND orders.order_status IN (6,9) order by orders.id desc";
    $Array = $db->Database->select_qry_array($Qry);
    $calRow = "SELECT FOUND_ROWS() AS TotalRows";
    $calRowArray = $db->Database->select_qry_array($calRow);
    return $calRowArray;
}

function GetVendorOrderListForRider($userid, $offset = 0) {
    $db = LoadDB();
    $json = file_get_contents('php://input');
    $json = json_decode($json, TRUE);
    $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
    $userAr = !empty($_REQUEST['userAr']) ? $_REQUEST['userAr'] : '';
    $viewType = !empty($_REQUEST['viewType']) ? $_REQUEST['viewType'] : '';

    $deliveryboy_added_by = !empty($_REQUEST['deliveryboy_added_by']) ? $_REQUEST['deliveryboy_added_by'] : '';

    $having = "";
    $cond = "";
    if ($userAr->deliveryboy_added_by != '1') {
        //new_orders  completed ongoing
        if ($viewType == 'ongoing') {
            $cond = $cond . " AND O.order_status IN (2,8,14,5) and O.assign_rider='$VendorId' ";
        } else if ($viewType == 'new_orders') {
            $cond = $cond . " AND O.order_status IN (0,1) and O.assign_rider='$VendorId' ";
        } else {
            $cond = $cond . " AND O.order_status IN (6) and O.assign_rider='$VendorId' ";
        }
    } else {
        $cond = $cond . " AND RESTR.isDelivery=0 ";
        if ($viewType == 'ongoing') {
            $cond = $cond . " AND O.order_status IN (1,8,2,14,5) and O.assign_rider='$VendorId' ";
        } else if ($viewType == 'new_orders') {
            $having = " HAVING distnc < '$userAr->delivery_radius' ";
            $cond = $cond . " AND O.order_status IN (1,8,2,3,14) AND O.assign_rider IN (0) ";
        } else {
            $cond = $cond . " AND O.order_status IN (6) and O.assign_rider='$VendorId' ";
        }
    }



    if (empty($userAr->latitude) || empty($userAr->longitude)) {
        if ($userAr->deliveryboy_added_by != '1') {
            $userAr->latitude = '00';
            $userAr->longitude = '00';
        } else {
            die(json_encode(array("code" => 200, 'response' => array('status' => false, 'message' => 'Rider location not found.'))));
        }
    }


    $datebefore3min = date("Y-m-d H:i:s", strtotime('-3 minutes', strtotime(date('Y-m-d H:i:s'))));

    $select = " SQL_CALC_FOUND_ROWS O.id as order_id,orderno,O.store_type,O.scheduled_delivery_date,O.scheduled_delivery_time,O.scheduled_deliveryend_time,DATE_FORMAT(O.date,'%d %b %Y') AS order_date,DATE_FORMAT(O.date,'%h:%i %p') AS order_time,O.user_id,U.name as customer_name,CONCAT(UOD.mobile_code,UOD.mobile_number) as customer_mobile_number,UOD.address as delivery_location,RESTR.isDelivery,U.online_status,VNDR.area AS pickup_address ";
    $distnc = ",IFNULL(ROUND((6371 * acos( cos( radians($userAr->latitude) ) * cos( radians(UOD.latitude) ) * cos( radians(UOD.longitude) - radians($userAr->longitude) ) + sin( radians($userAr->latitude) ) * sin( radians(UOD.latitude) ) ) ),2),0) AS distnc";
    $join = "LEFT JOIN user_order_address UOD ON UOD.id=O.delivery_address  LEFT JOIN users U ON U.id=O.user_id LEFT JOIN restaurant_details RESTR ON RESTR.vendor_id=O.vendor_id LEFT JOIN users VNDR ON VNDR.id=O.vendor_id";
    $qry = "SELECT $select $distnc FROM `orders` O $join WHERE O.delivery_type='1' AND O.archive=0  $cond AND O.date < '$datebefore3min' $having ORDER BY O.id DESC";
    $Array = $db->Database->select_qry_array($qry);
    for ($i = 0; $i < count($Array); $i++) {
        $d = $Array[$i];

        if ($d->store_type != '1') {
            $d->scheduled_delivery_date = $d->scheduled_delivery_date != '0000-00-00' ? date('Y-m-d', strtotime($d->scheduled_delivery_date)) . ' ' . date('h:i A', strtotime($d->scheduled_delivery_time)) : '';
        }
        if (!empty($d->scheduled_delivery_date) && $d->scheduled_deliveryend_time != '00:00:00') {
            $d->scheduled_delivery_date = $d->scheduled_delivery_date . ' - ' . date('h:i A', strtotime($d->scheduled_deliveryend_time));
        }
    }
    if (!empty($_REQUEST['isCount'])) {
        $calRow = "SELECT FOUND_ROWS() AS TotalRows";
        $calRowArray = $db->Database->select_qry_array($calRow);
        return $calRowArray;
    }

    return $Array;
}

function GetVendorOrderList($userid, $order_status, $offset = 0) {
    $db = LoadDB();
    $basePath = base_url() . 'uploads/user_images/';

    $customerAr = !empty($_REQUEST['customerAr']) ? $_REQUEST['customerAr'] : '';
    $cond = " and orders.vendor_id=" . $userid . " ";
    if ($customerAr['user_type'] == '4') {
        $cond = " and orders.assign_rider=" . $userid . " ";
    }

    $datebefore3min = date("Y-m-d H:i:s", strtotime('-3 minutes', strtotime(date('Y-m-d H:i:s'))));

    $Qry = "SELECT SQL_CALC_FOUND_ROWS orders.id as order_id,orderno,orders.store_type,orders.scheduled_delivery_date,orders.scheduled_delivery_time,orders.scheduled_deliveryend_time,DATE_FORMAT(orders.date,'%d %b %Y') AS order_date,DATE_FORMAT(orders.date,'%h:%i %p') AS order_time,orders.user_id,users.name as customer_name,CONCAT(users.mobile_code,users.mobile_number) as customer_mobile_number,user_order_address.address as delivery_location,users.online_status,VNDR.area AS pickup_address,orders.delivery_type AS is_self_pickup 
    FROM `orders` LEFT JOIN restaurant_details ON restaurant_details.vendor_id=orders.vendor_id 
    LEFT JOIN users ON users.id=orders.user_id 
      LEFT JOIN users VNDR ON VNDR.id=orders.vendor_id  
    LEFT JOIN user_order_address ON user_order_address.id=orders.delivery_address 
    WHERE orders.archive=0 $cond AND orders.order_status=" . $order_status . " AND orders.date < '$datebefore3min' order by orders.id desc  LIMIT $offset,10";
    $Array = $db->Database->select_qry_array($Qry);
    for ($i = 0; $i < count($Array); $i++) {
        $d = $Array[$i];
        if ($d->store_type != '1') {
            $d->scheduled_delivery_date = $d->scheduled_delivery_date != '0000-00-00' ? date('Y-m-d', strtotime($d->scheduled_delivery_date)) . ' ' . date('h:i A', strtotime($d->scheduled_delivery_time)) : '';
        }
        if (!empty($d->scheduled_delivery_date) && $d->scheduled_deliveryend_time != '00:00:00') {
            $d->scheduled_delivery_date = $d->scheduled_delivery_date . ' - ' . date('h:i A', strtotime($d->scheduled_deliveryend_time));
        }
    }
    return $Array;
}

function GetVendorOngoingOrderList($userid, $offset) {
    $db = LoadDB();
    $customerAr = !empty($_REQUEST['customerAr']) ? $_REQUEST['customerAr'] : '';
    $cond = " and orders.vendor_id=" . $userid . " ";
    if ($customerAr['user_type'] == '4') {
        $cond = " and orders.assign_rider=" . $userid . " ";
    }



    $basePath = base_url() . 'uploads/user_images/';
    $Qry = "SELECT SQL_CALC_FOUND_ROWS orders.id as order_id,orderno,orders.store_type,orders.scheduled_delivery_date,orders.scheduled_delivery_time,orders.scheduled_deliveryend_time,DATE_FORMAT(orders.date,'%d %b %Y') AS order_date,DATE_FORMAT(orders.date,'%h:%i %p') AS order_time,orders.user_id,users.name as customer_name,CONCAT(users.mobile_code,users.mobile_number) as customer_mobile_number,user_order_address.address as delivery_location,users.online_status,VNDR.area AS pickup_address,orders.delivery_type AS is_self_pickup  
    FROM `orders` LEFT JOIN restaurant_details ON restaurant_details.vendor_id=orders.vendor_id 
    LEFT JOIN users ON users.id=orders.user_id 
    LEFT JOIN user_order_address ON user_order_address.id=orders.delivery_address
        LEFT JOIN users VNDR ON VNDR.id=orders.vendor_id  
    WHERE orders.archive=0 $cond AND orders.order_status IN (1,2,3,4,5,8,14) order by orders.id desc LIMIT $offset,10";
    $Array = $db->Database->select_qry_array($Qry);
    for ($i = 0; $i < count($Array); $i++) {
        $d = $Array[$i];
        if ($d->store_type != '1') {
            $d->scheduled_delivery_date = $d->scheduled_delivery_date != '0000-00-00' ? date('Y-m-d', strtotime($d->scheduled_delivery_date)) . ' ' . date('h:i A', strtotime($d->scheduled_delivery_time)) : '';
        }
        if (!empty($d->scheduled_delivery_date) && $d->scheduled_deliveryend_time != '00:00:00') {
            $d->scheduled_delivery_date = $d->scheduled_delivery_date . ' - ' . date('h:i A', strtotime($d->scheduled_deliveryend_time));
        }
    }
    return $Array;
}

function GetVendorCompletedOrderList($userid, $order_status, $offset = 0) {
    $db = LoadDB();

    $customerAr = !empty($_REQUEST['customerAr']) ? $_REQUEST['customerAr'] : '';
    $cond = " and orders.vendor_id=" . $userid . " ";
    if ($customerAr['user_type'] == '4') {
        $cond = " and orders.assign_rider=" . $userid . " ";
    }


    $basePath = base_url() . 'uploads/user_images/';
    $Qry = "SELECT SQL_CALC_FOUND_ROWS orders.id as order_id,orderno,orders.store_type,orders.scheduled_delivery_date,orders.scheduled_delivery_time,orders.scheduled_deliveryend_time,DATE_FORMAT(orders.date,'%d %b %Y') AS order_date,DATE_FORMAT(orders.date,'%h:%i %p') AS order_time,orders.user_id,users.name as customer_name,CONCAT(users.mobile_code,users.mobile_number) as customer_mobile_number,user_order_address.address as delivery_location,users.online_status,VNDR.area AS pickup_address,orders.delivery_type AS is_self_pickup  
    FROM `orders` LEFT JOIN restaurant_details ON restaurant_details.vendor_id=orders.vendor_id 
    LEFT JOIN users ON users.id=orders.user_id  
       LEFT JOIN users VNDR ON VNDR.id=orders.vendor_id  
    LEFT JOIN user_order_address ON user_order_address.id=orders.delivery_address
    WHERE orders.archive=0 $cond AND orders.order_status IN (6,9) order by orders.id desc LIMIT $offset,10";
    $Array = $db->Database->select_qry_array($Qry);
    $Array = $db->Database->select_qry_array($Qry);
    for ($i = 0; $i < count($Array); $i++) {
        $d = $Array[$i];
        if ($d->store_type != '1') {
            $d->scheduled_delivery_date = $d->scheduled_delivery_date != '0000-00-00' ? date('Y-m-d', strtotime($d->scheduled_delivery_date)) . ' ' . date('h:i A', strtotime($d->scheduled_delivery_time)) : '';
        }
        if (!empty($d->scheduled_delivery_date) && $d->scheduled_deliveryend_time != '00:00:00') {
            $d->scheduled_delivery_date = $d->scheduled_delivery_date . ' - ' . date('h:i A', strtotime($d->scheduled_deliveryend_time));
        }
    }
    return $Array;
}

// function GetVendorOrderDetails($userid,$order_id) {
//     $db = LoadDB();
//     $basePath = base_url().'uploads/user_images/';
//     $Qry = "SELECT orders.id as order_id,DATE_FORMAT(orders.date,'%d %b %Y') AS order_date,DATE_FORMAT(orders.date,'%h:%i %p') AS order_time,orders.user_id,users.name as customer_name,CONCAT(user_order_address.mobile_code,user_order_address.mobile_number) as customer_mobile_number,orders.delivery_address,orders.order_status
//     FROM `orders` LEFT JOIN restaurant_details ON restaurant_details.vendor_id=orders.vendor_id 
//     LEFT JOIN users ON users.id=orders.user_id 
//     LEFT JOIN user_order_address ON user_order_address.id=orders.delivery_address 
//     WHERE orders.archive=0 and orders.vendor_id=" . $userid . " AND orders.id=" . $order_id . " order by orders.id desc";
//     $Array = $db->Database->select_qry_array($Qry);
//     $dArray = !empty($Array) ? $Array[0] : [];
//     return $dArray;
// }
function GetVendorOrderDetails($userid, $order_id) {
    $db = LoadDB();
    $basePath = base_url() . 'uploads/user_images/';
    $Qry = "SELECT orders.id as order_id,orderno,orders.scheduled_deliveryend_time,DATE_FORMAT(orders.date,'%d %b %Y') AS order_date,DATE_FORMAT(orders.date,'%h:%i %p') AS order_time,orders.user_id,users.name as customer_name,CONCAT(user_order_address.mobile_code,user_order_address.mobile_number) as customer_mobile_number,orders.delivery_address,orders.mode_of_payment as payment_mode,orders.service_charge as delivery_charge,orders.total_price as total_amount,orders.order_status
        ,orders.special_request
    FROM `orders` LEFT JOIN restaurant_details ON restaurant_details.vendor_id=orders.vendor_id 
    LEFT JOIN users ON users.id=orders.user_id 
    LEFT JOIN user_order_address ON user_order_address.id=orders.delivery_address 
    WHERE orders.id=" . $order_id . " order by orders.id desc"; // and orders.vendor_id=" . $userid . "

    $Array = $db->Database->select_qry_array($Qry);
    $dArray = !empty($Array) ? $Array[0] : [];
    return $dArray;
}

function GetOrderDetailsByOrderIdApi($order_id = '') {
    $db = LoadDB();
    $Qry = "SELECT *,order_details.price AS ord_del_price FROM `order_details` LEFT JOIN menu_list on menu_list.id=order_details.product_id  WHERE order_details.archive=0 and `order_details`.order_id='$order_id'";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetStoreType($id = '') {
    $db = LoadDB();
    $Qry = "SELECT store_type FROM `users` WHERE id='" . $id . "'";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function APINotifactionCount() {
    $db = LoadDB();
    $json = file_get_contents('php://input');
    $json = json_decode($json, TRUE);
    $VendorId = !empty($json['vendor_id']) ? $json['vendor_id'] : '';
    $UserArray = GetCustomerArrayById($VendorId);

    $datebefore3min = date("Y-m-d H:i:s", strtotime('-3 minutes', strtotime(date('Y-m-d H:i:s'))));
    $cond = '';
    $isRead = '';

    if ($UserArray['user_type'] == '2') {
        $cond = " AND `vendor_id` = '$VendorId' AND is_read=0";
        $isRead = 'is_read';
    } else if ($UserArray['user_type'] == '4') {
        $cond = " AND `assign_rider` = '$VendorId' AND is_read_rider=0";
        $isRead = 'is_read_rider AS is_read';
    }



    $sql = '';
    $sql = $sql . "SELECT id AS order_id,CONCAT(party_orderno,' New order received.') AS title,date,$isRead,'1' AS type  FROM `party_order` WHERE 1 AND archive=0  $cond UNION ALL ";

    $sql = $sql . "SELECT id AS order_id,CONCAT(orderno,' New order received.') AS title,date,$isRead,'2' AS type  FROM `orders` WHERE 1 AND archive=0 AND date < '$datebefore3min' $cond UNION ALL ";

    $sql = substr($sql, 0, -11);
    $select = "MN.order_id,MN.title,MN.date,MN.is_read,MN.type";
    $join = " ";
    $mainSql = "SELECT $select FROM ($sql) MN $join ORDER BY MN.date DESC";
    $nArray = $db->Database->select_qry_array($mainSql);
    return $nArray;
}

function GetOrderSubmitResponce2XXXXXXX($orderId = '') {
    $order = GetOrderByOrderId($orderId);
    if (empty($order)) {
        return false;
    }
    $order = $order[0];
    $Restaurant = GetRestaurantById($order->vendor_id);
    $Restaurant = !empty($Restaurant) ? $Restaurant[0] : '';
    $vendor = GetusersById($order->vendor_id);
    $return['msgOrderDelv'] = '';
    if ($order->delivery_type != '2' && $order->store_type == '1') {

        $address_details = GetuseraddressBy($order->delivery_address);
        $delivery_time = !empty($Restaurant->delivery_time) ? $Restaurant->delivery_time : '0';
        $distance = $order->distance_ord;
        $delvTimeexpt = $distance * RIDER_DELIVER_TIME_PER_KM;
        $return['msgOrderDelv'] = 'Order Delivery In: ' . convertToHoursMins($delivery_time + $delvTimeexpt, '%02d hours %02d minutes');
    }
    return $return;
}

function GetOrderSubmitResponce($orderId = '') {
    $order = GetOrderByOrderId($orderId);
    if (empty($order)) {
        return false;
    }
    $order = $order[0];
    $Restaurant = GetRestaurantById($order->vendor_id);
    $Restaurant = !empty($Restaurant) ? $Restaurant[0] : '';
    $vendor = GetusersById($order->vendor_id);
    $return['msgOrderDelv'] = '';
    if ($order->delivery_type != '2' && $order->store_type == '1') {
        $Restaurant->delivery_time = '20'; 
        $address_details = GetuseraddressBy($order->delivery_address);
        $delivery_time = (!empty($Restaurant->delivery_time) ? $Restaurant->delivery_time : '0') * 60;
        $distanceRunTime = distanceTime($vendor->latitude, $vendor->longitude, $address_details->latitude, $address_details->longitude, $unit = 'K');
        $distanceRunTime = (int) ($distanceRunTime + $delivery_time) / 60;
        $distanceRunTime = (int) $distanceRunTime;
        if ($distanceRunTime > 60) {
            $return['msgOrderDelv'] = "ooos your order is running late we are trying our best for it to get to you as soon as possible.";
        } else {
            $return['msgOrderDelv'] = 'Order Delivery In: ' . convertToHoursMins($distanceRunTime, '%02d hours %02d minutes');
        }
    }
    return $return;
}

?>