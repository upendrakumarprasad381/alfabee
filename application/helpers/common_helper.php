<?php

function LoadDB() {
    $ci = & get_instance();
    $ci->load->model('Database');
    return $ci;
}

function includePHPExcel() {
    require_once HOME_DIR . 'application/third_party/PHPExcel/Classes/PHPExcel.php';
}

function GetAdditionalSession() {
    $ci = & get_instance();
    $SessionArray = $ci->session->userdata('AdditionalSession');
    return $SessionArray;
}

function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}

function getsystemlanguage() {
    $ci = & get_instance();
    $language = $ci->session->userdata('language');
    if (empty($language)) {
        $language = 'en';
    }
    return $language;
}

function listMenuByRestaurantAPISearch($vendor_id = '', $category_id = '') {
    $db = LoadDB();

    $search_text = !empty($_REQUEST['search_text']) ? $_REQUEST['search_text'] : '';
    $arrayname = [];
    //$Qry = "SELECT * FROM `menu_list` LEFT JOIN promo_code ON (promo_code.offer_addedby=menu_list.vendor_id AND promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE()) JOIN most_selling ON most_selling.vendor_id=menu_list.vendor_id WHERE menu_list.vendor_id='$vendor_id' AND (menu_list.category_id='$category_id' OR most_selling.category_id='$category_id') AND menu_list.archive=0";
    $Qry = "SELECT * FROM most_selling WHERE most_selling.vendor_id='$vendor_id' AND category_id='$category_id'";
    $Array = $db->Database->select_qry_array($Qry);
    // print_r($Qry);die();

    if (count($Array) > 0) {
        $cat_id = $Array[0]->category_id;
        // $most = $Array[0]->menu_id;
        // $value = explode(',',$most);
        // foreach($value as $v)
        // {
        $Qry1 = "SELECT menu_list.id,menu_list.stock,most_selling.vendor_id,menu_list.category_id,menu_list.menu_name,menu_list.menu_name_ar,description,price,image,choice,menu_list.status,menu_list.archive,menu_list.lastupdate,menu_list.timestamp,promocode_id,promo_code,
            title,promocode_type,purchase_amount,date_from,date_from,discount,user_type,offer_type,offer_addedby,promo_code.menu_id,restaurant_id,offer_image 
            FROM `menu_list` LEFT JOIN promo_code ON (promo_code.offer_addedby=menu_list.vendor_id AND promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE()) LEFT JOIN most_selling ON (most_selling.vendor_id=menu_list.vendor_id AND most_selling.menu_id=menu_list.id) LEFT JOIN category_positions CP ON CP.category_id=menu_list.category_id AND CP.vendor_id='$vendor_id' WHERE most_selling.category_id='$cat_id' AND most_selling.vendor_id='$vendor_id' AND menu_list.archive=0 AND (menu_list.menu_name LIKE '%$search_text%' || BINARY(CONVERT(menu_list.menu_name_ar USING latin1))  LIKE '%$search_text%')  AND menu_list.status=0 GROUP BY menu_list.id ORDER BY CP.ordercat_view ASC";
        // print_r($Qry1);die();
        $Array1 = $db->Database->select_qry_array($Qry1);
        // array_push($arrayname,$Array1);
        // }
        // $Qry2 = "SELECT * FROM `menu_list` LEFT JOIN promo_code ON (promo_code.offer_addedby=menu_list.vendor_id AND promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE())   WHERE menu_list.vendor_id='$vendor_id' AND menu_list.category_id!='$cat_id' AND menu_list.archive=0";
        // $Array2 = $db->Database->select_qry_array($Qry2);
        // print_r($Array2);die();
        // array_push($arrayname,$Array2);
        // $dat = array_merge($Array1,$Array2);
    } else {
        $Qry2 = "SELECT * FROM `menu_list` LEFT JOIN promo_code ON (promo_code.offer_addedby=menu_list.vendor_id AND promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE()) LEFT JOIN category_positions CP ON CP.category_id=menu_list.category_id AND CP.vendor_id='$vendor_id' WHERE menu_list.vendor_id='$vendor_id' AND menu_list.category_id='$category_id' AND menu_list.archive=0 AND (menu_list.menu_name LIKE '%$search_text%' || BINARY(CONVERT(menu_list.menu_name_ar USING latin1))  LIKE '%$search_text%') AND menu_list.status=0 GROUP BY menu_list.id ORDER BY CP.ordercat_view ASC";
        $Array1 = $db->Database->select_qry_array($Qry2);
        // print_r($Array1);die();
        // array_push($arrayname,$Array2);
        //$dat = $Array2;
    }
    return $Array1;
}

function CheckPermission() {
    $db = LoadDB();
    $Ci = & get_instance();
    $AdminSession = $Ci->session->userdata('Admin');
    $result_list = array();
    $page_id = '';
    if ($db->uri->segment(2) == '') {
        $link = $db->uri->segment(1);
    } else {
        $link = $db->uri->segment(1) . '/' . $db->uri->segment(2);
    }
    $sqls = "select id from sidebar_menu where link='" . $link . "'";
    $array_list = $db->Database->select_qry_array($sqls);
    if (!empty($array_list)) {
        $page_id = $array_list[0]->id;
        $sql1 = "SELECT * FROM staff_role where staff_id='" . $AdminSession->id . "'";
        $row = $db->Database->select_qry_array($sql1);
        if (!empty($row)) {
            for ($i = 0; $i < count($row); $i++) {
                $result_list[] = $row[$i]->menu_id;
            }
        }
    }
    if (!in_array($page_id, $result_list)) {
        redirect(base_url('Admin/ERROR_404'));
    }
}

function DecimalAmount($Amount = 0, $expl = 0) {
    return sprintf('%0.2f', $Amount);
}

function send_mail($email_id, $subject, $messages, $cc = '', $file_path = '') {
    $ci = & get_instance();
    $ci->load->library('email');
    $config['protocol'] = 'smtp';
    $config['mail_path'] = 'ssl://smtp.googlemail.com';
    $config['smtp_host'] = 'ssl://smtp.googlemail.com';
    $config['smtp_port'] = 465;
    $config['smtp_user'] = 'shamzammail@gmail.com';
    $config['smtp_pass'] = 'shamzam123';
    $config['charset'] = "utf-8";
    $config['mailtype'] = "html";


    $ci->email->set_newline("\r\n");
    // $ci->email->initialize($config);



    error_reporting(0);
    $ci->email->from('info@alfabee.com', 'Alfabee');
    $ci->email->to($email_id);
    if ($cc != '') {
        $ci->email->cc($cc);
    }
    $ci->email->subject($subject);
    $ci->email->message($messages);
    if ($file_path != '') {
        $ci->email->attach($file_path);
    }
    $ci->email->send();
}

function send_pushnotification($device_id = array(), $subject = '', $message = '', $identifier = '', $argument = array()) {
    $device_id = reformateMydeviceId($device_id);
    $notification['title'] = $subject;
    $notification['body'] = $message;
    $notification['sound'] = 'Default';
    $argument['identifier'] = $identifier;
    $argument['click_action'] = 'FLUTTER_NOTIFICATION_CLICK';
    $dArray = $device_id;
    $ch = curl_init("https://fcm.googleapis.com/fcm/send");
    $arrayToSend = array('registration_ids' => $dArray, 'notification' => $notification, 'priority' => 'high', 'sound' => 'Default', 'data' => $argument);
    $json = json_encode($arrayToSend);
    $headers = array();
    $headers[] = 'Content-Type: application/json';
    // $headers[] = 'Authorization: key=AAAA7Ijjqy0:APA91bGiqxygUP01HVH4zmcGqLIOlAlhzvJWbOWFFrhREPlt8nmwYFkfCZeHPKXQtLHDDNsGAwTA2-j76DN03jDte__I5QEJN-SQiEsH-h86WGpHhpDpZl_S-QvgXZVdaNW1h9kUxnyu'; // key here
    $headers[] = 'Authorization: key=AAAA2CdvUlU:APA91bHt_gmT0sj40_KhrHaRuxOmBdD5gRl8fNNa8B1dCjT10hEc3ZZmrdeKn6bv03Z79BX-HpcUZ0ewR_SFDeleY8nCjsvJeS0Bh1G0h3ct8GYjrK433H9Cwf7eBRnTTcFnPgQM-uZL'; // key here


    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_exec($ch);
    curl_close($ch);
}
function send_pushnotification_public($device_id = array(), $subject = '', $message = '', $identifier = '', $argument = array()) {
  //  $device_id = reformateMydeviceId($device_id);
    $notification['title'] = $subject;
    $notification['body'] = $message;
    $notification['sound'] = 'Default';
    $argument['identifier'] = $identifier;
    $argument['click_action'] = 'FLUTTER_NOTIFICATION_CLICK';
    $dArray = $device_id;
    $ch = curl_init("https://fcm.googleapis.com/fcm/send");
    $arrayToSend = array('registration_ids' => $dArray, 'notification' => $notification, 'priority' => 'high', 'sound' => 'Default', 'data' => $argument);
    $json = json_encode($arrayToSend);
    $headers = array();
    $headers[] = 'Content-Type: application/json';
    // $headers[] = 'Authorization: key=AAAA7Ijjqy0:APA91bGiqxygUP01HVH4zmcGqLIOlAlhzvJWbOWFFrhREPlt8nmwYFkfCZeHPKXQtLHDDNsGAwTA2-j76DN03jDte__I5QEJN-SQiEsH-h86WGpHhpDpZl_S-QvgXZVdaNW1h9kUxnyu'; // key here
    $headers[] = 'Authorization: key=AAAA2CdvUlU:APA91bHt_gmT0sj40_KhrHaRuxOmBdD5gRl8fNNa8B1dCjT10hEc3ZZmrdeKn6bv03Z79BX-HpcUZ0ewR_SFDeleY8nCjsvJeS0Bh1G0h3ct8GYjrK433H9Cwf7eBRnTTcFnPgQM-uZL'; // key here


    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_exec($ch);
    curl_close($ch);
}
function GetMenuPermission($MenuId = '', $full = false) {
    $db = LoadDB();
    $controller = $db->router->fetch_class();
    $method = $db->router->fetch_method();
    $Session = $db->session->userdata('Admin');
    $Search = $controller . '/' . $method;
    $condSearch = "";
    if (!in_array($Session->id, ADMIN_PERMISSION_ARRAY)) {
        $condSearch = $condSearch . " AND UP.user_id='$Session->id' ";
    }
    $MenuIdCond = !empty($MenuId) ? " SM.id='$MenuId' " : " SM.link='$Search' ";
    $Select = ",UP.additional_json";
    $join = " LEFT JOIN user_permission UP ON UP.menu_id=SM.id ";
    $qry = "SELECT SM.* $Select FROM `sidebar_menu` SM $join WHERE $MenuIdCond $condSearch ORDER BY SM.order_view ASC";
    $MenuArray = $db->Database->select_qry_array($qry);
    $additional_json = [];
    if (!empty($MenuArray)) {
        $MenuArray = $MenuArray[0];
        if (in_array($Session->id, ADMIN_PERMISSION_ARRAY)) {
            $menu_json = json_decode($MenuArray->menu_json, true);
            $menu_json = !empty($menu_json) ? $menu_json : [];
            for ($i = 0; $i < count($menu_json); $i++) {
                $additional_json[] = $menu_json[$i]['actionId'];
            }
        } else {
            $additional_json = !empty($MenuArray->additional_json) ? json_decode($MenuArray->additional_json) : [];
            $additional_json = !empty($additional_json) && is_array($additional_json) ? $additional_json : [];
        }
        $MenuArray->additional_json = $additional_json;
    }
    if (!empty($full)) {
        return $MenuArray;
    } else {
        return $additional_json;
    }
}

function GetMenus() {
    $db = LoadDB();
    $Qry = "SELECT * FROM `sidebar_menu` WHERE archive='0' and parent='0' order by order_view asc";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetUserMenu($menuid, $userid) {
    $db = LoadDB();
    $Qrys = "SELECT * FROM `user_permission` WHERE user_id='" . $userid . "' and menu_id='" . $menuid . "'";
    $Array = $db->Database->select_qry_array($Qrys);
    return $Array;
}

function GetSubMenu($id = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `sidebar_menu` WHERE parent='" . $id . "' and archive='0' order by order_view asc";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetOrderAddressById($id = '') {
    $db = LoadDB();
    $saved_address = "SELECT * FROM `user_order_address` WHERE id='$id'   ORDER BY id DESC LIMIT 1";
    $address_details = $db->Database->select_qry_array($saved_address);
    return $address_details;
}

function GetAllAddress($id = '') {
    $db = LoadDB();
    $saved_address = "SELECT * FROM `user_order_address` WHERE user_id=$id AND archive=0  ORDER BY id DESC";
    $address_details = $db->Database->select_qry_array($saved_address);
    return $address_details;
}

function GetAddressArrayByAddressId($AddressId) {
    $db = LoadDB();
    $CondutaionArray = array('id' => $AddressId);
    $Qry = "SELECT * FROM user_order_address WHERE `id` =:id";
    $Array = $db->Database->select_qry_array($Qry, $CondutaionArray);
    if (isset($Array[0])) {
        return $Array[0];
    }
}

function GetCustomerDetailsAll() {
    $db = LoadDB();
    $Qry = "SELECT * FROM `users` WHERE archive=0 AND user_type = 3 ORDER BY name ASC";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetRestaurantDetailsAll() {
    $db = LoadDB();
    $Qry = "SELECT users.id,restaurant_name FROM `users` JOIN restaurant_details ON restaurant_details.vendor_id = users.id WHERE archive=0 AND is_approved=1 ORDER BY restaurant_name ASC";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function get_timedifference($datetime = '') {

    $datetime = $datetime == '' ? date('Y-m-d H:i:s') : $datetime;
    $today = date('Y-m-d H:i:s');
    $start_date = new DateTime($today);
    $since_start = $start_date->diff(new DateTime($datetime));
    $string = '';
    if ($since_start->y > 0) {
        if ($since_start->y == 1) {
            $string = $since_start->y . ' year ago';
        } else {
            $string = $since_start->y . ' years ago';
        }
    } else if ($since_start->m > 0) {
        if ($since_start->m == 1) {
            $string = $since_start->m . ' month ago';
        } else {
            $string = $since_start->m . ' months ago';
        }
    } else if ($since_start->d > 0) {
        if ($since_start->d == 1) {
            $string = $since_start->d . ' day ago';
        } else {
            $string = $since_start->d . ' days ago';
        }
    } else if ($since_start->h > 0) {
        if ($since_start->h == 1) {
            $string = $since_start->h . ' hour ago';
        } else {
            $string = $since_start->h . ' hours ago';
        }
    } else if ($since_start->i > 0) {
        if ($since_start->i == 1) {
            $string = $since_start->i . ' min ago';
        } else {
            $string = $since_start->i . ' mins ago';
        }
    } else if ($since_start->s > 0) {
        if ($since_start->s == 1) {
            $string = $since_start->s . ' second ago';
        } else {
            $string = $since_start->s . ' seconds ago';
        }
    }
    return $string;
}

function updateNotification() {
    $db = LoadDB();
    try {
        $updatedata = array('notification_read' => 1);
        $where = array('notification_id' => $_POST['id']);
        $result = $db->Database->update('notification', $updatedata, $where);

        if ($result) {
            $url = $_POST['redirect_url'];
            $resp_array = array("status" => 'success', "url" => $url);
        } else {
            $resp_array = '{"status":"error","message":"Try Again"}';
        }
        echo json_encode($resp_array);
    } catch (Exception $e) {
        echo json_encode($e->getTraceAsString());
    }
}

function send_sms($mobile_no, $messagqwe = '') {
    header('Content-Type: text/html; charset=utf-8');
    $USER_NAME = SMS_USER_NAME;
    $SenderId = SMS_SENDER_ID;
    $SMS_PASSWORD = SMS_PASSWORD;

    $messagqwe1 = $messagqwe;
    $mobile_no = $mobile_no;
    $string = "http://onlysms.ae/api/api_http2.php?username=$USER_NAME&password=$SMS_PASSWORD&senderid=$SenderId&to=$mobile_no&text=$messagqwe1&type=unicode";
    $url = preg_replace("/ /", "%20", $string);
    $sendSms = file_get_contents($url);
    return $sendSms;
}

function GetCustomerArrayById($CustomerId = 0) {
    $basePath = base_url();
    $db = LoadDB();

    $join = " LEFT JOIN restaurant_details RD ON RD.vendor_id=u.id";
    $Qry = "SELECT u.*,RD.restaurant_name FROM `users` u $join WHERE u.id='$CustomerId'";

    $Array = $db->Database->select_qry_array($Qry);
    if (!empty($Array)) {
        $Array = $Array[0];
        if ($Array->user_type == 3) {
            $profileImg = 'uploads/user_images/' . $Array->image;
        } else {
            $profileImg = 'uploads/vendor_images/' . $Array->image;
        }
        $Array->profile_image_url = is_file(HOME_DIR . $profileImg) ? base_url($profileImg) : base_url(DEFAULT_LOGO_USER);
    }

    $data = '';
    if (!empty($Array)) {
        $data = array(
            'user_id' => $Array->id,
            'email' => $Array->email,
            'full_name' => $Array->name,
            'mobile_code' => $Array->mobile_code,
            'mobile_number' => $Array->mobile_number,
            'user_type' => $Array->user_type,
            'profile_image_url' => $Array->profile_image_url,
            'location' => $Array->area,
            'delivery_radius' => $Array->delivery_radius,
            'device_id' => $Array->device_id,
            'status' => $Array->status,
            'restaurant_name' => $Array->restaurant_name,
            'busy_status' => $Array->busy_status,
        );
        // die(json_encode(array('status' => true, 'message' => 'successfullly', 'USER' => $data)));
    }
    return $data;
}

function GetusersById($CustomerId = 0) {
    $db = LoadDB();
    $qry = "SELECT * FROM `users` u WHERE u.id='$CustomerId'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : '';
    return $dArray;
}

function common_upload_file_only($array) {
    $return = fileonly_upload_common($array);
    echo $return;
}

function fileonly_upload_common($array) {

    $six_digit = mt_rand(100000, 999999);
    $destination = HOME_DIR . 'uploads/' . $_REQUEST['location'] . '/' . $six_digit . '_' . $_FILES['common_file']['name'];
    $json = '';
    $delete_path = HOME_DIR . 'uploads/' . $_REQUEST['location'] . '/' . $delete_file_name = $_REQUEST['delete_img'];
    if (is_file($delete_path)) {
        unlink($delete_path);
    }
    $extension = pathinfo($_FILES['common_file']['name'], PATHINFO_EXTENSION);
    if ($extension == 'pdf' || $extension == 'PDF' || $extension == 'docx' || $extension == 'doc' || $extension == 'ppt' || $extension == 'jpeg' || $extension == 'jpg' || $extension == 'pptx' || $extension == 'png' || $extension == 'mp4' || $extension == 'wmv') {
        if (move_uploaded_file($_FILES['common_file']['tmp_name'], $destination)) {
            $json = '{"status":"success","file_name":"' . $six_digit . '_' . $_FILES['common_file']['name'] . '","id":"' . $_REQUEST['id'] . '","orginal_file_name":"' . $_FILES['common_file']['name'] . '"}';
        } else {
            $json = '{"status":"error","file_name":""}';
        }
    } else {
        $json = '{"status":"error","file_name":""}';
    }
    return $json;
}

function GetUserDetails($id = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `users` WHERE id=" . $id;
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetCommissionByVendor($vendor_id = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `restaurant_details` WHERE vendor_id=" . $vendor_id;
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetCountry($id = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `countries` order by id asc";
    $arr = $db->Database->select_qry_array($Qry);
    $html = '';
    for ($i = 0; $i < count($arr); $i++) {
        $d = $arr[$i];
        $select = '';
        if ($id == $d->id) {
            $select = 'selected="selected"';
        }
        $html = $html . '<option ' . $select . ' value="' . $d->id . '">' . $d->country_name . '</option>';
    }
    return $html;
}

function GetCities() {
    $db = LoadDB();
    $Qry = "SELECT * FROM `cities` order by city_id asc";
    $arr = $db->Database->select_qry_array($Qry);
    $html = '';
    for ($i = 0; $i < count($arr); $i++) {
        $d = $arr[$i];
        $select = '';
        $html = $html . '<option  value="' . $d->city_id . '">' . $d->city_name . '</option>';
    }
    return $html;
}

function change_profile_image_user($array) {
    $db = LoadDB();
    $session_arr = $db->session->userdata('UserLogin');

    $six_digit = uniqid();
    $fileNameRRR = $six_digit . '_' . $_FILES['profile_dashboard']['name'];

    $destination = HOME_DIR . 'uploads/user_images/' . $fileNameRRR;
    $json = '';
    if ($_FILES['profile_dashboard']['name'] != '') {
        $extension = pathinfo($_FILES['profile_dashboard']['name'], PATHINFO_EXTENSION);
        if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png') {
            if (move_uploaded_file($_FILES['profile_dashboard']['tmp_name'], $destination)) {
                $delete_path = HOME_DIR . '/uploads/user_images/' . $_REQUEST['delete_img'];
                if (is_file($delete_path)) {
                    unlink($delete_path);
                }
                $CondArray = array('id' => $session_arr->id);
                $jsonRR['image'] = $fileNameRRR;
                $db->Database->update('users', $jsonRR, $CondArray);
                $json = '{"status":"success","message":"Image Updated Successfully"}';
            } else {
                $json = '{"status":"error","message":" "}';
            }
        } else {
            $json = '{"status":"error","message":"Invalid file format"}';
        }
    } else {
        $json = '{"status":"error","message":" "}';
    }
    return $json;
}

function change_profile_image($array) {
    $return = change_profile_image_user($array);
    echo $return;
}

function GetCategory($CategoryId = '') {
    $db = LoadDB();
    $CondutaionArray = array();
    $CondutaionArray['archive'] = 0;
    $Cond = '';
    if (is_numeric($CategoryId)) {
        $CondutaionArray['id'] = $CategoryId;
        $Cond = 'AND id=:id';
    }
    $Qry = "SELECT * FROM `category_details` WHERE archive=:archive $Cond";
    $CategoryArray = $db->Database->select_qry_array($Qry, $CondutaionArray);
    return $CategoryArray;
}

function CategoryDelete($Array) {
    $db = LoadDB();
    $CondArray = array('id' => $Array['CategoryId']);
    $json['archive'] = 1;
    $db->Database->update('category_details', $json, $CondArray);
    echo json_encode(array('status' => 'success'));
}

function GetCategoryFrontend() {
    $db = LoadDB();
    $CondutaionArray = array('archive' => 0);
    $Qry = "SELECT * FROM `category_details` WHERE archive=:archive";
    $CategoryArray = $db->Database->select_qry_array($Qry, $CondutaionArray);
    return $CategoryArray;
}

function GetHomeSlider() {
    $db = LoadDB();
    $CondutaionArray = array('archive' => 0);
    $Qry = "SELECT * FROM `home_banner` WHERE archive=:archive";
    $CategoryArray = $db->Database->select_qry_array($Qry, $CondutaionArray);
    return $CategoryArray;
}

function GetOccasion() {
    $db = LoadDB();
    $CondutaionArray = array('archive' => 0);
    $Qry = "SELECT * FROM `occasion` WHERE archive=:archive";
    $CategoryArray = $db->Database->select_qry_array($Qry, $CondutaionArray);
    return $CategoryArray;
}

function autoupdate() {
    $updatejson = !empty($_POST['updatejson']) ? json_decode($_POST['updatejson'], true) : [];
    $condjson = !empty($_POST['condjson']) ? json_decode($_POST['condjson'], true) : [];
    $dbtable = !empty($_POST['dbtable']) ? $_POST['dbtable'] : '';
    if (!empty($dbtable) && is_array($updatejson) && is_array($condjson)) {
        $db = LoadDB();
        $db->Database->update($dbtable, $updatejson, $condjson);
        die(json_encode(array('status' => true, 'message' => 'Successfully')));
    }
}

function common_delete() {
    $db = LoadDB();
    $id = !empty($_POST['Id']) ? $_POST['Id'] : '';
    $cond_array = array('id' => $id);
    $json['archive'] = 1;
    $db->Database->update($_POST['table'], $json, $cond_array);
    if ($_POST['table'] == 'staff') {
        $cond_array = array('staff_id' => $id);
        $result = $db->Database->delete('staff_role', $cond_array);
    }
    die(json_encode(array('status' => true, 'message' => 'Deleted successfully')));
}

function menudelete() {
    $updatejson = !empty($_POST['updatejson']) ? json_decode($_POST['updatejson'], true) : [];
    $condjson = !empty($_POST['condjson']) ? json_decode($_POST['condjson'], true) : [];
    $dbtable = !empty($_POST['dbtable']) ? $_POST['dbtable'] : '';
    $menuId = !empty($_POST['menuId']) ? $_POST['menuId'] : '';

    if (!empty($dbtable) && is_array($updatejson) && is_array($condjson)) {
        $db = LoadDB();
        $db->Database->update($dbtable, $updatejson, $condjson);

        $conddel = array('menu_id' => $menuId);
        $db->Database->delete('add_to_cart', $conddel);

        die(json_encode(array('status' => true, 'message' => 'Successfully')));
    }
}

function cuisine_delete() {
    $db = LoadDB();
    $id = !empty($_POST['id']) ? $_POST['id'] : '';
    $cond_array = array('cuisine_id' => $id);
    $json['archive'] = 1;
    $db->Database->update($_POST['table'], $json, $cond_array);
    if ($_POST['table'] == 'staff') {
        $cond_array = array('staff_id' => $id);
        $result = $db->Database->delete('staff_role', $cond_array);
    }
    die(json_encode(array('status' => true, 'message' => 'Deleted successfully')));
}

function common_approve() {
    $db = LoadDB();
    $id = !empty($_POST['id']) ? $_POST['id'] : '';
    $cond_array = array('id' => $id);
    $json['is_approved'] = 1;
    $db->Database->update($_POST['table'], $json, $cond_array);


    $Qry = "SELECT restaurant_name,email FROM  users LEFT JOIN restaurant_details ON users.id=restaurant_details.vendor_id WHERE users.id= $id";
    $userArray = $db->Database->select_qry_array($Qry);

    send_email_to_vendor_after_approve($userArray);
    die(json_encode(array('status' => true, 'message' => 'Approved successfully')));
}

function common_reject() {
    $db = LoadDB();
    $id = !empty($_POST['id']) ? $_POST['id'] : '';
    $cond_array = array('id' => $id);
    $json['is_approved'] = 2;
    $db->Database->update($_POST['table'], $json, $cond_array);
    die(json_encode(array('status' => true, 'message' => 'Rejected successfully')));
}

function get_category($id = '', $store_type = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `category_details` WHERE category_name NOT LIKE 'Most Selling%' AND store_type='$store_type' AND archive=0";
    $arr = $db->Database->select_qry_array($qry);
    $html = '';
    for ($i = 0; $i < count($arr); $i++) {
        $d = $arr[$i];
        $select = '';
        if ($id == $d->id) {
            $select = 'selected="selected"';
        }
        $html = $html . '<option ' . $select . ' value="' . $d->id . '">' . $d->category_name . '</option>';
    }
    return $html;
}

function get_mostselling($vendor_id) {
    $db = LoadDB();
    $qry = "SELECT * FROM `most_selling` WHERE vendor_id =" . $vendor_id;
    $arr = $db->Database->select_qry_array($qry);
    return $arr;
}

function get_cuisine($id = '') {

    $db = LoadDB();
    $qry = "SELECT * FROM `cuisine`";
    $arr = $db->Database->select_qry_array($qry);
    $html = '';
    for ($i = 0; $i < count($arr); $i++) {
        $d = $arr[$i];
        $select = '';

        if ($id == $d->cuisine_id) {
            $select = 'selected="selected"';
        }
        $html = $html . '<option ' . $select . ' value="' . $d->cuisine_id . '">' . $d->cuisine_name . '</option>';
    }
    return $html;
}

function get_storetype($id = '') {

    $db = LoadDB();
    $qry = "SELECT *,store_type_ar FROM `store_type` WHERE status=0 AND archive=0 ORDER BY order_view ASC";
    $arr = $db->Database->select_qry_array($qry);
    $lan = getsystemlanguage();
    $html = '';
    for ($i = 0; $i < count($arr); $i++) {
        $d = $arr[$i];
        $select = '';

        if ($id == $d->id) {
            $select = 'selected="selected"';
        }
        $html = $html . '<option ' . $select . ' value="' . $d->id . '">' . ($lan == 'ar' ? $d->store_type_ar : $d->store_type) . '</option>';
    }
    return $html;
}

function Get_AllStoreType($id = '') {
    $db = LoadDB();
    $Qry = "SELECT *,store_type_ar FROM `store_type` WHERE status=0 and archive=0 ORDER BY order_view ASC";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function Get_VendorList($store_type = '') {
    $db = LoadDB();
    $qry = "SELECT users.*,restaurant_details.restaurant_name FROM `users` join restaurant_details ON restaurant_details.vendor_id=users.id WHERE is_approved != 0 AND user_type=2 AND users.store_type='$store_type' order by users.id desc";
    $Array = $db->Database->select_qry_array($qry);
    return $Array;
}

function get_emirates($id = '') {

    $db = LoadDB();
    $qry = "SELECT * FROM `cities`";
    $arr = $db->Database->select_qry_array($qry);
    $html = '';
    for ($i = 0; $i < count($arr); $i++) {
        $d = $arr[$i];
        if ($db->session->userdata('language') == 'en' || empty($db->session->userdata('language'))) {
            $city_name = $d->city_name;
        } else {
            $city_name = $d->city_name_ar;
        }

        $select = '';

        if ($id == $d->city_id) {
            $select = 'selected="selected"';
        }
        $html = $html . '<option ' . $select . ' value="' . $d->city_id . '">' . $city_name . '</option>';
    }
    return $html;
}

function GetPaymentType($PaymentType = false) {
    $db = LoadDB();
    $Return = 'Not Given';
    if ($PaymentType == 3) {
        $Return = $db->lang->line("cash_on_delivery");
    } elseif ($PaymentType == 1) {
        $Return = $db->lang->line("credit_card");
    } elseif ($PaymentType == 2) {
        $Return = $db->lang->line("visa_checkout");
    }
    return $Return;
}

function get_country_options($id = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `countries`";
    $arr = $db->Database->select_qry_array($qry);
    $html = '';
    for ($i = 0; $i < count($arr); $i++) {
        $d = $arr[$i];
        $select = '';
        if ($id == $d->id) {
            $select = 'selected="selected"';
        }
        $html = $html . '<option ' . $select . ' value="' . $d->id . '">' . $d->country_name . '</option>';
    }
    return $html;
}

function GetCategoryById($id = '') {
    $db = LoadDB();
    $Qrys = "SELECT * FROM `category_details` WHERE id='" . $id . "'";
    $Array = $db->Database->select_qry_array($Qrys);
    return $Array;
}

function common_remove_image($array) {

    $db = LoadDB();
    if ($array['type'] == 'multiple') {
        $qry = "SELECT * FROM " . $array['table'] . " where id='" . $array['id'] . "'";
        $arrays = $db->Database->select_qry_array($qry);
        if ($arrays[0]->image != '') {
            $arr = explode(",", $arrays[0]->image);
            foreach ($arr as $k => $v) {
                $destination = HOME_DIR . $array['path'] . $v;
                if ($array['name'] == $v) {
                    unset($arr[$k]);
                    if (is_file($destination)) {
                        unlink($destination);
                    }
                }
            }
            if (!empty($arr)) {
                $newarray = implode(",", $arr);
            } else {
                $newarray = '';
            }
            $update['image'] = $newarray;
            $CondArray = array('id' => $array['id']);
            $result = $db->Database->update($array['table'], $update, $CondArray);
        }
    } else {
        $delete_path = HOME_DIR . $array['path'] . $array['name'];
        if (is_file($delete_path)) {
            unlink($delete_path);
        }
        $CondArray = array('id' => $array['id']);
        if ($array['type'] != '') {
            $json[$array['type']] = '';
        } else {
            $json['image'] = '';
        }
        $result = $db->Database->update($array['table'], $json, $CondArray);
    }
    if (isset($result)) {
        echo $json = '{"status":"success","message":"Deleted successfully"}';
        exit;
    } else {
        echo $json = '{"status":"error","message":"Something went wrong."}';
        exit;
    }
}

function offer_remove_image($array) {
    $db = LoadDB();
    if ($array['type'] == 'multiple') {
        $qry = "SELECT * FROM " . $array['table'] . " where promocode_id='" . $array['id'] . "'";
        $arrays = $db->Database->select_qry_array($qry);
        if ($arrays[0]->offer_image != '') {
            $arr = explode(",", $arrays[0]->offer_image);
            foreach ($arr as $k => $v) {
                $destination = HOME_DIR . $array['path'] . $v;
                if ($array['name'] == $v) {
                    unset($arr[$k]);
                    if (is_file($destination)) {
                        unlink($destination);
                    }
                }
            }
            if (!empty($arr)) {
                $newarray = implode(",", $arr);
            } else {
                $newarray = '';
            }
            $update['offer_image'] = $newarray;
            $CondArray = array('promocode_id' => $array['id']);
            $result = $db->Database->update($array['table'], $update, $CondArray);
        }
    } else {
        $delete_path = HOME_DIR . $array['path'] . $array['name'];
        if (is_file($delete_path)) {
            unlink($delete_path);
        }
        $CondArray = array('promocode_id' => $array['id']);
        if ($array['type'] != '') {
            $json[$array['type']] = '';
        } else {
            $json['offer_image'] = '';
        }
        $result = $db->Database->update($array['table'], $json, $CondArray);
    }
    if (isset($result)) {
        echo $json = '{"status":"success","message":"Deleted successfully"}';
        exit;
    } else {
        echo $json = '{"status":"error","message":"Something went wrong."}';
        exit;
    }
}

function GetCuisine() {
    $db = LoadDB();
    $CondutaionArray = array('archive' => 0);
    $Qry = "SELECT * FROM `cuisine` WHERE archive=:archive";
    $CuisineArray = $db->Database->select_qry_array($Qry, $CondutaionArray);
    return $CuisineArray;
}

function GetCuisineCount($location = '', $lat = '', $long = '') {

    $db = LoadDB();
    $latitude = floor($lat * 100) / 100;
    $longitude = floor($long * 100) / 100;
    $CondutaionArray = array('archive' => 0);

    // $Qry = "SELECT city_id FROM  cities WHERE cities.city_name LIKE CONCAT('%', '".trim($location)."' ,'%')";
    // $Array = $db->Database->select_qry_array($Qry);
    // $city_id = $Array[0]->city_id;

    $Qry1 = "SELECT restaurant_details.vendor_id,delivery_timings.cities
        FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        join delivery_timings on delivery_timings.vendor_id = restaurant_details.vendor_id 
        left join cities on cities.city_id = delivery_timings.cities
        WHERE user_type=2 AND latitude LIKE CONCAT('%', '" . $latitude . "' ,'%')";
    $Array1 = $db->Database->select_qry_array($Qry1);

    // $Qry_1 = "SELECT restaurant_details.vendor_id,".
    //         "GeoDistDiff('km', '{$latitude}', '{$longitude}', users.latitude, users.longitude) as distance".
    //         " FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
    //         WHERE user_type=2 AND isDelivery=0 HAVING distance <= 50";
    // $Array_1 = $db->Database->select_qry_array($Qry_1);
    // $Array_new = array_merge($Array1,$Array_1);


    $ids = [];
    for ($i = 0; $i < count($Array1); $i++) {
        $d = $Array1[$i];
        array_push($ids, $d->vendor_id);
    }

    $arr = implode("','", $ids);

    $Qry = " SELECT cuisine.cuisine_id, cuisine.cuisine_name,cuisine.cuisine_name_ar, dt.count FROM cuisine LEFT JOIN ( SELECT cuisine_id,COUNT(users.id) AS count 
                FROM restaurant_cuisine_type LEFT JOIN users ON users.id=restaurant_cuisine_type.vendor_id 
                LEFT JOIN restaurant_details ON restaurant_details.vendor_id=users.id  
                WHERE restaurant_details.vendor_id IN('" . $arr . "') GROUP BY restaurant_cuisine_type.cuisine_id ) AS dt ON cuisine.cuisine_id = dt.cuisine_id";
    // $Qry = "SELECT count(users.id) AS count,cuisine_id,cuisine_name FROM cuisine LEFT JOIN `restaurant_cuisine_type` ON restaurant_cuisine_type.cuisine_id=cuisine.cuisine_id
    //             LEFT JOIN users ON users.id=restaurant_cuisine_type.vendor_id 
    //             LEFT JOIN restaurant_details ON restaurant_details.vendor_id=users.id 
    //             WHERE delivery_location LIKE CONCAT('%', '".$latitude."' ,'%') AND restaurant_cuisine_type.cuisine_id=$cuisine_id GROUP BY restaurant_cuisine_type.cuisine_id";
    //  $Qry = " SELECT cuisine.cuisine_id, cuisine.cuisine_name,cuisine.cuisine_name_ar, dt.count FROM cuisine LEFT JOIN ( SELECT cuisine_id,COUNT(users.id) AS count 
    //             FROM restaurant_cuisine_type LEFT JOIN users ON users.id=restaurant_cuisine_type.vendor_id 
    //             LEFT JOIN restaurant_details ON restaurant_details.vendor_id=users.id  
    //             WHERE delivery_location LIKE CONCAT('%', '".$latitude."' ,'%') GROUP BY restaurant_cuisine_type.cuisine_id ) AS dt ON cuisine.cuisine_id = dt.cuisine_id";
    $CuisineArray = $db->Database->select_qry_array($Qry, $CondutaionArray);
    return $CuisineArray;
}

function GetCuisineCountRestaurant($location = '', $lat = '', $long = '', $rest_name = '', $table_booking = '', $store_type = '') {
    $db = LoadDB();
    $CondutaionArray = array('archive' => 0);
    $latitude = floor($lat * 100) / 100;
    $longitude = floor($long * 100) / 100;
    $CondutaionArray = array('archive' => 0);

    // $Qry = "SELECT city_id FROM  cities WHERE cities.city_name LIKE CONCAT('%', '".trim($location)."' ,'%')";
    // $Array = $db->Database->select_qry_array($Qry);
    // $city_id = $Array[0]->city_id;

    $Qry1 = "SELECT restaurant_details.vendor_id,delivery_timings.cities
        FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        join delivery_timings on delivery_timings.vendor_id = restaurant_details.vendor_id 
        left join cities on cities.city_id = delivery_timings.cities
        WHERE user_type=2 AND store_type='$store_type' AND latitude LIKE CONCAT('%', '" . $latitude . "' ,'%')";
    $Array1 = $db->Database->select_qry_array($Qry1);

    // $Qry_1 = "SELECT restaurant_details.vendor_id,".
    //         "GeoDistDiff('km', '{$latitude}', '{$longitude}', users.latitude, users.longitude) as distance".
    //         " FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
    //         WHERE user_type=2 AND store_type='$store_type' AND isDelivery=0 HAVING distance <= 50";
    // $Array_1 = $db->Database->select_qry_array($Qry_1);
    // $Array_new = array_merge($Array1,$Array_1);


    $ids = [];
    for ($i = 0; $i < count($Array1); $i++) {
        $d = $Array1[$i];
        array_push($ids, $d->vendor_id);
    }

    $arr = implode("','", $ids);

    $cond = '';
    if ($table_booking != '') {
        $cond = "AND  restaurant_details.table_booking='$table_booking'";
    }
    // $Qry = "SELECT count(users.id) AS count,cuisine_id,cuisine_name FROM cuisine LEFT JOIN `restaurant_cuisine_type` ON restaurant_cuisine_type.cuisine_id=cuisine.cuisine_id
    //             LEFT JOIN users ON users.id=restaurant_cuisine_type.vendor_id 
    //             LEFT JOIN restaurant_details ON restaurant_details.vendor_id=users.id 
    //             WHERE delivery_location LIKE CONCAT('%', '".$latitude."' ,'%') AND restaurant_cuisine_type.cuisine_id=$cuisine_id GROUP BY restaurant_cuisine_type.cuisine_id";
    $Qry = " SELECT cuisine.cuisine_id, cuisine.cuisine_name,cuisine.cuisine_name_ar, dt.count FROM cuisine LEFT JOIN ( SELECT cuisine_id,COUNT(users.id) AS count 
                FROM restaurant_cuisine_type LEFT JOIN users ON users.id=restaurant_cuisine_type.vendor_id 
                LEFT JOIN restaurant_details ON restaurant_details.vendor_id=users.id  
                WHERE restaurant_details.vendor_id IN('" . $arr . "') $cond AND restaurant_name LIKE '%" . $rest_name . "%' GROUP BY restaurant_cuisine_type.cuisine_id ) AS dt ON cuisine.cuisine_id = dt.cuisine_id";
    // print_r($Qry);die();
    $CuisineArray = $db->Database->select_qry_array($Qry, $CondutaionArray);
    return $CuisineArray;
}

function GetCategories($restaurantId = '') {
    $db = LoadDB();

    $Qry = "SELECT * FROM `category_details` JOIN `menu_list` ON `menu_list`.`category_id`=`category_details`.`id` WHERE menu_list.vendor_id='$restaurantId' AND category_details.archive=0 AND category_details.status=0  GROUP BY category_id ORDER BY category_details.position ASC";
    $CategoryArray = $db->Database->select_qry_array($Qry);

    return $CategoryArray;
}

function GetRestaurantCategory($restaurantId = '') {
    $db = LoadDB();

    $Qry = "SELECT category_details.id as category_id,category_details.category_name,category_details.category_name_ar,position FROM `category_details`  JOIN `menu_list` ON `menu_list`.`category_id`=`category_details`.`id` LEFT JOIN category_positions CP ON CP.category_id=menu_list.category_id AND CP.vendor_id='$restaurantId' WHERE menu_list.vendor_id='$restaurantId' AND category_details.archive=0 AND category_details.status=0  GROUP BY menu_list.category_id ORDER BY CP.ordercat_view ASC";
    $CategoryArray = $db->Database->select_qry_array($Qry);

    $Qry1 = "SELECT category_details.id as category_id,category_details.category_name,category_details.category_name_ar,position FROM `category_details`  LEFT JOIN `most_selling` ON `most_selling`.`category_id`=`category_details`.`id` LEFT JOIN category_positions CP ON CP.category_id=most_selling.category_id AND CP.vendor_id='$restaurantId' WHERE most_selling.vendor_id='$restaurantId' AND category_details.archive=0 AND category_details.status=0 GROUP BY most_selling.category_id  ORDER BY CP.ordercat_view ASC";
    $CategoryArray1 = $db->Database->select_qry_array($Qry1);

    $result = array_merge($CategoryArray, $CategoryArray1);

    //usort($result, 'sortByOrder');
    return $result;
}

function GetRestaurantPartyCategory($restaurantId = '') {
    $db = LoadDB();

    $Qry = "SELECT category_details.id as category_id,category_details.category_name,category_details.category_name_ar,position FROM `category_details`  JOIN `party_order_menu` ON `party_order_menu`.`category_id`=`category_details`.`id` WHERE party_order_menu.vendor_id='$restaurantId' AND category_details.archive=0 AND category_details.status=0  GROUP BY party_order_menu.category_id ORDER BY category_details.position ASC";
    $CategoryArray = $db->Database->select_qry_array($Qry);

    return $CategoryArray;
}

function sortByOrder($a, $b) {
    return $a->position - $b->position;
}

function listMenuByRestaurant($vendor_id = '', $category_id = '') {
    $db = LoadDB();
    $arrayname = [];
    $search = !empty($_REQUEST['search']) ? $_REQUEST['search'] : '';
    $cond = '';
    if (!empty($search)) {
        $cond = $cond . " AND (menu_list.menu_name LIKE '%$search%' OR BINARY(CONVERT(menu_list.menu_name_ar USING latin1))  LIKE '%" . $search . "%')";
    }

    $Qry = "SELECT * FROM most_selling WHERE most_selling.vendor_id='$vendor_id' AND category_id='$category_id'";
    $Array = $db->Database->select_qry_array($Qry);


    if (count($Array) > 0) {
        $cat_id = $Array[0]->category_id;

        $Qry1 = "SELECT menu_list.id,menu_list.stock,most_selling.vendor_id,menu_list.category_id,menu_list.menu_name,menu_list.menu_name_ar,description,price,image,choice,menu_list.status,menu_list.archive,menu_list.lastupdate,menu_list.timestamp,promocode_id,promo_code,
            title,promocode_type,purchase_amount,date_from,date_from,discount,user_type,offer_type,offer_addedby,GROUP_CONCAT(promo_code.menu_id) AS menu_id,restaurant_id,offer_image,menu_list.stock 
            FROM `menu_list` LEFT JOIN promo_code ON (promo_code.offer_addedby=menu_list.vendor_id AND promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE() AND promo_code.status=0) LEFT JOIN most_selling ON (most_selling.vendor_id=menu_list.vendor_id AND most_selling.menu_id=menu_list.id) WHERE most_selling.category_id='$cat_id' AND most_selling.vendor_id='$vendor_id' AND menu_list.archive=0 AND menu_list.status=0 $cond GROUP BY menu_list.id ORDER BY menu_list.order_view ASC";

        $Array1 = $db->Database->select_qry_array($Qry1);
    } else {
        $Qry2 = "SELECT *,GROUP_CONCAT(promo_code.menu_id) AS menu_id FROM `menu_list` LEFT JOIN promo_code ON (promo_code.offer_addedby=menu_list.vendor_id AND promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE() AND promo_code.status=0) WHERE menu_list.vendor_id='$vendor_id' AND menu_list.category_id='$category_id' AND menu_list.archive=0 AND menu_list.status=0 $cond GROUP BY menu_list.id ORDER BY menu_list.order_view ASC";
        $Array1 = $db->Database->select_qry_array($Qry2);
    }
    return $Array1;
}

function listPartyMenuByRestaurant($vendor_id = '', $category_id = '') {
    $db = LoadDB();
    $arrayname = [];
    $Qry2 = "SELECT * FROM `party_order_menu` LEFT JOIN promo_code ON (promo_code.offer_addedby=party_order_menu.vendor_id AND promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE()) WHERE party_order_menu.vendor_id='$vendor_id' AND party_order_menu.category_id='$category_id' AND party_order_menu.archive=0 ORDER BY party_order_menu.id DESC";

    $Array1 = $db->Database->select_qry_array($Qry2);
    return $Array1;
}

function GetRestaurantById($id = '') {
    $db = LoadDB();
    $Qrys = "SELECT * FROM `restaurant_details` WHERE vendor_id='" . $id . "'";
    $Array = $db->Database->select_qry_array($Qrys);
    return $Array;
}

function GetOrders() {
    $db = LoadDB();
    $Qry = "SELECT * FROM `orders` WHERE archive=0 order by id desc";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetAdminOrders($Cond = '') {
    $db = LoadDB();
    $condSearch = '';
    if (isset($_POST['Filter'])) {
        $Cond = '';
        $data['Form'] = $_POST;
        extract($_POST);
        if (!empty($FilterStartDate) && !empty($FilterEndDate)) {
            $datefrom = date('Y-m-d', strtotime($FilterStartDate));
            $dateto = date('Y-m-d', strtotime($FilterEndDate));
            $Cond = "AND DATE(O.date) BETWEEN '$datefrom' AND '$dateto'";
        } else if (!empty($FilterStartDate)) {
            $datefrom = date('Y-m-d', strtotime($FilterStartDate));
            $Cond = $Cond . "AND DATE(O.date) >= '$datefrom'";
        } elseif (!empty($FilterEndDate)) {
            $dateto = date('Y-m-d', strtotime($FilterEndDate));
            $Cond = $Cond . "AND DATE(O.date) < '$dateto'";
        }

        if (!empty($FilterStoreType)) {
            $Cond = $Cond . "AND US.store_type = '$FilterStoreType'";
        }
        if (!empty($Filterorderno)) {
            $Cond = $Cond . "AND O.orderno LIKE '%$Filterorderno%'";
        }
        if (!empty($Filterorderstatus)) {
            $Cond = $Cond . "AND O.order_status = '$Filterorderstatus'";
        }
    }
    $Join = 'LEFT JOIN users U ON U.id=O.user_id LEFT JOIN restaurant_details RD ON RD.vendor_id=O.vendor_id LEFT JOIN order_status OS ON OS.status_id=O.order_status LEFT JOIN users US ON US.id=O.vendor_id';
    $Qry = "SELECT O.*,OS.*,US.store_type FROM `orders` O $Join WHERE O.archive=0 $Cond order by O.id DESC LIMIT 1000";

    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetOrdersById($id = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `orders` LEFT JOIN order_status OS ON OS.status_id=orders.order_status WHERE `orders`.`archive`=0 and `orders`.`id`=" . $id;
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetAdminPartyOrders($Cond = '') {

    $db = LoadDB();
    $condSearch = '';
    if (isset($_POST['Filter'])) {
        $Cond = '';
        $data['Form'] = $_POST;
        extract($_POST);

        if (!empty($FilterStartDate) && !empty($FilterEndDate)) {
            $datefrom = date('Y-m-d', strtotime($FilterStartDate));
            $dateto = date('Y-m-d', strtotime($FilterEndDate));
            $Cond = "AND DATE(O.inserted_on) BETWEEN '$datefrom' AND '$dateto'";
        } else if (!empty($FilterStartDate)) {
            $datefrom = date('Y-m-d', strtotime($FilterStartDate));
            $Cond = $Cond . "AND DATE(O.inserted_on) >= '$datefrom'";
        } elseif (!empty($FilterEndDate)) {
            $dateto = date('Y-m-d', strtotime($FilterEndDate));
            $Cond = $Cond . "AND DATE(O.inserted_on) < '$dateto'";
        }
        if (!empty($FilterCustomerId)) {
            $Cond = $Cond . "AND U.id = '$FilterCustomerId'";
        }
        if (!empty($FilterRestaurantId)) {
            $Cond = $Cond . "AND O.vendor_id = '$FilterRestaurantId'";
        }
    }
    $Join = 'LEFT JOIN users U ON U.id=O.user_id LEFT JOIN restaurant_details RD ON RD.vendor_id=O.vendor_id ';
    $Qry = "SELECT O.* FROM `party_order` O $Join WHERE O.archive=0 $Cond order by O.id DESC";

    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetOrdersByVendorId($id = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `orders` O LEFT JOIN order_status OS ON OS.status_id=order_status WHERE O.archive=0 AND O.vendor_id=$id order by O.id desc";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetOrdersOnlyByVendorId($id = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `orders` O LEFT JOIN order_status OS ON OS.status_id=order_status WHERE O.archive=0 AND O.vendor_id=$id and order_status !=7 order by O.id desc";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetorderstatusBy($statusId = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `order_status` WHERE status_id='$statusId'";
    $Array = $db->Database->select_qry_array($Qry);
    $Array = !empty($Array[0]) ? $Array[0] : '';
    return $Array;
}

function GetorderstatusByAr($statusId = '') {

    $statusName = '';
    if (empty($statusId)) {
        $statusName = 'موصول ہوا';
    } else if ($statusId == '1') {
        $statusName = 'آرڈر کی تصدیق ہوگئی';
    } else if ($statusId == '2') {
        $statusName = 'آرڈر کی تیاری';
    } else if ($statusId == '3') {
        $statusName = 'کی فراہمی کے لئے تفویض کریں';
    } else if ($statusId == '4') {
        $statusName = 'پک اپ آرڈر';
    } else if ($statusId == '5') {
        $statusName = 'راستے میں آرڈر';
    } else if ($statusId == '6') {
        $statusName = 'آرڈر پہنچا دیا گیا';
    } else if ($statusId == '7') {
        $statusName = 'منسوخ';
    } else if ($statusId == '8') {
        $statusName = 'لینے کے لئے تیار ہے';
    } else if ($statusId == '9') {
        $statusName = 'آرڈر مکمل ہوگیا';
    } else if ($statusId == '14') {
        $statusName = 'ترسیل کے لئے تیار';
    }
    return $statusName;
}

function GetPartyOrders() {
    $db = LoadDB();
    $Qry = "SELECT * FROM `partyorder_request` order by id desc";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetUserListOnOrders() {
    $db = LoadDB();
    $Qry = "SELECT * FROM `orders` LEFT JOIN users ON users.id = orders.user_id WHERE users.archive=0  GROUP BY users.id order by users.id desc";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetLatest10Orders() {
    $Cond = '';
    $db = LoadDB();
    if (isset($_REQUEST['FilterOrder'])) {
        $FilterArray = FilterOrder($_REQUEST);
        $Cond = $FilterArray['Cond'];
        if ($Cond != '') {
            $Cond = ' WHERE ' . $Cond;
        }
    }
    $Select = ',U.name,U.id as user_id,O.order_status,OS.status_color,OS.status_name';
    $Join = 'LEFT JOIN users U ON U.id=O.user_id LEFT JOIN order_status OS ON OS.status_id=O.order_status';
    $qry = "SELECT O.* $Select FROM `orders` O $Join $Cond ORDER BY O.id DESC LIMIT 10";
    $order = $db->Database->select_qry_array($qry);
    return $order;
}

function GetPartyOrdersByVendorId($id = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `party_order` WHERE vendor_id=$id order by id desc";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetPartyOrdersById($id = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `party_order` WHERE id=$id order by id desc";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetNameById($id = '', $table = '', $field = "") {
    $db = LoadDB();
    $Qrys = "SELECT $field FROM `$table` WHERE id='" . $id . "'";
    $Array = $db->Database->select_qry_array($Qrys);
    if (!empty($Array)) {
        return $Array[0]->$field;
    }
}

function GetCustomerTotalOrder($CustomerId) {
    $db = LoadDB();
    $CondutaionArray = array('user_id' => $CustomerId);
    $Qry = "SELECT COUNT(OD.id) AS TotalOrder FROM `orders` OD WHERE OD.user_id=:user_id";
    $Array = $db->Database->select_qry_array($Qry, $CondutaionArray);
    $TotalOrder = 0;
    $TotalProductOrder = 0;
    if (count($Array) > 0) {
        $TotalOrder = $Array[0]->TotalOrder;
        // $TotalProductOrder = $Array[0]->TotalProductOrder;
    }
    $returnArray = array('TotalOrder' => $TotalOrder);
    return $returnArray;
}

function GetOrderDetailsByOrderId($order_id = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `order_details`  WHERE archive=0 and `order_details`.order_id=" . $order_id;
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetPartyOrderDetailsByOrderId($order_id = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `party_order_details`  WHERE archive=0 and `party_order_details`.party_order_id=" . $order_id;
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetOrderById($id = '') {
    $db = LoadDB();
    $Qry = "SELECT *,ML.menu_name_ar,order_details.price AS ord_del_price FROM `orders` LEFT JOIN `order_details` ON `orders`.`id`=`order_details`.`order_id` LEFT JOIN order_status OS ON OS.status_id=orders.order_status LEFT JOIN menu_list ML ON ML.id=order_details.product_id  WHERE  `orders`.`id`='$id'";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetPartyOrderDetailsById($id = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `party_order` LEFT JOIN `party_order_details` ON `party_order`.`id`=`party_order_details`.`party_order_id`  WHERE `party_order`.`archive`=0 and `party_order`.`id`=" . $id;
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetOrderByOrderId($id = '') {
    $db = LoadDB();
    $select = ",RD.isDelivery,UOD.latitude,UOD.longitude,UOD.address";
    $join = " LEFT JOIN restaurant_details RD ON RD.vendor_id=O.vendor_id LEFT JOIN user_order_address UOD ON UOD.id=O.delivery_address";
    $Qry = "SELECT O.* $select FROM `orders` O $join WHERE  O.`id`=" . $id;
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetPartyOrderStatusName($StatusId) {
    $Status = '';
    if (empty($StatusId)) {
        $Status = 'Received';
    } else if ($StatusId == 1) {
        $Status = 'Order Confirmed';
    } elseif ($StatusId == 2) {
        $Status = 'Party Finished';
    } elseif ($StatusId == 7) {
        $Status = 'Cancelled';
    }
    return $Status;
}

function GetOrderStatusName($StatusId) {
    $Status = '';
    if ($StatusId == 1) {
        $Status = 'Confirmed';
    } elseif ($StatusId == 2) {
        $Status = 'Preparing Order';
    } elseif ($StatusId == 3) {
        $Status = 'Assign to deliver';
    } elseif ($StatusId == 4) {
        $Status = 'Pickup order';
    } elseif ($StatusId == 5) {
        $Status = 'Order on the way';
    } elseif ($StatusId == 6) {
        $Status = 'Order Delivered';
    } elseif ($StatusId == 7) {
        $Status = 'Cancelled';
    } elseif ($StatusId == 8) {
        $Status = 'Ready for pickup';
    } elseif ($StatusId == 9) {
        $Status = 'Order Completed';
    } elseif ($StatusId == 14) {
        $Status = 'Ready for delivery';
    }
    return $Status;
}

function GetProductDetailsById($id = '', $table = '') {
    $db = LoadDB();
    $Qrys = "SELECT * FROM $table WHERE id='" . $id . "'";
    $Array = $db->Database->select_qry_array($Qrys);
    return $Array;
}

function GetOrderByUserId($userid = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `orders` WHERE archive=0 and user_id=" . $userid . " order by id desc";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function OrderAddress() {
    $db = LoadDB();
    $id = $_POST['address_id'];
    $Qry = "SELECT * FROM `user_order_address` WHERE `id`=" . $id;
    $Array = $db->Database->select_qry_array($Qry);
    echo json_encode($Array);
}

function total_customer() {
    $db = LoadDB();
    $Qry = "SELECT count(id) as total_customer FROM `users` WHERE `user_type`=3 AND archive=0";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function total_vendor() {
    $db = LoadDB();
    $Qry = "SELECT count(id) as total_vendor FROM `users` WHERE `user_type`=2 AND archive=0 AND is_approved=1";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function total_order() {
    $db = LoadDB();
    $Qry = "SELECT count(id) as total_order FROM `orders` WHERE archive=0 AND order_status !=7";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function total_order_byid($vendor_id = '') {
    $db = LoadDB();
    $Qry = "SELECT count(id) as total_order FROM `orders` WHERE vendor_id='$vendor_id' AND archive=0 AND order_status NOT IN (7)";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function total_ongoingorder_byid($vendor_id = '') {
    $db = LoadDB();
    $Qry = "SELECT count(id) as total_order FROM `orders` WHERE vendor_id='$vendor_id' AND archive=0 AND order_status NOT IN (7,6)";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function total_completedorder_byid($vendor_id = '') {
    $db = LoadDB();
    $Qry = "SELECT count(id) as total_order FROM `orders` WHERE vendor_id='$vendor_id' AND archive=0 AND order_status NOT IN (7) AND order_status IN (6)";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function total_partyorder() {
    $db = LoadDB();
    $Qry = "SELECT count(id) as total_partyorder FROM `partyorder_request`";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function total_cancelled_order() {
    $db = LoadDB();
    $Qry = "SELECT count(id) as total_cancelled_order FROM `orders` WHERE order_status=7 and archive=0";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function total_promocode($id = '') {
    $db = LoadDB();
    $Qry = "SELECT count(promocode_id) as total_promocode FROM `promo_code` WHERE offer_addedby=" . $id;
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function CancelOrder() {
    $db = LoadDB();
    $id = !empty($_REQUEST['Id']) ? $_REQUEST['Id'] : '';
    $CondArray = array('id' => $id);
    $json['order_status'] = 7;
    $db->Database->update('orders', $json, $CondArray);
    die(json_encode(array('status' => true, 'message' => 'Updated successfully')));
}

function CustomerBlockUnblock($Array) {
    $db = LoadDB();
    $CondArray = array('id' => $Array['CustomerId']);
    $json['archive'] = $Array['archive'];
    $json['lastupdate'] = date('Y-m-d H:i:s');

    $db->Database->update('users', $json, $CondArray);
    die(json_encode(array('status' => 'success', 'message' => 'Status updated successfully')));
}

function updatePremium($Array) {
    $db = LoadDB();
    $CondArray = array('vendor_id' => $Array['vendor_id']);
    $json['isPremium'] = $Array['premium'];

    $db->Database->update('restaurant_details', $json, $CondArray);
    die(json_encode(array('status' => 'success', 'message' => 'Updated successfully')));
}

function notification_count($id = '') {
    $db = LoadDB();
    $todays = date('Y-m-d');
    $endDate = date('Y-m-d', strtotime('-1 month'));
    $Qry = "SELECT count(notification_id) as total_notification FROM `notification` WHERE receiver_id=$id AND DATE(inserted_on) BETWEEN '$endDate' AND '$todays' "; //AND notification_read=0
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function get_all_notification($id = '') {
    $db = LoadDB();
    $todays = date('Y-m-d');
    $endDate = date('Y-m-d', strtotime('-1 month'));
    $Qry = "SELECT * FROM `notification` WHERE receiver_id=$id AND DATE(inserted_on) BETWEEN '$endDate' AND '$todays' ORDER BY notification_id DESC"; // AND notification_read=0 

    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function changeOrderStatus($Array) {
    $db = LoadDB();
    $CondArray = array('id' => $Array['order_id']);
    if ($Array['payment_status'] != '') {
        $jsons['payment_status'] = $Array['payment_status'];
        $db->Database->update('orders', $jsons, $CondArray);
    } else {

        $json['order_status'] = $Array['order_status'];
        $db->Database->update('orders', $json, $CondArray);

        $data['order_id'] = $Array['order_id'];
        $data['status_id'] = $Array['order_status'];
        $data['isPartyOrder'] = '0';
        $data['notes'] = "function name changeOrderStatus, status Id-" . $data['status_id'] . ", order Id-" . $data['order_id'];
        if (canUpdateorderStatus($data['order_id'], $data['status_id'])) {
            $db->Database->insert('order_status_history', $data);
        }
        $rest = GetRestaurantById($Array['vendor_id']);

        $type = '';
        $message = '';
        if ($data['status_id'] == 1) {
            $message = $rest[0]->restaurant_name . ' has accepted your order';
            $type = ORDER_CONFIRMED_NOTIFICATION;
        } elseif ($data['status_id'] == 2) {
            $message = 'Your order from ' . $rest[0]->restaurant_name . ' is on the way';
            $type = ORDER_ON_THE_WAY_NOTIFICATION;
        } elseif ($data['status_id'] == 3) {
            $message = 'Order delivered';
            $type = ORDER_DELIVERED_NOTIFICATION;
        } elseif ($data['status_id'] == 5) {
            $message = 'Your order has been cancelled';
            $type = ORDER_CANCELLED_BY_VENDOR_NOTIFICATION;
        }

        $user_array['sender_id'] = $Array['vendor_id'];
        $user_array['receiver_id'] = $Array['user_id'];
        $user_array['notification_type'] = $type;
        $user_array['notification_message'] = $message;
        $user_array['redirect_url'] = '';
        $user_array['order_id'] = $Array['order_id'];

        $db->Database->insert('notification', $user_array);
        if ($data['status_id'] == '1') {

            SendpushNotifactionTorider($Array['order_id']);
        }
        // SendnotificationorderStatus($Array['order_id']);
        SendNotifactionOrderStatus($orderId = $Array['order_id']);
    }
    echo json_encode(array('status' => 'success'));
}

function changePartyOrderStatus($Array) {
    $db = LoadDB();
    $CondArray = array('id' => $Array['order_id']);
    $json['order_status'] = $Array['order_status'];
    $db->Database->update('party_order', $json, $CondArray);

    $data['order_id'] = $Array['order_id'];
    $data['status_id'] = $Array['order_status'];
    $data['isPartyOrder'] = '1';
    $data['notes'] = "function name changePartyOrderStatus, status Id-" . $data['status_id'] . ", order Id-" . $data['order_id'];
    if (canUpdateorderStatus($data['order_id'], $data['status_id'])) {
        $db->Database->insert('order_status_history', $data);
    }
    // $rest = GetRestaurantById($Array['vendor_id']);
    // if($data['status_id']==1)
    // {
    //     $message = $rest[0]->restaurant_name .' has accepted your order';
    //     $type = ORDER_CONFIRMED_NOTIFICATION;
    // }elseif($data['status_id']==2)
    // {
    //     $message = 'Your order from '.$rest[0]->restaurant_name .' is on the way';
    //     $type = ORDER_ON_THE_WAY_NOTIFICATION;
    // }elseif($data['status_id']==3)
    // {
    //     $message = 'Order delivered';
    //     $type = ORDER_DELIVERED_NOTIFICATION;
    // }elseif($data['status_id']==5)
    // {
    //     $message = 'Your order has been cancelled';
    //     $type = ORDER_CANCELLED_BY_VENDOR_NOTIFICATION;
    // }
    // $user_array['sender_id'] = $Array['vendor_id'];
    // $user_array['receiver_id'] = $Array['user_id'];
    // $user_array['notification_type'] = $type;
    // $user_array['notification_message'] = $message;
    // $user_array['redirect_url'] = '';
    // $user_array['order_id'] = $Array['order_id'];
    // $db->Database->insert('notification', $user_array);
    // SendnotificationorderStatus($Array['order_id']);
    // $json['payment_status'] = $Array['payment_status'];
    // $db->Database->update('partyorder_request', $json, $CondArray);

    echo json_encode(array('status' => 'success'));
}

function GetorderstatushistoryBy($orderId = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `order_status_history` LEFT JOIN order_status ON order_status_history.status_id=order_status.status_id  WHERE order_id='$orderId' AND isPartyOrder=0 ORDER BY timestamp ASC";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}

function GetpartyorderstatushistoryBy($orderId = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `order_status_history` WHERE order_id='$orderId' AND isPartyOrder=1 ORDER BY timestamp ASC";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}

function GetNotifications($userId = '') {
    $db = LoadDB();
    $qry = "SELECT notification_id, notification_message,inserted_on as timestamp FROM `notification` WHERE receiver_id='$userId' ORDER BY inserted_on DESC";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}

function GetMenudetails($user_id = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `menu_list` WHERE vendor_id='$user_id' AND archive=0 AND status=0";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}

function GetMenudetailsById($menu_id = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `menu_list` WHERE id='$menu_id' AND archive=0";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}

function GetpromocodeBy($promoId = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `promo_code` WHERE promocode_id='$promoId'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : [];
    return $dArray;
}

function autodelete() {
    $condjson = !empty($_POST['condjson']) ? json_decode($_POST['condjson'], true) : [];
    $dbtable = !empty($_POST['dbtable']) ? $_POST['dbtable'] : '';
    $removefile = !empty($_POST['removefile']) ? $_POST['removefile'] : '';
    if (is_file($removefile)) {
        unlink($removefile);
    }
    if (!empty($dbtable) && is_array($condjson)) {
        $db = LoadDB();
        $db->Database->delete($dbtable, $condjson);
        die(json_encode(array('status' => true, 'message' => 'Successfully')));
    }
}

function GetCuisineByRestaurant($id = '') {
    $db = LoadDB();
    $Qrys = "SELECT * FROM `cuisine` LEFT JOIN restaurant_cuisine_type ON restaurant_cuisine_type.cuisine_id=cuisine.cuisine_id WHERE vendor_id='" . $id . "'";
    $Array = $db->Database->select_qry_array($Qrys);
    return $Array;
}

function updateQuotedPrice() {
    $db = LoadDB();
    $price = $_POST['price'];
    $party_id = $_POST['party_id'];
    $updatedata = array('price' => $price);
    $where = array('id' => $party_id);
    $result = $db->Database->update('partyorder_request', $updatedata, $where);
    echo json_encode(array('status' => 'success'));
}

function GetPartyOrderByUserId($userid = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `party_order` WHERE user_id=" . $userid . " order by id desc";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetPartyOrderById($orderid = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `party_order` WHERE id=" . $orderid . " order by id desc";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function requestPayment() {
    $db = LoadDB();
    $updatedata = array('payment_status' => 2, 'mode_of_payment' => 3);
    $where = array('id' => $_POST['party_id']);
    $result = $db->Database->update('partyorder_request', $updatedata, $where);
    echo json_encode(array('status' => 'success'));
}

function update_status() {

    $db = LoadDB();
    $id = !empty($_POST['feedback_id']) ? $_POST['feedback_id'] : '';
    $CondArray = array('id' => $id);
    $json['status'] = $_POST['status'];
    $db->Database->update('user_feedback', $json, $CondArray);
    die(json_encode(array('status' => true, 'message' => 'Updated successfully')));
}

function getRestaurantList() {
    $db = LoadDB();
    $qry = "SELECT * FROM `restaurant_details` GROUP BY id order by id desc";
    $arr = $db->Database->select_qry_array($qry);
    $html = '';
    for ($i = 0; $i < count($arr); $i++) {
        $d = $arr[$i];
        $select = '';
        if ($id == $d->id) {
            $select = 'selected="selected"';
        }
        $html = $html . '<option ' . $select . ' value="' . $d->id . '">' . $d->restaurant_name . '</option>';
    }
    return $html;
}

function delete_address() {
    $db = LoadDB();
    $address_id = !empty($_POST['address_id']) ? $_POST['address_id'] : '';
    $CondArray = array('id' => $address_id);
    $json['archive'] = 1;
    $db->Database->update('user_order_address', $json, $CondArray);
    die(json_encode(array('status' => true, 'message' => 'Deleted successfully')));
}

function updateAddressStatus() {
    $db = LoadDB();
    $address_id = !empty($_POST['address_id']) ? $_POST['address_id'] : '';
    $json['isDefault'] = 0;
    $session_arr = $db->session->userdata('UserLogin');
    $Cond_Array = array('user_id' => $session_arr->id);
    $db->Database->update('user_order_address', $json, $Cond_Array);
    $CondArray = array('id' => $address_id);
    $jsons['isDefault'] = 1;
    $db->Database->update('user_order_address', $jsons, $CondArray);
    $session_cart = $db->session->userdata('CartData');
    for ($i = 0; $i < count($session_cart); $i++) {
        $session_cart[$i]['delivery_address'] = $address_id;
    }
    $cart = $session_cart;
    $cart = array_values(array_filter($cart));
    $db->session->set_userdata('CartData', $cart);
    die(json_encode(array('status' => true, 'message' => 'Updated successfully')));
}

function GetcmsBycmsId($cmsid) {
    $db = LoadDB();
    $qry = "SELECT * FROM `cms` WHERE identify='$cmsid'";
    $Array = $db->Database->select_qry_array($qry);
    $Array = !empty($Array) ? $Array[0] : '';
    return $Array;
}

function update_position($array) {
    $db = LoadDB();
    $qry = "SELECT * FROM  " . $array['table'] . " where status=0 and archive=0 and position = '" . $array['position'] . "' and id !='" . $array['id'] . "'  and store_type !='" . $array['store_type'] . "'";
    $arr = $db->Database->select_qry_array($qry);
    if (count($arr) > 0) {
        echo $json = '{"status":"error","message":"Position Already exists"}';
        exit;
    } elseif ($array['position'] > 4) {
        echo $json = '{"status":"error","message":"Only 4 position allowed"}';
        exit;
    } else {
        $insert_array = array('position' => $array['position']);
        $cond = array('id' => $array['id'], 'store_type' => $array['store_type']);
        $result = $db->Database->update($array['table'], $insert_array, $cond);
        echo $json = '{"status":"success","message":"Position updated successfully"}';
        exit;
    }
}

function reset_position($array) {
    $db = LoadDB();
    $insert_array = array('position' => 0);
    $cond = array('id' => $array['id']);
    $result = $db->Database->update($array['table'], $insert_array, $cond);
    echo $json = '{"status":"success","message":"Position reset successfully"}';
    exit;
}

function GetdeliveryboyByVendor($id) {
    $db = LoadDB();
    $Qry = "SELECT * FROM `users` WHERE deliveryboy_added_by=" . $id . " order by id desc";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function updateDeliveyCharge() {
    $db = LoadDB();
    $session = GetSessionArrayAdmin();
    $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
    $user = !empty($_POST['user']) && is_array($_POST['user']) ? $_POST['user'] : [];
    $vendor_id = !empty($_POST['vendor_id']) ? $_POST['vendor_id'] : '';
    $cuisine_type = !empty($_POST['cuisine_type']) && is_array($_POST['cuisine_type']) ? $_POST['cuisine_type'] : [];


    $json['delivery_hours_st'] = !empty($json['delivery_hours_st']) ? date("H:i", strtotime($json['delivery_hours_st'])) : '00:00:00';
    $json['delivery_hours_et'] = !empty($json['delivery_hours_et']) ? date("H:i", strtotime($json['delivery_hours_et'])) : '00:00:00';


    $cond = array('vendor_id' => $vendor_id);
    $result = $db->Database->update('restaurant_details', $json, $cond);

    $conduser = array('id' => $vendor_id);
    $db->Database->update('users', $user, $conduser);

    $decolcon = array('vendor_id' => $vendor_id);
    $db->Database->delete('restaurant_cuisine_type', $decolcon);


    $updateLog = [$json, $user, $cuisine_type];
    $log['vendor_id'] = $vendor_id;
    $log['updated_by'] = $session->id;
    $log['update_type'] = 'VENDOR_PROFILE';
    $log['data'] = json_encode($updateLog);
    $log['timestamp'] = date('Y-m-d H:i:s');
    $db->Database->insert('vendor_log', $log);


    for ($i = 0; $i < count($cuisine_type); $i++) {
        $d = $cuisine_type[$i];
        $insert['cuisine_id'] = $d;
        $insert['vendor_id'] = $vendor_id;
        $insert['lastupdate'] = date('Y-m-d H:i:s');
        $insert['timestamp'] = $insert['lastupdate'];
        $db->Database->insert('restaurant_cuisine_type', $insert);
    }

    die(json_encode(array('status' => true, 'message' => 'updated successfully')));
    // echo $json = '{"status":"success","message":"updated successfully"}';
    // exit;
}

function GetRiderCommission() {
    $db = LoadDB();
    $Qry = "SELECT * FROM `rider_commission`";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetOrderStatusList($tId = '') {
    $db = LoadDB();
    if ($tId == '') {
        $CondutaionArray = array('vendor_type' => $tId);
        $Qry = "SELECT * FROM order_status WHERE `vendor_type` =:vendor_type ORDER BY sort_order";
    } else {
        $CondutaionArray = array('');
        $Qry = "SELECT * FROM order_status ORDER BY sort_order";
    }
    $Array = $db->Database->select_qry_array($Qry, $CondutaionArray);
    return $Array;
}

function getRiderTotalEarnedAmount($userId = '') {
    $db = LoadDB();
    $qry = "SELECT SUM(rider_commission) AS totalEarned FROM `orders` WHERE assign_rider='$userId' AND archive=0";
    $rArray = $db->Database->select_qry_array($qry);
    $totalEarned = !empty($rArray[0]->totalEarned) ? $rArray[0]->totalEarned : '0';
    $totalEarned = DecimalAmount($totalEarned);
    return $totalEarned;
}

function checkTableAvailability() {
    $db = LoadDB();
    $vendor_id = $_POST['vendor_id'];
    $table_capacity = $_POST['table_capacity'];
    $no_of_people = $_POST['no_of_people'];
    $reservation_date = $_POST['reservation_date'];
    $reservation_time = $_POST['reservation_time'];

    // $Qry = "SELECT * FROM `book_table` WHERE reservation_date='$reservation_date' AND reservation_time='$reservation_time'";
    // $Qry = "SELECT SUM(no_of_people) as total_table_booking FROM `book_table` WHERE vendor_id='$vendor_id'";
    // $Array = $db->Database->select_qry_array($Qry);
    // $total = $table_capacity - $Array[0]->total_table_booking;
    // if ($total == 0) {
    //     echo $json = '{"status":"error","message":"At the moment, there’s no table availability"}';
    //     exit;
    // }elseif ($total != 0) {
    $Qry1 = "SELECT SUM(no_of_people) as total_table_booking FROM `book_table` WHERE vendor_id='$vendor_id' AND reservation_date='$reservation_date' AND reservation_time='$reservation_time'";
    $Array1 = $db->Database->select_qry_array($Qry1);
    $total1 = $table_capacity - $Array1[0]->total_table_booking;
    $today_date = date('Y-m-d');
    $current_time = date('H:i');
    if ($reservation_date == $today_date && $reservation_time < $current_time) {
        echo $json = '{"status":"error","message":"At the moment, there’s no table availability"}';
        exit;
    } elseif ($total1 == 0 || $no_of_people > $total1) {
        echo $json = '{"status":"error","message":"At the moment, there’s no table availability"}';
        exit;
    } else {
        echo $json = '{"status":"success","message":"Success"}';
        exit;
    }
    // }
    // else{
    //     echo $json = '{"status":"success","message":"Success"}';
    //     exit;
    // }
}

function checkTime() {
    $db = LoadDB();
    $party_date = $_POST['party_date'];
    $party_time = $_POST['party_time'];
    $party_time_end = !empty($_POST['party_time_end']) ? $_POST['party_time_end'] : '';

    $start_time = date('H:i', strtotime($party_time));
    $start_time = date('H:i', strtotime('+1 minutes' . $start_time));


    $end_time = date('H:i', strtotime($party_time_end));

    $resturent_id = !empty($_REQUEST['resturent_id']) ? $_REQUEST['resturent_id'] : '';

    $today_date = date('Y-m-d');
    $current_time = date('H:i');

    if ($party_date == $today_date) {
        if (strtotime($party_time) < strtotime($current_time)) {
            echo $json = '{"status":"error","message":"This facility is not available for past time"}';
            exit;
        } else {

            echo $json = '{"status":"success","message":"Success"}';
            exit;
        }
    }
    $qry = "SELECT P.* FROM `party_order` P  WHERE (TIME_TO_SEC('$start_time') BETWEEN TIME_TO_SEC(P.party_time) AND TIME_TO_SEC(P.party_time_end) OR TIME_TO_SEC('$end_time') BETWEEN TIME_TO_SEC(P.party_time) AND TIME_TO_SEC(P.party_time_end) OR P.party_time BETWEEN '$start_time' AND '$end_time') AND DATE(P.party_date)='$party_date' AND P.vendor_id='$resturent_id'";
    $arr = $db->Database->select_qry_array($qry);

    if (count($arr) > 0) {
        echo $json = '{"status":"error","message":"Not available at the time please choose another time"}';
        exit;
    }
    echo $json = '{"status":"success","message":"Success"}';
    exit;
}

function deliveryboyUpdateCommission() {
    $db = LoadDB();
    $update['commission'] = !empty($_POST['cmamount']) ? $_POST['cmamount'] : '';
    $cond = array('id' => $_POST['dboyid']);
    $db->Database->update('users', $update, $cond);
    die(json_encode(array('status' => true, 'message' => 'updated successfully')));
}

function SendpushNotifactionTorider($orderId = '') {
    $db = LoadDB();
    $order = GetOrderByOrderId($orderId);
    if (empty($order)) {
        return false;
    }
    $order = $order[0];
    if ($order->delivery_type == '1' && $order->isDelivery == '0') {
        $distnc = ",IFNULL(ROUND((6371 * acos( cos( radians($order->latitude) ) * cos( radians(U.latitude) ) * cos( radians(U.longitude) - radians($order->longitude) ) + sin( radians($order->latitude) ) * sin( radians(U.latitude) ) ) ),2),0) AS distnc";
        $qry = "SELECT U.name,U.device_id,U.delivery_radius $distnc FROM `users` U WHERE U.user_type='4' AND U.deliveryboy_added_by=1 AND U.device_id!='' AND U.archive=0 AND status='0' AND U.latitude!='' AND U.longitude!=''";
        $Array = $db->Database->select_qry_array($qry);
       
        for ($i = 0; $i < count($Array); $i++) {
            $d = $Array[$i];
            $subject = "New Order - $order->orderno";
            if ($d->distnc <= $d->delivery_radius) {
                send_pushnotification(array($d->device_id), $subject, $order->address, $identifier = '1', array('order_id' => $order->id));
            }
        }
    }
}

function NeworderSendVendorNotifaction($orderId = '') {
    $order = GetOrderByOrderId($orderId);
    $vendor = GetUserDetails($order[0]->vendor_id);
    if (empty($order) || empty($vendor)) {
        return false;
    }
    $order = $order[0];
    $vendor = $vendor[0];
    $subject = "New order received - $order->orderno";
    $message = "$order->address\nNote: After 3 min request will be active in app.";

    send_pushnotification(array($vendor->device_id), $subject, $message, $identifier = 0, $argument = []);
}

function NewPartyorderSendVendorNotifaction($orderId = '') {
    $order = GetPartyOrderById($orderId);
    $vendor = GetUserDetails($order[0]->vendor_id);
    if (empty($order) || empty($vendor)) {
        return false;
    }
    $order = $order[0];
    $vendor = $vendor[0];
    $subject = "New party order received - $order->party_orderno";
    $message = "Party date : " . date('d/m/Y', strtotime($order->party_date)) . ' ' . date('h:i A', strtotime($order->party_time)) . "\nNumber of people : $order->no_of_people";
    send_pushnotification(array($vendor->device_id), $subject, $message, $identifier = 0, $argument = []);
}

function getdeleverychagesfeecal35($rest_details, $userAdlat = '25.2776', $userAdlong = '55.3527') {
    $rest_details = $rest_details[0];
    $charges_type = !empty($rest_details->charges_type) ? $rest_details->charges_type : '';
    if (empty($charges_type)) {
        return $rest_details->service_charge;
    } else {
        $distance = distance($rest_details->latitude, $rest_details->longitude, $userAdlat, $userAdlong, $unit = 'K');

        if ($distance <= $rest_details->dlvry_chrge_frst_km) {
            return $rest_details->service_charge;
        } else {
            $pendingdis = $distance - $rest_details->dlvry_chrge_frst_km;
            $ExtraDistanceAmt = $pendingdis * $rest_details->dlvry_chrge_perkm;
            $total = $rest_details->service_charge + $ExtraDistanceAmt;
            return $total;
        }
    }
}

function SendNotifactionOrderStatus($orderId = '') {
    $order = GetOrderById($orderId);
    if (empty($order)) {
        return false;
    }
    $order = $order[0];
    $statusAr = GetorderstatusBy($order->order_status);
    $user = GetusersById($order->user_id);
    $subject = "$order->orderno - $statusAr->status_name";
    $message = "Order status updated.";
    send_pushnotification([$user->device_id], $subject, $message, $identifier = '1', $argument = array('order_id' => $orderId));
}

function locationchageBtnclick() {
    $db = LoadDB();
    $location = !empty($_POST['locName']) ? $_POST['locName'] : '';
    $latitude = !empty($_POST['latitude']) ? $_POST['latitude'] : '';
    $longitude = !empty($_POST['longitude']) ? $_POST['longitude'] : '';
    if (!empty($location) && !empty($latitude) && !empty($longitude)) {
        $db->session->set_userdata('location', $location);
        $db->session->set_userdata('latitude', $latitude);
        $db->session->set_userdata('longitude', $longitude);
        echo $json = '{"status":' . true . ',"message":"Success"}';
        exit;
    }
}

function getUserAddressByuserId383($userId = '') {
    $all_address = GetAllAddress($userId);
    $cArray = [];
    foreach ($all_address as $ad) {
        $cArray[] = array(
            'address_id' => $ad->id,
            'address_label' => $ad->address_label,
            'address' => $ad->address,
            'latitude' => $ad->latitude,
            'longitude' => $ad->longitude,
            'street' => $ad->street,
            'additional_direction' => $ad->additional_direction,
            'mobile_code' => $ad->mobile_code,
            'mobile_number' => $ad->mobile_number,
            'landphone_no' => $ad->landphone_no,
            'building' => $ad->building,
            'office' => $ad->office,
            'house' => $ad->house,
            'apartment_no' => $ad->apartment_no,
            'floor' => $ad->floor,
            'address_type' => $ad->address_label,
        );
    }
    return $cArray;
}

function getMenudiscountArray($menuId = '', $menuPrice = '', $vendorId = '') {
    $db = LoadDB();
    $sql = "SELECT P.discount,P.discount_type  FROM `promo_code` P WHERE P.`offer_addedby` = '$vendorId' AND P.date_from<=CURDATE() AND P.date_to>=CURDATE() AND P.status=0 AND P.archive=0 AND FIND_IN_SET($menuId,P.menu_id)";
    $cArray = $db->Database->select_qry_array($sql);
    $return['cross_price'] = '';
    $return['final_price'] = $menuPrice;
    $return['total_discount'] = '';
    if (!empty($cArray)) {
        $cArray = $cArray[0];
        $return['cross_price'] = $menuPrice;
        if (empty($cArray->discount_type)) {
            $discount = $menuPrice * $cArray->discount / 100;
        } else {
            $discount = $cArray->discount;
        }

        $return['total_discount'] = $discount;
        $return['final_price'] = $return['cross_price'] - $discount;
    }
    return $return;
}

function menucategoryorderviewupdate() {
    $db = LoadDB();
    $vendorId = !empty($_POST['vendorId']) ? $_POST['vendorId'] : '';
    $orderview = !empty($_POST['orderview']) ? $_POST['orderview'] : [];
    for ($i = 0; $i < count($orderview); $i++) {
        $d = $orderview[$i];
        $cond['category_id'] = $d['cartId'];
        $cond['vendor_id'] = $vendorId;
        $update['ordercat_view'] = $d['orderview'];

        $db->Database->update('category_positions', $update, $cond);
    }
    die(json_encode(array('status' => true, 'message' => 'updated successfully')));
}

function menulistorderviewupdate() {
    $db = LoadDB();
    $orderview = !empty($_POST['orderview']) ? $_POST['orderview'] : [];
    for ($i = 0; $i < count($orderview); $i++) {
        $d = $orderview[$i];
        $cond['id'] = $d['id'];
        $update['order_view'] = $d['orderview'];
        $db->Database->update('menu_list', $update, $cond);
    }
    die(json_encode(array('status' => true, 'message' => 'updated successfully')));
}

function vendororderviewupdate() {
    $db = LoadDB();
    $orderview = !empty($_POST['orderview']) ? $_POST['orderview'] : [];

    for ($i = 0; $i < count($orderview); $i++) {
        $d = $orderview[$i];
        $cond['id'] = $d['id'];
        $update['position'] = $d['orderview'];
        $db->Database->update('users', $update, $cond);
    }
    die(json_encode(array('status' => true, 'message' => 'updated successfully')));
}

function uploadquotationBillRequest() {
    $db = LoadDB();
    $UploadExcel = empty($_FILES['UploadExcel']['error']) ? $_FILES['UploadExcel'] : [];


    $fileName = uniqid() . '.' . pathinfo($UploadExcel['name'], PATHINFO_EXTENSION);
    $baseDir = HOME_DIR . "uploads/prescription/" . $fileName;
    $oldfile = !empty($_POST['oldfile']) ? $_POST['oldfile'] : '';
    if (move_uploaded_file($UploadExcel['tmp_name'], $baseDir)) {
        $oldfile = HOME_DIR . "uploads/prescription/" . $oldfile;
        if (is_file($oldfile)) {
            unlink($oldfile);
        }
        die(json_encode(array('status' => true, 'message' => 'Successfully uploaded.', 'file_path' => $fileName)));
    }
    die(json_encode(array('status' => false, 'message' => 'Successfully.')));
}

function getnewuserlistforchat() {
    $name = !empty($_POST['name']) ? $_POST['name'] : '';
    $today = date('Y-m-d H:i:s');
    if (!empty($name)) {
        $db = LoadDB();
        $qry = "SELECT email,id FROM `users` U WHERE U.user_type='3' AND U.email='$name'";

        $rArray = $db->Database->select_qry_array($qry);

        $userId = '';
        if (empty($rArray)) {
            $Inst['name'] = 'user-' . mt_rand(2000, 9000);
            $Inst['email'] = $name;
            $Inst['password'] = md5(0000);
            $Inst['mobile_code'] = '971';
            $Inst['mobile_number'] = '';
            $Inst['device_id'] = '';
            $Inst['user_type'] = '3';
            $Inst['is_approved'] = 1;
            $Inst['lastupdate'] = $today;
            $Inst['timestamp'] = $today;
            $userId = $db->Database->insert('users', $Inst);
        } else {
            $userId = $rArray[0]->id;
        }
        die(json_encode(array('status' => true, 'message' => 'success.', 'user_id' => $userId)));
    }
    die(json_encode(array('status' => false, 'message' => 'invalid name.')));
}

function readyMychat() {
    $db = LoadDB();
    $cond['chat_id'] = $_POST['chat_id'];
    $update['admin_read'] = 1;
    $db->Database->update('chat_history', $update, $cond);
    die(json_encode(array('status' => true, 'message' => 'success.', 'list' => '')));
}

function autochatNotifaction() {
    $db = LoadDB();
    $session = GetSessionArrayAdmin();
    $todaysss = date('Y-m-d');
    $iddddp = !empty($_POST['iddddp']) ? $_POST['iddddp'] : '';

//    $webNotifaction = $db->session->userdata('webNotifaction');
//    $webNotifaction = !empty($webNotifaction) && is_array($webNotifaction) ? $webNotifaction : [];



    $qry = "SELECT CH.*,UD.name FROM `chat_history` CH LEFT JOIN users UD ON UD.id=CH.sender_id WHERE CH.admin_read=0 AND sender_id NOT IN (1)";
    $rArray = $db->Database->select_qry_array($qry);

    //  $qry = "SELECT O.orderno,O.id,UOD.address FROM `orders` O LEFT JOIN user_order_address UOD ON UOD.id=O.delivery_address WHERE O.admin_read=0 AND O.archive=0 AND O.order_status NOT IN (7) ORDER BY O.id DESC LIMIT 10";
    $qry = "SELECT O.orderno,O.id,UOD.address,O.admin_read FROM `orders` O LEFT JOIN user_order_address UOD ON UOD.id=O.delivery_address WHERE NOT FIND_IN_SET('$session->id',O.admin_read) AND DATE(O.date)='$todaysss' AND O.archive=0 AND O.order_status NOT IN (7) ORDER BY O.id DESC ";

    $notifications = $db->Database->select_qry_array($qry);


    for ($i = 0; $i < count($notifications); $i++) {
        $d = $notifications[$i];
        $admin_read = !empty($d->admin_read) ? explode(',', $d->admin_read) : [];
        $admin_read = !empty($admin_read) && is_array($admin_read) ? $admin_read : [];
        $admin_read[] = $session->id;

        $cond33['id'] = $d->id;
        $update33['admin_read'] = implode(',', $admin_read);
        $db->Database->update('orders', $update33, $cond33);
    }

    // $db->session->set_userdata('webNotifaction', $webNotifaction);


    for ($i = 0; $i < count($rArray); $i++) {
        $cond['chat_id'] = $rArray[$i]->chat_id;
        $update['admin_read'] = 1;
        $db->Database->update('chat_history', $update, $cond);
    }
    die(json_encode(array('status' => true, 'message' => 'success.', 'notifications' => $notifications, 'list' => $rArray)));
}

function autochatNotifactionvendor() {
    $db = LoadDB();
    $vendor = GetSessionArrayVendor();

//    print_r($vendor);
//    exit;
//    $webNotifaction = $db->session->userdata('webNotifactionVendor');
//    $webNotifaction = !empty($webNotifaction) && is_array($webNotifaction) ? $webNotifaction : [];
    //  $newNot = [];

    $qry = "SELECT O.orderno,O.id,UOD.address FROM `orders` O LEFT JOIN user_order_address UOD ON UOD.id=O.delivery_address WHERE O.vendor_read=0 AND O.vendor_id='$vendor->id' AND O.archive=0 AND O.order_status NOT IN (7) ORDER BY O.id DESC LIMIT 10";
    $notifications = $db->Database->select_qry_array($qry);

    for ($i = 0; $i < count($notifications); $i++) {
        $d = $notifications[$i];
        $cond33['id'] = $d->id;
        $update33['vendor_read'] = '1';
        $db->Database->update('orders', $update33, $cond33);
//        if (!in_array($d->orderno, $webNotifaction)) {
//            $newNot[] = $d;
//            $webNotifaction[] = $d->orderno;
//        }
    }

    // $db->session->set_userdata('webNotifactionVendor', $webNotifaction);





    die(json_encode(array('status' => true, 'message' => 'success.', 'notifications' => $notifications)));
}

function addNewOrderNotifaction($userId, $vendorId, $orderId) {
    $db = LoadDB();
    $vendor_array['sender_id'] = $userId;
    $vendor_array['receiver_id'] = $vendorId;
    $vendor_array['notification_type'] = NEW_ORDER;
    $vendor_array['notification_message'] = 'New Order Received';
    $vendor_array['redirect_url'] = 'vendor/OrderDetails' . base64_encode($orderId);
    ;
    $vendor_array['order_id'] = $orderId;
    $vendor_array['inserted_on'] = date('Y-m-d H:i:s');

    $db->Database->insert('notification', $vendor_array);

    $admin_array['sender_id'] = $userId;
    $admin_array['receiver_id'] = '1';
    $admin_array['notification_type'] = NEW_ORDER;
    $admin_array['notification_message'] = 'New Order Received';
    $admin_array['redirect_url'] = 'admin/OrderDetails/' . base64_encode($orderId);
    $admin_array['order_id'] = $orderId;
    $admin_array['inserted_on'] = date('Y-m-d H:i:s');

    $db->Database->insert('notification', $admin_array);
    return true;
}
