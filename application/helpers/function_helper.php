<?php

function updateRidercommissionByOrder($orderId = '') {
    $db = LoadDB();
    $order = GetOrderById($orderId);
    $order = !empty($order) ? $order[0] : '';
    $assignRider = !empty($order->assign_rider) ? $order->assign_rider : '';
    $rider = GetusersById($assignRider);

    $riderCommissionId = !empty($rider->rider_commission_id) ? $rider->rider_commission_id : '';
    $comArray = GetridercommissionBy($riderCommissionId);


    if (!empty($rider) && !empty($comArray)) {
        $vendor = GetusersById($order->vendor_id);
        $address = GetOrderAddressById($order->delivery_address);
        if (empty($address)) {
            return 0;
        }
        $address = $address[0];
        $distanceTotal = $order->distance_ord;
        $basePrice = $comArray->commission;
        if (!empty($comArray->additional_commission)) {
            if ($distanceTotal > $comArray->km) {
                $pendingDis = $distanceTotal - $comArray->km;
                $basePrice = $basePrice + ($pendingDis * $comArray->additional_commission);
            }
        }
        $cond = array('id' => $order->order_id);
        $update ['rider_commission'] = $basePrice;
        $db->Database->update('orders', $update, $cond);
        return true;
    }
}

function GetRestaurantDetails($restaurantId = '') {
    $db = LoadDB();

    $Qry = "SELECT users.*,restaurant_details.*,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar,restaurant_details.id as rest_id,users.store_type,restaurant_details.table_booking,restaurant_details.table_capacity,restaurant_details.table_booking_opening_time,restaurant_details.table_booking_closing_time FROM `users` 
            JOIN `restaurant_details` ON `restaurant_details`.`vendor_id`=`users`.`id` 
            left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
            left join cuisine on cuisine.cuisine_id=rct.cuisine_id
            WHERE users.id='$restaurantId' GROUP BY restaurant_details.vendor_id,restaurant_details.id ";
    //print_r($Qry);    
    $DetailsArray = $db->Database->select_qry_array($Qry);
    if (!empty($DetailsArray)) {
        $resImage = 'uploads/vendor_images/' . $DetailsArray[0]->image;
        $resImage = is_file(HOME_DIR . $resImage) ? base_url($resImage) : base_url(DEFAULT_LOGO_RESTAURANT);
        $DetailsArray[0]->resImage = $resImage;
    }
    return $DetailsArray;
}

function getBusinessName($id = '') {
    $db = LoadDB();
    $store_type = $id;
    $store_details = "SELECT restaurant_details.vendor_id,restaurant_details.restaurant_name FROM `users` 
    LEFT JOIN restaurant_details ON restaurant_details.vendor_id=users.id WHERE store_type='$store_type' AND archive=0 AND status=1  
    ORDER BY users.id DESC";

    $store_data = $db->Database->select_qry_array($store_details);
    return $store_data;
}

function get_menu_search() {

    $db = LoadDB();
    $session_cart = $db->session->userdata('CartData');

    if (!empty($session_cart)) {
        $session_vendor = $session_cart[0]['vendor_id'];
        $party = $session_cart[0]['party'];
    } else {
        $session_vendor = '';
        $party = '';
    }
    $menu_name = $_POST['menu'];
    $vendor_id = $_POST['vendor_id'];
    $get_restaurant_details = GetRestaurantDetails($vendor_id);
    $Qry = "SELECT menu_list.id,category_details.category_name,menu_list.vendor_id,category_details.id as category_id,menu_list.menu_name,description,price,image,choice,menu_list.status,menu_list.archive,menu_list.lastupdate,menu_list.timestamp 
    FROM `menu_list` 
    LEFT JOIN most_selling ON most_selling.menu_id=menu_list.id 
    LEFT JOIN category_details ON (category_details.id=menu_list.category_id OR category_details.id=most_selling.category_id) WHERE menu_list.menu_name LIKE '%$menu_name%' AND (menu_list.vendor_id='$vendor_id' OR most_selling.vendor_id='$vendor_id') 
    GROUP BY category_details.id,menu_list.id 
    ORDER BY category_details.position";
    // $Qry = "SELECT * FROM `category_details` JOIN `menu_list` ON `menu_list`.`category_id`=`category_details`.`id` LEFT JOIN most_selling ON most_selling.menu_id=menu_list.id  WHERE menu_list.menu_name LIKE '%$menu_name%' AND menu_list.vendor_id='$vendor_id' GROUP BY category_details.id ";
    //print_r($Qry);die();
    $CategoryArray = $db->Database->select_qry_array($Qry);
    // print_r($CategoryArray);die();
    $html = '';
    $result = '';
    if (count($CategoryArray) > 0) {
        $i = 1;
        foreach ($CategoryArray as $cat) {

            $html .= '<li><a href="#">' . $cat->category_name . '</a></li>';

            $result .= '<div class="panel panel-default">
                            <div class="panel-heading active" role="tab" id="headingOne">
    									<h4 class="panel-title">
                                            <a id="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">' .
                    $cat->category_name . '</a>
                                        </h4> 
                                    </div>';
            //     if($cat->choice!=1)
            //  {
            //      $class = 'add_to_cart';
            //  }else{
            //      $class = 'sub_cat';
            //  }
            $arr1 = array();
            $class = '';
            if (!empty($session_cart) && $session_cart[0]['party'] == 0) {

                for ($i = 0; $i < count($session_cart); $i++) {
                    array_push($arr1, $session_cart[$i]['menu_id']);
                }
                $menu_val = (int) trim($cat->id);

                if (in_array($menu_val, $arr1)) {

                    foreach ($session_cart as $d) {
                        if ($d['menu_id'] == $menu_val) {

                            $class = '';
                        }
                    }
                } else {
                    if ($cat->choice != 1) {
                        $class = 'add_to_cart';
                    } else {
                        $class = 'sub_cat';
                    }
                }
            } else {
                if ($cat->choice != 1) {
                    $class = 'add_to_cart';
                } else {
                    $class = 'sub_cat';
                }
            }
            $opening_time = date('H:i', strtotime($get_restaurant_details[0]->opening_time));
            $closing_time = date('H:i', strtotime($get_restaurant_details[0]->closing_time));
            $currentTime = date('H:i', time());
            if ($currentTime > $opening_time && $get_restaurant_details[0]->busy_status == 1) {
                $busy = 'data-busy=1';
            } else {
                $busy = '';
            }


            // if ($currentTime < $opening_time) {
            if (($closing_time < $opening_time && ($currentTime <= $opening_time && $currentTime >= $closing_time)) ||
                    ($closing_time > $opening_time && ($currentTime <= $opening_time || $currentTime >= $closing_time))) {
                $close = 'data-close=1';
            } else {
                $close = '';
            }
            $result .= '
                        <div id="collapseOne' . $i . '" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
    					<div class="panel-body">
    					<div class="wd100 __itm_crtwp">
    					<div class="media ' . $class . '" data-category="' . $cat->category_id . '" data-party="0" data-id="' . $cat->id . '" data-choice="' . $cat->choice . '" data-vendor="' . $cat->vendor_id . '" data-sessionvendor="' . $session_vendor . '" data-sessionparty="' . $party . '" "' . $close . '" "' . $busy . '">';
            $image = base_url() . 'uploads/menu/' . $cat->image;
            if (isset($cat->image) && $cat->image != '') {
                $result .= '<img class="__itm_crtimg" src="' . $image . '">';
            } else {
                $result .= '<img class="__itm_crtimg" src="' . base_url(DEFAULT_LOGO_MENU) . '">';
            }
            $result .= '<div class="media-body">
    					<h3>' . $cat->menu_name . '</h3>
    				    <div class="wd100 __ctdcrp">
    					<p>' . $cat->description . '</p>
    													
    					</div>
    					</div>
    					<div class="col-4 _cartcm">
    					<div class="row">
    					<div class="col p-0">';

            if ($cat->price != 0) {
                $result .= '<div class="wd100 _cartpris"> ' . $db->lang->line("aed") . '' . DecimalAmount($cat->price) . '</div>';
            } else {
                $result .= '<div class="wd100 _cartpris sub_cat" class="option_product">' . $db->lang->line("price_on_selection") . '</div>';
            }
            $result .= '
    		
    					</div>
    					<div class="col-5 p-0">';
            if ($cat->choice != 1) {

                if (!empty($session_cart) && $session_cart[0]['party'] == 0) {
                    $arr = array();
                    for ($i = 0; $i < count($session_cart); $i++) {
                        array_push($arr, $session_cart[$i]['menu_id']);
                    }
                    $menu_val = (int) trim($cat->id);

                    if (in_array($menu_val, $arr)) {
                        foreach ($session_cart as $d) {
                            if ($d['menu_id'] == $menu_val) {
                                $result .= '<div class="  __pqty">
                                <section class="qty-update-section incart b-a ng-scope">
                                <input type="hidden" value="' . $get_restaurant_details[0]->store_type . '" id="store_type">
								<button type="button" class="btn btn-sm b-r" onclick="updateQty(this)" data-id="' . $d['menu_id'] . '" data-cart="' . $d['cart_id'] . '"  data-type="minus" data-value="' . $d['quantity'] . '">
									<i class="fa fa-minus orange"></i>
								</button>
								<span class="f-11" data-id="' . $d['menu_id'] . '" id="quantity"><b>' . $d['quantity'] . '</b></span>
								<button type="button" class="btn btn-sm b-l" onclick="updateQty(this)" data-id="' . $d['menu_id'] . '" data-cart="' . $d['cart_id'] . '"  data-type="plus" data-value="' . $d['quantity'] . '">
									<i class="fa fa-plus orange"></i>
								</button>
                               
                                </section>
                                </div>';
                            }
                        }
                    } else {
                        $result .= '<div class="__crtbtn"  data-category="' . $cat->category_id . '" data-id="' . $cat->id . '" data-party="0" data-choice="' . $cat->choice . '" data-vendor="' . $cat->vendor_id . '" data-sessionvendor="' . $session_vendor . '" data-sessionparty="' . $party . '" "' . $close . '" "' . $busy . '"> Add <i class="fas fa-plus"></i> </div>';
                    }
                } else {

                    $result .= '<div class="__crtbtn"  data-category="' . $cat->category_id . '" data-id="' . $cat->id . '" data-party="0" data-choice="' . $cat->choice . '" data-vendor="' . $cat->vendor_id . '" data-sessionvendor="' . $session_vendor . '" data-sessionparty="' . $party . '" "' . $close . '" "' . $busy . '"> Add <i class="fas fa-plus"></i> </div>';
                }
            } else {

                $result .= ' <div class="__crtbtn"  data-category="' . $cat->category_id . '" data-id="' . $cat->id . '" data-party="0" data-choice="' . $cat->choice . '" data-vendor="' . $cat->vendor_id . '" data-sessionparty="' . $party . '" data-sessionvendor="' . $session_vendor . '" "' . $close . '" "' . $busy . '">  
							    Add <i class="fas fa-plus"></i> 
							    
							    </div>
							    <span class="have_options">customizable</span>';
            }
            $result .= '</div>
    					</div>
    					</div>
    					</div>
    					</div>
    					</div>';
            $i++;
        }
    }

    $data = array('data1' => $html, 'data2' => $result);
    echo json_encode($data);
}

function get_partymenu_search() {

    $db = LoadDB();
    $session_cart = $db->session->userdata('CartData');
    if (!empty($session_cart)) {
        $session_vendor = $session_cart[0]['vendor_id'];
        $session_party = $session_cart[0]['party'];
    } else {
        $session_vendor = '';
        $session_party = '';
    }
    $menu_name = $_POST['menu'];
    $vendor_id = $_POST['vendor_id'];
    $get_restaurant_details = GetRestaurantDetails($vendor_id);
    // $Qry = "SELECT party_order_menu.id,category_details.category_name,party_order_menu.vendor_id,category_details.id as category_id,party_order_menu.menu_name,description,price,image,choice,party_order_menu.status,party_order_menu.archive,party_order_menu.lastupdate,party_order_menu.timestamp 
    // FROM `party_order_menu` 
    // LEFT JOIN category_details ON (category_details.id=party_order_menu.category_id OR category_details.id=most_selling.category_id) WHERE party_order_menu.menu_name LIKE '%$menu_name%' AND (party_order_menu.vendor_id='$vendor_id' OR most_selling.vendor_id='$vendor_id') 
    // GROUP BY category_details.id,party_order_menu.id 
    // ORDER BY category_details.position";
    $Qry = "SELECT * FROM `category_details` JOIN `party_order_menu` ON `party_order_menu`.`category_id`=`category_details`.`id`  WHERE party_order_menu.menu_name LIKE '%$menu_name%' AND party_order_menu.vendor_id='$vendor_id' GROUP BY category_details.id ";
    //print_r($Qry);die();
    $CategoryArray = $db->Database->select_qry_array($Qry);
    // print_r($CategoryArray);die();
    $html = '';
    $result = '';
    if (count($CategoryArray) > 0) {
        $i = 1;
        foreach ($CategoryArray as $cat) {

            $html .= '<li><a href="#">' . $cat->category_name . '</a></li>';

            $result .= '<div class="panel panel-default">
                            <div class="panel-heading active" role="tab" id="headingOne">
    									<h4 class="panel-title">
                                            <a id="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">' .
                    $cat->category_name . '</a>
                                        </h4> 
                                    </div>';

            $opening_time = date('H:i', strtotime($get_restaurant_details[0]->opening_time));
            $closing_time = date('H:i', strtotime($get_restaurant_details[0]->closing_time));
            $currentTime = date('H:i', time());
            if ($currentTime > $opening_time && $get_restaurant_details[0]->busy_status == 1) {
                $busy = 'data-busy=1';
            } else {
                $busy = '';
            }


            // if ($currentTime < $opening_time) {
            if (($closing_time < $opening_time && ($currentTime <= $opening_time && $currentTime >= $closing_time)) ||
                    ($closing_time > $opening_time && ($currentTime <= $opening_time || $currentTime >= $closing_time))) {
                $close = 'data-close=1';
            } else {
                $close = '';
            }
            $arr2 = array();
            $class = '';
            if (!empty($session_cart) && $session_cart[0]['party'] == 1) {

                for ($i = 0; $i < count($session_cart); $i++) {
                    array_push($arr2, $session_cart[$i]['menu_id']);
                }
                $menu_val = (int) trim($cat->id);

                if (in_array($menu_val, $arr2)) {

                    foreach ($session_cart as $d) {
                        if ($d['menu_id'] == $menu_val) {

                            $class = '';
                        }
                    }
                } else {
                    $class = 'add_to_cart';
                }
            } else {
                $class = 'add_to_cart';
            }
            $result .= '
                        <div id="collapseOne' . $i . '" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
    					<div class="panel-body">
    					<div class="wd100 __itm_crtwp">
    					<div class="media ' . $class . '" data-category="' . $cat->category_id . '" data-id="' . $cat->id . '" data-party="1" data-choice="0" data-vendor="' . $cat->vendor_id . '" data-sessionvendor="' . $session_vendor . '" data-sessionparty="' . $session_party . '" "' . $close . '" "' . $busy . '">';
            $image = base_url() . 'uploads/party_menu/' . $cat->image;
            if (isset($cat->image) && $cat->image != '') {
                $result .= '<img class="__itm_crtimg" src="' . $image . '">';
            } else {
                $result .= '<img class="__itm_crtimg" src="' . base_url(DEFAULT_LOGO_MENU) . '">';
            }
            $result .= '<div class="media-body">
    					<h3>' . $cat->menu_name . '</h3>
    				    <div class="wd100 __ctdcrp">
    					<p>' . $cat->description . '</p>
    													
    					</div>
    					</div>
    					<div class="col-4 _cartcm">
    					<div class="row">
    					<div class="col p-0">';

            if ($cat->price != 0) {
                $result .= '<div class="wd100 _cartpris"> ' . $db->lang->line("aed") . '' . DecimalAmount($cat->price) . '</div>';
            } else {
                $result .= '<div class="wd100 _cartpris sub_cat" class="option_product">' . $db->lang->line("price_on_selection") . '</div>';
            }
            $result .= '
    		
    					</div>
    					<div class="col-5 p-0">';
            $arr3 = array();
            if (!empty($session_cart) && $session_cart[0]['party'] == 1) {
                $arr3 = array();
                for ($i = 0; $i < count($session_cart); $i++) {
                    array_push($arr3, $session_cart[$i]['menu_id']);
                }
                $menu_val = (int) trim($cat->id);

                if (in_array($menu_val, $arr3)) {
                    foreach ($session_cart as $d) {
                        if ($d['menu_id'] == $menu_val) {
                            $result .= '<div class="  __pqty">
                                <section class="qty-update-section incart b-a ng-scope">
                                <input type="hidden" value="' . $get_restaurant_details[0]->store_type . '" id="store_type">
								<button type="button" class="btn btn-sm b-r" onclick="updateQty(this)" data-id="' . $d['menu_id'] . '" data-cart="' . $d['cart_id'] . '"  data-type="minus" data-value="' . $d['quantity'] . '">
									<i class="fa fa-minus orange"></i>
								</button>
								<span class="f-11" data-id="' . $d['menu_id'] . '" id="quantity"><b>' . $d['quantity'] . '</b></span>
								<button type="button" class="btn btn-sm b-l" onclick="updateQty(this)" data-id="' . $d['menu_id'] . '" data-cart="' . $d['cart_id'] . '"  data-type="plus" data-value="' . $d['quantity'] . '">
									<i class="fa fa-plus orange"></i>
								</button>
                               
                                </section>
                                </div>';
                        }
                    }
                } else {
                    $result .= '<div class="__crtbtn"  data-category="' . $cat->category_id . '" data-id="' . $cat->id . '" data-party="1" data-choice="0" data-vendor="' . $cat->vendor_id . '" data-sessionparty="' . $session_party . '" data-sessionvendor="' . $session_vendor . '" "' . $close . '" "' . $busy . '"> Add <i class="fas fa-plus"></i> </div>';
                }
            } else {
                $result .= '<div class="__crtbtn"  data-category="' . $cat->category_id . '" data-id="' . $cat->id . '" data-party="1" data-choice="0" data-vendor="' . $cat->vendor_id . '" data-sessionparty="' . $session_party . '" data-sessionvendor="' . $session_vendor . '" "' . $close . '" "' . $busy . '"> Add <i class="fas fa-plus"></i> </div>';
            }
            $result .= ' </div>
    					</div>
    					</div>
    					</div>
    					</div>
    					</div>';
            $i++;
        }
    }

    $data = array('data1' => $html, 'data2' => $result);
    echo json_encode($data);
}

function bookingFacility() {
    $db = LoadDB();
    $table_booking = trim($_POST['table_booking']);
    $rest_name = trim($_POST['rest']);
    $location = trim($_POST['location']);
    // $cusine_type = !empty($_POST['cusine_type'])?$_POST['cusine_type']:0;
    // $cusine_data = $_POST['cuisine_type'];
    $cusine_data = !empty($_POST['cuisine_type']) ? $_POST['cuisine_type'] : '';
    $cuisine = explode(',', $cusine_data);
    $cusine_type = implode("','", $cuisine);
    $latitude = floor($_POST['latitude'] * 100) / 100;
    $longitude = floor($_POST['longitude'] * 100) / 100;
    $loc_data = $_POST['locality'];

    // $Qry1 = "SELECT restaurant_details.vendor_id,restaurant_details.delivery_location
    //     FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
    //      WHERE user_type=2 AND delivery_location !='' and (delivery_location LIKE CONCAT('%', '".$latitude."' ,'%'))
    //     GROUP BY users.id DESC ORDER BY FIELD(restaurant_details.isPremium ,0, 1) DESC";
    // $Array1 = $db->Database->select_qry_array($Qry1);
    // $ids = [];
    // for($i=0;$i<count($Array1);$i++){
    //     $d = $Array1[$i];
    //     array_push($ids,$d->vendor_id);
    // }
    // $arr = implode("','",$ids); 
    $store_type = $_POST['store_type'];
    $store_name = GetNameById($store_type, 'store_type', 'store_type');
    // $Qry = "SELECT city_id FROM  cities WHERE cities.city_name LIKE CONCAT('%', '".trim($loc_data)."' ,'%')";
    // $Array = $db->Database->select_qry_array($Qry);
    // $city_id = $Array[0]->city_id;
    $Qry1 = "SELECT restaurant_details.vendor_id,delivery_timings.cities
        FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        join delivery_timings on delivery_timings.vendor_id = restaurant_details.vendor_id 
        left join cities on cities.city_id = delivery_timings.cities
        WHERE user_type=2 AND store_type='$store_type' AND latitude LIKE CONCAT('%', '" . $latitude . "' ,'%')";
    $Array1 = $db->Database->select_qry_array($Qry1);

    // $Qry_1 = "SELECT restaurant_details.vendor_id,".
    //         "GeoDistDiff('km', '{$latitude}', '{$longitude}', users.latitude, users.longitude) as distance".
    //         " FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
    //         WHERE user_type=2 AND store_type='$store_type' AND isDelivery=0 HAVING distance <= 50";
    // $Array_1 = $db->Database->select_qry_array($Qry_1);
    // $Array_new = array_merge($Array1,$Array_1);


    $ids = [];
    for ($i = 0; $i < count($Array1); $i++) {
        $d = $Array1[$i];
        array_push($ids, $d->vendor_id);
    }

    $arr = implode("','", $ids);
    if ($cusine_type != '') {

        $Qry = "SELECT users.id,users.image,users.position,restaurant_details.*,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
            left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
            left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
            WHERE restaurant_details.vendor_id IN('" . $arr . "') AND restaurant_details.table_booking=1 AND restaurant_name LIKE '%" . $rest_name . "%' AND rct.cuisine_id IN('" . $cusine_type . "') 
            GROUP BY users.id,restaurant_details.id
            ORDER BY users.position = 0, users.position";
        // $Qry = "SELECT users.id,users.image,restaurant_details.*,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        //     left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
        //     left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
        //     WHERE restaurant_details.vendor_id IN('".$arr."') AND restaurant_name LIKE '%" . $rest_name . "%' AND rct.cuisine_id ='" . $cusine_type . "' 
        //     GROUP BY users.id,restaurant_details.id
        //     ORDER BY FIELD(restaurant_details.isPremium ,0, 1) DESC";
        // $Qry = "SELECT users.id,users.image,restaurant_details.restaurant_name,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount,restaurant_details.isDelivery,restaurant_details.delivery_time_start,restaurant_details.delivery_time_ends FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        //     left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
        //     left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
        //     WHERE (city LIKE CONCAT('%', '".$location."' ,'%') or area LIKE CONCAT('%', '".$location."' ,'%') or 
        //     street LIKE CONCAT('%', '".$location."' ,'%')) AND restaurant_name LIKE '%" . $rest_name . "%' AND rct.cuisine_id ='" . $cusine_type . "' GROUP BY users.id ORDER BY FIELD(restaurant_details.isPremium ,0, 1) DESC";
    } else {
        $Qry = "SELECT users.id,users.image,users.position,restaurant_details.*,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
            left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
            left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
            WHERE restaurant_details.vendor_id IN('" . $arr . "') AND restaurant_details.table_booking=1 AND restaurant_name LIKE '%" . $rest_name . "%'  
            GROUP BY users.id,restaurant_details.id
            ORDER BY users.position = 0, users.position";
        // $Qry = "SELECT users.id,users.image,restaurant_details.restaurant_name,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount,restaurant_details.isDelivery,restaurant_details.delivery_time_start,restaurant_details.delivery_time_ends FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        //     left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
        //     left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
        //     WHERE (city LIKE CONCAT('%', '".$location."' ,'%') or area LIKE CONCAT('%', '".$location."' ,'%') or 
        //     street LIKE CONCAT('%', '".$location."' ,'%')) AND restaurant_name LIKE '%" . $rest_name . "%'  GROUP BY users.id ORDER BY FIELD(restaurant_details.isPremium ,0, 1) DESC";
    }

    $Array = $db->Database->select_qry_array($Qry);

    $html = '';
    $result = '';
    $result1 = '';
    if (count($Array) > 0) {
        foreach ($Array as $details) {
            $Qry1 = "SELECT  delivery_timings.*, GROUP_CONCAT(city_name) AS city_name FROM  delivery_timings 
                                        INNER JOIN cities ON FIND_IN_SET(city_id, cities) > 0 WHERE delivery_timings.vendor_id = $details->id AND cities.city_name LIKE CONCAT('%', '" . trim($_POST['locality']) . "' ,'%') GROUP BY cities,delivery_timings.id";
            // print_r($Qry1);
            $Array = $db->Database->select_qry_array($Qry1);
            foreach ($Array as $dat) {
                $start_time = date('H:i', strtotime($dat->delivery_from));
                $end_time = date('H:i', strtotime($dat->delivery_to));
                $currentTime = date('H:i', time());
                //print_r($currentTime);
                if ($start_time != '00:00:00' && $end_time != '00:00:00') {
                    // if (($end_time < $start_time && ($currentTime <= $start_time && $currentTime >= $end_time)) || 
                    // ($end_time > $start_time && ($currentTime <= $start_time || $currentTime >= $end_time)) ){
                    //     $delivery = 0;
                    // }else{
                    //     $delivery = 1;
                    // }
                    if ($currentTime > $start_time && $currentTime < $end_time) {
                        $delivery = 1;
                    } elseif ($currentTime < $start_time && $currentTime < $end_time) {
                        $delivery = 1;
                    } else {
                        $delivery = 0;
                    }
                }
                // print_r($delivery);
                if ($delivery == 1) {
                    $delivery_time = $db->lang->line("same_day");
                } else {
                    $delivery_time = $db->lang->line("next_day");
                }
            }

            $html .= '<div class="wd100 __gdwp">
    							<div class="media">';

            if (!empty($details->image)) {
                $image = base_url() . 'uploads/vendor_images/' . $details->image;
            } else {
                $image = base_url() . 'images/default_rest.png';
            }
            $qry = "SELECT count(id) as total_reviews,AVG(rating) as rating FROM user_feedback WHERE vendor_id=" . $details->vendor_id;
            $Array1 = $db->Database->select_qry_array($qry);
            $rating = !empty($Array1[0]->rating) ? number_format($Array1[0]->rating, 1) : 0;

            $ConditionArray = array('vendor_id' => $details->vendor_id);
            $Qry2 = "SELECT IFNULL(GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ','), '') as cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar FROM `restaurant_cuisine_type` LEFT JOIN `cuisine` ON cuisine.cuisine_id=restaurant_cuisine_type.cuisine_id  WHERE vendor_id=:vendor_id";
            $Array2 = $db->Database->select_qry_array($Qry2, $ConditionArray);

            if ($db->session->userdata('language') == 'ar') {
                if ($Array2[0]->cuisine_name_ar != '') {
                    $cuisin = $Array2[0]->cuisine_name_ar;
                } else {
                    $cuisin = $Array2[0]->cuisine_name;
                }
            } else {
                $cuisin = $Array2[0]->cuisine_name;
            }

            if ($db->session->userdata('language') == 'ar') {
                if ($details->restaurant_name_ar != '') {
                    $restaur_name = $details->restaurant_name_ar;
                } else {
                    $restaur_name = $details->restaurant_name;
                }
            } else {
                $restaur_name = $details->restaurant_name;
            }

            $html .= '<a href="#">';
            $opening_time = date('H:i A', strtotime($details->opening_time));
            $closing_time = date('H:i A', strtotime($details->closing_time));
            $currentTime = date('H:i', time());

            if (($closing_time < $opening_time && ($currentTime <= $opening_time && $currentTime >= $closing_time)) ||
                    ($closing_time > $opening_time && ($currentTime <= $opening_time || $currentTime >= $closing_time))) {
                $html .= '<div class="__closed">
        				Closed	
        			  </div>';
            }

            if ($currentTime > $opening_time && $details->busy_status == 1) {
                $html .= '<div class="__busy">
        					Busy	
        				</div>';
            }
            $html .= '<img class="mr-4 ml-4 __gdimg" src="' . $image . '"></a>
    				 <div class="media-body __gdtx">
    				 <a href="' . base_url('search_details/' . base64_encode($details->vendor_id)) . '">
    				 <h3>' . $restaur_name . '</h3>
    				<div class="wd100 __gd_subtag">' . $cuisin . '</div>
    				<div class="wd100 __star_reviews">
        			<div class="__star"> <i class="fa fa-star"></i>' . $rating . '</div>
    				<div class="_reviews">(' . $Array1[0]->total_reviews . ' reviews)</div>
        			</div>
					<div class="wd100 __subinfo">
					<ul>';
            // 	$start_time = date('H:i', strtotime($details->delivery_time_start));
            // 	$end_time = date('H:i', strtotime($details->delivery_time_ends));
            // 	$currentTime = date('H:i', time());
            //                 if($start_time!='00:00:00' && $end_time!='00:00:00')
            //                 {
            //         if ($currentTime > $start_time && $currentTime < $end_time) {
            //             $delivery = 1;
            //         }
            //         elseif($currentTime < $start_time && $currentTime < $end_time){
            //             $delivery = 1;
            //         }
            //         else{
            //             $delivery = 0;
            //         }
            //                 }
            //                 if($delivery == 1)
            //                 {
            //                     $delivery_time = $db->lang->line("same_day");
            //                 }else{
            //                     $delivery_time = $db->lang->line("next_day");
            //                 }	
            if ($details->isDelivery == 1) {


                // $html.= '<li style="display:none;">'.$db->lang->line("delivery").' : '. $delivery_time.'</li>';
            }
            $html .= '<li>' . $details->delivery_time . '</li>';
            // 	<li>'.$db->lang->line("delivery_fee").': '.number_format($details->service_charge,2).'</li>
            $html.= '
				
    				<li>' . $db->lang->line("minimum_order") . ': ' . $details->min_amount . '</li>
    				</ul>
						</div>
    									</a>
    								</div>
    							</div>
    						</div>';
        }
    } else {
        $html .= '<div class="wd100 __gdwp">
					    <div class="media">
					        <div class="media-body __gdtx">
					            <h3 class="empty_result">Oh We are Sorry ! No ' . $store_name . ' found</h3> 
					        </div>
						</div>
					</div>';
    }
    $get_cuisine = GetCuisineCountRestaurant($loc_data, $latitude, $longitude, $rest_name, $table_booking, $store_type);

    $result .= '<h5>Cuisines</h5>
									<ul class="scrollbar">
										';

    foreach ($get_cuisine as $cuisine) {
        if ($db->session->userdata('language') == 'ar') {
            if ($cuisine->cuisine_name_ar != '') {
                $cuis_name = $cuisine->cuisine_name_ar;
            } else {
                $cuis_name = $cuisine->cuisine_name;
            }
        } else {
            $cuis_name = $cuisine->cuisine_name;
        }
        if (!empty($cuisine->count)) {
            $count = $cuisine->count;
        } else {
            $count = '0';
        }

        $result .= '<li>
				        <div class="form-check">
						<label class="form-check-label">
							<input type="checkbox" class="form-check-input" name="cuisinetype" id="cuisinetype" value="' . $cuisine->cuisine_id . '">' . $cuis_name . ' <span class="__ct">' . $count . '</span></label>
						</div>
					    </li>';
    }

    $result .= '</ul> <a href="#" class="show_all" data-toggle="modal" data-target="#exampleModal">' . $db->lang->line("show_all") . '</a>
		<hr />';

    $result1 .= '<div class="wd100 __sort_by __radio_sly">
                          <div class="row">';

    foreach ($get_cuisine as $cuisine) {
        if ($db->session->userdata('language') == 'ar') {
            if ($cuisine->cuisine_name_ar != '') {
                $cuis_name = $cuisine->cuisine_name_ar;
            } else {
                $cuis_name = $cuisine->cuisine_name;
            }
        } else {
            $cuis_name = $cuisine->cuisine_name;
        }
        if (!empty($cuisine->count)) {
            $count = $cuisine->count;
        } else {
            $count = '0';
        }

        $result1 .= '<div class="col-sm-5 col-md-4">
    										<div class="form-check">
    											<label class="form-check-label">
    													<input type="checkbox" class="form-check-input" name="cuisinetype" id="cuisinetype" value="' . $cuisine->cuisine_id . '">' . $cuis_name . ' <span class="__ct">(' . $count . ')</span></label>
    										</div>
    									</div>';
    }
    $result1 .= '</div>
                          </div>';

// 		echo $html;
    $data = array('data1' => $html, 'data2' => $result, 'data3' => $result1);
    echo json_encode($data);
}

function get_restaurant_search() {
    $db = LoadDB();
    $rest_name = trim($_POST['rest']);
    $location = trim($_POST['location']);
    $table_booking = trim($_POST['table_booking']);
    // $cusine_type = !empty($_POST['cusine_type'])?$_POST['cusine_type']:0;
    // $cusine_data = $_POST['cuisine_type'];
    $cusine_data = $_POST['cuisine_type'];
    $cuisine = explode(',', $cusine_data);
    $cusine_type = implode("','", $cuisine);
    $latitude = floor($_POST['latitude'] * 100) / 100;
    $longitude = floor($_POST['longitude'] * 100) / 100;
    $loc_data = $_POST['locality'];

    // $Qry1 = "SELECT restaurant_details.vendor_id,restaurant_details.delivery_location
    //     FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
    //      WHERE user_type=2 AND delivery_location !='' and (delivery_location LIKE CONCAT('%', '".$latitude."' ,'%'))
    //     GROUP BY users.id DESC ORDER BY FIELD(restaurant_details.isPremium ,0, 1) DESC";
    // $Array1 = $db->Database->select_qry_array($Qry1);
    // $ids = [];
    // for($i=0;$i<count($Array1);$i++){
    //     $d = $Array1[$i];
    //     array_push($ids,$d->vendor_id);
    // }
    // $arr = implode("','",$ids); 
    $store_type = $_POST['store_type'];
    $store_name = GetNameById($store_type, 'store_type', 'store_type');
    // $Qry = "SELECT city_id FROM  cities WHERE cities.city_name LIKE CONCAT('%', '".trim($loc_data)."' ,'%')";
    // $Array = $db->Database->select_qry_array($Qry);
    // $city_id = $Array[0]->city_id;
    $Qry1 = "SELECT restaurant_details.vendor_id,delivery_timings.cities
        FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        join delivery_timings on delivery_timings.vendor_id = restaurant_details.vendor_id 
        left join cities on cities.city_id = delivery_timings.cities
        WHERE user_type=2 AND store_type='$store_type' AND latitude LIKE CONCAT('%', '" . $latitude . "' ,'%')";
    $Array1 = $db->Database->select_qry_array($Qry1);

    // $Qry_1 = "SELECT restaurant_details.vendor_id,".
    //         "GeoDistDiff('km', '{$latitude}', '{$longitude}', users.latitude, users.longitude) as distance".
    //         " FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
    //         WHERE user_type=2 AND store_type='$store_type' AND isDelivery=0 HAVING distance <= 50";
    // $Array_1 = $db->Database->select_qry_array($Qry_1);
    // $Array_new = array_merge($Array1,$Array_1);


    $ids = [];
    for ($i = 0; $i < count($Array1); $i++) {
        $d = $Array1[$i];
        array_push($ids, $d->vendor_id);
    }

    $arr = implode("','", $ids);
    if ($cusine_type != '') {

        $Qry = "SELECT users.id,users.image,users.position,restaurant_details.*,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
            left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
            left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
            WHERE restaurant_details.vendor_id IN('" . $arr . "') AND restaurant_name LIKE '%" . $rest_name . "%' AND rct.cuisine_id IN('" . $cusine_type . "') 
            GROUP BY users.id,restaurant_details.id
            ORDER BY users.position = 0, users.position";
        // $Qry = "SELECT users.id,users.image,restaurant_details.*,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        //     left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
        //     left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
        //     WHERE restaurant_details.vendor_id IN('".$arr."') AND restaurant_name LIKE '%" . $rest_name . "%' AND rct.cuisine_id ='" . $cusine_type . "' 
        //     GROUP BY users.id,restaurant_details.id
        //     ORDER BY FIELD(restaurant_details.isPremium ,0, 1) DESC";
        // $Qry = "SELECT users.id,users.image,restaurant_details.restaurant_name,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount,restaurant_details.isDelivery,restaurant_details.delivery_time_start,restaurant_details.delivery_time_ends FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        //     left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
        //     left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
        //     WHERE (city LIKE CONCAT('%', '".$location."' ,'%') or area LIKE CONCAT('%', '".$location."' ,'%') or 
        //     street LIKE CONCAT('%', '".$location."' ,'%')) AND restaurant_name LIKE '%" . $rest_name . "%' AND rct.cuisine_id ='" . $cusine_type . "' GROUP BY users.id ORDER BY FIELD(restaurant_details.isPremium ,0, 1) DESC";
    } else {
        $Qry = "SELECT users.id,users.image,users.position,restaurant_details.*,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
            left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
            left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
            WHERE restaurant_details.vendor_id IN('" . $arr . "') AND restaurant_name LIKE '%" . $rest_name . "%'  
            GROUP BY users.id,restaurant_details.id
            ORDER BY users.position = 0, users.position";
        // $Qry = "SELECT users.id,users.image,restaurant_details.restaurant_name,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount,restaurant_details.isDelivery,restaurant_details.delivery_time_start,restaurant_details.delivery_time_ends FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        //     left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
        //     left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
        //     WHERE (city LIKE CONCAT('%', '".$location."' ,'%') or area LIKE CONCAT('%', '".$location."' ,'%') or 
        //     street LIKE CONCAT('%', '".$location."' ,'%')) AND restaurant_name LIKE '%" . $rest_name . "%'  GROUP BY users.id ORDER BY FIELD(restaurant_details.isPremium ,0, 1) DESC";
    }

    $Array = $db->Database->select_qry_array($Qry);

    $html = '';
    $result = '';
    $result1 = '';
    if (count($Array) > 0) {
        foreach ($Array as $details) {
            $Qry1 = "SELECT  delivery_timings.*, GROUP_CONCAT(city_name) AS city_name FROM  delivery_timings 
                                        INNER JOIN cities ON FIND_IN_SET(city_id, cities) > 0 WHERE delivery_timings.vendor_id = $details->id AND cities.city_name LIKE CONCAT('%', '" . trim($_POST['locality']) . "' ,'%') GROUP BY cities,delivery_timings.id";
            // print_r($Qry1);
            $Array = $db->Database->select_qry_array($Qry1);
            foreach ($Array as $dat) {
                $start_time = date('H:i', strtotime($dat->delivery_from));
                $end_time = date('H:i', strtotime($dat->delivery_to));
                $currentTime = date('H:i', time());
                //print_r($currentTime);
                if ($start_time != '00:00:00' && $end_time != '00:00:00') {
                    // if (($end_time < $start_time && ($currentTime <= $start_time && $currentTime >= $end_time)) || 
                    // ($end_time > $start_time && ($currentTime <= $start_time || $currentTime >= $end_time)) ){
                    //     $delivery = 0;
                    // }else{
                    //     $delivery = 1;
                    // }
                    if ($currentTime > $start_time && $currentTime < $end_time) {
                        $delivery = 1;
                    } elseif ($currentTime < $start_time && $currentTime < $end_time) {
                        $delivery = 1;
                    } else {
                        $delivery = 0;
                    }
                }
                // print_r($delivery);
                if ($delivery == 1) {
                    $delivery_time = $db->lang->line("same_day");
                } else {
                    $delivery_time = $db->lang->line("next_day");
                }
            }

            $html .= '<div class="wd100 __gdwp">
    							<div class="media">';

            if (!empty($details->image)) {
                $image = base_url() . 'uploads/vendor_images/' . $details->image;
            } else {
                $image = base_url() . 'images/default_rest.png';
            }
            $qry = "SELECT count(id) as total_reviews,AVG(rating) as rating FROM user_feedback WHERE vendor_id=" . $details->vendor_id;
            $Array1 = $db->Database->select_qry_array($qry);
            $rating = !empty($Array1[0]->rating) ? number_format($Array1[0]->rating, 1) : 0;

            $ConditionArray = array('vendor_id' => $details->vendor_id);
            $Qry2 = "SELECT IFNULL(GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ','), '') as cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar FROM `restaurant_cuisine_type` LEFT JOIN `cuisine` ON cuisine.cuisine_id=restaurant_cuisine_type.cuisine_id  WHERE vendor_id=:vendor_id";
            $Array2 = $db->Database->select_qry_array($Qry2, $ConditionArray);

            if ($db->session->userdata('language') == 'ar') {
                if ($Array2[0]->cuisine_name_ar != '') {
                    $cuisin = $Array2[0]->cuisine_name_ar;
                } else {
                    $cuisin = $Array2[0]->cuisine_name;
                }
            } else {
                $cuisin = $Array2[0]->cuisine_name;
            }

            if ($db->session->userdata('language') == 'ar') {
                if ($details->restaurant_name_ar != '') {
                    $restaur_name = $details->restaurant_name_ar;
                } else {
                    $restaur_name = $details->restaurant_name;
                }
            } else {
                $restaur_name = $details->restaurant_name;
            }

            $html .= '<a href="#">';
            $opening_time = date('H:i A', strtotime($details->opening_time));
            $closing_time = date('H:i A', strtotime($details->closing_time));
            $currentTime = date('H:i', time());

            if (($closing_time < $opening_time && ($currentTime <= $opening_time && $currentTime >= $closing_time)) ||
                    ($closing_time > $opening_time && ($currentTime <= $opening_time || $currentTime >= $closing_time))) {
                $html .= '<div class="__closed">
        				Closed	
        			  </div>';
            }

            if ($currentTime > $opening_time && $details->busy_status == 1) {
                $html .= '<div class="__busy">
        					Busy	
        				</div>';
            }
            $html .= '<img class="mr-4 ml-4 __gdimg" src="' . $image . '"></a>
    				 <div class="media-body __gdtx">
    				 <a href="' . base_url('search_details/' . base64_encode($details->vendor_id)) . '">
    				 <h3>' . $restaur_name . '</h3>
    				<div class="wd100 __gd_subtag">' . $cuisin . '</div>
    				<div class="wd100 __star_reviews">
        			<div class="__star"> <i class="fa fa-star"></i>' . $rating . '</div>
    				<div class="_reviews">(' . $Array1[0]->total_reviews . ' reviews)</div>
        			</div>
					<div class="wd100 __subinfo">
					<ul>';
            // 	$start_time = date('H:i', strtotime($details->delivery_time_start));
            // 	$end_time = date('H:i', strtotime($details->delivery_time_ends));
            // 	$currentTime = date('H:i', time());
            //                 if($start_time!='00:00:00' && $end_time!='00:00:00')
            //                 {
            //         if ($currentTime > $start_time && $currentTime < $end_time) {
            //             $delivery = 1;
            //         }
            //         elseif($currentTime < $start_time && $currentTime < $end_time){
            //             $delivery = 1;
            //         }
            //         else{
            //             $delivery = 0;
            //         }
            //                 }
            //                 if($delivery == 1)
            //                 {
            //                     $delivery_time = $db->lang->line("same_day");
            //                 }else{
            //                     $delivery_time = $db->lang->line("next_day");
            //                 }	
            if ($details->isDelivery == 1) {


                // $html.= '<li style="display:none;">'.$db->lang->line("delivery").' : '. $delivery_time.'</li>';
            }
            $html .= '<li>' . $details->delivery_time . '</li>';
            // 	<li>'.$db->lang->line("delivery_fee").': '.number_format($details->service_charge,2).'</li>
            $html.= '
				
    				<li>' . $db->lang->line("minimum_order") . ': ' . $details->min_amount . '</li>
    				</ul>
						</div>
    									</a>
    								</div>
    							</div>
    						</div>';
        }
    } else {
        $html .= '<div class="wd100 __gdwp">
					    <div class="media">
					        <div class="media-body __gdtx">
					            <h3 class="empty_result">Oh We are Sorry ! No ' . $store_name . ' found</h3> 
					        </div>
						</div>
					</div>';
    }
    $get_cuisine = GetCuisineCountRestaurant($loc_data, $latitude, $longitude, $rest_name, $table_booking, $store_type);

    $result .= '<h5>Cuisines</h5>
									<ul class="scrollbar">
										<li style=display:none;>
											<div class="form-check">
												<label class="form-check-label">
													<input type="radio" class="form-check-input"  value="0" name="cuisinetype" id="cuisinetype" checked>' . $db->lang->line("all") . '<span class="__ct">' . count($Array) . '</span></label>
											</div>
										</li>';

    foreach ($get_cuisine as $cuisine) {
        if ($db->session->userdata('language') == 'ar') {
            if ($cuisine->cuisine_name_ar != '') {
                $cuis_name = $cuisine->cuisine_name_ar;
            } else {
                $cuis_name = $cuisine->cuisine_name;
            }
        } else {
            $cuis_name = $cuisine->cuisine_name;
        }
        if (!empty($cuisine->count)) {
            $count = $cuisine->count;
        } else {
            $count = '0';
        }

        $result .= '<li>
				        <div class="form-check">
						<label class="form-check-label">
							<input type="checkbox" class="form-check-input" name="cuisinetype" id="cuisinetype" value="' . $cuisine->cuisine_id . '">' . $cuis_name . ' <span class="__ct">' . $count . '</span></label>
						</div>
					    </li>';
    }

    $result .= '</ul> <a href="#" class="show_all" data-toggle="modal" data-target="#exampleModal">' . $db->lang->line("show_all") . '</a>
		<hr />';

    $result1 .= '<div class="wd100 __sort_by __radio_sly">
                          <div class="row">';

    foreach ($get_cuisine as $cuisine) {
        if ($db->session->userdata('language') == 'ar') {
            if ($cuisine->cuisine_name_ar != '') {
                $cuis_name = $cuisine->cuisine_name_ar;
            } else {
                $cuis_name = $cuisine->cuisine_name;
            }
        } else {
            $cuis_name = $cuisine->cuisine_name;
        }
        if (!empty($cuisine->count)) {
            $count = $cuisine->count;
        } else {
            $count = '0';
        }

        $result1 .= '<div class="col-sm-5 col-md-4">
    										<div class="form-check">
    											<label class="form-check-label">
    													<input type="checkbox" class="form-check-input" name="cuisinetype" id="cuisinetype" value="' . $cuisine->cuisine_id . '">' . $cuis_name . ' <span class="__ct">(' . $count . ')</span></label>
    										</div>
    									</div>';
    }
    $result1 .= '</div>
                          </div>';

// 		echo $html;
    $data = array('data1' => $html, 'data2' => $result, 'data3' => $result1);
    echo json_encode($data);
}

function restaurant_search() {
    $db = LoadDB();
    $rest_name = trim($_POST['rest']);
    $parttyorder = !empty($_REQUEST['parttyorder']) ? $_REQUEST['parttyorder'] : '0';
    $dat = '';
    if (!empty($_POST['store_type'])) {
        $dat = $dat . " AND users.store_type=" . $_POST['store_type'];
    } else {
        $dat = $dat . '';
    }
    if (!empty($parttyorder)) {
        $dat = $dat . " AND restaurant_details.party_order=1";
    }
    $urducon = " OR BINARY(CONVERT(restaurant_details.restaurant_name_ar USING latin1))  LIKE '%" . $rest_name . "%' OR BINARY(CONVERT(menu_list.menu_name_ar USING latin1))  LIKE '%" . $rest_name . "%'";
    $selectTime = ",restaurant_details.opening_time,restaurant_details.closing_time,restaurant_details.delivery_hours_st,restaurant_details.delivery_hours_et";
    $Qry = "SELECT users.id,users.image,users.position $selectTime ,restaurant_details.restaurant_name,restaurant_details.restaurant_name_ar,restaurant_details.vendor_id,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount 
            FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
            left join menu_list on menu_list.vendor_id=users.id 
            LEFT JOIN party_order_menu POM ON POM.vendor_id=users.id 
            WHERE (restaurant_details.restaurant_name LIKE '%" . $rest_name . "%' OR menu_list.menu_name LIKE '%" . $rest_name . "%' OR POM.menu_name LIKE '%" . $rest_name . "%' $urducon) AND users.status=1 AND users.archive=0 AND users.is_approved=1 $dat GROUP BY users.id ORDER BY users.position = 0, users.position";
    $Array = $db->Database->select_qry_array($Qry);

    if (count($Array) > 0) {
        foreach ($Array as $details) {
            $cuisine_data = "SELECT GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar FROM `cuisine` LEFT JOIN restaurant_cuisine_type ON  restaurant_cuisine_type.cuisine_id=cuisine.cuisine_id WHERE vendor_id='$details->vendor_id'";
            $cuisine_result = $db->Database->select_qry_array($cuisine_data);

            if (!empty($details->image)) {
                $image = base_url() . 'uploads/vendor_images/' . $details->image;
            } else {
                $image = base_url() . 'images/default_rest.png';
            }

            if ($db->session->userdata('language') == 'ar') {
                if ($details->restaurant_name_ar != '') {
                    $rest_name = $details->restaurant_name_ar;
                } else {
                    $rest_name = $details->restaurant_name;
                }
            } else {
                $rest_name = $details->restaurant_name;
            }

            $html .= '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center pl-2 pr-2">
					<a href="' . base_url('search_details/' . base64_encode($details->vendor_id)) . '"><div class="wd100 __wtbx __msims">
					<div class="__msims_img">
						<img class="img-fluid" src="' . $image . '" />
					</div>
						<h4>' . $rest_name . '</h4>
						<h6>' . $cuisine_result[0]->cuisine_name . '</h6><h6>';

            if ($details->opening_time != '00:00:00') {
                $html .= 'Opening Time:' . date('h:i A', strtotime($details->opening_time)) . '  -' . date('h:i A', strtotime($details->closing_time));
            }if ($details->delivery_hours_et != '00:00:00') {
                $html .= '<br>Delivery Time:' . date('h:i A', strtotime($details->delivery_hours_st)) . ' - ' . date('h:i A', strtotime($details->delivery_hours_et));
            }

            $html .= '</h6></div>
					</a></div>
				';
        }
    } else {
        $html .= '<h3>' . $db->lang->line("no_results_found") . '</h3> ';
    }

    echo $html;
}

function cuisineType() {
    $db = LoadDB();
    $cusine_data = $_POST['cuisine_type'];
    $cuisine = explode(',', $cusine_data);
    $cusine_type = implode("','", $cuisine);
    $location = $_POST['location'];
    $rest_name = $_POST['rest'];
    $table_booking = trim($_POST['table_booking']);
    $latitude = floor($_POST['latitude'] * 100) / 100;
    $longitude = floor($_POST['longitude'] * 100) / 100;
    $dat = $_POST['locality'];
    $store_type = $_POST['store_type'];
    $store_name = GetNameById($store_type, 'store_type', 'store_type');
    // $Qry1 = "SELECT restaurant_details.vendor_id,restaurant_details.delivery_location
    //     FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
    //      WHERE user_type=2 AND delivery_location !='' and (delivery_location LIKE CONCAT('%', '".$latitude."' ,'%'))
    //     GROUP BY users.id DESC ORDER BY FIELD(restaurant_details.isPremium ,0, 1) DESC";
    // $Array1 = $db->Database->select_qry_array($Qry1);
    // $ids = [];
    // for($i=0;$i<count($Array1);$i++){
    //     $d = $Array1[$i];
    //     array_push($ids,$d->vendor_id);
    // }
    // $arr = implode("','",$ids); 
    // $Qry = "SELECT city_id FROM  cities WHERE cities.city_name LIKE CONCAT('%', '".trim($dat)."' ,'%')";
    //     $Array = $db->Database->select_qry_array($Qry);
    //     $city_id = $Array[0]->city_id;
    $Qry1 = "SELECT restaurant_details.vendor_id,delivery_timings.cities
        FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        join delivery_timings on delivery_timings.vendor_id = restaurant_details.vendor_id 
        left join cities on cities.city_id = delivery_timings.cities
        WHERE user_type=2 AND store_type='$store_type' AND latitude LIKE CONCAT('%', '" . $latitude . "' ,'%')";
    $Array1 = $db->Database->select_qry_array($Qry1);

    // $Qry_1 = "SELECT restaurant_details.vendor_id,".
    //         "GeoDistDiff('km', '{$latitude}', '{$longitude}', users.latitude, users.longitude) as distance".
    //         " FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
    //         WHERE user_type=2 AND store_type='$store_type' AND isDelivery=0 HAVING distance <= 50";
    // $Array_1 = $db->Database->select_qry_array($Qry_1);
    // $Array_new = array_merge($Array1,$Array_1);


    $ids = [];
    for ($i = 0; $i < count($Array1); $i++) {
        $d = $Array1[$i];
        array_push($ids, $d->vendor_id);
    }

    $arr = implode("','", $ids);
    $cond = '';
    if ($table_booking != '') {
        $cond = "AND  restaurant_details.table_booking='$table_booking'";
    }
    if ($cusine_type != 0) {

        // $Qry = "SELECT users.id,users.image,restaurant_details.*,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar
        //     FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        //     left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
        //     left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
        //     WHERE restaurant_details.vendor_id IN('".$arr."') AND rct.cuisine_id ='" . $cusine_type . "' AND restaurant_name LIKE '%" . $rest_name . "%'  GROUP BY users.id,restaurant_details.id";
        $Qry = "SELECT users.id,users.image,users.position,restaurant_details.*,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar
            FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
            left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
            left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
            WHERE restaurant_details.vendor_id IN('" . $arr . "') $cond AND rct.cuisine_id IN('" . $cusine_type . "') AND restaurant_name LIKE '%" . $rest_name . "%'  GROUP BY users.id,restaurant_details.id ORDER BY users.position = 0, users.position";
    } else {
        $Qry = "SELECT users.id,users.image,users.position,restaurant_details.*,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar
            FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
            left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
            left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
            WHERE restaurant_details.vendor_id IN('" . $arr . "') $cond AND restaurant_name LIKE '%" . $rest_name . "%' GROUP BY users.id,restaurant_details.id ORDER BY users.position = 0, users.position";
    }

    $Array = $db->Database->select_qry_array($Qry);

    $html = '';
    if (count($Array) > 0) {
        foreach ($Array as $cusine_data) {

            $Qry1 = "SELECT  delivery_timings.*, GROUP_CONCAT(city_name) AS city_name FROM  delivery_timings 
                                        INNER JOIN cities ON FIND_IN_SET(city_id, cities) > 0 WHERE delivery_timings.vendor_id = $cusine_data->id AND cities.city_name LIKE CONCAT('%', '" . trim($_POST['locality']) . "' ,'%') GROUP BY cities,delivery_timings.id";
            // print_r($Qry1);
            $Array = $db->Database->select_qry_array($Qry1);
            foreach ($Array as $dat) {
                $start_time = date('H:i', strtotime($dat->delivery_from));
                $end_time = date('H:i', strtotime($dat->delivery_to));
                $currentTime = date('H:i', time());
                //print_r($currentTime);
                if ($start_time != '00:00:00' && $end_time != '00:00:00') {
                    // if (($end_time < $start_time && ($currentTime <= $start_time && $currentTime >= $end_time)) || 
                    // ($end_time > $start_time && ($currentTime <= $start_time || $currentTime >= $end_time)) ){
                    //     $delivery = 0;
                    // }else{
                    //     $delivery = 1;
                    // }
                    if ($currentTime > $start_time && $currentTime < $end_time) {
                        $delivery = 1;
                    } elseif ($currentTime < $start_time && $currentTime < $end_time) {
                        $delivery = 1;
                    } else {
                        $delivery = 0;
                    }
                }
                // print_r($delivery);
                if ($delivery == 1) {
                    $delivery_time = $db->lang->line("same_day");
                } else {
                    $delivery_time = $db->lang->line("next_day");
                }
            }
            $html .= '<div class="wd100 __gdwp">
        			<div class="media">';
            if (!empty($cusine_data->image)) {
                $image = base_url() . 'uploads/vendor_images/' . $cusine_data->image;
            } else {
                $image = base_url() . 'images/default_rest.png';
            }
            if ($db->session->userdata('language') == 'ar') {
                if ($cusine_data->restaurant_name_ar != '') {
                    $rest_name = $cusine_data->restaurant_name_ar;
                } else {
                    $rest_name = $cusine_data->restaurant_name;
                }
            } else {
                $rest_name = $cusine_data->restaurant_name;
            }

            $ConditionArray = array('vendor_id' => $cusine_data->vendor_id);
            $Qry2 = "SELECT IFNULL(GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ','), '') as cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar FROM `restaurant_cuisine_type` LEFT JOIN `cuisine` ON cuisine.cuisine_id=restaurant_cuisine_type.cuisine_id  WHERE vendor_id=:vendor_id";
            $Array2 = $db->Database->select_qry_array($Qry2, $ConditionArray);
            $qry = "SELECT count(id) as total_reviews,AVG(rating) as rating FROM user_feedback WHERE vendor_id=" . $cusine_data->vendor_id;
            $Array1 = $db->Database->select_qry_array($qry);
            $rating = !empty($Array1[0]->rating) ? number_format($Array1[0]->rating, 1) : 0;

            $html .= '<a href="#">';
            $opening_time = date('H:i A', strtotime($cusine_data->opening_time));
            $closing_time = date('H:i A', strtotime($cusine_data->closing_time));
            $currentTime = date('H:i', time());

            if (($closing_time < $opening_time && ($currentTime <= $opening_time && $currentTime >= $closing_time)) ||
                    ($closing_time > $opening_time && ($currentTime <= $opening_time || $currentTime >= $closing_time))) {
                $html .= '<div class="__closed">
        				Closed	
        			  </div>';
            }

            if ($currentTime > $opening_time && $cusine_data->busy_status == 1) {
                $html .= '<div class="__busy">
        					Busy	
        				</div>';
            }
            $html .='<img class="mr-4 ml-4 __gdimg" src="' . $image . '"></a>
        			<div class="media-body __gdtx">
        			<a href="' . base_url("search_details/" . base64_encode($cusine_data->vendor_id)) . '">
        			<h3>' . $rest_name . '</h3>
        			<div class="wd100 __gd_subtag">' . $Array2[0]->cuisine_name . '</div>
        			<div class="wd100 __star_reviews">
        			<div class="__star"> <i class="fa fa-star"></i>' . $rating . '</div>
    				<div class="_reviews"> (' . $Array1[0]->total_reviews . ' reviews) </div>
        			</div>
    				<div class="wd100 __subinfo">
    					<ul>';
            // 					$start_time = date('H:i', strtotime($cusine_data->delivery_time_start));
            // 	$end_time = date('H:i', strtotime($cusine_data->delivery_time_ends));
            // 	$currentTime = date('H:i', time());
            //                 if($start_time!='00:00:00' && $end_time!='00:00:00')
            //                 {
            //         if ($currentTime > $start_time && $currentTime < $end_time) {
            //             $delivery = 1;
            //         }
            //         elseif($currentTime < $start_time && $currentTime < $end_time){
            //             $delivery = 1;
            //         }
            //         else{
            //             $delivery = 0;
            //         }
            //                 }
            //                 if($delivery == 1)
            //                 {
            //                     $delivery_time = $db->lang->line("same_day");
            //                 }else{
            //                     $delivery_time = $db->lang->line("next_day");
            //                 }	
            if ($cusine_data->isDelivery == 1) {


                // $html.= '<li style="display:none;">'.$db->lang->line("delivery").' : '. $delivery_time.'</li>';
            }
            $html .= '<li>' . $cusine_data->delivery_time . '</li>';
            // 	$html.= '<li>'.$db->lang->line("delivery_fee").': '.number_format($cusine_data->service_charge,2).'</li>
            $html .='<li>' . $db->lang->line("minimum_order") . ': ' . $cusine_data->min_amount . '</li>
    					</ul>
    				</div>
    				</a>
    				</div>
    				</div>
    			</div>';
        }
    } else {
        $html .= '<div class="wd100 __gdwp">
						    <div class="media">
						        <div class="media-body __gdtx">
						            <h3 class="empty_result">Oh We are Sorry ! No ' . $store_name . ' found</h3> 
						        </div>
							</div>
						</div>';
    }
    echo $html;
}

function GetHomelyFoodById($rest_id = '') {
    $db = LoadDB();
    $Qry = "SELECT users.id,users.image,restaurant_details.restaurant_name,restaurant_details.restaurant_name_ar,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount,restaurant_details.about,users.area
                FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
                left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
                left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
                WHERE users.id='" . $rest_id . "'
                GROUP BY users.id";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function AddToCart() {
    error_reporting(0);
    $db = LoadDB();
    $choice = $_POST['choice'];
    $party = $_POST['party'];
    $rand_no = rand(10, 100);
    $menu_id = trim($_POST['menu_id']);
    if ($party == 0) {
        if ($choice == 0) {
            $menu_id = $menu_id;
            $category = $_POST['category'];
            $session_cart = $db->session->userdata('CartData');
            $vendorId444 = !empty($_REQUEST['vendor_id']) ? $_REQUEST['vendor_id'] : '';

            $exists = false;
            $array['quantity'] = 0;
            if (isset($_POST)) {
                $array['choice'] = $_POST['choice'];
                $array['party'] = $_POST['party'];
                $array['menu_id'] = $menu_id;
                $array['category'] = $_POST['category'];
                // $CondutaionArray = array('id' => $array['menu_id'],'category_id'=>$array['category']);
                $CondutaionArray = array('id' => $array['menu_id']);
                // $Qry = "SELECT * FROM `menu_list` LEFT JOIN offers ON offers.offer_id=menu_list.deal_id LEFT JOIN promo_code ON promo_code.promocode_id=offers.offer_promocode_id WHERE id=:id AND category_id=:category_id";
                // $Qry = "SELECT * FROM  menu_list WHERE id=:id AND category_id=:category_id";
                $Qry = "SELECT * FROM `menu_list` LEFT JOIN promo_code ON (promo_code.offer_addedby=menu_list.vendor_id AND promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE() AND promo_code.status=0 AND promo_code.archive=0) WHERE id=:id";
                $CategoryArray = $db->Database->select_qry_array($Qry, $CondutaionArray);


                $prc = getMenudiscountArray($menuId = $array['menu_id'], $CategoryArray[0]->price, $vendorId444);
                $final_price = $prc['final_price'];




                $array['menu_name'] = $CategoryArray[0]->menu_name;
                // $array['price'] = $CategoryArray[0]->price;
                $array['price'] = $final_price;
                $array['quantity'] += 1;
                $array['options_id'] = 0;
                $array['subtotal'] = $array['quantity'] * $array['price'];
                $array['vendor_id'] = $_POST['vendor_id'];
                $array['location'] = trim($_POST['location']);
                $array['latitude'] = $_POST['latitude'];
                $array['longitude'] = $_POST['longitude'];
                $array['cart_id'] = $rand_no;
                $cond = array('vendor_id' => $_POST['vendor_id']);
                $Qry = "SELECT * FROM  restaurant_details WHERE vendor_id=:vendor_id";
                $RestArray = $db->Database->select_qry_array($Qry, $cond);
                $arrays['vendor_id'] = $RestArray[0]->vendor_id;
                $arrays['restaurant_name'] = $RestArray[0]->restaurant_name;

                $db->session->set_userdata('Restaurant', $arrays);

                if (!empty($session_cart)) {

                    for ($i = 0; $i < count($session_cart); $i++) {
                        if ($array['menu_id'] == $session_cart[$i]['menu_id']) {
                            $exists = true;
                            $session_cart[$i]['quantity'] = $session_cart[$i]['quantity'] + 1;
                            $session_cart[$i]['subtotal'] = $session_cart[$i]['price'] * $session_cart[$i]['quantity'];
                        }
                    }

                    if ($exists == false) {
                        array_push($session_cart, $array);
                    }


                    $cart = $session_cart;
                } else {
                    $cart[0] = $array;
                }

                $cart = array_values(array_filter($cart));

                $db->session->set_userdata('CartData', $cart);
                $count = 0;
                $total_vat = 0;
                $html = '<div class="__cartsummytabv wd100">
                					<table class="table __crtb">';
                $subtotal = 0;
                foreach ($cart as $c) {

                    $html .= '<tr>
                								<td class="__itxtt">' . $c["menu_name"] . '
                								</td>
                								<td class="qtytd">
                									<section class="qty-update-section incart b-a">
                									 	<button type="button" class="btn btn-sm b-r">
                											<i class="fa fa-minus orange"></i>
                										</button>
                									 	
                										<span class="f-11"><b>1</b></span>
                										<button type="button" class="btn btn-sm b-l">
                											<i class="fa fa-plus orange"></i>
                										</button>
                								 	</section>
                									
                								  </td>
                								<td class="__crpc">' . $c["price"] . '</td>
                								<td>
                								  <a class="__close remove_cart_product" href="" data-id="' . $c['menu_id'] . '"><i class="fas fa-times-circle"></i></a>
                								  </td>
                							  </tr>';
                    $c['price'] = DecimalAmount($c['price']);
                    $subtotal += $c['price'] * $c['quantity'];
                }
                $db->session->set_userdata('sub_total', $subtotal);
                $html .= '</table>
                						</div>
                						
                						';

                $subtotal = $db->session->userdata('sub_total');
                // <td align="left">'.$db->lang->line("delivery_fee").'</td>
                $html .= '<div class="wd100 __cartsummytabv_btpart">
                							<table class="table">
                							<tr>
                							<td align="left">' . $db->lang->line("sub_total") . '</td>
                							<td align="right" class="__srylst">' . $db->lang->line("aed") . '' . DecimalAmount($subtotal) . '</td>
                						  </tr>
                						  ';
                // 		  <tr>
                // 			<td align="right" class="__srylst">'.$db->lang->line("aed") .''. $RestArray[0]->service_charge.'</td>
                // 		  </tr>
                $sub_tot = DecimalAmount($subtotal);
                // 	$serv_charge = number_format($RestArray[0]->service_charge,2);
                $serv_charge = 0;
                $total_amount = $sub_tot + $serv_charge;
                $html .= '<tr>
                							  <td align="left"><b>' . $db->lang->line("total_amount") . '</b> </td>
                							<td align="right"><b>' . $db->lang->line("aed") . '' . DecimalAmount($total_amount) . '</b></td>
                						  </tr>';
                $html .='</table>
                							
                							
                						 <div class="form-group">
                								<button type="button" class="btn btn-success btn-block">' . $db->lang->line("proceed_to_checkout") . '</button>
                						 </div>
                						
                						</div>';
                echo $html;
            }
        } else {
            $options = !empty($_POST['options_data']) ? $_POST['options_data'] : '';

            $opt_data = json_decode($options, true);
            $menu_id = $menu_id;
            $category = $_POST['category'];
            $session_cart = $db->session->userdata('CartData');
            $exists = false;
            $array['quantity'] = 0;
            if (isset($_POST)) {
                $array['choice'] = $_POST['choice'];
                $array['party'] = $_POST['party'];
                $array['menu_id'] = $menu_id;
                $array['category'] = $category;
                $array['options_id'] = 0;
                $CondutaionArray = array('id' => $array['menu_id']);
                $Qry = "SELECT * FROM  menu_list WHERE id=:id ";
                $CategoryArray = $db->Database->select_qry_array($Qry, $CondutaionArray);
                $array['menu_name'] = $CategoryArray[0]->menu_name;
                $array['price'] = $_POST['price'];
                $array['quantity'] = $_POST['quantity'];
                $array['subtotal'] = $array['quantity'] * $array['price'];
                $array['vendor_id'] = $_POST['vendor_id'];
                $array['location'] = trim($_POST['location']);
                $array['latitude'] = $_POST['latitude'];
                $array['longitude'] = $_POST['longitude'];
                $array['cart_id'] = $rand_no;

                // foreach($opt_data as $option)
                // {
                //     $array[$option['type']][] = $option;
                // }
                $tmp = '';
                if (!empty($opt_data)) {
                    foreach ($opt_data as $option) {
                        $array[$option['type']][] = $option;
                        $tmp .= $option['id'] . ',';
                    }
                }
                $tmp = trim($tmp, ',');

                $cond = array('vendor_id' => $_POST['vendor_id']);
                $Qry = "SELECT * FROM  restaurant_details WHERE vendor_id=:vendor_id";
                $RestArray = $db->Database->select_qry_array($Qry, $cond);
                $arrays['vendor_id'] = $RestArray[0]->vendor_id;
                $arrays['restaurant_name'] = $RestArray[0]->restaurant_name;

                $db->session->set_userdata('Restaurant', $arrays);
                // if (!empty($session_cart)) {
                //         for ($i = 0; $i < count($session_cart); $i++) {
                //             if ($array['menu_id'] == $session_cart[$i]['menu_id'] ) {
                //                 $exists = true;
                //                 $session_cart[$i]['quantity'] = $session_cart[$i]['quantity'] + 1;
                //                 $session_cart[$i]['subtotal'] = $session_cart[$i]['price'] * $session_cart[$i]['quantity'];
                //             }
                //         }
                //         if ($exists == false) {
                //             array_push($session_cart, $array);
                //         }
                //         $cart = $session_cart;
                // } else {
                //     $cart[0] = $array;
                // }
                if (!empty($session_cart)) {
                    $dat = explode(',', $tmp);


                    for ($i = 0; $i < count($session_cart); $i++) {
                        if (isset($session_cart[$i]['size'])) {
                            $array5 = array_column($session_cart[$i]['size'], 'id');
                            $array5 = implode(',', $array5);
                        } else {
                            $array5 = '';
                        }
                        if (isset($session_cart[$i]['addon'])) {
                            $array1 = array_column($session_cart[$i]['addon'], 'id');
                            $array1 = implode(',', $array1);
                        } else {
                            $array1 = '';
                        }
                        if (isset($session_cart[$i]['topping'])) {
                            $array2 = array_column($session_cart[$i]['topping'], 'id');
                            $array2 = implode(',', $array2);
                        } else {
                            $array2 = '';
                        }

                        if (isset($session_cart[$i]['dip'])) {
                            $array4 = array_column($session_cart[$i]['dip'], 'id');
                            $array4 = implode(',', $array4);
                        } else {
                            $array4 = '';
                        }

                        $tmp1[] = implode(",", array_filter([$array5, $array1, $array2, $array4]));

                        if ($tmp == $tmp1[$i]) {
                            if ($array['menu_id'] == $session_cart[$i]['menu_id']) {
                                $exists = true;
                                $session_cart[$i]['quantity'] = $session_cart[$i]['quantity'] + $array['quantity'];
                                $session_cart[$i]['subtotal'] = $session_cart[$i]['price'] * $session_cart[$i]['quantity'];
                            }
                        }
                    }

                    if ($exists == false) {
                        array_push($session_cart, $array);
                    }


                    $cart = $session_cart;
                } else {
                    $cart[0] = $array;
                }
                $cart = array_values(array_filter($cart));

                $db->session->set_userdata('CartData', $cart);
                $count = 0;
                $total_vat = 0;
                $html = '<div class="__cartsummytabv wd100">
                					<table class="table __crtb">';
                $subtotal = 0;
                foreach ($cart as $c) {
                    $html .= '<tr>
                								<td class="__itxtt">' . $c["menu_name"] . '</td>
                								<td class="qtytd">
                									<section class="qty-update-section incart b-a">
                									 	<button type="button" class="btn btn-sm b-r" onclick="updateQty(this)" data-id="' . $c['menu_id'] . '" data-type="minus" data-value="' . $c['quantity'] . '">
                											<i class="fa fa-minus orange"></i>
                										</button>
                									 	
                										<span class="f-11"><b>1</b></span>
                										<button type="button" class="btn btn-sm b-l" onclick="updateQty(this)" data-id="' . $c['menu_id'] . '" data-type="plus" data-value="' . $c['quantity'] . '">
                											<i class="fa fa-plus orange"></i>
                										</button>
                								 	</section>
                									
                								  </td>
                								<td class="__crpc">' . DecimalAmount($c['subtotal']) . '</td>
                								<td>
                								  <a class="__close remove_cart_product" href="" data-id="' . $c['menu_id'] . '"><i class="fas fa-times-circle"></i></a>
                								  </td>
                							  </tr>';
                    $subtotal += $c['price'] * $c['quantity'];
                }
                $db->session->set_userdata('sub_total', $subtotal);
                $html .= '</table>
                						</div>
                						
                						';

                $subtotal = $db->session->userdata('sub_total');
                // <td align="left">'.$db->lang->line("delivery_fee").'</td>
                $html .= '<div class="wd100 __cartsummytabv_btpart">
                							<table class="table">
                							<tr>
                							<td align="left">' . $db->lang->line("sub_total") . '</td>
                							<td align="right" class="__srylst">' . $db->lang->line("aed") . '' . DecimalAmount($subtotal) . '</td>
                						  </tr>
                						  <tr>
                							
                							<td align="right" class="__srylst">' . $db->lang->line("aed") . '' . DecimalAmount($RestArray[0]->service_charge) . '</td>
                						  </tr>
                						  <tr>';
                $sub_tot = DecimalAmount($subtotal);
                $serv_charge = DecimalAmount($RestArray[0]->service_charge);
                $total_amount = $sub_tot + $serv_charge;
                $html .= '<tr>
                							  <td align="left"><b>' . $db->lang->line("total_amount") . '</b> </td>
                							<td align="right"><b>' . $db->lang->line("aed") . '' . DecimalAmount($total_amount) . '</b></td>
                						  </tr>';
                $html .='</table>
                							
                							
                						 <div class="form-group">
                								<button type="button" class="btn btn-success btn-block">' . $db->lang->line("proceed_to_checkout") . '</button>
                						 </div>
                						
                						</div>';
                echo $html;
            }
        }
    } else {
        $menu_id = $menu_id;
        $category = $_POST['category'];
        $session_cart = $db->session->userdata('CartData');
        $exists = false;
        $array['quantity'] = 0;
        if (isset($_POST)) {
            $array['choice'] = $_POST['choice'];
            $array['party'] = $_POST['party'];
            $array['menu_id'] = $menu_id;
            $array['category'] = $_POST['category'];
            $CondutaionArray = array('id' => $array['menu_id']);

            $Qry = "SELECT * FROM `party_order_menu` LEFT JOIN promo_code ON (promo_code.offer_addedby=party_order_menu.vendor_id AND promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE() AND promo_code.status=0 AND promo_code.archive=0) WHERE id=:id";
            $CategoryArray = $db->Database->select_qry_array($Qry, $CondutaionArray);

            if (isset($menu_list->menu_id)) {
                $menu_id = explode(',', $menu_list->menu_id);
                if (in_array($array['menu_id'], $menu_id)) {
                    $discount_price = DecimalAmount($CategoryArray[0]->price) * $CategoryArray[0]->discount / 100;
                    $final_price = DecimalAmount($CategoryArray[0]->price) - $discount_price;
                } else {
                    $final_price = DecimalAmount($CategoryArray[0]->price);
                }
            } else {
                $final_price = DecimalAmount($CategoryArray[0]->price);
            }
            $array['menu_name'] = $CategoryArray[0]->menu_name;
            $array['price'] = $final_price;
            $array['quantity'] += 1;
            $array['options_id'] = 0;
            $array['subtotal'] = $array['quantity'] * $array['price'];
            $array['vendor_id'] = $_POST['vendor_id'];
            $array['location'] = trim($_POST['location']);
            $array['latitude'] = $_POST['latitude'];
            $array['longitude'] = $_POST['longitude'];
            $array['cart_id'] = $rand_no;
            $cond = array('vendor_id' => $_POST['vendor_id']);
            $Qry = "SELECT * FROM  restaurant_details WHERE vendor_id=:vendor_id";
            $RestArray = $db->Database->select_qry_array($Qry, $cond);
            $arrays['vendor_id'] = $RestArray[0]->vendor_id;
            $arrays['restaurant_name'] = $RestArray[0]->restaurant_name;

            $db->session->set_userdata('Restaurant', $arrays);

            if (!empty($session_cart)) {

                for ($i = 0; $i < count($session_cart); $i++) {
                    if ($array['menu_id'] == $session_cart[$i]['menu_id']) {
                        $exists = true;
                        $session_cart[$i]['quantity'] = $session_cart[$i]['quantity'] + 1;
                        $session_cart[$i]['subtotal'] = $session_cart[$i]['price'] * $session_cart[$i]['quantity'];
                    }
                }

                if ($exists == false) {
                    array_push($session_cart, $array);
                }


                $cart = $session_cart;
            } else {
                $cart[0] = $array;
            }

            $cart = array_values(array_filter($cart));

            $db->session->set_userdata('CartData', $cart);
            $count = 0;
            $total_vat = 0;
            $html = '<div class="__cartsummytabv wd100">
                					<table class="table __crtb">';
            $subtotal = 0;
            foreach ($cart as $c) {

                $html .= '<tr>
                								<td class="__itxtt">' . $c["menu_name"] . '
                								</td>
                								<td class="qtytd">
                									<section class="qty-update-section incart b-a">
                									 	<button type="button" class="btn btn-sm b-r">
                											<i class="fa fa-minus orange"></i>
                										</button>
                									 	
                										<span class="f-11"><b>1</b></span>
                										<button type="button" class="btn btn-sm b-l">
                											<i class="fa fa-plus orange"></i>
                										</button>
                								 	</section>
                									
                								  </td>
                								<td class="__crpc">' . $c["price"] . '</td>
                								<td>
                								  <a class="__close remove_cart_product" href="" data-id="' . $c['menu_id'] . '"><i class="fas fa-times-circle"></i></a>
                								  </td>
                							  </tr>';
                $subtotal += $c['price'] * $c['quantity'];
            }
            $db->session->set_userdata('sub_total', $subtotal);
            $html .= '</table>
                						</div>
                						
                						';

            $subtotal = $db->session->userdata('sub_total');
            // <td align="left">'.$db->lang->line("delivery_fee").'</td>
            $html .= '<div class="wd100 __cartsummytabv_btpart">
                							<table class="table">
                							<tr>
                							<td align="left">' . $db->lang->line("sub_total") . '</td>
                							<td align="right" class="__srylst">' . $db->lang->line("aed") . '' . DecimalAmount($subtotal) . '</td>
                						  </tr>
                						  ';
            // 		  <tr>
            // 			<td align="right" class="__srylst">'.$db->lang->line("aed") .''. $RestArray[0]->service_charge.'</td>
            // 		  </tr>
            $sub_tot = DecimalAmount($subtotal);
            // 	$serv_charge = number_format($RestArray[0]->service_charge,2);
            $serv_charge = 0;
            $total_amount = $sub_tot + $serv_charge;
            $html .= '<tr>
                							  <td align="left"><b>' . $db->lang->line("total_amount") . '</b> </td>
                							<td align="right"><b>' . $db->lang->line("aed") . '' . DecimalAmount($total_amount) . '</b></td>
                						  </tr>';
            $html .='</table>
                							
                							
                						 <div class="form-group">
                								<button type="button" class="btn btn-success btn-block">' . $db->lang->line("proceed_to_checkout") . '</button>
                						 </div>
                						
                						</div>';
            echo $html;
        }
    }
}

function RemoveFromCart() {
    $db = LoadDB();
    $id = trim($_POST['id']);
    $cart_id = trim($_POST['cart_id']);
    $session_cart = $db->session->userdata('CartData');
    if (isset($_POST)) {
        for ($i = 0; $i < count($session_cart); $i++) {
            if ($_POST['id'] == $session_cart[$i]['menu_id'] && $_POST['cart_id'] == $session_cart[$i]['cart_id']) {
                unset($session_cart[$i]);
            }
        }
        $session_cart = array_values(array_filter($session_cart));
        $db->session->set_userdata('CartData', $session_cart);
        $count = 0;
        $total_vat = 0;
        // foreach ($session_cart as $item) {
        //     $count += $item['quantity'];
        //     $total_vat += $item['vat_amount'];
        // }
        // $db->session->set_userdata('cart_count', $count);
        // $db->session->set_userdata('total_vat', $total_vat);
        echo 'success';
        exit;
    }
}

function UpdateQuantity() {
    $session_cart = $db->session->userdata('CartData');
    if (isset($_POST)) {
        for ($i = 0; $i < count($session_cart); $i++) {
            if ($_POST['product_id'] == $session_cart[$i]['product_id'] && $_POST['table'] == $session_cart[$i]['table'] && $_POST['bundleid'] == $session_cart[$i]['bundleid']) {
                $session_cart[$i]['quantity'] = $_POST['quantity'];
                if ($_POST['vat'] == 1) {
                    $session_cart[$i]['vat_amount'] = 0.05 * $session_cart[$i]['price'] * $session_cart[$i]['quantity'];
                }
                $session_cart[$i]['subtotal'] = $session_cart[$i]['price'] * $session_cart[$i]['quantity'];
            }
        }
        $session_cart = array_values(array_filter($session_cart));
        $db->session->set_userdata('Cart', $session_cart);
        $count = 0;
        $total_vat = 0;
        foreach ($session_cart as $item) {
            $count += $item['quantity'];
            $total_vat += $item['vat_amount'];
        }
        $db->session->set_userdata('cart_count', $count);
        $db->session->set_userdata('total_vat', $total_vat);
        echo 'success';
        exit;
    }
}

function GetProductCountByProductId($ProductId) {
    $db = LoadDB();
    $Qry = "SELECT stock FROM menu_list WHERE id='$ProductId' AND status=0";

    $Array = $db->Database->select_qry_array($Qry);
    if (!empty($Array)) {
        return $Array[0];
    }
}

function filter_search() {
    $db = LoadDB();
    $location = trim($_POST['location']);
    $filter = $_POST['filter'];
    $cusine_data = $_POST['cuisine_type'];
    $cuisine = explode(',', $cusine_data);
    $cusine_type = implode("','", $cuisine);

    // $cuisine_type = !empty($_POST['cuisine_type'])?$_POST['cuisine_type']:0;
    $rest_name = trim($_POST['rest']);
    $latitude = floor($_POST['latitude'] * 100) / 100;
    $longitude = floor($_POST['longitude'] * 100) / 100;
    $dat = $_POST['locality'];
    $store_type = $_POST['store_type'];
    $store_name = GetNameById($store_type, 'store_type', 'store_type');
    // $Qry = "SELECT city_id FROM  cities WHERE cities.city_name LIKE CONCAT('%', '".trim($dat)."' ,'%')";
    // $Array = $db->Database->select_qry_array($Qry);
    // $city_id = $Array[0]->city_id;
    $Qry1 = "SELECT restaurant_details.vendor_id,delivery_timings.cities
    FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
    join delivery_timings on delivery_timings.vendor_id = restaurant_details.vendor_id 
    left join cities on cities.city_id = delivery_timings.cities
    WHERE user_type=2 AND store_type='$store_type' AND latitude LIKE CONCAT('%', '" . $latitude . "' ,'%')";
    $Array1 = $db->Database->select_qry_array($Qry1);

    // $Qry_1 = "SELECT restaurant_details.vendor_id,".
    //             "GeoDistDiff('km', '{$latitude}', '{$longitude}', users.latitude, users.longitude) as distance".
    //             " FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
    //             WHERE user_type=2 AND store_type='$store_type' AND isDelivery=0 HAVING distance <= 50";
    //     $Array_1 = $db->Database->select_qry_array($Qry_1);
    //     $Array_new = array_merge($Array1,$Array_1);


    $ids = [];
    for ($i = 0; $i < count($Array1); $i++) {
        $d = $Array1[$i];
        array_push($ids, $d->vendor_id);
    }

    $arr = implode("','", $ids);

    // $Qry1 = "SELECT restaurant_details.vendor_id,restaurant_details.delivery_location
    //     FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
    //      WHERE user_type=2 AND delivery_location !='' and (delivery_location LIKE CONCAT('%', '".$latitude."' ,'%'))
    //     GROUP BY users.id DESC ORDER BY FIELD(restaurant_details.isPremium ,0, 1) DESC";
    // $Array1 = $db->Database->select_qry_array($Qry1);
    // $ids = [];
    // for($i=0;$i<count($Array1);$i++){
    //     $d = $Array1[$i];
    //     array_push($ids,$d->vendor_id);
    // }
    // $arr = implode("','",$ids); 
    // $cond1 = '';
    // $cond2 = '';
    // if ($cuisine_type != 0) {
    //     $cond1 .= " and rct.cuisine_id ='" . $cuisine_type . "'";
    // }
    // if ($rest_name != '') {
    //     $cond2 .= " and restaurant_name LIKE '%" . $rest_name . "%'";
    // }
    // if($filter=='new')
    // {
    //     $Qry = "SELECT users.id,users.image,restaurant_details.restaurant_name,GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ', ')AS cuisine_name,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
    //         left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
    //         left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
    //         WHERE (city LIKE CONCAT('%', '".$location."' ,'%') or area LIKE CONCAT('%', '".$location."' ,'%') or 
    //         street LIKE CONCAT('%', '".$location."' ,'%')) $cond1 $cond2 GROUP BY users.id ORDER BY users.id DESC";
    // }elseif($filter=='alpha')
    // {
    //     $Qry = "SELECT users.id,users.image,restaurant_details.restaurant_name,GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ', ')AS cuisine_name,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
    //         left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
    //         left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
    //         WHERE (city LIKE CONCAT('%', '".$location."' ,'%') or area LIKE CONCAT('%', '".$location."' ,'%') or 
    //         street LIKE CONCAT('%', '".$location."' ,'%')) $cond1 $cond2 GROUP BY users.id ORDER BY restaurant_details.restaurant_name";
    // }elseif($filter=='min_order')
    // {
    //     $Qry = "SELECT users.id,users.image,restaurant_details.restaurant_name,GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ', ')AS cuisine_name,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
    //         left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
    //         left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
    //         WHERE (city LIKE CONCAT('%', '".$location."' ,'%') or area LIKE CONCAT('%', '".$location."' ,'%') or 
    //         street LIKE CONCAT('%', '".$location."' ,'%')) $cond1 $cond2 GROUP BY users.id ORDER BY restaurant_details.min_amount";
    // }elseif($filter=='fast')
    // {
    //     $Qry = "SELECT users.id,users.image,restaurant_details.restaurant_name,GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ', ')AS cuisine_name,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
    //         left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
    //         left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
    //         WHERE (city LIKE CONCAT('%', '".$location."' ,'%') or area LIKE CONCAT('%', '".$location."' ,'%') or 
    //         street LIKE CONCAT('%', '".$location."' ,'%')) $cond1 $cond2 GROUP BY users.id ORDER BY restaurant_details.delivery_time";
    // }
    // elseif($filter=='rating')
    // {
    //     $Qry = "SELECT users.id,users.image,restaurant_details.restaurant_name,AVG(user_feedback.rating) as rating,GROUP_CONCAT(DISTINCT cuisine.cuisine_name SEPARATOR ', ')AS cuisine_name,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount FROM users 
    //         left join restaurant_details on users.id=restaurant_details.vendor_id 
    //         left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
    //         left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
    //         left join user_feedback on user_feedback.vendor_id=restaurant_details.vendor_id 
    //         WHERE (city LIKE CONCAT('%', '".$location."' ,'%') or area LIKE CONCAT('%', '".$location."' ,'%') or 
    //         street LIKE CONCAT('%', '".$location."' ,'%')) $cond1 $cond2 GROUP BY users.id ORDER BY rating DESC";
    // }
    $cond = '';
    $cond1 = '';
    $cond2 = '';
    if ($cusine_type != '') {
        // $cond1 = "AND rct.cuisine_id ='$cusine_type'";
        $cond1 = "AND rct.cuisine_id IN('" . $cusine_type . "')";
    }
    if ($rest_name != '') {
        $cond2 .= " and restaurant_name LIKE '%" . $rest_name . "%'";
    }
    if ($filter == 1) {
        $cond = 'ORDER BY FIELD(restaurant_details.isPremium ,0, 1) DESC';
    } elseif ($filter == 2) {
        $cond = 'ORDER BY users.id DESC';
    } elseif ($filter == 3) {
        $cond = 'ORDER BY restaurant_details.restaurant_name';
    } elseif ($filter == 4) {
        $cond = 'ORDER BY restaurant_details.min_amount';
    } elseif ($filter == 5) {
        $cond = 'ORDER BY restaurant_details.delivery_time';
    }
    if ($filter == 6) {
        $Qry = "SELECT users.id,users.image,restaurant_details.*,AVG(user_feedback.rating) as rating,GROUP_CONCAT(DISTINCT cuisine.cuisine_name SEPARATOR ', ')AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar
            FROM users 
            left join restaurant_details on users.id=restaurant_details.vendor_id 
            left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
            left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
            left join user_feedback on user_feedback.vendor_id=restaurant_details.vendor_id 
            WHERE restaurant_details.vendor_id IN('" . $arr . "') AND rct.cuisine_id IN('" . $cusine_type . "')  $cond1  GROUP BY users.id,restaurant_details.id
            ORDER BY rating DESC";


        // $Qry = "SELECT users.id,users.image,restaurant_details.restaurant_name,AVG(user_feedback.rating) as rating,GROUP_CONCAT(DISTINCT cuisine.cuisine_name SEPARATOR ', ')AS cuisine_name,restaurant_details.vendor_id,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount
        // FROM users 
        // left join restaurant_details on users.id=restaurant_details.vendor_id 
        // left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
        // left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
        // left join user_feedback on user_feedback.vendor_id=restaurant_details.vendor_id 
        // WHERE (city LIKE CONCAT('%', '".$location."' ,'%') or area LIKE CONCAT('%', '".$location."' ,'%') or 
        // street LIKE CONCAT('%', '".$location."' ,'%')) $cond1  GROUP BY users.id ORDER BY rating DESC";
    } else {
        $Qry = "SELECT users.id,users.image,restaurant_details.*,GROUP_CONCAT(DISTINCT cuisine.cuisine_name SEPARATOR ', ')AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar
            FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
            left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
            left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
            WHERE restaurant_details.vendor_id IN('" . $arr . "')   $cond1 $cond2 GROUP BY users.id,restaurant_details.id
            $cond ";
        // $Qry = "SELECT users.id,users.image,restaurant_details.restaurant_name,GROUP_CONCAT(DISTINCT cuisine.cuisine_name SEPARATOR ', ')AS cuisine_name,restaurant_details.vendor_id,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount,restaurant_details.opening_time,restaurant_details.closing_time,restaurant_details.busy_status
        //     FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        //     left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
        //     left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
        //     WHERE (location LIKE CONCAT('%', '".$location."' ,'%') or area LIKE CONCAT('%', '".$location."' ,'%') or 
        //     street LIKE CONCAT('%', '".$location."' ,'%'))  $cond1 GROUP BY users.id $cond ";
    }

    $Array = $db->Database->select_qry_array($Qry);

    $html = '';
    if (count($Array) > 0) {
        foreach ($Array as $details) {

            $Qry1 = "SELECT  delivery_timings.*, GROUP_CONCAT(city_name) AS city_name FROM  delivery_timings 
                                        INNER JOIN cities ON FIND_IN_SET(city_id, cities) > 0 WHERE delivery_timings.vendor_id = $details->id AND cities.city_name LIKE CONCAT('%', '" . trim($_POST['locality']) . "' ,'%') GROUP BY cities,delivery_timings.id";
            // print_r($Qry1);
            $Array = $db->Database->select_qry_array($Qry1);
            foreach ($Array as $dat) {
                $start_time = date('H:i', strtotime($dat->delivery_from));
                $end_time = date('H:i', strtotime($dat->delivery_to));
                $currentTime = date('H:i', time());
                //print_r($currentTime);
                if ($start_time != '00:00:00' && $end_time != '00:00:00') {
                    // if (($end_time < $start_time && ($currentTime <= $start_time && $currentTime >= $end_time)) || 
                    // ($end_time > $start_time && ($currentTime <= $start_time || $currentTime >= $end_time)) ){
                    //     $delivery = 0;
                    // }else{
                    //     $delivery = 1;
                    // }
                    if ($currentTime > $start_time && $currentTime < $end_time) {
                        $delivery = 1;
                    } elseif ($currentTime < $start_time && $currentTime < $end_time) {
                        $delivery = 1;
                    } else {
                        $delivery = 0;
                    }
                }
                // print_r($delivery);
                if ($delivery == 1) {
                    $delivery_time = $db->lang->line("same_day");
                } else {
                    $delivery_time = $db->lang->line("next_day");
                }
            }

            $qry = "SELECT count(id) as total_reviews,AVG(rating) as rating FROM user_feedback WHERE vendor_id=" . $details->vendor_id;
            $Array1 = $db->Database->select_qry_array($qry);
            $rating = !empty($Array1[0]->rating) ? number_format($Array1[0]->rating, 1) : 0;

            $ConditionArray = array('vendor_id' => $details->vendor_id);
            $Qry2 = "SELECT IFNULL(GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ','), '') as cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar FROM `restaurant_cuisine_type` LEFT JOIN `cuisine` ON cuisine.cuisine_id=restaurant_cuisine_type.cuisine_id  WHERE vendor_id=:vendor_id";
            $Array2 = $db->Database->select_qry_array($Qry2, $ConditionArray);

            $html .= '<div class="wd100 __gdwp">
    							<div class="media">';

            if (!empty($details->image)) {
                $image = base_url() . 'uploads/vendor_images/' . $details->image;
            } else {
                $image = base_url() . 'images/default_rest.png';
            }
            if ($db->session->userdata('language') == 'ar') {
                if ($details->restaurant_name_ar != '') {
                    $rest_name = $details->restaurant_name_ar;
                } else {
                    $rest_name = $details->restaurant_name;
                }
            } else {
                $rest_name = $details->restaurant_name;
            }

            $html .= '<a href="#">';
            $opening_time = date('H:i A', strtotime($details->opening_time));
            $closing_time = date('H:i A', strtotime($details->closing_time));
            $currentTime = date('H:i', time());

            if (($closing_time < $opening_time && ($currentTime <= $opening_time && $currentTime >= $closing_time)) ||
                    ($closing_time > $opening_time && ($currentTime <= $opening_time || $currentTime >= $closing_time))) {
                $html .= '<div class="__closed">
        				Closed	
        			  </div>';
            }

            if ($currentTime > $opening_time && $details->busy_status == 1) {
                $html .= '<div class="__busy">
        					Busy	
        				</div>';
            }
            $html .= '<img class="mr-4 ml-4 __gdimg" src="' . $image . '"></a>
    				 <div class="media-body __gdtx">
    				 <a href="' . base_url('search_details/' . base64_encode($details->vendor_id)) . '">
    				 <h3>' . $rest_name . '</h3>';
            if ($store_type == '1') {
                $cus = $Array2[0]->cuisine_name;
            } else {
                $cus = GetNameById($store_type, 'store_type', 'store_type');
            }
            $html .='<div class="wd100 __gd_subtag">' . $cus . '</div>
    				<div class="wd100 __star_reviews">
    			        <div class="__star"> <i class="fa fa-star"></i>' . $rating . '</div>
    				<div class="_reviews">(' . $Array1[0]->total_reviews . ' reviews) </div>
    				</div>
					<div class="wd100 __subinfo">
					<ul>';
            // 	$start_time = date('H:i', strtotime($details->delivery_time_start));
            // 	$end_time = date('H:i', strtotime($details->delivery_time_ends));
            // 	$currentTime = date('H:i', time());
            //                 if($start_time!='00:00:00' && $end_time!='00:00:00')
            //                 {
            //         if ($currentTime > $start_time && $currentTime < $end_time) {
            //             $delivery = 1;
            //         }
            //         elseif($currentTime < $start_time && $currentTime < $end_time){
            //             $delivery = 1;
            //         }
            //         else{
            //             $delivery = 0;
            //         }
            //                 }
            //                 if($delivery == 1)
            //                 {
            //                     $delivery_time = $db->lang->line("same_day");
            //                 }else{
            //                     $delivery_time = $db->lang->line("next_day");
            //                 }	
            if ($details->isDelivery == 1) {


                // $html.= '<li style="display:none;">'.$db->lang->line("delivery"). ' : '. $delivery_time.'</li>';
            }
            $html .= '<li>' . $details->delivery_time . '</li>';
            // 	$html .= '<li>'.$db->lang->line("delivery_fee").' : '.number_format($details->service_charge,2).'</li>
            $html .= '<li>' . $db->lang->line("minimum_order") . ' : ' . $details->min_amount . '</li>
    				</ul>
						</div>
    									</a>
    								</div>
    							</div>
    						</div>';
        }
    } else {
        $html .= '<div class="wd100 __gdwp">
						    <div class="media">
						        <div class="media-body __gdtx">
						            <h3 class="empty_result">Oh We are Sorry ! No ' . $store_name . ' found</h3> 
						        </div>
							</div>
						</div>';
    }
    echo $html;
}

function clear_search() {
    $db = LoadDB();

    $data = $db->session->userdata('locality');
    $latitude = floor($_POST['latitude'] * 100) / 100;
    $longitude = floor($_POST['longitude'] * 100) / 100;
    $store_type = $_POST['store_type'];
    $store_name = GetNameById($store_type, 'store_type', 'store_type');
    // $Qry = "SELECT city_id FROM  cities WHERE cities.city_name LIKE CONCAT('%', '".trim($data)."' ,'%')";
    // $Array = $db->Database->select_qry_array($Qry);
    // $city_id = $Array[0]->city_id;

    $Qry1 = "SELECT restaurant_details.vendor_id
                FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
                join delivery_timings on delivery_timings.vendor_id = restaurant_details.vendor_id 
                left join cities on cities.city_id = delivery_timings.cities
                WHERE user_type=2 AND store_type='$store_type' AND latitude LIKE CONCAT('%', '" . $latitude . "' ,'%')";
    $Array1 = $db->Database->select_qry_array($Qry1);
    // $Qry_1 = "SELECT restaurant_details.vendor_id,".
    //             "GeoDistDiff('km', '{$latitude}', '{$longitude}', users.latitude, users.longitude) as distance".
    //             " FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
    //             WHERE user_type=2 AND store_type='$store_type' AND isDelivery=0 HAVING distance <= 50";
    // $Array_1 = $db->Database->select_qry_array($Qry_1);
    // $Array_new = array_merge($Array1,$Array_1);
    $ids = [];
    for ($i = 0; $i < count($Array1); $i++) {
        $d = $Array1[$i];
        array_push($ids, $d->vendor_id);
    }

    $arr = implode("','", $ids);

    // $Qry = "SELECT users.id,users.image,restaurant_details.restaurant_name,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar,restaurant_details.vendor_id,restaurant_details.delivery_time,restaurant_details.service_charge,restaurant_details.min_amount,restaurant_details.opening_time,restaurant_details.closing_time,restaurant_details.busy_status
    //     ,restaurant_details.isDelivery,restaurant_details.delivery_time_start,restaurant_details.delivery_time_ends,isPremium
    $Qry = "SELECT users.id,users.image,users.position,restaurant_details.*,GROUP_CONCAT(cuisine.cuisine_name)AS cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar
        
        FROM users left join restaurant_details on users.id=restaurant_details.vendor_id 
        left join restaurant_cuisine_type as rct on rct.vendor_id=users.id 
        left join cuisine on cuisine.cuisine_id=rct.cuisine_id 
        WHERE restaurant_details.vendor_id IN('" . $arr . "')  
        GROUP BY users.id,restaurant_details.id
        
        ORDER BY users.position = 0, users.position";


    $Array = $db->Database->select_qry_array($Qry);

    $html = '';
    if (count($Array) > 0) {
        foreach ($Array as $details) {

            // $Qry1 = "SELECT * FROM `delivery_timings` left join cities on cities.city_id=delivery_timings.cities WHERE vendor_id=$details->vendor_id AND cities.city_name LIKE CONCAT('%', '".trim($array['locality'])."' ,'%')";
            $Qry_dat = "SELECT  delivery_timings.*, GROUP_CONCAT(city_name) AS city_name FROM  delivery_timings 
                    INNER JOIN cities ON FIND_IN_SET(city_id, cities) > 0 WHERE delivery_timings.vendor_id = $details->id AND cities.city_name LIKE CONCAT('%', '" . trim($data) . "' ,'%') GROUP BY cities,delivery_timings.id";

            $Array_dat = $db->Database->select_qry_array($Qry_dat);

            foreach ($Array_dat as $dat) {

                $start_time = date('H:i', strtotime($dat->delivery_from));
                $end_time = date('H:i', strtotime($dat->delivery_to));
                $currentTime = date('H:i', time());
                //print_r($currentTime);
                if ($start_time != '00:00:00' && $end_time != '00:00:00') {
                    // if (($end_time < $start_time && ($currentTime <= $start_time && $currentTime >= $end_time)) || 
                    // ($end_time > $start_time && ($currentTime <= $start_time || $currentTime >= $end_time)) ){
                    //     $delivery = 0;
                    // }else{
                    //     $delivery = 1;
                    // }
                    if ($currentTime > $start_time && $currentTime < $end_time) {
                        $delivery = 1;
                    } elseif ($currentTime < $start_time && $currentTime < $end_time) {
                        $delivery = 1;
                    } else {
                        $delivery = 0;
                    }
                }

                if ($delivery == 1) {
                    $delivery_time = $db->lang->line("same_day");
                } else {
                    $delivery_time = $db->lang->line("next_day");
                }
            }

            $qry = "SELECT count(id) as total_reviews,AVG(rating) as rating FROM user_feedback WHERE vendor_id=" . $details->vendor_id;
            $Array1 = $db->Database->select_qry_array($qry);
            $rating = !empty($Array1[0]->rating) ? number_format($Array1[0]->rating, 1) : 0;

            $ConditionArray = array('vendor_id' => $details->vendor_id);
            $Qry2 = "SELECT IFNULL(GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ','), '') as cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar))AS cuisine_name_ar FROM `restaurant_cuisine_type` LEFT JOIN `cuisine` ON cuisine.cuisine_id=restaurant_cuisine_type.cuisine_id  WHERE vendor_id=:vendor_id";
            $Array2 = $db->Database->select_qry_array($Qry2, $ConditionArray);

            $html .= '<div class="wd100 __gdwp">
    							<div class="media">';

            if (!empty($details->image)) {
                $image = base_url() . 'uploads/vendor_images/' . $details->image;
            } else {
                $image = base_url() . 'images/default_rest.png';
            }
            if ($db->session->userdata('language') == 'ar') {
                if ($details->restaurant_name_ar != '') {
                    $rest_name = $details->restaurant_name_ar;
                } else {
                    $rest_name = $details->restaurant_name;
                }
            } else {
                $rest_name = $details->restaurant_name;
            }

            $html .= '<a href="#">';
            $opening_time = date('H:i A', strtotime($details->opening_time));
            $closing_time = date('H:i A', strtotime($details->closing_time));
            $currentTime = date('H:i', time());

            if (($closing_time < $opening_time && ($currentTime <= $opening_time && $currentTime >= $closing_time)) ||
                    ($closing_time > $opening_time && ($currentTime <= $opening_time || $currentTime >= $closing_time))) {
                $html .= '<div class="__closed">
        				Closed	
        			  </div>';
            }

            if ($currentTime > $opening_time && $details->busy_status == 1) {
                $html .= '<div class="__busy">
        					Busy	
        				</div>';
            }

            $html .= '<img class="mr-4 ml-4 __gdimg" src="' . $image . '"></a>
    				 <div class="media-body __gdtx">
    				 <a href="' . base_url('search_details/' . base64_encode($details->vendor_id)) . '">
    				 <h3>' . $rest_name . '</h3>
    				<div class="wd100 __gd_subtag">' . $Array2[0]->cuisine_name . '</div>
    				<div class="wd100 __star_reviews">
    			    <div class="__star"> <i class="fa fa-star"></i>' . $rating . '</div>
    				<div class="_reviews">(' . $Array1[0]->total_reviews . ' reviews)</div>
    				</div>
					<div class="wd100 __subinfo">
					<ul>';
            // 	$start_time = date('H:i', strtotime($details->delivery_time_start));
            // 	$end_time = date('H:i', strtotime($details->delivery_time_ends));
            // 	$currentTime = date('H:i', time());
            //                 if($start_time!='00:00:00' && $end_time!='00:00:00')
            //                 {
            //         if ($currentTime > $start_time && $currentTime < $end_time) {
            //             $delivery = 1;
            //         }
            //         elseif($currentTime < $start_time && $currentTime < $end_time){
            //             $delivery = 1;
            //         }
            //         else{
            //             $delivery = 0;
            //         }
            //                 }
            //                 if($delivery == 1)
            //                 {
            //                     $delivery_time = $db->lang->line("same_day");
            //                 }else{
            //                     $delivery_time = $db->lang->line("next_day");
            //                 }	
            if ($details->isDelivery == 1) {


                // $html.= '<li style="display:none;>'.$db->lang->line("delivery").' : '. $delivery_time.'</li>';
            }
            $html .= '<li>' . $details->delivery_time . '</li>';
            // 	$html .= '<li>'.$db->lang->line("delivery_fee"). ' : '.number_format($details->service_charge,2).'</li>
            $html .= '<li>' . $db->lang->line("minimum_order") . ' : ' . $details->min_amount . '</li>
    				</ul>
						</div>
    									</a>
    								</div>
    							</div>
    						</div>';
        }
    } else {
        $html .= '<div class="wd100 __gdwp">
						    <div class="media">
						        <div class="media-body __gdtx">
						            <h3 class="empty_result">Oh We are Sorry ! No ' . $store_name . ' found</h3> 
						        </div>
							</div>
						</div>';
    }
    echo $html;
}

function mostSelling() {
    $db = LoadDB();
    $menu_list = $_POST['menu_list'];
    $session_arr = $db->session->userdata('Vendor');
    $data['category_id'] = $_POST['category_id'];
    $data['vendor_id'] = $session_arr->id;
    $menu_id = $menu_list;
    $Qry = "SELECT * FROM `most_selling` WHERE vendor_id=" . $data['vendor_id'];
    $CategoryArray = $db->Database->select_qry_array($Qry);
    $menu_list = explode(',', $menu_id);
    foreach ($menu_list as $menu) {
        $data['menu_id'] = $menu;
        if (count($CategoryArray) > 0) {
            $CondArray = array('vendor_id' => $data['vendor_id']);
            $db->Database->delete('most_selling', $CondArray);
            $db->Database->insert('most_selling', $data);
        } else {
            $db->Database->insert('most_selling', $data);
        }
    }
    die(json_encode(array('status' => true, 'message' => 'inserted successfully')));
}

function ApplyPromoCode() {
    $db = LoadDB();
    if (isset($_REQUEST)) {
        $array = $_REQUEST;
        $service_charge = !empty($_POST['service_charge']) ? $_POST['service_charge'] : '0';
        $promo_code = $array['promo_code'];
        $qry = "SELECT * FROM `promo_code` WHERE promo_code='$promo_code' AND archive=0 AND status=0 AND date_to >=DATE(NOW())";
        $pArray = $db->Database->select_qry_array($qry);
        if (empty($promo_code)) {
            die(json_encode(array("status" => 'error', "message" => "Please enter promocode")));
        }
        if (empty($pArray)) {
            $qry = "SELECT * FROM `promo_code` WHERE promo_code='$promo_code' AND archive=0 AND status=0";
            $pArray = $db->Database->select_qry_array($qry);
            if (!empty($pArray)) {
                die(json_encode(array("status" => 'error', "message" => "Promo code has expired.")));
            }
            die(json_encode(array("status" => 'error', "message" => "Invalid promo code.")));
        }
        $pArray = $pArray[0];
        if (date('Y-m-d', strtotime($pArray->date_from)) > date('Y-m-d')) {
            die(json_encode(array("status" => 'error', "message" => "Promo code has expired.")));
        }



        $discountedAmt = 0;
        $session_cart = $db->session->userdata('CartData');
        $sub_total = 0;
        $exists = false;
        if (!empty($pArray->restaurant_id)) {

            $matchedBox = explode(',', $pArray->restaurant_id);
            $matchedBoxFound = [];

            if (empty($pArray->discount_type)) {
                $discountedAmt = $pArray->discount / 100;
            } else {
                $discountedAmt = $pArray->discount;
            }
            $promocode_id = $pArray->promocode_id;
            $ProductExists = false;
            for ($i = 0; $i < count($session_cart); $i++) {
                $dty = $session_cart[$i];
                // print_r($dty);die();
                $sub_total+=$dty['subtotal'];
                if (in_array($dty['vendor_id'], $matchedBox)) {

                    $ProductExists = true;
                }
            }

            if ($sub_total < $pArray->purchase_amount) {
                die(json_encode(array("status" => 'error', "message" => "This code is applicable only on orders above PKR  $pArray->purchase_amount")));
            }

            if (!empty($pArray->discount_from)) {
                if (empty($service_charge)) {
                    die(json_encode(array("status" => 'error', "message" => "This code is applicable only for delivery charges")));
                }
                $sub_total = $service_charge;
            }
            if (empty($ProductExists)) {
                die(json_encode(array("status" => 'error', "message" => "Promo code is not available for this product")));
            }

            for ($i = 0; $i < count($session_cart); $i++) {
                $session_cart[$i]['promocode_id'] = $promocode_id;
                $session_cart[$i]['discount_amt'] = $discountedAmt;
                $session_cart[$i]['maximum_discount'] = $pArray->maximum_discount;
                $session_cart[$i]['discount_type'] = $pArray->discount_type;
                $session_cart[$i]['discount_from'] = $pArray->discount_from;
            }

            $cart = $session_cart;
            $cart = array_values(array_filter($cart));
            $db->session->set_userdata('CartData', $cart);
            die(json_encode(array("status" => 'success', "message" => "Promocode applied successfully")));
        }
        die(json_encode(array("status" => 'error', "message" => "Promo code is not available for this product")));
    }
}

function RemovePromoCode() {
    $db = LoadDB();
    $session_cart = $db->session->userdata('CartData');
    if (isset($_REQUEST)) {

        $array = $_REQUEST;
        for ($i = 0; $i < count($session_cart); $i++) {
            unset($session_cart[$i]['promocode_id']);
        }
        $session_cart = array_values(array_filter($session_cart));
        $db->session->set_userdata('CartData', $session_cart);
        die(json_encode(array("status" => 'success', "message" => "Promocode removed successfully")));
    }
}

function order_pdf($id = '') {
    ob_start();
    error_reporting(0);
    $db = LoadDB();
    $db->load->library('MPdf');
    $mpdf = new \Mpdf\Mpdf();
    $date = date("d-m-Y");
    $calib = 'Arial';
    $datet = date("jFY");
    $total = 0;
    $cnt = 1;
    $order = GetOrderById($id);
    $order_details = GetOrderDetailsByOrderId($order[0]->id);
    if (!empty($order)) {
        $userdetails = GetUserDetails($order[0]->user_id);
        $address = GetOrderAddressById($order[0]->delivery_address);

        $order_data = GetOrderByOrderId($id);

        if ($address[0]->building != '') {
            $type = 'Building Name';
            $building = $address[0]->building;
            $floor = $address[0]->floor;
        }if ($address[0]->house != '') {
            $type = 'House Name';
            $building = $address[0]->house;
            $floor = '';
        }if ($address[0]->office != '') {
            $type = 'Office Name';
            $building = $address[0]->office;
            $floor = $address[0]->floor;
        }
    }
    $mpdf->mirrorMargins = 0;
    $html = '<table width="100%" border="0" cellspacing="0" cellpadding="10" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;
    color:#000;border:#ccc 1px solid; ">
  <tr>
    <td width="50%" align="left" valign="top" scope="col"><img src="' . base_url() . 'images/logo.png" alt="" style="width:150px;padding-top:10px" class="CToWUd"></td>
    
  </tr>
  <tr>
  
<td colspan="2" align="center" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:17px ;color:#000;">
	<h3>Order Invoice</h3>
</td> 
  </tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="10" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;
    color:#000;border:#ccc 1px solid; border-top: none;">
<tr>
<td width="50%" align="left" valign="top">
<h5 style="<?= $style ?>">Order ID: <span style="font-weight: normal;">#' . $order[0]->orderno . '</span></h5>
<h5 style="<?= $style ?>">Order Date: <span style="font-weight: normal;">' . date('d/m/Y', strtotime($order[0]->date)) . '</span></h5>
</td>
<td width="50%" align="right" valign="top">
<h5 style="<?= $style ?>">Customer Name: <span style="font-weight: normal;">' . $userdetails[0]->name . '</span></h5>
<h5 style="<?= $style ?>">Email:<span style="font-weight: normal;">' . $userdetails[0]->email . '</span></h5>
<h5 style="<?= $style ?>">Phone:<span style="font-weight: normal;">' . $userdetails[0]->mobile_code . $userdetails[0]->mobile_number . '</span></h5>
</td> 
</tr>
</table>
 
<table width="100%" border="0" cellspacing="0" cellpadding="8" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;
    color:#000;border:#ccc 1px solid; border-top: none;border-bottom: none; ">
    <tr>
        <td colspan="2" style="border-bottom:#ccc 1px solid; font-weight:bold;font-size:14px;" valign="middle" align="left">Delivery Address</td>
       <td width="45" style="border-bottom:#ccc 1px solid;"></td>
        <td  colspan="2" style="border-bottom:#ccc 1px solid; font-weight:bold;font-size:14px;" valign="middle" align="left"></td>
    </tr>
    <tr>
        <td width="62" align="left" valign="middle" style="border-bottom:#ccc 1px solid; ">Address:</td>
        <td width="266" align="left" valign="middle" style="border-bottom:#ccc 1px solid; ">' . $address[0]->address . ',' . $address[0]->street . ',' . $building . '</td>
        <td width="45" align="left" valign="middle" style="border-bottom:#ccc 1px solid; ">&nbsp;</td>
        <td width="73" align="left" valign="middle" style="border-bottom:#ccc 1px solid; "></td>
        <td width="335" align="left" valign="middle" style="border-bottom:#ccc 1px solid; "></td>
    </tr>
   
</table>
 
<table width="100%" border="0" cellspacing="0" cellpadding="5" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;
    color:#000;border:#ccc 1px solid; border-top: none;border-bottom: none;">
        
        <tr><th valign="middle" align="center" colspan="4" style="font-size:15px">&nbsp;Order Details</th></tr>
        <tr>
        <th width="60%" align="left" valign="middle" bgcolor="#e4e4e4" style="font-weight:500;font-size:14px;">Item(s)</th>
        <th width="15%" align="left" valign="middle" bgcolor="#e4e4e4" style="font-weight:500;font-size:14px;">Quantity</th>
        <th width="30%" align="left" valign="middle" bgcolor="#e4e4e4" style="font-weight:500;font-size:14px;">Price</th>
        <th width="60%" align="left" valign="middle" bgcolor="#e4e4e4" style="font-weight:500;font-size:14px;">Total</th>
        </tr>';
    foreach ($order as $ord) {

        $html .= '<tr>
            <td valign="middle" align="left" style="border-bottom:#ccc 1px solid; font-size:14px;">' . $ord->product_name . '</td>
            <td valign="middle" align="left" style="border-bottom:#ccc 1px solid; font-size:14px;">' . $ord->quantity . '</td>
            <td valign="middle" align="left" style="border-bottom:#ccc 1px solid; font-size:14px;">' . DecimalAmount($ord->ord_del_price) . '</td>
            <td valign="middle" align="left" style="border-bottom:#ccc 1px solid; font-size:14px;">' . DecimalAmount($ord->ord_del_price * $ord->quantity) . '</td>
         </tr>';
    }
    // <tr>
    //     <td align="right" valign="middle" style="border-bottom:#ccc 1px solid; font-size:12px;">Delivery Charge</td>
    //     <td width="21%" colspan="-1"  align="center" valign="middle" style="border-left:#ccc 1px solid; border-bottom:#ccc 1px solid; font-size:12px; font-weight:bold;">PKR  '.number_format($order_data[0]->service_charge,2).'</td>
    // </tr>
    $html .= '</table>
    <table width="100%" border="0" cellspacing="0" cellpadding="5" style="font-family:Arial, Helvetica, sans-serif;
        font-size:13px;color:#000;border:#ccc 1px solid; border-top: none;border-bottom: none;">    

                <tr>
                    <td align="right" valign="middle" style="border-bottom:#ccc 1px solid; font-size:12px;">Sub Total</td>
                    <td width="21%" colspan="-1"  align="center" valign="middle" style="border-left:#ccc 1px solid; border-bottom:#ccc 1px solid; font-size:12px; font-weight:bold;">PKR  ' . DecimalAmount($order_data[0]->price) . '</td>
                </tr>
                <tr>
                    <td align="right" valign="middle" style="border-bottom:#ccc 1px solid; font-size:12px;">Delivery Charge</td>
                    <td width="21%" colspan="-1"  align="center" valign="middle" style="border-left:#ccc 1px solid; border-bottom:#ccc 1px solid; font-size:12px; font-weight:bold;">PKR  ' . DecimalAmount($order_data[0]->service_charge) . '</td>
                </tr>

                <tr>
                    <td align="right" valign="middle" style="border-bottom:#ccc 1px solid; font-size:12px;">Discount Amount</td>
                    <td width="21%" colspan="-1"  align="center" valign="middle" style="border-left:#ccc 1px solid; border-bottom:#ccc 1px solid; font-size:12px; font-weight:bold;">PKR  ' . DecimalAmount($order_data[0]->discount) . '</td>
                </tr>
                
                <tr>
                    <td align="right" valign="middle" style="border-bottom:#ccc 1px solid; font-size:12px;">Total</th>
                    <td colspan="-1"  align="center" valign="middle" style="border-left:#ccc 1px solid; border-bottom:#ccc 1px solid; font-size:12px; font-weight:bold;">PKR  ' . DecimalAmount($ord->total_price + $order_data[0]->service_charge) . '</td>
                </tr>
                <tr>
                  <td  colspan="2" align="left" valign="middle" style="border-bottom:#ccc 1px solid; font-size:12px;">    
                         
           		 <p style="color:#616161;font-style:italic; font-family:Arial, Helvetica, sans-serif; 
                   padding:0px 0 0;  margin:0px; font-size:11px;text-align:left;"> This is a computer generated invoice.</p>
        </tr>
    </table>';
    $mpdf->defaultfooterline = 0;
    $a = $mpdf->WriteHTML($html);
    $filename = HOME_DIR . "uploads/invoice/" . $order[0]->orderno . "-" . $datet . ".pdf";
    $mpdf->Output($filename, 'f');
    $CondArray = array('id' => $id);
    $json['invoice'] = $order[0]->orderno . "-" . $datet . ".pdf";
    $db->Database->update('orders', $json, $CondArray);
}

function CancelledMyOrder() {
    $db = LoadDB();
    $OrderId = !empty($_POST['OrderId']) ? $_POST['OrderId'] : '';
    $Order = GetOrderByOrderId($OrderId);
    $UserArray = GetSessionArrayUser();
    if (!empty($Order)) {
        $user_details = GetUserDetails($Order[0]->vendor_id);

        if ($user_details[0]->store_type != 1) {
            CancelProductOrder($Order[0]->id);
        }

        $CondP455 = array('id' => $Order[0]->id);
        $UpdateSzz['order_status'] = 7;
        $UpdateSzz['cancellation_msg'] = $_POST['WritenQuery'] . " (cancelled by $UserArray->name)";
        $UpdateSzz['cancellation_reason'] = $_POST['WritenQueryOp'];
        $db->Database->update('orders', $UpdateSzz, $CondP455);


        die(json_encode(array('status' => true, 'message' => 'your order has been cancelled.')));
    }
    die(json_encode(array('status' => false, 'message' => 'your order does not exists.')));
}

function CancelProductOrder($OrderId) {
    $db = LoadDB();
    $OrderPer = GetOrderById($OrderId);
    for ($i = 0; $i < count($OrderPer); $i++) {
        $d = $OrderPer[$i];
        $pd = GetProductCountByProductId($d->product_id);
        $CondP = array('id' => $d->product_id);
        $UpdateQtry['stock'] = $pd->stock + $d->quantity;
        $db->Database->update('menu_list', $UpdateQtry, $CondP);
    }
    return true;
}

function GetTransactionsByVendorId($id = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `user_transactions` WHERE vendor_id=$id order by id desc";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetTransactions() {
    $db = LoadDB();
    $Qry = "SELECT UT.*,O.service_charge,O.orderno FROM `user_transactions` UT LEFT JOIN orders O ON O.id=UT.order_id WHERE O.order_status NOT IN (7) order by UT.id desc";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetorderstatushistorylastBy($orderId = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `order_status_history` WHERE order_id='$orderId' ORDER BY id DESC LIMIT 1";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : '';
    return $dArray;
}

function assign_deliveryboy() {
    $db = LoadDB();
    $updateArray = array(
        'deliveryboy_id' => !empty($_POST['deliveryboy_id']) ? $_POST['deliveryboy_id'] : '',
        'order_status' => ASSIGN_DELIVERYBOY,
    );


    $cond = array('id' => $_POST['orderId']);
    $db->Database->update('orders', $updateArray, $cond);

    $data['order_id'] = $_POST['orderId'];
    $data['status_id'] = ASSIGN_DELIVERYBOY;
    $data['notes'] = "function name assign_deliveryboy, status Id-" . $data['status_id'] . ", order Id-" . $data['order_id'];
    if (canUpdateorderStatus($data['order_id'], $data['status_id'])) {
        $db->Database->insert('order_status_history', $data);
    }
    $delivery_array['sender_id'] = $_POST['vendor_id'];
    $delivery_array['receiver_id'] = $_POST['deliveryboy_id'];
    $delivery_array['notification_type'] = DELIVERYBOY_ASSIGNED;
    $delivery_array['notification_message'] = 'New Order Request';
    $delivery_array['redirect_url'] = '';
    $delivery_array['order_id'] = $_POST['orderId'];

    $db->Database->insert('notification', $delivery_array);

    if (isset($_POST['login_id'])) {

        $admin_array['sender_id'] = $_POST['login_id'];
        $admin_array['receiver_id'] = $_POST['vendor_id'];
        $admin_array['notification_type'] = DELIVERYBOY_ASSIGNED;
        $admin_array['notification_message'] = 'Delivery boy assigned';
        $admin_array['redirect_url'] = '';
        $admin_array['order_id'] = $_POST['orderId'];

        $db->Database->insert('notification', $admin_array);
    } else {
        $qry = "SELECT id FROM `admin_login` WHERE LoginType='1'";
        $array = $db->Database->select_qry_array($qry);
        $admin_array['sender_id'] = $_POST['vendor_id'];
        $admin_array['receiver_id'] = $array[0]->id;
        $admin_array['notification_type'] = DELIVERYBOY_ASSIGNED;
        $admin_array['notification_message'] = 'Delivery boy assigned';
        $admin_array['redirect_url'] = '';
        $admin_array['order_id'] = $_POST['orderId'];

        $db->Database->insert('notification', $admin_array);
    }

    $user_array['sender_id'] = $_POST['vendor_id'];
    $user_array['receiver_id'] = $_POST['user_id'];
    $user_array['notification_type'] = DELIVERYBOY_ASSIGNED;
    $user_array['notification_message'] = 'Deliveryboy is assigned to your order';
    $user_array['redirect_url'] = '';
    $user_array['order_id'] = $_POST['orderId'];

    $db->Database->insert('notification', $user_array);

    // $rArray = GetrequestArrayBy($_POST['requestId']);
    // $subject = "Auto Glow - $rArray->category_name";
    // $statusName = GetRequestStatus($_POST['request_status']);
    // if ($_POST['request_status'] == '1') {
    //     $message = "One Request has been assigned by admin.";
    //     send_pushnotifaction($rArray->technician_deviceid, $subject, $message, $identifier = '1', $argument = ['request_id' => $_POST['requestId']]);
    // }
    // $assignto = $_POST['request_status'] == '1' ? "to $rArray->technician_name" : '';
    // $message = "Your request has been $statusName $assignto.";
    // send_pushnotifaction($rArray->user_deviceid, $subject, $message, $identifier = '1', $argument = ['request_id' => $_POST['requestId']]);

    die(json_encode(array('status' => true, 'message' => 'successfully')));
}

function GetLoyalityPointsAmountByuserId($userId = '') {
    $db = LoadDB();
    $qry = "SELECT SUM(available_balance) AS balance FROM `loyality_point_history` WHERE user_id='$userId'";
    $dArray = $db->Database->select_qry_array($qry);
    $balanceTotal = !empty($dArray[0]->balance) ? $dArray[0]->balance : '0';
    $qry = "SELECT SUM(amount) AS amountSpend FROM `loyality_point_spent` WHERE user_id='$userId'";
    $sArray = $db->Database->select_qry_array($qry);
    $spendTotal = !empty($sArray[0]->amountSpend) ? $sArray[0]->amountSpend : '0';

    $total = $balanceTotal - $spendTotal;

    if ($total < 0) {
        return false;
    }
    return $total;
}

function GetpartyorderBy($orderId = '') {
    $db = LoadDB();
    $select = ",CONCAT(U.name) AS user_name,CONCAT(U.mobile_code,U.mobile_number) AS user_phone,U.email AS user_email";
    $join = "LEFT JOIN users U ON U.id=PO.user_id";
    $qry = "SELECT PO.* $select FROM `party_order` PO $join WHERE PO.id='$orderId'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : '';
    return $dArray;
}

function GetpartyorderdetailsBy($orderId = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `party_order_details` WHERE party_order_id='$orderId'";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}

function GetstoretypeBy($storeId = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `store_type` WHERE id='$storeId'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : '';
    return $dArray;
}

function GetstoretypeByAll() {
    $db = LoadDB();
    $qry = "SELECT * FROM `store_type` WHERE archive=0";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}

function GetuseraddressBy($addressId = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `user_order_address` WHERE id='$addressId'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : '';
    return $dArray;
}

function GetridercommissionBy($comId = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `rider_commission` WHERE id='$comId'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : '';
    return $dArray;
}

function GetcontactusBy($id) {
    $db = LoadDB();
    $qry = "SELECT * FROM `contact_us` WHERE id='$id'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : '';
    return $dArray;
}

function getdeliverychargesBy($addressId, $UserId) {
    $db = LoadDB();

    $address = GetuseraddressBy($addressId);
    if (empty($address)) {
        die(json_encode(array('response' => array('status' => false, 'message' => 'invalid address id', 'result' => []))));
    }
    $Qry = "SELECT * FROM  add_to_cart WHERE user_id='$UserId'";
    $CartArray = $db->Database->select_qry_array($Qry);
    $vendor_id = !empty($CartArray) ? $CartArray['0']->vendor_id : '';
    $vendor = GetUserDetails($vendor_id);


    $Qry = "SELECT * FROM `restaurant_details` LEFT JOIN users ON users.id=restaurant_details.vendor_id WHERE vendor_id='$vendor_id'";
    $rest_details = $db->Database->select_qry_array($Qry);

    $rest_details1 = GetusersById($rest_details[0]->vendor_id);

    $distanceTotal = distance($rest_details[0]->latitude, $rest_details[0]->longitude, $address->latitude, $address->longitude, $unit = 'K');
    $delivery_radius = !empty($rest_details1->delivery_radius) ? $rest_details1->delivery_radius : '0';

    $dlCharges = getdeleverychagesfeecal35($rest_details, $address->latitude, $address->longitude);
    return $dlCharges;
}

function GetloyalitypointsBy($loyalityId = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `loyality_points` WHERE id='$loyalityId'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : '';
    return $dArray;
}

function calculateloyalitypointByAmount($totalAmt = '') {
    $lArray = GetloyalitypointsBy($loyalityId = '1');
    $amtcalc = '0';
    if (!empty($lArray)) {
        $amtcalc = ($totalAmt / $lArray->point_amt) * $lArray->point;
    }
    return $amtcalc;
}

function addloyalitypointcreditAmt($userId, $orderId, $orderTotal) {
    $db = LoadDB();
    $insert['user_id'] = $userId;
    $insert['order_id'] = $orderId;
    $insert['credit_amt'] = calculateloyalitypointByAmount($orderTotal);
    $insert['lastupdate'] = date('Y-m-d H:i:s');
    $insert['timestamp'] = $insert['lastupdate'];
    $db->Database->insert('loyality_point_history', $insert);
}

function getloyalityTotalCreditPoint($userId) {
    $db = LoadDB();
    $qry = "SELECT SUM(LPH.credit_amt) AS totalCredit FROM `loyality_point_history` LPH LEFT JOIN orders O ON O.id=LPH.order_id WHERE LPH.user_id='$userId' AND O.order_status NOT IN (7) AND O.archive=0";
    $dArray = $db->Database->select_qry_array($qry);
    $totalCredit = !empty($dArray[0]->totalCredit) ? $dArray[0]->totalCredit : '0';

    $qry = "SELECT SUM(loyality_point_discount) AS totaldebit FROM `orders` WHERE user_id='$userId' AND order_status NOT IN (7) AND archive=0";
    $d45Array = $db->Database->select_qry_array($qry);
    $totaldebit = !empty($d45Array[0]->totaldebit) ? $d45Array[0]->totaldebit : '0';
    $totalAmt = $totalCredit - $totaldebit;
    return $totalAmt;
}

function applyloyalityPoints($applyPoint, $userId, $cartTotal) {
    $isApi = !empty($_REQUEST['isApi']) ? $_REQUEST['isApi'] : false;
    $db = LoadDB();
    $lArray = GetloyalitypointsBy($loyalityId = '1');
    $creditPoint = getloyalityTotalCreditPoint($userId);




    if ($creditPoint < $lArray->point_limit_for_redeem) {
        die(json_encode(array("code" => 200, "response" => array("status" => false, "message" => "Please collect minimum $lArray->point_limit_for_redeem points to redeem  ."))));
    } else if ($creditPoint < $applyPoint) {
        die(json_encode(array("code" => 200, "response" => array("status" => false, "message" => "Invalid point please check again."))));
    } else if ($applyPoint < 1) {
        die(json_encode(array("code" => 200, "response" => array("status" => false, "message" => "Invalid point please recheck again."))));
    }

    $applyAmt = $applyPoint * $lArray->redm_amt;




    if ($cartTotal < $applyAmt) {
        die(json_encode(array("code" => 200, "response" => array("status" => false, "message" => "applying greater then cart amount."))));
    }


    if (empty($isApi)) {
        $db->session->set_userdata('CSLoyalityPoints', $applyAmt);
        $db->session->set_userdata('CSLoyalityINPoints', $applyPoint);
    }
    $result['loyality_point_discount'] = $applyPoint;
    $result['loyality_discount'] = $applyAmt;
    return $result;
    // die(json_encode(array("code" => 200, "response" => array("status" => true, "message" => "success"), 'result' => $result)));
}

function updatedeveiceToken($userId, $deviceId) {
    $db = LoadDB();
    $qry = "SELECT device_id  FROM `users` WHERE `id` = '$userId'";
    $divAr = $db->Database->select_qry_array($qry);
    $divAr = !empty($divAr) ? $divAr[0] : '';
    $devicelistId = !empty($divAr->device_id) ? json_decode($divAr->device_id, true) : [];

    if (empty($devicelistId) && !empty($divAr->device_id)) {
        $devicelistId[] = array('device_id' => $divAr->device_id);
    }
    $devicelistId = is_array($devicelistId) ? $devicelistId : [];
    $newdiv = [];
    for ($i = 0; $i < count($devicelistId); $i++) {
        $d = $devicelistId[$i];
        if ($d['device_id'] != $deviceId) {
            $newdiv[] = $d;
        }
    }
    $newdiv[] = array('device_id' => $deviceId);

    $updatedev['device_id'] = json_encode($newdiv);
    $cond = array('id' => $userId);
    $db->Database->update('users', $updatedev, $cond);
    return true;
}

function reformateMydeviceId($deviceId = []) {
    $mydeviceId = !empty($deviceId[0]) ? $deviceId[0] : '';
    $jsonEn = json_decode($mydeviceId, true);
    $storedDev = [];
    if (is_array($jsonEn)) {
        for ($i = 0; $i < count($jsonEn); $i++) {
            $dr = $jsonEn[$i];
            $storedDev[] = $dr['device_id'];
        }
    } else {
        if (empty($jsonEn) && !empty($mydeviceId)) {
            $storedDev[] = $mydeviceId;
        }
    }
    return $storedDev;
}

function removeDeviceId($userId, $deviceId) {
    $db = LoadDB();
    updatedeveiceToken($userId, $deviceId);
    $qry = "SELECT device_id  FROM `users` WHERE `id` = '$userId'";
    $divAr = $db->Database->select_qry_array($qry);
    $divAr = !empty($divAr) ? $divAr[0] : '';
    $devicelistId = !empty($divAr->device_id) ? json_decode($divAr->device_id, true) : [];

    $newIds = [];
    for ($i = 0; $i < count($devicelistId); $i++) {
        $d = $devicelistId[$i];
        if ($d['device_id'] != $deviceId) {
            $newIds[] = $d;
        }
    }

    $updatedev['device_id'] = json_encode($newIds);
    $cond = array('id' => $userId);
    $db->Database->update('users', $updatedev, $cond);
}

function getOrderdeliverydate($orderid = '') {
    $db = LoadDB();
    $qry = "SELECT timestamp  FROM `order_status_history` WHERE `status_id` = 6 AND order_id='$orderid'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : '';
    $date = !empty($dArray->timestamp) ? date('d M-Y h:i A', strtotime($dArray->timestamp)) : '';
    return $date;
}
