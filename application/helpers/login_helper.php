<?php

function IsLogedAdmin() {
    $Ci = & get_instance();
    $AdminSession = $Ci->session->userdata('Admin');
    if (empty($AdminSession)) {
        redirect(base_url('Login/Admin'));
    }
}

function IsLogedVendor() {
    $Ci = & get_instance();
    $AdminSession = $Ci->session->userdata('Vendor');
    if (empty($AdminSession)) {
        redirect(base_url('Login/Vendor'));
    } else {
        $db = LoadDB();
        $qry = "SELECT id FROM `users` WHERE id ='$AdminSession->id'";
        $array_list = $db->Database->select_qry_array($qry);
        if (empty($array_list)) {
            redirect(base_url('Login/Vendor'));
        }
    }
}

function is_logged_user() {
    $ci = & get_instance();
    $user_session = $ci->session->userdata('UserLogin');
    if (empty($user_session)) {
        redirect(base_url('login_signup'));
    } else {
        $db = LoadDB();
        $qry = "SELECT id FROM `users` WHERE id ='$user_session->id'";
        $array_list = $db->Database->select_qry_array($qry);
        if (empty($array_list)) {
            redirect(base_url('login_signup'));
        }
    }
}

function is_not_logged_user() {
    $ci = & get_instance();
    $user_session = $ci->session->userdata('UserLogin');
    if (!empty($user_session)) {
        redirect(base_url('/'));
    }
}

function GetSessionArrayAdmin() {
    $Ci = & get_instance();
    $UserSession = $Ci->session->userdata('Admin');
    if (!empty($UserSession)) {
        $UserSession = GetAdminArrayBy($UserSession->id);
        return $UserSession;
    }
}

function GetSessionArrayVendor() {
    $Ci = & get_instance();
    $UserSession = $Ci->session->userdata('Vendor');
    if (!empty($UserSession)) {
        $UserSession = GetVendorArrayBy($UserSession->id);
        return $UserSession;
    }
}

function GetSessionArrayUser() {
    $ci = & get_instance();
    $SessionArray = $ci->session->userdata('UserLogin');
    return $SessionArray;
}

function GetVendorArrayBy($Id = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM  users  WHERE id = '$Id'";
    $array_list = $db->Database->select_qry_array($qry);
    if (!empty($array_list)) {
        $array_list = $array_list[0];
        $ImageFullPath = 'files/vendor_images/' . $array_list->image;
        if (!is_file(HOME_DIR . $ImageFullPath)) {
            $ImageFullPath = 'assets/layouts/layout/img/avatar3_small.jpg';
        }
        $array_list->LogoFullURL = PROJECT_PATH . $ImageFullPath;
        return $array_list;
    }
}

function GetAdminArrayBy($Id = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM  admin_login  WHERE id = '$Id'";
    $array_list = $db->Database->select_qry_array($qry);
    if (!empty($array_list)) {
        $array_list = $array_list[0];
        // $ImageFullPath = 'uploads/' . $array_list->image;
        // if (!is_file(HOME_DIR . $ImageFullPath)) {
        //     $ImageFullPath = 'assets/layouts/layout/img/avatar3_small.jpg';
        // }
        // $array_list->LogoFullURL = PROJECT_PATH . $ImageFullPath;
        return $array_list;
    }
}

function SetAdditionalSession($Array) {
    $AdditionalSession = GetAdditionalSession();
    $ci = & get_instance();
    if (empty($AdditionalSession)) {
        $AdditionalSession = array();
    }
    if (isset($Array['CallBackURL']) && $Array['CallBackURL'] != '') {
        $AdditionalSession['CallBackURL'] = $Array['CallBackURL'];
    }
    if (isset($Array['ReviewDescription']) && $Array['ReviewDescription'] != '') {
        $AdditionalSession['ProductPageReview'] = $Array;
    }
    $ci->session->set_userdata('AdditionalSession', $AdditionalSession);
}

function UnsetAdditionalSession($Array) {
    $AdditionalSession = GetAdditionalSession();
    $ci = & get_instance();
    if (empty($AdditionalSession)) {
        $AdditionalSession = array();
    }
    for ($i = 0; $i < count($Array); $i++) {
        unset($AdditionalSession[$Array[$i]]);
    }
    $ci->session->set_userdata('AdditionalSession', $AdditionalSession);
}

function validationLoyalityPoints() {
    $sessionUser = GetSessionArrayUser();

    $loyalityAmt = !empty($_POST['loyalityAmt']) ? $_POST['loyalityAmt'] : '0';
    $total_amouttt = !empty($_POST['total_amouttt']) ? $_POST['total_amouttt'] : '0';
    applyloyalityPoints($loyalityAmt, $sessionUser->id, $total_amouttt);
    exit;

    $ci = & get_instance();
    if ($total_amouttt < $loyalityAmt) {
        die(json_encode(array("status" => false, "message" => "Invalid amount.")));
    }
    if (!empty($sessionUser)) {
        $amount = GetLoyalityPointsAmountByuserId($sessionUser->id);
        if ($amount > 0) {
            $currentBal = $amount - $loyalityAmt;

            if ($currentBal >= 0) {
                $ci->session->set_userdata('CSLoyalityPoints', $loyalityAmt);
                die(json_encode(array("status" => true, "message" => "PKR  $loyalityAmt dictated amount successfully.")));
            } else {
                die(json_encode(array("status" => false, "message" => "Only available $amount PKR  in you account.")));
            }
        } else {
            die(json_encode(array("status" => false, "message" => "No balance available for the user.")));
        }
    }
    die(json_encode(array("status" => false, "message" => "No balance available for the user.")));
}

function GetSessionLoyalityPoints() {
    $ci = & get_instance();
    $Amount = $ci->session->userdata('CSLoyalityPoints');
    $Amount = !empty($Amount) ? $Amount : '0';
    return $Amount;
}

function GetSessionLoyalityPointsadded() {
    $ci = & get_instance();
    $Amount = $ci->session->userdata('CSLoyalityINPoints');
    $Amount = !empty($Amount) ? $Amount : '0';
    return $Amount;
}

function removeLoyalityPoints() {
    $ci = & get_instance();
    $ci->session->set_userdata('CSLoyalityPoints', '');
    die(json_encode(array("status" => true, "message" => "Removed successfully.")));
}

function assignRiderbyVendor() {
    $db = LoadDB();
    $cArray = array('id' => $_POST['orderId']);
    $json['assign_rider'] = $_POST['riderId'];
    $commission_update = !empty($_POST['commission_update']) ? $_POST['commission_update'] : '';

    $order = GetOrderByOrderId($_POST['orderId']);
    $userArrayd = GetusersById($_POST['riderId']);
    if (!empty($userArrayd) && !empty($order)) {
        $order = $order[0];
        $subject = "New order assign - $order->orderno";
        if (!empty($commission_update)) {
            updateRidercommissionByOrder($_POST['orderId']);
        }
        send_pushnotification(array($userArrayd->device_id), $subject, $order->address, $identifier = '1', array('order_id' => $order->id));
    }

    $db->Database->update('orders', $json, $cArray);
    die(json_encode(array("status" => true, "message" => "successfully assign.")));
}

function updateRiderCommissionRRRR() {
    $db = LoadDB();
    $cArray = array('id' => $_POST['dboyid']);
    $json['rider_commission_id'] = $_POST['rider_commission_id'];



    $db->Database->update('users', $json, $cArray);
    die(json_encode(array("status" => true, "message" => "successfully assign.")));
}

function getRiderlivelocation() {
    $db = LoadDB();
    $qry = "SELECT name,liv_lat,liv_long,liv_address,lastupdate,online_status FROM `users` WHERE user_type=4 AND archive=0 AND liv_lat!='' AND liv_long!=''";
    $list = $db->Database->select_qry_array($qry);
    die(json_encode(array("status" => true, "message" => "successfully assign.", 'result' => $list)));
}

function GetSalesMarker() {
    $db = LoadDB();
    $select = ",'' AS timestamp,'' AS sales_name";

    $latitude = !empty($_REQUEST['latitude']) ? $_REQUEST['latitude'] : '';
    $longitude = !empty($_REQUEST['longitude']) ? $_REQUEST['longitude'] : '';

    $dateFrom = !empty($_REQUEST['dateFrom']) ? date('Y-m-d', strtotime($_REQUEST['dateFrom'])) : '';
    $dateTo = !empty($_REQUEST['dateTo']) ? date('Y-m-d', strtotime($_REQUEST['dateTo'])) : '';


    $distance = !empty($_REQUEST['radius']) ? $_REQUEST['radius'] : '1';
    ;
    $distnc = '';
    $having = '';
    $cond = '';
    if (!empty($latitude) && !empty($longitude)) {
        $having = $having . "HAVING distnc<= '$distance'";
        $distnc = ",IFNULL(ROUND((6371 * acos( cos( radians($latitude) ) * cos( radians(UOA.latitude) ) * cos( radians(UOA.longitude) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians(UOA.latitude) ) ) ),2),0) AS distnc";
    }
    if (!empty($dateFrom) && !empty($dateTo)) {
        $cond = $cond . " AND DATE(O.date) BETWEEN '$dateFrom' AND '$dateTo' ";
    }
    $qry = "SELECT O.orderno AS description,O.id AS sales_id,UOA.latitude,UOA.longitude,UOA.address AS location_name $distnc $select FROM `orders` O LEFT JOIN user_order_address UOA ON UOA.id=O.delivery_address WHERE O.archive=0 AND O.order_status NOT IN(7) AND O.delivery_address NOT IN (0) $cond $having";
    $list = $db->Database->select_qry_array($qry);



    die(json_encode(array("status" => true, "message" => "successfully assign.", 'latitude' => $latitude, 'longitude' => $longitude, 'result' => $list)));
    echo '{"status":true,"message":"","result":[{"id":"295511","sales_id":"9","latitude":"25.0374281","longitude":"56.3511285","location_name":"93 Rugaylat Rd - Kalba - United Arab Emirates","timestamp":"2020-09-28 13:58:33","sales_name":"SAIF0507382118","description":"SAIF0507382118 01:58 PM"},{"id":"295513","sales_id":"10","latitude":"25.8111802","longitude":"56.0290046","location_name":"Unnamed Road - Shamal Julphar - Ras Al-Khaimah - United Arab Emirates","timestamp":"2020-09-28 14:00:37","sales_name":"SHAHZAD0568870235","description":"SHAHZAD0568870235 02:00 PM"},{"id":"295512","sales_id":"15","latitude":"24.2226904","longitude":"55.7732907","location_name":"Unnamed Road - Al Murabaa - Abu Dhabi - United Arab Emirates","timestamp":"2020-09-28 13:58:54","sales_name":"FIROZ0507941683","description":"FIROZ0507941683 01:58 PM"},{"id":"295505","sales_id":"30","latitude":"24.3478116","longitude":"54.4844785","location_name":"Mussaffah 40 - Facing ICAD Residential City - \u0645\u0635\u0641\u062dM-40 - \u0623\u0628\u0648 \u0638\u0628\u064a - United Arab Emirates","timestamp":"2020-09-28 13:49:02","sales_name":"ALI0544115701","description":"ALI0544115701 01:49 PM"},{"id":"295514","sales_id":"36","latitude":"25.3296975","longitude":"55.3673835","location_name":"Helipad - Unnamed Road - \u0627\u0644\u062e\u0627\u0646 - \u0627\u0644\u0634\u0627\u0631\u0642\u0629 - United Arab Emirates","timestamp":"2020-09-28 14:00:58","sales_name":"KHUZEMA0568870214","description":"KHUZEMA0568870214 02:00 PM"},{"id":"295509","sales_id":"70","latitude":"24.3197436","longitude":"54.5394401","location_name":"Unnamed Road - \u0645\u062f\u064a\u0646\u0629 \u0645\u062d\u0645\u062f \u0628\u0646 \u0632\u0627\u064a\u062fShabiya 12 - Mohamed Bin Zayed CityShabiya 12 - Abu Dhabi - United Arab Emirates","timestamp":"2020-09-28 13:58:06","sales_name":"SHABEER0503269103","description":"SHABEER0503269103 01:58 PM"},{"id":"295491","sales_id":"76","latitude":"25.0807498","longitude":"55.140515","location_name":"Dubai Marina - Dubai - United Arab Emirates","timestamp":"2020-09-28 13:23:43","sales_name":"MELINA0568870231","description":"MELINA0568870231 01:23 PM"},{"id":"295515","sales_id":"77","latitude":"25.1755256","longitude":"55.3871532","location_name":"Union Cooperative Society - Ras Al Khor Industrial AreaRas Al Khor Industrial Area 3 - Dubai - United Arab Emirates","timestamp":"2020-09-28 14:03:27","sales_name":"RUTCHEL0568870217","description":"RUTCHEL0568870217 02:03 PM"}]}';
}

function deliveryRadio1Chnage() {
    $ci = & get_instance();
    $delivery = !empty($_REQUEST['delivery']) ? $_REQUEST['delivery'] : '1';
    $ci->session->set_userdata('searchtype', $delivery);

    die(json_encode(array("status" => true, "message" => "successfully assign.")));
}

function distance($lat1, $lon1, $lat2, $lon2, $unit) {
    if (($lat1 == $lat2) && ($lon1 == $lon2)) {
        return 0;
    } else {


        $urllatlong = "origins=" . $lat1 . ',' . $lon1 . "&destinations=" . ($lat2 . ',' . $lon2);
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?mode=BICYCLING&sensor=false&$urllatlong&key=" . MAP_API_KEY;
        $response = file_get_contents($url);


        curl_close($auth);
        $response = json_decode($response, true);

        $duration = !empty($response['rows'][0]['elements'][0]['distance']['value']) ? $response['rows'][0]['elements'][0]['distance']['value'] : [];
        if (!empty($duration)) {
         
            return $duration / 1000;
        }






        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}
function distance22($lat1, $lon1, $lat2, $lon2, $unit) {
    if (($lat1 == $lat2) && ($lon1 == $lon2)) {
        return 0;
    } else {


        $urllatlong = "origins=" . $lat1 . ',' . $lon1 . "&destinations=" . ($lat2 . ',' . $lon2);
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?mode=BICYCLING&sensor=false&$urllatlong&key=" . MAP_API_KEY;
        $response = file_get_contents($url);

        echo $url;exit;

        curl_close($auth);
        $response = json_decode($response, true);

        echo '<pre>';
        print_r($response);
        exit;
        $duration = !empty($response['rows'][0]['elements'][0]['distance']['value']) ? $response['rows'][0]['elements'][0]['distance']['value'] : [];
        if (!empty($duration)) {
            return $duration / 1000;
        }






        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}
function distanceTime($lat1, $lon1, $lat2, $lon2, $unit) {
    if (($lat1 == $lat2) && ($lon1 == $lon2)) {
        return 0;
    } else {


        $urllatlong = "origins=" . $lat1 . ',' . $lon1 . "&destinations=" . ($lat2 . ',' . $lon2);
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?mode=BICYCLING&sensor=false&$urllatlong&key=" . MAP_API_KEY;
        $response = file_get_contents($url);


        curl_close($auth);
        $response = json_decode($response, true);

        $duration = !empty($response['rows'][0]['elements'][0]['duration']['value']) ? $response['rows'][0]['elements'][0]['duration']['value'] : [];
        if (!empty($duration)) {
            return $duration;
        }






        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        $returnVal = '';
        if ($unit == "K") {
            $returnVal = ($miles * 1.609344);
        } else if ($unit == "N") {
            $returnVal = ($miles * 0.8684);
        } else {
            $returnVal = $miles;
        }

        return (int) ($returnVal * RIDER_DELIVER_TIME_PER_KM) * 60;
    }
}

function uploadExcelMenu() {
    includePHPExcel();
    $db = LoadDB();
    $Session = GetSessionArrayVendor();
    $UploadExcel = empty($_FILES['UploadExcel']['error']) ? $_FILES['UploadExcel'] : [];
    $fileName = HOME_DIR . "images/" . uniqid() . '.' . pathinfo($UploadExcel['name'], PATHINFO_EXTENSION);

    if (move_uploaded_file($UploadExcel['tmp_name'], $fileName)) {
        $json = !empty($_POST['json']) ? $_POST['json'] : [];

        $objPHPExcel = new PHPExcel();
        $objPHPExcel = PHPExcel_IOFactory::load($fileName);


        $allData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        unlink($fileName);

        if (!empty($allData)) {
            $uploadUniqid = uniqid();
            for ($i = 2; $i <= count($allData); $i++) {
                $dr = $allData[$i];
                $category_name = !empty($dr['A']) ? $dr['A'] : '';
                $qry = "SELECT * FROM `category_details` WHERE category_name NOT LIKE 'Most Selling%' AND store_type='$Session->store_type' AND archive=0 AND category_name LIKE '$category_name'";
                $arr = $db->Database->select_qry_array($qry);
                $arr = !empty($arr) ? $arr[0] : '';

                $insert = array(
                    'vendor_id' => $Session->id,
                    'store_type' => $Session->store_type,
                    'category_id' => !empty($arr->id) ? $arr->id : '',
                    'menu_name' => !empty($dr['B']) ? $dr['B'] : '',
                    'menu_name_ar' => !empty($dr['C']) ? $dr['C'] : '',
                    'description' => !empty($dr['D']) ? $dr['D'] : '',
                    'description_ar' => !empty($dr['E']) ? $dr['E'] : '',
                    'price' => !empty($dr['F']) ? $dr['F'] : '',
                    'stock' => !empty($dr['G']) ? $dr['G'] : '',
                    'excel_upload_id' => $uploadUniqid,
                    'lastupdate' => date('Y-m-d H:i:s'),
                    'timestamp' => date('Y-m-d H:i:s'),
                );
                $db->Database->insert('menu_list', $insert);
            }
            die(json_encode(array('status' => true, 'message' => 'successfully upload', 'RedUrl' => base_url('vendor/menu'))));
        }
    }
}

function canUpdateorderStatus($orderId = '', $statusId = '') {
    $db = LoadDB();
    $qry = "SELECT *  FROM `order_status_history` WHERE `order_id` = '$orderId' AND status_id='$statusId'";
    $arr = $db->Database->select_qry_array($qry);
    if(!empty($arr)){
        return false;
    }else{
        return true;
    }
}
