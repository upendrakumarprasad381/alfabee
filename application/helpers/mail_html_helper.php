<?php

function send_email_for_registration($array = '') {
    $newHtml = '';
    $newHtml = $newHtml . '<p>Dear ' . ucfirst($array['name']) . ',</p>';
    $newHtml = $newHtml . '<p>Your registration is successful.</p>';
    $newHtml = $newHtml . '<p>Name: ' . (!empty($array['name']) ? $array['name'] : '') . '</p>';
    $newHtml = $newHtml . '<p>Email: ' . (!empty($array['email']) ? $array['email'] : '') . '</p>';
    $newHtml = $newHtml . '<p>Phone: ' . (!empty($array['mobile_number']) ? $array['mobile_number'] : '') . '</p>';
    $newHtml = $newHtml . '<p><a type="button" href="' . base_url('login_signup') . '" style="text-decoration:none;">Click Here To Login</a></p>';
    $newHtml = emailTemplate($newHtml);

    $html464 = "";
    $html464 = $html464 . "<p>Dear Team,</p>";
    $html464 = $html464 . "<p>New user registered.</p>";
    $html464 = $html464 . "<p>Name: " . (!empty($array['name']) ? $array['name'] : '') . "</p>";
    $html464 = $html464 . '<p>Email: ' . (!empty($array['email']) ? $array['email'] : '') . '</p>';
    $html464 = emailTemplate($html464);


    $subject = 'Registration';
    send_mail($array['email'], $subject, $newHtml);
    send_mail(ADMIN_EMAIL_ID, $subject, $html464);
}

function send_email_for_vendor_registration($array = '') {

    $newHtml = '';
    $newHtml = $newHtml . '<p>Dear ' . ucfirst($array['name']) . ',</p>';
    $newHtml = $newHtml . '<p>Thanks for your interest in joining Alfabee!.</p>';
    $newHtml = $newHtml . '<p>Our team will assess your application and get back to you .</p>';

    $newHtml = emailTemplate($newHtml);
    $subject = 'Thank your for your registration';
    send_mail($array['email'], $subject, $newHtml);
}

function send_email_forgotpassord($name = '', $otp, $mail) {

    $newHtml = '';
    $newHtml = $newHtml . '<p>Dear ' . ucfirst($name) . ',</p>';
    $newHtml = $newHtml . '<p>You have one new OTP code :' . $otp . '</p>';
    $newHtml = emailTemplate($newHtml);
    $subject = "AlfaBee OTP code";
    send_mail($mail, $subject, $newHtml);
}

function send_email_to_vendor_after_approve($array = '') {
    $newHtml = '';
    $newHtml = $newHtml . '<p>Dear ' . ucfirst($array[0]->restaurant_name) . ',</p>';
    $newHtml = $newHtml . '<p>Your account is activated .</p>';
    $newHtml = $newHtml . '<p><a type="button" href="' . base_url('vendor') . '" style="text-decoration:none;">Click Here To Login</a></p>';
    $newHtml = emailTemplate($newHtml);


    $subject = 'Your account is activated';
    send_mail($array[0]->email, $subject, $newHtml);
}

function send_email_for_registration_rider($array = '') {
    $newHtml = '';
    $newHtml = $newHtml . '<p>Dear Team,</p>';
    $newHtml = $newHtml . '<p>New rider registration.</p>';
    $newHtml = $newHtml . '<p>Rider Name: ' . (!empty($array['rider_name']) ? $array['rider_name'] : '') . '</p>';
    $newHtml = $newHtml . '<p>NIC: ' . (!empty($array['nic']) ? $array['nic'] : '') . '</p>';
    $newHtml = $newHtml . '<p>Mobile Number: ' . (!empty($array['mobile_number']) ? $array['mobile_number'] : '') . '</p>';
    $newHtml = $newHtml . '<p>Email Id: ' . (!empty($array['email_id']) ? $array['email_id'] : '') . '</p>';
    $newHtml = emailTemplate($newHtml);


    $subject = 'Rider Registration';
    send_mail(ADMIN_EMAIL_ID, $subject, $newHtml);
}

function send_email_for_password_change($array = '') {
    $newHtml = '';
    $newHtml = $newHtml . '<p>Dear ' . ucfirst($array['name']) . ', </p>';
    $newHtml = $newHtml . '<p>Your password has been changed successfully.</p>';
    $newHtml = $newHtml . '<p>Please login with your new credentials.</p>';

    $newHtml = emailTemplate($newHtml);

    $subject = 'Password Change';
    send_mail($array['email'], $subject, $newHtml);
}

function send_email_for_forgot_password($id = '', $email = '') {
    $name = explode("@", $email);

    $newHtml = '';
    $newHtml = $newHtml . '<p>Dear ' . ucfirst($name[0]) . ', </p>';
    $newHtml = $newHtml . '<p><a href="' . base_url() . 'resetpassword/' . base64_encode($id) . '">Click here to reset your password </a></p>';


    $newHtml = emailTemplate($newHtml);

    $subject = 'Reset Password';
    send_mail($email, $subject, $newHtml);
}

function send_email_for_order($id = '') {
    return true;
    $order = GetOrderById($id);
    if (!empty($order)) {
        $userdetails = GetUserDetails($order[0]->user_id);
        $rest_details = GetRestaurantById($order[0]->vendor_id);
        $saved_address = GetOrderAddressById($order[0]->delivery_address);
    }
    $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Mail</title>
</head><body style="    
        background: #e6e6e6;
        padding:0;
        margin:0 auto;
	  "> 
      <div style="   
       background: #e6e6e6;
        padding:0;
        width:100%;
        height:auto;
        margin:0 auto;
        ">
<table width="650" border="0" align="center" cellpadding="0" cellspacing="0" style="background:#fff;">
 
  <tr>
    <td height="150" align="center" valign="middle"> <img src="' . base_url() . 'images/logo.png" /> </td>
  </tr>
  <tr>
    <td style="    padding-left: 15px;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    background: #fff3e6;
    padding: 20px;" >
    <h4 style="margin:0px; font-size:18px;">' . $userdetails[0]->name . '</h4>
    <p style="margin:0;">Thanks for your order!</p>
    </td>
  </tr>
  <tr>
    <td height="50" align="center" valign="middle"  style="
        padding: 0px 20px;
        color: #f78f1e;
        font-family: Arial, Helvetica, sans-serif;
     "><h3 style="font-size:16px;">Your order from ' . $rest_details[0]->restaurant_name . '</h3>
     </td>
  </tr>
  <tr>
    <td  style="
        padding: 0px 20px 20px;
        color: #f78f1e;
        font-family: Arial, Helvetica, sans-serif;
        
        border-bottom:#ccc 3px solid;
     ">
     
     <table width="100%" border="0" cellspacing="0" cellpadding="0" style=" 
     font-family: Arial, Helvetica, sans-serif;
     font-size:14px;
     color:#000; 
      ">';
    foreach ($order as $ord) {
        $html .= '<tr>
    <td height="25" align="left" valign="middle">' . $ord->product_name . ' (' . $ord->quantity . ')</td>
    <td height="25" align="right" valign="middle">' . number_format($ord->subtotal, 2) . '</td>
  </tr>';
    }
// <tr>
//     <td height="25" align="left" valign="middle">Delivery Charge</td>
//     <td height="25" align="right" valign="middle">'.number_format($rest_details[0]->service_charge,2).'</td>
//   </tr>

    $html .= '<tr>
    <td height="25" align="left" valign="middle">&nbsp;</td>
    <td height="25" align="right" valign="middle">&nbsp;</td>
  </tr>
  
  <tr>
    <td height="25" align="left" valign="middle">Discount Amount</td>
    <td height="25" align="right" valign="middle">' . number_format($order[0]->discount, 2) . '</td>
  </tr>
  <tr>
    <td height="25" align="left" valign="middle">Total</td>
    <td height="25" align="right" valign="middle"><strong>PKR  ' . number_format($ord->total_price, 2) . '</strong></td>
  </tr>
     </table>

     </td>
  </tr>
  <tr>
    <td  style="
        padding: 30px 20px;
        color: #7b7b7b;
        text-align:center;
        font-size:12px;
        font-family: Arial, Helvetica, sans-serif;
     ">Your order ID: ' . $order[0]->orderno . '<br />';
    if ($saved_address[0]->address_label == 1) {
        $buil_name = $saved_address[0]->building;
        $floor = $saved_address[0]->floor;
    }if ($saved_address[0]->address_label == 2) {
        $buil_name = $saved_address[0]->house;
        $floor = '';
    } else {
        $buil_name = $saved_address[0]->building;
        $floor = $saved_address[0]->floor;
    }
    $street = $saved_address[0]->street;

    $html .= 'Delivering to: ' . $saved_address[0]->address . ',' . $street . ',' . $buil_name . ',' . $floor . '<br />
      Time of order: ' . $order[0]->date . '<br />';

    if ($order[0]->mode_of_payment == 1) {
        $mode = 'Credit Card';
    } else {
        $mode = 'Cash';
    }
    $html .= 'Paid by: ' . $mode;
    $html .= '</td>
  </tr>
  <tr>
    <td  align="center" valign="middle"  style="
        color: #000;
        font-family: Arial, Helvetica, sans-serif;
        
     ">
     <table width="100%" border="0" cellspacing="0" cellpadding="0"  >
      <tr>
         <td height="70" align="right" valign="top" style="padding-right:10px;"><a href="#" target="_blank"><img width="180"  src="http://demo.softwarecompany.ae/home_eats_design/html/images/ios.png" border="0" /></a></td>
         <td align="left" valign="top"  style="padding-left:10px;"><a href="#" target="_blank"><img width="180" src="http://demo.softwarecompany.ae/home_eats_design/html/images/play.png" border="0" /></a></td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td height="70" align="center" valign="middle"  style=" 
    	border-top:#ccc 1px solid ;
 
        
    padding: 0px 20px; ">
    <a href="#"><img src="http://demo.softwarecompany.ae/home_eats_design/html/images/m_facebook.png" /></a>
    <a href="#"><img src="http://demo.softwarecompany.ae/home_eats_design/html/images/m_twitter.png" /></a>
    <a href="#"><img src="http://demo.softwarecompany.ae/home_eats_design/html/images/m_linkedin.png" /></a>
    <a href="#"><img src="http://demo.softwarecompany.ae/home_eats_design/html/images/m_youtube.png" /></a>
        <a href="#"><img src="http://demo.softwarecompany.ae/home_eats_design/html/images/m_instagram.png" /></a>
     </td>
  </tr>
 
</table>

</div>
</body>
</html>';

    $subject = 'Your order from ' . $rest_details[0]->restaurant_name;
    send_mail($userdetails[0]->email, $subject, $html);
}

function sendContactusEmail($contactId) {
    $array = GetcontactusBy($contactId);
    $html464 = "";
    $html464 = $html464 . "<p>Dear Team,</p>";
    $html464 = $html464 . "<p>You have one new enquiry.</p>";
    $html464 = $html464 . "<p>Name: " . (!empty($array->name) ? $array->name : '') . "</p>";
    $html464 = $html464 . '<p>Email: ' . (!empty($array->email) ? $array->email : '') . '</p>';
    $html464 = $html464 . '<p>Phone: ' . (!empty($array->mobile_number) ? $array->mobile_number : '') . '</p>';
    $html464 = $html464 . '<p>Message: ' . (!empty($array->message) ? $array->message : '') . '</p>';
    $html464 = emailTemplate($html464);
    $subject = 'New Enquiry';
    send_mail('info@alfabee.pk', $subject, $html464);
}

function send_email_for_order_to_Vendor($id = '') {
    return true;
    $order = GetOrderById($id);
    if (!empty($order)) {
        $userdetails = GetUserDetails($order[0]->user_id);
        $rest_details = GetRestaurantById($order[0]->vendor_id);
        $saved_address = GetOrderAddressById($order[0]->delivery_address);
        $restaurant = GetRestaurantDetails($order[0]->vendor_id);
    }
    $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Mail</title>
</head><body style="    
        background: #e6e6e6;
        padding:0;
        margin:0 auto;
	  "> 
      <div style="   
       background: #e6e6e6;
        padding:0;
        width:100%;
        height:auto;
        margin:0 auto;
        ">
<table width="650" border="0" align="center" cellpadding="0" cellspacing="0" style="background:#fff;">
 
  <tr>
    <td height="150" align="center" valign="middle"> <img src="' . base_url() . 'images/logo.png" /> </td>
  </tr>
  <tr>
    <td style="    padding-left: 15px;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    background: #fff3e6;
    padding: 20px;" >
    <h4 style="margin:0px; font-size:18px;">' . $rest_details[0]->restaurant_name . '</h4>
    <p style="margin:0;">New order from ' . $userdetails[0]->name . '</p>
    </td>
  </tr>
 
  <tr>
    <td  style="
        padding: 0px 20px 20px;
        color: #f78f1e;
        font-family: Arial, Helvetica, sans-serif;
        
        border-bottom:#ccc 3px solid;
     ">
     
     <table width="100%" border="0" cellspacing="0" cellpadding="0" style=" 
     font-family: Arial, Helvetica, sans-serif;
     font-size:14px;
     color:#000; 
      ">';
    foreach ($order as $ord) {

        $html .= '<tr>
    <td height="25" align="left" valign="middle">' . $ord->product_name . ' (' . $ord->quantity . ')</td>
    <td height="25" align="right" valign="middle">' . number_format($ord->subtotal, 2) . '</td>
  </tr>';
    }
//   <tr>
//     <td height="25" align="left" valign="middle">4 For 100 Dhs (1)</td>
//     <td height="25" align="right" valign="middle">100.00</td>
//   </tr>
//   <tr>
//     <td height="25" align="left" valign="middle">Delivery Charge</td>
//     <td height="25" align="right" valign="middle">'.number_format($rest_details[0]->service_charge,2).'</td>
//   </tr>
    $html .= '<tr>
    <td height="25" align="left" valign="middle">&nbsp;</td>
    <td height="25" align="right" valign="middle">&nbsp;</td>
  </tr>

  <tr>
    <td height="25" align="left" valign="middle">Discount Amount</td>
    <td height="25" align="right" valign="middle">' . number_format($order[0]->discount, 2) . '</td>
  </tr>
  <tr>
    <td height="25" align="left" valign="middle">Delivery Fee</td>
    <td height="25" align="right" valign="middle">' . number_format($order[0]->service_charge, 2) . '</td>
  </tr>
  <tr>
    <td height="25" align="left" valign="middle">Total</td>
    <td height="25" align="right" valign="middle"><strong>PKR ' . number_format($ord->total_price + + $order[0]->service_charge, 2) . '</strong></td>
  </tr>
     </table>

     </td>
  </tr>
  <tr>
    <td  style="
        padding: 30px 20px;
        color: #7b7b7b;
        text-align:center;
        font-size:12px;
        font-family: Arial, Helvetica, sans-serif;
     ">Order ID: ' . $order[0]->orderno . '<br />';
    if ($saved_address[0]->address_label == 1) {
        $buil_name = $saved_address[0]->building;
        $floor = $saved_address[0]->floor;
    }if ($saved_address[0]->address_label == 2) {
        $buil_name = $saved_address[0]->house;
        $floor = '';
    } else {
        $buil_name = $saved_address[0]->building;
        $floor = $saved_address[0]->floor;
    }
    $street = $saved_address[0]->street;

    $html .= 'Delivering to: ' . $saved_address[0]->address . ',' . $street . ',' . $buil_name . ',' . $floor . '<br />
      Time of order: ' . $order[0]->date . '<br />';

    if ($order[0]->mode_of_payment == 1) {
        $mode = 'Credit Card';
    } else {
        $mode = 'Cash';
    }
    $html .= 'Paid by: ' . $mode;
    $html .= '</td>
  </tr>
  
</table>

</div>
</body>
</html>';

    $subject = 'New order from ' . $userdetails[0]->name;
    send_mail($restaurant[0]->email, $subject, $html);
}

function emailTemplate($message = '') {

    $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <title></title>
    </head> 
    <body>
    <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="border-right:solid 1px #ccc;border-left:solid 1px #ccc;border-top:solid 1px #f9a01b;border-bottom:solid 1px #f9a01b;font-size:15px;">
    <tbody><tr>
    <td style="border-top:solid 5px #f9a01b"><table width="94%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Arial,Helvetica,sans-serif;font-size:15px;color:#333;line-height:18px">
    <tbody><tr>
    <td  align="left" valign="top" style="padding:15px 0px;"><img src="' . base_url() . 'images/logo.png" alt="" style="width:150px;padding-top:10px" class="CToWUd"></td>
    </tr>
    <tr>
    <td style="border-bottom: solid 3px #eee;"><strong><br/>
    </strong></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>
  ' . $message . '
     </td></tr>
   
    <tr>
    <td>&nbsp;</td>
    </tr>
    <tr><td ><strong>Alfabee Team</strong></td></tr>
    <tr>
    <td>&nbsp;</td>
    </tr>
    </tbody></table>
    </td>
    </tr>       
    <tr>
    <td style="padding:5px 0;text-align:center;background: #f9a01b;color: #000;font-size:11px;font-family:Arial,Helvetica,sans-serif;">Copyright &copy; ' . date("Y", time()) . ' Alfabee . All rights reserved.</td>
    </tr>
    </tbody></table></body>
    </html>';
    return $html;
}

function loadFrontendMenulistHTML() {
    error_reporting(E_ERROR | E_WARNING | E_PARSE);
    $db = LoadDB();
    $ci = & get_instance();
    $catId = !empty($_POST['catId']) ? $_POST['catId'] : '';
    $vendorId = !empty($_POST['vendorId']) ? $_POST['vendorId'] : '';
    $language = !empty($ci->session->userdata('language')) ? $ci->session->userdata('language') : '';

    $get_restaurant_details = GetRestaurantDetails($vendorId);
    $session_cart = $ci->session->userdata('CartData');

    $store_typeUp = !empty($get_restaurant_details[0]->store_type) ? $get_restaurant_details[0]->store_type : '0';
    $CategoryArray = listMenuByRestaurant($vendorId, $catId);

    foreach ($CategoryArray as $menu_list) {
        $menu_list->id = trim($menu_list->id);

        $menu_name = $language == 'ar' && !empty($menu_list->menu_name_ar) ? $menu_list->menu_name_ar : $menu_list->menu_name;
        $menudesc = $language == 'ar' && !empty($menu_list->description_ar) ? $menu_list->description_ar : $menu_list->description;


        $image = 'uploads/menu/' . $menu_list->image;




        $opening_time = date('H:i', strtotime($get_restaurant_details[0]->opening_time));
        $closing_time = date('H:i', strtotime($get_restaurant_details[0]->closing_time));
        $currentTime = date('H:i', time());
        if ($currentTime > $opening_time && $get_restaurant_details[0]->busy_status == 1) {
            $busy = 'data-busy=1';
        } else {
            $busy = '';
        }

        $close = '';
        if ($store_typeUp == '1') {
            if ($currentTime < $opening_time) {
                $close = 'data-close=1';
            }
        }
        ?>

        <div class="wd100 __itm_crtwp">
            <?php
            $style = '';
            if ($get_restaurant_details[0]->store_type != 1 && $menu_list->stock == 0) {
                $style = "pointer-events: none";
            }

            $arr1 = array();
            $class = '';
            if (!empty($session_cart) && $session_cart[0]['party'] == 0) {

                for ($i = 0; $i < count($session_cart); $i++) {
                    array_push($arr1, trim($session_cart[$i]['menu_id']));
                }
                $menu_val = (int) trim($menu_list->id);

                if (in_array($menu_val, $arr1)) {

                    foreach ($session_cart as $d) {
                        if ($d['menu_id'] == $menu_val) {

                            $class = '';
                        }
                    }
                } else {
                    if ($menu_list->choice != 1) {
                        $class = 'add_to_cart';
                    } else {
                        $class = 'sub_cat';
                    }
                }
            } else {
                if ($menu_list->choice != 1) {
                    $class = 'add_to_cart';
                } else {
                    $class = 'sub_cat';
                }
            }
            ?> 
            <div id="menuIdse<?= $menu_list->id ?>" class="media <?= $class ?>" style="<?= $style ?>" data-category="<?= $menu_list->category_id; ?>" data-party="0" data-id="<?= $menu_list->id; ?> " data-choice="<?= $menu_list->choice ?>" data-vendor="<?= $get_restaurant_details[0]->vendor_id ?>" data-sessionparty="<?= !empty($session_cart) ? $session_cart[0]['party'] : '' ?>" data-sessionvendor="<?= !empty($session_cart) ? $session_cart[0]['vendor_id'] : '' ?>" <?= $busy ?> <?= $close ?>> 

                <?php
                if ($get_restaurant_details[0]->store_type != 1) {
                    if ($menu_list->stock == 0) {
                        $style = "pointer-events: none";
                        ?>
                        <div class="__outofstock">
                            Out of Stock	
                        </div>
                        <?php
                    }
                }
                ?>
                <?php
                if (isset($menu_list->image) && $menu_list->image != '') {
                    ?>
                    <img class="__itm_crtimg" data-toggle="popover-hover" data-placement="top" data-img="<?= base_url($image); ?>" src="<?= base_url($image); ?>">
                    <?php
                } else {
                    ?>
                    <img class="__itm_crtimg" src="<?= base_url(DEFAULT_LOGO_MENU); ?>">
                    <?php
                }
                ?>
                <div class="media-body">
                    <h3 trrr="ff"><?= $menu_name; ?></h3>
                    <div class="wd100 __ctdcrp">
                        <p><?= $menudesc; ?></p>
                    </div>
                    <?php
                    $menu_id = explode(',', $menu_list->menu_id);
                    $prc = getMenudiscountArray($menuId = $menu_list->id, $menu_list->price, $vendorId);

                    if ($menu_list->price != 0 && !empty($prc['total_discount'])) {
                        ?>
                        <div fff class="wd100 __offer">
                            <p><?= $language == 'ar' ? 'بچت' : 'Save' ?> <?php echo $ci->lang->line("aed"); ?> <?= DecimalAmount($prc['total_discount']) ?></p>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-4 _cartcm">
                    <div class="row">
                        <div class="col p-0">
                            <?php
                            $menu_id = explode(',', $menu_list->menu_id);

                            if ($menu_list->price != 0) {
                                if (!empty($prc['cross_price'])) {
                                    ?>
                                    <div class="wd100 _cartpris"  cross_price="<?= $prc['cross_price'] ?>" final_price="<?= $prc['final_price'] ?>" cross style="text-decoration: line-through"> <?php echo $ci->lang->line("aed"); ?> <?= DecimalAmount($prc['cross_price']); ?> </div>
                                <?php } ?>
                                <div class="wd100 _cartpris" tff="<?= $prc['final_price'] ?>"> <?php echo $ci->lang->line("aed"); ?> <?= DecimalAmount($prc['final_price']); ?> </div>
                                <?php
                            } else {
                                ?>
                                <div class="wd100 _cartpris" class="option_product" ><?php echo $ci->lang->line("price_on_selection"); ?></div>
                                <?php
                            }
                            ?>

                        </div>
                        <div class="col-5 p-0">
                            <?php
                            $isaddedcart = false;
                            $cartIds773 = '';
                            $cartQty773 = '1';
                            if (!empty($session_cart) && $session_cart[0]['party'] == 0) {
                                $arr = array();
                                for ($i = 0; $i < count($session_cart); $i++) {
                                    array_push($arr, $session_cart[$i]['menu_id']);
                                }
                                $menu_val = (int) trim($menu_list->id);

                                if (in_array($menu_val, $arr)) {
                                    foreach ($session_cart as $d) {
                                        if ($d['menu_id'] == $menu_val) {

                                            $cartIds773 = !empty($d['cart_id']) ? $d['cart_id'] : '';
                                            $cartQty773 = !empty($d['quantity']) ? $d['quantity'] : $cartQty773;
                                            $isaddedcart = true;
                                        }
                                    }
                                }
                            }
                            ?>

                            <div class="__pqty XXXXremove_cart_product"  data-id="<?= $menu_list->id ?>" data-cart="<?= $cartIds773 ?>" id="addedBtnId<?= $menu_list->id ?>" style="display: <?= !empty($isaddedcart) ? 'block' : 'none' ?>;">
                                <!--<div class="__crtbtn addedlist7353"  > <php echo $ci->lang->line("added"); ?></div>-->
                                <section class="qty-update-section incart b-a menu-class-<?= $menu_list->id ?>">
                                    <input type="hidden" value="<?= $cartIds773 ?>" class="cart_id-menu-list-<?= $menu_list->id ?>">
                                    <button type="button" class="btn btn-sm b-r " style=""   onclick="updateQty(this)" data-id="<?= $menu_list->id ?>" data-cart="<?= $cartIds773 ?>"  data-type="minus" data-value="<?= $cartQty773 ?>">
                                        <i class="fa fa-minus orange"></i>
                                    </button>

                                    <span class="f-11" data-id="<?= $menu_list->id ?>" id="quantity"><b><qtymenu-<?= $menu_list->id ?>><?= $cartQty773 ?></qtymenu-<?= $menu_list->id ?>></b></span>
                                    <button type="button" class="btn btn-sm b-l" onclick="updateQty(this)" data-id="<?= $menu_list->id ?>" data-cart="<?= $cartIds773 ?>"  data-type="plus" data-value="<?= $cartQty773 ?>">
                                        <i class="fa fa-plus orange"></i>
                                    </button>
                                </section>

                            </div>
                            <menubtnaddId<?= $menu_list->id ?> style="display: <?= empty($isaddedcart) ? 'block' : 'none' ?>;">
                                <div  class="__crtbtn"  data-category="<?= $menu_list->category_id; ?>" data-id="<?= $menu_list->id; ?>" data-party="0" data-choice="<?= $menu_list->choice ?>" data-vendor="<?= $get_restaurant_details[0]->vendor_id ?>" data-sessionparty="<?= !empty($session_cart) ? $session_cart[0]['party'] : '' ?>" data-sessionvendor="<?= !empty($session_cart) ? $session_cart[0]['vendor_id'] : '' ?>" <?= $busy ?> <?= $close ?>>  
                                    <?php echo $ci->lang->line("ADD"); ?> <i class="fas fa-plus"></i> </div>
                            </menubtnaddId<?= $menu_list->id ?>>
                            <?php
                            if (!empty($session_cart) && $session_cart[0]['party'] == 0) {
                                $arr = array();
                                for ($i = 0; $i < count($session_cart); $i++) {
                                    array_push($arr, $session_cart[$i]['menu_id']);
                                }
                                $menu_val = (int) trim($menu_list->id);

                                if (in_array($menu_val, $arr)) {

                                    $groupedItems = array();
                                    $out = array();
                                    foreach ($session_cart as $item) {
                                        $groupedItems[$item['menu_id']][] = $item;
                                    }
                                    $groupedItems = array_values($groupedItems);
                                    foreach ($groupedItems as $as) {
                                        foreach ($as as $ad) {
                                            if ($ad['choice'] == 1 && $menu_val == $ad['menu_id']) {
                                                $out[] = $ad['quantity'];
                                            }
                                        }
                                    }
                                    $sum_qty = array_sum($out);
                                    ?>



                                    <div class="modal fade" id="cartPopupModal_<?= $menu_val ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenter" aria-modal="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">

                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5>Remove your items</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                                </div>
                                                <div class="modal-body">
                                                    <input type="hidden" value="" id="menu_val" name="menu_val">
                                                    <?php
                                                    foreach ($groupedItems as $as) {
                                                        foreach ($as as $ad) {

                                                            if (isset($ad['size'])) {
                                                                //echo ',';
                                                                $array = array_column($ad['size'], 'name');
                                                                $array = implode(',', $array);
                                                                //   $array = end($array);
                                                            } else {
                                                                $array = '';
                                                            }
                                                            if (isset($ad['addon'])) {
                                                                //echo ',';
                                                                $array1 = array_column($ad['addon'], 'name');
                                                                $array1 = implode(',', $array1);
                                                            } else {
                                                                $array1 = '';
                                                            }
                                                            if (isset($ad['topping'])) {   //echo ',';
                                                                $array2 = array_column($ad['topping'], 'name');
                                                                $array2 = implode(',', $array2);
                                                            } else {
                                                                $array2 = '';
                                                            }
                                                            if (isset($ad['drink'])) {
                                                                //echo ',';
                                                                $array3 = array_column($ad['drink'], 'name');
                                                                $array3 = implode(',', $array3);
                                                            } else {
                                                                $array3 = '';
                                                            }
                                                            if (isset($ad['dip'])) {
                                                                //echo ',';
                                                                $array4 = array_column($ad['dip'], 'name');
                                                                $array4 = implode(',', $array4);
                                                            } else {
                                                                $array4 = '';
                                                            }
                                                            if (isset($ad['side'])) {
                                                                $array5 = array_column($ad['side'], 'name');
                                                                $array5 = implode(',', $array5);
                                                            } else {
                                                                $array5 = '';
                                                            }
                                                            // $options = $array.$array1.$array2.$array3.$array4.$array5;
                                                            $options = implode(",", array_filter([$array, $array1, $array2, $array3, $array4, $array5]))
                                                            ?>
                                                            <table class="table __crtb">
                                                                <tbody><tr>
                                                                        <?php
                                                                        if ($ad['choice'] == 1 && $menu_val == $ad['menu_id']) {
                                                                            ?>

                                                                            <td class="__itxtt"><?= $ad['menu_name'] ?>
                                                                                <br><span><?= $options ?></span></td>
                                                                            <td class="qtytd">

                                                                                <section class="qty-update-section incart b-a">
                                                                                    <input type="hidden" value="<?= $get_restaurant_details[0]->store_type ?>" id="store_type">
                                                                                    <button type="button" class="btn btn-sm b-r"  onclick="updateQty(this)" data-id="<?= $ad['menu_id'] ?>" data-cart="<?= $ad['cart_id'] ?>" data-type="minus" data-value="<?= $ad['quantity'] ?>">
                                                                                        <i class="fa fa-minus orange"></i>
                                                                                    </button>

                                                                                    <span class="f-11" data-id="<?= $ad['menu_id'] ?>" id="quantity"><b><?= $ad['quantity'] ?></b></span>
                                                                                    <button type="button" class="btn btn-sm b-l" onclick="updateQty(this)" data-id="<?= $ad['menu_id'] ?>" data-cart="<?= $ad['cart_id'] ?>" data-type="plus" data-value="<?= $ad['quantity'] ?>">
                                                                                        <i class="fa fa-plus orange"></i>
                                                                                    </button>
                                                                                </section>

                                                                            </td>
                                                                            <td class="__crpc"><?= DecimalAmount($ad['subtotal']) ?></td>
                                                                            <td>
                                                                                <a class="__close remove_cart_product" href="" data-id="<?= $ad['menu_id'] ?>" data-cart="<?= $ad['cart_id'] ?>"><i class="fas fa-times-circle"></i></a>
                                                                            </td>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </tr>


                                                                </tbody></table>
                                                            <?php
                                                        }
                                                    }
                                                    ?>


                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            if ($menu_list->choice == '1') {
                                echo '<span class="have_options">customizable</span>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <?php
    }
}

function getmycartHTMLWebsite() {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    $db = LoadDB();
    $session_cart = $db->session->userdata('CartData');
    $lan = getsystemlanguage();
    $session_arr = $db->session->userdata('UserLogin');
    $RedirectUrl = base_url('checkout');
    $OnclickUrl = empty($session_arr) ? "LoginAndCallBack('$RedirectUrl')" : "window.location = '$RedirectUrl'";
    $restaurantId = !empty($_REQUEST['restaurantId']) ? $_REQUEST['restaurantId'] : '';

    $get_restaurant_details = GetRestaurantDetails($restaurantId);
    $encode = !empty($_POST['encode']) ? $_POST['encode'] : '';
    if (!empty($encode)) {
        ob_start();
    }
    if (!empty($session_cart) && count($session_cart) > 0) {
        $Qry = "SELECT restaurant_name  FROM `restaurant_details` WHERE vendor_id='" . $session_cart[0]['vendor_id'] . "'";

        $Array = $db->Database->select_qry_array($Qry);
        ?>
        <div class="wd100 __wtbx __cartsummy"> 
            <h2><?php echo $db->lang->line("your_cart"); ?></h2>

            <h5><a href="#">
                    <?php
                    if ($lan == 'ar' && !empty($get_restaurant_details[0]->restaurant_name_ar)) {
                        echo $get_restaurant_details[0]->restaurant_name_ar;
                    } else {
                        echo (!empty($Array[0]->restaurant_name) ? ucwords($Array[0]->restaurant_name) : '');
                    }
                    ?>
                </a></h5>


            <div class="__cartsummytabv wd100 cart_summary">

                <table class="table __crtb">
                    <?php
                    $subtotal = 0;

                    foreach ($session_cart as $cart) {
                        $menu_idTrimp = trim($cart['menu_id']);
                        if ($cart['party'] == 0) {
                            $Qry1 = "SELECT *  FROM `menu_list` WHERE id='" . $menu_idTrimp . "'";

                            $Array1 = $db->Database->select_qry_array($Qry1);

                            if ($db->session->userdata('language') == 'ar') {
                                if ($Array1[0]->menu_name_ar != '') {
                                    $menuname = $Array1[0]->menu_name_ar;
                                } else {
                                    $menuname = $cart['menu_name'];
                                }
                            } else {
                                $menuname = $cart['menu_name'];
                            }
                            ?>
                            <tr>
                                <td class="__itxtt"><?= $menuname ?> 
                                    <?php
                                    if (isset($cart['size'])) {
                                        //echo ',';
                                        $array = array_column($cart['size'], 'name');
                                        // 	$array = end($array);
                                        $array = implode(',', $array);
                                    } else {
                                        $array = '';
                                    }
                                    if (isset($cart['addon'])) {
                                        //echo ',';
                                        $array1 = array_column($cart['addon'], 'name');
                                        $array1 = implode(',', $array1);
                                    } else {
                                        $array1 = '';
                                    }
                                    if (isset($cart['topping'])) {   //echo ',';
                                        $array2 = array_column($cart['topping'], 'name');
                                        $array2 = implode(',', $array2);
                                    } else {
                                        $array2 = '';
                                    }
                                    if (isset($cart['drink'])) {
                                        //echo ',';
                                        $array3 = array_column($cart['drink'], 'name');
                                        $array3 = implode(',', $array3);
                                    } else {
                                        $array3 = '';
                                    }
                                    if (isset($cart['dip'])) {
                                        //echo ',';
                                        $array4 = array_column($cart['dip'], 'name');
                                        $array4 = implode(',', $array4);
                                    } else {
                                        $array4 = '';
                                    }
                                    if (isset($cart['side'])) {
                                        $array5 = array_column($cart['side'], 'name');
                                        $array5 = implode(',', $array5);
                                    } else {
                                        $array5 = '';
                                    }
                                    // $options = $array.$array1.$array2.$array3.$array4.$array5;
                                    $options = implode(",", array_filter([$array, $array1, $array2, $array3, $array4, $array5]));
                                    ?>
                                    <?php
                                    if ($cart['choice'] == 1 && !empty($options)) {
                                        ?>
                                        <i class="fa fa-info-circle hidden-xs" data-toggle="tooltip" title="<?= $options ?>"></i>
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td class="qtytd">

                                    <section class="qty-update-section incart b-a menu-class-<?= $menu_idTrimp ?>">
                                        <input type="hidden" value="<?= $cart['cart_id'] ?>" class="cart_id-cart-list-<?= $menu_idTrimp ?>">
                                        <button type="button" class="btn btn-sm b-r " style=""   onclick="updateQty(this)" data-id="<?= $menu_idTrimp ?>" data-cart="<?= $cart['cart_id'] ?>"  data-type="minus" data-value="<?= $cart['quantity'] ?>">
                                            <i class="fa fa-minus orange"></i>
                                        </button>

                                        <span class="f-11 cartlistqty" data-id="<?= $menu_idTrimp ?>" ><b><qtymenu-<?= $menu_idTrimp ?>><?= $cart['quantity'] ?></qtymenu-<?= $menu_idTrimp ?>></b></span>
                                        <button type="button" class="btn btn-sm b-l" onclick="updateQty(this)" data-id="<?= $menu_idTrimp ?>" data-cart="<?= $cart['cart_id'] ?>"  data-type="plus" data-value="<?= $cart['quantity'] ?>">
                                            <i class="fa fa-plus orange"></i>
                                        </button>
                                    </section>

                                </td>
                                <td class="__crpc"><?= DecimalAmount($cart['subtotal']) ?></td>
                                <td>
                                    <a class="__close remove_cart_product" id="menuremoveId-<?= $menu_idTrimp ?>" href="javascript:void(0)" data-id="<?= $menu_idTrimp ?>" data-cart="<?= $cart['cart_id'] ?>"><i class="fas fa-times-circle"></i></a>
                                </td>
                            </tr>


                            <?php
                            $cart['price'] = DecimalAmount($cart['price']);
                            $subtotal += $cart['price'] * $cart['quantity'];
                        } else {
                            $Qry1 = "SELECT *  FROM `party_order_menu` WHERE id='" . $menu_idTrimp . "'";

                            $Array1 = $db->Database->select_qry_array($Qry1);

                            if ($db->session->userdata('language') == 'ar') {
                                if ($Array1[0]->menu_name_ar != '') {
                                    $menuname = $Array1[0]->menu_name_ar;
                                } else {
                                    $menuname = $cart['menu_name'];
                                }
                            } else {
                                $menuname = $cart['menu_name'];
                            }
                            ?>
                            <tr>
                                <td class="__itxtt"><?= $menuname ?> 

                                </td>
                                <td class="qtytd">

                                    <section class="qty-update-section incart b-a">
                                        <input type="hidden" value="<?= $get_restaurant_details[0]->store_type ?>" id="store_type">
                                        <button type="button" class="btn btn-sm b-r" onclick="updateQty(this)" data-id="<?= $menu_idTrimp ?>" data-cart="<?= $cart['cart_id'] ?>"  data-type="minus" data-value="<?= $cart['quantity'] ?>">
                                            <i class="fa fa-minus orange"></i>
                                        </button>

                                        <span class="f-11" data-id="<?= $menu_idTrimp ?>" id="quantity"><b><?= $cart['quantity'] ?></b></span>
                                        <button type="button" class="btn btn-sm b-l" onclick="updateQty(this)" data-id="<?= $menu_idTrimp ?>" data-cart="<?= $cart['cart_id'] ?>"  data-type="plus" data-value="<?= $cart['quantity'] ?>">
                                            <i class="fa fa-plus orange"></i>
                                        </button>
                                    </section>

                                </td>
                                <td class="__crpc"><?= DecimalAmount($cart['subtotal']) ?></td>
                                <td>
                                    <a class="__close remove_cart_product" href="javascript:void(0)" data-id="<?= $menu_idTrimp ?>" data-cart="<?= $cart['cart_id'] ?>"><i class="fas fa-times-circle"></i></a>
                                </td>
                            </tr>
                            <?php
                            $subtotal += $cart['price'] * $cart['quantity'];
                        }
                    }
                    $db->session->set_userdata('sub_total', $subtotal);
                    ?>
                </table>
            </div>


            <?php
            $subtotal = $db->session->userdata('sub_total');
            ?>
            <div class="wd100 __cartsummytabv_btpart total">
                <table class="table">
                    <tr>
                        <td align="left"><?php echo $db->lang->line("sub_total"); ?></td>
                        <td align="right" class="__srylst"><?php echo $db->lang->line("aed"); ?> <?= DecimalAmount($subtotal); ?></td>
                    </tr>
                    <?php
                    if ($session_cart[0]['party'] == 0) {
                        ?>
                        <tr style="display: none;">
                            <td align="left"><?php echo $db->lang->line("delivery_fee"); ?></td>
                            <td align="right" class="__srylst"><?php echo $db->lang->line("aed"); ?> <?= DecimalAmount($get_restaurant_details[0]->service_charge) ?></td>
                        </tr>

                        <?php
                        $sub_tot = DecimalAmount($subtotal);
                        $serv_charge = DecimalAmount($get_restaurant_details[0]->service_charge);
                        $serv_charge = 0;
                    } else {
                        $sub_tot = DecimalAmount($subtotal);
                        $serv_charge = 0;
                    }

                    $total_amount = $sub_tot + $serv_charge;
                    ?>
                    <tr>
                        <td align="left"><b><?php echo $db->lang->line("total_amount"); ?></b> </td>
                        <td align="right"  class="__crt_totalleft" dddddd="<?= $sub_tot ?>"><b><?php echo $db->lang->line("aed"); ?> <?= DecimalAmount($subtotal); ?></b></td>
                    </tr>
                </table>
                <?php
                $start_time = date('H:i', strtotime($get_restaurant_details[0]->delivery_time_start));
                $end_time = date('H:i', strtotime($get_restaurant_details[0]->delivery_time_ends));
                $currentTime = date('H:i', time());

                if ($start_time != '00:00:00' && $end_time != '00:00:00') {
                    if ($start_time < $currentTime && $currentTime > $end_time) {
                        $delivery = 1;
                    } elseif ($currentTime < $start_time) {
                        $delivery = 0;
                    } else {
                        $delivery = 1;
                    }
                }
                ?>

                <input type="hidden" value="<?= $delivery ?>" id="delivery_time">
                <div class="form-group">
                    <?php
                    $sub_tot = DecimalAmount($subtotal, 2);

                    $min_amount = GetRestaurantDetails($cart['vendor_id']);
                    $minimum_amount = $min_amount[0]->min_amount;

                    $iScheckOut = true;
                    if (!empty($min_amount[0]->min_amount)) {
                        if ($sub_tot < $minimum_amount) {
                            $iScheckOut = false;
                        }
                    }

                    if (!empty($iScheckOut)) {
                        ?>
                        <button type="button" style="displayRR: <?= !empty($partyOrdAct) ? 'none' : 'block' ?>" class="btn btn-success btn-block   checkoutbtn" id="proceed_to_checkout" url="<?= $OnclickUrl ?>" onClick=""><?php echo $db->lang->line("proceed_to_checkout"); ?></button>
                        <?php
                    } else {


                        $min_amount = GetRestaurantDetails($cart['vendor_id']);
                        $minimum_amount = $min_amount[0]->min_amount;
                        //   $min_amount = number_format($get_restaurant_details[0]->min_amount,2);
                        $rem_amt = $minimum_amount - $sub_tot;
                        ?>
                        <button type="button" fff class="btn btn-success btn-block checkoutbtn" id="proceed_to_checkout" url="<?= $OnclickUrl ?>" onClick="" disabled style="cursor:not-allowed;"><?php echo $db->lang->line("add_aed"); ?> <?= DecimalAmount($rem_amt, 2) ?> <?php echo $db->lang->line("to_checkout"); ?></button>
                        <?php
                    }
                    ?>
                </div>

            </div>


        </div>
        <?php
    } else {
        ?>
        <div class="wd100 __wtbx __cartsummy empty_cart">
            <h2><?php echo $db->lang->line("your_cart"); ?></h2>
            <img src="<?= base_url('images/empty-cart.svg') ?>" width="80">
            <p><?php echo $db->lang->line("no_items_cart"); ?></p>
        </div>
        <?php
    }
    if (!empty($encode)) {
        $html = ob_get_clean();
        die(json_encode(array('status' => true, 'HTML' => $html)));
    }
}

function headerAdminNotifaction() {
    ob_start();
    $noti = get_all_notification('1'); //$Session->id
    foreach ($noti as $notification) {
        if ($notification->inserted_on != '0000-00-00 00:00:00') {
            $datas = get_timedifference($notification->inserted_on);
        } else {
            $datas = '';
        }

        if ($notification->notification_type == NEW_USER) {
            $class = 'fas fa-user-plus';
        } else if ($notification->notification_type == NEW_RIDER) {
            $class = 'fas fa-user-plus';
        } elseif ($notification->notification_type == NEW_RESTAURANT) {
            $class = 'fas fa-store';
        } elseif ($notification->notification_type == NEW_ORDER) {
            $class = 'fas fa-shopping-cart';
        } elseif ($notification->notification_type == PARTY_ORDER) {
            $class = 'fas fa-shopping-cart';
        }
        ?>
        <li>
            <a href="javascript:void(0);" style="background:<?= empty($notification->notification_read) ? '#fbf7f7' : '#ffffff' ?>;" onclick="readNotification(this)" data-id="<?= $notification->notification_id ?>" data-url="<?= $notification->redirect_url ?>">
                <span class="time"><?= $datas ?></span>
                <span class="details">
                    <span class="label label-sm label-icon label-success">
                        <!--<i class="fa fa-plus"></i>-->
                        <i style="font-size: 15px;" class="<?= $class ?>"></i>
                    </span>
                    <?= $notification->notification_message ?> 
                </span>
            </a>
        </li>
        <?php
    }
    $html = ob_get_clean();
    die(json_encode(array('status' => true, 'HTML' => $html,'countlist'=>  count($noti))));
}
?>