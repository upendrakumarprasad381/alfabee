<?php

function SendnotificationorderStatus($orderId = '') {
    $db = LoadDB();
    $order = GetOrderById($orderId);
    $user = GetCustomerArrayById($order->user_id);

    $dArray = GetorderstatushistorylastBy($orderId);
    // $subject = "Order status Updated: $order->orderno";
    // $message = "Order " . !empty($dArray->status_name) ? $dArray->status_name : 'Received';
    $rest = GetRestaurantById($order->vendor_id);
    if($dArray->status_id==1)
    {
        $subject = $rest[0]->restaurant_name .' has accepted your order';
        $message = $rest[0]->restaurant_name .' has accepted your order';
    }elseif($dArray->status_id==2)
    {
        $subject = 'Your order from '.$rest[0]->restaurant_name .' is on the way';
        $message = 'Your order from '.$rest[0]->restaurant_name .' is on the way';
    }elseif($dArray->status_id==3)
    {
        $subject = 'Order delivered';
        $message = 'Order delivered';
    }elseif($dArray->status_id==5)
    {
        $subject = 'Your order has been cancelled';
        $message = 'Your order has been cancelled';
    }
    send_pushnotification([$user->device_id], $subject, $message, $identifier = '1', $argument = array('order_id' => $orderId));
}
