<?php


function get_menu_options(){
    $db = LoadDB();
    $session_cart = $db->session->userdata('CartData');
    if(!empty($session_cart))
     {
         $session_vendor = $session_cart[0]['vendor_id'];
         $session_party = $session_cart[0]['party'];
     }else{
         $session_vendor = 0;
         $session_party = '';
     }
     $get_vendor_id = $_POST['vendor_id'];

        $category_id = $_POST['category_id'];
        $menu_id = $_POST['menu_id'];
        $Qry = "SELECT menu_list.*,most_selling.id as most_id,menu_list.category_id as category_id,most_selling.menu_id as menu_id,promo_code.* 
                FROM `menu_list` LEFT JOIN promo_code ON (promo_code.offer_addedby=menu_list.vendor_id AND promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE() 
                AND promo_code.status=0 AND promo_code.archive=0) LEFT JOIN most_selling ON most_selling.vendor_id=menu_list.vendor_id WHERE menu_list.id='$menu_id' AND (menu_list.category_id='$category_id' OR most_selling.category_id='$category_id') 
                GROUP BY menu_list.category_id,most_id,promo_code.promocode_id ";
        // $Qry = "SELECT * FROM `menu_list` LEFT JOIN offers ON offers.offer_id=menu_list.deal_id LEFT JOIN promo_code ON promo_code.promocode_id=offers.offer_promocode_id WHERE menu_list.id='$menu_id' AND menu_list.category_id='$category_id'";
        // print_r($Qry);die();
        $CategoryArray = $db->Database->select_qry_array($Qry);
        
        $ConditionArray = array('id' => $CategoryArray[0]->id);
        
        $Qry1 = "SELECT * FROM `menu_sizes` WHERE menu_id=:id ORDER BY menu_size_id DESC";
        $Array1 = $db->Database->select_qry_array($Qry1, $ConditionArray);
    
        $Qry2 = "SELECT * FROM `menu_addons` WHERE menu_id=:id ORDER BY menu_addon_id DESC";
        $Array2 = $db->Database->select_qry_array($Qry2, $ConditionArray);
    
        $Qry3 = "SELECT * FROM `menu_topping` WHERE menu_id=:id ORDER BY menu_topping_id DESC";
        $Array3 = $db->Database->select_qry_array($Qry3, $ConditionArray);
        
        $Qry4 = "SELECT * FROM `menu_drink` WHERE menu_id=:id ORDER BY menu_drink_id DESC";
        $Array4 = $db->Database->select_qry_array($Qry4, $ConditionArray);
        
        $Qry5 = "SELECT * FROM `menu_dips` WHERE menu_id=:id ORDER BY menu_dip_id DESC";
        $Array5 = $db->Database->select_qry_array($Qry5, $ConditionArray);
    
        $Qry6 = "SELECT * FROM `menu_side` WHERE menu_id=:id ORDER BY menu_side_id DESC";
        $Array6 = $db->Database->select_qry_array($Qry6, $ConditionArray);
        	
        $html = '';
        
        foreach($CategoryArray as $menu_options){
            
            if($db->session->userdata('language')=='ar')
	        {
	            if($menu_options->menu_name_ar!='')
	            {
	                $menu_name = $menu_options->menu_name_ar;
	            }else{
	                $menu_name = $menu_options->menu_name;
	            }
	        }else{
	             $menu_name = $menu_options->menu_name;
	        }
    
            $html = '<div class="row">
    					
    					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 __mtetxt">
    						<div class="media-body">
    							<h3>'.$menu_name.'</h3>
    							<div class="wd100 __ctdcrp">
    								<p>'.$menu_options->description.'</p>
    							</div>
    						</div>
    				 	</div>
    					
    					<div class="col __pqty">
    						<section class="qty-update-section incart b-a ng-scope">
    							<button type="button" class="btn btn-sm b-r" onclick="updateQuantity(this)" data-id="'.$menu_options->id.'" data-type="minus"> <i class="fa fa-minus orange"></i> </button> <span class="f-11"><b class="qty">1</b></span>
    							<button type="button" class="btn btn-sm b-l" onclick="updateQuantity(this)" data-id="'.$menu_options->id.'" data-type="plus"> <i class="fa fa-plus orange"></i> </button>
    						</section>
    					</div>
    				 	
    					<div class="col __pirc">';
    					$menu_id = explode(',',$menu_options->menu_id);
					    if (in_array($menu_options->id, $menu_id)) 
    					{
    				// 	if($menu_options->deal_id!=0)
					   // {
					       $disc_price = $menu_options->discount/100;
					       $dis_price = DecimalAmount($menu_options->price)*$disc_price;
					       $discount_price = DecimalAmount($menu_options->price) - $dis_price;
					       $di_price = $discount_price;
					    }else{
					        $discount_price = 0;
					        $disc_price = 0;
					        $di_price = $menu_options->price;
					    }
    					if($menu_options->price!=0 && $menu_options->choice == 1)
    					{
    					    if (in_array($menu_options->id, $menu_id)) 
        					{
    					   // if($menu_options->deal_id!=0)
    					   // {
        					    $html .= '<div class="wd100 _cartpris item_price"> <span style="text-decoration: line-through">Price PKR  '.  DecimalAmount($menu_options->price).'</span><input type="hidden" value="0" id="options_price"><input type="hidden" value="'.$menu_options->price.'" id="tot_price"><input type="hidden" value="'.  DecimalAmount($di_price,2).'" id="menu_tot_price"><input type="hidden" value="'.$disc_price.'" id="discount"> </div>';
        					    $html .= '<div class="wd100 _cartpris item_discount_price"> <span>Price PKR  '.  DecimalAmount($discount_price).'</span></div>';
    					    }else{
    					        $html .= '<div class="wd100 _cartpris item_price"> <span>Price PKR  '.  DecimalAmount($menu_options->price).'</span><input type="hidden" value="0" id="options_price"><input type="hidden" value="'.$menu_options->price.'" id="tot_price"><input type="hidden" value="'.  DecimalAmount($menu_options->price,2).'" id="menu_tot_price"><input type="hidden" value="'.$discount_price.'" id="discount"> </div>';
    					    }
    					    
    					}else{
    					    $html .= '<div class="wd100 _cartpris item_price"><span>'.$db->lang->line("price_on_selection").'</span><input type="hidden" value="0" id="options_price"><input type="hidden" value="'.$menu_options->price.'" id="tot_price"><input type="hidden" value="'.  DecimalAmount($menu_options->price,2).'" id="menu_tot_price"><input type="hidden" value="'.$disc_price.'" id="discount"></div>';
    				        $html .= '<div class="wd100 _cartpris item_discount_price" style="display:none;"> <span>Price PKR  '.  DecimalAmount($discount_price).'</span></div>';
    					}
    				 	$html .= '</div>
    				 	
    				</div>
    				
    		<div class="row">
    	 	<div class="wd100 __popaccd__accordion_cm">
    			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
    		    if(!empty($Array1))
    		    {
    				
    				$html .= '<div class="panel panel-default">
    					<div class="panel-heading active" role="tab" id="addone_headingOne">
    						<h4 class="panel-title">
    				<a role="button" data-toggle="collapse" data-parent="#accordion" href="#addone_collapseOne" aria-expanded="true" aria-controls="addone_collapseOne">
    					<div class="__chkicon">
    						<img class="img-fluid" id="customRadio" src="'.base_url().'images/success-circle.svg">
    					</div>'.	
    				    $db->lang->line('select_size') .'<small>('.$db->lang->line("choose_1").')</small>
    					<div class="__chkslcttext" id="selected_item">
    						
    					</div> 
    							
    					</a> </h4> 
    					</div>
    					<div id="addone_collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="addone_headingOne">
    						<div class="panel-body wd100"> 
    					 		
    							<ul class="wd100">';
    					foreach($Array1 as $size)
    					{
    
    					    if($size->menu_price!=0)
    					    {
    					        $menu_price = $size->menu_price;
    					        $a = ' ('.  DecimalAmount($menu_price).')';
    					    }else{
    					       $a = '';
    					    }
    					 if($size->menu_size=='Small')
    					 {
    					     $size_name = $db->lang->line('small');
    					 }elseif($size->menu_size=='Large')
    					 {
    					     $size_name = $db->lang->line('large');
    					 }elseif($size->menu_size=='Medium')
    					 {
    					     $size_name = $db->lang->line('medium');
    					 }elseif($size->menu_size=='Half')
    					 {
    					     $size_name = $db->lang->line('Half');
    					 }elseif($size->menu_size=='Full')
    					 {
    					     $size_name = $db->lang->line('Full');
    					 }
    					$html .= '<li>
    								<div class="custom-control custom-radio">
    									<input type="radio" id="customRadio'.$size->menu_size_id.'" name="customRadio" class="custom-control-input selected_item" value="'.  DecimalAmount($size->menu_price,2).'" data-price="'.$menu_options->price.'" data-name="'.$size->menu_size.'" data-type="size">
    									<input type="hidden" value="'.$size->menu_price.'" name="menu_price" id="menu_price" data-price="'.$menu_options->price.'">
    									<label class="custom-control-label" for="customRadio'.$size->menu_size_id.'">'.$size_name.'<span class="__uprs">'.$a.'</span></label>
    								</div>
    								</li>';
    					}
    					$html .=  '</ul>
    						 </div>
    					</div>
    				</div>';
    				}
    				if(!empty($Array2))
    		    {
    
    				$html .= '<div class="panel panel-default">
    					<div class="panel-heading active" role="tab" id="addone_headingTwo">
    						<h4 class="panel-title">
    			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#addone_collapseTwo" aria-expanded="false" aria-controls="addone_collapseTwo">
    				
    									<div class="__chkicon">
    						<img class="img-fluid" src= "'.base_url().'images/success-circle.svg">
    					</div>'.	
    				    $db->lang->line('add_ons') .'<small></small>
    					<div class="__chkslcttext" id="addon_checkbox">
    						
    					</div>
    				 
    			</a>
    		</h4> </div>
    					<div id="addone_collapseTwo" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="addone_headingTwo">
    						<div class="panel-body wd100"> 
    					 		
    							<ul class="wd100">';
    						foreach($Array2 as $addon)
    						{
        						if($addon->addon_price!=0)
        					    {
        					        $addon_price = $addon->addon_price;
        					        $a = ' ('.  DecimalAmount($addon_price).')';
        					    }else{
        					       $a = ''; 
        					       
        					    }
    								
    							$html .='<li>
    							 		<div class="custom-control custom-checkbox add_on">
    							 	
    									  <input type="checkbox" name="addon[]" onclick="myFunction(this)" class="custom-control-input addon_checkbox" id="customCheck'.$addon->menu_addon_id.'" value="'.  DecimalAmount($addon->addon_price).'" data-price="'.$menu_options->price.'" data-name="'.$addon->add_on.'" data-type="addon">
    									  <label class="custom-control-label text" for="customCheck'.$addon->menu_addon_id.'">'.$addon->add_on.'<span class="__uprs">'.$a.'</span></label>
    									</div>
    									 
    								</li>';
    						}
    					
    						$html .= '</ul>
    						 </div>
    					</div>
    				</div>';
    		    }
    		    
    		    	if(!empty($Array3))
    		    {
    		        $html .= '<div class="panel panel-default">
    					<div class="panel-heading active" role="tab" id="addone_headingTwo">
    						<h4 class="panel-title">
    			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#addone_collapseTwo" aria-expanded="false" aria-controls="addone_collapseTwo">
    				
    									<div class="__chkicon">
    						<img class="img-fluid" src= "'.base_url().'images/success-circle.svg">
    					</div>'.	
    				    $db->lang->line('topping') .'<small></small>
    					<div class="__chkslcttext" id="topping_checkbox">
    						
    					</div>
    				 
    			</a>
    		</h4> </div>
    					<div id="addone_collapseTwo" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="addone_headingTwo">
    						<div class="panel-body wd100"> 
    					 		
    							<ul class="wd100">';
    						foreach($Array3 as $topping)
    						{
        						if($topping->topping_price!=0)
        					    {
        					        $topping_price = $topping->topping_price;
        					        $a = ' ('.  DecimalAmount($topping_price,2).')';
        					    }else{
        					       $a = ''; 
        					       
        					    }
    								
    							$html .='<li>
    							 		<div class="custom-control custom-checkbox topping">
    							 	
    									  <input type="checkbox" name="topping[]" onclick="myFunction(this)" class="custom-control-input topping_checkbox" id="customCheck'.$topping->menu_topping_id.'" value="'.  DecimalAmount($topping->topping_price,2).'" data-price="'.$menu_options->price.'" data-name="'.$topping->topping.'" data-type="topping">
    									  <label class="custom-control-label text" for="customCheck'.$topping->menu_topping_id.'">'.$topping->topping.'<span class="__uprs">'.$a.'</span></label>
    									</div>
    									 
    								</li>';
    						}
    					
    						$html .= '</ul>
    						 </div>
    					</div>
    				</div>';
    
    		    }
    // 		   if(!empty($Array4))
    // 		    {
    
    // 				$html .= '<div class="panel panel-default">
    // 					<div class="panel-heading active" role="tab" id="addone_headingTwo">
    // 						<h4 class="panel-title">
    // 			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#addone_collapseTwo" aria-expanded="false" aria-controls="addone_collapseTwo">
    				
    // 									<div class="__chkicon">
    // 						<img class="img-fluid" id="customOptions" src= "'.base_url().'images/success-circle.svg">
    // 					</div>'.	
    // 				    $db->lang->line('drink') .'<small>('.$db->lang->line("choose_1").')</small>
    // 					<div class="__chkslcttext" id="drink_radio">
    						
    // 					</div>
    				 
    // 			</a>
    // 		</h4> </div>
    // 					<div id="addone_collapseTwo" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="addone_headingTwo">
    // 						<div class="panel-body wd100"> 
    					 		
    // 							<ul class="wd100">';
    // 						foreach($Array4 as $drink)
    // 						{
    //     						if($drink->drink_price!=0)
    //     					    {
    //     					        $drink_price = $drink->drink_price;
    //     					        $a = ' ('.number_format($drink_price,2).')';
    //     					    }else{
    //     					       $a = ''; 
        					       
    //     					    }
    								
    // 							$html .='<li>
    // 							 		<div class="custom-control custom-radio">
    // 							 		<input type="radio" id="customRadio'.$drink->menu_drink_id.'" name="customOptions" class="custom-control-input drink_radio" value="'.number_format($drink->drink_price,2).'" data-price="'.$menu_options->price.'" data-name="'.$drink->drink_name.'" data-type="drink">
    // 									<input type="hidden" value="'.$drink->drink_price.'" name="menu_price" id="menu_price" data-price="'.$menu_options->price.'">
    // 									<label class="custom-control-label" for="customRadio'.$drink->menu_drink_id.'">'.$drink->drink_name.'<span class="__uprs">'.$a.'</span></label>
    							
    // 									</div>
    									 
    // 								</li>';
    // 						}
    					
    // 						$html .= '</ul>
    // 						 </div>
    // 					</div>
    // 				</div>';
    // 		    }
    		
    			if(!empty($Array5))
    		    {
    
    				$html .= '<div class="panel panel-default">
    					<div class="panel-heading active" role="tab" id="addone_headingTwo">
    						<h4 class="panel-title">
    			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#addone_collapseTwo" aria-expanded="false" aria-controls="addone_collapseTwo">
    				
    									<div class="__chkicon">
    						<img class="img-fluid" src= "'.base_url().'images/success-circle.svg">
    					</div>'.	
    				    $db->lang->line('dips') .'<small></small>
    					<div class="__chkslcttext" id="dip_checkbox">
    						
    					</div>
    				 
    			</a>
    		</h4> </div>
    					<div id="addone_collapseTwo" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="addone_headingTwo">
    						<div class="panel-body wd100"> 
    					 		
    							<ul class="wd100">';
    						
    						foreach($Array5 as $dip)
    						{
        						if($dip->dip_price!=0)
        					    {
        					        $dip_price = $dip->dip_price;
        					        $a = ' ('.  DecimalAmount($dip_price,2).')';
        					    }else{
        					       $a = ''; 
        					       
        					    }
    								
    							$html .='<li>
    							 		<div class="custom-control custom-checkbox">
    									  <input type="checkbox" onclick="myFunction(this)"  class="custom-control-input dip_checkbox" id="customCheck'.$dip->menu_dip_id.'" value="'.  DecimalAmount($dip->dip_price,2).'" data-price="'.$menu_options->price.'" data-name="'.$dip->dips.'" data-type="dip">
    									  <label class="custom-control-label" for="customCheck'.$dip->menu_dip_id.'">'.$dip->dips.'<span class="__uprs">'.$a.'</span></label>
    									</div>
    									 
    								</li>';
    						}
    					
    						$html .= '</ul>
    						 </div>
    					</div>
    				</div>';
    		    }
    		    if(!empty($Array6))
    		    {
    
    				$html .= '<div class="panel panel-default">
    					<div class="panel-heading active" role="tab" id="addone_headingTwo">
    						<h4 class="panel-title">
    			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#addone_collapseTwo" aria-expanded="false" aria-controls="addone_collapseTwo">
    				
    									<div class="__chkicon">
    						<img class="img-fluid" src= "'.base_url().'images/success-circle.svg">
    					</div>'.	
    				    $db->lang->line('side') .'<small></small>
    					<div class="__chkslcttext" id="side_checkbox">
    						
    					</div>
    				 
    			</a>
    		</h4> </div>
    					<div id="addone_collapseTwo" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="addone_headingTwo">
    						<div class="panel-body wd100"> 
    					 		
    							<ul class="wd100">';
    						
    						foreach($Array6 as $side)
    						{
        						if($side->side_dish_price!=0)
        					    {
        					        $side_price = $side->side_dish_price;
        					        $a = ' ('.  DecimalAmount($side_dish_price,2).')';
        					    }else{
        					       $a = ''; 
        					       
        					    }
    								
    							$html .='<li>
    							 		<div class="custom-control custom-checkbox">
    									  <input type="checkbox" onclick="myFunction(this)"  class="custom-control-input side_checkbox" id="customCheck'.$side->menu_side_id.'" value="'.  DecimalAmount($side->side_dish_price,2).'" data-price="'.$menu_options->price.'" data-name="'.$side->side_dish.'" data-type="side">
    									  <label class="custom-control-label" for="customCheck'.$side->menu_side_id.'">'.$side->side_dish.'<span class="__uprs">'.$a.'</span></label>
    									</div>
    									 
    								</li>';
    						}
    					
    						$html .= '</ul>
    						 </div>
    					</div>
    				</div>';
    		    }
    				$html .= '</div>
    				</div>
    				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center pt-3 __pop_cartcmtbnt">
    				<input type="hidden" id="selected_values" value="">
    			<button type="button" class="btn btn-success btn-lg add_to_cart" id="add_tocart" data-id="'.$menu_options->id.'" data-party="0" data-choice="'.$menu_options->choice.'" data-vendor="'.$menu_options->vendor_id.'" data-category="'.$menu_options->category_id.'" data-sessionvendor="'. $session_vendor.'" data-sessionparty="'. $session_party.'" ><i class="fas fa-cart-plus"></i> '.$db->lang->line("add_to_cart").'</button>
    		</div>	';
        }
        echo $html;
           
}