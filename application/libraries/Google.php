<?php

class Google {

    private $Client;
    private $Plus;

    public function __construct() {
        $dir = HOME_DIR . 'application/third_party/Google/';
        require_once $dir . 'vendor/autoload.php';
        $this->Client = new Google_Client();
        $this->Client->setAuthConfig($dir . 'json2.json');
        $this->Plus = new Google_Service_Plus($this->Client);
        $Scope = array(
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/userinfo.profile',
        );
        $this->Client->addScope($Scope);
        $RedirectUri = base_url('Login/GoogleLoginDetails');
        $this->Client->setRedirectUri($RedirectUri);
    }

    public function Login() {
        $auth_url = $this->Client->createAuthUrl();
        header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
    }

    public function UserInfo($Code) {
        $this->Client->authenticate($Code);
        $TockenArray = $this->Client->getAccessToken();
        if ($this->Client->getAccessToken()) {
            // $me = $this->Plus->people->get('me');
            $me = new Google_Service_PeopleService( $this->Client);
        
            $profile = $me->people->get(
              'people/me', 
              array('personFields' => 'names,emailAddresses,photos')
            );

            $returnArray = array(
                'Live' => true,
                'login_type' => 2,
                'social_id' => $profile['modelData']['names'][0]['metadata']['source']['id'],
                'name' => $profile['modelData']['names'][0]['displayName'],
                'image' => $profile['modelData']['photos'][0]['url'],
                'email' => $profile['modelData']['emailAddresses'][0]['value'],
            );
            return $returnArray;
        }
    }

}
