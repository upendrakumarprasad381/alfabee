<?php

class Database extends CI_Model {

    private $Pdo;
    private $timestamp_table = array(
        'addon_details' => 'timestamp',
        'add_on' => 'timestamp',
        'add_to_cart' => 'timestamp',
        'admin_login' => 'timestamp',
        'ads' => 'timestamp',
        'book_table' => 'lastupdate',
        'category_details' => 'timestamp',
        'chat_history' => 'timestamp',
        'commission' => 'timestamp',
        'contact_us' => 'timestamp',
        'cuisine' => 'timestamp',
        'delivery_timings' => 'timestamp',
        'featured_deals_image' => 'timestamp',
        'features' => 'timestamp',
        'home_banner' => 'timestamp',
        'loyality_points' => 'timestamp',
        'loyality_point_history' => 'timestamp',
        'menu_addons' => 'timestamp',
        'menu_dips' => 'timestamp',
        'menu_drink' => 'timestamp',
        'menu_list' => 'timestamp',
        'menu_side' => 'timestamp',
        'menu_sizes' => 'timestamp',
        'menu_topping' => 'timestamp',
        'notification' => 'inserted_on',
        'offers' => 'offer_addedon',
        'order_status_history' => 'timestamp',
        'partyorder_request' => 'inserted_on',
        'party_order' => 'date',
        'party_order_menu' => 'timestamp',
        'promo_code' => 'timestamp',
        'restaurant_cuisine_type' => 'timestamp',
        'rider_commission' => 'timestamp',
        'sponsored_ad' => 'timestamp',
        'store_type' => 'timestamp',
        'users' => 'timestamp',
        'user_feedback' => 'inserted_on',
        'user_order_address' => 'timestamp',
        'user_permission' => 'timestamp',
        'user_transactions' => 'timestamp',
        'vendor_online_history' => 'timestamp',
    );

    function __construct() {
        parent::__construct();
        try {
            $this->Pdo = new PDO("mysql:host=" . HOSTNAME . ";dbname=" . DATABASE . "", USERNAME, PASSWORD);
            $this->Pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $ex) {
            echo "Connection Error : " . die($ex->getMessage());
        }
    }

    public function insert($tablename, $data) {

        if (!empty($this->timestamp_table[$tablename])) {
            $data[$this->timestamp_table[$tablename]] = date('Y-m-d H:i:s');
        }

        $ValueQues = implode(',', array_fill(0, count($data), '?'));
        $sql = "INSERT INTO $tablename (" . implode(', ', array_keys($data)) . ") VALUES ($ValueQues)";
        try {
            $Result = $this->Pdo->prepare($sql);
            $TotalKey = 1;
            foreach ($data as $keys => $value) {
                $Result->bindValue($TotalKey, $value);
                $TotalKey++;
            }
            $Result->execute();
            return $this->Pdo->lastInsertId();
        } catch (PDOException $ex) {
            echo "Failed To Insert Query:- " . $sql . ' &nbsp;' . die($ex->getMessage());
        }
    }

    public function select_qry_array($Query, $CondutaionArray = array()) {
        try {
            $Result = $this->Pdo->prepare($Query);
            foreach ($CondutaionArray as $keys => $value) {
                $a = $Result->bindValue($keys, $value);
            }
            $Result->execute();
            $DataArray = $Result->fetchAll(PDO::FETCH_OBJ);
            return $DataArray;
        } catch (PDOException $ex) {
            echo "Fail To Excute This Query :  " . $Query . die($ex->getMessage());
        }
    }

    public function num_rows($Query, $CondutaionArray = array()) {

        try {
            $Result = $this->Pdo->prepare($Query);
            foreach ($CondutaionArray as $keys => $value) {
                $Result->bindValue($keys, $value);
            }
            $Result->execute();
            $Count = $Result->rowCount();
            return $Count;
        } catch (PDOException $ex) {
            echo "Fail To Excute This Query :  " . $Query . die($ex->getMessage());
        }
    }

    public function PrepareQuery($Query, $BindValue = array(), $Execute = false) {
        try {
            $Result = $this->Pdo->prepare($Query);
            foreach ($BindValue as $keys => $value) {
                $Result->bindValue($keys, $value);
            }
            if ($Execute == true) {
                $Result->execute();
            }
            return $Result;
        } catch (PDOException $ex) {
            echo "Fail To Excute This Query :  " . $Query . die($ex->getMessage());
        }
    }

    public function GetConnection() {
        try {
            return $this->Pdo;
        } catch (PDOException $ex) {
            echo "Fail To Connection :  " . die($ex->getMessage());
        }
    }

    public function update($table_name, $array, $CondArray) {

        $CondKey = array_keys($CondArray);
        $SetCondKey = implode('=? AND ', $CondKey);
        $SetCondKey = $SetCondKey . '=?';

        $Key = array_keys($array);
        $SetQuesy = implode('=?,', $Key);
        $SetQuesy = $SetQuesy . '=?';
        $Sql = "UPDATE " . $table_name . " SET $SetQuesy WHERE $SetCondKey";

        try {
            $Result = $this->Pdo->prepare($Sql);
            $TotalKey = 1;
            foreach ($array as $keys => $value) {
                $Result->bindValue($TotalKey, $value);
                $TotalKey++;
            }
            foreach ($CondArray as $keys => $value) {
                $Result->bindValue($TotalKey, $value);
                $TotalKey++;
            }

            return $Result->execute();
        } catch (PDOException $ex) {
            echo "Fail To Update :  " . $Sql . die($ex->getMessage());
        }
    }

    public function delete($TableName, $CondArray) {
        $CondString = '';
        foreach ($CondArray as $key => $value) {
            $CondString = $CondString . $key . '=:' . $key . ' AND ';
        }
        $CondString = substr($CondString, 0, -5);
        $Sql = "DELETE FROM $TableName WHERE $CondString";
        try {
            $Result = $this->Pdo->prepare($Sql);
            foreach ($CondArray as $keys => $value) {
                $Result->bindValue($keys, $value);
            }
            return $Result->execute();
        } catch (PDOException $ex) {
            echo 'Error To Delete' . $ex->getMessage();
        }
    }

    public function random_password($len = 8) {

        //enforce min length 8
        if ($len < 8)
            $len = 8;

        //define character libraries - remove ambiguous characters like iIl|1 0oO
        $sets = array();
        $sets[] = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        $sets[] = '23456789';
        $sets[] = '~!@#$%^&*(){}[],./?';

        $password = '';

        //append a character from each set - gets first 4 characters
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
        }

        //use all characters to fill up to $len
        while (strlen($password) < $len) {
            //get a random set
            $randomSet = $sets[array_rand($sets)];

            //add a random char from the random set
            $password .= $randomSet[array_rand(str_split($randomSet))];
        }

        //shuffle the password string before returning!
        return str_shuffle($password);
    }

}
