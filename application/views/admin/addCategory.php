<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                        
                            <span class="caption-subject bold uppercase"><?= empty($Id) ? $this->lang->line("add_new") : $this->lang->line("update"); ?> <?php echo $this->lang->line("category")?></span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="Id" value="<?= $Id ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("store_type")?> </label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="store_type" name="store_type" >
        							     <option value="">Choose Type</option>
        							    <?= get_storetype(isset($Categories->store_type) ? $Categories->store_type : '' )?>
        							</select>
        							</div>
        						</div>
        							
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("category")?> <?php echo $this->lang->line("in_english")?></label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($Categories->category_name) ? $Categories->category_name : '' ?>" id="Name" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("category")?> <?php echo $this->lang->line("in_arabic")?></label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($Categories->category_name_ar) ? $Categories->category_name_ar : '' ?>" id="Name_ar" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("status")?></label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="Status">
                                            <option <?= isset($Categories->status) ? $Categories->status == 0 ? 'selected="selected"' : '' : '' ?> value="0"><?php echo $this->lang->line("active")?></option>
                                            <option <?= isset($Categories->status) ? $Categories->status == 1 ? 'selected="selected"' : '' : '' ?> value="1"><?php echo $this->lang->line("inactive")?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                               



                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="button" class="btn blue" id="SubmitCategory">
                                                <i class="fa fa-check"></i><?php echo $this->lang->line("submit")?></button>
                                            <button type="button" onclick="window.location = '<?= base_url('admin/Category'); ?>';" class="btn default"><?php echo $this->lang->line("cancel")?></button>
                                        </div>
                                    </div>
                                </div>

                                <!-- END FORM-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>





        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
