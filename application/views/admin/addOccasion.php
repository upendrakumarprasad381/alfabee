<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                        
                            <span class="caption-subject bold uppercase"><?= empty($Id) ? $this->lang->line("add_new") : $this->lang->line("update"); ?> Occasion</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="Id" value="<?= $Id ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("occasion")?> <?php echo $this->lang->line("in_english")?></label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($Occasion->occasion_name) ? $Occasion->occasion_name : '' ?>" id="Name" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("occasion")?> <?php echo $this->lang->line("in_arabic")?></label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($Occasion->occasion_name_ar) ? $Occasion->occasion_name_ar : '' ?>" id="Name_ar" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("status")?></label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="Status">
                                            <option <?= isset($Occasion->status) ? $Occasion->status == 0 ? 'selected="selected"' : '' : '' ?> value="0"><?php echo $this->lang->line("active")?></option>
                                            <option <?= isset($Occasion->status) ? $Occasion->status == 1 ? 'selected="selected"' : '' : '' ?> value="1"><?php echo $this->lang->line("inactive")?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                               



                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="button" class="btn blue" id="SubmitOccasion">
                                                <i class="fa fa-check"></i> Submit</button>
                                            <button type="button" onclick="window.location = '<?= base_url('admin/occasion'); ?>';" class="btn default">Cancel</button>
                                        </div>
                                    </div>
                                </div>

                                <!-- END FORM-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>





        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
