
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= $HomeBannerId == '' ? $this->lang->line("add_new") : $this->lang->line("update"); ?> <?php echo $this->lang->line("sponsored")?></span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <div class="form-horizontal form-bordered">
                            <fff>
                            <input type="hidden" id="Id" value="<?= $HomeBannerId ?>">
                            <div class="form-body">
                                <!--<div class="form-group">-->
                                <!--    <label class="control-label col-md-3">URL</label>-->
                                <!--    <div class="col-md-4">-->
                                <!--        <input id="homeurl" value="<?= isset($CategoryDetails->home_url) ? $CategoryDetails->home_url : '' ?>" type="text" class="form-control">-->
                                <!--    </div>-->
                                <!--</div>-->

                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("image")?>
                                    <br><span class="img-size-common">width and height 1170px * 156px</span></label>
                                    <div class="col-md-4">
                                        <form method="post" id="common_file" name="common_file">
                                            <input prev_image="<?= @$CategoryDetails->image ?>" type="file" form_name="common_file" class="form-cont common_file"  file_name="<?= @$CategoryDetails->image ?>" file_id="" name="common_file" id="" location="sponsored">                                            
                                        </form>
                                    </div>
                                    <div class="col-md-3">
                                        <a target="_blank" href="<?php if(isset($CategoryDetails->image) && $CategoryDetails->image!=""){ echo base_url().'uploads/sponsored/'.$CategoryDetails->image;} ?>">
                                        <img src=" <?php if(isset($CategoryDetails->image) && $CategoryDetails->image!=""){ echo base_url().'uploads/sponsored/'.$CategoryDetails->image;} ?>" width="20%" id="ImagesEncode" OldImage='<?= !empty($CategoryDetails->image) ? 'uploads/sponsored/' . $CategoryDetails->image : '' ?>'>
                                    </a>
                                    <?php if(isset($CategoryDetails->image) && $CategoryDetails->image!=""){ ?>
                                        <br><a class="remove_image_common" path="uploads/sponsored/" name="<?=isset($CategoryDetails->image)?$CategoryDetails->image:''?>" table="sponsored_ad" id="<?=$HomeBannerId?>" type="">Remove</a>
                                      <?php } ?>
                                    <?php
                                    // $ImageFilePath = 'uploads/sponsored/' . (empty($CategoryDetails->image) ? '' : $CategoryDetails->image);
                                    // if (is_file(HOME_DIR . $ImageFilePath)) {
                                        ?>
                                        <!--<div class="col-md-3">-->
                                        <!--    <img style="height: 30px;width: 34px;" src="<?= base_url($ImageFilePath) ?>">-->
                                        <!--</div>-->
                                    <?php 
                                    // } 
                                    ?>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("status")?></label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="HomeStatus">
                                            <option <?= isset($CategoryDetails->archive) ? $CategoryDetails->archive == 0 ? 'selected="selected"' : '' : '' ?> value="0"><?php echo $this->lang->line("active")?></option>
                                            <option <?= isset($CategoryDetails->archive) ? $CategoryDetails->archive == 1 ? 'selected="selected"' : '' : '' ?> value="1"><?php echo $this->lang->line("inactive")?></option>
                                        </select>
                                    </div>
                                </div>                                

                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="SubmitSponsored" class="btn blue">
                                            <i class="fa fa-check"></i> <?php echo $this->lang->line("submit")?></button>
                                        <button type="button" onclick="window.location = '/alfabee/Admin/addSponsored';" class="btn default"><?php echo $this->lang->line("cancel")?></button>
                                    </div>
                                </div>
                            </div>
                        </fff>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->