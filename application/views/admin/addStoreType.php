<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">

                            <span class="caption-subject bold uppercase"><?= empty($Id) ? $this->lang->line("add_new") : $this->lang->line("update"); ?> <?php echo $this->lang->line("store_type") ?></span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <fff  class="form-horizontal form-bordered">
                            <input type="hidden" id="Id" value="<?= $Id ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("store_type") ?> <?php echo $this->lang->line("in_english") ?></label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($Categories->store_type) ? $Categories->store_type : '' ?>" id="Name" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("store_type") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($Categories->store_type_ar) ? $Categories->store_type_ar : '' ?>" id="Name_ar" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Image</label>
                                    <div class="col-md-4">
                                        <form method="post" id="common_file" name="common_file">
                                            <input prev_image="<?= @$Categories->image ?>" type="file" form_name="common_file" class="form-cont common_file"  file_name="<?= @$Categories->image ?>" file_id="" name="common_file" id="" location="store_type">                                            
                                        </form>
                                    </div>
                                    <div class="col-md-3">
                                        <a target="_blank" href="<?php
                                        if (isset($Categories->image) && $Categories->image != "") {
                                            echo base_url() . 'uploads/store_type/' . $Categories->image;
                                        }
                                        ?>">
                                            <img src=" <?php
                                        if (isset($Categories->image) && $Categories->image != "") {
                                            echo base_url() . 'uploads/store_type/' . $Categories->image;
                                        }
                                        ?>" width="20%" id="ImagesEncode" OldImage='<?= !empty($Categories->image) ? 'uploads/store_type/' . $Categories->image : '' ?>'>
                                        </a>
                                        <?php if (isset($Categories->image) && $Categories->image != "") { ?>
                                            <br><a class="remove_image_common" path="uploads/store_type/" name="<?= isset($Categories->image) ? $Categories->image : '' ?>" table="store_type" id="<?= $Id ?>" type="">Remove</a>
                                        <?php } ?>
<?php
// $ImageFilePath = 'uploads/store_type/' . (empty($Categories->image) ? '' : $Categories->image);
// if (is_file(HOME_DIR . $ImageFilePath)) {
?>
                                        <!--<div class="col-md-3">-->
                                        <!--    <img style="height: 30px;width: 34px;" src="<?= base_url($ImageFilePath) ?>">-->
                                        <!--</div>-->
                                        <?php
                                        // } 
                                        ?>
                                        <?php
                                        //  if(isset($Categories->image) && $Categories->image!=""){ 
                                        ?>
                                        <!--<br><a class="remove_image_common" path="uploads/store_type/" name="<?= isset($Categories->image) ? $Categories->image : '' ?>" table="store_type" id="<?= $Id ?>" type="">Remove</a>-->
<?php
//   } 
?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Description</label>
                                    <div class="col-md-4">
                                        <textarea class="form-control summernote" id="description" rows="10"><?= !empty($Categories->description) ? $Categories->description : '' ?></textarea>  
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("status") ?></label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="Status">
                                            <option <?= isset($Categories->status) ? $Categories->status == 0 ? 'selected="selected"' : '' : '' ?> value="0"><?php echo $this->lang->line("active") ?></option>
                                            <option <?= isset($Categories->status) ? $Categories->status == 1 ? 'selected="selected"' : '' : '' ?> value="1"><?php echo $this->lang->line("inactive") ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <hr>
                            </div>
                            <div class="form-group" >
                                <label class="control-label col-md-3">In case of urdu Alert</label>
                                <div class="col-md-9">
                                    <div class="col-sm-4">
                                        <label>If no data found</label>
                                        <input type="text" id="if_nodatafound" value="<?= !empty($Categories->if_nodatafound) ? $Categories->if_nodatafound : '' ?>" placeholder="" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="col-sm-4">
                                         <label>If shop close</label>
                                         <input type="text" id="if_shopclose" value="<?= !empty($Categories->if_shopclose) ? $Categories->if_shopclose : '' ?>" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="col-sm-4">
                                         <label>If shop busy</label>
                                         <input type="text" id="if_shopbusy" value="<?= !empty($Categories->if_shopbusy) ? $Categories->if_shopbusy : '' ?>" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                            </div>



                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" class="btn blue" id="SubmitStoreType">
                                            <i class="fa fa-check"></i><?php echo $this->lang->line("submit") ?></button>
                                        <button type="button" onclick="window.location = '<?= base_url('admin/store_type'); ?>';" class="btn default"><?php echo $this->lang->line("cancel") ?></button>
                                    </div>
                                </div>
                            </div>

                            <!-- END FORM-->
                    </div>
                    </fff>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
