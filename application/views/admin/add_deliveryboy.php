<?php
$Session = GetSessionArrayAdmin();
$userId = !empty($this->uri->segment(3)) ? base64_decode($this->uri->segment(3)) : '';
$cArray = GetCustomerArrayById($userId);
$users = GetusersById($userId);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= empty($cArray['user_id']) ? 'Add New' : 'Update'; ?> Rider</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered" method="post">
                            <input type="hidden" id="customer_id" name="customer_id" value="<?= !empty($cArray['user_id']) ? $cArray['user_id'] : '' ?>">
                            <div class="form-body">
                                <?php
                                if (isset($_POST['add_deliveryboy'])) {
                                    $update = [];
                                    $customer_id = !empty($_POST['customer_id']) ? $_POST['customer_id'] : '';

                                    $update['name'] = $_POST['name'];
                                    $update['email'] = $_POST['email'];
                                    $update['mobile_number'] = $_POST['mobile_number'];
                                    $update['area'] = !empty($_POST['business_location']) ? $_POST['business_location'] : '';
                                    $update['delivery_radius'] = $_POST['delivery_radius'];
                                    $update['mobile_code'] = '92';
                                    $update['store_type'] = '0';

                                    $update['lastupdate'] = date('Y-m-d H:i:s');
                                    
                                    $update['latitude'] = !empty($_POST['latitude']) ? $_POST['latitude'] : '';
                                    $update['longitude'] = !empty($_POST['longitude']) ? $_POST['longitude'] : '';
                                    $update['area'] = !empty($_POST['area']) ? $_POST['area'] : '';
                                    if (!empty($_POST['password'])) {
                                        $update['password'] = md5($_POST['password']);
                                    }
                                    $update['status'] = $_POST['status'];
                                    $update['archive']=$update['status'];
                                    if (empty($customer_id)) {
                                        $qry = "SELECT * FROM `users` WHERE user_type=4 AND email LIKE '" . $update['email'] . "'";
                                        $checkEmail = $this->Database->select_qry_array($qry);
                                        if (empty($checkEmail)) {
                                            $update['user_type'] = 4;
                                            $update['deliveryboy_added_by'] = '1';
                                            $this->Database->insert('users', $update);
                                            redirect(base_url('admin/delivery_boy'));
                                        } else {
                                            $message = "Already registered this email";
                                        }
                                    } else {
                                        unset($update['email']);
                                        $cond = array('id' => $customer_id);
                                        $this->Database->update('users', $update, $cond);
                                        redirect(base_url('admin/delivery_boy'));
                                    }
                                }
                                if (!empty($message)) {
                                    ?>
                                    <div class="form-group">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-8 alert alert-info" role="alert">
                                            <?= $message ?>
                                        </div>

                                    </div>
                                <?php } ?>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Name</label>
                                    <div class="col-md-4">
                                        <input id="name" name="name" value="<?= isset($cArray['full_name']) ? $cArray['full_name'] : '' ?>" type="text" class="form-control" required autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Email</label>
                                    <div class="col-md-4">
                                        <!--<input name="email" value="<?= isset($cArray['email']) ? $cArray['email'] : '' ?>" type="hidden" class="form-control" autocomplete="off" >-->
                                        <input name="email" value="<?= isset($cArray['email']) ? $cArray['email'] : '' ?>" type="email" class="form-control" autocomplete="off" <?= isset($cArray['email']) ? 'disabled' : '' ?>  required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Phone</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <button id="mobile_code" name="mobile_code" value="<?= isset($cArray['mobile_code']) ? $cArray['mobile_code'] : '92' ?>" class="btn" type="button"><?= isset($cArray->mobile_code) ? $cArray->mobile_code : '+92' ?></button>

                                            </span>
                                            <input id="mobile_number" name="mobile_number" value="<?= isset($cArray['mobile_number']) ? $cArray['mobile_number'] : '' ?>" type="number" class="form-control" autocomplete="off" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Password</label>
                                    <div class="col-md-4">
                                        <input name="password" value="" type="text" class="form-control" autocomplete="off" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Delivery Radius</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <input id="delivery_radius" name="delivery_radius" value="<?= isset($cArray['delivery_radius']) ? $cArray['delivery_radius'] : '' ?>" type="number" class="form-control" autocomplete="off" required>
                                            <span class="input-group-btn">
                                                <button id="mobile_code" name="mobile_code" value="<?= isset($cArray['mobile_code']) ? $cArray['mobile_code'] : '92' ?>" class="btn" type="button">km</button>

                                            </span>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Location</label>
                                    <div class="col-md-4">
                                        <input va type="hidden" name="latitude" id="latitude" value="<?= !empty($users->latitude) ? $users->latitude : '' ?>">
                                        <input type="hidden" name="longitude" id="longitude" value="<?= !empty($users->longitude) ? $users->longitude : '' ?>">
                                        <input type="text" name="area" id="business-location" class="form-control" value="<?= !empty($users->area) ? $users->area : '' ?>" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("status") ?></label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="status" name="status">

                                            <option <?= isset($cArray['status']) ? $cArray['status'] == 0 ? 'selected="selected"' : '' : '' ?> value="0"><?php echo $this->lang->line("active") ?></option>
                                            <option <?= isset($cArray['status']) ? $cArray['status'] == 1 ? 'selected="selected"' : '' : '' ?> value="1"><?php echo $this->lang->line("inactive") ?></option>
                                        </select>
                                    </div>
                                </div>





                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" name="add_deliveryboy" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <button type="button" onclick="window.location = '';" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<div id="business_location_map" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <input type="hidden" value="" id="isEditing">
            <div class="modal-header modal-alt-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $this->lang->line("location"); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
            <div class="modal-body">
                <div class="srch-by-txt">
                    <input type="hidden" id="lat" value="">
                    <input type="hidden" id="lng" value="">
                    <input type="text" id='business-location' value="" placeholder="<?php echo $this->lang->line("search_road_landmark"); ?>" autocomplete="off" class="form-control">
                </div>
                <div class="map-wrapper-inner" id="map-page">
                    <div id="google-maps-box">
                        <!--<div class="map-icons location_icon"><img ng-src="<?= base_url() ?>images/locate-mp.svg" class="noDrag locate cursor" onclick="getLocation()" width="45" height="45" src="<?= base_url() ?>images/locate-mp.svg" title="Current Location"></div>-->
                        <div id="map" style="width:100%; height:300px;"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="button-wrap">
                    <button type="button" class="btn btn-success confirm_location" data-dismiss="modal"><?php echo $this->lang->line("CONFIRM"); ?></button>
                </div>
            </div>
        </div>
    </div>
</div>