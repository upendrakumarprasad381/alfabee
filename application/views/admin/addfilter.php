<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= $FilterId == '' ? 'Add New' : 'Update'; ?> Filter</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <fff  class="form-horizontal form-bordered">
                            <input type="hidden" id="FilterId" value="<?= $FilterId ?>">
                            <div class="form-body">
                               <div class="form-group">
                                    <label class="control-label col-md-3">Name</label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($CategoryDetails->name) ? $CategoryDetails->name : '' ?>" id="name" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Image</label>
                                    <div class="col-md-4">
                                        <form method="post" id="common_file" name="common_file">
                                            <input prev_image="<?= @$CategoryDetails->image ?>" type="file" form_name="common_file" class="form-cont common_file"  file_name="<?= @$CategoryDetails->image ?>" file_id="" name="common_file" id="" location="filter">                                            
                                        </form>
                                    </div>
                                    <?php
                                    $ImageFilePath = 'uploads/filter/' . (empty($CategoryDetails->image) ? '' : $CategoryDetails->image);
                                    if (is_file(HOME_DIR . $ImageFilePath)) {
                                        ?>
                                        <div class="col-md-3">
                                            <img style="height: 30px;width: 34px;" src="<?= base_url($ImageFilePath) ?>">
                                        </div>
                                    <?php } ?>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3">Status</label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="Status">
                                            <option <?= isset($CategoryDetails->archive) ? $CategoryDetails->archive == 0 ? 'selected="selected"' : '' : '' ?> value="0">Active</option>
                                            <option <?= isset($CategoryDetails->archive) ? $CategoryDetails->archive == 1 ? 'selected="selected"' : '' : '' ?> value="1">Inactive</option>
                                        </select>
                                    </div>
                                </div>                                

                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="SubmitFilter" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <button type="button" onclick="window.location = '/home_eats/Admin/addfilter';" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </fff>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->