<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">

                            <span class="caption-subject bold uppercase"><?= empty($Id) ? $this->lang->line("add_new") : $this->lang->line("update"); ?> Loyality Points</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="Id" value="<?= $Id ?>">
                            <div class="form-body">

                                <!--<div class="form-group">-->
                                <!--    <label class="control-label col-md-3">Points</label>-->
                                <!--    <div class="col-md-4">-->
                                <!--        <input value="<?= !empty($loyality->point) ? $loyality->point : '' ?>" id="point" type="text" class="form-control" autocomplete="off">-->
                                <!--    </div>-->
                                <!--</div>-->
                                <div class="form-group">

                                    <label class="control-label col-md-3"><?php echo $this->lang->line("price") ?></label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>
                                            </span>
                                            <input type="text"  class="form-control col-md-2" name="point_amt" value="<?= !empty($loyality->point_amt) ? $loyality->point_amt : '' ?>" id="point_amt"/> </div>
                                    </div>
                                    <label class="control-label col-md-1">=</label>
                                    <!--<div class="col-md-3">-->
                                    <label class="control-label col-md-1">Point</label>
                                    <div class="col-md-3">
                                        <input value="<?= !empty($loyality->point) ? $loyality->point : '' ?>" id="point" type="text" class="form-control" disabled autocomplete="off">
                                        <!--</div>-->
                                    </div>
                                </div>
                                <div class="form-group" >
                                    <label class="control-label col-md-3">Point limit for redeem</label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($loyality->point_limit_for_redeem) ? $loyality->point_limit_for_redeem : '' ?>" id="point_limit_for_redeem" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group" >
                                    <h4 style="text-align: center;"><strong>Redeem</strong></h4>
                                </div>



                                <div class="form-group">
                                    <label class="control-label col-md-3">Point</label>
                                    <div class="col-md-3">
                                        <input value="<?= !empty($loyality->redm_point) ? $loyality->redm_point : '1' ?>" id="redm_point" type="text" class="form-control" disabled autocomplete="off">
                                        <!--</div>-->
                                    </div>

                                    <label class="control-label col-md-1">=</label>
                                    <label class="control-label col-md-1"><?php echo $this->lang->line("price") ?></label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>
                                            </span>
                                            <input type="text"  class="form-control col-md-2" name="redm_amt" value="<?= !empty($loyality->redm_amt) ? $loyality->redm_amt : '' ?>" id="redm_amt"/> </div>
                                    </div>

                                    <!--<div class="col-md-3">-->

                                </div>





                                <div class="form-group" style="display: none;">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("status") ?></label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="Status">
                                            <option <?= isset($Categories->status) ? $Categories->status == 0 ? 'selected="selected"' : '' : '' ?> value="0"><?php echo $this->lang->line("active") ?></option>
                                            <option <?= isset($Categories->status) ? $Categories->status == 1 ? 'selected="selected"' : '' : '' ?> value="1"><?php echo $this->lang->line("inactive") ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>




                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" class="btn blue" id="SubmitLoyality">
                                            <i class="fa fa-check"></i><?php echo $this->lang->line("submit") ?></button>
                                        <button type="button" onclick="window.location = '<?= base_url('admin/loyality_points'); ?>';" class="btn default"><?php echo $this->lang->line("cancel") ?></button>
                                    </div>
                                </div>
                            </div>

                            <!-- END FORM-->
                    </div>
                    </form>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
