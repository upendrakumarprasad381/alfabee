<?php
$Id = !empty($this->uri->segment(4)) ? base64_decode($this->uri->segment(4)) : '';
$cArray = GetpromocodeBy($Id);
$Session = GetSessionArrayAdmin();

?>

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= empty($cArray->promo_id) ? $this->lang->line("add_new") : $this->lang->line("update"); ?> <?php echo $this->lang->line("offers")?></span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="Id" value="<?= !empty($cArray->promocode_id) ? $cArray->promocode_id : '' ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("offer_code")?></label>
                                    <div class="col-md-4">
                                        <input id="promo_code" value="<?= !empty($cArray->promo_code) ? $cArray->promo_code : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("offer_title")?> <?php echo $this->lang->line("in_english")?></label>
                                    <div class="col-md-4">
                                        <input id="offer_title" value="<?= !empty($cArray->title) ? $cArray->title : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("offer_title")?> <?php echo $this->lang->line("in_arabic")?></label>
                                    <div class="col-md-4">
                                        <input id="offer_title_ar" value="<?= !empty($cArray->title_ar) ? $cArray->title_ar : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                
                                
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3">Discount From</label>
                                    <div class="col-md-4">
                                        <?php
                                        $discount_from=!empty($cArray->discount_from) ? $cArray->discount_from : '';
                                        ?>
                                        <select  id="discount_from" class="form-control" onchange="ADMINCMN.admindiscountTypechnage();">
                                            <option <?= empty($discount_from) ? 'selected' : '' ?> value="0">Order</option>
                                            <option <?= $discount_from=='1' ? 'selected' : '' ?> value="1">Delivery charges</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Discount Type</label>
                                    <div class="col-md-4">
                                        <?php
                                        $discount_type=!empty($cArray->discount_type) ? $cArray->discount_type : '';
                                        ?>
                                        <select  id="discount_type" class="form-control" onchange="ADMINCMN.admindiscountTypechnage();">
                                            <option <?= empty($discount_type) ? 'selected' : '' ?> value="0">Percentage</option>
                                            <option <?= $discount_type=='1' ? 'selected' : '' ?> value="1">Fixed</option>
                                        </select>
                                    </div>
                                </div>
                                
                                
                                
                                

                                <div class="form-group">
                                    <label class="control-label col-md-3" id="distypemsg"><?= empty($discount_type) ? 'Discount in %' : 'Enter amount' ?></label>
                                    <div class="col-md-4">
                                        <input type="number" value="<?= !empty($cArray->discount) ? $cArray->discount : '' ?>" id="discount" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3">Minimum purchase amount</label>
                                    <div class="col-md-4">
                                        <input type="number" value="<?= !empty($cArray->purchase_amount) ? $cArray->purchase_amount : '' ?>" id="purchase_amount" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="form-group" id="maxdisdiv" style="display: <?= !empty($cArray->discount_type) ? 'none' : 'block' ?>;">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("maximum_discount")?></label>
                                    <div class="col-md-4">
                                        <input type="number" value="<?= !empty($cArray->maximum_discount) ? $cArray->maximum_discount : '' ?>" id="maximum_discount" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("date_from")?></label>
                                    <div class="col-md-4">
                                        <input id="date_from" value="<?= !empty($cArray->date_from) && $cArray->date_from != '0000-00-00' ? $cArray->date_from : '' ?>"  type="text" class="form-control datepicker" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("date_to")?></label>
                                    <div class="col-md-4">
                                        <input id="date_to" value="<?= !empty($cArray->date_to) && $cArray->date_to != '0000-00-00' ? $cArray->date_to : '' ?>" type="text" class="form-control datepicker" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Store type</label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="business_type" name="business_type">
                                            <option value="">Choose store type</option>
												    
												<?=get_storetype(isset($cArray->store_type) ? $cArray->store_type : '' )?>
										<select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("restaurant_name")?></label>
                                    <div class="col-md-4">
                                        <?php
                                        if(empty($cArray->store_type))
                                        {
                                        ?>
                                       <select class="form-control selectpicker" data-live-search='true' multiple="multiple" id="rest_id"></select>
                                        <?php
                                        }else{
                                            ?>
                                            <select class="form-control selectpicker" data-live-search='true' multiple="multiple" id="rest_id">
                                        <?php
                                            
                                            $Array = getBusinessName($cArray->store_type);
                    
                                            $menu_id = !empty($cArray->restaurant_id) ? explode(',', $cArray->restaurant_id) : [];
                                               
                                                for ($i = 0; $i < count($Array); $i++) {
                                                    $drt = $Array[$i];
                                                    ?> 
                                                    <option <?= in_array($drt->vendor_id, $menu_id) ? 'selected' : '' ?> value="<?= $drt->vendor_id ?>"><?= $drt->restaurant_name?></option>
                                                    <?php
                                                }
                                            ?>
                                            </select>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="button" class="btn blue" id="addpromocode">
                                                <i class="fa fa-check"></i> <?php echo $this->lang->line("submit")?></button>
                                            <button type="button" onclick="window.location = '<?= base_url('admin/promocode'); ?>';" class="btn default"><?php echo $this->lang->line("cancel")?></button>
                                        </div>
                                    </div>
                                </div>

                                <!-- END FORM-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>





        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN CONTENT -->

