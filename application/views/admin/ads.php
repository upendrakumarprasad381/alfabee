<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert_header" style="display:none;"> <strong>Success!</strong> <span id="success-message"></span> </div>
                    <div class="alert alert-danger alert_header" style="display:none;"> <strong>Error!</strong> <span id="error-message"> </span> </div>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Ads</span>
                            <a href="<?= base_url('Admin/ads/add-new'); ?>" class="btn btn-sm green small"> <?php echo $this->lang->line("add_new")?>
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no")?></th>
                                    <th><?php echo $this->lang->line("image")?></th>
                                    <th><?php echo $this->lang->line("position")?></th>
                                    <th><?php echo $this->lang->line("status")?></th>
                                    <th><?php echo $this->lang->line("action")?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($Categories); $i++) {
                                    $d = $Categories[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><img src="<?=base_url('uploads/ads/'.$d->image)?>" width="100px"/></td>
                                        <td>
                                            <form>
                                                <input style="width:60px;" name="position" type="text" class="form-control position" autocomplete="off"  value=" <?=$d->position?>">
                                                <a class="btn default update_position" table="ads" tableid='<?=$d->id?>'  style="margin-top:-55px;margin-left:71px;"><?php echo $this->lang->line("update")?></a> 
                                                <a class="btn default reset_position" table="ads" tableid='<?=$d->id?>' style="margin-top: -56px;margin-left: 0px;"><?php echo $this->lang->line("reset")?></a>
                                            </form>
                                        </td>
                                        <td><?= $d->status == 0 ? '<span class="label label-sm label-success">'.$this->lang->line("active").'</span>' : '<span class="label label-sm label-danger">'.$this->lang->line("inactive").'</span>' ?></td>
                                        <td>
                                            <a href="<?= base_url('Admin/ads/add-new/' . base64_encode($d->id)) ?>"><span class="label label-sm label-success"><i class="fa fa-pencil" aria-hidden="true"></i></span></a>
                                            <a HomeId="<?= $d->id ?>" class="Homeurl"><span class="label label-sm label-danger"><i class="far fa-trash-alt"></i></span></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
