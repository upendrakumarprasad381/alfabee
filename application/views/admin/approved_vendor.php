<?php
$store_type = Get_AllStoreType();
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert_header" style="display:none;"> <strong>Success!</strong> <span id="success-message"></span> </div>
                <div class="alert alert-danger alert_header" style="display:none;"> <strong>Error!</strong> <span id="error-message"> </span> </div>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Approved Vendor</span>

                        </div>
                    </div>

                    <div class="portlet-body">

                        <div class="portlet light bordered">

                            <div class="caption">
                                <span class="caption-subject font-dark bold uppercase"></span>
                            </div>
                            <ul class="nav nav-tabs">
                                <?php
                                if (!empty($store_type)) {
                                    for ($i = 0; $i < count($store_type); $i++) {
                                        $d = $store_type[$i];
                                        if ($i == 0) {
                                            $class = "active";
                                            $area = "true";
                                        } else {
                                            $class = '';
                                            $area = "false";
                                        }
                                        ?>
                                        <li class="<?= $class ?>">
                                            <a href="#portlet_tab2_<?= $i ?>" data-toggle="tab" aria-expanded="<?= $area ?>"> <?= $d->store_type ?> </a>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>  
                            </ul>

                            <div class="portlet-body">
                                <div class="tab-content">
                                    <?php
                                    if (!empty($store_type)) {
                                        for ($i = 0; $i < count($store_type); $i++) {
                                            $d = $store_type[$i];
                                            if ($i == 0) {
                                                $class = "active";
                                                $area = "true";
                                            } else {
                                                $class = '';
                                                $area = "false";
                                            }
                                            ?>
                                            <div class="tab-pane <?= $class ?>" id="portlet_tab2_<?= $i ?>">
                                                <a target="_blank" href="<?= base_url('admin/vendor_position?storeId='.$d->id) ?>"><button type="button" class="btn btn-success">Position Update</button></a>
                                                <table class="table table-striped table-bordered table-hover DataTableClass">
                                                    <thead>
                                                        <tr>
                                                            <th>Sl</th>
                                                            <th>Business Name</th>
                                                        
                                                            <th>Status</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        //$store_data=Get_VendorList($d->id);
                                                        $qry = "SELECT users.*,restaurant_details.restaurant_name FROM `users` join restaurant_details ON restaurant_details.vendor_id=users.id WHERE is_approved != 0 AND user_type=2 AND users.store_type='$d->id' AND users.archive=0 order by users.position ASC";
                                                        $store_data = $this->Database->select_qry_array($qry);
                                                        for ($j = 0; $j < count($store_data); $j++) {
                                                            $d1 = $store_data[$j];
                                                            ?>
                                                            <tr>
                                                                <td><?= $j + 1; ?></td>
                                                                <td>
                                                                    <?= $d1->restaurant_name ?>

                                                                </td>
                                                               
                                                                <td><?= $d1->status == 0 ? '<span class="label label-sm label-danger">' . $this->lang->line("inactive") . '</span>' : '<span class="label label-sm label-success">' . $this->lang->line("active") . '</span>' ?></td>
                                                                <td>
                                                                    <a href="<?= base_url('Admin/vendor_info/' . base64_encode($d1->id)) ?>" Id="<?= $d->id ?>"><span class="label label-sm label-success"><i class="fas fa-external-link-alt"></i></span></a>
                                                                    <a style="display: none;" href="javascript:void(0)" updatejson='{"archive":"1"}'  condjson='{"id":"<?= $d1->id ?>"}' dbtable="users" class="autoupdate" title="Remove"><span class="label label-sm label-danger"><i class="fas fa-trash-alt"></i></span></a>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>    
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
