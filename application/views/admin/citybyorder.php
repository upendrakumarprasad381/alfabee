
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">City By Order</span>

                        </div>
                    </div>

                    <div class="portlet-body">
                        <form method="GET" action="<?= base_url('admin/transactions') ?>">
                            <div class="row">
                                <div class="col-sm-2">

                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label>Enter your city name</label>
                                        <input name="business-location" id="business-location" value="" type="text" class="form-control " autocomplete="off">
                                        <input type="hidden" id="latitude" name="latitude">
                                        <input type="hidden" id="longitude" name="longitude">

                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Near distance</label>
                                        <input name="distance" value="10" type="number" class="form-control " autocomplete="off">
                                    </div>
                                </div>


                                <div class="col-sm-2">
                                    <label style="width: 100%;">&nbsp; </label>
                                    <input type="submit" name="searchBylatlong" class="btn blue" value="Search">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
