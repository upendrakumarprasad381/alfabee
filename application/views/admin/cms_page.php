<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">

                    <div class="portlet-body form"> 

                        <div class="row">
                             <div class="col-md-12 ">
                                <div class=" " style="padding:15px; padding-bottom:0px;" > 
                                    <form class="form" role="form" method="post">
                                        <?php
                                        if (isset($_POST['updateAbout'])) {
                                            $update = array(
                                                'col1' => $_POST['about'],
                                            );
                                            $cond = array('identify' => 'ABOUT');
                                            $this->Database->update('cms', $update, $cond);
                                        }
                                        $data = GetcmsBycmsId('ABOUT');
                                        ?>
                                        <div class="portlet box grey-cascade" style="margin-bottom: 0px;">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <?php echo $this->lang->line("about_us")?>
                                                </div>
                                                <div class="tools">
                                                    <a href="" class="collapse" data-original-title="" title=""> </a>
                                                    <button name="updateAbout" style="float: left;padding: 1px 6px 2px 6px;margin-right: 7px;" type="submit" class="btn btn-primary"><?php echo $this->lang->line("submit")?></button>
                                                </div> 
                                            </div>
                                            <div class="portlet-body " > 
                                                <div class="row"> 
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label><?php echo $this->lang->line("about_us")?><span style="color:red">*</span></label>
                                                            <textarea class="form-control summernote" rows="3" name="about" placeholder="About Us!"><?=(isset($data->col1) && $data->col1 ? $data->col1:'')?></textarea>
                                                        </div>
                                                    </div>

                                                </div>      
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div> 
                           

                         
                            <div class="col-md-12 ">
                                <div class=" " style="padding:15px; padding-bottom:0px;" > 
                                    <form class="form" role="form" method="post">
                                        <?php
                                        if (isset($_POST['updateTermsPolicy'])) {
                                            $update = array(
                                                'col1' => $_POST['terms'],
                                            );
                                            $cond = array('identify' => 'TERMS_CONDITION');
                                            $this->Database->update('cms', $update, $cond);
                                        }
                                        $data = GetcmsBycmsId('TERMS_CONDITION');
                                        ?>
                                        <div class="portlet box grey-cascade" style="margin-bottom: 0px;">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <?php echo $this->lang->line("terms")?>
                                                </div>
                                                <div class="tools">
                                                    <a href="" class="collapse" data-original-title="" title=""> </a>
                                                    <button name="updateTermsPolicy" style="float: left;padding: 1px 6px 2px 6px;margin-right: 7px;" type="submit" class="btn btn-primary"><?php echo $this->lang->line("submit")?></button>
                                                </div> 
                                            </div>
                                            <div class="portlet-body " > 
                                                <div class="row"> 
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label><?php echo $this->lang->line("terms")?><span style="color:red">*</span></label>
                                                            <textarea class="form-control summernote" rows="3" name="terms" placeholder="Terms & Conditions!"><?=(isset($data->col1) && $data->col1 ? $data->col1:'')?></textarea>
                                                        </div>
                                                    </div>

                                                </div>      
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>                            

                            <div class="col-md-12 ">
                                <div class=" " style="padding:15px; padding-bottom:0px;" > 
                                    <form class="form" role="form" method="post">
                                        <?php
                                        if (isset($_POST['updatePrivacyPolicy'])) {
                                            $update = array(
                                                'col1' => $_POST['privacy'],
                                            );
                                            $cond = array('identify' => 'PRIVACY_POLICY');
                                            $this->Database->update('cms', $update, $cond);
                                        }
                                        $data = GetcmsBycmsId('PRIVACY_POLICY');
                                        ?>
                                        <div class="portlet box grey-cascade" style="margin-bottom: 0px;">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <?php echo $this->lang->line("privacy")?>
                                                </div>
                                                <div class="tools">
                                                    <a href="" class="collapse" data-original-title="" title=""> </a>
                                                    <button name="updatePrivacyPolicy" style="float: left;padding: 1px 6px 2px 6px;margin-right: 7px;" type="submit" class="btn btn-primary"><?php echo $this->lang->line("submit")?></button>
                                                </div> 
                                            </div>
                                            <div class="portlet-body " > 
                                                <div class="row"> 
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label><?php echo $this->lang->line("privacy")?><span style="color:red">*</span></label>
                                                            <textarea class="form-control summernote" rows="3" name="privacy" placeholder="Privacy Policy!"><?=(isset($data->col1) && $data->col1 ? $data->col1:'')?></textarea>
                                                        </div>
                                                    </div>

                                                </div>      
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div> 
                            
                             <div class="col-md-12 ">
                                <div class=" " style="padding:15px; padding-bottom:0px;" > 
                                    <form class="form" role="form" method="post">
                                        <?php
                                        if (isset($_POST['updateFaq'])) {
                                            $update = array(
                                                'col1' => $_POST['faq'],
                                            );
                                            $cond = array('identify' => 'FAQ');
                                            $this->Database->update('cms', $update, $cond);
                                        }
                                        $data = GetcmsBycmsId('FAQ');
                                        ?>
                                        <div class="portlet box grey-cascade" style="margin-bottom: 0px;">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <?php echo $this->lang->line("faq")?>
                                                </div>
                                                <div class="tools">
                                                    <a href="" class="collapse" data-original-title="" title=""> </a>
                                                    <button name="updateFaq" style="float: left;padding: 1px 6px 2px 6px;margin-right: 7px;" type="submit" class="btn btn-primary"><?php echo $this->lang->line("submit")?></button>
                                                </div> 
                                            </div>
                                            <div class="portlet-body " > 
                                                <div class="row"> 
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label><?php echo $this->lang->line("faq")?><span style="color:red">*</span></label>
                                                            <textarea class="form-control summernote" rows="3" name="faq"><?=(isset($data->col1) && $data->col1 ? $data->col1:'')?></textarea>
                                                        </div>
                                                    </div>

                                                </div>      
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                           
                        </div>


                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
