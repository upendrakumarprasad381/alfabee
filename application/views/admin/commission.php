
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?php echo $this->lang->line("commission")?></span>
                            <a href="<?= base_url('admin/commission/add-new'); ?>" class="btn btn-sm green small"> <?php echo $this->lang->line("add_new")?>
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover commontableXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no")?></th>
                                    <th><?php echo $this->lang->line("store_type")?></th>
                                    <th><?php echo $this->lang->line("commission")?></th>
                                     <!--<th><?php echo $this->lang->line("position")?></th>-->
                                    <th><?php echo $this->lang->line("status")?></th>
                                    <th><?php echo $this->lang->line("action")?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($commission); $i++) {
                                    $d = $commission[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= GetNameById($d->store_type,'store_type','store_type') ?></td>
                                        <td><?= $d->percentage ?></td>
                                        
                                        <td><?= $d->status == 0 ? '<span class="label label-sm label-success">'.$this->lang->line("active").'</span>' : '<span class="label label-sm label-danger">'.$this->lang->line("inactive").'</span>' ?></td>
                                        <td>
                                            <a href="<?= base_url('admin/commission/add-new/' . base64_encode($d->id)) ?>"><span class="label label-sm label-success"><i class="fa fa-pencil" aria-hidden="true"></i></span></a> 
                                            <a ref="javascript:void(0)" class="common_delete" Id="<?= $d->id ?>" table="commission" class="autoupdate" redrurl="commission"><span class="label label-sm label-<?= empty($d->archive) ? 'danger' : 'info' ?>"><?= empty($d->archive) ? '<i class="fas fa-ban"></i>' : '<i class="far fa-check-circle"></i>' ?></span></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
