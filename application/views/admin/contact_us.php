
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?php echo $this->lang->line("contact_us")?></span>
                            
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover commontableXXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no")?></th>
                                    <th><?php echo $this->lang->line("full_name")?></th>
                                    <th><?php echo $this->lang->line("email")?></th>
                                    <th><?php echo $this->lang->line("mobile_number")?></th>
                                    <th><?php echo $this->lang->line("message")?></th>
                                    <th><?php echo $this->lang->line("date")?></th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($customer); $i++) {
                                    $d = $customer[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->name ?></td>
                                        <td><a href="mailto:<?= $d->email ?>"><?= $d->email ?></a></td>
                                        <td><?= $d->mobile_number ?></td>
                                        <td><?= $d->message?></td>
                                        <td><?= date('D d M-Y h:i A', strtotime($d->timestamp)); ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
