<?php
$session = GetSessionArrayAdmin();
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Rider</span>
                            <a href="<?= base_url('admin/add_deliveryboy'); ?>" class="btn btn-sm green small"> <?php echo $this->lang->line("add_new") ?>
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no") ?></th>
                                    <th><?php echo $this->lang->line("full_name") ?></th>
                                    <th><?php echo $this->lang->line("email") ?></th>
                                    <th><?php echo $this->lang->line("mobile_number") ?></th>
                                    <th>Commission</th>
                                    <th><?php echo $this->lang->line("status") ?></th>
                                    <th><?php echo $this->lang->line("action") ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cArrayRR = GetRiderCommission();
                                $qry = "SELECT * FROM `users` WHERE user_type=4 AND deliveryboy_added_by='1' AND is_deleted=0  ORDER BY id DESC";
                                $cArray = $this->Database->select_qry_array($qry);
                                for ($i = 0; $i < count($cArray); $i++) {
                                    $d = $cArray[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->name ?></td>
                                        <td><?= $d->email ?></td>
                                        <td><?= $d->mobile_code . $d->mobile_number ?></td>

                                        <td>
                                            <select dboyid="<?= $d->id ?>" onchange="ADMINCMN.updateRiderCommission(this);" style="width: 100px;">
                                                <option value="">select</option>
                                            <?php
                                            for ($j = 0; $j < count($cArrayRR); $j++) {
                                                $dR = $cArrayRR[$j];
                                                ?><option <?= $d->rider_commission_id == $dR->id ? 'selected' : '' ?> value="<?= $dR->id ?>"><?= $dR->com_title ?></option><?php
                                            }
                                            ?>
                                            </select>
                                </td>


                                <td><?= $d->status == 0 ? '<span class="label label-sm label-success">' . $this->lang->line("active") . '</span>' : '<span class="label label-sm label-danger">' . $this->lang->line("inactive") . '</span>' ?></td>
                                <td>
                                    <a data-toggle="tooltip" title="Edit" href="<?= base_url('admin/add_deliveryboy/' . base64_encode($d->id)) ?>"><span class="label label-sm label-success"><i class="fa fa-pencil" aria-hidden="true"></i></span></a>
                                    <a style="display: none;" ref="javascript:void(0)" class="common_delete" Id="<?= $d->id ?>" table="users" redrurl="delivery_boy"><span class="label label-sm label-<?= empty($d->archive) ? 'danger' : 'info' ?>"><?= empty($d->archive) ? '<i class="fas fa-trash-alt"></i>' : '<i class="far fa-check-circle"></i>' ?></span></a>
                                    <a data-toggle="tooltip"  href="<?= base_url('admin/deliveryboydetails?id=' . base64_encode($d->id)) ?>"><span class="label label-sm label-success"><i class="fas fa-external-link-alt"></i></span></a>

                                </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
