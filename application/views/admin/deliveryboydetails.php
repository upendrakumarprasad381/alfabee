<?php
$id = !empty($_REQUEST['id']) ? base64_decode($_REQUEST['id']) : '';
$user = GetUserDetails($id);
if (!empty($user)) {
    $user = $user[0];
} else {
    redirect(base_url('admin'));
}
//echo '<pre>';
//print_r($user);exit;
?>
<style>
    .fasCd {
        opacity: 1 !important;
        line-height: 50px !important;
        margin-left: -5px !important;
        font-size: 50px !important;
    }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption" style="padding: 0px;">
                            <span class="caption-subject font-dark sbold"> <span class="uppercase">delivery boy -  <?= $user->name ?></span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <input type="hidden" id="dboyid" value="<?= $user->id ?>">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="portlet blue-hoki box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i style="font-size: 24px;margin-top: 2px;" class="fas fa-info-circle"></i>delivery boy -  <?= $user->name ?> 
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Name : </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= $user->name ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Email : </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= $user->email ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Phone : </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= $user->mobile_code . $user->mobile_number ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Area : </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= $user->area ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info" style="display: none;">
                                                    <div class="col-md-5 name">Commission Amount: </div>
                                                    <div class="col-md-7 value"> 
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <input type="number" class="form-control" value="<?= $user->commission ?>" id="cmamount">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <a href="javascript:void(0)"><i class="fas fa-sync" onclick="ADMINCMN.deliveryboyUpdateCommission();"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Commission : </div>
                                                    <div class="col-md-4 value"> 
                                                        <select class="form-control selectpicker" dboyid="<?= $user->id ?>" onchange="ADMINCMN.updateRiderCommission(this);" data-live-search="true">
                                                            <option value="">Select</option>
                                                            <?php
                                                            $cArray = GetRiderCommission();
                                                            $rider_commission_id = !empty($user->rider_commission_id) ? $user->rider_commission_id : '';
                                                            for ($i = 0; $i < count($cArray); $i++) {
                                                                $d = $cArray[$i];
                                                                ?><option <?= $rider_commission_id == $d->id ? 'selected' : '' ?> value="<?= $d->id ?>"><?= $d->com_title ?></option><?php
                                                            }
                                                            ?>

                                                        </select>
                                                    </div>
                                                </div>

                                                 <div class="row static-info">
                                                    <div class="col-md-5 name"> Rider Earned Amount : </div>
                                                    <div class="col-md-7 value"> <?= getRiderTotalEarnedAmount($id) ?> PKR</div>
                                                </div>
                                                
                                                
                                                
                                            </div>
                                        </div>
                                    </div>

                                </div>



                            </div>

                        </div>
                    </div>
                </div>
                <!-- End: life time stats -->
            </div>
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Rider order list</span>

                        </div>
                    </div>
 <form method="post" action="">  
                        <div class="panel-group accordion" id="accordion1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="advance_filter accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1" aria-expanded="false"> <?php echo $this->lang->line("advance_filter") ?> </a>
                                    </h4>
                                </div>
                                <div id="collapse_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Order No</label>
                                                <input name="Filterorderno" value="<?= !empty($_POST['Filterorderno']) ? $_POST['Filterorderno'] : '' ?>" type="text" class="form-control " autocomplete="off">
                                            </div>
                                        </div>
                                       

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label> <?php echo $this->lang->line("start_date") ?></label>
                                                <input name="FilterStartDate" value="<?= !empty($_POST['FilterStartDate']) ? $_POST['FilterStartDate'] : '' ?>" type="text" class="form-control datepicker2" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label> <?php echo $this->lang->line("end_date") ?></label>

                                                <input name="FilterEndDate" value="<?= !empty($_POST['FilterEndDate']) ? $_POST['FilterEndDate'] : '' ?>" type="text" class="form-control datepicker2" autocomplete="off">
                                            </div>
                                        </div>


                                       




                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label style="width: 100%;">&nbsp; </label>
                                                <input type="submit" name="Filter" class="btn blue" value=" <?php echo $this->lang->line("search") ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th> <?php echo $this->lang->line("sl_no") ?></th>
                                    <th><?php echo $this->lang->line("order_no") ?></th>

                                    <th> <?php echo $this->lang->line("restaurant_name") ?></th>
                                    <th> <?php echo $this->lang->line("customer_name") ?></th>
                                    <th> <?php echo $this->lang->line("price") ?></th>
                                    <th> Rider Earnings</th>
                                    <th> <?php echo $this->lang->line("order_date") ?></th>

                                    <th> <?php echo $this->lang->line("action") ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                  $Cond = '';
                                if (isset($_POST['Filter'])) {
                                    $data['Form'] = $_POST;
                                    extract($_POST);
                                    if (!empty($FilterStartDate) && !empty($FilterEndDate)) {
                                        $datefrom = date('Y-m-d', strtotime($FilterStartDate));
                                        $dateto = date('Y-m-d', strtotime($FilterEndDate));
                                        $Cond = "AND DATE(O.date) BETWEEN '$datefrom' AND '$dateto'";
                                    } else if (!empty($FilterStartDate)) {
                                        $datefrom = date('Y-m-d', strtotime($FilterStartDate));
                                        $Cond = $Cond . "AND DATE(O.date) >= '$datefrom'";
                                    } elseif (!empty($FilterEndDate)) {
                                        $dateto = date('Y-m-d', strtotime($FilterEndDate));
                                        $Cond = $Cond . "AND DATE(O.date) < '$dateto'";
                                    }

                                   
                                    if (!empty($Filterorderno)) {
                                        $Cond = $Cond . "AND O.orderno LIKE '%$Filterorderno%'";
                                    }
                                   
                                }
                                $select=",U.name AS user_name,RD.restaurant_name";
                                $join = 'LEFT JOIN users U ON U.id=O.user_id LEFT JOIN restaurant_details RD ON RD.vendor_id=O.vendor_id';
                                $qry = "SELECT O.* $select FROM `orders` O $join WHERE O.archive=0 AND O.assign_rider='$id' $Cond order by O.id DESC";

                                $orders = $this->Database->select_qry_array($qry);
                                for ($i = 0; $i < count($orders); $i++) {
                                    $d = $orders[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>

                                        <td><?= $d->orderno ?></td>

                                        <td><?= $d->restaurant_name ?></td>
                                        <td><?= $d->user_name ?></td>
                                        <td>PKR <?= number_format($d->total_price + $d->service_charge, 2) ?></td>
                                        <td>PKR <?= number_format($d->rider_commission, 2) ?></td>
                                        <td><?= date('d/m/Y h:i A', strtotime($d->date)) ?></td>
                                        <td>
                                            <a href="<?= base_url('Admin/OrderDetails/' . base64_encode($d->id)) ?>"><span class="label label-sm label-success"><i class="fa fa-eye" aria-hidden="true"></i></span></a>

                                        </td>

                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>




    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<style>
    .dataTables_info{
        display: none;
    }
</style>