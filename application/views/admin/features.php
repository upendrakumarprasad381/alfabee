<?php
if (isset($_POST['formsubmit'])) {
    $form = $_FILES['agreementform'];
    $FileName = uniqid() . '.' . pathinfo($form['name'], PATHINFO_EXTENSION);

    if (move_uploaded_file($form['tmp_name'], HOME_DIR . 'uploads/banner_images/' . $FileName)) {
        $insert = array(
            'file_name' => $FileName,
        );

       
    }
    $insert['vendor_id']=!empty($_POST['vendor_id']) ? $_POST['vendor_id']: '';
     $this->Database->insert('features', $insert);
}
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Features</span>
                            <a data-toggle="modal" data-target="#myModal" href="javascript:void(0)" class="btn btn-sm green small"> Add-New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Restaurants Name</th>
                                    <!--<th>Logo</th>-->
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $select=",U.name";
                                $join=" LEFT JOIN users U ON U.id=F.vendor_id";
                                $qry = "SELECT F.* $select FROM `features` F $join ORDER BY F.id DESC";
                                $customer = $this->Database->select_qry_array($qry);
                                for ($i = 0; $i < count($customer); $i++) {
                                    $d = $customer[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->name ?></td>
                                        <!--<td><img height="50" width="75" src="<?= base_url('uploads/banner_images/'.$d->file_name) ?>"></td>-->
                                        <td>
                                             <a href="javascript:void(0)" removefile="<?= HOME_DIR.'uploads/banner_images/'.$d->file_name ?>"  condjson='{"id":"<?= $d->id ?>"}' dbtable="features" class="autodelete" title="Remove"><span class="label label-sm label-danger"><i class="fas fa-trash-alt"></i></span></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
              <form method="post" enctype="multipart/form-data">
            <div class="modal-body">
              
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label ">Restaurants Name</label>
                            <select class="form-control selectpicker" name="vendor_id" data-live-search="true" required="">
                                <option value="">select</option>
                                <?php
                                $noteIn="SELECT vendor_id FROM `features`";
                                $qrty = "SELECT id,name FROM `users` WHERE user_type='2' AND id NOT IN ($noteIn) AND archive=0";
                                $dArray = $this->Database->select_qry_array($qrty);
                                for ($i = 0; $i < count($dArray); $i++) {
                                    $d=$dArray[$i];
                                    ?>  <option value="<?= $d->id ?>"><?= $d->name ?></option><?php
                                }
                                ?>
                              
                            </select>
                        </div>

                        <div class="form-group" style="display: none;">
                            <label class="control-label ">Logo</label>
                            <input type="file" name="agreementform" class="form-control">
                        </div>


                    </div>
                </div>
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="formsubmit" class="btn btn-primary">Save changes</button>
            </div>
             </form>
        </div>

    </div>
</div>
