</div>
<!-- END CONTAINER -->


<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> <?= date('Y') ?> &copy; AlfaBee &nbsp;&nbsp;&nbsp;
        <a target="_blank" href="https://alwafaagroup.com">Alwafaa Group</a> 

    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
</div>
<!-- BEGIN QUICK NAV -->
<div class="quick-nav-overlay"></div>
<!-- END QUICK NAV -->
<?php
$Controller = $this->router->fetch_class();
$Method = $this->router->fetch_method();
$segment3 = $this->uri->segment(3);
$segment2 = $this->uri->segment(2);
$sess_value = $this->session->userdata(ADMIN_LANGUAGE_ID);
?>
<script>
    var base_url = '<?= base_url(); ?>';
    var Controller = '<?= $Controller ?>';
    var Method = '<?= $Method ?>';
    var segment3 = '<?= $segment3 ?>';
    var segment2 = '<?= $segment2 ?>';
    var sessionValue = '<?= $sess_value ?>';

</script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?= base_url(); ?>assets/global/scripts/app.min.js" type="text/javascript"></script>


<script src="<?= base_url(); ?>js/additional/jquery-confirm.min.js"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<script src="<?= base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/pages/scripts/DatatableClass.js" type="text/javascript"></script> 
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>  
<script src="<?= base_url(); ?>assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?= base_url(); ?>assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/lobibox-master/js/lobibox.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap-select.min.js"></script>
<script src="https://demo.softwarecompany.ae/home_eats/assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="<?= base_url(); ?>js/additional/jquery-ui.js"></script>

<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<link href="<?= base_url(); ?>assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />


<script src="<?= base_url(); ?>js/admin.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>js/adminnew.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>js/permission.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<?php
if ($segment2 == 'vendor_info') {
   ?>
       <script src="<?= base_url(); ?>js/additional/mapmodel_admin.js" ></script>   
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&libraries=geometry,places&callback=initMap&language=en" ></script>
       <?php
}
if ($segment2 == 'add_deliveryboy' || $segment2 == 'citybyorder') {
    ?>
                                <!--<script src="<?= base_url() ?>js/additional/add_food.js"></script>-->
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&callback=initMap&libraries=geometry,places&language=en"></script>
    <?php
}
?>
<!-- END THEME LAYOUT SCRIPTS -->
<script>
    $(document).ready(function () {
        $('.commontable').DataTable();
        $('.common_table').DataTable({
            buttons: [
                {extend: "excel", className: "buttonsToHide"},
                {extend: "pdf", className: "buttonsToHide"},
            ]
        });
        $('#rest').multiselect({
            enableFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 400,
            dropUp: true,
            buttonWidth: '100%'
        });
    });

</script>
</body>

</html>
<?php if ($segment2 == 'chat') { ?>
    <script>
        //    $(document).ready(function () {
        //        window.setInterval(function () {
        //            chat.refreshUserlist();
        //        }, 2000);
        //
        //    });
        var last_chat_id = 0;
        var chat = (function () {
            var sender_id = $('#sender_id').val();
            var receiver_id = $('#receiver_id').val();
            var user_name = '';
            var IntervalId = '';
            var fn = {};
            fn.init = function () {
                chat.updatechatHistory();
                window.clearInterval(IntervalId);
                IntervalId = window.setInterval(function () {
                    chat.updatechatHistory();
                }, 5000);
            }
            fn.opendialogueBox = function (e) {
                $('#chatboxdialogue').removeClass('chatbox-min');
                receiver_id = $(e).attr('receiverId');
                user_name = $(e).attr('user_name');
                $('#userchatName').html(user_name);
                last_chat_id = 0;
                $('.chat-messages').empty();
                $('#chatbord').show();
                $('#userchatName').attr('href', base_url + 'Admin/user_info/' + btoa(receiver_id));
                chat.init();
            }, fn.closedialogueBox = function () {
                $('#chatbord').hide();
                $('#chatboxdialogue').addClass('chatbox-min');
            }
            fn.updatechatHistory = function () {
                var user_name = $('#user_name').val();
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": base_url + "Services/chat_list",
                    "method": "POST",
                    "headers": {
                        "user-agents": "com.alfabee.com",
                        "authtoken": "Alwafaa123",
                        "user-id": "50",
                        "devicetoken": "5558",
                        "cache-control": "no-cache",
                        "postman-token": "7eb3b02c-006d-ec2f-f3d1-4ac331d9586b"
                    },
                    "data": "{\"sender_id\":\"" + sender_id + "\",\"receiver_id\":\"" + receiver_id + "\",\"chat_id\":\"" + last_chat_id + "\"}"
                }

                $.ajax(settings).done(function (response) {
                    var json = response;
                    var chat_list = json.result.chat_list;
                    for (var i = 0; i < chat_list.length; i++) {
                        var d = chat_list[i];
                        var html = '';
                        if (d.sender_id != receiver_id) {
                            html = html + '<div class="message-box-holder">';
                            html = html + '      <div class="message-box">' + d.message + '</div>';
                            html = html + ' </div>';
                        } else {
                            if (d.message_type == '1') {
                                html = html + ' <div class="message-box-holder">';
                                html = html + '   <div class="message-sender">'
                                html = html + ' <audio controls>';
                                html = html + '<source src="' + d.message + '" type="audio/mpeg">';
                                html = html + ' Your browser does not support the audio element.';
                                html = html + '</audio>';
                                html = html + ' </div>';
                                html = html + ' </div>';
                            } else {
                                html = html + ' <div class="message-box-holder">';
                                html = html + '   <div class="message-sender">' + user_name + '</div>';
                                html = html + '   <div class="message-box message-partner">' + d.message + '</div>';
                                html = html + ' </div>';
                            }
                        }
                        $('.chat-messages').append(html);
                    }
                    last_chat_id = json.result.last_chat_id > 0 ? json.result.last_chat_id : last_chat_id;
                    if (chat_list.length) {
                        chat.chatScrolldown();
                    }
                });
            }
            fn.send_message = function () {
                var chatInput = $('.chat-input').val();
                if (chatInput != '') {
                    var html = '';
                    html = html + '<div class="message-box-holder">';
                    html = html + '      <div class="message-box">' + chatInput + '</div>';
                    html = html + ' </div>';
                    $('.chat-input').val('');
                    $('.chat-messages').append(html);
                    var settings = {
                        "async": true,
                        "crossDomain": true,
                        "url": base_url + "Services/send_message",
                        "method": "POST",
                        "headers": {
                            "user-agents": "com.alfabee.com",
                            "authtoken": "Alwafaa123",
                            "user-id": "50",
                            "devicetoken": "5558",
                            "cache-control": "no-cache",
                            "postman-token": "f66a2c51-d49e-e4a5-31a4-caf863a38f18"
                        },
                        "data": "{\"sender_id\":\"" + sender_id + "\",\"receiver_id\":\"" + receiver_id + "\",\"message\":\"" + chatInput + "\"}"
                    }

                    $.ajax(settings).done(function (response) {
                        var json = response;
                        last_chat_id = json.result.last_chat_id;
                        chat.chatScrolldown();
                    });
                }
            }
            fn.refreshUserlist = function () {
                var data = {function: 'refreshChatlist'};
                $.ajax({
                    data: data,
                    type: 'POST',
                    async: true,
                    url: base_url + "Admin/Helper",
                    success: function (data) {
                        $('.inbox_chat').html(data);
                    }
                });
            }
            fn.autosend_message = function () {
                var key = window.event.keyCode;
                if (key === 13) {
                    chat.send_message();
                    return false;
                }
                else {
                    return true;
                }
            }
            fn.chatScrolldown = function () {
                $('.chat-messages').scrollTop($('.chat-messages')[0].scrollHeight);
            }
            return fn;
        })();

    </script>
<?php } ?>
