<?php
$Session = GetSessionArrayAdmin();
$segment2 = $this->uri->segment(2);
//$admin_notification = notification_count('1'); // $Session->id

$admin_session = $this->session->userdata('Admin');
$ADMIN_PERMISSION_ARRAY = ADMIN_PERMISSION_ARRAY;
?>
<!DOCTYPE html>
<html lang="en">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <title>Alfabee</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <?php
        if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'en' || empty($this->session->userdata(ADMIN_LANGUAGE_ID))) {
            ?>
            <link href="<?= base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="<?= base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
            <?php
        } else {
            ?>
            <link href="<?= base_url() ?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css" />
            <link href="<?= base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css" rel="stylesheet" type="text/css" />
            <?php
        }
        ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
        <!-- BEGIN THEME GLOBAL STYLES -->
        <?php
        if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'en' || empty($this->session->userdata(ADMIN_LANGUAGE_ID))) {
            ?>
            <link href="<?= base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
            <link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
            <?php
        } else {
            ?>
            <link href="<?= base_url() ?>assets/global/css/components-rtl.min.css" rel="stylesheet" id="style_components" type="text/css" />
            <link href="<?= base_url() ?>assets/global/css/plugins-rtl.min.css" rel="stylesheet" type="text/css" />
            <?php
        }
        ?>
        <!-- END THEME GLOBAL STYLES -->

        <link href="<?= base_url() ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <?php
        if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'en' || empty($this->session->userdata(ADMIN_LANGUAGE_ID))) {
            ?>
            <link href="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css"/>
            <?php
        } else {
            ?>
            <link href="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css" rel="stylesheet" type="text/css"/>
            <?php
        }
        ?>
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?= base_url() ?>css/jquery-confirm.min.css"> 
        <?php
        if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'en' || empty($this->session->userdata(ADMIN_LANGUAGE_ID))) {
            ?>
            <!-- BEGIN THEME LAYOUT STYLES -->
            <link href="<?= base_url() ?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
            <link href="<?= base_url() ?>assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="<?= base_url() ?>assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME LAYOUT STYLES -->
            <?php
        } else {
            ?>
            <link href="<?= base_url() ?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
            <link href="<?= base_url() ?>assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="<?= base_url() ?>assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!--            <link href="<?= base_url() ?>assets/layouts/layout/css/layout-rtl.min.css" rel="stylesheet" type="text/css" />
            <link href="<?= base_url() ?>assets/layouts/layout/css/themes/darkblue-rtl.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="<?= base_url() ?>assets/layouts/layout/css/custom-rtl.min.css" rel="stylesheet" type="text/css" />-->
            <?php
        }
        ?>
        <link rel="stylesheet" href="<?= base_url() ?>assets/lobibox-master/dist/css/lobibox.min.css"/>
        <link href="<?= base_url(); ?>assets/css/bootstrap-select.min.css" rel="stylesheet">
        <link href="<?= base_url(); ?>assets/global/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?= base_url(); ?>css/jquery-ui.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
        <style>
            .img-size-common {
                color: #f01818 !important;
                font-size: 66% !important;
                font-weight: 600 !important;
            }
            .newmessage{
                color: #d9b214;
                font-size: 16px;
                text-decoration: underline;
            }
        </style>

        <!--<link rel="shortcut icon" href="<?= base_url() ?>favicon.ico" /> -->
        <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>images/favicons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>images/favicons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>images/favicons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>images/favicons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>images/favicons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>images/favicons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>images/favicons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>images/favicons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>images/favicons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url() ?>images/favicons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>images/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>images/favicons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>images/favicons/favicon-16x16.png">
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="<?= base_url('admin') ?>">
                            <img src="<?= base_url() ?>assets/layouts/layout/img/logo.png" alt="logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>

                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <li class="nav-item dropdown"> 

                                <div class="lang_adm" style="display:none;">
                                    <form method="POST" action="<?= base_url() ?>admin/set_lang">
                                        <?php
                                        $url_value = $this->uri->segment(1);
                                        if (!empty($this->uri->segment(2))) {
                                            $url_value .= '/' . $this->uri->segment(2);
                                        }
                                        if (!empty($this->uri->segment(3))) {
                                            $url_value .= '/' . $this->uri->segment(3);
                                        }
                                        ?>
                                        <input type="hidden" id="url"  value="<?php echo $url_value; ?>" name='url'>  
                                        <select id="choose_lang" name="choose_lang" onchange="this.form.submit()">
                                            <option value="en" <?php if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'en') { ?>selected<?php } ?> >English</option>
                                            <option class="__ar" value="ar"  <?php if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'ar') { ?>selected<?php } ?>>ﺍﻟﻌﺮﺑﻴﺔ</option></select>

                                    </form>


                                </div>

                            </li>
                            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default"> <headerNotifactionCount>0</headerNotifactionCount> </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="external">
                                        <h3>
                                            <span class="bold"><headerNotifactionCount>0</headerNotifactionCount> </span> <?php echo $this->lang->line("new_notifications") ?></h3>
                                        <!--<a href="page_user_profile_1.html">view all</a>-->
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" id="notilistdf" style="height: 250px;" data-handle-color="#637283">
                                            <?php
                                          
                                            ?>

                                        </ul>
                                    </li>
                                </ul>
                            </li>



                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="<?= base_url() ?>assets/layouts/layout/img/avatar3_small.jpg" />
                                    <span class="username username-hide-on-mobile"> <?= $Session->name ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <!--<li>-->
                                    <!--    <a href="">-->
                                    <!--        <i class="icon-user"></i> My Profile </a>-->
                                    <!--</li>-->
                                    <li>
                                        <a href="<?= base_url('Login/AdminLoginout'); ?>">
                                            <i class="icon-key"></i> <?php echo $this->lang->line("logout") ?> </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->




            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <div class="page-sidebar-wrapper">
                    <div class="page-sidebar navbar-collapse collapse">
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <?php
                            $menu = GetMenus();
                            if (!empty($menu)) {
                                for ($i = 0; $i < count($menu); $i++) {
                                    $d = $menu[$i];
                                    if (!in_array($admin_session->id, $ADMIN_PERMISSION_ARRAY)) {
                                        $usermenu = GetUserMenu($d->id, $admin_session->id);
                                    } else {
                                        $usermenu = 'Admin';
                                    }
                                    // $usermenu=GetUserMenu($d->menu_id,$admin_session->id);
                                    // print_r($admin_session->id);
                                    if (!empty($usermenu)) {
                                        ?>
                                        <li class="nav-item start <?php
                                        if ('Admin/' . $segment2 == $d->link || ($d->link == "Admin" && $segment2 == "")) {
                                            echo "active";
                                            echo " activeclass";
                                        }
                                        ?> liclass<?= $i ?>">
                                            <a href="<?= base_url($d->link); ?>" class="nav-link nav-toggle">
                                                <?= $d->icon ?>
                                                <span class="title"><?= $d->label ?></span>
                                                <?php if ($d->toggle == '1') { ?>
                                                    <span class="arrow arrowclass<?= $i ?>"></span>
                                                <?php } ?>
                                            </a>
                                            <?php
                                            $submenu = GetSubMenu($d->id);
                                            if (!empty($submenu)) {
                                                ?>
                                                <ul class="sub-menu ulclass<?= $i ?>">
                                                    <?php
                                                    for ($j = 0; $j < count($submenu); $j++) {
                                                        $dn = $submenu[$j];
                                                        if (!in_array($admin_session->id, $ADMIN_PERMISSION_ARRAY)) {
                                                            $usermenusub = GetUserMenu($dn->id, $admin_session->id);
                                                        } else {
                                                            $usermenusub = 'Admin';
                                                        }
                                                        //  $usermenusub=GetUserMenu($dn->menu_id,$admin_session->id);
                                                        // print_r($usermenusub);
                                                        if (!empty($usermenusub)) {
                                                            ?>
                                                            <li class="nav-item <?php
                                                            if ('Admin/' . $segment2 == $dn->link) {
                                                                echo "active";
                                                                echo " activeclass";
                                                            }
                                                            ?>" id="<?= $i ?>">
                                                                <a href="<?= base_url($dn->link); ?>" class="nav-link">
                                                                    <span class="title"><?= $dn->label ?></span>
                                                                </a>
                                                            </li>
                                                        <?php
                                                        }
                                                    }
                                                    ?>
                                                </ul>    
                                        <?php } ?>    
                                        </li>
                                        <?php
                                    }
                                }
                            }
                            ?>
                            <!--<li class="nav-item start liclass12">-->
                            <!--    <a  class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-users"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("homelyfood_user") ?></span>-->
                            <!--        <span class="arrow arrowclass12"></span>-->
                            <!--    </a>-->
                            <!--    <ul class="sub-menu ulclass12">-->
                            <!--    <li class="nav-item " id="12">-->
                            <!--        <a href="<?= base_url() ?>admin/vendor" class="nav-link">-->
                            <!--            <span class="title"><?php echo $this->lang->line("new_homelyfood_user") ?></span>-->
                            <!--        </a>-->
                            <!--    </li>-->
                            <!--    <li class="nav-item " id="12">-->
                            <!--        <a href="<?= base_url() ?>admin/approved_vendor" class="nav-link">-->
                            <!--            <span class="title"><?php echo $this->lang->line("approved_homelyfood_user") ?></span>-->
                            <!--        </a>-->
                            <!--    </li>-->
                            <!--</ul> -->
                            <!--</li>-->
                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('admin/home_page'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-home"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("homepage_manage") ?></span>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('admin/cms_page'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-home"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("cms_page_manage") ?></span>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!-- <li class="nav-item start liclass12">-->
                            <!--    <a class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-home"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("cms_page_manage") ?></span>-->
                            <!--         <span class="arrow arrowclass12"></span>-->
                            <!--    </a>-->
                            <!--    <ul class="sub-menu ulclass12">-->
                            <!--    <li class="nav-item " id="12">-->
                            <!--        <a href="<?= base_url() ?>admin/about_us" class="nav-link">-->
                            <!--            <span class="title"><?php echo $this->lang->line("about_us") ?></span>-->
                            <!--        </a>-->
                            <!--    </li>-->
                            <!--    <li class="nav-item " id="12">-->
                            <!--        <a href="<?= base_url() ?>admin/terms_condition" class="nav-link">-->
                            <!--            <span class="title"><?php echo $this->lang->line("terms") ?></span>-->
                            <!--        </a>-->
                            <!--    </li>-->
                            <!--    <li class="nav-item " id="12">-->
                            <!--        <a href="<?= base_url() ?>admin/privacy" class="nav-link">-->
                            <!--            <span class="title"><?php echo $this->lang->line("privacy") ?></span>-->
                            <!--        </a>-->
                            <!--    </li>-->
                            <!--</ul> -->
                            <!--</li>-->
                            <!--<li class="nav-item start liclass12">-->
                            <!--    <a class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-home"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("featured_deal") ?></span>-->
                            <!--         <span class="arrow arrowclass12"></span>-->
                            <!--    </a>-->
                            <!--    <ul class="sub-menu ulclass12">-->
                            <!--    <li class="nav-item " id="12">-->
                            <!--        <a href="<?= base_url() ?>admin/home_slider" class="nav-link">-->
                            <!--            <span class="title"><?php echo $this->lang->line("main_slider") ?></span>-->
                            <!--        </a>-->
                            <!--    </li>-->
                            <!--    <li class="nav-item " id="12">-->
                            <!--        <a href="<?= base_url() ?>admin/sub_slider" class="nav-link">-->
                            <!--            <span class="title"><?php echo $this->lang->line("sub_slider") ?></span>-->
                            <!--        </a>-->
                            <!--    </li>-->
                            <!--    <li class="nav-item " id="12">-->
                            <!--        <a href="<?= base_url() ?>admin/side_banner" class="nav-link">-->
                            <!--            <span class="title"><?php echo $this->lang->line("side_banner") ?></span>-->
                            <!--        </a>-->
                            <!--    </li>-->
                            <!--</ul> -->
                            <!--</li>-->
                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('admin/customers'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-user"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("customers") ?></span>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('admin/store_type'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="far fa-calendar-check"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("store_type") ?></span>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('admin/category'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fa fa-list"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("categories") ?></span>-->
                            <!--    </a>-->
                            <!--</li>-->

                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('admin/occasion'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="far fa-calendar-check"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("occasion") ?></span>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('admin/cuisine'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-utensils"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("cuisines") ?></span>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('admin/orders'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-shopping-cart"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("orders") ?></span>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('admin/party_orders'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-shopping-cart"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("party_orders") ?></span>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('admin/promocode'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-percent"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("promo_code") ?></span>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!-- <li class="nav-item start">-->
                            <!--    <a href="<?= base_url('admin/commission'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-tags"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("manage_commission") ?></span>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('admin/reviews'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-comments"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("reviews") ?></span>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!-- <li class="nav-item start">-->
                            <!--    <a href="<?= base_url('admin/transactions'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-coins"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("transactions") ?></span>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('admin/sponsored_ad'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-audio-description"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("sponsored") ?></span>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('admin/contact_us'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-phone"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("contact_us") ?></span>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('admin/suggest_homebusiness'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fa fa-search"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("suggested_homebusiness") ?></span>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!--<li class="nav-item start liclass12">-->
                            <!--    <a  class="nav-link nav-toggle">-->
                            <!--        <i class="fab fa-app-store"></i>-->
                            <!--        <span class="title"><?php echo $this->lang->line("app_settings") ?></span>-->
                            <!--        <span class="arrow arrowclass12"></span>-->
                            <!--    </a>-->
                            <!--    <ul class="sub-menu ulclass12">-->
                            <!--        <li class="nav-item start">-->
                            <!--        <a href="<?= base_url('admin/filter'); ?>" class="nav-link nav-toggle">-->
                            <!--            <i class="fas fa-sort-amount-up"></i>-->
                            <!--            <span class="title"><?php echo $this->lang->line("mobile_filter") ?></span>-->
                            <!--        </a>-->
                            <!--        </li>-->
                            <!--    </ul> -->
                            <!--</li>-->



                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->