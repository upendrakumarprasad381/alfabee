<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>alfabee</title>
    </head>
    <body style="background:#999;" onload="window.print()">
        <?php
        error_reporting(0);
        $orderId = !empty($_REQUEST['orderId']) ? base64_decode($_REQUEST['orderId']) : '';
        $ord = GetOrderById($orderId);
        if(empty($ord)){
            redirect(base_url());
        }
        $ord = !empty($ord) ? $ord[0] : '';

        $curl = curl_init();
        $post = array(
            'order_id' => $orderId,
            'user_id' => '',
            'user_type' => '2',
        );
        curl_setopt_array($curl, array(
            CURLOPT_URL => base_url() . "Services/OrderDetails",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($post),
            CURLOPT_HTTPHEADER => array(
                "authtoken: Alwafaa123",
                "cache-control: no-cache",
                "postman-token: cf2c3f9e-4722-bebd-d6ca-56e6c4ca2166",
                "user-agents: com.alfabee.com"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
            exit;
        }
        $details = json_decode($response, true);
        if (empty($details['response']['status'])) {
            echo 'some thing went wrong';
            exit;
        }
        $user = GetusersById($ord->user_id);

        $orders = $details['result']['orders'];
        $order_details = !empty($orders['order_details']) ? $orders['order_details'] : '';
        $delivery_address = !empty($orders['delivery_address']) ? $orders['delivery_address'] : '';
        ?>
        <table width="277" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="
               padding-right:10px;
               padding-left:10px;">
            <tr>
                <td>
                    <table bgcolor="#FFFFFF" width="100%" align="center" cellpadding="0" cellspacing="0" dir="ltr">
                        <tr>
                            <td align="center" valign="middle" style="padding:10px 0px; color:#000; "><img width="150" src="<?= base_url('images/poslogo.png') ?>" /></td>
                        </tr>
                        <tr></tr>
                        <tr>
                            <td align="center" style="font-family:Tahoma, Geneva, sans-serif; font-size:15px; 
                                padding:5px 0px; font-weight:bold; color:#000;">Order Invoice</td>
                        </tr>

                        <tr>
                            <td align="center" style="font-family:Tahoma, Geneva, sans-serif; font-size:12px; 
                                padding:2px 0px;  color:#000; border-top:1px solid #000;
                                ">Order ID: #<?= !empty($order_details['orderno']) ? $order_details['orderno'] : '' ?><br />
                                Order Date: <?= !empty($order_details['order_date']) ? date('d/m/Y', strtotime($order_details['order_date'])) : '' ?></td>
                        </tr>




                        <tr>
                            <td style="font-family:Tahoma, Geneva, sans-serif; font-size:11px; 
                                padding:5px 0px; font-weight:bold; color:#242424; text-align:center 
                                ">
                                <div style=" 
                                     text-transform:uppercase;
                                     text-decoration:underline;
                                     color:#000;">Customer</div>

                            </td>
                        </tr>

                        <tr>
                            <td align="center" valign="top" style="border-bottom:#444444 dashed 1px;  font-family:Tahoma, Geneva, sans-serif; font-size:11px; 
                                padding:0px 0px 5px; color:#000; text-transform:uppercase; "><?= !empty($user->name) ? $user->name : 'Invalid users' ?><br />
                                <?= !empty($user->email) ? $user->email : 'invalid@gmail.com' ?><br />
                                <?= !empty($user->mobile_number) ? $user->mobile_code . $user->mobile_number : (!empty($delivery_address['mobile_number']) ? $delivery_address['mobile_code'] . $delivery_address['mobile_number'] : '') ?></td>
                        </tr>

                        <tr>
                            <td style="font-family:Tahoma, Geneva, sans-serif; font-size:11px; 
                                padding:5px 0px; font-weight:bold; color:#242424; text-align:center 
                                ">
                                <div style="
                                     text-decoration:underline; text-transform:uppercase;
                                     color:#000;">DeliveR To</div>

                            </td>
                        </tr>

                        <tr>
                            <td align="center" valign="top" style="

                                font-family:Tahoma, Geneva, sans-serif;
                                font-size:11px; 
                                padding:0px 0px 10px; color:#000; 
                                ">

                                Building Name/No:<?= !empty($delivery_address['building']) ? $delivery_address['building'] : '' ?>, Street: <?= !empty($delivery_address['street']) ? $delivery_address['street'] : '' ?><br />
                                Building Type:
                                <?php
                                if ($delivery_address['building'] == 1) {
                                    $type = 'Apartment';
                                } elseif ($delivery_address['building'] == 2) {
                                    $type = 'House';
                                } else {
                                    $type = 'Office';
                                }
                                ?>
                                <?= $type ?>, Mobile: <?= !empty($delivery_address['mobile_code']) ? $delivery_address['mobile_code'] . $delivery_address['mobile_number'] : '' ?>,<br />
                                <?= !empty($delivery_address['address']) ? $delivery_address['address'] : '' ?>,<br />

                            </td>
                        </tr>

                        <tr>
                            <td align="center" valign="top" style="
                                font-family:Tahoma, Geneva, sans-serif;
                                font-size:11px; 
                                padding:0px 0px 10px; 
                                color:#000; 
                                "><table width="100%" cellspacing="0" cellpadding="0"  >
                                    <tr>
                                        
                                        <td width="171" height="20" align="center" valign="middle"  style=" 
                                            font-family:Tahoma, Geneva, sans-serif;
                                            font-size:11px; 
                                            border-bottom:#444444 solid 1px; 	
                                            border-top:#444444 solid 1px; 	
                                            color:#000;"><strong>Item</strong></td>
                                            <td width="20" height="20" align="left" valign="middle" style=" 
                                            font-family:Tahoma, Geneva, sans-serif;
                                            font-size:11px; 
                                            border-top:#444444 solid 1px; 	
                                            border-bottom:#444444 solid 1px; 	
                                            color:#000;"
                                            ><strong>Qty</strong></td>
                                        <td width="37" align="right" valign="middle"  style=" 
                                            font-family:Tahoma, Geneva, sans-serif;
                                            font-size:11px; 
                                            border-top:#444444 solid 1px; 	
                                            border-bottom:#444444 solid 1px; 	
                                            color:#000;">&nbsp;</td>
                                        <td width="43" height="20" align="right" valign="middle"  style=" 
                                            font-family:Tahoma, Geneva, sans-serif;
                                            font-size:11px; 
                                            border-top:#444444 solid 1px; 	
                                            border-bottom:#444444 solid 1px; 	
                                            color:#000;"><strong>Price</strong></td>
                                    </tr>
                                    <?php
                                    $product_details=!empty($orders['product_details']) ? $orders['product_details'] : [];
                                   for($i=0;$i<count($product_details);$i++){
                                       $d=$product_details[$i];
                                    ?>
                                    <tr>
                                      
                                        <td align="left" valign="middle" style="color:#000; font-size:12px; padding-top:5px;"><?= !empty($d['product_name']) ? $d['product_name'] : '' ?></td>
                                          <td align="left" valign="middle" style="padding-top:5px;" ><?= !empty($d['quantity']) ? $d['quantity'] : '' ?></td>
                                        <td align="center" valign="middle" style="padding-top:5px;" >&nbsp;</td>
                                        <td align="right" valign="middle" style="padding-top:5px;" ><?= !empty($d['price']) ? $d['price'] : '' ?></td>
                                    </tr>
                                   <?php } ?>
                                    
                                    <tr>
                                        <td colspan="2" align="right" valign="middle" style="padding-top:5px; border-top:#000 1px solid;" >Sub Total</td>
                                        <td colspan="2" align="right" valign="middle" style="padding-top:5px; border-top:#000 1px solid;" >PKR <?= DecimalAmount($orders['subtotal']) ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right" valign="middle" style="padding-top:5px;" >Delivery Charge</td>
                                        <td colspan="2" align="right" valign="middle" style="padding-top:5px;" >PKR <?= DecimalAmount($orders['delivery_fee']) ?></td>
                                    </tr>
                                    <?php if($orders['discount'] > 0){ ?>
                                    <tr>
                                        <td colspan="2" align="right" valign="middle" style="padding-top:5px;" >Discount Amount</td>
                                        <td colspan="2" align="right" valign="middle" style="padding-top:5px;" >PKR <?= DecimalAmount($orders['discount']) ?></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td colspan="2" align="right" valign="middle" style="padding-top:5px;  border-top:#000 1px solid; "  ><strong>Total</strong></td>
                                        <td colspan="2" align="right" valign="middle" style="padding-top:5px; border-top:#000 1px solid;" ><strong>PKR <?= DecimalAmount($orders['total_amount']) ?> </strong></td>
                                    </tr>


                                </table>


                                <table width="100%" cellspacing="0" cellpadding="0">

                                    <td colspan="2" align="right" valign="middle" style="color:#464646; padding:15px 0; font-size:11px; text-align: center;"> 
                                        Thank You For Your Order

                                    </td>
                                    </tr>
                                </table>



                            </td>
                        </tr>


                    </table>

                </td>
            </tr>
        </table>


    </body>
</html>
