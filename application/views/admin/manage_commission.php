<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                        
                            <span class="caption-subject bold uppercase"><?php echo $this->lang->line("commission")?></span>
                        </div>
                    </div>
                 
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="Id" value="<?= !empty($commission->id)?$commission->id:'' ?>">
                            <div class="form-body">
                                 <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("store_type")?> </label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="store_type" name="store_type" >
        							     <option value="">Choose Type</option>
        							    <?= get_storetype(isset($commission->store_type) ? $commission->store_type : '' )?>
        							</select>
        							</div>
        						</div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("commission")?></label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($commission->percentage) ? $commission->percentage : '' ?>" id="commission" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Status</label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="Status">
                                            <option <?= isset($commission->status) ? $commission->status == 0 ? 'selected="selected"' : '' : '' ?> value="0">Active</option>
                                            <option <?= isset($commission->status) ? $commission->status == 1 ? 'selected="selected"' : '' : '' ?> value="1">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                               



                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="button" class="btn blue" id="SubmitCommission">
                                                <i class="fa fa-check"></i> <?php echo $this->lang->line("submit")?></button>
                                            <button type="button" onclick="window.location = '<?= base_url('admin/manage_commission'); ?>';" class="btn default"><?php echo $this->lang->line("cancel")?></button>
                                        </div>
                                    </div>
                                </div>

                                <!-- END FORM-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>





        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
