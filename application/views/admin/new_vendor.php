<style>
    #loader {      
        display: none;      
        position: fixed;      
        z-index: 999;      
        height: 2em;      
        width: 2em;      
        overflow: show;      
        margin: auto;      
        top: 0;      
        left: 0;      
        bottom: 0;      
        right: 0;    
   }    
   #loader:before {      
        content: '';      
        display: block;      
        position: fixed;      
        top: 0;      
        left: 0;      
        width: 100%;      
        height: 100%;      
        background-color: rgba(0,0,0,0.3);    
   }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                 <div id="loader" style="display:none;"><img src="<?= base_url('images/loading.gif')?>"/></div>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">New Vendor</span>
                            
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover commontableXXXX DataTableClass"> 
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no")?></th>
                                    <th><?php echo $this->lang->line("full_name")?></th>
                                    <th>Business Name</th>
                                    <th>Store Type</th>
                                    <th><?php echo $this->lang->line("email")?></th>
                                    <th><?php echo $this->lang->line("mobile_number")?></th>
                                    <th><?php echo $this->lang->line("action")?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($Vendor); $i++) {
                                    $d = $Vendor[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->name ?></td>
                                        <td><?= $d->restaurant_name?></td>
                                        <td><?= GetNameById($d->store_type,'store_type','store_type')?></td>
                                        <td><?= $d->email ?></td>
                                        <td><?= $d->mobile_number ?></td>
                                        <td>
                                            <a href="<?= base_url('Admin/vendor_info/' . base64_encode($d->id)) ?>" Id="<?= $d->id ?>"><span class="label label-sm label-success"><i class="fa fa-eye" aria-hidden="true" title="View" ></i></span></a>
                                            <a redrurl="vendor" class="commonApprove" table="users" Id="<?= $d->id ?>"><span class="label label-sm label-success"><i class="fa fa-check" aria-hidden="true" title="Approve" ></i></span></a> 
                                            <a redrurl="vendor" class="commonReject" table="users" Id="<?= $d->id ?>"><span class="label label-sm label-danger"><i class="fas fa-ban" title="Reject"></i></span></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
