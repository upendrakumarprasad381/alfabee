<?php
$message = "";
if (isset($_POST['sendnotify'])) {
    $cuscond = !empty($_POST['customerscond']) ? $_POST['customerscond'] : [];
    $message = !empty($_POST['message']) ? $_POST['message'] : '';
    $subject = !empty($_POST['subject']) ? $_POST['subject'] : '';
    $conds = '';


    if (!empty($cuscond)) {
        if (!in_array('ALL', $cuscond)) {
            $conds = $conds . " AND id IN (" . implode(',', $cuscond) . ")";
        }
    }
    $qry = "SELECT CONCAT(name,' ',last_name) AS customer_name,id,device_id FROM `users` WHERE user_type=3 AND archive=0 AND device_id!='' $conds AND name!=''";
    $cArray = $this->Database->select_qry_array($qry);


    $dev1 = [];
    $dev2 = [];
    $dev3 = [];
    $dev4 = [];
    $dev5 = [];
    $counter = 0;
    for ($i = 0; $i < count($cArray); $i++) {
        $d = $cArray[$i];

        $data = reformateMydeviceId([$d->device_id]);

        for ($k = 0; $k < count($data); $k++) {
            if (!empty($data[$k]) && $data[$k] != '[]') {
                $counter++;
                if ($counter < 1000) {
                    $dev1[] = $data[$k];
                } else if ($counter < 2000) {
                    $dev2[] = $data[$k];
                } else if ($counter < 3000) {
                    $dev3[] = $data[$k];
                } else if ($counter < 4000) {
                    $dev4[] = $data[$k];
                } else if ($counter < 5000) {
                    $dev5[] = $data[$k];
                }
            }
        }
    }


    send_pushnotification_public($dev1, $subject, $message, $argument = []);
    send_pushnotification_public($dev2, $subject, $message, $argument = []);
    send_pushnotification_public($dev3, $subject, $message, $argument = []);
    send_pushnotification_public($dev4, $subject, $message, $argument = []);
    $message = "Message sent successfully";
}

$qry = "SELECT name AS customer_name,id,device_id FROM `users` WHERE user_type=3 AND archive=0 AND name!=''";
$cArray = $this->Database->select_qry_array($qry);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">

                    <div class="portlet-body form"> 

                        <div class="row">
                            <div class="col-md-12">
                                <div class=" " style="padding:15px; padding-bottom:0px;" > 
                                    <form class="form" role="form" method="post">

                                        <div class="portlet box green" style="margin-bottom: 0px;">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    Send Notification
                                                </div>
                                                <div class="tools">
                                                    <a href="" class="collapse" data-original-title="" title=""> </a>
                                                    <button name="sendnotify" style="float: left;padding: 1px 6px 2px 6px;margin-right: 7px;" type="submit" class="btn btn-primary">Send</button>
                                                </div> 
                                            </div>
                                            <div class="portlet-body " > 
                                                <div class="row"> 


                                                    <?php
                                                    if (!empty($message)) {
                                                        ?>
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-success" role="alert">
                                                                <?= $message ?>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Customers <span style="color:red">*</span></label>
                                                            <select class="form-control selectpicker" required name="customerscond[]" multiple="multiple" data-live-search="true">
                                                                <option value="ALL">Select All</option>
                                                                <?php
                                                                for ($i = 0; $i < count($cArray); $i++) {
                                                                    $fgy = $cArray[$i];
                                                                    ?><option value="<?= $fgy->id ?>"><?= $fgy->customer_name ?></option><?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Subject</label>
                                                            <input type="text" class="form-control" required name="subject">

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Message <span style="color:red">*</span></label>
                                                            <textarea class="form-control" name="message" required rows="4"></textarea>

                                                        </div>
                                                    </div>




                                                </div>      
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>


                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
