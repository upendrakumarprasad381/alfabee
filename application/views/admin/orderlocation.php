<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
        <meta charset="utf-8">
           <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>images/favicons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>images/favicons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>images/favicons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>images/favicons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>images/favicons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>images/favicons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>images/favicons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>images/favicons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>images/favicons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url() ?>images/favicons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>images/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>images/favicons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>images/favicons/favicon-16x16.png">
        <title>Order location</title>
        <style>
            html, body {
                height: 100%;
                margin: 0;
                padding: 0;
            }
            #map {
                height: 100%;
                width: 100%;
                height: 100%;
            }
            .sdcfsd {
                height: 100vh; /* For 100% screen height */
            }
        </style>
    </head>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" style="background: none;">
        <div class="page-wrapper">
            <div class="page-container" style="margin-top: unset;">
                <manageinvoice> </manageinvoice>
            </div>
        </div>
        <form method="get" action="">
            <div class="col-sm-12" style="margin-top: 13px;">
                <div class="col-sm-2">

                    <div class="form-group">
                        <label>location <span style="    color: red;">*</span></label>
                        <input id="business-location" name="address" value="<?= !empty($_GET['address']) ? $_GET['address'] : '' ?>" type="text" class="form-control " required autocomplete="off">
                        <input type="hidden" id="latitude" name="latitude" value="<?= !empty($_GET['latitude']) ? $_GET['latitude'] : '' ?>">
                        <input type="hidden" id="longitude" name="longitude" value="<?= !empty($_GET['longitude']) ? $_GET['longitude'] : '' ?>">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Date From</label>
                        <input name="dateFrom"  type="date" value="<?= !empty($_GET['dateFrom']) ? $_GET['dateFrom'] : '' ?>" class="form-control " autocomplete="off">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Date To</label>
                        <input name="dateTo"  type="date" value="<?= !empty($_GET['dateTo']) ? $_GET['dateTo'] : '' ?>" class="form-control" autocomplete="off">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Radius</label>
                        <input name="radius"  type="number" value="<?= !empty($_GET['radius']) ? $_GET['radius'] : '1' ?>" class="form-control" autocomplete="off">
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <input style="    margin-top: 22px;" type="submit" id="Filter" name="Filter" class="btn blue" value="Search">
                    </div>
                </div>
            </div>
        </form>
        <div id="map"></div>
    </body>
    <?php
    $Controller = $this->router->fetch_class();
    $Method = $this->router->fetch_method();
    $segment3 = $this->uri->segment(3);
    ?>
    <script>
        var base_url = '<?= base_url(); ?>';
        var Controller = '<?= $Controller ?>';
        var Method = '<?= $Method ?>';
        var segment3 = '<?= $segment3 ?>';
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&callback=initMap&libraries=geometry,places&language=en"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?= base_url() ?>assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
    <script src="<?= base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
    <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css">
    <!--<script src="<?= base_url(); ?>js/common.js"></script>-->

    <link rel="stylesheet" href="<?= base_url() . 'css/' ?>AdminMain.css">

    <div id="directions-panel"></div>
    <script>
        var sessionValue = '';
        var base_url = '<?= base_url(); ?>';
        var map;
        var iconImage = new google.maps.MarkerImage('mapIcons/marker_red.png',
                // This marker is 20 pixels wide by 34 pixels tall.
                new google.maps.Size(20, 34),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is at 9,34.
                new google.maps.Point(9, 34));
        var iconShadow = new google.maps.MarkerImage('http://www.google.com/mapfiles/shadow50.png',
                // The shadow image is larger in the horizontal dimension
                // while the position and offset are the same as for the main image.
                new google.maps.Size(37, 34),
                new google.maps.Point(0, 0),
                new google.maps.Point(9, 34));
        // Shapes define the clickable region of the icon.
        // The type defines an HTML &lt;area&gt; element 'poly' which
        // traces out a polygon as a series of X,Y points. The final
        // coordinate closes the poly by connecting to the first
        // coordinate.
        var iconShape = {
            coord: [9, 0, 6, 1, 4, 2, 2, 4, 0, 8, 0, 12, 1, 14, 2, 16, 5, 19, 7, 23, 8, 26, 9, 30, 9, 34, 11, 34, 11, 30, 12, 26, 13, 24, 14, 21, 16, 18, 18, 16, 20, 12, 20, 8, 18, 4, 16, 2, 15, 1, 13, 0],
            type: 'poly'
        };
        var icons = new Array();
        icons["red"] = new google.maps.MarkerImage("mapIcons/marker_red.png",
                // This marker is 20 pixels wide by 34 pixels tall.
                new google.maps.Size(20, 34),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is at 9,34.
                new google.maps.Point(9, 34));
        var infowindow = new google.maps.InfoWindow(
                {
                    size: new google.maps.Size(150, 50)
                });




        $(document).ready(function () {
            $('body').on('click', '.ViewReport', function () {
                var SalesId = $(this).attr('SalesId');
                var data = {function: 'ViewReportInMap', SalesId: SalesId};
                $.ajax({
                    url: base_url + "Admin/Helper",
                    data: data,
                    async: false,
                    type: 'POST',
                    success: function (data) {
                        $('manageInvoice').html(data);
                        setTimeout(function () {
                            $('body').addClass('page-quick-sidebar-open');
                        }, 200);

                    }
                });
            });
            $('body').on('click', '.QuickSettingsClose', function () {
                $('body').removeClass('page-quick-sidebar-open');
                setTimeout(function () {
                    $('manageInvoice').html('');
                }, 200);
            });
            MapSetMarker();
            $('body').on('click', '#SalesRoadMap', function () {
                displayRoute();
            });

        });
        function displayRoute() {
            var SalesId = $('#SalesId').val();
            var startTime = $('#startTime').val();
            var endTime = $('#endTime').val();
            var data = {function: 'GetSalesMapRought', SalesId: SalesId, startTime: startTime, endTime: endTime};
            $.ajax({
                url: base_url + "Admin/Helper",
                async: false,
                data: data,
                type: 'POST',
                success: function (data) {
                    try {
                        var json = jQuery.parseJSON(data);
                        if (json.status == true) {
                            var json = jQuery.parseJSON(data);
                            var result = json.result;



                            var stops = result.waypointsss;
                            var map = new window.google.maps.Map(document.getElementById("map"));

                            // new up complex objects before passing them around
                            var directionsDisplay = new window.google.maps.DirectionsRenderer({suppressMarkers: true});
                            var directionsService = new window.google.maps.DirectionsService();
                            Tour_startUp(stops);

                            window.tour.loadMap(map, directionsDisplay);
                            window.tour.fitBounds(map);

                            if (stops.length > 1) {
                                window.tour.calcRoute(directionsService, directionsDisplay);
                            }
                            $('body').removeClass('page-quick-sidebar-open');
                            setTimeout(function () {
                                $('manageInvoice').html('');
                            }, 50);

                        } else {
                            $('#Alertdiv').html(json.message);
                            setTimeout(function () {
                                $('Alertdiv').html('');
                            }, 500);
                        }
                    } catch (e) {
                        alert(e);
                    }










                }
            });
        }

        function Tour_startUp(stops) {
            if (!window.tour)
                window.tour = {
                    updateStops: function (newStops) {
                        stops = newStops;
                    },
                    // map: google map object
                    // directionsDisplay: google directionsDisplay object (comes in empty)
                    loadMap: function (map, directionsDisplay) {
                        var myOptions = {
                            zoom: 13,
                            center: new window.google.maps.LatLng(51.507937, -0.076188), // default to London
                            mapTypeId: window.google.maps.MapTypeId.ROADMAP
                        };
                        map.setOptions(myOptions);
                        directionsDisplay.setMap(map);
                    },
                    fitBounds: function (map) {
                        var bounds = new window.google.maps.LatLngBounds();

                        // extend bounds for each record
                        jQuery.each(stops, function (key, val) {
                            var myLatlng = new window.google.maps.LatLng(val.Geometry.Latitude, val.Geometry.Longitude);
                            bounds.extend(myLatlng);
                        });
                        map.fitBounds(bounds);
                    },
                    calcRoute: function (directionsService, directionsDisplay) {
                        var batches = [];
                        var itemsPerBatch = 10; // google API max = 10 - 1 start, 1 stop, and 8 waypoints
                        var itemsCounter = 0;
                        var wayptsExist = stops.length > 0;

                        while (wayptsExist) {
                            var subBatch = [];
                            var subitemsCounter = 0;

                            for (var j = itemsCounter; j < stops.length; j++) {
                                subitemsCounter++;
                                subBatch.push({
                                    location: new window.google.maps.LatLng(stops[j].Geometry.Latitude, stops[j].Geometry.Longitude),
                                    stopover: true
                                });
                                if (subitemsCounter == itemsPerBatch)
                                    break;
                            }

                            itemsCounter += subitemsCounter;
                            batches.push(subBatch);
                            wayptsExist = itemsCounter < stops.length;
                            // If it runs again there are still points. Minus 1 before continuing to
                            // start up with end of previous tour leg
                            itemsCounter--;
                        }

                        // now we should have a 2 dimensional array with a list of a list of waypoints
                        var combinedResults;
                        var unsortedResults = [{}]; // to hold the counter and the results themselves as they come back, to later sort
                        var directionsResultsReturned = 0;

                        for (var k = 0; k < batches.length; k++) {
                            var lastIndex = batches[k].length - 1;
                            var start = batches[k][0].location;
                            var end = batches[k][lastIndex].location;

                            // trim first and last entry from array
                            var waypts = [];
                            waypts = batches[k];
                            waypts.splice(0, 1);
                            waypts.splice(waypts.length - 1, 1);

                            var request = {
                                origin: start,
                                destination: end,
                                waypoints: waypts,
                                travelMode: window.google.maps.TravelMode.WALKING
                            };
                            (function (kk) {
                                directionsService.route(request, function (result, status) {
                                    if (status == window.google.maps.DirectionsStatus.OK) {

                                        var unsortedResult = {order: kk, result: result};
                                        unsortedResults.push(unsortedResult);

                                        directionsResultsReturned++;

                                        if (directionsResultsReturned == batches.length) // we've received all the results. put to map
                                        {
                                            // sort the returned values into their correct order
                                            unsortedResults.sort(function (a, b) {
                                                return parseFloat(a.order) - parseFloat(b.order);
                                            });
                                            var count = 0;
                                            for (var key in unsortedResults) {
                                                if (unsortedResults[key].result != null) {
                                                    if (unsortedResults.hasOwnProperty(key)) {
                                                        if (count == 0) // first results. new up the combinedResults object
                                                            combinedResults = unsortedResults[key].result;
                                                        else {
                                                            // only building up legs, overview_path, and bounds in my consolidated object. This is not a complete
                                                            // directionResults object, but enough to draw a path on the map, which is all I need
                                                            combinedResults.routes[0].legs = combinedResults.routes[0].legs.concat(unsortedResults[key].result.routes[0].legs);
                                                            combinedResults.routes[0].overview_path = combinedResults.routes[0].overview_path.concat(unsortedResults[key].result.routes[0].overview_path);

                                                            combinedResults.routes[0].bounds = combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getNorthEast());
                                                            combinedResults.routes[0].bounds = combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getSouthWest());
                                                        }
                                                        count++;
                                                    }
                                                }
                                            }
                                            directionsDisplay.setDirections(combinedResults);
                                            var legs = combinedResults.routes[0].legs;

                                            for (var i = 0; i < legs.length; i++) {
                                                var markerletter = "A".charCodeAt(0);
                                                markerletter += i;
                                                markerletter = String.fromCharCode(markerletter);

                                                var TIME = stops[i].TIME;
                                                createMarker(directionsDisplay.getMap(), legs[i].start_location, TIME, legs[i].start_address, markerletter);
                                            }
                                            var i = legs.length;
                                            var markerletter = "A".charCodeAt(0);
                                            markerletter += i;
                                            markerletter = String.fromCharCode(markerletter);
                                            var TIME = stops[i].TIME;
                                            createMarker(directionsDisplay.getMap(), legs[i].start_location, TIME, legs[i].start_address, markerletter);
                                        }
                                    }
                                });
                            })(k);
                        }
                    }
                };
        }
        function getMarkerImage(iconStr) {
            if ((typeof (iconStr) == "undefined") || (iconStr == null)) {
                iconStr = "red";
            }
            if (!icons[iconStr]) {
                icons[iconStr] = new google.maps.MarkerImage("http://www.google.com/mapfiles/marker" + iconStr + ".png",
                        // This marker is 20 pixels wide by 34 pixels tall.
                        new google.maps.Size(20, 34),
                        // The origin for this image is 0,0.
                        new google.maps.Point(0, 0),
                        // The anchor for this image is at 6,20.
                        new google.maps.Point(9, 34));
            }
            return icons[iconStr];

        }
        function createMarker(map, latlng, label, html, color) {
// alert("createMarker("+latlng+","+label+","+html+","+color+")");
            var contentString = '<b>' + label + '</b><br>' + html;
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                shadow: iconShadow,
                icon: getMarkerImage(color),
                shape: iconShape,
                title: label,
                zIndex: Math.round(latlng.lat() * -100000) << 5
            });
            marker.myname = label;

            google.maps.event.addListener(marker, 'click', function () {
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            });
            return marker;
        }




        function MapSetMarker(SalesId = '') {
            var DEFAULT_LAT = 32.5832258;
            var DEFAULT_LONG = 73.4905263;

            var data = {function: 'GetSalesMarker', SalesId: SalesId};
            $.ajax({
                url: base_url + "Admin/Helper?<?= $_SERVER['QUERY_STRING'] ?>",
                async: false,
                data: data,
                type: 'POST',
                success: function (data) {
                    try {
                        var json = jQuery.parseJSON(data);
                        console.log(json);
                        var markerArray = json.result;
                        if (json.latitude != '' && json.longitude != '') {
                            DEFAULT_LAT = parseFloat(json.latitude);
                            DEFAULT_LONG = parseFloat(json.longitude);
                        }
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 12,
                            center: {"lat": DEFAULT_LAT, "lng": DEFAULT_LONG}
                        });

                        map.addListener('click', function (e) {
                            placeMarker(e.latLng, map);
                        });
                        var infowindow = new google.maps.InfoWindow();
                        for (var i = 0; i < markerArray.length; i++) {
                            var d = markerArray[i];
                            marker = new google.maps.Marker({
                                position: new google.maps.LatLng(d.latitude, d.longitude),
                                map: map,
                                label: {color: "#ffff", fontSize: "12px", fontWeight: "bold"},
                                animation: google.maps.Animation.DROP
                            });
                            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                                return function () {
                                    var d = markerArray[i];
                                    infowindow.setContent(d.description + ' <a SalesId="' + d.sales_id + '" target="_blank" class="ViewReport" href="' + base_url + 'Admin/OrderDetails/' + btoa(d.sales_id) + '">Click here to view</a>');
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));
                        }
                    } catch (e) {
                        alert(e);
                    }
                }
            });
        }
        function placeMarker(position, map) {
            var geocoder = new google.maps.Geocoder();
            var image = base_url + 'images/marker-icon.png';
            var lat = position.lat();
            var long = position.lng();

            var marker = new google.maps.Marker({
                position: position,
                map: map,
                icon: image
            });
            map.panTo(position);

            $('#latitude').val(lat);
            $('#longitude').val(long);

            geocoder.geocode({
                'latLng': position
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#business-location').val(results[0].formatted_address);
                    }
                }
            });

            setTimeout(function () {
                $('#Filter').click();
            }, 500);



        }
        $(document).ready(function () {
            var input = document.getElementById('business-location');
            var autocomplete = new google.maps.places.Autocomplete(input);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                $('#latitude').val(place.geometry.location.lat());
                $('#longitude').val(place.geometry.location.lng());
            });
        });
    </script>
</html>