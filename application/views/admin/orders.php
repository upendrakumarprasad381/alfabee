<?php
$OutSideFilter = $this->uri->segment(3);
$Cond = '';
if ($OutSideFilter == 'today') {
    $Today = date('Y-m-d');
    $Cond = "AND DATE(O.date) >= '$Today'";
}
// } else if ($OutSideFilter == 'pending') {
//     $Cond = "AND O.order_status= '1'";
// } else if ($OutSideFilter == '') {
//     $Cond = "AND RD.request_status= '5'";
// } else if ($OutSideFilter == 'working') {
//     $Cond = "AND RD.request_status= '3'";
// } else if ($OutSideFilter == 'scheduled') {
//     $Cond = "AND RD.request_status= '6'";
// } else if ($OutSideFilter == 'cancelled') {
//     $Cond = "AND RD.request_status= '1'";
// } else if ($OutSideFilter == 'complite') {
//     $Cond = "AND RD.request_status= '4'";
// }else if ($OutSideFilter == 'pending_payments') {
//     $Cond = "AND  RD.payment_type !='1'";
// }
else if ($OutSideFilter == 'paid') {
    $Cond = "AND  O.payment_status =='1'";
}

//$orders = GetAdminOrders($Cond);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"> <?php echo $this->lang->line("orders") ?></span>

                        </div>
                    </div>

                    <form method="post" action="">  
                        <div class="panel-group accordion" id="accordion1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="advance_filter accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1" aria-expanded="false"> <?php echo $this->lang->line("advance_filter") ?> </a>
                                    </h4>
                                </div>
                                <div id="collapse_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Order No</label>
                                                <input name="Filterorderno" value="<?= !empty($Filter) ? $Filter['Filterorderno'] : '' ?>" type="text" class="form-control " autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label> Store Type</label>
                                                <select name="FilterStoreType" class="form-control selectpicker" data-live-search="true">
                                                    <option value="">select</option>
                                                    <?php
                                                    $StoreArray = Get_AllStoreType();
                                                    $SearchedStore = !empty($Filter) ? $Filter['FilterStoreType'] : '';
                                                    for ($i = 0; $i < count($StoreArray); $i++) {
                                                        $selected = '';
                                                        if ($SearchedStore == $StoreArray[$i]->id) {
                                                            $selected = 'selected="selected"';
                                                        }
                                                        ?>
                                                        <option <?= $selected ?> value="<?= $StoreArray[$i]->id ?>"><?= $StoreArray[$i]->store_type ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div> 

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label> <?php echo $this->lang->line("start_date") ?></label>
                                                <input name="FilterStartDate" value="<?= !empty($Filter) ? $Filter['FilterStartDate'] : '' ?>" type="text" class="form-control datepicker2" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label> <?php echo $this->lang->line("end_date") ?></label>

                                                <input name="FilterEndDate" value="<?= !empty($Filter) ? $Filter['FilterEndDate'] : '' ?>" type="text" class="form-control datepicker2" autocomplete="off">
                                            </div>
                                        </div>


                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label> Order status</label>
                                                <?php
                                                $studtdif = !empty($Filter['Filterorderstatus']) ? $Filter['Filterorderstatus'] : ''
                                                ?>
                                                <select name="Filterorderstatus" class="form-control" autocomplete="off">
                                                    <option value="">select</option>
                                                    <option  value="1" <?= $studtdif == 1 ? 'selected="selected"' : '' ?> >Order Confirmed</option>
                                                    <option  value="2" <?= $studtdif == 2 ? 'selected="selected"' : '' ?> >Preparing Order</option>
                                                    <option  value="14" <?= $studtdif == 14 ? 'selected="selected"' : '' ?> >Ready for delivery</option>
                                                    <option  value="5" <?= $studtdif == 5 ? 'selected="selected"' : '' ?> >Order on the way</option>
                                                    <option  value="8" <?= $studtdif == 8 ? 'selected="selected"' : '' ?> >Ready for pickup</option>
                                                    <option  value="6" <?= $studtdif == 6 ? 'selected="selected"' : '' ?> >Order Delivered</option>
                                                    <option  value="7" <?= $studtdif == 7 ? 'selected="selected"' : '' ?>  >Order Cancel</option>
                                                </select>
                                            </div>
                                        </div>




                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label style="width: 100%;">&nbsp; </label>
                                                <input type="submit" name="Filter" class="btn blue" value=" <?php echo $this->lang->line("search") ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="portlet-body">
                        <form method="post">
                            <?php
                            $disabled = '';
                            $offset = !empty($_POST['offset']) ? $_POST['offset'] : '0';
                            $nameBtn = !empty($_POST['btnPage']) ? $_POST['btnPage'] : 'next';
                            $limit = 1000;

                            if ($nameBtn == 'prev') {
                                $offset = $offset - 1;
                            } else {
                                $offset = $offset + 1;
                            }
                            $start = ($limit * $offset) - $limit;
                            $end = $limit * $offset;
                            if ($start == 0) {
                                $disabled = 'disabled';
                            }
                            ?>
                            <div class="pagtopbigcon">
                                <button <?= $disabled ?> value="prev" name="btnPage" type="submit"><</button>
                                <input type="button" value="<?= $start . '-' . $end ?>" >
                                <button name="btnPage" value="next" type="submit" >></button></span>
                                <input type="hidden" name="offset" value="<?= $offset ?>">
                            </div>


                        </form>
                        <table class="table table-striped table-bordered table-hover commontableXXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th> <?php echo $this->lang->line("sl_no") ?></th>
                                    <th><?php echo $this->lang->line("order_no") ?></th>
                                    <th>  Store Type</th>
                                    <th> <?php echo $this->lang->line("restaurant_name") ?></th>
                                    <th> <?php echo $this->lang->line("customer_name") ?></th>
                                    <th> <?php echo $this->lang->line("price") ?> In PKR</th>
                                    <th> <?php echo $this->lang->line("order_status") ?></th>
                                    <th> <?php echo $this->lang->line("order_date") ?></th>
                                    <!--<th>Status</th>-->
                                   <!--<th>Payment Date</th>-->
                                    <!--<th>Invoice</th>-->
                                    <th> <?php echo $this->lang->line("action") ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($_POST['Filter'])) {
                                    $Cond = '';
                                    $data['Form'] = $_POST;
                                    extract($_POST);
                                    if (!empty($FilterStartDate) && !empty($FilterEndDate)) {
                                        $datefrom = date('Y-m-d', strtotime($FilterStartDate));
                                        $dateto = date('Y-m-d', strtotime($FilterEndDate));
                                        $Cond = "AND DATE(O.date) BETWEEN '$datefrom' AND '$dateto'";
                                    } else if (!empty($FilterStartDate)) {
                                        $datefrom = date('Y-m-d', strtotime($FilterStartDate));
                                        $Cond = $Cond . "AND DATE(O.date) >= '$datefrom'";
                                    } elseif (!empty($FilterEndDate)) {
                                        $dateto = date('Y-m-d', strtotime($FilterEndDate));
                                        $Cond = $Cond . "AND DATE(O.date) < '$dateto'";
                                    }

                                    if (!empty($FilterStoreType)) {
                                        $Cond = $Cond . "AND US.store_type = '$FilterStoreType'";
                                    }
                                    if (!empty($Filterorderno)) {
                                        $Cond = $Cond . "AND O.orderno LIKE '%$Filterorderno%'";
                                    }
                                    if (!empty($Filterorderstatus)) {
                                        $Cond = $Cond . "AND O.order_status = '$Filterorderstatus'";
                                    }
                                }
                                $Join = 'LEFT JOIN users U ON U.id=O.user_id LEFT JOIN restaurant_details RD ON RD.vendor_id=O.vendor_id LEFT JOIN order_status OS ON OS.status_id=O.order_status LEFT JOIN users US ON US.id=O.vendor_id';
                                $Qry = "SELECT O.*,OS.*,US.store_type FROM `orders` O $Join WHERE O.archive=0 $Cond order by O.id DESC LIMIT $start,$limit";

                                $orders = $this->Database->select_qry_array($Qry);
                                for ($i = 0; $i < count($orders); $i++) {
                                    $d = $orders[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>

                                        <td><?= $d->orderno ?></td>
                                        <td><?= GetNameById($d->store_type, 'store_type', 'store_type') ?></td>
                                        <td>
                                            <?php
                                            $Qry = "SELECT restaurant_name FROM `restaurant_details` WHERE vendor_id=" . $d->vendor_id;
                                            $rest_details = $this->Database->select_qry_array($Qry);
                                            echo $rest_details[0]->restaurant_name;
                                            ?>
                                        </td>
                                        <td><?= GetNameById($d->user_id, 'users', 'name') ?></td>
                                        <td><?= number_format($d->total_price + $d->service_charge, 2) ?></td>
                                        <td>
                                            <span class="label label-sm label-success" style="    background-color: <?= $d->status_color ?>;"><?= !empty($d->status_name) ? $d->status_name : 'Received' ?></span>
                                            <?php
                                            // if($d->order_status==1)
                                            // {
                                            //     echo  $this->lang->line("order_confirmed");
                                            // }elseif($d->order_status==2)
                                            // {
                                            //     echo $this->lang->line("assign_to_deliver");
                                            // }
                                            // elseif($d->order_status==3)
                                            // {
                                            //     echo $this->lang->line("order_on_the_way");
                                            // }elseif($d->order_status==4){
                                            //     echo $this->lang->line("order_delivered");
                                            // }elseif($d->order_status==5){
                                            //     echo $this->lang->line("order_cancelled_by_user");
                                            // }elseif($d->order_status==6){
                                            //     echo $this->lang->line("order_cancelled_by_homelyfood_user");
                                            // }
                                            ?>
                                        </td>
                                        <td><?= date('D d M-Y h:i A', strtotime($d->date)) ?></td>
                                        <td>
                                            <a href="<?= base_url('Admin/OrderDetails/' . base64_encode($d->id)) ?>"><span class="label label-sm label-success"><i class="fa fa-eye" aria-hidden="true"></i></span></a>
                                            <a href="javascript:void(0)" updatejson='{"archive":"1"}'  condjson='{"id":"<?= $d->id ?>"}' dbtable="orders" class="autoupdate" title="Remove"><span class="label label-sm label-danger"><i class="fas fa-trash-alt"></i></span></a>

                                            <?php if ($d->invoice != "") { ?>
                                                <a href="<?= base_url('login/invoice?orderId=' . base64_encode($d->id)) ?>" target="_blank"><i class="fas fa-print"></i></a>
                                                <a href="<?= base_url() ?>uploads/invoice/<?= $d->invoice ?>" target="_blank"><i class="fa fa-file" aria-hidden="true"></i></a>
                                                <?php } ?>
                                        </td>

                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
