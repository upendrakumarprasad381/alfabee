<?php
$Order = GetPartyOrdersById($orderid);
$PaymentType = GetPaymentType($Order[0]->mode_of_payment);
$UserArray = GetUserDetails($Order[0]->user_id);;
// $AddressArray = GetAddressArrayByAddressId($Order[0]->delivery_address);
$op=GetPartyOrderDetailsByOrderId($orderid);

?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <input value="<?= $orderid ?>" type="hidden" id="order_id">
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption" style="padding: 0px;">
                            <?php
                            $Select = 'selected="selected"';
                            $UserProfile = 'uploads/user_images/' . $UserArray[0]->image;
                            if (is_file(HOME_DIR . $UserProfile)) {
                                $ImageHtml = '<img style="border-radius: 50% !important;width:32px" src="' . base_url() . $UserProfile . '" />';
                            } else {
                                $ImageHtml = '<i style="font-size: 36px;" class="fab fa-first-order"></i>';
                            }
                            echo $ImageHtml;
                            ?>
                            <span class="caption-subject font-dark sbold uppercase"> <?php echo $this->lang->line("order_no")?> - <?= $Order[0]->party_orderno ?>
                                <span class="hidden-xs">| <?= date('D d M-Y h:i A', strtotime($Order[0]->date)) ?></span>
                            </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="portlet blue-hoki box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i  class="fab fa-first-order"></i>  <?php echo $this->lang->line("order_details")?>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"><?php echo $this->lang->line("party_order_no")?>: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= $Order[0]->party_orderno ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("order_date_time")?>: </div>
                                                    <div class="col-md-7 value">  <?= date('D d M-Y h:i A', strtotime($Order[0]->date)) ?></div>
                                                </div>
                                                

                                                <?php
                                                if (!empty($Order->promo_code)) {
                                                    ?>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("promo_code")?>: </div>
                                                        <div class="col-md-7 value"> <?= $Order->promo_code ?> </div>
                                                    </div>
                                                    <?php
                                                }
                                                if (!empty($Order->WritenQuery)) {
                                                    ?>
                                                    <div class="row static-info">
                                                    <?php if(!empty($Order->WritenQueryOp)) { ?>
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("cancel_reason")?>: </div>
                                                        <div class="col-md-7 value"> <?=$Order->WritenQueryOp?> </div>

                                                        <?php } ?>                                                    
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("written_msg")?>: </div>
                                                        <div class="col-md-7 value"> <?= $Order->WritenQuery ?> </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                                <!--<div class="row static-info">-->
                                                <!--    <div class="col-md-5 name"> Last Update: </div>-->
                                                <!--    <div class="col-md-7 value"> -->
                                                    <?php
                                                        // if (!empty($Order->LastUpdatedBy)) {
                                                        //     echo $Order->LastUpdatedBy . ' - ' . date('D, d M-Y h:i A', strtotime($Order->lastupdate));
                                                        // }
                                                        ?>
                                                <!--        </div>-->
                                                <!--</div>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="portlet blue-hoki box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fas fa-user"></i><?php echo $this->lang->line("customer_information")?></div>

                                            </div>
                                            <div class="portlet-body">
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("customer_name")?>: </div>
                                                    <div class="col-md-7 value"> <?= $UserArray[0]->name ?></div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("email")?>: </div>
                                                    <div class="col-md-7 value" style="user-select: all;"> <?= $UserArray[0]->email ?> </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("phone_number")?>: </div>
                                                    <div class="col-md-7 value"> <a href="tel:<?= $UserArray[0]->mobile_code . $UserArray[0]->mobile_number ?>"><?= $UserArray[0]->mobile_code . $UserArray[0]->mobile_number ?></a> </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("gender")?>: </div>
                                                    <div class="col-md-7 value"> <?php
                                                        if ($UserArray[0]->gender == 1) {
                                                            echo $this->lang->line("male");
                                                        } elseif ($UserArray[0]->gender == 2) {
                                                            echo $this->lang->line("female");
                                                        }
                                                        ?> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    
                                </div>


                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="portlet blue-hoki box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fas fa-shopping-cart"></i><?php echo $this->lang->line("party_details")?>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                              
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("no_of_guests")?>: </div>
                                                    <div class="col-md-7 value"> <?= $Order[0]->no_of_people?>  </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("party_date")?>: </div>
                                                    <div class="col-md-7 value"> <?= date('D, d M-Y', strtotime($Order[0]->party_date)) ?> </div>
                                                </div>
                                                  <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("party_time")?>: </div>
                                                    <div class="col-md-7 value"> <?= date('h:i A', strtotime($Order[0]->party_time)) ?> </div>
                                                </div>
                            
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="portlet blue-hoki box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fas fa-store"></i>Business Details
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <?php
                                                $get_restaurant_details = GetRestaurantDetails($Order[0]->vendor_id);
                                                ?>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("restaurant_name")?>: </div>
                                                    <div class="col-md-7 value"> <?= $get_restaurant_details[0]->restaurant_name ?> </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("working_hours")?>: </div>
                                                    <?php
                                                        
										                $opening_time = date('h:i a ', strtotime($get_restaurant_details[0]->opening_time));
										                $closing_time = date('h:i a ', strtotime($get_restaurant_details[0]->closing_time));
            										?>
                                                    <div class="col-md-7 value"> <?=$opening_time ?> - <?= $closing_time?> </div>
                                                </div>
                                                  <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("cuisines")?>: </div>
                                                    <div class="col-md-7 value"> <?= $get_restaurant_details[0]->cuisine_name?> </div>
                                                </div>
                            
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("mobile_number")?>: </div>
                                                    <div class="col-md-7 value"> 
                                                    
                                                     <?= $get_restaurant_details[0]->mobile_code?><?= $get_restaurant_details[0]->mobile_number?>  </div>
                                                </div>
                                                
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("address")?>: </div>
                                                    <div class="col-md-7 value"> 
                                                       <?= $get_restaurant_details[0]->area?>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                 <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="portlet grey-cascade box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fas fa-shopping-cart"></i><?php echo $this->lang->line("order_details")?>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th> <?php echo $this->lang->line("sl_no")?> </th>
                                                                <th> <?php echo $this->lang->line("item_name")?> </th>
                                                                <th> <?php echo $this->lang->line("price")?> </th>
                                                                <th> <?php echo $this->lang->line("quantity")?> </th>
                                                                <th> <?php echo $this->lang->line("total")?> </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            for ($i = 0; $i < count($op); $i++) {
                                                                $d = $op[$i];
                                                             
                                                                ?>
                                                                <tr>
                                                                    <td><?= $i + 1; ?></td>
                                                                    <td><?= ucwords($d->product_name) ?></td>
                                                           
                                                                    <td><?= number_format($d->price,2) ?></td>
                                                                    <td><?= $d->quantity ?></td>
                                                                    <td><?= number_format($d->subtotal,2) ?></td>
                                                           
                                                                 
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="display:none;">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="portlet grey-cascade box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-flag-o"></i><?php echo $this->lang->line("quotation")?>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-bordered table-striped">
                                                        <thead>
                                                            <tr>

                                                                <th width="15%"><?php echo $this->lang->line("total_guests")?></th>
                                                                <th width="25%"><?php echo $this->lang->line("quotation_amt")?></th>
                                                                <th width="25%"></th>
                                                                <?php
                                                            
                                                                if($Order[0]->price!=0)
                                                                {
                                                                ?>
                                                               
                                                                <th width="25%"><?php echo $this->lang->line("payment_status")?></th>
                                                                <?php
                                                                }
                                                                ?>
                                                                <th width="25%"></th>
                                                               
                                                            </tr>
                                                        </thead>
                                                            <tr>
                                                                <td><?= $Order[0]->no_of_adult + $Order[0]->no_of_children?></td>
                                                                 
                                                                
                                                                    <td>
                                                                    <div class="input-group col-md-9">
                                                                        <span class="input-group-btn">
                                                                            <button class="btn default" type="button"><?php echo $this->lang->line("aed")?></button>
                                                                        </span>
                                                                        <input value="<?= !empty($Order[0]->price) ? $Order[0]->price : '' ?>" id="price" type="text" class="form-control col-md-3" autocomplete="off" disabled>
                                                                           
                                                                    </div>
                                                                   
                                                                </td>
                                                                <td>
                                                                    <!--<button type="submit" name="quoteprice" id="quoteprice" class="btn btn-success" disabled>Quote Price</button>-->
                                                                </td>
                                                                    
                                                                
                                                                <?php
                                                                 if($Order[0]->price!=0)
                                                                {
                                                            
                                                                   if($Order[0]->mode_of_payment==3 && $Order[0]->payment_status==2)
                                                                   {
                                                                    ?>
                                                            
                                                                    
                                                                        <td>
                                                                        <div class="input-holder">
                                                                            <div class="custom-radio">
                                                                                <!--<input type="checkbox" id="payment_status" class="filled-in manage-products-checkbox" name="order_status" value="1">-->
                                                                                <label for="id04" class="strong-label post-as"><?php echo $this->lang->line("payment_received")?></label>
                                                                            </div>
                                                                        </div>
                                                                        </td>
                                                                        <!--<td><button type="submit" id="party_order_status" class="btn btn-success">Payment Confirmation</button></td>-->
                                                                    
                                                                    <?php
                                                                    
                                                                   }else{
                                                                    ?>
                                                                    <td>
                                                                        <div class="input-holder">
                                                                            <div class="custom-radio">
                                                                                <!--<input type="checkbox" id="payment_status" class="filled-in manage-products-checkbox" name="order_status" value="1">-->
                                                                                <label for="id04" class="strong-label post-as"><?php echo $this->lang->line("payment_pending")?></label>
                                                                            </div>
                                                                        </div>
                                                                        </td>
                                                                    <?php
                                                                   }
                                                                }
                                                                   ?>
                                                                
                                                            
                                                            </tr>
                                
                                                        
                                                    </table>
                                                    <div class="col-sm-12">
                <style>
                    .root {
                        padding: 1rem;
                        border-radius: 0;
                        box-shadow: 0 0 4px rgba(0, 0, 0, 0.12);
                        margin-bottom: 10px;
                    }
                   

                    .order-track {
                        margin-top: 2rem;
                        padding: 0 1rem;
                        border-top: 1px dashed #2c3e50;
                        padding-top: 2.5rem;
                        display: flex;
                        flex-direction: column;
                    }
                    .order-track-step {
                        display: flex;
                        height: 7rem;
                    }
                    .order-track-step:last-child {
                        overflow: hidden;
                        height: 4rem;
                    }
                    .order-track-step:last-child .order-track-status span:last-of-type {
                        display: none;
                    }
                    .order-track-status {
                        margin-right: 1.5rem;
                        position: relative;
                    }
                    .order-track-status-dot {
                        display: block;
                        width: 2.2rem;
                        height: 2.2rem;
                        border-radius: 50% !important;
                        background: #95A5A6;
                    }
                    .order-track-status-line {
                        display: block;
                        margin: 0 auto;
                        width: 2px;
                        height: 7rem;
                        background:#822024;
                    }
                    .order-track-text-stat {
                        font-size: 1.3rem;
                        font-weight: 500;
                        margin-bottom: 3px;
                        margin-top: 0px;
                    }
                    .order-track-text-sub {
                        font-size: 1rem;
                        font-weight: 300;
                    }

                    .order-track {
                        transition: all .3s height 0.3s;
                        transform-origin: top center;
                    }
                </style>
                <?php
                $his = GetpartyorderstatushistoryBy($orderid);
                if(count($his)>0)
                {
                ?>
                <section class="root">
                    
                    <div class="order-track">
                        <?php

                        foreach($his as $item){
                          if($item->status_id=='1'){
                                $status = $this->lang->line("order_confirmed");
                            }
                            else if($item->status_id=='2'){
                                $status = $this->lang->line("assign_to_deliver");
                            }
                        //     else if($item->status_id=='3'){
                        //         $status = $this->lang->line("order_on_the_way");
                        //     }
                        //     else if($item->status_id=='4'){
                        //         $status = $this->lang->line("order_delivered");
                        //     }
                           
                            ?>
                            <div class="order-track-step">
                                <div class="order-track-status">
                                    <span class="order-track-status-dot"></span>
                                    <span class="order-track-status-line"></span>
                                </div>
                                <div class="order-track-text">
                                    <p class="order-track-text-stat"><?= $status ?></p>
                                    <span class="order-track-text-sub"><?= date('d F,Y h:i A',  strtotime($item->timestamp)) ?> </span>
                                </div>
                            </div>
                        <?php } ?>
                       
                    </div>
                </section>  
                <?php
                }
                ?>
            </div>
                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- End: life time stats -->
            </div>
        </div>


    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
