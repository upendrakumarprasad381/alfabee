<?php
$OutSideFilter = $this->uri->segment(3);

$Cond = '';
if ($OutSideFilter == 'today') {
    $Today = date('Y-m-d');
    $Cond = "AND DATE(O.inserted_on) >= '$Today'";
}
// } else if ($OutSideFilter == 'pending') {
//     $Cond = "AND O.order_status= '1'";
// } else if ($OutSideFilter == '') {
//     $Cond = "AND RD.request_status= '5'";
// } else if ($OutSideFilter == 'working') {
//     $Cond = "AND RD.request_status= '3'";
// } else if ($OutSideFilter == 'scheduled') {
//     $Cond = "AND RD.request_status= '6'";
// } else if ($OutSideFilter == 'cancelled') {
//     $Cond = "AND RD.request_status= '1'";
// } else if ($OutSideFilter == 'complite') {
//     $Cond = "AND RD.request_status= '4'";
// }else if ($OutSideFilter == 'pending_payments') {
//     $Cond = "AND  RD.payment_type !='1'";
// }
else if($OutSideFilter == 'paid'){
    $Cond = "AND  O.payment_status =='1'";
}

$orders=GetAdminPartyOrders($Cond);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?php echo $this->lang->line("party_orders")?></span>
                            
                        </div>
                    </div>
                    <form method="post" action="">  
                        <div class="panel-group accordion" id="accordion1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="advance_filter accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1" aria-expanded="false"><?php echo $this->lang->line("advance_filter")?></a>
                                    </h4>
                                </div>
                                <div id="collapse_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label><?php echo $this->lang->line("customer_name")?></label>
                                                <select name="FilterCustomerId" class="form-control selectpicker" data-live-search="true">
                                                    <option value="">select</option>
                                                    <?php
                                                    $CustomerArray = GetCustomerDetailsAll();
                                                    $SearchedCustomer = !empty($Filter) ? $Filter['FilterCustomerId'] : '';
                                                    for ($i = 0; $i < count($CustomerArray); $i++) {
                                                        $selected = '';
                                                        if ($SearchedCustomer == $CustomerArray[$i]->id) {
                                                            $selected = 'selected="selected"';
                                                        }
                                                        ?><option <?= $selected ?> value="<?= $CustomerArray[$i]->id ?>"><?= $CustomerArray[$i]->name ?></option><?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div> 
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label><?php echo $this->lang->line("restaurant_name")?></label>
                                                <select name="FilterRestaurantId" class="form-control selectpicker" data-live-search="true">
                                                    <option value="">select</option>
                                                    <?php
                                                    $CustomerArray = GetRestaurantDetailsAll();
                                                    $SearchedCustomer = !empty($Filter) ? $Filter['FilterRestaurantId'] : '';
                                                    for ($i = 0; $i < count($CustomerArray); $i++) {
                                                        $selected = '';
                                                        if ($SearchedCustomer == $CustomerArray[$i]->id) {
                                                            $selected = 'selected="selected"';
                                                        }
                                                        ?><option <?= $selected ?> value="<?= $CustomerArray[$i]->id ?>"><?= $CustomerArray[$i]->restaurant_name ?></option><?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div> 
                                    
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label><?php echo $this->lang->line("start_date")?></label>
                                                <input name="FilterStartDate" value="<?= !empty($Filter) ? $Filter['FilterStartDate'] : '' ?>" type="text" class="form-control datepicker2" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label><?php echo $this->lang->line("end_date")?></label>

                                                <input name="FilterEndDate" value="<?= !empty($Filter) ? $Filter['FilterEndDate'] : '' ?>" type="text" class="form-control datepicker2" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label style="width: 100%;">&nbsp; </label>
                                                <input type="submit" name="Filter" class="btn blue" value="<?php echo $this->lang->line("search")?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover commontableXXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no")?></th>
                                    
                                    <th><?php echo $this->lang->line("order_no")?></th>
                                    <th><?php echo $this->lang->line("restaurant_name")?></th>
                                    <th><?php echo $this->lang->line("customer_name")?></th>
                                    <th><?php echo $this->lang->line("price")?></th>
                                    <th><?php echo $this->lang->line("order_date")?></th>
                                    <!--<th>Order Status</th>-->
                                    <!--<th>Status</th>-->
                                   <!--<th>Payment Date</th>-->
                                    <!--<th>Invoice</th>-->
                                    <th><?php echo $this->lang->line("action")?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($orders); $i++) {
                                    $d = $orders[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        
                                        <td><?= $d->party_orderno ?></td>
                                        <td>
                                            <?php
                                            $Qry = "SELECT restaurant_name FROM `restaurant_details` WHERE vendor_id=".$d->vendor_id;
                                            $rest_details = $this->Database->select_qry_array($Qry);
                                            echo $rest_details[0]->restaurant_name;
                                            ?>
                                        </td>
                                        <td><?=GetNameById($d->user_id,'users','name')?></td>
                                        <td>PKR <?=number_format($d->price,2)?></td>
                                        <td><?=date('D, d M-Y h:i A',strtotime($d->date))?></td>
                                        <!--<td>-->
                                            <?php
                                            // if($d->order_status==1)
                                            // {
                                            //     echo 'Order Confirmed';
                                            // }elseif($d->order_status==2)
                                            // {
                                            //     echo 'Order on the way';
                                            // }elseif($d->order_status==3){
                                            //     echo 'Order delivered';
                                            // }
                                            ?>
                                        <!--</td>-->
                                        <!--<td> -->
                                           
                                        <td>
                                        <a href="<?= base_url('Admin/party_orderDetails/' . base64_encode($d->id)) ?>"><span class="label label-sm label-success"><i class="fa fa-eye" aria-hidden="true"></i></span></a>
                                        <?php 
                                        // if($d->invoice !=""){ 
                                        ?>
                                            <!--<a href="<?=base_url()?>uploads/invoice/<?=$d->invoice?>" target="_blank"><i class="fa fa-file" aria-hidden="true"></i></a>-->
                                        <?php 
                                        // } 
                                        ?>
                                        <!--<a redrurl="Orders" Id="<?= $d->id ?>" title="<?=$d->orderno?>" class="CancelOrder" table="orders">-->
                                        <!--      <span class="label label-sm label-danger" ><i class="fa fa-times"></i></span></a>-->
                                        </td>
                                        
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
