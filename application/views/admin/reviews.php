
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?php echo $this->lang->line("user_reviews")?></span>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover commontableXXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no")?></th>
                                    <th><?php echo $this->lang->line("customer_name")?></th>
                                    <th><?php echo $this->lang->line("restaurant_name")?></th>
                                    <!--<th>Order Id</th>-->
                                    <th><?php echo $this->lang->line("rating")?></th>
                                    <th><?php echo $this->lang->line("comments")?></th>
                                    <th><?php echo $this->lang->line("added_date")?></th>
                                    <th style="display:none;"><?php echo $this->lang->line("status")?></th>
                                    <th><?php echo $this->lang->line("action")?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($Reviews); $i++) {
                                    $d = $Reviews[$i];
                                
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->name ?></td>
                                        <td><?= $d->restaurant_name ?></td>
                                        <!--<td><?= $d->orderno ?></td>-->
                                        <td>
                                            <div class="rating">
                                                
                                                <?php
                                                $stars = (int)$d->rating;
                                                $count = 1;
                                                for($j = 1; $j <= 5; $j++){
                                                    if($stars >= $count){
                                                        $color = "#ff9900";
                                                    } else {
                                                        $color = "";
                                                    }
                                                    $count++;
                                                
                                                ?>
                                                <span class="float-left">
										        <i class="fa fa-star" style="color:<?= $color ?>"></i>
										    </span> 
				                                <!--<span style="color:<?= $color ?>">☆</span>	-->
				                                <?php
                                                }
                                                
                                                ?>
				                            </div>
                                        </td>
                                        <td><?= $d->comments?></td>
                                        <td><?= date('d M Y, h:i A ',strtotime($d->inserted_on))?></td>
                                        <td style="display:none;">
                                            <input type="hidden" id="feedback_id" value="<?= $d->feedback_id?>">
                                            <select class="review_status" id="<?=$d->id?>">
                                 
                                            <option <?= isset($d->review_status) ? $d->review_status == 0 ? 'selected="selected"' : '' : '' ?> value="0"><?php echo $this->lang->line("active")?></option>
                                            <option <?= isset($d->review_status) ? $d->review_status == 1 ? 'selected="selected"' : '' : '' ?> value="1"><?php echo $this->lang->line("inactive")?></option>
                                        </td>
                                        <!--<td>-->
                                        <!--    <input type="hidden" id="feedback_id" value="<?= $d->feedback_id?>">-->
                                             <!--<input data-id="{{$user->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $user->status ? 'checked' : '' }}>-->
                                            <!--<input type="checkbox" checked data-toggle="toggle" id="toggle"  data-size="sm" data-on="Active" data-off="Inactive" <?php echo (isset($d->status) && $d->status==1 ? 'checked="checked"' : '');?>>-->
                                        <!--</td>-->
                                        <!--<td><?= $d->status == 0 ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Inactive</span>' ?></td>-->
                                        <!--<td>-->
                                        <!--     <a title="User Information" href="<?= base_url('Admin/user_info/' . base64_encode($d->user_id)) ?>"><span class="label label-sm label-success"><i class="fas fa-street-view"></i></span></a>-->
                                        <!--    <a  href="<?= base_url('Admin/vendor_info/' . base64_encode($d->vendor_id)) ?>" Id="<?= $d->id ?>"><span class="label label-sm label-info"><i class="fa fa-eye" aria-hidden="true" title="HomelyFood User Info" ></i></span></a>-->
                                            <!--<a href="<?= base_url('admin/category/add-new/' . base64_encode($d->feedback_id)) ?>"><span class="label label-sm label-success"><i class="fa fa-pencil" aria-hidden="true"></i></span></a> -->
                                        <!--    <a ref="javascript:void(0)" class="common_delete" Id="<?= $d->feedback_id ?>" table="user_feedback" class="autoupdate" redrurl="reviews"><span class="label label-sm label-<?= empty($d->archive) ? 'danger' : 'info' ?>"><?= empty($d->archive) ? '<i class="fa fa-trash-o"></i>' : '<i class="fa fa-trash-o"></i>' ?></span></a>-->
                                        <!--</td>-->
                                         <td>
                                             <a title="User Information" href="<?= base_url('Admin/user_info/' . base64_encode($d->user_id)) ?>"><span class="label label-sm label-success"><i class="fas fa-street-view"></i></span></a>
                                            <a  href="<?= base_url('Admin/vendor_info/' . base64_encode($d->vendor_id)) ?>" Id="<?= $d->id ?>"><span class="label label-sm label-info"><i class="fa fa-eye" aria-hidden="true" title="Vendor Info" ></i></span></a>
                                            <!--<a href="<?= base_url('admin/category/add-new/' . base64_encode($d->feedback_id)) ?>"><span class="label label-sm label-success"><i class="fa fa-pencil" aria-hidden="true"></i></span></a> -->
                                            <a ref="javascript:void(0)" class="common_delete" Id="<?= $d->feedback_id ?>" table="user_feedback" class="autoupdate" redrurl="reviews"><span class="label label-sm label-<?= empty($d->archive) ? 'danger' : 'info' ?>"><?= empty($d->archive) ? '<i class="fa fa-trash-o"></i>' : '<i class="fa fa-trash-o"></i>' ?></span></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
