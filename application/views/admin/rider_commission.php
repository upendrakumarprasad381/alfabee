<?php
if (isset($_POST['submit'])) {
    $json = !empty($_POST['json']) ? $_POST['json'] : [];
    if (!empty($json['id'])) {
        $this->Database->update('rider_commission', $json, array('id' => $json['id']));
    } else {
        $this->Database->insert('rider_commission', $json);
    }
    header("Refresh:0");
}
$cArray = GetRiderCommission();
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"> RIDER COMMISSION</span>
                            <a href="javascript:void(0)" json='' onclick="ADMINCMN.riderCommissingOpenModel(this);"  class="btn btn-sm green small"> Add-New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover commontableXXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Title</th>
                                    <th>Radius KM</th>
                                    <th>Commission</th>
                                    <th>Per KM Commission</th>

                                    <th> <?php echo $this->lang->line("action") ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($cArray); $i++) {
                                    $d = $cArray[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>

                                        <td><?= $d->com_title ?></td>
                                        <td><?= $d->km ?></td>
                                        <td><?= $d->commission ?></td>
                                        <td><?= !empty($d->additional_commission) ? $d->additional_commission : '' ?></td>
                                        <td>
                                            <a href="javascript:void(0)" onclick="ADMINCMN.riderCommissingOpenModel(this);" json='<?= base64_encode(json_encode($d)) ?>'  title="Update"><span class="label label-sm label-success"><i class="fas fa-edit"></i></span></a>
                                            <a href="javascript:void(0)"  condjson='{"id":"<?= $d->id ?>"}' dbtable="rider_commission" class="autodelete" title="Remove"><span class="label label-sm label-danger"><i class="fas fa-trash-alt"></i></span></a>
                                        </td>

                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<div class="modal fade" id="myModal" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Commission</h4>
            </div>
            <form method="post" enctype="multipart/form-data">
                <div class="modal-body"> 
                    <div class="row">
                        <div class="col-sm-12"> 

                            <div class="row">


                                <input type="hidden" id="idsnamed" name="json[id]">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" id="com_title" name="json[com_title]" value="" class="form-control" required autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Radius KM</label>
                                        <input type="number" id="km" name="json[km]" class="form-control" required autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Commission </label>
                                        <input type="number" id="commission" name="json[commission]" class="form-control" required autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Per Km Commission <br><small style="font-weight: 600;">If you want to charge additional per kilometers  then enter the per kilometers  amount.</small></label>
                                        <input type="number" id="additional_commission" name="json[additional_commission]" class="form-control" autocomplete="off">
                                    </div>
                                </div>





                            </div>

                        </div>
                    </div> 
                </div>
                <div class="modal-footer"> 
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button> 
                    <button type="submit" name="submit" class="btn green">Save changes</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>