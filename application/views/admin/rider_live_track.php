<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
        <meta charset="utf-8">
        <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>images/favicons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>images/favicons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>images/favicons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>images/favicons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>images/favicons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>images/favicons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>images/favicons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>images/favicons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>images/favicons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url() ?>images/favicons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>images/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>images/favicons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>images/favicons/favicon-16x16.png">
        <title>Rider live track</title>
        <style>
            html, body {
                height: 100%;
                margin: 0;
                padding: 0;
            }
            #map {
                height: 100%;
                width: 100%;
                height: 100%;
            }
            .sdcfsd {
                height: 100vh; /* For 100% screen height */
            }
        </style>
    </head>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" style="background: none;">
        <div id="map"></div>
    </body>
    <?php
    $Controller = $this->router->fetch_class();
    $Method = $this->router->fetch_method();
    $segment3 = $this->uri->segment(3);
    ?>
    <script>
        var base_url = '<?= base_url(); ?>';
        var Controller = '<?= $Controller ?>';
        var Method = '<?= $Method ?>';
        var segment3 = '<?= $segment3 ?>';
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&callback=initMap&libraries=geometry,places&language=en"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?= base_url() ?>assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
    <script src="<?= base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
    <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css">
    <!--<script src="<?= base_url(); ?>js/common.js"></script>-->

    <link rel="stylesheet" href="<?= base_url() . 'css/' ?>AdminMain.css">

    <div id="directions-panel"></div>
    <script>
        $(document).ready(function () {
            MapSetMarker();
        });
        function MapSetMarker(SalesId = '') {
            var DEFAULT_LAT = 32.5832258;
            var DEFAULT_LONG = 73.4905263;

            var data = {status: 'getRiderlivelocation'};
            $.ajax({
                url: base_url + "Frontend/Helper?<?= $_SERVER['QUERY_STRING'] ?>",
                async: false,
                data: data,
                type: 'POST',
                success: function (data) {
                    try {
                        var json = jQuery.parseJSON(data);
                        console.log(json);
                        var markerArray = json.result;

                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 12,
                            center: {"lat": DEFAULT_LAT, "lng": DEFAULT_LONG}
                        });
                        var infowindow = new google.maps.InfoWindow();
                        for (var i = 0; i < markerArray.length; i++) {
                            var d = markerArray[i];

                            var image = base_url + 'images/marker-red.png';
                            if (d.online_status == '1') {
                                image = base_url + 'images/marker-green.png';
                            }
                            marker = new google.maps.Marker({
                                position: new google.maps.LatLng(parseFloat(d.liv_lat), parseFloat(d.liv_long)),
                                map: map,
                                label: {color: "#ffff", fontSize: "12px", fontWeight: "bold"},
                                animation: google.maps.Animation.DROP,
                                icon: image
                            });
                            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                                return function () {
                                    var d = markerArray[i];
                                    infowindow.setContent('<a  href="javascript:void(0)">' + d.name + ' - ' + d.liv_address + ' - ' + d.lastupdate + ' </a>');
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));
                        }
                    } catch (e) {
                        alert(e);
                    }
                }
            });
        }
    </script>
</html>