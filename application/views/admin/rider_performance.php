<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Rider Performance</span>

                        </div>
                    </div>



                    <form method="post" action="">  
                        <div class="panel-group accordion" id="accordion1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="advance_filter accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1" aria-expanded="false"> <?php echo $this->lang->line("advance_filter") ?> </a>
                                    </h4>
                                </div>
                                <div id="collapse_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body">


                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label> <?php echo $this->lang->line("start_date") ?></label>
                                                <input name="start_date" value="<?= !empty($_POST['start_date']) ? $_POST['start_date'] : '' ?>" type="text" class="form-control datepicker2" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label> <?php echo $this->lang->line("end_date") ?></label>

                                                <input name="end_date" value="<?= !empty($_POST['end_date']) ? $_POST['end_date'] : '' ?>" type="text" class="form-control datepicker2" autocomplete="off">
                                            </div>
                                        </div>







                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label style="width: 100%;">&nbsp; </label>
                                                <input type="submit" name="Filter" class="btn blue" value=" <?php echo $this->lang->line("search") ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Rider Name</th>
                                    <th>Earnings Order</th>
                                    <th style="display:none;">Online Hours </th>
                                    <th>Commission </th>
                                    <th>No of Delivered</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cond = '';
                               
                                $condonline = '';
                                if (isset($_POST['Filter'])) {
                                    extract($_POST);
                                    if (!empty($start_date) && !empty($end_date)) {
                                        $start_date = date('Y-m-d', strtotime($start_date));
                                        $end_date = date('Y-m-d', strtotime($end_date));

                                        $condonline = " AND DATE(start_date) >= '$start_date' AND DATE(end_time) <='$end_date' ";

                                        $cond = " AND DATE(date) >= '$start_date' AND DATE(date) <='$end_date' ";
                                        
                                        
                                    }
                                }
                                $onlineHrs = ",(SELECT SUM((HOUR(TIMEDIFF(start_date,end_time))*60)+MINUTE(TIMEDIFF(start_date,end_time)))  FROM `vendor_online_history` WHERE vendor_id=U.id $condonline) AS totalmin";
                                $earning = ",(SELECT (SUM(total_price)+SUM(service_charge))   FROM `orders` WHERE `assign_rider` = U.id AND archive=0 $cond) AS earning";
                                $rider_commission = ",(SELECT (SUM(rider_commission))   FROM `orders` WHERE `assign_rider` = U.id AND archive=0 $cond) AS rider_commission";
                                $total_delvrd = ",(SELECT (COUNT(orderno))   FROM `orders` WHERE `assign_rider` = U.id AND order_status=6 AND archive=0 $cond) AS total_delvrd";

                                //$qry = "SELECT U.id,U.name $onlineHrs $earning $rider_commission $total_delvrd FROM `users` U LEFT JOIN orders O ON O.vendor_id=U.id WHERE U.user_type=4 AND U.archive=0 AND U.id IN (367)  GROUP BY U.id";
                                $qry = "SELECT U.id,U.name $onlineHrs $earning $rider_commission $total_delvrd FROM `users` U LEFT JOIN orders O ON O.vendor_id=U.id WHERE U.user_type=4 AND U.archive=0  GROUP BY U.id";
                                $dArray = $this->Database->select_qry_array($qry);
                                for ($i = 0; $i < count($dArray); $i++) {
                                    $d = $dArray[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->name ?></td>
                                        <td><?= !empty($d->earning) ? DecimalAmount($d->earning) : '' ?></td>
                                        <td style="display:none;"><?= !empty($d->totalmin) ? (intdiv($d->totalmin, 60) . ':' . ($d->totalmin % 60)) . ' hrs' : ''; ?></td>   
                                        <td><?= !empty($d->rider_commission) ? DecimalAmount($d->rider_commission) : '' ?></td>  
                                        <td><?= !empty($d->total_delvrd) ? $d->total_delvrd : '' ?></td>   

                                        <td>
                                            <a target="_blank" href="<?= base_url('admin/deliveryboydetails?id=' . base64_encode($d->id)); ?>" data-toggle="tooltip" title="Update"><span class="label label-sm label-success"><i class="fas fa-external-link-alt"></i></span></a>

                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
