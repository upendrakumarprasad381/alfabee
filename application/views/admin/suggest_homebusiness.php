
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Suggested Business</span>
                            
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover commontableXXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no")?></th>
                                    <th>Business Name</th>
                                    <th><?php echo $this->lang->line("location")?></th>
                                    <th><?php echo $this->lang->line("contact_details")?></th>
                                    <th><?php echo $this->lang->line("additional_comments")?></th>
                                    
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($customer); $i++) {
                                    $d = $customer[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->name ?></td>
                                        <td><?= $d->location ?></td>
                                        <td><?= $d->contact_details ?></td>
                                        <td><?= $d->additional_comments?></td>
                                        
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
