
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Transactions</span>

                        </div>
                    </div>


                    <form method="post" action="">  
                        <div class="panel-group accordion" id="accordion1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="advance_filter accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1" aria-expanded="false"> <?php echo $this->lang->line("advance_filter") ?> </a>
                                    </h4>
                                </div>
                                <div id="collapse_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body">


                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Vendor</label>
                                                <select class="form-control selectpicker" data-live-search="true" name="vendor_id">
                                                    <option value="">--select--</option>
                                                    <?php
                                                    $vendorff_id = !empty($_POST['vendor_id']) ? $_POST['vendor_id'] : '';
                                                    $qry = "SELECT U.name,U.id,RD.restaurant_name FROM  users U LEFT JOIN restaurant_details RD ON RD.vendor_id=U.id  WHERE U.archive=0 AND is_approved != 0 AND U.user_type=2  AND U.status=1";
                                                    $dlist = $this->Database->select_qry_array($qry);
                                                    for ($i = 0; $i < count($dlist); $i++) {
                                                        $d = $dlist[$i];
                                                        ?><option <?= $vendorff_id == $d->id ? 'selected' : '' ?> value="<?= $d->id ?>"><?= $d->name . ' - ' . $d->restaurant_name ?></option><?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>




                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Order No</label>
                                                <input name="Filterorderno" value="<?= !empty($_POST['Filterorderno']) ? $_POST['Filterorderno'] : '' ?>" type="text" class="form-control " autocomplete="off">
                                            </div>
                                        </div>


                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label> <?php echo $this->lang->line("start_date") ?></label>
                                                <input name="FilterStartDate" value="<?= !empty($_POST['FilterStartDate']) ? $_POST['FilterStartDate'] : '' ?>" type="text" class="form-control datepicker2" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label> <?php echo $this->lang->line("end_date") ?></label>

                                                <input name="FilterEndDate" value="<?= !empty($_POST['FilterEndDate']) ? $_POST['FilterEndDate'] : '' ?>" type="text" class="form-control datepicker2" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label style="width: 100%;">&nbsp; </label>
                                                <input type="submit" name="Filter" class="btn blue" value=" <?php echo $this->lang->line("search") ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no") ?></th>
                                    <th>Order No</th>
                                    <th>Vendor Name</th>
                                    <th><?php echo $this->lang->line("customer_name") ?></th>
                                    <th><?php echo $this->lang->line("mode_of_payment") ?></th>
                                    <th><?php echo $this->lang->line("order_date") ?></th>
                                    <th><?php echo $this->lang->line("amount") ?>  In PKR</th>
                                    <th>Service Charge  In PKR</th>

                                    <th>Vendor Earnings  In PKR</th>
                                    <th>Alfabee Earnings  In PKR</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $Cond = '';
                                if (isset($_POST['Filter'])) {
                                    extract($_POST);
                                    if (!empty($vendor_id)) {
                                        $Cond = $Cond . " AND O.vendor_id LIKE '$vendor_id'";
                                    }
                                    if (!empty($Filterorderno)) {
                                        $Cond = $Cond . " AND O.orderno LIKE '%$Filterorderno%'";
                                    }
                                    if (!empty($FilterStartDate) && !empty($FilterEndDate)) {
                                        $datefrom = date('Y-m-d', strtotime($FilterStartDate));
                                        $dateto = date('Y-m-d', strtotime($FilterEndDate));
                                        $Cond = $Cond. "AND DATE(O.date) BETWEEN '$datefrom' AND '$dateto'";
                                    } else if (!empty($FilterStartDate)) {
                                        $datefrom = date('Y-m-d', strtotime($FilterStartDate));
                                        $Cond = $Cond . "AND DATE(O.date) >= '$datefrom'";
                                    } elseif (!empty($FilterEndDate)) {
                                        $dateto = date('Y-m-d', strtotime($FilterEndDate));
                                        $Cond = $Cond . "AND DATE(O.date) < '$dateto'";
                                    }
                                   
                                }
                                $vendorId = !empty($_GET['vendorId']) ? $_GET['vendorId'] : '';
                                $rtype = !empty($_GET['rtype']) ? $_GET['rtype'] : '';

                                $todats = date('Y-m-d');
                                $before7days = date('Y-m-d', strtotime('-7 days'));
                                if (!empty($vendorId)) {
                                    $Cond = $Cond . "AND O.vendor_id= '$vendorId'";
                                }if ($rtype == 'todays') {
                                    $Cond = $Cond . "AND DATE(date)='$todats'";
                                }if ($rtype == 'weekly') {
                                    $Cond = $Cond . " AND DATE(date) BETWEEN '$before7days' AND '$todats' ";
                                }if ($rtype == 'monthly') {
                                    $Cond = $Cond . " AND MONTH(date) = '" . date('m') . "' AND YEAR(date) = '" . date('Y') . "'";
                                }if ($rtype == 'yearly') {
                                    $Cond = $Cond . " AND YEAR(date) = '" . date('Y') . "'";
                                }




                                $distnc = '';
                                $having = '';
                                if (isset($_REQUEST['searchBylatlong'])) {
                                    $latitude = !empty($_REQUEST['latitude']) ? $_REQUEST['latitude'] : '';
                                    $longitude = !empty($_REQUEST['longitude']) ? $_REQUEST['longitude'] : '';
                                    $distance = !empty($_REQUEST['distance']) ? $_REQUEST['distance'] : '';

                                    $having = $having . "HAVING distnc<= '$distance'";
                                    $distnc = ",IFNULL(ROUND((6371 * acos( cos( radians($latitude) ) * cos( radians(UOD.latitude) ) * cos( radians(UOD.longitude) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians(UOD.latitude) ) ) ),2),0) AS distnc";
                                }





                                $select = ",U.name,RD.restaurant_name,UT.restaurant_earning,UT.homeeats_earning,RD.commission";
                                $join = " LEFT JOIN users U ON U.id=O.user_id LEFT JOIN restaurant_details RD ON RD.vendor_id=O.vendor_id LEFT JOIN user_transactions UT ON UT.order_id=O.id LEFT JOIN user_order_address UOD ON UOD.id=O.id";
                                $Qry = "SELECT O.* $select $distnc FROM `orders` O $join WHERE  O.archive=0 AND O.order_status NOT IN(7) $Cond $having order by O.id desc LIMIT 2000";
                                $transactions = $this->Database->select_qry_array($Qry);


                                $grandTot=0;
                                $service_charge=0;
                                $totalVendoEar=0;
                                $totalAlfabeeEar=0;
                                for ($i = 0; $i < count($transactions); $i++) {
                                    $d = $transactions[$i];
                                    $total = $d->total_price ; // + $d->service_charge
                                    $alfaBeeEar = ($total * $d->commission) / 100;
                                    $vendorEar=$total - $alfaBeeEar;
                                    
                                    
                                    $grandTot=$grandTot+$total;
                                    $service_charge=$service_charge+$d->service_charge;
                                    $totalVendoEar=$totalVendoEar+$vendorEar;
                                    $totalAlfabeeEar=$totalAlfabeeEar+$alfaBeeEar;
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->orderno; ?></td>
                                        <td><?= $d->restaurant_name ?> </td> 
                                        <td><?= $d->name ?></td>
                                        <td>
                                            <?php
                                            if ($d->transaction_type == 1) {
                                                echo 'Online';
                                            } else {
                                                echo 'Cash on delivery';
                                            }
                                            ?>
                                        </td>
                                        <td><?= date('d/m/Y', strtotime($d->date)) ?></td>
                                        <td><?= number_format($total, 2) ?></td>
                                        <td><?= number_format($d->service_charge, 2) ?></td>


                                        <td><?= number_format($vendorEar, 2) ?></td>
                                        <td><?= number_format($alfaBeeEar, 2) ?></td>

                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>



                        <div class="row">
                            <br>
                            <div class="col-sm-4" style="    float: right;">
                                <style>
                                    #calc table, th, td {
                                        border: 1px solid black;
                                        border-collapse: collapse;
                                    }
                                    #calc th, td {
                                        text-align: center;
                                        padding: 15px;
                                    }
                                </style>
                                <table id="calc" style="width: 100%;">
                                    <tr>
                                        <th>Amount  In PKR</th>
                                        <td><?= DecimalAmount($grandTot) ?></td>
                                    </tr>
                                    <tr>
                                        <th>Service Charge  In PKR</th>
                                        <td><?= DecimalAmount($service_charge) ?></td>
                                    </tr>
                                    <tr>
                                        <th>Vendor Earnings  In PKR</th>
                                        <td><?= DecimalAmount($totalVendoEar) ?></td>
                                    </tr>
                                    <tr>
                                        <th>AlfaBee Earnings  In PKR</th>
                                        <td><?= DecimalAmount($totalAlfabeeEar) ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
