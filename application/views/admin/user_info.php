<?php
$UserArray = GetUserDetails($UserId);
?>
<style>
.portlet.light .portlet-body {
    padding-top: 0px;
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption" style="padding: 0px;">
                            <?php
                            $Select = 'selected="selected"';
                            $UserProfile = 'uploads/user_images/' . $UserArray[0]->image;
                            if (is_file(HOME_DIR . $UserProfile)) {
                                $ImageHtml = '<img style="border-radius: 50% !important;width:32px" src="' . base_url() . $UserProfile . '" />';
                            } else {
                                $ImageHtml = '<i style="font-size: 36px;" class="fab fa-first-order"></i>';
                            }
                            echo $ImageHtml;
                            ?>
                            <span class="caption-subject font-dark sbold"> <span class="uppercase"> <?= $UserArray[0]->name . ' - ' . $UserArray[0]->id ?></span>
                                <?php
                                $Archivetitle = 'double click to block this user';
                                $Archive = 1;
                                $Archivemsg = $this->lang->line("active");
                                $ArchiveIcon = '<i style="color: #10ef36f7;" class="fas fa-circle"></i>';
                                if ($UserArray[0]->archive == 1) {
                                    $Archivetitle = 'double click to unblock this user';
                                    $Archive = 0;
                                    $Archivemsg = $this->lang->line("inactive");
                                    $ArchiveIcon = '<i style="color: red;cursor: pointer;" class="fas fa-ban"></i>';
                                }
                                ?>
                                <span title="<?= $Archivetitle ?>" Archive="<?= $Archive ?>" class="hidden-xs CustomerBlockUnblock" style="cursor: pointer;" CustomerId="<?= $UserArray[0]->id ?>">|    <?= $ArchiveIcon . ' ' . $Archivemsg ?></span>
                            </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="portlet blue-hoki box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i style="font-size: 24px;margin-top: 2px;" class="fas fa-user"></i>

                                                    <?php echo $this->lang->line("basic_information")?>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("customer_name")?>: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= $UserArray[0]->name ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info" style="display: none;">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("gender")?>: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?php
                                                        if ($UserArray[0]->gender == 1) {
                                                            echo $this->lang->line("male");
                                                        } else if ($UserArray[0]->gender == 2) {
                                                            echo $this->lang->line("female");
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("creation_date")?>: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= date('M d, Y H:i:s A', strtotime($UserArray[0]->timestamp)) ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("account_status")?>: </div>
                                                    <div class="col-md-7 value"> 
                                                        <span style="cursor: pointer;" title="<?= $Archivetitle ?>" Archive="<?= $Archive ?>" class="CustomerBlockUnblock" CustomerId="<?= $UserArray[0]->id ?>"> <?= $ArchiveIcon ?> <?= $Archivemsg ?></span>
                                                    </div>
                                                </div>

                                                <div class="row static-info">
                                                    <div class="col-md-5 name">&nbsp;</div>
                                                    <div class="col-md-7 value"> 
                                                        &nbsp;
                                                    </div>
                                                </div> 
                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="portlet blue-hoki box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i style="font-size: 24px;margin-top: 2px;" class="fas fa-info-circle"></i> <?php echo $this->lang->line("additional_information")?>
                                                </div>
                                            </div>
                                            <div class="portlet-body">

                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("email_id")?>: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= $UserArray[0]->email ?>
                                                    </div>
                                                </div>


                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("mobile_number")?>: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= $UserArray[0]->mobile_code . $UserArray[0]->mobile_number ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info" style="display: none;">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("country_of_residence")?>: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= GetNameById($UserArray[0]->country_id,'countries','country_name') ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("login_info")?>: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?php
                                                        if ($UserArray[0]->password != '') {
                                                            echo '<i title="genral login" style="color:#4d5bff;font-size: 17px;cursor: pointer;" class="fas fa-sign-in-alt"></i>';
                                                        }
                                                        if ($UserArray[0]->login_type != 0) {
                                                            if ($UserArray[0]->login_type == 2) {
                                                                echo '&nbsp; <i title="google login" style="color:#ff4d4d;font-size: 17px;cursor: pointer;" class="fab fa-google-plus"></i>';
                                                            }
                                                            if ($UserArray[0]->login_type == 1) {
                                                                echo '&nbsp; <i title="facebook login" style="color:#714dff;;font-size: 17px;cursor: pointer;" class="fab fa-facebook"></i>';
                                                            }
                                                            // if ($UserArray->social_media_type == 3) {
                                                            //     echo '&nbsp; <i title="twitter login" style="color:#714dff;;font-size: 17px;cursor: pointer;" class="fab fa-twitter"></i>';
                                                            // }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"><?php echo $this->lang->line("total_orders")?>: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?php $ArrayOrder = GetCustomerTotalOrder($UserId); ?>
                                                        <?= $ArrayOrder['TotalOrder'] ?> Orders 
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    for ($i = 0; $i < count($AddressArray); $i++) {
                                        $d = $AddressArray[$i];
                                        ?>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet blue-hoki box">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i style="font-size: 24px;margin-top: 2px;" class="far fa-address-card"></i><?php echo $this->lang->line("delivery_address")?> - <?= $i + 1 ?>
                                                        <?php if ($d->archive == 1) { ?>
                                                            <span><i title="Inactive Address" style="color: red;cursor: pointer;" class="fas fa-ban"></i></span>
                                                        <?php } ?></div>

                                                </div>
                                                <div class="portlet-body">
                                                    
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("building_name_no")?>: </div>
                                                        <div class="col-md-7 value"> <?= $d->building ?> </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("street")?>: </div>
                                                        <div class="col-md-7 value"> <?= $d->street ?> </div>
                                                    </div>
                                                    
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("building_type")?>: </div>
                                                        <div class="col-md-7 value"> 
                                                        <?php
                                                    if($d->address_label==1)
                                                    {
                                                        $type = $this->lang->line("apartment");
                                                    }elseif($d->address_label==2){
                                                        $type = $this->lang->line("house");
                                                    }else{
                                                        $type = $this->lang->line("office");
                                                    }
                                                    ?>
                                                    <?= $type?>, <?php echo $this->lang->line("mobile")?> -<?= $d->mobile_code . $d->mobile_number ?>  </div>
                                                    </div>
                                                    <?php
                                                    if($d->additional_direction!='undefined')
                                                    {
                                                    ?>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("additional_direction")?>: </div>
                                                        <div class="col-md-7 value"> <?= $d->additional_direction ?> </div>
                                                    </div>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>



                            </div>

                        </div>
                    </div>
                </div>
                <!-- End: life time stats -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?php echo $this->lang->line("order_details")?></span>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no")?></th>
                                    <th><?php echo $this->lang->line("order_no")?></th>
                                    <th><?php echo $this->lang->line("price")?></th>
                                 
                                    <th><?php echo $this->lang->line("order_status")?></th>
                                    <th><?php echo $this->lang->line("order_date")?></th>
                                    <th><?php echo $this->lang->line("action")?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($order); $i++) {
                                    $d = $order[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->orderno ?></td>
                                        <td>PKR <?=number_format($d->total_price+$d->service_charge,2)?></td>
                                     
                                        <td><span class="label label-sm label-success" style="    background-color: <?= $d->status_color ?>;"><?= !empty($d->status_name) ? $d->status_name : 'Received' ?></span></td>
                                        <td><?= date('D, d M-Y h:i A', strtotime($d->date)) ?></td>
                                        <td>
<!--                                            <a ><span class="label label-sm label-success"><i class="fa fa-pencil" aria-hidden="true"></i></span></a>-->
                                            <a href="<?= base_url('Admin/OrderDetails/' . base64_encode($d->id)) ?>" title="View"><span class="label label-sm label-danger"><i class="fa fa-eye"></i></span></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
