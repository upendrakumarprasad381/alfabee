<?php 
$Session =  $this->session->userdata('Admin');
$ADMIN_PERMISSION_ARRAY = ADMIN_PERMISSION_ARRAY;
$Permission = GetMenuPermission();
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Admin Login</span>
                           <?php if (in_array(1, $Permission)) { ?>   <a href="<?= base_url('Admin/AddNewUser') ?>" class="btn btn-sm green small"> Add-New
                                <i class="fa fa-plus"></i>
                            </a> <?php } ?>
                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>User Type</th>
                                    <th>Phone Number</th>
                                    <th>Date</th>
                                    <th>Archive</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($UserArray); $i++) {
                                    $d = $UserArray[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->name ?></td>
                                        <td><?= $d->email ?></td>
                                        <td><?php  if($d->LoginType=='1'){ echo 'Super Admin';}else{ echo 'Sub Admin';} ?></td>
                                        <td><?= $d->mobile ?></td>
                                        <td><?= date('H:i-d/m/Y', strtotime($d->timestamp)); ?></td>
                                        <td><?= $d->status == 0 ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Inactive</span>' ?></td>
                                        <td style="width: 100px;">
                                        <?php if (in_array(2, $Permission)) { ?>
                                        <a title="Edit" href="<?= base_url('Admin/AddNewUser/' . base64_encode($d->id)); ?>"><span class="label label-sm label-success"><i class="fa fa-pencil" aria-hidden="true"></i></span></a>
                                        <?php } 
                                        
                                        if (in_array($Session->id, $ADMIN_PERMISSION_ARRAY)) {
                                        ?>  <a href="<?= base_url('Admin/userpermission?userId=' . base64_encode($d->id)) ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Set Permission"><i class="fas fa-lock"></i></a><?php
                                        }
                                        ?>
                                        <?php if (in_array(3, $Permission)) { ?> 
                                         <a href="javascript:void(0)" cmessage='Are you sure want to remove ?' updatejson='{"archive":"1"}'  condjson='{"id":"<?= $d->id ?>"}' dbtable="admin_login" class="autoupdate" title="reject"><span class="label label-sm label-danger"><i class="fas fa-trash-alt"></i></span></a>
                                            
                                            <?php } ?>

                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
