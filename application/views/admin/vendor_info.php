<?php
if (isset($_POST['submit'])) {
    $user = !empty($_POST['user']) ? $_POST['user'] : [];
    $user_id = !empty($user['user_id']) ? $user['user_id'] : '';
    unset($user['user_id']);
    if (!empty($user_id)) {
        $this->Database->update('users', $user, array('id' => $user_id));
    }
    header("Refresh:0");
}


$ConditionArray = array('vendor_id' => $UserId);
$Qry1 = "SELECT * FROM `restaurant_details` WHERE vendor_id=:vendor_id";
$Array1 = $this->Database->select_qry_array($Qry1, $ConditionArray);
$store = !empty($Array1) ? $Array1[0] : '';





$UserArray = GetUserDetails($UserId);
$count_order = total_order_byid($restauarnt[0]->vendor_id);
$count_ongoing = total_ongoingorder_byid($restauarnt[0]->vendor_id);
$count_completed = total_completedorder_byid($restauarnt[0]->vendor_id);
?>
<style>
    .fasCd {
        opacity: 1 !important;
        line-height: 50px !important;
        margin-left: -5px !important;
        font-size: 50px !important;
    }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption" style="padding: 0px;">
                            <?php
                            $Select = 'selected="selected"';
                            $UserProfile = 'uploads/vendor_images/' . $UserArray[0]->image;
                            if (is_file(HOME_DIR . $UserProfile)) {
                                $ImageHtml = '<img style="border-radius: 50% !important;width:32px" src="' . base_url() . $UserProfile . '" />';
                            } else {
                                $ImageHtml = '<i style="font-size: 36px;" class="fab fa-first-order"></i>';
                            }
                            echo $ImageHtml;
                            ?>
                            <?php
                            if ($UserArray[0]->is_approved == 1) {
                                ?>
                                <span class="caption-subject font-dark sbold"> <span class="uppercase"> <?= $UserArray[0]->name . ' - ' . $UserArray[0]->id ?></span>
                                    <?php
                                    $Archivetitle = 'double click to block this user';
                                    $Archive = 1;
                                    $Archivemsg = $this->lang->line("active");
                                    $ArchiveIcon = '<i style="color: #10ef36f7;" class="fas fa-circle"></i>';
                                    if ($UserArray[0]->archive == 1) {
                                        $Archivetitle = 'double click to unblock this user';
                                        $Archive = 0;
                                        $Archivemsg = $this->lang->line("inactive");
                                        $ArchiveIcon = '<i style="color: red;cursor: pointer;" class="fas fa-ban"></i>';
                                    }
                                    ?>
                                    <span title="<?= $Archivetitle ?>" Archive="<?= $Archive ?>" class="hidden-xs CustomerBlockUnblock" style="cursor: pointer;" CustomerId="<?= $UserArray[0]->id ?>">|    <?= $ArchiveIcon . ' ' . $Archivemsg ?></span>
                                </span>
                                <?php
                            } else {
                                ?>
                                <span class="caption-subject font-dark sbold"> <span class="uppercase"> <?= $UserArray[0]->name . ' - ' . $UserArray[0]->id ?></span>

                                    <?php
                                }
                                ?>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="portlet blue-hoki box">
                                            <a class="dashboard-stat dashboard-stat-v2 blue" href="<?= base_url('admin/orders') ?>">
                                                <div class="visual">
                                                    <i class="fasCd fa fa-shopping-cart"></i>
                                                </div>
                                                <div class="details">
                                                    <div class="number">
                                                        <span data-counter="counterup" data-value="1349"><?= $count_order[0]->total_order ?></span>
                                                    </div>
                                                    <div class="desc"> <?php echo $this->lang->line("total_orders") ?> </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="portlet blue-hoki box">
                                            <a class="dashboard-stat dashboard-stat-v2 red" href="<?= base_url('admin/orders') ?>">
                                                <div class="visual">
                                                    <i class="fasCd fa fa-bar-chart-o"></i>
                                                </div>
                                                <div class="details">
                                                    <div class="number">
                                                        <span data-counter="counterup" data-value="1349"><?= $count_ongoing[0]->total_order ?></span>
                                                    </div>
                                                    <div class="desc"> Ongoing Orders </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="portlet blue-hoki box">
                                            <a class="dashboard-stat dashboard-stat-v2 green" href="<?= base_url('admin/orders') ?>">
                                                <div class="visual">
                                                    <i class="fasCd fas fa-check-circle"></i>
                                                </div>
                                                <div class="details">
                                                    <div class="number">
                                                        <span data-counter="counterup" data-value="1349"><?= $count_completed[0]->total_order ?></span>
                                                    </div>
                                                    <div class="desc"> Order Delivered </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>


                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="portlet blue-hoki box">
                                            <a style="background-color: #009688;" class="dashboard-stat dashboard-stat-v2 green" href="<?= base_url('admin/transactions?vendorId=' . $UserId) ?>">
                                                <div class="visual">
                                                    <i class="fasCd fas fa-check-circle"></i>
                                                </div>
                                                <div class="details">
                                                    <div class="number">
                                                        <?php
                                                        $rurry = "SELECT (SUM(service_charge)+SUM(total_price)) AS finalTotal FROM `orders` WHERE archive=0 AND order_status NOT IN (7) AND vendor_id='$UserId'";
                                                        $calAmt = $this->Database->select_qry_array($rurry);
                                                        $finaTotal = !empty($calAmt[0]->finalTotal) ? $calAmt[0]->finalTotal : '0';
                                                        ?>
                                                        <span data-counter="counterup" data-value="<?= DecimalAmount($finaTotal) ?>"><?= DecimalAmount($finaTotal) ?></span>
                                                    </div>
                                                    <div class="desc">Vendor Earnings </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>










                                    <?php
                                    $todats = date('Y-m-d');
                                    $before7days = date('Y-m-d', strtotime('-7 days'));


                                    $oneweek = ",(SELECT (SUM(service_charge)+SUM(total_price)) FROM `orders` WHERE archive=0 AND order_status NOT IN (7) AND vendor_id='$UserId' AND DATE(date) BETWEEN '$before7days' AND '$todats') AS one_week";
                                    $this_month = ",(SELECT (SUM(service_charge)+SUM(total_price)) FROM `orders` WHERE archive=0 AND order_status NOT IN (7) AND vendor_id='$UserId' AND MONTH(date) = '" . date('m') . "' AND YEAR(date) = '" . date('Y') . "') AS this_month";
                                    $this_year = ",(SELECT (SUM(service_charge)+SUM(total_price)) FROM `orders` WHERE archive=0 AND order_status NOT IN (7) AND vendor_id='$UserId'  AND YEAR(date) = '" . date('Y') . "') AS this_year";

                                    $rurry = "SELECT (SUM(service_charge)+SUM(total_price)) AS todays_order $oneweek $this_month $this_year FROM `orders` WHERE archive=0 AND order_status NOT IN (7) AND vendor_id='$UserId' AND DATE(date)='$todats'";
                                    $calAmt = $this->Database->select_qry_array($rurry);
                                    $calc = !empty($calAmt[0]) ? $calAmt[0] : '';
                                    ?>

                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="portlet blue-hoki box">
                                            <a style="background-color: #9C27B0;" class="dashboard-stat dashboard-stat-v2 green" href="<?= base_url('admin/transactions?vendorId=' . $UserId . '&rtype=todays') ?>">
                                                <div class="visual">
                                                    <i class="fasCd fas fa-check-circle"></i>
                                                </div>
                                                <div class="details">
                                                    <div class="number">

                                                        <span data-counter="counterup" data-value=""><?= DecimalAmount($calc->todays_order) ?></span>
                                                    </div>
                                                    <div class="desc">Todays Earnings </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="portlet blue-hoki box">
                                            <a style="    background-color: #009688;" class="dashboard-stat dashboard-stat-v2 green" href="<?= base_url('admin/transactions?vendorId=' . $UserId . '&rtype=weekly') ?>">
                                                <div class="visual">
                                                    <i class="fasCd fas fa-check-circle"></i>
                                                </div>
                                                <div class="details">
                                                    <div class="number">

                                                        <span data-counter="counterup" data-value=""><?= DecimalAmount($calc->one_week) ?></span>
                                                    </div>
                                                    <div class="desc">Weekly Earnings </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="portlet blue-hoki box">
                                            <a style="    background-color: #4CAF50;" class="dashboard-stat dashboard-stat-v2 green" href="<?= base_url('admin/transactions?vendorId=' . $UserId . '&rtype=monthly') ?>">
                                                <div class="visual">
                                                    <i class="fasCd fas fa-check-circle"></i>
                                                </div>
                                                <div class="details">
                                                    <div class="number">

                                                        <span data-counter="counterup" data-value=""><?= DecimalAmount($calc->this_month) ?></span>
                                                    </div>
                                                    <div class="desc">Monthly Earnings </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="portlet blue-hoki box">
                                            <a style="background-color: #6523da;" class="dashboard-stat dashboard-stat-v2 green" href="<?= base_url('admin/transactions?vendorId=' . $UserId . '&rtype=yearly') ?>">
                                                <div class="visual">
                                                    <i class="fasCd fas fa-check-circle"></i>
                                                </div>
                                                <div class="details">
                                                    <div class="number">

                                                        <span data-counter="counterup" data-value=""><?= DecimalAmount($calc->this_year) ?></span>
                                                    </div>
                                                    <div class="desc">Yearly  Earnings </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>



















                                    <div class="col-md-12 col-sm-12">
                                        <div class="portlet blue-hoki box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i style="font-size: 24px;margin-top: 2px;" class="fas fa-user"></i>

                                                    <?php echo $this->lang->line("basic_information") ?>
                                                </div>
                                                <a data-toggle="modal" data-target="#basicExampleModal" style="float: right;color: white;margin-top: 10px;"><i style="font-size: 24px;margin-top: 2px;" class="far fa-edit"></i></a>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Vendor Name: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= $UserArray[0]->name ?>
                                                    </div>
                                                </div>

                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("email") ?> : </div>
                                                    <div class="col-md-3 value"> 
                                                        <?= $UserArray[0]->email ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("mobile_number") ?> : </div>
                                                    <div class="col-md-3 value"> 
                                                        <?= $UserArray[0]->mobile_code ?> <?= $UserArray[0]->mobile_number ?>
                                                    </div>
                                                </div>


                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("creation_date") ?>: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= date('M d, Y H:i:s A', strtotime($UserArray[0]->timestamp)) ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("account_status") ?>: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?php
                                                        if ($UserArray[0]->is_approved == 1) {
                                                            ?>
                                                            <span style="cursor: pointer;" title="<?= $Archivetitle ?>" Archive="<?= $Archive ?>" class="CustomerBlockUnblock" CustomerId="<?= $UserArray[0]->id ?>"> <?= $ArchiveIcon ?> <?= $Archivemsg ?></span>
                                                        <?php } elseif ($UserArray[0]->is_approved == 0) {
                                                            ?>
                                                            <span style="cursor: pointer;" >Waiting for approval</span>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <span style="cursor: pointer;" >Rejected by Admin</span>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info" style="display:none;">
                                                    <input type="hidden" value="<?= $UserArray[0]->id ?>" id="vendor_id">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("premium_user") ?>: </div>
                                                    <div class="col-md-7 value"> 
                                                        <input type="radio" value="1" name="premium" id="premium" <?php echo ($restauarnt[0]->isPremium == 1) ? "checked" : ""; ?>> <?php echo $this->lang->line("yes") ?> &nbsp;&nbsp;
                                                        <input type="radio" value="0" name="premium" id="premium" <?php echo ($restauarnt[0]->isPremium == 0) ? "checked" : ""; ?>> <?php echo $this->lang->line("no") ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="portlet blue-hoki box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i style="font-size: 24px;margin-top: 2px;" class="fas fa-info-circle"></i> <?php echo $this->lang->line("business_info") ?> 
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <input type="hidden" value="<?= $restauarnt[0]->vendor_id ?>" id="vendor_id">
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("store_type") ?> : </div>
                                                    <div class="col-md-3 value"> 
                                                        <select id="store_type" class="form-control">
                                                            <?php
                                                            $store_type = !empty($UserArray[0]->store_type) ? $UserArray[0]->store_type : '';
                                                            $storelist = GetstoretypeByAll();
                                                            for ($i = 0; $i < count($storelist); $i++) {
                                                                $d = $storelist[$i];
                                                                ?><option <?= $d->id == $store_type ? 'selected' : '' ?> value="<?= $d->id ?>"><?= $d->store_type ?></option><?php
                                                            }
                                                            ?>

                                                        </select>

                                                    </div>
                                                </div>

                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Active Status : </div>
                                                    <div class="col-md-3 value"> 
                                                        <select id="status" class="form-control">
                                                            <?php
                                                            $status = !empty($UserArray[0]->status) ? $UserArray[0]->status : ''
                                                            ?>
                                                            <option <?= !empty($status) ? 'selected' : '' ?> value="1">Active</option>
                                                            <option <?= empty($status) ? 'selected' : '' ?> value="0">Inactive</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Business Name : </div>
                                                    <div class="col-md-3 value"> 
                                                        <input type="text" id="restaurant_name" class="form-control" value="<?= $restauarnt[0]->restaurant_name ?>">
                                                    </div>
                                                </div>


                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Delivery Radius <small>(In km)</small>: </div>
                                                    <div class="col-md-3 value"> 
                                                        <input type="number" id="delivery_radius" class="form-control" value="<?= $UserArray[0]->delivery_radius ?>"> 
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Free Delivery <small>(Minimum order amount)</small>: </div>
                                                    <div class="col-md-3 value"> 
                                                        <input type="number" id="free_delivery" class="form-control" value="<?= !empty($UserArray[0]->free_delivery) ? $UserArray[0]->free_delivery : '' ?>"> 
                                                    </div>
                                                </div>
                                                <?php
                                                if ($UserArray[0]->store_type == 1) {
                                                    $cusine = GetCuisine();
                                                    ?>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("cuisine_type") ?> : </div>
                                                        <div class="col-md-3 value"> 
                                                            <select class="form-control selectpicker" id="cuisine_type" multiple="multiple" data-live-search="true">
                                                                <option value="">select</option>
                                                                <?php
                                                                $cuisineSelected = GetCuisineByRestaurant($restauarnt[0]->vendor_id);
                                                                $cuisineSelectedDec = json_decode(json_encode($cuisineSelected), true);
                                                                $cuisineSelectedDecSingle = array_column($cuisineSelectedDec, 'cuisine_id');
                                                                for ($i = 0; $i < count($cusine); $i++) {
                                                                    $d = $cusine[$i];
                                                                    ?><option <?= in_array($d->cuisine_id, $cuisineSelectedDecSingle) ? 'selected' : '' ?> value="<?= $d->cuisine_id ?>"><?= $d->cuisine_name ?></option><?php
                                                                }
                                                                ?>
                                                            </select>
                                                            <?php
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>





                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("area") ?> : </div>
                                                    <div class="col-md-3 value"> 
                                                        <input type="text" id="autocomplete" class="form-control" value="<?= $UserArray[0]->area ?>">
                                                    </div>
                                                </div>












                                                <div class="row static-info" >
                                                    <div class="col-md-5 name">Delivery hours start time</div>
                                                    <div class="col-md-2">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control timepicker timepicker-no-seconds" id="delivery_hours_st" value="<?php echo isset($Array1[0]->delivery_hours_st) ? $Array1[0]->delivery_hours_st : '' ?>">
                                                            <span class="input-group-btn">
                                                                <button class="btn default" type="button">
                                                                    <i class="fa fa-clock-o"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control timepicker timepicker-no-seconds" id="delivery_hours_et" value="<?php echo isset($Array1[0]->delivery_hours_et) ? $Array1[0]->delivery_hours_et : '' ?>">
                                                            <span class="input-group-btn">
                                                                <button class="btn default" type="button">
                                                                    <i class="fa fa-clock-o"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>








                                                <div class="row static-info" >
                                                    <div class="col-md-5 name"> Delivery : </div>
                                                    <div class="col-md-7 value"> 
                                                        <input type="checkbox" id="isDelivery" <?php echo (isset($restauarnt[0]->isDelivery) && $restauarnt[0]->isDelivery == 1 ? 'checked="checked"' : ''); ?> name="isDelivery" value="1" >
                                                        <label>Yes</label>
                                                    </div>
                                                </div>



                                                <div class="row static-info" >
                                                    <div class="col-md-5 name"> Self Pickup : </div>
                                                    <div class="col-md-7 value"> 
                                                        <input type="checkbox" id="self_pickup" <?php echo (isset($restauarnt[0]->self_pickup) && $restauarnt[0]->self_pickup == 1 ? 'checked="checked"' : ''); ?> name="self_pickup" value="1" >
                                                        <label>Yes</label>
                                                    </div>
                                                </div>
                                                <div class="row static-info" >
                                                    <div class="col-md-5 name"> Fast Delivery : </div>
                                                    <div class="col-md-7 value"> 
                                                        <input type="checkbox" id="fastdelvry" <?php echo (isset($restauarnt[0]->fastdelvry) && $restauarnt[0]->fastdelvry == 1 ? 'checked="checked"' : ''); ?> name="fastdelvry" value="1" >
                                                        <label>Yes</label>
                                                    </div>
                                                </div>



                                                <div class="row static-info" style="display:none">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("street") ?> : </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= $UserArray[0]->street ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info" style="display:none">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("city") ?> : </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= $UserArray[0]->city ?>
                                                    </div>
                                                </div>




                                                <div class="row static-info" >
                                                    <?php
                                                    $charges_type = isset($restauarnt[0]->charges_type) ? $restauarnt[0]->charges_type : '0';
                                                    $store = !empty($restauarnt[0]) ? $restauarnt[0] : '';
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5">Charges Type</label>
                                                        <div class="col-md-3">
                                                            <input type="radio" onclick="ADMINCMN.updatedeleverycharges()" name="charges_type" <?= $charges_type == '0' ? 'checked' : '' ?>  value="0">
                                                            <label>Fixed</label>
                                                            <input type="radio" onclick="ADMINCMN.updatedeleverycharges()" name="charges_type" <?= $charges_type == '1' ? 'checked' : '' ?> value="1">
                                                            <label>Flexible</label>
                                                        </div>
                                                    </div>

                                                    <style>
                                                        #Flexiblecharges  .form-control {
                                                            margin-bottom: 6px !important;
                                                        }
                                                    </style>
                                                    <div id="Flexiblecharges" style="display: <?= $charges_type == '1' ? 'block' : 'none' ?>">
                                                        <div class="form-group static-info">
                                                            <label class="control-label col-md-5">First Kilometre</label>
                                                            <div class="col-md-3">
                                                                <input id="dlvry_chrge_frst_km" value="<?= !empty($store->dlvry_chrge_frst_km) ? $store->dlvry_chrge_frst_km : '' ?>" type="text" placeholder="Kilometre" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">First Kilometre Amount</label>
                                                            <div class="col-md-3">
                                                                <input id="dlvry_chrge" value="<?= !empty($store->service_charge) ? $store->service_charge : '' ?>" type="text" placeholder="Amount" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">Every per Kilometre charges</label>
                                                            <div class="col-md-3">
                                                                <input id="dlvry_chrge_perkm" value="<?= !empty($store->dlvry_chrge_perkm) ? $store->dlvry_chrge_perkm : '' ?>" type="text" placeholder="Amount" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="Fixedcharges" style="display: <?= $charges_type == '0' ? 'block' : 'none' ?>">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">Charges</label>
                                                            <div class="col-md-3">
                                                                <input id="dlvry_chrge_ch" value="<?= !empty($store->service_charge) ? $store->service_charge : '' ?>" type="text" placeholder="Amount" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>




                                                </div>


                                                <div class="row static-info" >
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("commission") ?> (%) : </div>
                                                    <div class="col-md-3 value"> 
                                                        <input id="commission" value="<?= !empty($restauarnt[0]->commission) ? $restauarnt[0]->commission : '' ?>" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="row static-info">

                                                    <div class="col-md-7 col-md-offset-5"> 
                                                        <button type="submit" id="update_details" class="btn green-meadow">Update</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>



                            </div>

                        </div>
                    </div>
                </div>
                <!-- End: life time stats -->
            </div>
        </div>




    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Update Basic Information</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="hidden" name="user[user_id]" value="<?= $UserId ?>">
                            <div class="form-group">
                                <label>Vendor Name</label>
                                <input type="text" id="name" name="user[name]" value=" <?= $UserArray[0]->name ?>" class="form-control" required="" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label>Mobile Number</label>
                                <div class="row">
                                    <div class="col-sm-3" style="    padding-right: 0px;">
                                        <input type="text"  value="92" class="form-control" disabled="" autocomplete="off">
                                    </div>
                                    <div class="col-sm-9" style="    padding-left: 0px;">
                                        <input type="text" id="mobile_number" name="user[mobile_number]" value="<?= $UserArray[0]->mobile_number ?>" class="form-control" required="" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>






<div id="party_venue_map" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <input type="hidden" value="" id="isEditing">
            <div class="modal-header modal-alt-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Location</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
            <div class="modal-body">
                <div class="srch-by-txt">
                    <input type="hidden" id="lat" value="<?= $UserArray[0]->latitude ?>">
                    <input type="hidden" id="lng" value="<?= $UserArray[0]->longitude ?>">

                    <input type="text" id='store-location' value="<?= $UserArray[0]->area ?>"  autocomplete="off" class="form-control">
                </div>
                <div class="map-wrapper-inner" id="map-page">
                    <div id="google-maps-box">
                        <div id="map" style="width:100%; height:300px;"></div>
                    </div>
                </div>



            </div>
            <div class="modal-footer">

                <div class="button-wrap wd100">
                    <button type="button" class="float-right btn btn-success confirm_location" data-dismiss="modal"><?php echo $this->lang->line("CONFIRM"); ?></button>

                </div>

            </div>
        </div>
    </div>
</div>
<style>
    .pac-logo{
        z-index: 999999999 !important;
    }
</style>