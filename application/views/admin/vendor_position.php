<?php
$storeId=!empty($_REQUEST['storeId']) ? $_REQUEST['storeId'] : 'NA';
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">vendor listing</span>
                        </div>
                    </div>
                    <input type="hidden" id="vendor_id" value="<?= $Session->id ?>">
                    <div class="portlet-body">

                        <div class="row">
                            <?php
                            $option = '';
                            $qry = "SELECT users.*,restaurant_details.restaurant_name FROM `users` join restaurant_details ON restaurant_details.vendor_id=users.id WHERE is_approved != 0 AND user_type=2 AND users.store_type='$storeId' AND users.archive=0 order by users.position ASC";   //
                            $store_data = $this->Database->select_qry_array($qry);
                            for ($j = 0; $j < count($store_data); $j++) {
                                $d1 = $store_data[$j];

                                $option = $option . ' <li id="' . $d1->id . '" >' . $d1->restaurant_name . ' </li>';
                            }
                            ?>
                            <div class="col-sm-12">
                                <p><strong>Note: select and drag and drop to assign the order.</strong></p>
                            </div>
                            <div class="col-sm-12">
                                <ul id="sortable">
                                    <?= $option ?>
                                </ul>
                            </div>
                            <div class="col-sm-12">
                                <button onclick="ADMINCMN.vendororderviewupdate();" type="button" class="btn btn-primary">Submit</button>
                            </div>

                        </div>

                    </div>
                </div>


                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<style>
    #sortable li{
        margin-bottom: 8px;
    }
</style>