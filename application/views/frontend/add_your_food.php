<style>
    #loader {      
        display: none;      
        position: fixed;      
        z-index: 999;      
        height: 2em;      
        width: 2em;      
        overflow: show;      
        margin: auto;      
        top: 0;      
        left: 0;      
        bottom: 0;      
        right: 0;    
   }    
   #loader:before {      
        content: '';      
        display: block;      
        position: fixed;      
        top: 0;      
        left: 0;      
        width: 100%;      
        height: 100%;      
        background-color: rgba(0,0,0,0.3);    
   }
</style>
<section class="section __sccontacts _spg __addurfood">
		<div class="container">
		  	
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
					<h2><?php echo $this->lang->line("add_your_food");?></h2>
				 </div>
				
			 <div class="__form"> 
			    <div class="row">  
			    <div id="loader" style="display:none;"><img src="<?= base_url('images/loading.gif')?>"/></div>

				<form class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
					<div class="row">	 
					

						<div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("full_name");?></label><span class="required">*</span>
							<input type="text" class="form-control" aria-describedby="Full Name" id="name"> 
						</div>
						
						<div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("homeeats_name");?></label><span class="required">*</span>
							<input type="text" class="form-control" aria-describedby="Home Eats Name" id="restaurant_name"> 
						</div>
					 	
					 	<div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("area");?> </label><span class="required">*</span>
							<input type="hidden" value="" id="location_lat">
							<input type="hidden" value="" id="location_lng">
							<input type="hidden" value="" class="get_location">
						    <!--<input type="hidden" value="" id="locality">-->
						    <input type="hidden" value="" id="administrative_area_level_1">
						    <input type="hidden" value="" id="sublocality_level_1">
						    <input type="hidden" value="" id="approx_location">
							<input type="text" class="form-control area" aria-describedby="Area" id="venue"> 
						</div>
						
					 	<div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("street");?></label><span class="required">*</span>
							<input type="text" class="form-control" aria-describedby="Street" id="street"> 
						</div>
						<div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("city");?></label><span class="required" >*</span>
							
							<select class="form-control" id="locality">
                            <option value="">Select City</option>
							<?= GetCities()?>
							</select>
							<!--<input type="text" class="form-control city" aria-describedby="NaCityme" id="locality" value="" disabled> -->
						</div>
						<!--<div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">-->
						<!--	<label for="exampleInputEmail1"><?php echo $this->lang->line("city");?></label><span class="required" >*</span>-->
							<!--<select class="form-control" id="city_id">-->
       <!--                     <option value="">Select City</option>-->
							<!--<?= GetCities()?>-->
							<!--</select>-->
						<!--	<input type="text" class="form-control city" aria-describedby="NaCityme" id="locality" value="" disabled> -->
						<!--</div>-->
						<div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("phone_number");?></label><span class="required">*</span>
							<input type="text" class="form-control" aria-describedby="Phone Number" id="phone"> 
						</div>
					 	
						<div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("email_address");?></label><span class="required">*</span>
							<input type="email" class="form-control" aria-describedby="Name" id="email">
						</div>
						
						<div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("password");?></label><span class="required">*</span>
							<input type="password" class="form-control" aria-describedby="Name" id="password">
						</div>
						
						<div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("confirm_password");?></label><span class="required">*</span>
							<input type="password" class="form-control" aria-describedby="Name" id="cpassword">
						</div>
						<div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
						    	<?php $rand=rand();?>
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("business_logo");?></label><span class="required">*</span>
                            <input type="file" class="form-control MultipleImages __ppdt3" name="business_logo"  id="" refval="<?=$rand?>" src="">
                            <img src="" width="20%" id="ImagesEncode_<?=$rand?>" OldImage='' newimage='' style="display:none;" class="business_logo">

						</div>
				
						<div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
						    	<?php $rand=rand();?>
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("trade_license");?></label><span class="required">*</span>
                            <input type="file" class="form-control  MultipleImages __ppdt3" name="trade_license"  id="" refval="<?=$rand?>" src="">
                            <img src="" width="20%" id="ImagesEncode_<?=$rand?>" OldImage='' newimage='' style="display:none;" class="trade_license">
						</div>
						<div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("store_type");?></label><span class="required">*</span>
							 <select class="form-control" id="store_type" name="store_type" >
							     <option value="">Choose Type</option>
							    <?= get_storetype()?>
							</select>
						</div>
						<div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12 cuisine" style="display:none;">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("cuisine_type");?></label><span class="required">*</span>
							 <select class="form-control selectpicker" id="primary_cuisine_type" name="primary_cuisine_type[]" multiple data-live-search="true">
							    <?= get_cuisine()?>
							</select>
						</div>
					
					    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
							 <div class="custom-control custom-checkbox  ">
								<input type="checkbox" class="custom-control-input isDelivery" id="customControlAutosizing1" value="1">
								<label class="custom-control-label" for="customControlAutosizing1"><?php echo $this->lang->line("deliver_my_orders");?></label>
							  </div>
                        </div>
                        
                        
                        <div class="__wpourt delivery_option" style="display:none;">
                            
                            <div class="add-more-field wd100">
                                <div class="row">
                                    <div class="form-group col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line("delivery_from");?></label>
                                        <input type="text" class="form-control timepicker timepicker-no-seconds delivery_timepicker_from" id="delivery_timepicker_from" name="delivery_timepicker_from[]">
                                    </div>
                                    <div class="form-group col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line("delivery_to");?></label>
                                        <input type="text" class="form-control timepicker timepicker-no-seconds delivery_timepicker_to" id="delivery_timepicker_to" name="delivery_timepicker_to[]">
                                    </div>
                                    <div class="form-group col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line("delivery_location");?></label>
                                        <select class="form-control selectpicker emirates" id="emirates" name="emirates[]" multiple data-live-search="true" >
            							    <?= get_emirates()?>
            							</select>
                                    </div>
                                    
                                    <div class="form-group col-lg-1 col-md-12 col-sm-12 col-xs-12">
                                           <label class="wd100 __tabnone">&nbsp;</label>
                                        <button type="button" class="btn btn-success btn-block" onclick="add_more()"><i class="fa fa-plus-circle" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                    
                                </div>
                            </div>
                            
                            
                            <div class="add-more-copy wd100">
                   <!--             <div class="row add_copy">-->
                   <!--                 <div class="form-group col-lg-3 col-md-12 col-sm-12 col-xs-12">-->
                   <!--                     <label for="exampleInputEmail1">Delivery From</label>-->
                   <!--                     <input type="text" class="form-control timepicker timepicker-no-seconds delivery_timepicker_from" id="delivery_timepicker_from" name="delivery_timepicker_from[]">-->
                   <!--                 </div>-->
                   <!--                  <div class="form-group col-lg-3 col-md-12 col-sm-12 col-xs-12">-->
                   <!--                     <label for="exampleInputEmail1">Delivery To</label>-->
                   <!--                     <input type="text" class="form-control timepicker timepicker-no-seconds delivery_timepicker_to" id="delivery_timepicker_to" name="delivery_timepicker_to[]">-->
                   <!--                 </div>-->
                   <!--                  <div class="form-group col-lg-3 col-md-12 col-sm-12 col-xs-12">-->
                   <!--                     <label for="exampleInputEmail1">Emirates</label>-->
                   <!--                     <select class="form-control selectpicker emirates" id="emirates" name="emirates[]" multiple data-live-search="true">-->
            							<!--    <?= get_emirates()?>-->
            							<!--</select>-->
                   <!--                 </div>-->
                   <!--                  <div class="form-group col-lg-1 col-md-12 col-sm-12 col-xs-12">-->
                   <!--                        <label class="wd100 __tabnone">&nbsp;</label>-->
                   <!--                     <button type="button" class="btn btn-danger btn-block remove"><i class="fa fa-minus-circle" aria-hidden="true"></i>-->
                   <!--                     </button>-->
                   <!--                 </div>-->
                   <!--             </div>-->
                             
                            </div>
                        </div>
                        
                         <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
							 <div class="custom-control custom-checkbox  ">
								<input type="checkbox" class="custom-control-input isDelivery" id="customControlAutosizing2" value="0">
								<label class="custom-control-label" for="customControlAutosizing2"><?php echo $this->lang->line("homeeats_deliver_orders");?> (within 50 Km)</label>
							  </div>
                        </div>
						
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
						    <span class="register_message common_message"></span>
						</div>
						
						<div class="col"> </div>
    						<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 form-group">
    						    <button type="button" class="btn btn-primary btn-block FoodSubmit"><?php echo $this->lang->line("submit");?></button>
    						</div>
						</div>
					</form>
					
		            </div>
		           </div>
		    </div>
		    <div id="party_venue_map" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <input type="hidden" value="" id="isEditing">
                    <div class="modal-header modal-alt-header">
                        <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $this->lang->line("location");?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>
                    <div class="modal-body">
                        <div class="srch-by-txt">
                            <input type="hidden" id="lat" value="">
                            <input type="hidden" id="lng" value="">
                            <input type="text" id='store-location' value="" placeholder="<?php echo $this->lang->line("search_road_landmark");?>" autocomplete="off" class="form-control">
                        </div>
                        <div class="map-wrapper-inner" id="map-page">
                            <div id="google-maps-box">
                                <div id="map" style="width:100%; height:300px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="button-wrap">
                            <button type="button" class="btn btn-success confirm_location" data-dismiss="modal"><?php echo $this->lang->line("CONFIRM");?></button>
                        </div>
                    </div>
                </div>
            </div>
         </div>
	</section>