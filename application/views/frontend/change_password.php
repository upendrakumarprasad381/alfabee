<?php
$session_user= $this->session->userdata('UserLogin');
$userdetails=GetUserDetails($session_user->id);
?>
<style>
    #loader {      
        display: none;      
        position: fixed;      
        z-index: 999;      
        height: 2em;      
        width: 2em;      
        overflow: show;      
        margin: auto;      
        top: 0;      
        left: 0;      
        bottom: 0;      
        right: 0;    
   }    
   #loader:before {      
        content: '';      
        display: block;      
        position: fixed;      
        top: 0;      
        left: 0;      
        width: 100%;      
        height: 100%;      
        background-color: rgba(0,0,0,0.3);    
   }
</style>
<section class="section main_content_wrap __inner_nr dashboard">
<div class="container">
		<div class="row">
			<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
				<div class="wd100">
					<div class="wd100 _dsoutbox">
						<?php include('includes/dleftMenu.php'); ?>
					</div>
				</div>
			</div>
			<div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
				<div class="wd100">
				    <div id="loader" style="display:none;"><img src="<?= base_url('images/loading.gif')?>"/></div>
					<div class="wd100 _dsoutbox">
						<div class="row pl-2 pr-2 pb-3">
							<input type="hidden" id="userid" value="<?=!empty($userdetails)?$userdetails[0]->id:''?>">
						 	 <div class="col-lg-6 col-md-6 col-sm-12">
                             	<h4 class="pb-1"><?php echo $this->lang->line("change_password");?></h4> 
							   <div class="form-group">
                                    <label><?php echo $this->lang->line("your_old_password");?></label>
                                    <input type="password" class="form-control" value="" name="oldpassword" id="old_password" placeholder="" autocomplete="off"> 
                                </div>
								 
                                <div class="form-group">
                                    <label><?php echo $this->lang->line("your_new_password");?></label>
                                    <input type="password" class="form-control" value="" name="newpassword" id="password" placeholder="" autocomplete="off"> 
                                </div>
								 
                                <div class="form-group">
                                    <label><?php echo $this->lang->line("retype_new_password");?></label>
                                    <input type="password" class="form-control" value="" name="cpassword" id="cpassword" placeholder="" autocomplete="off"> 
                                </div>
								<span id="password_msg"></span>
								<div class="form-group">
									<button type="button" class="btn btn-primary float-right pl-5 pr-5 border-radius-0" id="change_password"><?php echo $this->lang->line("save");?></button>
                                </div>
							</div>		
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>