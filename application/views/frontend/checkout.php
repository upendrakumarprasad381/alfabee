<?php
$session_cart = $this->session->userdata('CartData');
if(empty($session_cart)){
    redirect(base_url());
}
$searchtype = $this->session->userdata('searchtype');
$lan = getsystemlanguage();

$searchtype = !empty($searchtype) ? $searchtype : '1';
$LoyalityPoints = GetSessionLoyalityPoints();
$session_arr = $this->session->userdata('UserLogin');
$RedirectUrl = base_url('payment');

$OnclickUrl = empty($session_arr) ? "LoginAndCallBack('$RedirectUrl')" : "$RedirectUrl";
$restaurant_arr = $this->session->userdata('Restaurant');
if (empty($restaurant_arr)) {
    redirect(base_url());
}
$Qry = "SELECT * FROM `restaurant_details` LEFT JOIN users ON users.id=restaurant_details.vendor_id WHERE vendor_id=" . $restaurant_arr["vendor_id"];
$rest_details = $this->Database->select_qry_array($Qry);

$free_delivery = !empty($rest_details[0]->free_delivery) ? $rest_details[0]->free_delivery : '0';


$rest_id = $restaurant_arr["vendor_id"];

$location = trim($session_cart[0]['location']);
$latitude = floor($session_cart[0]['latitude'] * 100) / 100;
$longitude = trim($session_cart[0]['longitude']);


$qry = "SELECT * FROM users where id='$rest_id'";
$rest_details1 = $this->Database->select_qry_array($qry);
// $saved_address = "SELECT * FROM `user_order_address` WHERE user_id='$session_arr->id'  AND isDefault=1 AND (address  LIKE '%$location%' OR street LIKE '%$location%' OR latitude LIKE '%$latitude%' OR longitude LIKE '%$longitude') ORDER BY id DESC LIMIT 1";
$saved_address = "SELECT * FROM `user_order_address` WHERE user_id='$session_arr->id'  AND isDefault=1 AND archive=0 ORDER BY id DESC LIMIT 1";
$address_details = $this->Database->select_qry_array($saved_address);
$locality = $this->session->userdata('locality');

$dlCharges = 0;
$distanceTotal = 0;
if (!empty($address_details)) {
    $distanceTotal = distance($rest_details[0]->latitude, $rest_details[0]->longitude, $address_details[0]->latitude, $address_details[0]->longitude, $unit = 'K');
    $dlCharges = getdeleverychagesfeecal35($rest_details, $address_details[0]->latitude, $address_details[0]->longitude);
}


$currentTime = date('H:i', time());
$Qry1 = "SELECT  delivery_timings.*, GROUP_CONCAT(city_name) AS city_name FROM  delivery_timings 
        INNER JOIN cities ON FIND_IN_SET(city_id, cities) > 0 WHERE delivery_timings.vendor_id = $rest_id GROUP BY cities,delivery_timings.id";
$Array = $this->Database->select_qry_array($Qry1);
foreach ($Array as $dat) {
    $start_time = date('H:i', strtotime($dat->delivery_from));
    $end_time = date('H:i', strtotime($dat->delivery_to));
    $currentTime = date('H:i', time());
    if ($start_time != '00:00:00' && $end_time != '00:00:00') {
        if ($currentTime > $start_time && $currentTime < $end_time) {
            $delivery = 1;
        } elseif ($currentTime < $start_time && $currentTime < $end_time) {
            $delivery = 1;
        } else {
            $delivery = 0;
        }
    }
    if ($delivery == 1) {
        $delivery_time = $this->lang->line("same_day");
    } else {
        $delivery_time = $this->lang->line("next_day");
    }
    // print_r($delivery);
}
?>
<input type="hidden" id="resturent_id" value="<?= $rest_id ?>">
<input type="hidden" id="order_type" value="<?= $searchtype ?>">
<input id="store_type" type="hidden" value="<?= $rest_details1[0]->store_type ?>">

<style>


    .secondary_button{
        border: 1px solid #00a53c;
        color: #00a53c;
    }

    #loader {      
        display: none;      
        position: fixed;      
        z-index: 999;      
        height: 2em;      
        width: 2em;      
        overflow: show;      
        margin: auto;      
        top: 0;      
        left: 0;      
        bottom: 0;      
        right: 0;    
    }    
    #loader:before {      
        content: '';      
        display: block;      
        position: fixed;      
        top: 0;      
        left: 0;      
        width: 100%;      
        height: 100%;      
        background-color: rgba(0,0,0,0.3);    
    }
    .toast{
        background-color:#fff;
    }

</style>

<div class="wd100 breadcrumb_wrap">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>"><?php echo $this->lang->line("home"); ?></a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('search_result') ?>"><?= $session_cart[0]['location'] ?></a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="<?= base_url('search_details/' . base64_encode($restaurant_arr['vendor_id'])) ?>"><?= $restaurant_arr['restaurant_name'] ?></a></li>
                <li class="breadcrumb-item" aria-current="page"><?php echo $this->lang->line("checkout"); ?></li>
            </ol>
        </nav>
    </div>
</div>
<section class="section __checkout">
    <div class="container">

        <div class="row">
            <div id="loader" style="display:none;"><img src="<?= base_url('images/loading.gif') ?>"/></div>
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="wd100 order_summary __bbeere">
                    <input type="hidden" id="store_lat" value="<?= $rest_details[0]->latitude ?>">
                    <input type="hidden" id="store_lng" value="<?= $rest_details[0]->longitude ?>">
                    <input type="hidden" id="party_or_not" value="<?= $session_cart[0]['party'] ?>">
                    <div class="wd100">
                        <h3><?php echo $this->lang->line("order_summary"); ?> <a class="__modifi _bmctn" href="<?= base_url('search_details/' . base64_encode($restaurant_arr['vendor_id'])) ?>"><?php echo $this->lang->line("modify_order"); ?></a></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row f-14">
                            <input type="hidden" value="<?= $rest_details[0]->id ?>" id="rest_id">
                            <div class="col-sm-8"><b class="">
                                    <?php
                                    if ($lan == 'ar' && !empty($rest_details[0]->restaurant_name_ar)) {
                                        echo $rest_details[0]->restaurant_name_ar;
                                    } else {
                                        echo $restaurant_arr['restaurant_name'];
                                    }
                                    ?>

                                </b></div>
                            <div class="col-sm-4 h5 text-right text-left-m"> </div>
                        </div>
                        <div class="table_order_items">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th align="left" valign="top" scope="col" class="_tdal"><strong><?php echo $this->lang->line("items"); ?></strong></th>
                                    <th align="center" valign="top" scope="col"><?php echo $this->lang->line("quantity"); ?> </th>
                                    <th align="center" valign="top" scope="col"><strong><?php echo $this->lang->line("price"); ?></strong></th>
                                    <th align="center" valign="top" scope="col"><strong><?php echo $this->lang->line("total"); ?></strong></th>
                                </tr>
                                <?php
                                $sub_total = 0;
                                $total = 0;
                                $groups = '';
                                $isPrecriptionReq = false;
                                foreach ($session_cart as $cart) {

                                    $sub_total+=$cart['subtotal'];
                                    $total+=$cart['subtotal'];

                                    $Qry2 = "SELECT *  FROM `menu_list` WHERE id=" . $cart['menu_id'];

                                    $Array2 = $this->Database->select_qry_array($Qry2);
                                    if (!empty($Array2[0]->prescriptionreq)) {
                                        $isPrecriptionReq = true;
                                    }
                                    if ($this->session->userdata('language') == 'ar') {
                                        if ($Array2[0]->menu_name_ar != '') {
                                            $menuname = $Array2[0]->menu_name_ar;
                                        } else {
                                            $menuname = $cart['menu_name'];
                                        }
                                    } else {
                                        $menuname = $cart['menu_name'];
                                    }
                                    ?>
                                    <input type="hidden" value="<?= $menuname ?>" id="location">
                                    <tr>
                                        <?php
                                        if ($this->session->userdata('language') == 'ar') {
                                            $align = 'right';
                                        } else {
                                            $align = 'left';
                                        }
                                        ?>
                                        <td align="<?php echo $align ?>" valign="top">
                                            <p><strong><?= $menuname ?></strong></p>
                                            <?php
                                            if (isset($cart['size'])) {
                                                $array = array_column($cart['size'], 'name');
                                                $array = implode(',', $array);

                                                echo $array;
                                            }
                                            if (isset($cart['addon'])) {
                                                echo ',';
                                                $array1 = array_column($cart['addon'], 'name');
                                                $array1 = implode(',', $array1);
                                                echo $array1;
                                            }
                                            if (isset($cart['topping'])) {
                                                echo ',';
                                                $array2 = array_column($cart['topping'], 'name');
                                                $array2 = implode(',', $array2);
                                                echo $array2;
                                            }
                                            if (isset($cart['drink'])) {
                                                echo ',';
                                                $array3 = array_column($cart['drink'], 'name');
                                                $array3 = implode(',', $array3);
                                                echo $array3;
                                            }
                                            if (isset($cart['dip'])) {
                                                echo ',';
                                                $array4 = array_column($cart['dip'], 'name');
                                                $array4 = implode(',', $array4);
                                                echo $array4;
                                            }
                                            if (isset($cart['side'])) {
                                                $array5 = array_column($cart['side'], 'name');
                                                $array5 = implode(',', $array5);
                                                echo $array5;
                                            }
                                            ?>
                                            <p><?php ?></p>
                                        </td>
                                        <td align="center" valign="top"><?= $cart['quantity'] ?></td>
                                        <td align="center" valign="top"><?php echo $this->lang->line("aed"); ?> <?= DecimalAmount($cart['price']) ?></td>
                                        <td align="center" valign="top"><?php echo $this->lang->line("aed"); ?> <?= DecimalAmount($cart['subtotal']) ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
                <input id="prescriptionreq" value="<?= !empty($isPrecriptionReq) ? '1' : '0'; ?>" type="hidden"> 
                <?php
                if ($session_cart[0]['party'] != 1) {
//                    echo '<pre>';
//                    print_r($rest_details);
//                    exit;
                    ?>
                    <div class="wd100 order_summary __bbeere __popaccd__accordion_cm">
                        <div class="wd100 panel-body" style="padding:0px;">
                            <h3><?php echo $this->lang->line("HOW_WOULD_YOU_LIKE_TO"); ?></h3>
                            <ul class="iwnt-toggleButton seconds">
                                <li>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="deliveryRadio1" onchange="COMN.deliveryRadio1Chnage();" name="deliveryRadio" class="custom-control-input selected_item" <?= $searchtype == '1' ? 'checked' : '' ?> value="1"  >
                                        <label class="custom-control-label" for="deliveryRadio1"><?php echo $this->lang->line("delivery"); ?></label>
                                    </div>
                                </li> 
                                <li dd="<?= $rest_details[0]->self_pickup ?>" style="display: <?= !empty($rest_details[0]->self_pickup) ? 'block' : 'none'; ?>">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="deliveryRadio2" onchange="COMN.deliveryRadio1Chnage();" name="deliveryRadio" class="custom-control-input selected_item" <?= $searchtype == '2' ? 'checked' : '' ?>  value="2">
                                        <label class="custom-control-label" for="deliveryRadio2"><?php echo $this->lang->line("self_pickup"); ?></label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="wd100 order_summary __bbeere" id="delivery_address" style="display: <?= $searchtype == '1' ? 'block' : 'none' ?>">
                        <div class="wd100">
                            <h3><?php echo $this->lang->line("delivery_address"); ?>
                                <?php
                                if (!empty($address_details)) {
                                    ?>
                                    <a class="__modifi _btnadd" href="#" data-toggle="modal" data-target="#map_panel" onClick = "showAddressPopup(false)">&nbsp;&nbsp;<?php echo $this->lang->line("ADD"); ?></a>
                                    <a class="__modifi _btnedit" href="#" data-toggle="modal" data-target="#map_panel" onClick = "showAddressPopup(true, this)" data-address="<?= $address_details[0]->id ?>"><?php echo $this->lang->line("EDIT"); ?></a>
                                    <?php
                                } else {
                                    ?>
                                    <a class="__modifi" href="#" style="font-size: 13px;text-decoration: revert;margin-left: 10px;" data-toggle="modal"  id="add_address">Add new address</a>
                                    <!--<a class="__modifi" href="#" data-toggle="modal" data-target="#exampleModalCenter" id="addr_address">ADD</a>-->
                                    <?php
                                }
                                ?>
                            </h3>


                            <div class="__addadresspop modal fade bd-example-modal-lg" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title address_title" id="exampleModalLongTitle"><?php echo $this->lang->line("add_new_address"); ?></h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <div class="modal-body">  

                                            <div class="row">
                                                <input type="hidden" value="" id="content">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                                    <!-- <div class="__map wd100">-->
                                                    <!--  <div class="  form-group">-->
                                                    <!--<label class="wd100" for="exampleFormControlSelect1">Search Road or Landmark</label>-->
                                                    <!-- <input type="email" class="form-control" placeholder="Search...." id="store-map-location">-->
                                                     <!--<input type="text" class="form-control area" aria-describedby="Area" id="pac-input">-->
                                                    <!--  </div>-->
                                                    <!--  <div id="map"></div>-->
                                                           <!--<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d10205.12794366965!2d55.339414167405394!3d25.268648208207992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sae!4v1582186793959!5m2!1sen!2sae" width="100%" height="150" frameborder="0" style="border:0;" allowfullscreen=""></iframe>-->
                                                    <!-- 	</div>-->
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <h5><?php echo $this->lang->line("contact_details"); ?></h5>
                                                </div>

                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

                                                    <div class="form-group row __nubrline">
                                                        <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("mobile_number"); ?></label>
                                                        <input type="text" class="form-control col _nodropcode" name="mobile_code" value="+92" disabled="true" id="mobile_code">
                                                        <input type="text" class="col-8 form-control _nodropfild" placeholder="" id="mobile_number" value="" maxlength="12">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("land_no_optional"); ?></label>
                                                    <input type="text" class="form-control" placeholder="" id="landphone_no" value="">
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <h5><?php echo $this->lang->line("address_details"); ?></h5>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <input type="hidden" value="" id="location-lat">
                                                    <input type="hidden" value="" id="location-lng">
                                                    <label class="wd100" for="exampleInputEmail1"><?php echo $this->lang->line("location"); ?></label>
                                                    <input type="text" class="form-control" aria-describedby="Area" id="location_area" value="" readonly>
                                                    <!--<input type="text" class="form-control location" aria-describedby="Area" id="location" value="" disabled="true">-->
                                                </div>

                                                <!--<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group">-->
                                                <!--<label class="wd100" for="exampleFormControlSelect1">City</label>-->
                                                <!--<input type="text" class="form-control city" aria-describedby="NaCityme" id="locality" value="" disabled="true"> -->
                                                <!--</div>-->

                                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("type"); ?></label>
                                                    <select class="form-control col _nodropcode" id="building_type">

                                                        <option value="1"><?php echo $this->lang->line("apartment"); ?></option>
                                                        <option value="2"><?php echo $this->lang->line("house"); ?></option>
                                                        <option value="3"><?php echo $this->lang->line("office"); ?></option> 
                                                    </select>
                                                </div>

                                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("street"); ?></label>
                                                    <input type="text" class="form-control" placeholder="" id="street" value="">
                                                </div>

                                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group" id="building">
                                                    <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("building"); ?></label>
                                                    <input type="text" class="form-control" placeholder="" id="building_name" value="">
                                                </div>
                                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group" id="apartment">
                                                    <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("apartment_no"); ?></label>
                                                    <input type="text" class="form-control" placeholder="" id="apartment_no" value="">
                                                </div>
                                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group" style="display:none;" id="office">
                                                    <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("office"); ?></label>
                                                    <input type="text" class="form-control" placeholder="" id="office_name">
                                                </div>
                                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group" style="display:none;" id="house">
                                                    <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("house"); ?></label>
                                                    <input type="text" class="form-control" placeholder="" id="house_name">
                                                </div>

                                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group" id="floor">
                                                    <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("floor"); ?></label>
                                                    <input type="email" class="form-control" placeholder="" id="floor_no" value="">
                                                </div>

                                                <div class="col-lg-8 col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("additional_directions"); ?></label>
                                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="1" id="additional_direction" value=""></textarea>
                                                </div>

                                            </div>

                                            <span class="common_message"></span>
                                        </div>

                                        <div class="modal-footer"> 
                                            <button type="button" class="btn secondary_button" id="show_map"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $this->lang->line("view_map"); ?></button>
                                            <button type="button" class="btn btn-primary" id="save_address"><?php echo $this->lang->line("save_address"); ?></button>
                                        </div>

                                    </div>
                                </div>


                            </div>



                            <?php
                            if (!empty($address_details[0])) {
                                ?>
                                <input type="hidden" id="address_id" value="<?= $address_details[0]->id ?>">	
                                <p class="m-b-0"> <b><?php echo $this->lang->line("address_name"); ?>: </b>
                                    <?php
                                    if ($address_details[0]->building != '') {
                                        $address = $address_details[0]->building;
                                    }if ($address_details[0]->house != '') {
                                        $address = $address_details[0]->house;
                                    }if ($address_details[0]->office != '') {
                                        $address = $address_details[0]->office;
                                    }
                                    echo $address;
                                    ?> 
                                    <a href="#" class="__sbsss" data-toggle="modal" data-target="#selectAddressModal" onClick="showSelectAddressPopup()" style="color:green;text-transform:uppercase;font-size:14px;cursor:pointer"><?php echo $this->lang->line("change_delivery_address"); ?></h5></a>
                                </p>
                                <p class="m-b-0"> <b><?php echo $this->lang->line("address"); ?>: </b> <?= $address_details[0]->address ?>, </p>
                                <p class="m-b-0"> 
                                    <?php
                                    if ($address_details[0]->additional_direction != 'undefined') {
                                        ?>
                                        <b><?php echo $this->lang->line("additional_directions"); ?>: </b><?= $address_details[0]->additional_direction ?>
                                        <?php
                                    }
                                    ?>
                                </p>
                                <p><b><?php echo $this->lang->line("mobile_number"); ?>: </b> <span class="ng_dcasdca"><?= $address_details[0]->mobile_code ?> <?= $address_details[0]->mobile_number ?></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                    <?php
                                    if ($address_details[0]->landphone_no != '' && $address_details[0]->landphone_no != '0') {
                                        ?>
                                        <b><?php echo $this->lang->line("land_no"); ?>: </b> <span class="ng_dcasdca"><?= $address_details[0]->landphone_no ?></span> 
                                        <?php
                                    }
                                    ?>
                                </p>
                                <?php
                                $delivery_radius = !empty($rest_details1[0]->delivery_radius) ? $rest_details1[0]->delivery_radius : '0';
                                if ($searchtype == '1') {
                                    if (!empty($delivery_radius) && $distanceTotal > $delivery_radius) {
                                        ?>
                                        <style>
                                            .place_order{
                                                display: none;
                                            }
                                        </style>
                                        <p style="color: red;font-weight: 600;">Sorry we will deliver only inside <?= $delivery_radius ?> kilometre. </p>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                } else {

//                    echo '<pre>';
//                    print_r($session_cart);exit;
                    ?>
                    <div class="wd100 order_summary __bbeere">
                        <div class="wd100">
                            <h3>Party Order Details</h3>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <label> No.of People</label>
                                        <input type="text" class="form-control" id="no_of_people" placeholder="No.of People" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <input type="hidden" value="0" id="schedule_delivery">
                                    <div class="form-group">
                                        <label>Choose Date</label>
                                        <input type="text" class="form-control check_date" id="party_date" placeholder="Date" value="<?= date('Y-m-d'); ?>" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">  
                                    <div class="form-group">
                                        <label>Choose Time</label>
                                        <?php
                                        $start = "07:00";
                                        $end = "23:00";

                                        $tStart = strtotime($start);
                                        $tEnd = strtotime($end);
                                        $tNow = $tStart;
                                        echo '<select class="form-control check_time" name="party_time" id="party_time">';
                                        echo '<option value="">Select Time</option>';
                                        while ($tNow <= $tEnd) {
                                            echo '<option value="' . date("H:i", $tNow) . '">' . date("h:i A", $tNow) . '</option>';
                                            $tNow = strtotime('+30 minutes', $tNow);
                                        }
                                        echo '</select>';
                                        ?>
                                    </div>
                                </div> 

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">  
                                    <div class="form-group">
                                        <label>End Time</label>
                                        <?php
                                        $start = "07:00";
                                        $end = "23:00";

                                        $tStart = strtotime($start);
                                        $tEnd = strtotime($end);
                                        $tNow = $tStart;
                                        echo '<select class="form-control check_time" name="party_time_end" id="party_time_end">';
                                        echo '<option value="">Select Time</option>';
                                        while ($tNow <= $tEnd) {
                                            echo '<option value="' . date("H:i", $tNow) . '">' . date("h:i A", $tNow) . '</option>';
                                            $tNow = strtotime('+30 minutes', $tNow);
                                        }
                                        echo '</select>';
                                        ?>
                                    </div>
                                </div> 
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="party_error"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <div class="wd100 order_summary __bbeere __ssrequests">
                    <div class="delivery_time">

                        <form  class="form-horizontal form-bordered" style="display: none;">
                            <input type="file" class="form-control" oldfile="" onchange="COMN.uploadquotationBillRequest();" id="UploadExcel">
                        </form>
                        <?php if ($rest_details1[0]->store_type == '3' && !empty($isPrecriptionReq)) { ?>
                            <div class="form-group mb-0">
                                <label onclick="COMN.uploadquotationBillRequestIcon();" for="exampleFormControlTextarea1" style="cursor: pointer;"><button style="background: #ffc900;border-color: #ffc900;color: black;" type="button" class="btn btn-success btn-block "><strong ><?php echo $this->lang->line("upload_prescription"); ?></strong></button></label>
                            </div>
                        <?php } ?>


                        <div class="form-group mb-0">
                            <label for="exampleFormControlTextarea1"><?php echo $this->lang->line("add_special_request"); ?></label>
                            <textarea class="form-control special_request" id="exampleFormControlTextarea1" rows="3" placeholder="<?php echo $this->lang->line("food_allergy"); ?>"></textarea> <small><?php echo $this->lang->line("chargeable_items"); ?></small>
                        </div>


                    </div>

                </div>
                <?php
                if ($rest_details1[0]->store_type != 1) {
                    ?>

                    <div class="wd100 order_summary __bbeere">
                        <div class="wd100" id="schedule">
                            <h3><?php echo $this->lang->line("SCHEDULE_DELIVERY"); ?>								</h3>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <input type="hidden" value="1" id="schedule_delivery">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line("date"); ?></label>
                                        <input type="text" class="form-control check_date" id="schedule_date" style="background-color: #ffffff;" readonly placeholder="Date" autocomplete="off" value="<?= date('Y-m-d'); ?>">
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">  
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line("time"); ?></label>
                                        <select class="form-control" fffff name="schedule_time" id="schedule_time">

                                        </select>
                                                                                                                  <!--<select class="form-control" id="schedule_time">-->

                                    </div>
                                </div> 
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="party_error"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

            </div>


            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

                <div class="wd100 order_summary __bbeere __ssrequests">

                    <div class="wd100">
                        <h3><?php echo $this->lang->line("payment_summary"); ?></h3>
                    </div>

                    <div class="row">
                        <?php
                        if (!isset($session_cart[0]['promocode_id'])) {
                            if (empty($session_cart[0]['promocode_id'])) {
                                ?>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="promocode">


                                    <form class="card p-2" style=" border-radius: 0;">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="<?php echo $this->lang->line("promo_code"); ?>" id="promo_code" autocomplete="off">
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-success" id="apply_couponcode" ><?php echo $this->lang->line("redeem"); ?></button>
                                            </div>
                                        </div>
                                    </form>
                                    <p class="message"></p>

                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="apply_promocode">
                                <?php
                                $sql = "SELECT * FROM promo_code WHERE promocode_id=" . $session_cart[0]['promocode_id'];
                                $array_data = $this->Database->select_qry_array($sql);
                                ?>
                                <input type="hidden" value="<?= $session_cart[0]['promocode_id'] ?>" id="promocode_id">
                                <div class="toast __promosms fade show" role="alert" aria-live="assertive" aria-atomic="true">

                                    <div class="toast-body">
                                        <button type="submit" class="btn btn-outline-danger btn-sm float-right" style="border-radius: 0;" id="remove_promocode"><?php echo $this->lang->line("remove"); ?></button>
                                        <p class="text-success"><b><?= $array_data[0]->promo_code ?> - Promo applied Successfully</b></p>

                                    </div>
                                </div>


                            </div>
                            <?php
                        }
                        ?>

                        <div class="row" style="width: 100%;margin-top: 15px;margin-left: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                <div class="form-group form-check" style="margin-bottom: 0;">
                                    <?php
                                    $creditAmt = getloyalityTotalCreditPoint($session_arr->id);
                                    ?>
                                    <input type="checkbox" onclick="document.getElementById('loyalitydiv').style.display == 'flex' ? document.getElementById('loyalitydiv').style.display = 'none' : document.getElementById('loyalitydiv').style.display = 'flex'" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1"><?php echo $this->lang->line("loyality_points"); ?>  <span style="color: green;    font-weight: 700;"><?php echo $this->lang->line("credit"); ?> : <?= DecimalAmount($creditAmt) ?> <?php echo $this->lang->line("point"); ?></span></label>
                                </div>

                                <div class="input-group" id="loyalitydiv" style="display: none;">
                                    <input type="number" class="form-control" placeholder="<?php echo $this->lang->line("enter_points"); ?>" id="loyalityAmt" autocomplete="off">
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-success" onclick="COMN.validationLoyalityPoints();">Redeem</button>
                                    </div>
                                </div>
                                <?php
                                if (!empty($LoyalityPoints)) {
                                    ?>
                                    <div class="toast __promosms fade show" role="alert" aria-live="assertive" aria-atomic="true">

                                        <div class="toast-body">
                                            <button type="submit" class="btn btn-outline-danger btn-sm float-right" onclick="COMN.removeLoyalityPoints();" style="border-radius: 0;" id=""><?php echo $this->lang->line("remove"); ?></button>
                                            <p class="text-success"><b>Loyality Points amount applied Successfully</b></p>
                                            <small><?= DecimalAmount($LoyalityPoints) ?> PKR</small>
                                        </div>
                                    </div>

                                <?php }
                                ?>


                            </div>
                        </div>




                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>



                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <article class="card __pyawrapbx" style="border-radius: 0;">
                                <div class="card-body ">

                                    <ul class="nav bg-light nav-pills rounded nav-fill mb-0" role="tablist">
                                        <li class="nav-item" style="display:none;">
                                            <a class="nav-link" data-toggle="pill" href="#nav-tab-card">
                                                <i class="fa fa-credit-card"></i> <?php echo $this->lang->line("credit_card"); ?></a></li>
                                        <li class="nav-item" style="display:none;">
                                            <a class="nav-link" data-toggle="pill" href="#nav-tab-paypal">
                                                <i class="fab fa-cc-visa"></i> <?php echo $this->lang->line("visa_checkout"); ?></a></li>
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="pill" href="#nav-tab-bank">
                                                <i class="far fa-money-bill-alt"></i>  <?php echo $this->lang->line("cash"); ?></a>
                                                
                                            
                                        </li>
                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane fade show" id="nav-tab-card">

                                            <form role="form">
                                                <div class="form-group">
                                                    <label for="username"><?php echo $this->lang->line("full_name"); ?><?php echo $this->lang->line("on_card"); ?></label>
                                                    <input type="text" class="form-control" name="username" placeholder="" required="">
                                                </div> <!-- form-group.// -->

                                                <div class="form-group">
                                                    <label for="cardNumber"><?php echo $this->lang->line("card_no"); ?></label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="cardNumber" placeholder="">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text text-muted">
                                                                <i class="fab fa-cc-visa"></i>   <i class="fab fa-cc-amex"></i>  
                                                                <i class="fab fa-cc-mastercard"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div> <!-- form-group.// -->

                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <div class="form-group">
                                                            <label><span class="hidden-xs"><?php echo $this->lang->line("expiration"); ?></span> </label>
                                                            <div class="input-group">
                                                                <input type="number" class="form-control" placeholder="MM" name="">
                                                                <input type="number" class="form-control" placeholder="YY" name="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label data-toggle="tooltip" title="" data-original-title="3 digits code on back side of the card"><?php echo $this->lang->line("cvv"); ?> <i class="fa fa-question-circle"></i></label>
                                                            <input type="number" class="form-control" required="">
                                                        </div> <!-- form-group.// -->
                                                    </div>
                                                </div> <!-- row.// -->
                                                <button class="subscribe btn btn-primary btn-block" type="button"  style="border-radius: 0;"> <?php echo $this->lang->line("confirm"); ?> </button>
                                            </form>
                                        </div> <!-- tab-pane.// -->
                                        <div class="tab-pane fade" id="nav-tab-paypal">
                                            <p>

                                                You can now pay safely online with fewer clicks by using Visa Checkout. <br/>
                                                Please click the Visa Checkout button below to proceed with your payment.</p>

                                        </div>
                                        <div class="tab-pane fade" id="nav-tab-bank">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        </div> <!-- tab-pane.// -->
                                    </div> <!-- tab-content .// -->

                                </div> <!-- card-body.// -->
                            </article> <!-- card.// -->
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="wd100 __cartsummytabv_btpart  checu_paypg">
                                <?php
                                $sub_tot = DecimalAmount($sub_total);
                                if ($session_cart[0]['party'] != 1) {

                                    $serv_charge = $dlCharges;
                                } else {
                                    $serv_charge = 0;
                                }
//$serv_charge = 0;    
                                if ($searchtype == '2') {
                                    $serv_charge = '0';
                                }
                                if ($searchtype == '1' && $free_delivery > 0) {
                                    if ($sub_total > $free_delivery) {
                                        $serv_charge = '0';
                                    }
                                }
//                                echo '<pre>';
//                                print_r($session_cart);
//                                exit;
                                if (isset($session_cart[0]['promocode_id']) && isset($session_cart[0]['discount_amt'])) {

                                    if (empty($session_cart[0]['discount_from'])) {
                                        $tempFordis = $sub_tot;
                                    } else {
                                        $tempFordis = $serv_charge;
                                    }

                                    if (isset($session_cart[0]['discount_type']) && $session_cart[0]['discount_type'] == '1') {
                                        $discount = $session_cart[0]['discount_amt'];
                                    } else {
                                        $discount = $tempFordis * $session_cart[0]['discount_amt'];
                                    }
                                    if ($session_cart[0]['maximum_discount'] > 0) {
                                        if ($discount > $session_cart[0]['maximum_discount']) {
                                            $discount = $session_cart[0]['maximum_discount'];
                                        }
                                    }
                                } else {
                                    $discount = 0;
                                }

                                $total_amount = $sub_tot + $serv_charge - $discount;
                                ?>
                                <input type="hidden" id="total_amouttt" value="<?= $total_amount ?>">
                                <input type="hidden" id="service_charge_amouttt" value="<?= $serv_charge ?>">
                                <?php
                                if (!empty($LoyalityPoints)) {
                                    $total_amount = $total_amount - $LoyalityPoints;
                                }
                                ?>

                                <table class="table">
                                    <tbody>

                                        <tr>
                                            <td align="left"><?php echo $this->lang->line("sub_total"); ?></td>
                                            <td align="right" class="__srylst"><?php echo $this->lang->line("aed"); ?> <?= DecimalAmount($sub_total) ?></td>
                                        </tr>
                                        <?php
                                        if ($session_cart[0]['party'] != 1) {
                                            ?>

                                            <tr>
                                                <td align="left"><?php echo $this->lang->line("delivery_fee"); ?><br>
                                                    <?php if ($searchtype == '1') { ?>
                                                    <span style="font-size: 10px;display: <?= empty($distanceTotal) ? 'none' : 'block' ?>">Distance: <?= number_format($distanceTotal,1) ?></span>
                                                    <?php } ?>
                                                </td>
                                                <td align="right" class="__srylst"><?php echo $this->lang->line("aed"); ?> <?= DecimalAmount($serv_charge) ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        <tr>

                                            <td align="left"><?php echo $this->lang->line("coupon_discount"); ?></td>
                                            <td align="right" class="__srylst"><?php echo $this->lang->line("aed"); ?> <?= DecimalAmount($discount) ?></td>
                                        </tr>
                                        <?php
                                        if (!empty($LoyalityPoints)) {
                                            ?>
                                            <tr>
                                                <td align="left">Loyality Points amount</td>
                                                <td align="right"  class="__crt_totalleft"><?php echo $this->lang->line("aed"); ?> <?= DecimalAmount($LoyalityPoints) ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        <tr>
                                            <td align="left"><b><?php echo $this->lang->line("total_amount"); ?></b> </td>
                                            <td align="right"  class="__crt_totalleft"><b><?php echo $this->lang->line("aed"); ?> <?= DecimalAmount($total_amount) ?></b></td>
                                        </tr>

                                    </tbody>
                                </table>


                                <div class="form-group">
                                    <?php
                                    $closed = 0;



                                    if ($rest_details[0]->store_type == '1') {
                                        $opening_time = date('H:i', strtotime($rest_details[0]->opening_time));
                                        $closing_time = date('H:i', strtotime($rest_details[0]->closing_time));
                                        $currentTime = date('H:i', time());
                                        if ($searchtype == '1') {
                                            $opening_time = date('H:i', strtotime($rest_details[0]->delivery_hours_st));
                                            $closing_time = date('H:i', strtotime($rest_details[0]->delivery_hours_et));
                                        }
                                        if ($currentTime < $opening_time) {
                                            $closed = 1;
                                        } else {
                                            if ($currentTime > $closing_time) {
                                                $closed = 1;
                                            }
                                        }
                                    }

                                    if ($rest_details[0]->busy_status == 1) {
                                        $busy = 1;
                                    } else {
                                        $busy = 0;
                                    }
                                    ?>
                                    <input type="hidden" id="closed_status" value="<?= $closed ?>">
                                    <input type="hidden" id="busy_status" value="<?= $busy ?>">
                                    <button type="button" class="btn btn-success btn-block place_order"  url="<?= $OnclickUrl ?>" onClick=""><?php echo $this->lang->line("place_order"); ?></button>
                                </div>

                            </div>

                        </div>


                    </div>
                </div>

            </div>

            <input type="hidden" value="<?= $rest_details1[0]->store_type ?>" id="store_category">	

            <div id="map_panel" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <input type="hidden" value="" id="isEditing">
                        <div class="modal-header modal-alt-header">
                            <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $this->lang->line("add_new_address"); ?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                        </div>
                        <div class="modal-body">
                            <div class="srch-by-txt">
                                <!--<input type="hidden" value="<?= $rest_details[0]->delivery_location ?>" id="delivery_location">-->
                                <input type="hidden" value="" id="location_lat">
                                <input type="hidden" value="" id="location_lng">
                                <input type="hidden" value="" class="get_location">
                                <input type="hidden" value="" id="locality">
                                <input type="hidden" value="<?= $locality ?>" id="emirate">
                                <input type="hidden" value="" id="administrative_area_level_1">
                                <input type="hidden" value="" id="sublocality_level_1">
                                <input type="hidden" value="<?= $rest_details[0]->delivery_radius ?>" id="delivery_radius">
                                <input type="text" id='store-location' value="" placeholder="<?php echo $this->lang->line("search_road_landmark"); ?>" autocomplete="off" class="form-control">
                            </div>
                            <div class="map-wrapper-inner" id="map-page">
                                <div id="google-maps-box">
                                    <div id="map" style="width:100%; height:300px;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="button-wrap">
                                <!--<button type="button" class="btn secondary_button" id="view_address"><i class="fa fa-address-card" aria-hidden="true"></i> View Address Fields</button>-->
                                <button type="button" class="btn btn-success confirm_location" data-dismiss="modal"><?php echo $this->lang->line("CONFIRM"); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="busyModalAlert" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenter" aria-modal="true">
                <div class="modal-dialog modal-dialog-centered " role="document">
                    <div class="modal-content __ctboxpopup">
                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                        </div>
                        <div class="modal-body">
                            <?php
                            if ($rest_details[0]->busy_status == 1 && $closed == 0) {
                                $status = $this->lang->line("busy");
                            } else {
                                $status = $this->lang->line("closed");
                            }
                            ?>
                            <br>
                            <p><?= $rest_details[0]->restaurant_name ?> <?php echo $this->lang->line("is_currently"); ?> <?= $status ?>  <?php echo $this->lang->line("not_accepting_order"); ?> </p>
                            <p> <?php echo $this->lang->line("view_more_restaurant"); ?></p>
                            <br>


                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success" onclick="ok_alert(this)"><?php echo $this->lang->line("ok"); ?></button>
                            <button class="btn btn-default" id="busyModalAlert" onclick="cancel_alert(this)"><?php echo $this->lang->line("cancel"); ?></button>

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="selectAddressModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenter" aria-modal="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $this->lang->line("delivery_address"); ?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                        </div>
                        <div class="modal-body">
                            <div class="dropdown">
                                <?php
                                $address_data = "SELECT * FROM `user_order_address` WHERE user_id='$session_arr->id' AND archive=0  ORDER BY id DESC";
// $address_data = "SELECT * FROM `user_order_address` WHERE user_id='$session_arr->id'  AND isDefault=1 AND (address  LIKE '%$location%' OR street LIKE '%$location%' OR latitude LIKE '%$latitude%' OR longitude LIKE '%$longitude') ORDER BY id DESC LIMIT 1";
// $address_data = "SELECT * FROM `user_order_address` WHERE user_id='$session_arr->id' AND address LIKE CONCAT('%', '".$session_cart[0]['location']."' ,'%') OR street LIKE CONCAT('%', '".$session_cart[0]['location']."' ,'%') ";
                                $address_det = $this->Database->select_qry_array($address_data);
                                ?>
                                <select class="form-control" onChange="addressSelected()" id="selected_address">
                                    <option value="" disabled selected><?php echo $this->lang->line("select_your_address"); ?></option>
                                    <?php
                                    foreach ($address_det as $d) {
                                        if ($d->building != '') {
                                            $address = $d->building;
                                        }if ($d->house != '') {
                                            $address = $d->house;
                                        }if ($d->office != '') {
                                            $address = $d->office;
                                        }
                                        $addrs = explode('-', $d->address);
                                        $addr = substr($addrs[0], 0, 45);
                                        ?>
                                        <option value="<?= $d->id ?>"><?= $address ?> (<?= $addr ?>)</option>
                                            <!--<option value="<?= $d->id ?>"><?= $session_cart[0]['location'] ?> (<?= $address ?>)</option>-->
                                        <?php
                                    }
                                    ?>
                                </select>

                            </div>
                            <br />
                            <br />
                            <br />
                            <br />
                        </div>
                        <div class="modal-footer">

                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

        </div>
    </div>
</section>

