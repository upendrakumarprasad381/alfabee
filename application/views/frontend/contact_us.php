<section class="section __sccontacts _spg __addurfood">
		<div class="container">
		  	
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
					<h2><?php echo $this->lang->line("contact_us")?></h2>
				 </div>
				
				 <div class="__form">
					 
				 <div class="row">
					 
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							
							<p><?php echo $this->lang->line("fill_the_form")?></p>
						    
					
						<hr/>
  					</div>
				 
					 
				<form class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					
					<div class="row">	 
						<div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("full_name")?></label><span class="required">*</span>
							<input type="text" class="form-control" id="contact_name" placeholder="<?php echo $this->lang->line("full_name")?>"> 
						</div>
						
						<div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("email")?></label><span class="required">*</span>
							<input type="text" class="form-control" id="contact_email" placeholder="<?php echo $this->lang->line("email")?>"> 
						</div>
					 	
						
					 	<div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("mobile_number")?></label><span class="required">*</span>
							<input type="text" class="form-control area" id="contact_mobile" placeholder="<?php echo $this->lang->line("mobile_number")?>"> 
						</div>
                        
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("message")?></label> <span class="required">*</span>
							<textarea class="form-control" id="contact_message" rows="2" placeholder="<?php echo $this->lang->line("message")?>"></textarea>
						</div> 
						
						<div class="col"> </div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
						    <span class="register_message common_message"></span>
						</div>
						<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 form-group">
						    <button type="button" class="btn btn-primary btn-block ContactUs"><?php echo $this->lang->line("submit")?></button>
						</div>

						</div>
					</form>
					
		            </div>
		           </div>
		    </div>
	</section>