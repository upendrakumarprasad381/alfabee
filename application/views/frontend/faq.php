<section class="section __sttcpg">
		<div class="container">
  
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
		<h2>FAQ </h2>
				
		
 <div id="accordion" class="myaccordion">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="d-flex align-items-center justify-content-between btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Is placing an order through AlfaBee safe?
          <span class="fa-stack fa-sm">
            <i class="fas fa-circle fa-stack-2x"></i>
            <i class="fas fa-minus fa-stack-1x fa-inverse"></i>
          </span>
        </button>
      </h2>
    </div>
    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
       <p>We are really concerned about the safe ordering experience of our precious customers, riders and our associated sellers. We think it of utmost importance that our customers experience a smooth journey of ordering and using our platform in complete privacy.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        Does AlfaBee provide 24 hour delivery?
          <span class="fa-stack fa-2x">
            <i class="fas fa-circle fa-stack-2x"></i>
            <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
          </span>
        </button>
      </h2>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
        <p>Depending on the distinct timings of restaurants and stores, AlfaBee is accessible 24 hours. Our application will keep you up to date about the opening and closing time of different restaurants and stores. Grocery delivery takes place between 10am to 8pm.</p>
      </div>
    </div>
  </div>
  
  
  
  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Can I book order in advance for next day?
          <span class="fa-stack fa-2x">
            <i class="fas fa-circle fa-stack-2x"></i>
            <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
          </span>
        </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <p>Yes, you can choose the date and time of delivery for your orders.</p>
      </div>
    </div>
  </div>
  
  
  
    
  <div class="card">
    <div class="card-header" id="heading4">
      <h2 class="mb-0">
        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
         How Can I place an order using Alfabee?
          <span class="fa-stack fa-2x">
            <i class="fas fa-circle fa-stack-2x"></i>
            <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
          </span>
        </button>
      </h2>
    </div>
    <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
      <div class="card-body">
        <p>You only have to follow these simple 
        steps: 
        <br/>1.login to our application 
        <br/>2. Choose the item you want to order offered in different categories 
        <br/>3. Now place an order.</p>
      </div>
    </div>
  </div>
  
  
   
    
  <div class="card">
    <div class="card-header" id="heading5">
      <h2 class="mb-0">
        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
        How long does it take the deliverables to reach the customer?
          <span class="fa-stack fa-2x">
            <i class="fas fa-circle fa-stack-2x"></i>
            <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
          </span>
        </button>
      </h2>
    </div>
    <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
      <div class="card-body">
        <p>Though the time may differ based on the size, site and time of the day, but normally it just takes 45 minutes to one hour to reach our customer. </p>
        
        <p>
Different issues like weather condition and traffic may change delivery timings.
</p>
      </div>
    </div>
  </div>
  
  
  
  
      
  <div class="card">
    <div class="card-header" id="heading6">
      <h2 class="mb-0">
        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
        Can I cancel my order once placed? 
          <span class="fa-stack fa-2x">
            <i class="fas fa-circle fa-stack-2x"></i>
            <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
          </span>
        </button>
      </h2>
    </div>
    <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion">
      <div class="card-body">
        <p>Yes you can indeed cancel it even after making an order but before the approval of order by the relevant restaurant or store. If they have approved your order it cannot be cancelled. For cancelling or altering your order you can access us through chat option given in the Website and Apps. </p>
         
      </div>
    </div>
  </div>
  
  
     
  
      
  <div class="card">
    <div class="card-header" id="heading7">
      <h2 class="mb-0">
        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
        Can I track my order once placed? 
          <span class="fa-stack fa-2x">
            <i class="fas fa-circle fa-stack-2x"></i>
            <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
          </span>
        </button>
      </h2>
    </div>
    <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordion">
      <div class="card-body">
        <p>For sure, you can easily track your order through our Apps and you can contact us by chat option also. </p>
         
      </div>
    </div>
  </div>
   
       
  
      
  <div class="card">
    <div class="card-header" id="heading8">
      <h2 class="mb-0">
        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
       What if there is an issue with my order?
          <span class="fa-stack fa-2x">
            <i class="fas fa-circle fa-stack-2x"></i>
            <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
          </span>
        </button>
      </h2>
    </div>
    <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordion">
      <div class="card-body">
        <p>AlfaBee is not answerable for the faults of restaurant/store. Nevertheless, your grievances shall be taken seriously by AlfaBee and passed on to our customer care unit which will look into the matter seriously. </p>
        
        <p>In order to ensure the correction or replacement of your order you can reach out to us within 15 minutes of placing your order.</p>
        <p>You can contact us in case you get the wrong order and we will resolve your order for you.</p>
        <p>Our extremely skilled customer care management is always on hand to help you to have seamless experience from the time of placing your order to the time of delivery.</p>
      </div>
    </div>
  </div>
  
  
    
      
  <div class="card">
    <div class="card-header" id="heading9">
      <h2 class="mb-0">
        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
       How Can I reach your costumer Care?
          <span class="fa-stack fa-2x">
            <i class="fas fa-circle fa-stack-2x"></i>
            <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
          </span>
        </button>
      </h2>
    </div>
    <div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#accordion">
      <div class="card-body">
        <p>There is an option “Chat with Support” on our website as well as application and you just have to click it and in a minute our customer service administrative will be there for you until you get your order or be contented with our services.</p>
      </div>
    </div>
  </div>
  
  
  
        
  <div class="card">
    <div class="card-header" id="heading10">
      <h2 class="mb-0">
        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse11">
       Why is AlfaBee not delivering in my area?
          <span class="fa-stack fa-2x">
            <i class="fas fa-circle fa-stack-2x"></i>
            <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
          </span>
        </button>
      </h2>
    </div>
    <div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#accordion">
      <div class="card-body">
        <p>Our system will make it clear that which restaurants or stores will be delivering in a particular area. Presently we are working on to enlarge to different parts and collaborate with more partners in order to give you more choices.</p>
      </div>
    </div>
  </div>
  
  
  
        
  <div class="card">
    <div class="card-header" id="heading11">
      <h2 class="mb-0">
        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
       What are the ways you are protecting my details? 
          <span class="fa-stack fa-2x">
            <i class="fas fa-circle fa-stack-2x"></i>
            <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
          </span>
        </button>
      </h2>
    </div>
    <div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#accordion">
      <div class="card-body">
        <p>We are highly concerned about the privacy of our customers. Our ultra modern security makes your personal data or any other details about payment invulnerable to our collaborators or any other entity.</p>
      </div>
    </div>
  </div>
  
  
  
  
  
  
  
  
  
  
  
  
  
  
</div>
		
	 	 
		
		
		
	 </div>
	 	 </div>
</section>
<style>
    .myaccordion {
    max-width: 100% !important;
    }
</style>




<?php 

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

