<section class="section main_content_wrap __inner_nr dashboard">
<div class="container">
		<div class="row">
			<div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
				<div class="wd100">
					<div class="wd100 _dsoutbox">
						<div class="row pl-2 pr-2 pb-3">
						 	 <div class="col-lg-6 col-md-6 col-sm-12">
                             	<h4 class="pb-1"><?php echo $this->lang->line("forgot_password")?></h4> 
							   <div class="form-group">
                                    <input type="email" class="form-control" value="" name="email" id="email" placeholder="<?php echo $this->lang->line("enter_your_email")?>" autocomplete="off"> 
                                </div>
								 
								<span class="common_message"></span>
								<div class="form-group">
									<button type="button" class="btn btn-primary float-right pl-5 pr-5 border-radius-0" id="forgotpassword">Submit</button>
                                </div>
							</div>		
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>