

<section class="section __sttcpg">
		<div class="container">
  
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
		<h2>Guidelines for Vendors</h2>






<p>Alfabee is a gateway  for restaurants to approach to an immeasurable foodie community. Here are some  points to be remembered:</p>
<ul  style="
    
    padding-left: 20px;
">
  <li><strong>Maintain catalogue update:</strong>&nbsp;Though our team is always busy to update  information on Alfabee, yet we highly encourage you to inform us when your  catalogue needs an update. Customers appearing late may complain (Alfabee may  as well) if you change your timings and do not update in your catalogue.</li>
  <li><strong>Say NO to beseeching reviews:</strong>&nbsp;Please your customers so much so that  they cannot help giving you review rather than imploring them for it. So say NO  to beseeching reviews by presenting any kind of reimbursement.</li>
  <ul>
    <li>Don&rsquo;t render any gift,  discounts etc as a settlement for reviews</li>
    <li>Don&rsquo;t present  enticements for customers or users to remove reviews</li>
    <li>Don&rsquo;t motivate any  kind of contest between your employees to accumulate reviews</li>
    <li>Don&rsquo;t collaborate with  companies or PR agencies in order to secure reviews or rating</li>
    <li>Foodie get together is  permitted but it should be kept in mind that reviews through such gatherings  should have a disclaimer beside it so that it becomes clear that this review  has been established on this get together. An invitation to such a get together  does not mean that the customer or use have to give positive review.</li>
    <li>Alfabee encourages  only such business parties whose customers do not anticipate any kind of offers  in lieu of writing positive reviews for them</li>
    <li>A disclaimer will be  triggered on the page of your restaurant to inform Alfabees that your reviews  are apprehensive, if we found any direct or indirect proof of your beseeching  reviews. </li>
  </ul>
  <li><strong>React to disparagement positively:</strong>&nbsp;As long as there are customers there  will be negative reviews. Even if that review seems unjustifiable you need to  take it positively in order to make your services better. Such reviews should  be thought beneficial and a chance to improve your services. Do not ever  respond in a bitter way since you cannot remove or change your reply later.  Therefore use polite words for answering such reviews.</li>
  <li><strong>Be liable:</strong> If there is a review posted on some dubious activities of your  restaurant (like providing shisha when it is not allowed, presenting alcohol to  adolescent or keeping your restaurant open later than allowed) Alfabee will not  curb it. Alfabee may not curb or remove any reviews by third party service  provider rather such reviews will be saved if it circumscribes to our  guidelines. One should be invulnerable in this field and straightforward  reviews about owners, employees or any individual act is not taken personally.</li>
  <li><strong>Don't accommodate:</strong> There may be such a customer who can threat you of bad reviews  and in return require benefits it shows that such attitude has been endured  erstwhile. Customers offered complementary meal for rating low back to the  restaurant is equally wrong. Such problems influence whole community hence  everyone should moderate their dominance. Do report such customers who get  involved in such activities. </li>
  <li>Restaurant owners, employees and all those who are associated  with us with business interests are not allowed to give their reviews on  Alfabee to make the content on Alfabee neutral. No doubt your passion for food may  induce you to share your experiences but being in the field with Alfabee may  naturally incline you to biased review.</li>
</ul>
<p>Significantly, we  (Alfabee) do not encourage paid reviews. Do not trust any visitor who claims to  be an official blogger, worker for reviewing or partner of Alfabee. Alfabee&rsquo;s  employees are not permitted to beseech unusual protocol or free lunch on the  base of ethics and contract. Report any such activity on (alfabee mail  address). If it can be proved proper action will be initiated against such  employees. </p>





		
	 </div>
	 	 </div>
</section>


<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

