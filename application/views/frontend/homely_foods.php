
<section class="section _spg __homely_foods">
		<div class="container">
			<div class="row _rpad ">
				 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pl-2 pr-2">
					<h2><?php echo $this->lang->line("homely_foods");?>
					<div class="__box_search_top">
                              <div class="form-group has-search">
                                  <input type="hidden" value="" id="store_type">
                                <span class="fa fa-search form-control-feedback search-icon"></span>
                                 <span class="delete-icon" onClick="clearRestSearchText()" style="display:none;"><i class="fas fa-times"></i></span>
                                <input type="text" class="form-control" placeholder="<?php echo $this->lang->line("search");?>" id="restSearch">
                              </div>
                            </div>  
					
					</h2>
				 </div>
			</div>
			<div class="__narlist row _rpad ">
                <?php
                foreach($homely_food_users as $users)
                {
                    if($users->image!='')
                    {
                        $image = base_url().'uploads/vendor_images/'.$users->image;
                    }else{
                        $image=base_url(DEFAULT_LOGO_RESTAURANT);
                    }
                    
                    if($this->session->userdata('language')=='ar')
			        {
			            if($users->cuisine_name_ar!='')
			            {
			                $cuis = $users->cuisine_name_ar;
			            }else{
			                $cuis = $users->cuisine_name;
			            }
			        }else{
			             $cuis = $users->cuisine_name;
			        }
			        
			        if($this->session->userdata('language')=='ar')
        	        {
        	            if($users->restaurant_name_ar!='')
        	            {
        	                $rest_name = $users->restaurant_name_ar;
        	            }else{
        	                $rest_name = $users->restaurant_name;
        	            }
        	        }else{
	                    $rest_name = $users->restaurant_name;
	                 }
	                 
	                 if($users->store_type=='1')
				    {
				        $cus = $cuis;
				    }else{
				        $cus = GetNameById($users->store_type,'store_type','store_type');
				    }
			        
                ?>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center pl-2 pr-2">
					<a href="<?=base_url('search_details/'.base64_encode($users->id))?>"><div class="wd100 __wtbx __msims">
					<div class="__msims_img">
						<img class="img-fluid" src="<?= $image?>" />
					</div>
						<h4><?= $rest_name?></h4>
						<h6><?= $cus?></h6>
					</div>
					</a>
				</div>
				<?php
                }
                ?>
			</div>
			
			 <div class="__search_result row _rpad" style="display:none;">
				 
		 </div>	
		</div>
</section>