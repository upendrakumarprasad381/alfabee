<?php
$homely_food_details = GetHomelyFoodById($rest_id);
$segment2 = base64_decode($this->uri->segment(2));

$slct_qry = "SELECT * FROM `most_selling` WHERE vendor_id=".$segment2;
$menu_array = $this->Database->select_qry_array($slct_qry);
?>
<section class="section _spg">
	<div class="container">
			
  		<div class="row justify-content-md-center">
					
			<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 mt-5 pt-4 __svpr">
				<?php
				if($homely_food_details[0]->image!='')
                {
                    $image = base_url().'uploads/vendor_images/'.$homely_food_details[0]->image;
                }else{
                    $image=base_url('images/default_rest.png');
                }
                
                if($this->session->userdata('language')=='ar')
		        {
		            if($homely_food_details[0]->cuisine_name_ar!='')
		            {
		                $cuisin = $homely_food_details[0]->cuisine_name_ar;
		            }else{
		                $cuisin = $homely_food_details[0]->cuisine_name;
		            }
		        }else{
		             $cuisin = $homely_food_details[0]->cuisine_name;
		        }
				?>
				<div class="wd100 __svpr_banner __svpr_banner">
				    <img class="mr-4 ml-4 __gdimg" src="<?= $image?>">
				 	<h2><?= $homely_food_details[0]->restaurant_name?></h2>
					<h5><?= $cuisin?></h5>
					
					 	<div class="__svpr_search">
							<h3><?php echo $this->lang->line("click_on_show_menu");?></h3>
							<div class="row p-0 m-0">
								<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 p-0 ___mr">
									<input type="text" class="form-control" id="autocomplete" value="<?= $homely_food_details[0]->area?>" disabled>
									<!--<div class="pointer"> </div>-->
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 p-0">
									<div class="_btn_serch_search"><a href="<?= base_url('search_details/'.base64_encode($rest_id));?>"><?php echo $this->lang->line("show_menu");?></a> <i class="fas fa-chevron-right"></i></div>
								</div>
							</div>
							<?php
						    $qry = "SELECT count(id) as total_reviews,AVG(rating) as rating FROM user_feedback WHERE vendor_id=".$segment2;
						    $Array = $this->Database->select_qry_array($qry);
						    $rating = !empty($Array[0]->rating)?number_format($Array[0]->rating,1):0;
						    
						    ?>
							<div class="wd100 __star_reviews">
								<!--<div class="__star"> <i class="fa fa-star"></i> <?= $rating?></div>-->
								<!--<div class="_reviews"> (<?= $Array[0]->total_reviews?> reviews) </div>-->
								<div class="__sp_pay text-right"  >
										 
										<img src="<?= base_url('images/cash-icon.svg')?>" itemprop="paymentAccepted" class="p-r ng-scope" width="40" height="20" ng-if="model.restaurant.ac">
								 
										<img src="<?= base_url('images/logo_vco_white.svg')?>" itemprop="paymentAccepted" ng-if="model.restaurant.acr &amp;&amp; global.country.vca" width="45" class="p-r ng-scope" height="20">
								 		
										<img src="<?= base_url('images/visa_brand.svg')?>" itemprop="paymentAccepted" ng-if="model.restaurant.acr" width="50" class="p-r ng-scope" height="20">
								 <img src="<?= base_url('images/mastercard-icon.svg')?>" itemprop="paymentAccepted" ng-if="model.restaurant.acr" width="40" class="p-r ng-scope">
									
									 <img src="<?= base_url('images/ico_amex.svg')?>" itemprop="paymentAccepted" ng-if="model.restaurant.acr" width="25" class="p-r ng-scope">
								
									</div>
								</div>
							</div>
							
						</div>
						
						
							<div class="wd100 __svpr_are">
								

								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
									<h2 class="  "><?= $homely_food_details[0]->restaurant_name?></h2>
									
									<p><?php
		
									if($homely_food_details[0]->about!='undefined' || $homely_food_details[0]->about!='')
									{
									    echo $homely_food_details[0]->about;
									}
									?>
									</p>
								</div>
								<?php
								if(count($menu_array)>0)
								{
								?>
                                <h3><?php echo $this->lang->line("best_seller_dishes");?></h3>
								<div class="row _rpad">
								    
								<?php
								
                                        // $menu = explode(',',$menu_array[0]->menu_id);
                        				foreach($menu_array as $menu_data)
                        				{
                        				    $qry = "SELECT menu_list.id,menu_list.menu_name,menu_list.image,restaurant_details.vendor_id,restaurant_details.restaurant_name FROM `menu_list` LEFT JOIN restaurant_details ON restaurant_details.vendor_id=menu_list.vendor_id WHERE menu_list.id='$menu_data->menu_id' LIMIT 4";
                                            $array = $this->Database->select_qry_array($qry);
                                            foreach($array as $data)
                                            {
                                                if($data->image!='')
                                                {
                                                    $image = base_url('uploads/menu/'.$data->image);
                                                }else{
                                                    $image = base_url(DEFAULT_LOGO_MENU);
                                                }
                                             ?>  
        
        								<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 text-center pl-2 pr-2">
        									<a href="<?=base_url('search_details/'.base64_encode($segment2))?>"><div class="wd100 __wtbx __msims">
        									<div class="__msims_img">
        										<img class="img-fluid" src="<?= $image?>" />
        									</div>
        										<h4><?= $data->menu_name?></h4>
        										<!--<h6><?= $data->restaurant_name?></h6>-->
        									</div>
        									</a>
        								 </div>
        						 <?php
                                            }
                        				}
                				?>

	       </div>
            <?php
				}
			?>
 
		</div>
	
		</div>
		</div>
		</div>
</section>