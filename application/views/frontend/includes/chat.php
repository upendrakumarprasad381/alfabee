<?php
$session_arr = $this->session->userdata('UserLogin');

?>


<div class="col-md-12" id="chatbord" ddstyle="display: none;">
    <div class="chatbox-holder">

        <input type="hidden" id="sender_id" value="<?= !empty($session_arr->id) ? $session_arr->id : '' ?>">
        <input type="hidden" id="receiver_id" value="1">
        <input type="hidden" id="user_name" value="">



        <div class="chatbox chatbox-min" id="chatboxdialogue">
            <div class="chatbox-top">
                <div class="chatbox-avatar">
                    <a  href="javascript:void(0)"><img src="https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-512.png" /></a>
                </div>
                <div class="chat-partner-name" receiverId="1" user_name="Admin" onclick="chat.opendialogueBox(this);">
                    <span class="status donot-disturb"></span>
                    <a href="javascript:void(0)" id="userchatName">Admin</a>
                </div>
                <div class="chatbox-icons">
                    <a href="javascript:void(0);" onclick="chat.closedialogueBox();"><i class="fa fa-times-circle" aria-hidden="true"></i></a>       
                </div>      
            </div>

            <div class="chat-messages">



            </div>

            <div class="chat-input-holder">
                <input placeholder="Message..."  type="text" class="chat-input" onkeypress="chat.autosend_message();"></textarea>
                <input type="submit" onclick="chat.send_message();" value="Send" class="message-send" />
            </div>


        </div>
    </div>
</div>  
<style>
    /*.container{max-width:1170px; margin:auto;}*/
   
   
.jconfirm-buttons .btn.btn-blue{
        float: right;
} 
.chatbox.chatbox-min  .chatbox-icons {
    display: none;
}


.__attcmts { }

.__attcmts:before {  
 
}

.btn-floating {
    position: relative;
    z-index: 1;
    display: inline-block;
    padding: 0;
    margin: 0;
    overflow: hidden;
    vertical-align: middle;
    cursor: pointer;
    border-radius: 50%;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
    width: 47px;
    height: 40px;
} 

.btn-floating i {
    display: inline-block;
    width: inherit;
    color: #7d7d7d;
    text-align: center;
 
    font-size: 1.25rem;
    line-height: 44px;
}

.__attcmts input[type="file"] {
      position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    padding: 0;
    margin: 0;
    cursor: pointer;
    filter: alpha(opacity=0);
    opacity: 0;
    cursor: pointer;
}





















img {
	max-width: 100%;
}

.inbox_people {
    background: #ffffff;
    float: left;
    overflow: hidden;
    width: 100%;
    border-right: 1px solid #c4c4c4;
}

.inbox_msg {
	border: 1px solid #c4c4c4;
	clear: both;
	overflow: hidden;
}

.top_spac {
	margin: 20px 0 0;
}

.recent_heading {
	float: left;
	width: 40%;
}

.headind_srch {
	padding: 10px 29px 10px 20px;
	overflow: hidden;
	border-bottom: 1px solid #c4c4c4;
}

.recent_heading h4 {
	color: #05728f;
	font-size: 21px;
	margin: auto;
}

.srch_bar input {
	border: 1px solid #cdcdcd;
	border-width: 0 0 1px 0;
	width: 80%;
	padding: 2px 0 4px 6px;
	background: none;
}

.srch_bar .input-group-addon button {
	background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
	border: medium none;
	padding: 0;
	color: #707070;
	font-size: 18px;
}

.srch_bar .input-group-addon {
	margin: 0 0 0 -27px;
}

.chat_ib h5 {
    font-size: 18px;
    color: #000000;
    margin: 0 0 8px 0;
    text-transform: capitalize;
}

.chat_ib h5 span {
	font-size: 13px;
	float: right;
}
.chat_ib p {
    font-size: 16px;
    color: #000000;
    margin: auto;
}

.chat_img {
	float: left;
	width: 11%;
}

.chat_ib {
	float: left;
	padding: 0 0 0 15px;
	width: 88%;
}

.chat_people {
	overflow: hidden;
	clear: both;
}

.chat_list {
    border-bottom: 1px solid #c4c4c4;
    margin: 7px 9px;
    padding: 18px 16px 10px;
    /* border: #fff 5px solid; */
    border-radius: 15px;
    -webkit-box-shadow: 0px 3px 13px 1px rgba(0, 0, 0, 0.11);
    -moz-box-shadow: 0px 3px 13px 1px rgba(0, 0, 0, 0.11);
    box-shadow: 1px 0px 3px 1px rgba(0, 0, 0, 0.11);
}

.inbox_chat {
	height: 550px;
	overflow-y: scroll;
}

.active_chat {
    background: #fdfdfd;
}

.incoming_msg_img {
	display: inline-block;
	width: 6%;
}

.received_msg {
	display: inline-block;
	padding: 0 0 0 10px;
	vertical-align: top;
	width: 92%;
}

.received_withd_msg p {
	background: #ebebeb none repeat scroll 0 0;
	border-radius: 3px;
	color: #646464;
	font-size: 14px;
	margin: 0;
	padding: 5px 10px 5px 12px;
	width: 100%;
}

.time_date {
	color: #747474;
	display: block;
	font-size: 12px;
	margin: 8px 0 0;
}

.received_withd_msg {
	width: 57%;
}

.mesgs {
	float: left;
	padding: 30px 15px 0 25px;
	width: 60%;
}

.sent_msg p {
	background: #05728f none repeat scroll 0 0;
	border-radius: 3px;
	font-size: 14px;
	margin: 0;
	color: #fff;
	padding: 5px 10px 5px 12px;
	width: 100%;
}

.outgoing_msg {
	overflow: hidden;
	margin: 26px 0 26px;
}

.sent_msg {
	float: right;
	width: 46%;
}

.input_msg_write input {
	background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
	border: medium none;
	color: #4c4c4c;
	font-size: 15px;
	min-height: 48px;
	width: 100%;
}

.type_msg {
	border-top: 1px solid #c4c4c4;
	position: relative;
}

.msg_send_btn {
	background: #05728f none repeat scroll 0 0;
	border: medium none;
	border-radius: 50%;
	color: #fff;
	cursor: pointer;
	font-size: 17px;
	height: 33px;
	position: absolute;
	right: 0;
	top: 11px;
	width: 33px;
}

.messaging {
	padding: 0 0 50px 0;
}

.msg_history {
	height: 516px;
	overflow-y: auto;
}
ul {
	list-style: none;
}

.chatbox-holder {
	position: fixed;
	right: 0;
	bottom: 0;
	display: flex;
	align-items: flex-end;
	height: 0;
   z-index: 99999;
}

.chatbox {
	width: 400px;
	height: 400px;
	margin: 0 20px 0 0;
	position: relative;
	box-shadow: 0 0 5px 0 rgba(0, 0, 0, .2);
	display: flex;
	flex-flow: column;
	border-radius: 10px 10px 0 0;
	background: white;
	bottom: 0;
	transition: .1s ease-out;
	z-index:1 !important;
}

.chatbox-top {
    position: relative;
    display: flex;
    padding: 10px 0;
    border-radius: 10px 10px 0 0;
    background: #FFC107;
}

.chatbox-icons {
	padding: 0 10px 0 0;
	display: flex;
	position: relative;
}

.chatbox-icons .fa {
	/*background: rgba(220, 0, 0, .6);*/
	/*padding: 3px 5px;*/
	/*margin: 0 0 0 3px;*/
	color: #000;
	font-size: 17px;
	/*border-radius: 0 5px 0 5px;*/
	/*transition: 0.3s;*/
}

.chatbox-icons .fa:hover {
	/*border-radius: 5px 0 5px 0;*/
	/*background: rgba(220, 0, 0, 1);*/
}

.chatbox-icons a,
.chatbox-icons a:link,
.chatbox-icons a:visited {
	color: white;
}

.chat-partner-name,
.chat-group-name {
	flex: 1;
	padding: 0 0 0 50px;
	font-size: 15px;
	font-weight: bold;
	color: #30649c;
	text-shadow: 1px 1px 0 white;
	transition: .1s ease-out;
    text-transform: capitalize;
}

.status {
    height: 10px;
    border-radius: 50%;
    display: inline-block;
    box-shadow: inset 0 0 3px 0 rgba(0, 0, 0, 0.2);
    border: 1px solid rgba(0, 0, 0, 0.15);
    background: #cacaca;
    margin: 0px 3px 0 0;
    padding: 0;
    width: 10px;
    line-height: 0;
}

 

.online {
	background: #b7fb00;
}

.away {
	background: #ffae00;
}


.donot-disturb {
    /*display: none;*/
	background: #ff4343;
	
}

.chatbox-avatar {
    width: 35px;
    height: 35px;
    overflow: hidden;
    border-radius: 50%;
    background: white;
    padding: 6px;
    box-shadow: 0 0 5px 0 rgba(0, 0, 0, .2);
    position: absolute;
    transition: .1s ease-out;
    bottom: 4px;
    left: 6px;
}

.chatbox-avatar img {
	width: 100%;
	height: 100%;
	border-radius: 50%;
}

.chat-messages {
	border-top: 1px solid rgba(0, 0, 0, .05);
	padding: 10px;
	overflow: auto;
	display: flex;
	flex-flow: row wrap;
	align-content: flex-start;
	flex: 1;
}

.message-box-holder {
	width: 100%;
	margin: 0 0 15px;
	display: flex;
	flex-flow: column;
	align-items: flex-end;
}

.message-sender {
	font-size: 12px;
	margin: 0 0 15px;
	color: #30649c;
	align-self: flex-start;
}

.message-sender a,
.message-sender a:link,
.message-sender a:visited,
.chat-partner-name a,
.chat-partner-name a:link,
.chat-partner-name a:visited {
	color: #000;
	text-decoration: none;
}

.message-box {
	padding: 6px 10px;
	border-radius: 6px 0 6px 0;
	position: relative;
	background: rgba(100, 170, 0, .1);
	border: 2px solid rgba(100, 170, 0, .1);
	color: #000;
	font-size: 15px;
}

.message-box:after {
	content: "";
	position: absolute;
	border: 10px solid transparent;
	border-top: 10px solid rgba(100, 170, 0, .2);
	border-right: none;
	bottom: -22px;
	right: 10px;
}

.message-partner {
	background: rgba(0, 114, 135, .1);
	border: 2px solid rgba(0, 114, 135, .1);
	align-self: flex-start;
}

.message-partner:after {
	right: auto;
	bottom: auto;
	top: -22px;
	left: 9px;
	border: 10px solid transparent;
	border-bottom: 10px solid rgba(0, 114, 135, .2);
	border-left: none;
}

.chat-input-holder {
	display: flex;
	border-top: 1px solid rgba(0, 0, 0, .1);
}

.chat-input {
	resize: none;
	padding: 5px 10px;
	height: 40px;
	font-family: 'Lato', sans-serif;
	font-size: 14px;
	color: #1b1b1b;
	flex: 1;
	border: none;
	background: rgba(0, 0, 0, .05);
	border-bottom: 1px solid rgba(0, 0, 0, .05);
}

.chat-input:focus,
.message-send:focus {
	outline: none;
}

.message-send::-moz-focus-inner {
	border: 0;
	padding: 0;
}

.message-send {
	-webkit-appearance: none;
    background: #17a738;
	/*background: -moz-linear-gradient(180deg, #00d8ff, #00b5d6);*/
	/*background: -webkit-linear-gradient(180deg, #00d8ff, #00b5d6);*/
	/*background: -o-linear-gradient(180deg, #00d8ff, #00b5d6);*/
	/*background: -ms-linear-gradient(180deg, #00d8ff, #00b5d6);*/
	/*background: linear-gradient(180deg, #00d8ff, #00b5d6);*/
	color: white;
    font-size: 16px;
    padding: 0 15px;
    border: none;
    text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.3);
    font-weight: bold;
}

.attachment-panel {
	padding: 3px 10px;
	text-align: right;
}

.attachment-panel a,
.attachment-panel a:link,
.attachment-panel a:visited {
	margin: 0 0 0 7px;
	text-decoration: none;
	color: rgba(0, 0, 0, 0.5);
}

.chatbox-min {
	margin-bottom: -362px;
}

.chatbox-min .chatbox-avatar {
	width: 35px;
	height: 35px;
}

.chatbox-min .chat-partner-name,
.chatbox-min .chat-group-name {
	padding: 0 0 0 50px;
}

.settings-popup {
	background: white;
	border-radius: 20px/10px;
	box-shadow: 0 3px 5px 0 rgba(0, 0, 0, .2);
	font-size: 13px;
	opacity: 0;
	padding: 10px 0;
	position: absolute;
	right: 0;
	text-align: left;
	top: 33px;
	transition: .15s;
	transform: scale(1, 0);
	transform-origin: 50% 0;
	width: 120px;
	z-index: 2;
	border-top: 1px solid rgba(0, 0, 0, .2);
	border-bottom: 2px solid rgba(0, 0, 0, .3);
}

.settings-popup:after,
.settings-popup:before {
	border: 7px solid transparent;
	border-bottom: 7px solid white;
	border-top: none;
	content: "";
	position: absolute;
	left: 45px;
	top: -10px;
	border-top: 3px solid rgba(0, 0, 0, .2);
}

.settings-popup:before {
	border-bottom: 7px solid rgba(0, 0, 0, .25);
	top: -11px;
}

.settings-popup:after {
	border-top-color: transparent;
}

#chkSettings {
	display: none;
}

#chkSettings:checked+.settings-popup {
	opacity: 1;
	transform: scale(1, 1);
}

.settings-popup ul li a,
.settings-popup ul li a:link,
.settings-popup ul li a:visited {
	color: #999;
	text-decoration: none;
	display: block;
	padding: 5px 10px;
}

.settings-popup ul li a:hover {
	background: rgba(0, 0, 0, .05);
}

@media (max-width: 767px){
    
    .chatbox {
        width: 282px;
    }
        
    
}
 
</style>