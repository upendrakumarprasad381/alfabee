<?php
$segment1=$this->uri->segment(1);
?>
<h5><?php echo $this->lang->line("shortcuts");?></h5>
<ul>
	<li><a class="<?php if($segment1=='my_profile'){echo 'actv';} ?>" href="<?=base_url('my_profile')?>"><?php echo $this->lang->line("my_profile");?></a></li>
	<li><a class="<?php if($segment1=='saved_address'){echo 'actv';} ?>" href="<?= base_url('saved_address')?>"><?php echo $this->lang->line("saved_address");?></a></li>
	<li style="display:none;"><a class="<?php if($segment1=='change_password'){echo 'actv';} ?>" href="<?= base_url('change_password')?>"><?php echo $this->lang->line("change_password");?></a></li>
	<li><a class="<?php if($segment1=='my_orders' || $segment1=='order_history'){echo 'actv';} ?>" href="<?= base_url('my_orders')?>"><?php echo $this->lang->line("my_orders");?></a></li>
    <li><a class="<?php if($segment1=='my_party_orders' || $segment1 == 'payment_invoice'){echo 'actv';} ?>" href="<?= base_url('my_party_orders')?>"><?php echo $this->lang->line("my_party_orders");?></a></li>
    <li style="display:none;"><a class="<?php if($segment1=='loyality_points'){echo 'actv';} ?>" href="<?=base_url('loyality_points')?>">Loyality Points</a></li>
</ul>