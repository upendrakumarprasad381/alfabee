<?php
$session_arr = $this->session->userdata('UserLogin');
?>

<style>
    .__star {
        display: none;
    }
</style>

<!--NEW s-->
<footer class="footer" id="page-four">
    <div class="container __subfooetr"  style="display:none;">
        <h2><?php echo $this->lang->line("contact_us"); ?></h2>
        <div class="row">

            <!--<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 __sufttext">-->
            <!--	<h5>AlfaBee</h5>-->
            <!--	<p>Dummy text of the printing <br/>-->
            <!--		When an unknown printer<br/>-->
            <!--		<a href="tel:+971565600000"> +971 56 56 00 000</a><br/>-->
            <!--		<a href="mailto:info@afabee.ae">info@afabee.ae</a><br/>-->
            <!--	</p> -->

            <!--</div>-->

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 __suftfrms">

                <div class="row">
                    <div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" id="contact_name" class="form-control" placeholder="" autocomplete="off">
                    </div>

                    <div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" id="contact_email" class="form-control" placeholder="" autocomplete="off">
                    </div>

                    <div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <label for="exampleInputEmail1">Phone</label>
                        <input type="number" id="contact_mobile" class="form-control" placeholder="" autocomplete="off">
                    </div> 

                    <div class="form-group col-lg-10 col-md-12 col-sm-12 col-xs-12">
                        <label for="exampleInputEmail1">Comments</label>
                        <textarea class="form-control" rows="1" id="contact_message" placeholder="" autocomplete="off"></textarea>
                    </div>

                    <div class="form-group col">
                        <div class="wd100 ttbm"></div>
                        <button type="button" class="btn btn-warning btn-block ContactUs">Submit</button>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <span class="register_message common_message"></span>
                    </div>

                </div>


            </div>

        </div>


    </div>

    <div class="wd100 __ftlstftr" >   

        <div class="container" >
            <div class="row">


                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 __ctmmenu">
                    <div class="footer-block-title">
                        <h3><?php echo $this->lang->line("links"); ?></h3>
                    </div>
                    <ul>
                            <!--<li><a href="<?= base_url() ?>#page-two">About Us</a></li>-->
                        <!--<li><a href="#">Blog</a></li>-->
                        <!--<li><a href="#">Careers</a></li>-->
                        <li><a href="<?= base_url('faq') ?>"><?php echo $this->lang->line("faq"); ?></a></li>

                        <li><a href="<?= base_url('store_category/MQ==?partyorder=1') ?>"><?php echo $this->lang->line("party_order"); ?></a></li>
                        <li><a href="<?= base_url('loyalty-program') ?>"><?php echo $this->lang->line("loyalty_program"); ?></a></li>

                        <li><a href="<?= base_url('contact_us') ?>"><?php echo $this->lang->line("contact_us"); ?></a></li>


                    </ul> 
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 __ctmmenu">
                    <div class="footer-block-title">
                        <h3><?php echo $this->lang->line("HELP"); ?></h3>
                    </div>

                    <ul>
                        <li><a href="<?= base_url() ?>partner_registration"><?php echo $this->lang->line("become_a_partner"); ?></a></li>
                        <li><a href="<?= base_url() ?>rider_registration"><?php echo $this->lang->line("become_a_rider"); ?></a></li>
                        <li><a href="<?= base_url('guidelines-for-vendors') ?>"><?php echo $this->lang->line("guidelines_for_vendors"); ?>  </a></li>
                        <li><a href="<?= base_url('privacy-policy') ?>"><?php echo $this->lang->line("privacy"); ?></a></li>
                        <li><a href="<?= base_url('terms-and-conditions') ?>"><?php echo $this->lang->line("terms"); ?></a></li>








                        <!--<li><a href="#">Payments</a></li>-->
                        <!--<li><a href="#">Cancellation & Returns</a></li>-->
                        <!--<li><a href="#">Shipping</a></li>-->

                    </ul> 
                </div>

                <!--<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 __ctmmenu">
                        
                                <div class="footer-block-title">
                                        <h3>POLICY</h3>
                                </div>

                                <ul>
                                
                                 <li><a href="#">Service Policy</a></li> 
                                         
                                 <li><a href="#">Maintenance</a></li> 
                                         <li><a href="#">Sitemap </a></li>  
                                </ul> 
                </div>-->

                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 __ctmmenu">
                    <div class="wd100 __ctmadre  ">
                        <?php echo $this->lang->line("HELP_CENTER"); ?><br>
                        <b><a href="mailto:info@alfabee.pk">info@alfabee.pk</a></b>
                    </div>
                    <div class="wd100 __ctmadre __icos">
                        <?php echo $this->lang->line("PHONE_SUPPORT"); ?><br>
                        <b>

                            <img class="" src="<?= base_url() ?>images/old-typical-phone.png" style="
                                 width: 17px;
                                 margin-right: 3px;
                                 ">


<!--<i class="fas fa-mobile-alt"></i> -->

                            <a href="tel:+92546502929">+92 546-502929</a></b> <br/>
                        <b><i class="fab fa-whatsapp"></i> <a href="https://wa.me/923165572935?text=hello">+92 316-5572935</a></b>  
                    </div>


                    <div class="wd100 __ctmadre">
                        <?php echo $this->lang->line("pakistan"); ?> 
                    </div>

                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 __ctmmenu">
                    <div class="wd100 __soicic">
                        <h6><?php echo $this->lang->line("CONNECT_WITH_US"); ?> </h6> 


                        <div class="_social_icons_top wd100"> 
                            <a target="__blank" href="https://www.facebook.com/AlfaBee152"><i class="fab fa-facebook-f"></i></a> 
                            <a target="__blank" href="https://twitter.com/alfaBee4"><i class="fab fa-twitter"></i></a> 
                            <a href="#"><i class="fab fa-linkedin-in"></i></a>
                            <a href="#"><i class="fab fa-youtube"></i></a> 
                            <a target="__blank" href="https://www.instagram.com/alfabee152/"><i class="fab fa-instagram"></i></a> 
                        </div>

                        <!--<ul>-->
                        <!--	<li><a href="#"><img src="https://nichenose.granddubai.net/pub/media/wysiwyg/f.png"></a></li>-->
                        <!--	<li><a href="#" target="_blank"><img src="https://nichenose.granddubai.net/pub/media/wysiwyg/whatsapp.png"></a></li>-->
                        <!--	<li><a href="#" target="_blank"><img src="https://nichenose.granddubai.net/pub/media/wysiwyg/ingrm.png"></a></li>-->
                        <!--	<li><a href="#"><img src="https://nichenose.granddubai.net/pub/media/wysiwyg/yo.png"  /></a></li>-->
                        <!--</ul>-->

                    </div>
                    <div class="wd100 __store">
                        <h6><?php echo $this->lang->line("SHOP_ON_THE_GO"); ?> </h6> 
                        <ul>
                            <li><a href="https://apps.apple.com/us/app/alfabee/id1536932609" target="_blank"><img src="<?= base_url() ?>images/ios.png"></a></li>
                            <li><a href="https://play.google.com/store/apps/details?id=com.app.alfabee" target="_blank"><img src="<?= base_url() ?>images/play.png"></a></li>
                        </ul>
                    </div> 
                </div>

            </div>
        </div>


        <div class="footer-bottom copy_right">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 copyright-footer">
                        © 2020 AlfaBee. All Rights Reserved.
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 footer-payment">
                        <img class="img-fluid" src="<?= base_url() ?>images/payment-id16.png" >                        </div>
                </div>
            </div>
        </div>	

    </div>
</footer>

<!--new E-->
<footer class="footer wd100" style="display:none;">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ft_menu">
                <h5><?php echo $this->lang->line("links"); ?></h5>

                <ul>
                    <li><a href="<?= base_url('about_us') ?>"><?php echo $this->lang->line("about_us"); ?></a></li>
                    <li><a href="<?= base_url('terms_condition') ?>"><?php echo $this->lang->line("terms"); ?></a></li>
                    <!--<li><a href="#"><?php echo $this->lang->line("faq"); ?></a></li>-->
                    <li><a href="<?= base_url('privacy_policy') ?>"><?php echo $this->lang->line("privacy"); ?></a></li>
                    <li><a href="<?= base_url('contact_us') ?>"><?php echo $this->lang->line("contact_us"); ?></a></li>
                    <li><a href="<?= base_url('suggest_homebusiness') ?>">Suggest a HomeBusiness</a></li>
                </ul>

            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ft_menu">
                <h5><?php echo $this->lang->line("homely_foods"); ?></h5>
                <ul>
                    <?php
                    $qry = "SELECT restaurant_name FROM restaurant_details LEFT JOIN users ON users.id=restaurant_details.vendor_id WHERE users.is_approved=1  LIMIT 5";
                    $sql = $this->Database->select_qry_array($qry);
                    foreach ($sql as $d) {
                        ?>
                        <li><a href="#" style="pointer-events: none;"><?= $d->restaurant_name ?></a></li>
                        <?php
                    }
                    ?>
                    <li><a href="<?= base_url('homely_foods') ?>"><i><?php echo $this->lang->line("more_homely_foods"); ?></i></a></li>


                </ul>
            </div>

            <!--<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ft_menu">-->
            <!--	<h5>Popular Areas</h5>-->
            <!--	<ul>-->
            <!--	 	<li><a href="#">Abu Hail</a></li>-->
            <!--		<li><a href="#">Academic City</a></li>-->
            <!--		<li><a href="#">Al Awir</a></li>-->
            <!--		<li><a href="#">Al Bada'a</a></li>-->
            <!--		<li><a href="#">Al Baraha </a></li>-->
            <!--		<li><a href="#">Abu Hail</a></li>-->
            <!--		<li><a href="#">Academic City</a></li>-->
            <!--		<li><a href="#">Al Awir</a></li>-->
            <!--		<li><a href="#">Al Bada'a</a></li>-->

            <!--	</ul>-->
            <!--</div>-->

            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ft_menu">

                <!--<div class="wd100 f_info">-->
                <!--	<h5> Subscribe to our newsletter</h5>-->
                <!--	<p>Join our mailing list to receive the latest news, offers and expert insights from our team.</p>-->
                <!-- </div>-->


                <!-- <div class="input-group newsletter">-->
                <!--	<input type="email" class="form-control" id="newsletter_email" placeholder="Enter your email"> <span class="input-group-btn">-->
                <!--		<button class="btn newsletter_submit" type="submit">Submit</button>-->
                <!--	</span> -->
                <!--</div>-->


                <div class="wd100 add_your_food">
                    <?php
                    if (empty($session_arr)) {
                        ?>

                        <a class="" href="<?= base_url('add_your_food') ?>">
                            <img class="add_your_food_img" src="<?= base_url() ?>images/add-your_food.png">
                            <h5><?php echo $this->lang->line("add_your_food"); ?></h5>
                            <h6><?php echo $this->lang->line("click_here"); ?> <img src="<?= base_url() ?>images/arw.png"></h6>
                        </a>
                        <?php
                    }
                    ?>
                </div>


                <div class="wd100 ft __social_wrap">
                    <h5><?php echo $this->lang->line("follow_us"); ?></h5>
                    <div class="_social_icons_top wd100"> 
                        <a href="#"><i class="fab fa-facebook-f"></i></a> 
                        <a href="#"><i class="fab fa-twitter"></i></a> 
                        <a href="#"><i class="fab fa-linkedin-in"></i></a>
                        <a href="#"><i class="fab fa-youtube"></i></a> 
                        <a href="#"><i class="fab fa-instagram"></i></a> 
                    </div>
                </div>




            </div>



        </div>
    </div>


</footer>


<?php
$Controller = $this->router->fetch_class();
$Method = $this->router->fetch_method();
$segment3 = $this->uri->segment(3);
$segment2 = $this->uri->segment(2);
$segment1 = $this->uri->segment(1);
$sess_value = $this->session->userdata('language');

 if (!empty($session_arr)) {
    // $this->load->view(FRONTED_DIR . 'includes/chat');
 }
?>
<script>
    var base_url = '<?= base_url(); ?>';
            var Controller = '<?= $Controller ?>';
            var Method = '<?= $Method ?>';
            var segment3 = '<?= $segment3 ?>';
            var segment2 = '<?= $segment2 ?>';
            var segment1 = '<?= $segment1 ?>';
            var sessionValue = '<?= $sess_value ?>';</script>
<script src="<?= base_url() ?>js/additional/jquery.min.js"></script>
<script src="<?= base_url() ?>js/additional/bootstrap.bundle.min.js"></script>
<script src="<?= base_url() ?>js/additional/swiper.min.js"></script>
 <link rel="stylesheet" href="https://app.firezapp.com/css/jquery-confirm.min.css">
<?php
if ($segment1 == 'order_history') {
    ?>
    <script src="<?= base_url() ?>js/additional/progress-bar.js"></script>
    <?php
}
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://demo.softwarecompany.ae/home_eats/js/additional/jquery-confirm.min.js"></script>
<script src="<?= base_url() ?>js/additional/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="<?= base_url() ?>js/frontend.js"></script>
<script src="<?= base_url() ?>js/newCommon.js"></script>

<?php  if (!empty($session_arr)) { ?>
    <!--<script src="<= base_url() ?>js/chat.js"></script>-->
<?php } ?>

<script>
            var swiper = new Swiper('.catpro', {
            slidesPerView: 5
                    , spaceBetween: 10
                    , loop: true, // init: false,
                    pagination: {
                    el: '.swiper-pagination'
                            , clickable: true
                            , }
            , navigation: {
            nextEl: '.catpro-next'
                    , prevEl: '.catpro-prev'
                    , }
            , breakpoints: {
            1024: {
            slidesPerView: 5
                    , spaceBetween: 10
                    , }
            , 992: {
            slidesPerView: 5
                    , spaceBetween: 5
                    , }
            , 768: {
            slidesPerView: 3
                    , spaceBetween: 1
                    , }
            , 640: {
            slidesPerView: 2
                    , spaceBetween: 1
                    , }
            , 320: {
            slidesPerView: 1
                    , spaceBetween: 1
                    , }
            }
            , autoplay: {
            delay: 2000
                    , disableOnInteraction: false
                    , }
            });
            var options = {};
            $('.tabnavhd').click(function(){
    $('.tablistlr').toggle(600);
    });
            $('.search_icontab').click(function(){
    $('.tabviewsearch').toggle(600);
    });
            $('.srchbtnclos').click(function(){
    $('.tabviewsearch').hide('slow');
    });
            $('.filter_tog').click(function(){
    $('.left_filtterbox').toggle(1000); });
            $('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
    });
            $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
    });</script>
<?php
if ($segment1 == 'checkout' || $segment1 == 'saved_address') {
    ?>
    <script src="<?= base_url() ?>js/additional/map.js"></script>
    <?php
    if ($this->session->userdata('language') == 'en' || empty($this->session->userdata('language'))) {
        ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&callback=initMap&libraries=places&language=en"></script>
        <?php
    } else {
        ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&callback=initMap&libraries=places&language=ar"></script>
        <?php
    }
    ?>
    <?php
} else if ($segment1 == 'search_details') {
    ?>
    <script src="<?= base_url() ?>js/additional/party_venue.js"></script>
    <?php
    if ($this->session->userdata('language') == 'en' || empty($this->session->userdata('language'))) {
        ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&callback=initMap&libraries=geometry,places&language=en"></script>
        <?php
    } else {
        ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&callback=initMap&libraries=geometry,places&language=ar"></script>
        <?php
    }
    ?>
    <?php
} else if ($segment1 == 'partner_registration' || $segment1 == 'rider_registration') {
    ?>
    <script src="<?= base_url() ?>js/additional/add_food.js"></script>
    <?php
    if ($this->session->userdata('language') == 'en' || empty($this->session->userdata('language'))) {
        ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&callback=initMap&libraries=geometry,places&language=en"></script>
        <?php
    } else {
        ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&callback=initMap&libraries=geometry,places&language=ar"></script>
        <?php
    }
    ?>
    <?php
} else {
    ?>
    <script src="<?= base_url() ?>js/additional/index.js" style="display:none"></script>
    <!--<script src="<?= base_url() ?>js/additional/add_food.js"></script>-->
    <!--<script src="<?= base_url() ?>js/additional/location.js"></script>-->
    <?php
    if ($this->session->userdata('language') == 'en' || empty($this->session->userdata('language'))) {
        ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&libraries=geometry,places&callback=initMap&language=en" ></script>
        <!--<script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&libraries=geometry,places&callback=initAutocomplete&language=en" async="" defer=""></script>-->
        <?php
    } else {
        ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&libraries=geometry,places&callback=initMap&language=ar" async="" defer=""></script>
        <!--<script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&libraries=geometry,places&callback=initAutocomplete&language=ar" async="" defer=""></script>-->
        <?php
    }
    ?>

        <!--<script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&libraries=places&&callback=initAutocomplete&language=en" async defer></script> -->
        <!--<script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&libraries=geometry,places&callback=initAutocomplete" async defer></script> -->
    <?php
}
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="<?= base_url() ?>assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
<!--<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous"> </body>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-185756887-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-185756887-1');
</script>
<script>
            $(function () {
            $('[data-toggle="tooltip"]').tooltip()
            })





<!--NEWs-->


// Enable Smooth Scrolling ...  by Chris Coyier of CSS-Tricks.com
            $('#mainNav a[href*="#"]:not([href="#"]):not([href="#show"]):not([href="#hide"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
    $('html,body').animate({
    scrollTop: target.offset().top
    }, 1000);
            return false;
    }
    }
    });</script> 
<?php
if ($segment1 == 'thankyou' || $segment1 == 'my_orders' || $segment1 == 'order_history') {
    ?>

    <script>
                var order_date = document.getElementById('order_time').value;
                var oldDateObj = new Date(order_date);
                // console.log(oldDateObj);
                var newDateObj = new Date();
                newDateObj.setTime(oldDateObj.getTime() + (3 * 60 * 1000));
                var x = setInterval(function() {

                var now = new Date().getTime();
                        var distance = newDateObj - now;
                        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                        element = document.getElementById("timer");
                        if (element != null) {

                document.getElementById("timer").innerHTML = "You can cancel this order within " + minutes + "m " + seconds + "s ";
                }
                if (distance < 0) {
                clearInterval(x);
                        var a = document.getElementById("timer");
                        if (a != null)
                {
                a.style.display = "none";
                }
                if (a != null)
                {
                setTimeout(function () {
                window.location = "";
                }, 1000);
                }
                }
                }, 1000);</script>
    <?php
}
?>

<script>

            $("#accordion").on("hide.bs.collapse show.bs.collapse", e = > {
    $(e.target)
            .prev()
            .find("i:last-child")
            .toggleClass("fa-minus fa-plus");
    });
</script>



</html>