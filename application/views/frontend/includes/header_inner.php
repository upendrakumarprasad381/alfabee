<?php
$session_arr = $this->session->userdata('UserLogin');
$segment1 = $this->uri->segment(1);

$session_cart = $this->session->userdata('CartData');
if (!empty($session_cart)) {
    $get_restaurant_details = GetRestaurantDetails($session_cart[0]['vendor_id']);
}
$RedirectUrl = base_url('checkout');
$OnclickUrl = empty($session_arr) ? "LoginAndCallBack('$RedirectUrl')" : "window.location = '$RedirectUrl'";
?>
<!doctype html>
<html lang="en">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--<link rel="stylesheet" href="<?= base_url() ?>css/bootstrap.css">-->
        <?php
        if ($this->session->userdata('language') == 'en' || empty($this->session->userdata('language'))) {
            ?>
            <link rel="stylesheet" href="<?= base_url() ?>css/bootstrap.css">
            <link rel="stylesheet" href="<?= base_url() ?>css/style.css">
            <?php
        } else {
            ?>
 <link rel="stylesheet" href="<?= base_url() ?>css/bootstrap.css">
            <link rel="stylesheet" href="<?= base_url() ?>css/style.css">
<!--            
            <link rel="stylesheet" href="<?= base_url() ?>css/bootstrap_ar.css">
            <link rel="stylesheet" href="<?= base_url() ?>css/style_ar.css">-->
            <?php
        }
        ?>
        <!--<link rel="stylesheet" href="https://demo.softwarecompany.ae/home_eats_design/html/css/bootstrap.css">-->
        <!--<link rel="stylesheet" href="https://demo.softwarecompany.ae/home_eats_design/html/css/style.css">-->
        <!--<link rel="stylesheet" href="<?= base_url() ?>css/style.css">-->
        <link rel="stylesheet" href="<?= base_url() ?>css/swiper.css">
        <link href="<?= base_url() ?>css/jquery-ui.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
        <link rel="stylesheet" href="<?= base_url() ?>css/jquery-confirm.min.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

        <title>Alfabee</title>

        <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>images/favicons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>images/favicons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>/favicons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>images/favicons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>images/favicons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>images/favicons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>images/favicons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>images/favicons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>images/favicons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url() ?>images/favicons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>images/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>images/favicons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>images/favicons/favicon-16x16.png">
        <link rel="manifest" href="<?= base_url() ?>images/favicons/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?= base_url() ?>images/favicons/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff"> </head>

    <body>
        <header class="header">
            <div class="container __cctest">
                <div class="logo ">
                    <a href="<?= base_url() ?>"><img class="img-fluid" src="<?= base_url() ?>images/logo.png" alt="Alfabee Logo"></a>
                </div>
                <div class="_hdrtpart">
                    <div class="wd100">
                         <style>
                            .__cart_droplist .qtytd{
                                display: none;
                            }
                        </style>
                        <?php if (empty($session_arr)) { ?>
                            <div class="__loregi" style="display:none;"> <a class="__login" href="<?= base_url('login_signup') ?>"><?php echo $this->lang->line("login"); ?> </a> <a class="__sign_up" href="<?= base_url('login_signup') ?>"><?php echo $this->lang->line("create_an_account"); ?></a> </div>
                            <div class="log_rgi" style="padding-top:0px;"><div class="login_upwrap">
                                    <a class="login" href="<?= base_url('login_signup') ?>">Sign up or Log in</a> 
                                </div></div>
                        <?php } ?>
                        <?php if (!empty($session_arr)) { ?>
                            <?php
                            $userdetails = GetUserDetails($session_arr->id);
                            if (isset($userdetails[0]->image) && $userdetails[0]->image != '') {
                                $image = base_url() . 'uploads/user_images/' . $userdetails[0]->image;
                            } else {
                                $image = base_url(DEFAULT_LOGO_USER);
                            }
                            ?>
                            <div class="dash">
                                <ul class="navbar-nav after_login">
                                    <li class="nav-item dropdown dmenu">
                                        <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="navbardrop" data-toggle="dropdown" aria-expanded="false">
                                            <img width="24" height="24" class="img-fluid" src="<?= $image ?>"> <span class="lim"><?= $session_arr->name ?></span>
                                        </a>
                                        <div class="dropdown-menu sm-menu">
                                            <a class="dropdown-item" href="<?= base_url('my_profile') ?>" ><?php echo $this->lang->line("my_profile"); ?></a>
                                            <a class="dropdown-item" href="<?= base_url('saved_address') ?>"><?php echo $this->lang->line("saved_address"); ?></a>
                                            <a class="dropdown-item" href="<?= base_url('my_orders') ?>" ><?php echo $this->lang->line("my_orders"); ?></a>
                                            <a class="dropdown-item" href="<?= base_url('my_party_orders') ?>" style="display:none;"><?php echo $this->lang->line("my_party_orders"); ?></a>
                                            <a class="dropdown-item" href="<?= base_url('change_password') ?>" style="display:none;" ><?php echo $this->lang->line("change_password"); ?></a> 
                                            <a class="dropdown-item"  href="<?= base_url('loyality_points') ?>" style="display:none;">Loyality Points</a>
                                            <a class="dropdown-item" href="<?= base_url('logout') ?>"><?php echo $this->lang->line("logout"); ?></a> 
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>

                        <?php
                        if ($segment1 != 'checkout') {
                            ?>
                            <div class="top_action cart_icon">
                                <a href="">
                                    <div class="_gtag cart_count"></div> <img src="<?= base_url() ?>images/cart-icon.png"> </a>
                                <?php
                                if (!empty($session_cart) && count($session_cart) > 0) {
                                    ?>

                                    <div class="wd100 __wtbx __cartsummy  __cart_droplist" style="display:none;">
                                        <h2><?php echo $this->lang->line("your_cart"); ?></h2>
                                        <div class="drop_close"><i class="far fa-times-circle" id="close_btn"></i></div>
                                        <h5><a href="#"><?= ucwords($get_restaurant_details[0]->restaurant_name) ?></a></a></h5>
                                        <div class="__cartsummytabv wd100">
                                            <table class="table __crtb">
                                                <tbody>
                                                    <?php
                                                    $subtotal = 0;
                                                    foreach ($session_cart as $cart) {
                                                         $vendorId=!empty($session_cart[0]['vendor_id']) ? $session_cart[0]['vendor_id'] : '';
                                                        $Qry1 = "SELECT *  FROM `menu_list` WHERE id=" . $cart['menu_id'];

                                                        $Array1 = $this->Database->select_qry_array($Qry1);

                                                        if ($this->session->userdata('language') == 'ar') {
                                                            if ($Array1[0]->menu_name_ar != '') {
                                                                $menuname = $Array1[0]->menu_name_ar;
                                                            } else {
                                                                $menuname = $cart['menu_name'];
                                                            }
                                                        } else {
                                                            $menuname = $cart['menu_name'];
                                                        }
                                                        ?>
                                                        <tr>
                                                            <td class="__itxtt"><?= $menuname ?>
                                                                <?php
                                                                if (isset($cart['size'])) {
                                                                    //  echo ',';
                                                                    $array = array_column($cart['size'], 'name');
                                                                    $array = implode(',', $array);
                                                                } else {
                                                                    $array = '';
                                                                }
                                                                if (isset($cart['addon'])) {
                                                                    //echo ',';
                                                                    $array1 = array_column($cart['addon'], 'name');
                                                                    $array1 = implode(',', $array1);
                                                                } else {
                                                                    $array1 = '';
                                                                }
                                                                if (isset($cart['topping'])) {   //echo ',';
                                                                    $array2 = array_column($cart['topping'], 'name');
                                                                    $array2 = implode(',', $array2);
                                                                } else {
                                                                    $array2 = '';
                                                                }
                                                                if (isset($cart['drink'])) {
                                                                    //echo ',';
                                                                    $array3 = array_column($cart['drink'], 'name');
                                                                    $array3 = implode(',', $array3);
                                                                } else {
                                                                    $array3 = '';
                                                                }
                                                                if (isset($cart['dip'])) {
                                                                    //echo ',';
                                                                    $array4 = array_column($cart['dip'], 'name');
                                                                    $array4 = implode(',', $array4);
                                                                } else {
                                                                    $array4 = '';
                                                                }
                                                                if (isset($cart['side'])) {
                                                                    $array5 = array_column($cart['side'], 'name');
                                                                    $array5 = implode(',', $array5);
                                                                } else {
                                                                    $array5 = '';
                                                                }
                                                                //  $options = $array.$array1.$array2.$array3.$array4.$array5;
                                                                $options = implode(",", array_filter([$array, $array1, $array2, $array3, $array4, $array5]));
                                                                ?>
                                                                <?php
                                                                if ($cart['choice'] == 1) {
                                                                    ?>
                                                                    <i class="fa fa-info-circle hidden-xs" data-toggle="tooltip" title="<?= $options ?>"></i>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </td>
                                                            <td class="qtytd">
                                                                <section class="qty-update-section incart b-a ng-scope">
                                                                    <button type="button" class="btn btn-sm b-r" onclick="updateQty(this)" data-id="<?= $cart['menu_id'] ?>" data-cart="<?= $cart['cart_id'] ?>" data-type="minus" data-value="<?= $cart['quantity'] ?>"> 
                                                                        <i class="fa fa-minus orange"></i> 
                                                                    </button> 
                                                                    <span class="f-11" data-id="<?= $cart['menu_id'] ?>" id="quantity">
                                                                        <b class="ng-binding"><?= $cart['quantity'] ?></b></span>
                                                                    <button type="button" class="btn btn-sm b-l" onclick="updateQty(this)" data-id="<?= $cart['menu_id'] ?>" data-cart="<?= $cart['cart_id'] ?>" data-type="plus" data-value="<?= $cart['quantity'] ?>"> <i class="fa fa-plus orange"></i> </button>
                                                                </section>
                                                            </td>
                                                            <td class="__crpc"><?= DecimalAmount($cart['subtotal'], 2) ?></td>
                                                            <td> <a class="__close remove_cart_product" href="" data-id="<?= $cart['menu_id'] ?>" data-cart="<?= $cart['cart_id'] ?>"><i class="fas fa-times-circle"></i></a> </td>
                                                        </tr>
                                                        <?php
                                                        $subtotal += $cart['price'] * $cart['quantity'];
                                                    }
                                                    $this->session->set_userdata('sub_total', $subtotal);
                                                    ?>

                                                </tbody></table>
                                        </div>

                                        <div class="wd100 __cartsummytabv_btpart">
                                            <table class="table">
                                                <tbody><tr>
                                                        <td align="left"><?php echo $this->lang->line("sub_total"); ?></td>
                                                        <td align="right" class="__srylst">PKR <?= DecimalAmount($subtotal, 2); ?></td>
                                                    </tr>

                                                    <!--
                                                                                                                    <tr>
                                                                                                                            <td align="left">Coupon Discount</td>
                                                                                                                            <td align="right" class="__srylst">PKR 00.00</td>
                                                                                                                    </tr>
                                                    <?php
                                                    $sub_tot = DecimalAmount($subtotal, 2);

                                                    $total_amount = $sub_tot;
                                                    ?>
                                                    -->
                                                    <tr>
                                                        <td align="left"><b><?php echo $this->lang->line("total_amount"); ?></b> </td>
                                                        <td align="right"  class="__crt_totalleft"><b>PKR <?= DecimalAmount($total_amount, 2); ?></b></td>
                                                    </tr>
                                                </tbody></table>
                                            <div class="form-group">
                                                <?php
                                                $start_time = date('H:i', strtotime($get_restaurant_details[0]->delivery_time_start));
                                                $end_time = date('H:i', strtotime($get_restaurant_details[0]->delivery_time_ends));
                                                $currentTime = date('H:i', time());

                                                if ($start_time != '00:00:00' && $end_time != '00:00:00') {
                                                    if ($start_time < $currentTime && $currentTime > $end_time) {
                                                        $delivery = 1;
                                                    } elseif ($currentTime < $start_time) {
                                                        $delivery = 0;
                                                    } else {
                                                        $delivery = 1;
                                                    }
                                                }
                                                ?>
                                                <input type="hidden" value="<?= $delivery ?>" id="delivery_time">

                                               <button type="button" class="btn btn-success btn-block checkoutbtn" id="proceed_to_checkout" onClick="window.location='<?= base_url('search_details/'.  base64_encode($vendorId)) ?>';">View More</button>

                                     <!--<button type="button" class="btn btn-success btn-block checkoutbtn" id="proceed_to_checkout" url="<?= $OnclickUrl ?>" onClick=""><?php echo $this->lang->line("proceed_to_checkout"); ?></button>-->
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="wd100 __wtbx __cartsummy  __cart_droplist empty_cart" style="display:none;">
                                        <h2><?php echo $this->lang->line("your_cart"); ?></h2>
                                        <div class="drop_close"><i class="far fa-times-circle" id="close_btn"></i></div>
                                        <h5></h5>
                                        <img src="<?= base_url('images/empty-cart.svg') ?>" width="80">
                                        <p><?php echo $this->lang->line("no_items_cart"); ?></p>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                        }
                        ?>
                        <ul class="navbar-nav lang" style="display:none">
                            <li class="nav-item dropdown"> 
                                <!--<a class="nav-link dropdown-toggle ar" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> ﺍﻟﻌﺮﺑﻴﺔ  </a>-->
                                <form method="POST" action="<?= base_url() ?>set_lang">
                                    <?php
                                    $url_value = $this->uri->segment(1);
                                    if (!empty($this->uri->segment(2))) {
                                        $url_value .= '/' . $this->uri->segment(2);
                                    }
                                    ?>
                                    <!--<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">-->
                                    <input type="hidden" id="url"  value="<?php echo $url_value; ?>" name='url'>  
                                    <select id="choose_lang" name="choose_lang" onchange="this.form.submit()">
                                        <option value="en" <?php if ($this->session->userdata('language') == 'en') { ?>selected<?php } ?> >English</option>
                                        <option value="ar"  <?php if ($this->session->userdata('language') == 'ar') { ?>selected<?php } ?>>ﺍﻟﻌﺮﺑﻴﺔ</option></select>
                                    <!--<a class="dropdown-item" href="#">English</a> -->
                                    <!--<a class="dropdown-item ar" href="#"> ﺍﻟﻌﺮﺑﻴﺔ </a> -->
                                    <!--</div>-->
                                </form>
                            </li>
                        </ul>

                        <!--<ul class="navbar-nav lang">-->
                        <!--	<li class="nav-item dropdown"> -->
                        <!--		<a class="nav-link dropdown-toggle ar" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">   </a>-->
                        <!--		<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> -->
                        <!--<a class="dropdown-item" href="#">English</a> -->
                        <!--<a class="dropdown-item ar" href="#"> ﺍﻟﻌﺮﺑﻴﺔ </a> -->
                        <!--		</div>-->
                        <!--	</li>-->
                        <!--</ul>-->
                        <?php
                        if ($segment1 == 'search_result') {
                            ?>
                            <!--<div class="__serachbox __top_search">-->
                            <!--	<div class="row">-->
                            <!--		<div class="col-lg-10 col-md-10 col-sm-9 col-xs-12 tpsrfd">-->
                            <!--			<input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Search restaurant..." id="restSearchBox">-->
                            <!--<div class="pointer"> </div>-->
                            <!--		</div>-->
                            <!--		<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 tpsrfd">-->
                            <!--			<div class="_btn_serch_search"> <img src="<?= base_url() ?>images/search_icon-w.png"> </div>-->
                            <!--		</div>-->
                            <!--	</div>-->
                            <!--</div>-->
                            <?php
                        }
                        ?>

                    </div>
                </div>
            </div>
        </header>