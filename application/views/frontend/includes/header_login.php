<?php
$segment1=$this->uri->segment(1);
?>
<!doctype html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!--<link rel="stylesheet" href="<?= base_url() ?>css/bootstrap.css">-->
	<!--<link rel="stylesheet" href="css/style.css">-->
	<!--<link rel="stylesheet" href="https://demo.softwarecompany.ae/home_eats_design/html/css/style.css">-->
	<?php
	if($this->session->userdata('language')=='en' || empty($this->session->userdata('language')))
	{
	 ?>
	<link rel="stylesheet" href="<?= base_url() ?>css/bootstrap.css">
	<link rel="stylesheet" href="<?= base_url() ?>css/style.css">
	<?php
	}else{
	    ?>

	<link rel="stylesheet" href="<?= base_url() ?>css/bootstrap_ar.css">
	<link rel="stylesheet" href="<?= base_url() ?>css/style_ar.css">
	<?php
	}
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<link rel="stylesheet" href="<?= base_url() ?>css/swiper.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

	<title>Alfabee</title>
	<link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>images/favicons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>images/favicons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>images/favicons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>images/favicons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>images/favicons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>images/favicons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>images/favicons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>images/favicons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>images/favicons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="<?= base_url() ?>images/favicons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>images/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>images/favicons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>images/favicons/favicon-16x16.png">
	<link rel="manifest" href="images/favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?= base_url() ?>images/favicons/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff"> </head>

<body>
	<header class="header">
		<div class="container __cctest">
			<div class="logo ">
				<a href="index.php"><img class="img-fluid" src="<?= base_url() ?>images/logo.png" alt="alfabee"></a>
			</div>
			<div class="_hdrtpart">
				<div class="wd100">
<!--					<div class="dash">-->
<!--						<ul class="navbar-nav after_login">-->
<!--							<li class="nav-item dropdown dmenu">-->
<!--								<a class="nav-link dropdown-toggle" href="javascript:void(0)" id="navbardrop" data-toggle="dropdown" aria-expanded="false">-->
<!--									<img width="24" height="24" class="img-fluid" src="<?= base_url() ?>images/smiling-man.jpg"> <span class="lim">Jone Jone </span>-->
<!--								</a>-->
<!--								<div class="dropdown-menu sm-menu">-->
<!--									<a class="dropdown-item" href="my_profile.php">My Profile</a>-->
<!--									<a class="dropdown-item" href="my_ads.php">My Ads</a>-->
<!--									<a class="dropdown-item" href="my_orders.php">My Orders</a>-->
<!--									<a class="dropdown-item" href="change_password.php">Change Password</a> -->
<!--									<a class="dropdown-item" href="#">Logout</a> -->
<!--								</div>-->
<!--							</li>-->
<!--						</ul>-->
<!--					</div>-->
					<!--<div class="cart_icon">-->
					<!--	<a href="#">-->
					<!--		<div class="_gtag">2</div> <img src="<?= base_url() ?>images/cart-icon.png"> </a>-->
					<!--</div>-->
					
					<ul class="navbar-nav lang">
						<li class="nav-item dropdown"> 
						<form method="POST" action="<?= base_url()?>set_lang">
						     <?php
							    $url_value = $this->uri->segment(1);
							    if(!empty($this->uri->segment(2)))
							    {
							        $url_value .= '/'.$this->uri->segment(2);
							    }
							    if(!empty($this->uri->segment(3)))
							    {
							        $url_value .= '/'.$this->uri->segment(3);
							    }
							?>
							 <input type="hidden" id="url"  value="<?php echo $url_value;?>" name='url'>  
							
							<select id="choose_lang" name="choose_lang" onchange="this.form.submit()"> 
							    <option value="en" <?php if($this->session->userdata('language')=='en'){?>selected<?php }?> >English</option>
                                <option value="ar"  <?php if($this->session->userdata('language')=='ar'){?>selected<?php }?>>ﺍﻟﻌﺮﺑﻴﺔ</option>
								<!--<a class="dropdown-item" href="#">English</a> -->
								<!--<a class="dropdown-item ar" href="#"> ﺍﻟﻌﺮﺑﻴﺔ </a> -->
							</select>
							</form>
						</li>
					</ul>
					<!--<ul class="navbar-nav lang">-->
					<!--	<li class="nav-item dropdown"> -->
							<!--<a class="nav-link dropdown-toggle ar" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> ﺍﻟﻌﺮﺑﻴﺔ  </a>-->
					<!--		<form method="POST" action="<?= base_url()?>set_lang">-->
							<!--<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">-->
					<!--		<input type="hidden" id="url"  value="<?php echo $_SERVER['REQUEST_URI'];?>" name='url'>  -->
					<!--		<select id="choose_lang" name="choose_lang" onchange="this.form.submit()">
					<option value="english" <?php if($this->session->userdata('language')=='english'){?>selected<?php }?> >English</option>-->
     <!--                       <option value="arabic"  <?php if($this->session->userdata('language')=='arabic'){?>selected<?php }?>>Arabic</option></select>-->
								<!--<a class="dropdown-item" href="#">English</a> -->
								<!--<a class="dropdown-item ar" href="#"> ﺍﻟﻌﺮﺑﻴﺔ </a> -->
							<!--</div>-->
					<!--		</form>-->
					<!--	</li>-->
					<!--</ul>-->
					<div class="main_menu">
						<div class="menu_wrap">
							<nav class="navbar navbar-expand-lg navbar-light">
								<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
								<div class="collapse navbar-collapse" id="navbarSupportedContent">
									<ul class="navbar-nav ml-lg">
										<li class="nav-item <?php if($segment1=='offers'){echo 'active';} ?>"> <a class="nav-link" href="<?= base_url('offers')?>"><?php echo $this->lang->line("offers");?></a> </li>
										<li class="nav-item <?php if($segment1=='most_selling'){echo 'active';} ?>"><a class="nav-link" href="<?= base_url('most_selling')?>"><?php echo $this->lang->line("most_selling");?></a></li>
										<li class="nav-item <?php if($segment1=='homely_foods'){echo 'active';} ?>"><a class="nav-link" href="<?=base_url('homely_foods')?>"><?php echo $this->lang->line("homely_foods");?></a></li>
									</ul>
									
								 
								</div>
						 	</nav>
						</div>
					</div>
					
					
				</div>
			</div>
		</div>
	</header>
 