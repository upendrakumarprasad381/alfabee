<style>
    .test {
        padding: 30px 0 5px;
    }

    .img-card .img-section {
        width: 40%;
        border-radius: 8px 0 0 8px;
        max-width: 200px;
        position: relative;
    }
    .img-card .img-section {
        width: 40%;
        border-radius: 8px 0 0 8px;
        max-width: 200px;
        position: relative;
    }
    .img-section img{
        width:70%;
    }
    #loader {      
        display: none;      
        position: fixed;      
        z-index: 999;      
        height: 2em;      
        width: 2em;      
        overflow: show;      
        margin: auto;      
        top: 0;      
        left: 0;      
        bottom: 0;      
        right: 0;    
    }    
    #loader:before {      
        content: '';      
        display: block;      
        position: fixed;      
        top: 0;      
        left: 0;      
        width: 100%;      
        height: 100%;      
        background-color: rgba(0,0,0,0.3);    
    }
    #loader_rider {      
        display: none;      
        position: fixed;      
        z-index: 999;      
        height: 2em;      
        width: 2em;      
        overflow: show;      
        margin: auto;      
        top: 0;      
        left: 0;      
        bottom: 0;      
        right: 0;    
    }    
    #loader_rider:before {      
        content: '';      
        display: block;      
        position: fixed;      
        top: 0;      
        left: 0;      
        width: 100%;      
        height: 100%;      
        background-color: rgba(0,0,0,0.3);    
    }
</style>
<?php
$sessionUser = GetSessionArrayUser();
$lan=  getsystemlanguage();
?>
<!--NEW S-->
<div class="banner_wrap">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <!--<ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol> -->
        <div class="carousel-inner">
            <?php
            $slider = GetHomeSlider();
            for ($i = 0; $i < count($slider); $i++) {
                if ($i == 0) {
                    $class = "active";
                } else {
                    $class = '';
                }
                if ($slider[$i]->image != '') {
                    $main_url = base_url() . 'uploads/home_banner/' . $slider[$i]->image;
                } else {
                    $main_url = base_url() . 'images/bnr1.jpg';
                }
                ?>

                <div class="carousel-item <?= $class ?>"> <img class="d-block w-100" src="<?= $main_url ?>"></div>
                <?php
            }
            ?>
<!--<div class="carousel-item"><img class="d-block w-100" src="images/bnr2.jpg"></div>-->
<!--<div class="carousel-item"><img class="d-block w-100" src="images/bnr3.jpg"></div>-->
<!--<div class="carousel-item"><img class="d-block w-100" src="images/bnr4.jpg"></div>-->
        </div>

        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"> 
            <span class="carousel-control-prev-icon" aria-hidden="true"></span> 
            <span class="sr-only">Previous</span> 
        </a> 
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> 
            <span class="carousel-control-next-icon" aria-hidden="true"></span> 
            <span class="sr-only">Next</span> 
        </a> 
    </div>
</div>








<section class="__bner_bottom">
    <div class="container">
        <div class="row">

            <!--<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">-->
            <div class="col-lg-2 col-md-1 col-sm-12 col-xs-12">

            </div>
            <!--<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">-->

            <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
                <div class="__b_frmplt_wrap">
                    <!--<h2>“We Deliver with Love”</h2>	-->
                    <form method="POST" action="<?= base_url() ?>search_result" id="search_location">
                        <div class="__b_frmplt">
                            <h5>Lorem Ipsum is simply dummy text of the printing</h5>
                            <div class="__frbox">
                                <div class="row">

                                    <div class="col __frdop">
                                        <select class="form-control" id="store_type" name="store_type">
                                            <?= get_storetype() ?>
                                            <!--<option>Restaurants</option>-->
                                            <!--<option>Grocery</option>-->
                                            <!--<option>Medical Stores</option>-->
                                            <!--<option>Gifts</option>-->
                                            <!--<option>Dairy</option>-->
                                        </select>

                                    </div>
                                    <div class="col-6 __frdop_ssl">
                                        <input type="hidden" value="" class="searchtype" name="searchtype">
                                        <input type="hidden" value="" class="latitude" name="latitude">
                                        <input type="hidden" value="" class="longitude" name="longitude">
                                        <input type="hidden" value="" class="get_location" name="get_location">
                                        <input type="hidden" value="" id="locality" name="locality" >
                                        <input type="hidden" value="" id="administrative_area_level_1" name="administrative_area_level_1">
                                        <input type="hidden" value="" id="sublocality_level_1" name="sublocality_level_1">
                                        <input type="text" id="autocomplete" class="form-control" placeholder="<?php echo $this->lang->line("search_area_street_landmark"); ?>">
                                    </div>


                                    <div class="__dlrbtn" id="search_restaurant" searchtype="1">
                                        <?php echo $this->lang->line("delivery"); ?>  
                                    </div>



                                    <div class="__picuprbtn" id="search_restaurant22" searchtype="2">
                                       <?php echo $this->lang->line("self_pickup"); ?> 
                                    </div>




<!--<div class=" __btn_serch_search" id="search_restaurant"><img src="images/search_icon-w.png"> </div>-->

                                </div>
                            </div>
                            <span id="error_msg"></span>
                        </div>
                    </form>
                    <!--<p id="error_msg"></p>-->
                </div>

            </div>



        </div>
    </div>
</section>







<section class="topddealsl_wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wlc">
                <?php
                $sql = "SELECT * FROM `cms` WHERE identify='ABOUT'";
                $sql_qry = $this->Database->select_qry_array($sql);
                if ($this->session->userdata('language') == 'ar') {

                    if (!empty($sql_qry[0]->col1_ar)) {
                        $data = substr($sql_qry[0]->col1_ar, 0, 570);
                    } else {
                        $data = substr($sql_qry[0]->col1, 0, 570);
                    }
                } else {
                    $data = substr($sql_qry[0]->col1, 0, 570);
                }
                echo $data;
                ?>
                <!--<h1 id="page-two">Who Are We ?</h1>-->
                <!--<p>AlfaBee aims to provide its customers with the luxury of ordering what they desire at their fingertips. We aim to change the outlook of modernized delivery by providing its local consumers an effortless experience for placing their order ranging from their favorite food to essential items needed on a daily basis with the highest quality.  </p>-->
                <!--                    <a class="read_more" href="#">Read More...</a>-->
            </div>
        </div>
    </div>
</section>





<section class="our_services_wrap">
    <div class="container">
        <div class="row">
            <?php
            $store_type = Get_AllStoreType();
            foreach ($store_type as $dat) {
                ?>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 srv_box_wrap222">

                    <div class="srv_box" style="max-width:400px; margin: 0 auto;">
                        <div class="img"> <a href="<?= base_url('store_category/' . base64_encode($dat->id)); ?>"><img class="img-fluid" src="<?= base_url('uploads/store_type/' . $dat->image) ?>"></a> </div>
                        <div class="__btxtbtm">
                            <h3><a href="<?= base_url('store_category/' . base64_encode($dat->id)); ?>"><?= ($lan=='ar' ? $dat->store_type_ar : $dat->store_type)  ?></a></h3>
                            <p><a href="<?= base_url('store_category/' . base64_encode($dat->id)); ?>"><?= $dat->description ?></a></p>
                        </div>
                    </div>
                    </a>
                </div>
                <?php
            }
            ?>

            <!--<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 srv_box_wrap">-->
            <!--	<div class="srv_box">-->
            <!--		<div class="img"> <img class="img-fluid" src="images/Grocery.jpg"> </div>-->
            <!--		<div class="__btxtbtm">-->
            <!--			<h3>Grocery</h3>-->
            <!--			<p>One stop shop for all grocery items</p>-->
            <!--		</div>-->
            <!--	</div>-->
            <!--</div>-->


            <!--<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 srv_box_wrap">-->
            <!--	<div class="srv_box">-->
            <!--		<div class="img"> <img class="img-fluid" src="images/Medica_Stores.jpg"> </div>-->
            <!--		<div class="__btxtbtm">-->
            <!--			<h3>Medical Stores</h3>-->
            <!--			<p>Get your medicines delivered at any time</p>-->
            <!--		</div>-->
            <!--	</div>-->
            <!--</div>-->


            <!--<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 srv_box_wrap">-->
            <!--	<div class="srv_box">-->
            <!--		<div class="img"> <img class="img-fluid" src="images/Gifts.jpg"> </div>-->
            <!--		<div class="__btxtbtm">-->
            <!--			<h3>Gifts</h3>-->
            <!--			<p>Gifts from Perfume to Toys and many more at one place</p>-->
            <!--		</div>-->
            <!--	</div>-->
            <!--</div>-->


            <!--<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 srv_box_wrap">-->
            <!--	<div class="srv_box">-->
            <!--		<div class="img"> <img class="img-fluid" src="images/Dairy.jpg"> </div>-->
            <!--		<div class="__btxtbtm">-->
            <!--			<h3>Dairy</h3>-->
            <!--			<p>Get fresh and pure milk directly from the farms</p>-->
            <!--		</div>-->
            <!--	</div>-->
            <!--</div>-->


        </div>
    </div>
</section>

<section class="__become_partner" id="page-three">
    <!--<a href="#" data-toggle="modal" data-target="#exampleModal">-->
    <a href="<?= base_url() ?>partner_registration">
        <img class="img-fluid __lg_acvt" src="images/Become_a_Partner.jpg" />
        <img class="img-fluid __sm_acvt" src="images/Become_a_Partner_sm.jpg" />

    </a>

    <!-- Modal -->
    <div class="modal fade  __popup_become_partner" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div id="loader" style="display:none;"><img src="<?= base_url('images/loading.gif') ?>"/></div>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Become a Partner</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <p>If you are a business owner, you can join hands with us for mutual gains. Please fill below details and our representative will be in touch with you as soon as possible.</p>

                    <form class="needs-validation row" novalidate>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1">Owner Name</label>
                            <input type="text" class="form-control" id="name" autocomplete="off"   placeholder="" required>
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1">Business Name</label>
                            <input type="text" id="restaurant_name" class="form-control" autocomplete="off"   placeholder="" required >
                        </div>

                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1">Business Address</label>
                            <input type="hidden" id="lat" value="">
                            <input type="hidden" id="lng" value="">
                            <textarea class="form-control" id="business_location"   rows="2" autocomplete="off"></textarea>
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1">Type of Business</label>
                            <select class="form-control" id="business_type">
                                <option value="">Choose Business </option>
                                <?= get_storetype() ?>
                                <!--   <option value="Grocery">Grocery </option>-->
                                <!--<option value="Restaurants">Restaurants</option> -->
                                <!--<option value="Medical Store">Medical Store</option> -->
                                <!--<option value="Gifts">Gifts</option>-->
                                <!--<option value="Dairy">Dairy</option>-->
                                <!--<option value="Others">Others</option>-->
                            </select>

                        </div>
                        <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12 cuisine" style="display:none">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("cuisine_type"); ?></label><span class="required">*</span>
                            <select class="form-control selectpicker" id="primary_cuisine_type" name="primary_cuisine_type[]" multiple data-live-search="true">
                                <?= get_cuisine() ?>
                            </select>
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1">Are you delivering already?</label>
                            <select class="form-control" id="isDelivery">
                                <option value="1">Yes </option>
                                <option value="0">No</option> 
                            </select>
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputPassword1">Mobile Number</label>
                            <input type="number"  id="mobile_number" class="form-control"  autocomplete="off" placeholder=" " required>
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" class="form-control" id="email_id" autocomplete="off"  placeholder="" required>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                            <span class="common_message"></span>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                            <button type="button" class="btn btn-warning float-right" id="SubmitVendor"><b>Submit</b></button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>




</section>


<section class="__discover" id="Download">
    
        <div class="container">
        <div class="row">
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 __appvimg">
                   <img class="img-fluid "  src="images/app_new.jpg" /> 
               </div>
               
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 __apptext">
                   <h2><?php echo $this->lang->line("discover_new_app"); ?></h2>
                   <h5><?php echo $this->lang->line("what_you_need"); ?></h5>
                   <div class="w100">
                        <div class="d-flex flex-row ">
                       <div class=" ">
                         <a href="https://apps.apple.com/us/app/alfabee/id1536932609"  target=”_blank”><img class="img-fluid" src="images/ios_app.png" /> </a>
                        </div>
                         <div class=" ">
                       <a href="https://play.google.com/store/apps/details?id=com.app.alfabee"  target=”_blank”><img class="img-fluid" src="images/g_play.png" /> </a>
                       </div>
                       </div>
                   </div>
               </div>
            
        </div>
     </div>
    
</section>
<!--
<section class="__discover" id="Download">
    <a target="_blank" href="https://play.google.com/store/apps/details?id=com.app.alfabee"><img class="img-fluid __lg_acvt"  src="images/discover.jpg" /> 
        <img class="img-fluid __sm_acvt"  src="images/discover_sm.jpg" />
    </a>

</section>-->

<section class="__become_rider" id="become_rider">
    <a href="<?= base_url() ?>rider_registration">
        <!--<a href="#" data-toggle="modal" data-target="#exampleModal2">-->
        <img class="img-fluid __lg_acvt" src="images/become_rider.jpg" />
        <img class="img-fluid __sm_acvt" src="images/become_rider_sm.jpg" />
    </a>

    <!-- Modal -->
    <div class="modal fade  __popup_become_partner" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div id="loader_rider" style="display:none;"><img src="<?= base_url('images/loading.gif') ?>"/></div>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Become a Rider</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <p>If you have a bike and Android Phone, you can become a rider. Please fill the details below and start earning.</p>

                    <form class="needs-validation row" >


                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" class="form-control" id="rider_name" autocomplete="off"   placeholder="" required>
                        </div>


                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1">Father Name</label>
                            <input type="text" class="form-control" id="father_name" autocomplete="off"   placeholder="" required>

                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1">NIC #</label>
                            <input type="text" class="form-control" id="nic" autocomplete="off"   placeholder="" required>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1">Mobile Number</label>
                            <input type="number" class="form-control" id="phone_number"   placeholder="" autocomplete="off" required>

                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" class="form-control" id="email_address" autocomplete="off"  placeholder="" required>
                        </div>



                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1">Bike Type</label>
                            <select class="form-control" id="bike_type">
                                <option value="">Choose Bike Type </option>
                                <option value="Motor Bike">Motor Bike </option>
                                <option value="Bicycle">Bicycle</option> 
                                <option value="Loader Bike">Loader Bike</option> 
                            </select>

                        </div>


                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1">Are you 18years old or above? </label>
                            <select class="form-control" id="age">
                                <option value="1">Yes </option>
                                <option value="0">No</option> 
                            </select>

                        </div>

                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1">Do you have Learning permit or License for Bike?</label>
                            <select class="form-control" id="isLicense">
                                <option value="1">Yes </option>
                                <option value="0">No</option> 
                            </select>

                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                            <span class="common_msg"></span>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <button type="button" class="btn btn-warning float-right" id="SubmitRider"><b>Submit</b></button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


</section>


<!--NEW E-->

<section class="section inner_banner mainpg" style="display:none;">
    <div class="container">
        <div class="_top_search">
            <h1><?php echo $this->lang->line("what_would_do_you_like_to_eat_today"); ?></h1>
            <p><?php echo $this->lang->line("find_the_homely_food_nearest_to_you"); ?></p>
            <form method="POST" action="<?= base_url() ?>search_result" id="search_location">
                <div class="__serachbox">
                    <div class="row">

                        <div class="col-lg-10 col-md-10 col-sm-9 col-xs-12 tpsrfd">
                            <input type="hidden" value="" class="latitude" name="latitude">
                            <input type="hidden" value="" class="longitude" name="longitude">
                            <input type="hidden" value="" class="get_location" name="get_location">
                            <input type="hidden" value="" id="locality" name="locality" >
                            <input type="hidden" value="" id="administrative_area_level_1" name="administrative_area_level_1">
                            <input type="hidden" value="" id="sublocality_level_1" name="sublocality_level_1">
                            <input type="hidden" value="" id="store_type" name="store_type">
                                <!--<input type="text" class="form-control" id="location_name" aria-describedby="emailHelp" placeholder="Search for area, street name, landmark...">-->
                            <span class="delete-icon" onclick="clearRestSearchText()" style="display: none;"><i class="fas fa-times"></i></span>
                            <input type="text" class="form-control autocomplete" id="autocomplete" name="autocomplete" placeholder="<?php echo $this->lang->line("search_area_street_landmark"); ?>"  autocomplete="off"/>

                            <div class="pointer"> </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 tpsrfd">
                                <!--<div class="_btn_serch_search" onclick="searchrestaurant()"> <img src="images/search_icon-w.png" /> </div>-->
                            <div class="_btn_serch_search"><img src="images/search_icon-w.png" /> </div>
                        </div>

                    </div>
                </div>
            </form>
            <p id="error_msg"></p>
        </div>
    </div>
    <div class="container __bnrsubwrp">
        <div class="row justify-content-md-center">
            <div class="hvr-wobble-horizontal col-md-auto __bnrsubbx text-center"> <a href="<?= base_url('homely_foods') ?>"><img src="images/homely_food_icon.png"> <span class="wd100"><?php echo $this->lang->line("homely_food"); ?></span></a> </div>
            <div class="hvr-wobble-horizontal col-md-auto __bnrsubbx text-center"> <a href="<?= base_url('party_orders') ?>"><img src="images/party_orders.png"> <span class="wd100"><?php echo $this->lang->line("party_orders"); ?></span> </a></div>
            <div class="hvr-wobble-horizontal col-md-auto __bnrsubbx text-center"> <a href="<?= base_url('offers') ?>"><img src="images/offers.png"> <span class="wd100"><?php echo $this->lang->line("offers"); ?></span> </a></div>
        </div>
    </div>
</section>
<?php
$sql = "SELECT * FROM store_type WHERE status=0";
$Array = $this->Database->select_qry_array($sql);
?>
<section class="test" style="display:none;">
    <div class="container __bnrsubwrp">
        <div class="row justify-content-md-center">
            <?php
            foreach ($Array as $dat) {
                ?>
                <div class="flex flex-wrap">
                    <a href="<?= base_url('category/' . base64_encode($dat->id)); ?>" class="col-md-8 col-lg-8 col-xs-8 vertical-card">
                        <div class="img-card">
                            <div class="img-section vertical-restaurants">
                                <img src="<?= base_url('uploads/store_type/' . $dat->image) ?>"></div>
                            <div class="details-section"><div class="heading"><?= $dat->store_type ?></div>
                            </div>
                        </div>
                    </a>
                </div>
                <?php
            }
            ?>

        </div>
    </div>
</section>


<section class="section __ftrwrap" style="display:none;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12  pl-2 pr-2">
                <h3><?php echo $this->lang->line("featured_deals"); ?></h3>
                <div class="wd100 banner">
                    <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
                        <!--<ol class="carousel-indicators">-->
                        <!--	<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>-->
                        <!--	<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>-->
                        <!--	<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>-->
                        <!--</ol>-->
                        <div class="carousel-inner">
                            <?php
                            $Qry = "SELECT * FROM `featured_deals_image` WHERE status=0 and archive=0 and type=1";
                            $Slider = $this->Database->select_qry_array($Qry);
// foreach($Slider as $slide)
// {
                            for ($i = 0; $i < count($Slider); $i++) {
                                if ($i == 0) {
                                    $class = "active";
                                } else {
                                    $class = '';
                                }
                                if ($Slider[$i]->home_url != '') {
                                    $main_url = $Slider[$i]->home_url;
                                } else {
                                    $main_url = '';
                                }
                                ?>
                                <div class="carousel-item <?= $class ?>"><a href="<?= $main_url ?>"><img src="<?= base_url('uploads/homeslider/' . $Slider[$i]->home_image) ?>" class="d-block w-100"></a> </div>
                                <?php
                            }
                            ?>
        <!--<div class="carousel-item active"><img src="images/banner1.jpg" class="d-block w-100"> </div>-->
        <!--<div class="carousel-item"><img src="images/banner2.jpg" class="d-block w-100"> </div>-->
        <!--<div class="carousel-item"><img src="images/banner3.jpg" class="d-block w-100"> </div>-->
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pl-2 pr-2">
                <?php
                $Qry2 = "SELECT * FROM `featured_deals_image` WHERE status=0 and archive=0 and type=3";
                $Slider2 = $this->Database->select_qry_array($Qry2);
                for ($i = 0; $i < count($Slider2); $i++) {
                    if ($i == 0) {
                        $class = "active";
                    } else {
                        $class = '';
                    }
                    if ($Slider2[$i]->home_url != '') {
                        $sub_url = $Slider2[$i]->home_url;
                    } else {
                        $sub_url = '';
                    }
                    ?>
                    <div class="wd100 ftr_box1" style="margin-bottom:-19px;">
                        <a href="<?= $sub_url ?>">
                            <img class="img-fluid" src="<?= base_url('uploads/homeslider/' . $Slider2[$i]->home_image) ?>">
                        </a>
                    </div>
                    <?php
                }
                ?>
                <!--<div class="wd100 ftr_box2">-->
                       <!--<img class="img-fluid" src="images/ftr_box2.jpg">-->
                <!--</div>-->
            </div> 

        </div>
    </div>
</section>


<section class="section __scbk_1" style="display:none;">
    <div class="container">
        <div class="wd100 catprod">
            <!-- Swiper -->
            <div class="catpro">
                <div class="swiper-wrapper">
                    <?php
                    $Qry1 = "SELECT * FROM `featured_deals_image` WHERE status=0 and archive=0 and type=2";
                    $Slider1 = $this->Database->select_qry_array($Qry1);
                    for ($i = 0; $i < count($Slider1); $i++) {
                        if ($i == 0) {
                            $class = "active";
                        } else {
                            $class = '';
                        }
                        ?>
                        <div class="swiper-slide <?= $class ?>">
                            <?php
                            if ($Slider1[$i]->home_url != '') {
                                $url = $Slider1[$i]->home_url;
                            } else {
                                $url = '';
                            }
                            ?>
                            <div class="sldtabbox"><a href="<?= $url ?>"> <img class="img-fluid" src="<?= base_url('uploads/homeslider/' . $Slider1[$i]->home_image) ?>"></a> </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next catpro-next"></div>
            <div class="swiper-button-prev catpro-prev"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>

<section class="section __scbk_2 text-center" style="display:none;">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-lg-11">
                <?php
                $sql = "SELECT * FROM `cms` WHERE identify='ABOUT'";
                $sql_qry = $this->Database->select_qry_array($sql);
                echo $sql_qry[0]->col1;
                ?>
                <!--<h1>Welcome to Home Eats</h1>-->
                <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>-->

            </div>
        </div>
    </div>
</section>
<!--  <section>-->
<!--     <div class="container">-->
<!--	<div class="row justify-content-md-center">-->
<!--		<div class="col-lg-4 _tpapp_img">-->
<!--			<a class="col-md-8 col-lg-8 col-xs-8 vertical-card" ng-href="/uae/restaurants" href="/uae/restaurants">-->
<!--			    <div class="img-card"><div class="img-section vertical-restaurants">-->
<!--			        <img src="https://www.talabat.com/images/Talabat/vertical-restaurants.png"></div><div class="details-section"><div class="heading">Restaurant</div><div class="card-desc">Find deals, free delivery, and more from our restaurant partners.</div><div class="card-cta"> Explore</div></div></div></a>-->
<!--		</div>-->
<!--		<div class="col-lg-4 _tpapp">-->
<!--			ss-->
<!--		</div>-->
<!--	</div>-->
<!--</div>-->
<!--  </section>-->
<section class="section __scbk_3" style="display:none;">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-lg-4 _tpapp_img">
                <img class="" src="images/app.jpg">
            </div>
            <div class="col-lg-4 _tpapp">
                <h1>Get the Home Eats App</h1>
                <p>See menus and photos for nearby homely foods and bookmark your favorite places on the go</p>

                <div class="wd100">
                    <div class="store">
                        <a href="#"><img class="img-fluid" src="images/store-1.png"></a>
                    </div>
                    <div class="store">
                        <a href="#"><img class="img-fluid" src="images/store-2.png"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="party_venue_map" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <input type="hidden" value="" id="isEditing">
            <div class="modal-header modal-alt-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $this->lang->line("location"); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
            <div class="modal-body">
                <div class="srch-by-txt">
                    <input type="hidden" id="lat" value="">
                    <input type="hidden" id="lng" value="">
                    <input type="text" id='store-location' value="" placeholder="<?php echo $this->lang->line("search_road_landmark"); ?>" autocomplete="off" class="form-control">
                </div>
                <div class="map-wrapper-inner" id="map-page">
                    <div id="google-maps-box">
                        <div id="map" style="width:100%; height:300px;"></div>
                    </div>
                </div>



            </div>
            <div class="modal-footer">

                <div class="button-wrap wd100">
                    <button type="button" class="float-right btn btn-success confirm_location" data-dismiss="modal"><?php echo $this->lang->line("CONFIRM"); ?></button>
                    <?php
                    if (!empty($sessionUser)) {
                        $addressAdded = GetAllAddress($sessionUser->id);
                        if (!empty($addressAdded)) {
                            ?> 
                            <div class="__p_hry_wps    mb-3">
                                <h6>Location History</h6>
                                <?php
                                for ($i = 0; $i < count($addressAdded); $i++) {
                                    $dttt=$addressAdded[$i];
                                    ?>
                                    <div class="__p_hry">
                                        <i class="fa fa-location-arrow" aria-hidden="true"></i>  <a lat='<?= $dttt->latitude ?>' long='<?= $dttt->longitude ?>' adddres='<?= $dttt->address ?>' onclick="COMN.setHomeAutouserAddress(this)" href="javascript:void(0)"><?= $dttt->address ?></a>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php
                        }
                    }
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>
<div id="business_location_map" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <input type="hidden" value="" id="isEditing">
            <div class="modal-header modal-alt-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $this->lang->line("location"); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
            <div class="modal-body">
                <div class="srch-by-txt">
                    <input type="hidden" id="lat" value="">
                    <input type="hidden" id="lng" value="">
                    <input type="text" id='business-location' value="" placeholder="<?php echo $this->lang->line("search_road_landmark"); ?>" autocomplete="off" class="form-control">
                </div>
                <div class="map-wrapper-inner" id="map-page">
                    <div id="google-maps-box">
                        <div id="business-map" style="width:100%; height:300px;"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="button-wrap">
                    <button type="button" class="btn btn-success confirm_location" data-dismiss="modal"><?php echo $this->lang->line("CONFIRM"); ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
