<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
        <meta charset="utf-8">
        <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>images/favicons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>images/favicons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>images/favicons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>images/favicons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>images/favicons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>images/favicons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>images/favicons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>images/favicons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>images/favicons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url() ?>images/favicons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>images/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>images/favicons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>images/favicons/favicon-16x16.png">
        <title>live track</title>

    </head>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" style="background: none;">
        <style>
            html, body {
                height: 100%;
                margin: 0;
                padding: 0;
            }
            #map {

                width: 100%;
                height: 100%;
            }
            .sdcfsd {
                height: 100vh; /* For 100% screen height */
            }
            .alert{
                text-align: center;
            }
            #selected_map_canvas {
                height: 100%;
                margin: 0px;
                padding: 0px
            }
        </style>

        <?php
        $session_user = $this->session->userdata('UserLogin');
        $userId = !empty($session_user->id) ? $session_user->id : '';
        $orderId = !empty($_REQUEST['orderid']) ? base64_decode($_REQUEST['orderid']) : '';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => base_url('services/get_rider_live_location'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"user_id\":\"$userId\",\"order_id\":\"$orderId\"}",
            CURLOPT_HTTPHEADER => array(
                "authtoken: Alwafaa123",
                "cache-control: no-cache",
                "postman-token: a45e771b-c73b-8486-8292-1dd95ac33c20",
                "user-agents: com.alfabee.com"
            ),
        ));

        $data = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
            exit;
        }
        $data = json_decode($data, true);
        $response = !empty($data['response']) ? $data['response'] : '';
        if (empty($response['status'])) {
            ?>
            <div class="alert alert-primary" role="alert">
                <h4><strong><?= !empty($response['message']) ? $response['message'] : '' ?></strong></h4>
            </div>
        <?php }
        ?>
    </body>
    <?php
    $Controller = $this->router->fetch_class();
    $Method = $this->router->fetch_method();
    $segment3 = $this->uri->segment(3);
    ?>
    <script>
        var base_url = '<?= base_url(); ?>';
        var Controller = '<?= $Controller ?>';
        var Method = '<?= $Method ?>';
        var segment3 = '<?= $segment3 ?>';
    </script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?= base_url() ?>assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
    <script src="<?= base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
    <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css">
    <!--<script src="<?= base_url(); ?>js/common.js"></script>-->

    <link rel="stylesheet" href="<?= base_url() . 'css/' ?>AdminMain.css">
    <?php if (!empty($response['status'])) { ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&libraries=geometry"  ></script>
    <?php } ?>




    <!-- Async script executes immediately and must be after any DOM elements used in callback. -->
    <?php if (!empty($response['status'])) { ?>
        <style>
            #map{
                width: 100%;
                height: 100%;
            }
        </style>
        <input type="hidden" id="currentlatRider">
        <script>
        var data;
        function hdbd() {
            var settings = {
                "async": false,
                "crossDomain": true,
                "url": base_url + "services/get_rider_live_location",
                "method": "POST",
                "headers": {
                    "user-agents": "com.alfabee.com",
                    "authtoken": "Alwafaa123",
                    "cache-control": "no-cache",
                    "postman-token": "e353f3c4-e3fc-5670-9167-651b44894890"
                },
                "data": "{\"user_id\":\"<?= $userId ?>\",\"order_id\":\"<?= $orderId ?>\"}"
            }

            $.ajax(settings).done(function (response) {
                data = response;
                if (data.response.status == false) {
                    window.location = '';
                } else {
    //                        alert(data.result.rider_lat)
                }
                console.log(response);
            });
            var res = data.result;
            console.log('currentFromDB---->'+res.rider_lat+'HiddenRider-->'+$('#currentlatRider').val());
            if (res.rider_lat == $('#currentlatRider').val()) {
                return false;
            }
            $('#currentlatRider').val(res.rider_lat);
            var stops = [
                {"Geometry": {"Latitude": parseFloat(res.vendor_lat), "Longitude": parseFloat(res.vendor_long)}},
                {"Geometry": {"Latitude": parseFloat(res.rider_lat), "Longitude": parseFloat(res.rider_long)}},
                {"Geometry": {"Latitude": parseFloat(res.user_lat), "Longitude": parseFloat(res.user_long)}}




            ];

            var map = new window.google.maps.Map(document.getElementById("map"));

            // new up complex objects before passing them around
            var directionsDisplay = new window.google.maps.DirectionsRenderer({suppressMarkers: true});
            var directionsService = new window.google.maps.DirectionsService();

            Tour_startUp(stops);

            window.tour.loadMap(map, directionsDisplay);
            window.tour.fitBounds(map);

            if (stops.length > 1)
                window.tour.calcRoute(directionsService, directionsDisplay);
        }
        jQuery(function () {

            window.setInterval(function () {
                hdbd();
            }, 25000);
            hdbd();
        });

        function Tour_startUp(stops) {
            if (!window.tour)
                window.tour = {
                    updateStops: function (newStops) {
                        stops = newStops;
                    },
                    // map: google map object
                    // directionsDisplay: google directionsDisplay object (comes in empty)
                    loadMap: function (map, directionsDisplay) {
                        var myOptions = {
                            zoom: 13,
                            center: new window.google.maps.LatLng(51.507937, -0.076188), // default to London
                            mapTypeId: window.google.maps.MapTypeId.ROADMAP
                        };
                        map.setOptions(myOptions);
                        directionsDisplay.setMap(map);
                    },
                    fitBounds: function (map) {
                        var bounds = new window.google.maps.LatLngBounds();

                        // extend bounds for each record
                        jQuery.each(stops, function (key, val) {
                            var myLatlng = new window.google.maps.LatLng(val.Geometry.Latitude, val.Geometry.Longitude);
                            bounds.extend(myLatlng);
                        });
                        map.fitBounds(bounds);
                    },
                    calcRoute: function (directionsService, directionsDisplay) {
                        var batches = [];
                        var itemsPerBatch = 10; // google API max = 10 - 1 start, 1 stop, and 8 waypoints
                        var itemsCounter = 0;
                        var wayptsExist = stops.length > 0;

                        while (wayptsExist) {
                            var subBatch = [];
                            var subitemsCounter = 0;

                            for (var j = itemsCounter; j < stops.length; j++) {
                                subitemsCounter++;
                                subBatch.push({
                                    location: new window.google.maps.LatLng(stops[j].Geometry.Latitude, stops[j].Geometry.Longitude),
                                    stopover: true
                                });
                                if (subitemsCounter == itemsPerBatch)
                                    break;
                            }

                            itemsCounter += subitemsCounter;
                            batches.push(subBatch);
                            wayptsExist = itemsCounter < stops.length;
                            // If it runs again there are still points. Minus 1 before continuing to
                            // start up with end of previous tour leg
                            itemsCounter--;
                        }

                        // now we should have a 2 dimensional array with a list of a list of waypoints
                        var combinedResults;
                        var unsortedResults = [{}]; // to hold the counter and the results themselves as they come back, to later sort
                        var directionsResultsReturned = 0;

                        for (var k = 0; k < batches.length; k++) {
                            var lastIndex = batches[k].length - 1;
                            var start = batches[k][0].location;
                            var end = batches[k][lastIndex].location;

                            // trim first and last entry from array
                            var waypts = [];
                            waypts = batches[k];
                            waypts.splice(0, 1);
                            waypts.splice(waypts.length - 1, 1);

                            var request = {
                                origin: start,
                                destination: end,
                                waypoints: waypts,
                                travelMode: window.google.maps.TravelMode.DRIVING
                            };
                            (function (kk) {
                                directionsService.route(request, function (result, status) {
                                    if (status == window.google.maps.DirectionsStatus.OK) {

                                        var unsortedResult = {order: kk, result: result};
                                        unsortedResults.push(unsortedResult);

                                        directionsResultsReturned++;

                                        if (directionsResultsReturned == batches.length) // we've received all the results. put to map
                                        {
                                            // sort the returned values into their correct order
                                            unsortedResults.sort(function (a, b) {
                                                return parseFloat(a.order) - parseFloat(b.order);
                                            });
                                            var count = 0;
                                            for (var key in unsortedResults) {
                                                if (unsortedResults[key].result != null) {
                                                    if (unsortedResults.hasOwnProperty(key)) {
                                                        if (count == 0) // first results. new up the combinedResults object
                                                            combinedResults = unsortedResults[key].result;
                                                        else {
                                                            // only building up legs, overview_path, and bounds in my consolidated object. This is not a complete
                                                            // directionResults object, but enough to draw a path on the map, which is all I need
                                                            combinedResults.routes[0].legs = combinedResults.routes[0].legs.concat(unsortedResults[key].result.routes[0].legs);
                                                            combinedResults.routes[0].overview_path = combinedResults.routes[0].overview_path.concat(unsortedResults[key].result.routes[0].overview_path);

                                                            combinedResults.routes[0].bounds = combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getNorthEast());
                                                            combinedResults.routes[0].bounds = combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getSouthWest());
                                                        }
                                                        count++;
                                                    }
                                                }
                                            }
                                            directionsDisplay.setDirections(combinedResults);
                                            var legs = combinedResults.routes[0].legs;
                                            // alert(legs.length);
                                            for (var i = 0; i < legs.length; i++) {
                                                var markerletter = "A".charCodeAt(0);
                                                markerletter += i;
                                                markerletter = String.fromCharCode(markerletter);
                                                createMarker(directionsDisplay.getMap(), legs[i].start_location, "marker" + i, "some text for marker " + i + "<br>" + legs[i].start_address, markerletter);
                                            }
                                            var i = legs.length;
                                            var markerletter = "A".charCodeAt(0);
                                            markerletter += i;
                                            markerletter = String.fromCharCode(markerletter);
                                            createMarker(directionsDisplay.getMap(), legs[legs.length - 1].end_location, "marker" + i, "some text for the " + i + "marker<br>" + legs[legs.length - 1].end_address, markerletter);
                                        }
                                    }
                                });
                            })(k);
                        }
                    }
                };
        }
        var infowindow = new google.maps.InfoWindow(
                {
                    size: new google.maps.Size(150, 50)
                });

        var icons = new Array();
        icons["red"] = new google.maps.MarkerImage("mapIcons/marker_red.png",
                // This marker is 20 pixels wide by 34 pixels tall.
                new google.maps.Size(20, 34),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is at 9,34.
                new google.maps.Point(9, 34));



        function getMarkerImage(iconStr) {
            if ((typeof (iconStr) == "undefined") || (iconStr == null)) {
                iconStr = "red";
            }
            var ICONHD = "http://www.google.com/mapfiles/marker" + iconStr + ".png";
            if (iconStr == 'A') {
                ICONHD = 'https://maps.google.com/mapfiles/kml/shapes/grocery_maps.png';
            } else if (iconStr == 'B') {
                ICONHD = 'https://lh3.googleusercontent.com/-kkgKwDHw7hA/YDYwRq7zrII/AAAAAAAANMs/1xe7w0fX8zgHSrQB_bzJ8QvBLMBQVDpjwCK8BGAsYHg/s0/2021-02-24.png';
            } else if (iconStr == 'C') {
                ICONHD = 'https://lh3.googleusercontent.com/--Dx32FR-hMQ/YDYvBK3mX6I/AAAAAAAANMY/eLgvreOrqI8prnImAK-MYVY5-lLLCUurgCK8BGAsYHg/s0/2021-02-24.png';
            }
            if (!icons[iconStr]) {
                //                icons[iconStr] = new google.maps.MarkerImage(ICONHD,
                //                        // This marker is 20 pixels wide by 34 pixels tall.
                //                        new google.maps.Size(20, 34),
                //                        // The origin for this image is 0,0.
                //                        new google.maps.Point(0, 0),
                //                        // The anchor for this image is at 6,20.
                //                        new google.maps.Point(9, 34));
                icons[iconStr] = new google.maps.MarkerImage(ICONHD);
            }
            return icons[iconStr];

        }
        // Marker sizes are expressed as a Size of X,Y
        // where the origin of the image (0,0) is located
        // in the top left of the image.

        // Origins, anchor positions and coordinates of the marker
        // increase in the X direction to the right and in
        // the Y direction down.

        var iconImage = new google.maps.MarkerImage('mapIcons/marker_red.png',
                // This marker is 20 pixels wide by 34 pixels tall.
                new google.maps.Size(20, 34),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is at 9,34.
                new google.maps.Point(9, 34));
        var iconShadow = new google.maps.MarkerImage('http://www.google.com/mapfiles/shadow50.png',
                // The shadow image is larger in the horizontal dimension
                // while the position and offset are the same as for the main image.
                new google.maps.Size(37, 34),
                new google.maps.Point(0, 0),
                new google.maps.Point(9, 34));
        // Shapes define the clickable region of the icon.
        // The type defines an HTML &lt;area&gt; element 'poly' which
        // traces out a polygon as a series of X,Y points. The final
        // coordinate closes the poly by connecting to the first
        // coordinate.
        var iconShape = {
            coord: [9, 0, 6, 1, 4, 2, 2, 4, 0, 8, 0, 12, 1, 14, 2, 16, 5, 19, 7, 23, 8, 26, 9, 30, 9, 34, 11, 34, 11, 30, 12, 26, 13, 24, 14, 21, 16, 18, 18, 16, 20, 12, 20, 8, 18, 4, 16, 2, 15, 1, 13, 0],
            type: 'poly'
        };


        function createMarker(map, latlng, label, html, color) {
            // alert("createMarker("+latlng+","+label+","+html+","+color+")");
            var contentString = '<b>' + label + '</b><br>' + html;
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                shadow: iconShadow,
                icon: getMarkerImage(color),
                shape: iconShape,
                title: label,
                zIndex: Math.round(latlng.lat() * -100000) << 5
            });
            marker.myname = label;

            google.maps.event.addListener(marker, 'click', function () {
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            });
            return marker;
        }
        </script>
    <?php } ?>
</head>
<body>
    <div id="map"></div>

</html>