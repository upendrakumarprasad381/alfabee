<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
        <meta charset="utf-8">
        <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>images/favicons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>images/favicons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>images/favicons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>images/favicons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>images/favicons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>images/favicons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>images/favicons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>images/favicons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>images/favicons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url() ?>images/favicons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>images/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>images/favicons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>images/favicons/favicon-16x16.png">
        <title>live track</title>

    </head>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" style="background: none;">
        <style>
            html, body {
                height: 100%;
                margin: 0;
                padding: 0;
            }
            #map {

                width: 100%;
                height: 100%;
            }
            .sdcfsd {
                height: 100vh; /* For 100% screen height */
            }
            .alert{
                text-align: center;
            }
            #selected_map_canvas {
                height: 100%;
                margin: 0px;
                padding: 0px
            }
        </style>

        <?php
        $session_user = $this->session->userdata('UserLogin');
        $userId = !empty($session_user->id) ? $session_user->id : '';
        $orderId = !empty($_REQUEST['orderid']) ? base64_decode($_REQUEST['orderid']) : '';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => base_url('services/get_rider_live_location'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"user_id\":\"$userId\",\"order_id\":\"$orderId\"}",
            CURLOPT_HTTPHEADER => array(
                "authtoken: Alwafaa123",
                "cache-control: no-cache",
                "postman-token: a45e771b-c73b-8486-8292-1dd95ac33c20",
                "user-agents: com.alfabee.com"
            ),
        ));

        $data = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
            exit;
        }
        $data = json_decode($data, true);
        $response = !empty($data['response']) ? $data['response'] : '';
        if (empty($response['status'])) {
            ?>
            <div class="alert alert-primary" role="alert">
                <?= !empty($response['message']) ? $response['message'] : '' ?>
            </div>
            <?php
        } else {
            $result = !empty($data['result']) ? $data['result'] : '';
            $rider_lat = !empty($result['rider_lat']) ? $result['rider_lat'] : '32.5154258';
            $rider_long = !empty($result['rider_long']) ? $result['rider_long'] : '73.4915148';

            $user_lat = !empty($result['user_lat']) ? $result['user_lat'] : '32.51362480';
            $user_long = !empty($result['user_long']) ? $result['user_long'] : '73.50284850';
            ?>
            <div id="panel" style="display: none;">
                <input type="text" id="slectedLat" value="<?= $rider_lat ?>"></input>
                <input type="text" id="slectedLon" value="<?= $rider_long ?>"></input>
            </div>
            <div id="selected_map_canvas"></div>
            <div id="directions-panel"></div>
        <?php } ?>
    </body>
    <?php
    $Controller = $this->router->fetch_class();
    $Method = $this->router->fetch_method();
    $segment3 = $this->uri->segment(3);
    ?>
    <script>
        var base_url = '<?= base_url(); ?>';
        var Controller = '<?= $Controller ?>';
        var Method = '<?= $Method ?>';
        var segment3 = '<?= $segment3 ?>';
    </script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?= base_url() ?>assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
    <script src="<?= base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
    <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css">
    <!--<script src="<?= base_url(); ?>js/common.js"></script>-->

    <link rel="stylesheet" href="<?= base_url() . 'css/' ?>AdminMain.css">
    <?php if (!empty($response['status'])) { ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&libraries=geometry"  ></script>


        <script>

        var mylatitude = parseFloat('<?= $user_lat ?>');
        var mylongitude = parseFloat('<?= $user_long ?>');

        function GoogleMap_selected() {

            var lattitude_value = document.getElementById('slectedLat').value;
            var longitude_value = document.getElementById('slectedLon').value;

            var from = new google.maps.LatLng(mylatitude, mylongitude);
            var to = new google.maps.LatLng(lattitude_value, longitude_value);

            var directionsService = new google.maps.DirectionsService();
            var directionsRequest = {
                origin: from,
                destination: to,
                travelMode: google.maps.DirectionsTravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC
            };

            this.initialize = function () {
                var map = showMap_selected();

                directionsService.route(
                        directionsRequest,
                        function (response, status) {

                            if (status == google.maps.DirectionsStatus.OK) {
                                new google.maps.DirectionsRenderer({
                                    map: map,
                                    directions: response,
                                    suppressMarkers: true
                                });
                                var leg = response.routes[0].legs[0];
                                makeMarker(leg.start_location, icons.start, "title", map);
                                makeMarker(leg.end_location, icons.end, 'title', map);

                            } else {
                                alert("Unable to retrive route");
                            }

                        });

            }

            function makeMarker(position, icon, title, map) {
                new google.maps.Marker({
                    position: position,
                    map: map,
                    icon: icon,
                    title: title
                });
            }

            var icons = {
                start: new google.maps.MarkerImage(
                        // URL
                        'https://lh3.googleusercontent.com/-kkgKwDHw7hA/YDYwRq7zrII/AAAAAAAANMs/1xe7w0fX8zgHSrQB_bzJ8QvBLMBQVDpjwCK8BGAsYHg/s0/2021-02-24.png',
                        // (width,height)
                        new google.maps.Size(44, 32),
                        // The origin point (x,y)
                        new google.maps.Point(0, 0),
                        // The anchor point (x,y)
                        new google.maps.Point(22, 32)),
                end: new google.maps.MarkerImage(
                        // URL
                        'https://lh3.googleusercontent.com/--Dx32FR-hMQ/YDYvBK3mX6I/AAAAAAAANMY/eLgvreOrqI8prnImAK-MYVY5-lLLCUurgCK8BGAsYHg/s0/2021-02-24.png',
                        // (width,height)
                        new google.maps.Size(44, 32),
                        // The origin point (x,y)
                        new google.maps.Point(0, 0),
                        // The anchor point (x,y)
                        new google.maps.Point(22, 32))
            };


            var showMap_selected = function () {
                var mapOptions = {
                    zoom: 12,
                    center: new google.maps.LatLng(lattitude_value, longitude_value),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById("selected_map_canvas"), mapOptions);
                return map;
            };
        }

        function initialize() {
            var instance = new GoogleMap_selected();
            instance.initialize();
        }
setTimeout(function(){ 
    location.reload();
 }, 60000);
        google.maps.event.addDomListener(window, 'load', initialize);
        </script>
    <?php } ?>




    <!-- Async script executes immediately and must be after any DOM elements used in callback. -->



</html>