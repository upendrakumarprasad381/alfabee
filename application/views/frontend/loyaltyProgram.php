
<section class="section __sttcpg">
		<div class="container">
  
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
		<h2>Loyalty Program -B’sMiles </h2>
		

 
<p> 
  AlfaBee&rsquo;s loyalty program is applicable at AlfaBee  platform from all vendors present on this platform in Pakistan for all the  cities. The use AlfaBee platform means that you have unconditionally accepted  the Terms pertaining to the Platform and this loyalty program.</p>
<ol start="1"  >
  <li>Whenever you make a purchase on       AlfaBee, you will earn B&rsquo;sMiles against each purchase. For each Rs.10 spent       on AlfaBee platform, you will earn 1 B&rsquo;sMile.</li>
  <li>The B&rsquo;sMiles will be added to your       account every time you make a purchase on AlfaBee.</li>
  <li>You can redeem these B&rsquo;sMiles       against cash vouchers once accumulated sufficient enough and can be used       on AlfaBee platform as well as at our partner locations.</li>
  <li>During promotions, loyalty program       will not apply.</li>
</ol>
<p>You can access  to your gains, anytime, anywhere</p>
<ul>
  <li>Earn  B&rsquo;sMiles,</li>
  <li>Redeem  your coupons &amp; B&rsquo;sMiles discount,</li>
  <li>View  your B&rsquo;sMiles balance and,</li>
  <li>Check  currently running promotions anywhere conveniently</li>
</ul>
<h5>Membership</h5>
<p>  B&rsquo;sMile membership is free of charge and can be availed by  any person who can form a legally binding contracts under applicable laws (18  years and above).  <br>
To join B&rsquo;sMiles, the subscriber must be registered on AlfaBee&rsquo;s  website or through the Mobile application. AlfaBee has the right to disable the  loyalty account and all acquired benefits. B&rsquo;sMiles cannot be used for any  purposes other than those provided for in these terms and conditions. The B&rsquo;sMiles  may not be transferred or used by anyone other than its subscriber, for any  reason. 
</p>
<h5>  Earning</h5>
<p>  The members can earn points and vouchers. 1 point for  every 10 Rupees spent on qualifying purchases at AlfaBee Platform and stores in  Pakistan.<br>
  Members cannot earn points when purchasing the following  items: gift cards, pharmacy, Cigarettes, and services.</p>
<h5>Redemption</h5>
<p>When the member accumulates 4000 points, he will be  eligible to redeem them and get 100.00 Rs loyalty discount voucher.  <br>
  This 100.00 Rs loyalty discount voucher can be used  against the member&rsquo;s next purchase only in participating AlfaBee stores in Pakistan  for an amount greater or equal to 100.00 Rs by representing the discount voucher  along with the B&rsquo;sMiles number and within the expiry date specified on the  loyalty discount voucher.<br>
  The B&rsquo;sMiles can be redeemed. A minimum of 100.00 Rs  against 4000 B&rsquo;sMiles points can be redeemed as a discount at the checkout  online.<br>
The B&rsquo;sMiles discount voucher is nonrefundable and cannot  be exchanged for cash and no change will be given. Each 100.00 Rs loyalty  discount voucher is linked to the loyalty account which has received it and  cannot be used by another loyalty account.</p>
<h5>Claim and Refund</h5>
<p>  In case of a member contesting missing points, or the  points balance, the member has seven days to formulate his claim to concerned  store or to the customer call center.<br>
  In case of an item being refunded, the points earned from  this item will be deducted from the B&rsquo;sMiles account. If the B&rsquo;sMiles account  does not have sufficient number of points to deduct from the account, the  missing points number multiplied by the point value will be deducted from the  item refund.</p>
<h5>  Suspension</h5>
<p>  B&rsquo;sMiles is a laylty program managed by AlfaBee and loyalty  account&nbsp;can be immediately suspended or terminated at the sole discretion of  AlfaBee&nbsp;at any time if, a Member:<br>
  has breached these terms and conditions.<br>
  has behaved fraudulently.<br>
  has supplied false or misleading information to AlfaBee; <br>
  In this case the member will lose all the acquired  benefits and any right of use.<br>
  If the B&rsquo;sMiles account is not used during a continuous  period of 6 months,&nbsp;AlfaBee&nbsp;will disable the loyalty account and the member  will lose all the acquired benefits and will not be able to use his/her account  anymore.<br>
  If a member wishes to close his/her account, he/she could  do it with a written notification stipulating his/her demand, his/her name,  last name, B&rsquo;sMiles account number and signature.<br>
  If any member claims back his points after disabling his/her  card; AlfaBee can still return him/her these points by transferring them to  his/her new account.&nbsp;&nbsp;</p>
<h5>Responsibility</h5>
<p>  Alfabee&nbsp;is exempted from any responsibility for any  direct or indirect consequences linked to any malfunction; nevertheless it will  do the best to keep all the member&rsquo;s acquired benefits.<br>
  AlfaBee&nbsp;is exempted from any responsibility for any  direct or indirect consequences linked to any fraudulent use or non-accordance  use with these terms and conditions.</p>
<h5> 
  Terms and conditions </h5>
<p>  AlfaBee&nbsp;reserves the right to modify or cancel some  or all of these terms and conditions.<br>
  Any disputes or claims arising out of, or in connection  with, its subject matter or formation (including non-contractual disputes or  claims) are governed by and construed in accordance with the laws of Pakistan.</p>



		
	 </div>
	 	 </div>
</section>


<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

