<?php
    $slct_qry = "SELECT * FROM `most_selling`";
    $menu_array = $this->Database->select_qry_array($slct_qry);
?>
<section class="section _spg">
		<div class="container">
			<div class="row _rpad ">
				 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pl-2 pr-2">
					<h2><?php echo $this->lang->line("most_selling");?></h2>
				 </div>
				<?php
				if(!empty($menu_array[0]->menu_id))
				{
				// $menu = explode(',',$menu_array[0]->menu_id);
				foreach($menu_array as $menu_data)
				{
				    $qry = "SELECT menu_list.id,menu_list.menu_name,menu_list.image,restaurant_details.vendor_id,restaurant_details.restaurant_name FROM `menu_list` LEFT JOIN restaurant_details ON restaurant_details.vendor_id=menu_list.vendor_id WHERE menu_list.id=".$menu_data->menu_id;
                    $array = $this->Database->select_qry_array($qry);
                    foreach($array as $data)
                    {
                        if($data->image!='')
                        {
                            $image = base_url('uploads/menu/'.$data->image);
                        }else{
                            $image = base_url(DEFAULT_LOGO_MENU);
                        }
                     ?>  
                   
        				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center pl-2 pr-2">
        					<a href="<?=base_url('details/'.base64_encode($data->vendor_id))?>"><div class="wd100 __wtbx __msims">
        					<div class="__msims_img">
        						<img class="img-fluid" src="<?= $image?>" />
        					</div>
        						<h4><?= $data->menu_name?></h4>
        						<h6><?= $data->restaurant_name?></h6>
        					</div>
        					</a>
        				 </div>
				  
				
				<?php
				}
				}
				
				?>
			<?php
            }else{
            ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 pl-2 pr-2">
        		<div class="wd100  __msims">
        		    <p>No records found</p>
        			<div class="__msims_img">
        			    
        			    <img class="img-fluid" src="" />
        			    
        			</div>
        		</div>
        	</div>
            <?php
            }
            ?>
		</div>
		</div>
</section>