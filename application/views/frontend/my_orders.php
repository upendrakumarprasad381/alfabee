<?php
$session_user= $this->session->userdata('UserLogin');
$userdetails=GetUserDetails($session_user->id);
$orders=GetOrderByUserId($session_user->id);

?>
<style>


#review-form-container {
  width: 100%;
  /* background-color: #eee; */
  padding: 0 20px 26px;
  color: #333;
  overflow-y: auto;
}
#review-form-container h2 {
  margin: 0 0 0 6px;
}
#review-form {
  display: flex;
  flex-direction: column;
  background: #fff;
  border: 1px solid #e5e5e5;
  border-radius: 4px;
}
#review-form label, #review-form input {
  display: block;
  /* width: 100%; */
}
#review-form label {
  font-weight: bold;
  margin-bottom: 5px;
}

#review-form .rate label, #review-form .rate input,
#review-form .rate1 label, #review-form .rate1 input {
  display: inline-block;
}
/* Modified from: https://codepen.io/tammykimkim/pen/yegZRw */
.rate {
  /* float: left; */
  /* display: inline-block; */
  height: 36px;
  display: inline-flex;
  flex-direction: row-reverse;
  align-items: flex-start;
  justify-content: flex-end;
}
#review-form .rate > label {
  margin-bottom: 0;
  margin-top: -5px;
  height: 30px;
}
.rate:not(:checked) > input {
  /* position: absolute; */
  top: -9999px;
  margin-left: -24px;
  width: 20px;
  padding-right: 14px;
  z-index: -10;
}
.rate:not(:checked) > label {
  float:right;
  width:1em;
  overflow:hidden;
  white-space:nowrap;
  cursor:pointer;
  font-size:30px;
  color:#ccc;
}
/* #star1:focus{

} */
.rate2 {
  float: none;
}
.rate:not(:checked) > label::before {
  content: '★ ';
  position: relative;
  top: -10px;
  left: 2px;
}
.rate > input:checked ~ label {
  color: #ffc700;
  /* outline: -webkit-focus-ring-color auto 5px; */
}
.rate > input:checked:focus + label, .rate > input:focus + label {
  outline: -webkit-focus-ring-color auto 5px;
}
.rate:not(:checked) > label:hover,
.rate:not(:checked) > label:hover ~ label {
  color: #deb217;
  /* outline: -webkit-focus-ring-color auto 5px; */
}
.rate > input:checked + label:hover,
.rate > input:checked + label:hover ~ label,
.rate > input:checked ~ label:hover,
.rate > input:checked ~ label:hover ~ label,
.rate > label:hover ~ input:checked ~ label {
  color: #c59b08;
}
#submit-review {
  align-self: flex-end;
}  
</style>
<section class="section main_content_wrap __inner_nr dashboard">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
					<div class="wd100">
						<div class="wd100 _dsoutbox">
							<?php include('includes/dleftMenu.php'); ?>
						</div>
					</div>
				</div>
				<?php
				if(!empty($orders))
				{
			    ?>
				<div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
					<div class="wd100">
						<div class="wd100 _dsoutbox otext">
							<h4><?php echo $this->lang->line("my_orders");?></h4>
						 
							<?php
							foreach($orders as $ord)
							{
							    $date = date("M. d Y", strtotime($ord->date));
                                $time = date('h:i a' ,strtotime($ord->date));
                            ?>
                            <!--<a href="<?= base_url('order_history/' . base64_encode($ord->id)) ?>" class="order_history">-->
							<div class="row" >
							    <input type="hidden" id="order_time" value="<?= $ord->date?>">
								<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12  __ordfr" onclick="location.href='order_history/<?= base64_encode($ord->id)?>'" style="cursor: pointer;">
									<div class="wd100 _or_probox">
									    <?php
									    $image = GetNameById($ord->vendor_id,'users','image');
									    if($image!='')
									    {
									        $img = base_url().'uploads/vendor_images/'.$image;
									    }else{
									        $img = base_url().'images/default_rest.png';
									    }
									    ?>
										<div class="media"> <img class="img-fluid" src="<?= $img;?>">
											<div class="media-body">
												<div class="_or_probox_text col">
												    <?php
                									    $Qry = "SELECT restaurant_name  FROM `restaurant_details` WHERE vendor_id=$ord->vendor_id";
                                                        $Array = $this->Database->select_qry_array($Qry);
                									?>
													<h5><?= $Array[0]->restaurant_name?></h5>
													<!--<p>Add Hot And Sour Soup, Vegetable Chow Mein, Supreme Fried Noodles Shrimp And Chicken</p> <small><b>QTY</b> - 1</small> -->
													</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 __ordcr" onclick="location.href='order_history/<?= base64_encode($ord->id)?>'" style="cursor: pointer;">
									<div class="wd100 _or_id"> <b><?php echo $this->lang->line("order_id");?>:</b> <?= $ord->orderno?> </div>
									<div class="wd100 _or_time"> <b><?php echo $this->lang->line("date");?> | <?php echo $this->lang->line("time");?> :</b> <span><?= $time?></span> | <span><?= $date?></span> </div>
									
								</div>
								<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 __ordls">
									<?php
									if($ord->order_status== 1)
									{?>
									    <div class="wd100"> <b><?php echo $this->lang->line("order_status");?>  </b>
										    <br> <a href="" class="status __pending"><?php echo $this->lang->line("order_confirmed");?></a>
								        </div>
								    <?php }elseif($ord->order_status== 2)
									{?>
									    <div class="wd100"> <b><?php echo $this->lang->line("order_status");?>  </b>
										    <br> <a href="" class="status __pending">Preparing Order</a>
								        </div>
									<?php }elseif($ord->order_status== 5)
									{?>
									    <div class="wd100"> <b><?php echo $this->lang->line("order_status");?>  </b>
										    <br> <a href="" class="status __pending"><?php echo $this->lang->line("order_on_the_way");?></a>
								        </div>
									<?php }elseif($ord->order_status== 6)
									{?>
									<div class="wd100"> <b><?php echo $this->lang->line("order_status");?>  </b>
										<br> <a href="#" class="status __approval"><?php echo $this->lang->line("order_delivered");?></a>
								    </div>
			
									<?php }elseif($ord->order_status== 7)
									{?>
									  <div class="wd100"> <b><?php echo $this->lang->line("order_status");?>  </b>
										<br> <a href="#" class="status __rejected"><?php echo $this->lang->line("order_cancelled");?></a>
								    </div>
									
									<?php }
									?>
								</div>
								<?php
								$datetime1 = strtotime("now");
                                $datetime2 = strtotime($ord->date);
                                $interval  = abs($datetime2 - $datetime1);
                                $minutes   = round($interval / 60);
                               
			                
			                if($minutes<3 &&  $ord->order_status!=5 && $ord->order_status!=7){
			                    ?>
								<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 __ordls">
								    <span id="timer"></span>
								    </div>
							<?php
			                }
			                ?>
								<?php
								if($ord->order_status== ORDER_DELIVERED)
								{
								    // $Qry1 = "SELECT *  FROM `user_feedback` WHERE order_id=$ord->id";
            //                         $Array1 = $this->Database->select_qry_array($Qry1);
            //                         if(count($Array1)==0)
            //                         {
                                
								?>
								<!--<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 __ordls">-->
								<!--    <button type="submit" class="btn btn-success btn-sm submit_feedback" id="submit_feedback_<?= $ord->id?>" data-orderid = "<?= $ord->id?>" data-vendorid ="<?= $ord->vendor_id?>"><i class="fas fa-comments"></i> Review</button>-->
								<!--</div>-->
								<?php
								// }
								}
								?>
								<!--<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 __ordls">-->
								<!--	<div class="wd100"> <b>Order Status  </b>-->
								<!--		<br> <a href="" class="status __rejected">Cancel</a></div>-->
								<!--</div>-->
							</div>
							<!--</a>-->
							<?php
							}
							?>
					</div>
				</div>
			</div>
			<?php
			}else{
			  ?>
			  <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
					<div class="wd100">
						<div class="wd100 _dsoutbox otext text-center">
						    <img src="<?= base_url('images/empty-cart.svg')?>" width="70">
						    <p><?php echo $this->lang->line("no_orders_to_display");?></p>
						</div>
					</div>
			  </div>
			 <?php
			}
			?>
		</div>
		</div>

  
  <div id="myModal-review" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header modal-alt-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Feedback</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                        <div class="col-md-5">
                            <input type="hidden" value="" id="vendor_id">
                            <input type="hidden" value="" id="order_id">
                            <label>Rating</label>
                        </div>
                        <div class="col-md-5 rate">
                              <input type="radio" id="star5" name="rate" value="5" onkeydown="navRadioGroup(event)" onfocus="setFocus(event)" required="">
                              <label for="star5" title="5 stars">5 stars</label>
                              <input type="radio" id="star4" name="rate" value="4" onkeydown="navRadioGroup(event)">
                              <label for="star4" title="4 stars">4 stars</label>
                              <input type="radio" id="star3" name="rate" value="3" onkeydown="navRadioGroup(event)">
                              <label for="star3" title="3 stars">3 stars</label>
                              <input type="radio" id="star2" name="rate" value="2" onkeydown="navRadioGroup(event)">
                              <label for="star2" title="2 stars">2 stars</label>
                              <input type="radio" id="star1" name="https://codepen.io/pen/rate" value="1" onkeydown="navRadioGroup(event)" onfocus="setFocus(event)">
                              <label for="star1" title="1 star">1 star</label>
                        </div>
                        <div class="col-md-5">
                            <label for="reviewComments">Comments</label>
                        </div>
                        <div class="col-md-5">
                            <textarea name="reviewComments" id="reviewComments" cols="28" rows="5" required=""></textarea>
                        </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="button-wrap">
                            <button type="submit" class="btn btn-success" id="submit_review">Submit</button>
                            <button type="submit" class="btn secondary_button" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
         </div>
</section>
<script>
  
</script>