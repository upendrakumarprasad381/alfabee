<?php
$session_user= $this->session->userdata('UserLogin');
$userdetails=GetUserDetails($session_user->id);
$orders=GetPartyOrderByUserId($session_user->id);

?>
<section class="section main_content_wrap __inner_nr dashboard">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
					<div class="wd100">
						<div class="wd100 _dsoutbox">
							<?php include('includes/dleftMenu.php'); ?>
						</div>
					</div>
				</div>
				<?php
				if(!empty($orders))
				{
			    ?>
				<div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
					<div class="wd100">
						<div class="wd100 _dsoutbox otext">
							<h4><?php echo $this->lang->line("my_party_orders");?></h4>
						 
							<?php
							foreach($orders as $ord)
							{
							    $date = date("M. d Y", strtotime($ord->date));
                                $time = date('h:i a' ,strtotime($ord->date));
                            ?>
                            <!--<a href="<?= base_url('payment_invoice/'. base64_encode($ord->id))?>" class="order_history">-->
							<div class="row" >
							    
								<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12  __ordfr" onclick="location.href='payment_invoice/<?= base64_encode($ord->id)?>'" style="cursor: pointer;">
									<div class="wd100 _or_probox">
									    <?php
									    $image = GetNameById($ord->vendor_id,'users','image');
									    if($image!='')
									    {
									        $img = base_url().'uploads/vendor_images/'.$image;
									    }else{
									        $img = base_url().'images/default_rest.png';
									    }
									    ?>
										<div class="media"> <img class="img-fluid" src="<?= $img;?>">
											<div class="media-body">
												<div class="_or_probox_text col">
												    <?php
                									    $Qry = "SELECT restaurant_name  FROM `restaurant_details` WHERE vendor_id=$ord->vendor_id";
                                                        $Array = $this->Database->select_qry_array($Qry);
                									?>
													<h5><?= $Array[0]->restaurant_name?></h5>
													<!--<p>Add Hot And Sour Soup, Vegetable Chow Mein, Supreme Fried Noodles Shrimp And Chicken</p> <small><b>QTY</b> - 1</small> -->
													</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 __ordcr">
									<div class="wd100 _or_id"> <b><?php echo $this->lang->line("order_id");?>:</b> <?= $ord->party_orderno?> </div>
									<div class="wd100 _or_time"> <b><?php echo $this->lang->line("date");?> | <?php echo $this->lang->line("time");?> :</b> <span><?= $time?></span> | <span><?= $date?></span> </div>
									
								</div>
								<?php
								// if($ord->price!=0)
								// {
								?>
								
								<!--<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 __ordls">-->
								<!--	<div class="wd100"> <b><?php echo $this->lang->line("price");?></b>-->
								<!--		<br> -->
										<!--<?php echo $this->lang->line("aed");?>-->
										<!--<?= $ord->price?>-->
								<!--		</div>-->
								<!--</div>-->
								<?php
								// if($ord->payment_status==0 && $ord->price!=0)
								// {
								?>
								
								<!--<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 __ordls">-->
								<!--	<div class="wd100 "> <b> </b>-->
								<!--		<br> <a href="" class="status __rejected"><?php echo $this->lang->line("pay_now");?></a></div>-->
								<!--</div>-->
								<?php
								// }
								?>
								<?php
								// }
								?>
							</div>
							<!--</a>-->
							<?php
							}
							?>
					</div>
				</div>
			</div>
			<?php
			}else{
			  ?>
			  <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
					<div class="wd100">
						<div class="wd100 _dsoutbox otext text-center">
						    <img src="<?= base_url('images/empty-cart.svg')?>" width="70">
						    <p><?php echo $this->lang->line("no_orders_to_display");?></p>
						</div>
					</div>
			  </div>
			 <?php
			}
			?>
		</div>
		</div>
</section>