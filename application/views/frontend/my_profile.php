<?php
$session_user = $this->session->userdata('UserLogin');
$userdetails = GetUserDetails($session_user->id);
?>
<section class="section main_content_wrap __inner_nr dashboard">

    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                <div class="wd100">
                    <div class="wd100 _dsoutbox">
                        <?php include('includes/dleftMenu.php'); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
                <div class="wd100">
                    <div class="wd100 _dsoutbox __myprofile">
                        <input type="hidden" id="userid" value="<?= !empty($userdetails) ? $userdetails[0]->id : '' ?>">
                        <div class="profile-box">
                            <form method="post" enctype="multipart/form-data" id="change_profile">
                                    <!--<p class="no-t-img">PM</p>-->
                                <?php
                                if (isset($userdetails[0]->image) && $userdetails[0]->image != '') {
                                    $image = base_url() . 'uploads/user_images/' . $userdetails[0]->image;
                                } else {
                                    $image = base_url('images/profile-default.png');
                                }
                                ?>
                                    <!--<input type="hidden" prev_image="<?= !empty($userdetails) ? $userdetails[0]->image : '' ?>">-->
                                    <!--<div class="no-t-img"><img id="imgview" style="height: 182px;width: 182px;" src="<?= $image ?>"></div>-->
                                <!--<div class="pic-click-box js_ga_t-profile-upload-pic">-->
                                <!--	<input name="userpic" id="profile_dashboard" type="file" form_name="change_profile" class="change_profile_image" file_name="<?= !empty($userdetails) ? $userdetails[0]->image : '' ?>"> <i class="camera-icon"></i>-->
                                <!--</div>-->
                                <div class="no-t-img"><img id="imgview" style="height: 182px;width: 182px;" src="<?= $image ?>"></div>
                                <div class="pic-click-box js_ga_t-profile-upload-pic">
                                    <input type="file" prev_image="<?= !empty($userdetails) ? $userdetails[0]->image : '' ?>" form_name="change_profile" class="form-control change_profile_image" id="profile_dashboard" file_name="<?= !empty($userdetails) ? $userdetails[0]->image : '' ?>"  name="profile_dashboard" accept="image/*"><i class="camera-icon"></i>
                                </div>
                                <small class="__dsss"><?php echo $this->lang->line("800*800_pixel"); ?></small>
                                <div class="info-box text-center">
                                    <h4><?= $userdetails[0]->name ?></h4>
                                    <h5><?= $userdetails[0]->email ?></h5>
                                    <?php
                                    
                                  $creditAmt=  getloyalityTotalCreditPoint($session_user->id);
                                  
                                
                                  
                               
                                    ?>
                                    <h5><span style="color: #2cab2c;    font-weight: 700;">Loyalty Credit: <?= DecimalAmount($creditAmt) ?> Point</span> </h5>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                        <label><?php echo $this->lang->line("full_name"); ?></label>
                                        <input type="text" class="form-control" name="name" id="name" value="<?= $userdetails[0]->name ?>" placeholder="" autocomplete="off">
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                        <label><?php echo $this->lang->line("email"); ?></label>
                                        <input type="text" class="form-control" name="email" id="email" value="<?= $userdetails[0]->email ?>" placeholder="" autocomplete="off" disabled="">
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-12 form-group">
                                        <label for=""><?php echo $this->lang->line("phone_number"); ?></label>
                                        <input type="text" class="form-control"  name="mobile_code" value="+92" disabled="true">
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 form-group">
                                        <label class="" for="exampleFormControlSelect1"> &nbsp;</label>
                                        <input type="number" class="form-control" name="phone" aria-describedby="emailHelp" id="phone" value="<?= $userdetails[0]->mobile_number ?>" placeholder="" autocomplete="off"  >
                                    </div>
                                    <!--<div class="col-lg-4 col-md-6 col-sm-12 form-group">-->
                                    <!--	<label for="exampleFormControlSelect1"><?php echo $this->lang->line("country_region"); ?></label>-->
                                    <!--	<select class="form-control" id="country_id">-->
                                    <!--	<option value=""><?php echo $this->lang->line("select_country"); ?></option>-->
                                    <!--                                  <?= GetCountry(isset($userdetails[0]->country_id) ? $userdetails[0]->country_id : '' ); ?>-->
                                    <!--                                  </select>-->
                                    <!--</div>-->
                                    <div class="col-lg-2 col-md-6 col-sm-12 form-group">
                                        <label for="exampleFormControlSelect1"><?php echo $this->lang->line("gender"); ?></label>

                                        <select class="form-control" name="gender" id="gender">
                                            <option selected="" value="male" <?php echo ($userdetails[0]->gender == 'male') ? 'selected' : '' ?>><?php echo $this->lang->line("male"); ?></option>
                                            <option value="female" <?php echo ($userdetails[0]->gender == 'female') ? 'selected' : '' ?>><?php echo $this->lang->line("female"); ?></option>
                                        </select>
                                    </div>
                                    <!--<div class="col-lg-3 col-md-3 col-sm-12 form-group" style="display: none;">-->
                                    <!--	<label for="exampleFormControlSelect1">Date of Birth </label>-->
                                    <!--	<input type="text" class="form-control datepicker" value="20/05/1994" placeholder="" name="json[dateofbirth]" autocomplete="off">-->
                                    <!--</div>-->
                                    <!--<span class="profile_message"></span>-->
                                    <div class="col-lg-12 col-md-12 col-sm-12 form-group"><span class="profile_message float-right"></span></div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                        <button type="button" class="btn btn-primary float-right pl-5 pr-5 border-radius-0" id="UpdateProfile"><?php echo $this->lang->line("save"); ?></button>

                                    </div>
                                </div>
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


</section>