<?php
    $Qry = "SELECT users.id,users.image,restaurant_details.restaurant_name,GROUP_CONCAT(cuisine.cuisine_name SEPARATOR ', ')AS cuisine_name,promo_code.menu_id,promo_code.discount 
            FROM promo_code LEFT JOIN restaurant_details ON restaurant_details.vendor_id=promo_code.offer_addedby 
            LEFT JOIN users ON users.id=restaurant_details.vendor_id 
            LEFT JOIN restaurant_cuisine_type as rct on rct.vendor_id=users.id 
            LEFT JOIN cuisine on cuisine.cuisine_id=rct.cuisine_id 
            WHERE promo_code.user_type=2 AND promo_code.offer_type=1 AND promo_code.status=0 AND promo_code.archive=0
            AND promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE()
            GROUP BY users.id";
    $Array = $this->Database->select_qry_array($Qry);
    
?>
<section class="section _spg __homely_foods __offerpg">
		<div class="container">
		    <?php
		    if(count($Array)>0)
		    {
		    ?>
		    
			<div class="row _rpad ">
				 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pl-2 pr-2">
					<h2><?php echo $this->lang->line("offers");?></h2>
				 </div>
                <?php
                foreach($Array as $dat)
                {
                    if($dat->image!='')
                    {
                        $image = base_url().'uploads/vendor_images/'.$dat->image;
                    }else{
                        $image=base_url(DEFAULT_LOGO_RESTAURANT);
                    }
                    
                    if($this->session->userdata('language')=='ar')
			        {
			            if($dat->cuisine_name_ar!='')
			            {
			                $cuis = $dat->cuisine_name_ar;
			            }else{
			                $cuis = $dat->cuisine_name;
			            }
			        }else{
			             $cuis = $dat->cuisine_name;
			        }
                ?>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center pl-2 pr-2">
					 
						<div class="wd100 __wtbx __msims" data-toggle="modal" data-target="#offerModal-<?= $dat->id?>">
							<div class="__msims_img">
								<img class="img-fluid" src="<?= $image?>" />
							</div>
						<h4><?= $dat->restaurant_name?></h4>
						<h6><?= $cuis?></h6>
					</div> 
				 </div>
				 
				<?php
				    $sql = "SELECT * FROM menu_list WHERE id IN ($dat->menu_id) GROUP BY id ORDER BY id DESC";
				    $Array1 = $this->Database->select_qry_array($sql);
				?>
				<div class="__addadresspop modal fade bd-example-modal-lg show" id="offerModal-<?= $dat->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-modal="true">
						  <div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">

							  <div class="modal-header">
								<h5 class="modal-title" id="exampleModalLongTitle">Offers from <?= $dat->restaurant_name?></h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								  <span aria-hidden="true">×</span>
								</button>
							  </div>

							  <div class="modal-body">  
								  <div class="row">
									   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									   <?php
                                        foreach($Array1 as $d)
                                        {
                                            if($d->image!='')
                                            {
                                                $image = base_url('uploads/menu/'.$d->image);
                                            }else{
                                                $image = base_url(DEFAULT_LOGO_MENU);
                                            }
                                        ?>
										   <a href="<?= base_url('search_details/'.base64_encode($dat->id))?>" class="wd100 __itm_crtwp">
												<div class="media"> <img class="__itm_crtimg" src="<?= $image?>">
												<div class="media-body">
													<h3><?= $d->menu_name?></h3>
													<div class="wd100 __ctdcrp">
														<p><?= $d->description?></p>
													</div>
												</div>
												<?php
												if($d->price!=0)
												{
												    $offer = $d->price*$dat->discount/100;
												    $discount = $d->price-$offer;

												?>
												<div class="col-3 _cartcm">
													<div class="row">
														<div class="col p-0">
															<div class="wd100 _cartpris" style="text-decoration: line-through"> PKR <?= number_format($d->price,2)?> </div>
															<div class="wd100 _cartpris"> PKR <?= number_format($discount,2)?> </div>
														</div>
														
													</div>
												</div>
												
												<?php
												}
												else{
												?>
												<div class="col-3 _cartcm">
													<div class="row">
														<div class="col p-0">
															<div class="wd100 _cartpris"> Price on selection </div>
														</div>

													</div>
												</div>
												<?php
												}
												?>
											 </div>
										  </a>
										<?php
                                        }
                                        ?>

  
									  </div>
								  </div>

							  </div>
							</div>
							</div>
						</div>
 		        <?php
                }
                ?>
				
			</div>
			<?php
		    }else{
		       ?>
		       <div class="row _rpad ">
		           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pl-2 pr-2">
					<h2><?php echo $this->lang->line("offers");?></h2>
				 </div>
		           
				 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pl-2 pr-2">
				     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 pl-2 pr-2">
					 
						<div class="wd100 __msims" data-toggle="modal" data-target="#offerModal">
							<div class="__msims_img">
				     <p><?php echo $this->lang->line("no_offers");?></p>
				     </div>
				     </div>
				 </div>
			   </div>
		       <?php
		    }
		    ?>
		</div>
</section>
