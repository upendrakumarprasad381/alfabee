<?php
$segment2 = $this->uri->segment(2);
$order_id = base64_decode($segment2);
$session_user = $this->session->userdata('UserLogin');

$userdetails = GetUserDetails($session_user->id);

$order = GetOrderById($order_id);
$orderDeliverDate = getOrderdeliverydate($order_id);
if (empty($order)) {
    redirect(base_url());
}
$item_count = count($order);
$order_data = GetOrderByOrderId($order_id);
$rider = GetusersById($order[0]->assign_rider);
$Qry = "SELECT * FROM `restaurant_details` LEFT JOIN users ON users.id=restaurant_details.vendor_id WHERE `vendor_id`=" . $order[0]->vendor_id;
$name = $this->Database->select_qry_array($Qry);

$vendor = GetusersById($order[0]->vendor_id);


$saved_address = "SELECT * FROM `user_order_address` WHERE user_id=$session_user->id  ORDER BY id DESC LIMIT 1";
$address_details = $this->Database->select_qry_array($saved_address);

$Restaurant = GetRestaurantById($order[0]->vendor_id);
$Restaurant = !empty($Restaurant) ? $Restaurant[0] : '';

if ($order[0]->delivery_type == '1') {

    $request_map = [
        1 => $this->lang->line("order_confirmed"),
        2 => $this->lang->line("prepare_order"),
        14 => 'Ready for delivery',
        5 => $this->lang->line("order_on_the_way"),
        6 => $this->lang->line("order_delivered"),
        7 => $this->lang->line("order_cancelled"),
    ];
} else if ($order[0]->delivery_type == '2') {
    $request_map = [
        1 => 'Order Confirmed',
        2 => $this->lang->line("prepare_order"),
        8 => 'Ready for pickup',
        6 => $this->lang->line("order_delivered"),
        7 => $this->lang->line("order_cancelled"),
    ];
}
?>
<section class="section main_content_wrap __inner_nr dashboard">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                <div class="wd100">
                    <div class="wd100 _dsoutbox">
                        <?php include('includes/dleftMenu.php'); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">

                <div class="wd100 _dsoutbox orders_wrap cart_table_wrap">

                    <h3><a href="<?= base_url('my_profile') ?>"> <?php echo $this->lang->line("my_account"); ?></a>  
                        <i class="fas fa-chevron-right"></i> <?php echo $this->lang->line("my_orders"); ?>
                    </h3>
                    <div class="orders_blk">
                        <div class="orders_blk_topbra">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                &nbsp;<span style="color: black;font-size: 15px;font-weight: 600;"><?= $name[0]->restaurant_name ?> - <?= $order[0]->orderno ?></span>
                                <?php
                                if ($order[0]->order_status == '5') {
                                    ?>
                                    <span style="color: black;font-size: 15px;font-weight: 600;float: right;"><a target="_black" href="<?= base_url('live-tracking?orderid=' . base64_encode($order_id)) ?>">Live Tracking <img src="https://www.pngjoy.com/pngm/132/2667990_live-icon-live-tracking-icon-transparent-png.png" height="15"></a></span>
                                    <?php
                                }
                                ?>
                            </div> 

                        </div>

                        <div class="clearfix"></div>
                        <div class="table-responsive">

                            <table  class="table"> 
                                <tr>
                                    <th align="left" valign="middle" scope="col"><?php echo $this->lang->line("items"); ?></th>
                                    <th align="left" valign="middle" scope="col"></th>
                                    <th align="center" valign="middle" scope="col" style="text-align:center;"><?php echo $this->lang->line("quantity"); ?></th>
                                    <th align="center" valign="middle" scope="col" style="text-align:center;"><?php echo $this->lang->line("price"); ?></th>
                                    <th align="center" valign="middle" scope="col" style="text-align:center;"><?php echo $this->lang->line("total"); ?></th>
                                </tr>
                                <?php
                                foreach ($order as $ord) {
                                    ?>
                                    <tr>
                                        <?php
                                        if ($this->session->userdata('language') == 'ar') {
                                            $align = 'right';
                                        } else {
                                            $align = 'left';
                                        }
                                        ?>
                                        <td align="<?php echo $align ?>" valign="top"><strong><?= $ord->product_name ?></strong>
                                            <p>
                                                <?php
                                                $size = explode(',', $ord->size);
                                                $addon = explode(',', $ord->add_on);
                                                $topping = explode(',', $ord->topping);
                                                $drink = explode(',', $ord->drink);
                                                $dip = explode(',', $ord->dip);
                                                $side = explode(',', $ord->side);

                                                if (isset($size) && $size[0] != '') {
                                                    foreach ($size as $s) {
                                                        $size_qry = "SELECT menu_size FROM `menu_sizes` WHERE `menu_size_id`=" . $s;
                                                        $size_data = $this->Database->select_qry_array($size_qry);
                                                        echo $size_data[0]->menu_size . ',';
                                                    }
                                                }
                                                if (isset($addon) && $addon[0] != '') {
                                                    foreach ($addon as $a) {
                                                        $addon_qry = "SELECT add_on FROM `menu_addons` WHERE `menu_addon_id`=" . $a;
                                                        $addon_data = $this->Database->select_qry_array($addon_qry);
                                                        echo $addon_data[0]->add_on . ',';
                                                    }
                                                }
                                                if (isset($topping) && $topping[0] != '') {
                                                    foreach ($topping as $t) {
                                                        $topping_qry = "SELECT topping FROM `menu_topping` WHERE `menu_topping_id`=" . $t;
                                                        $topping_data = $this->Database->select_qry_array($topping_qry);
                                                        echo $topping_data[0]->topping . ',';
                                                    }
                                                }
                                                if (isset($drink) && $drink[0] != '') {
                                                    foreach ($drink as $d) {
                                                        $drink_qry = "SELECT drink_name FROM `menu_drink` WHERE `menu_drink_id`=" . $d;
                                                        $drink_data = $this->Database->select_qry_array($drink_qry);
                                                        echo $drink_data[0]->drink_name . ',';
                                                    }
                                                }
                                                if (isset($dip) && $dip[0] != '') {
                                                    foreach ($dip as $di) {
                                                        $dip_qry = "SELECT dips FROM `menu_dips` WHERE `menu_dip_id`=" . $di;
                                                        $dip_data = $this->Database->select_qry_array($dip_qry);
                                                        echo $dip_data[0]->dips . ',';
                                                    }
                                                }
                                                if (isset($side) && $side[0] != '') {
                                                    foreach ($side as $s) {
                                                        $side_qry = "SELECT side_dish FROM `menu_side` WHERE `menu_side_id`=" . $s;
                                                        $side_data = $this->Database->select_qry_array($side_qry);
                                                        echo $side_data[0]->side_dish . ',';
                                                    }
                                                }
                                                ?>
                                            </p>
                                            <!--<p>Barbeque Sauce, Ground Beef, Bold Bbq, Garlic Parmesan</p></td>-->
                                        <td align="left" valign="top"></td>
                                        <td align="center" valign="middle"><?= $ord->quantity ?></td>
                                        <td align="center" valign="middle"><?php echo $this->lang->line("aed"); ?> <?= number_format($ord->price, 2) ?></td>
                                        <td align="center" valign="middle"><?php echo $this->lang->line("aed"); ?> <?= number_format($ord->subtotal, 2) ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>

                                <tr>
                                    <td colspan="4" align="right" valign="top"> 
                                        <?php echo $this->lang->line("sub_total"); ?>
                                    </td>
                                    <td align="center" valign="middle"><?php echo $this->lang->line("aed"); ?> <?= number_format($order_data[0]->price, 2) ?></td>
                                </tr>
                                <?php if (!empty($order_data[0]->discount)) { ?>
                                    <tr>
                                        <td colspan="4" align="right" valign="top">Discount</td>
                                        <td align="center" valign="middle"><?php echo $this->lang->line("aed"); ?> <?= number_format($order_data[0]->discount, 2) ?></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="4" align="right" valign="top"><?php echo $this->lang->line("delivery_charge"); ?></td>
                                    <td align="center" valign="middle"><?php echo $this->lang->line("aed"); ?> <?= number_format($order_data[0]->service_charge, 2) ?></td>
                                </tr>
                                <?php
                                if (!empty($order_data[0]->loyality_discount)) {
                                    ?>
                                    <tr>
                                        <td colspan="4" align="right" valign="top">Loyality Discount</td>
                                        <td align="center" valign="middle"><?php echo $this->lang->line("aed"); ?> <?= number_format($order_data[0]->loyality_discount, 2) ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                <tr>
                                    <td colspan="4" align="right" valign="top"><strong><?php echo $this->lang->line("total_amount"); ?></strong></td>
                                    <td align="center" valign="middle"><strong><?php echo $this->lang->line("aed"); ?> <?= number_format($ord->total_price + $order_data[0]->service_charge, 2) ?></strong></td>
                                </tr>
                            </table>


                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 pops">   
                                <div class="progress-bar-wrapper">
                                    <!--<div class="status-bar" style="width: 80%;">-->
                                    <!--	<div class="current-status" style="transition: width 0s linear -1500ms;"></div>-->
                                    <!--</div>-->
                                    <!--<ul class="progress-bar">-->
                                    <!--	<li class="section" style="width: 20%;">Order Received</li>-->
                                    <!--	<li class="section" style="width: 20%;">Order confirmed</li>-->
                                    <!--	<li class="section" style="width: 20%;">Assigned to courier</li>-->
                                    <!--	<li class="section" style="width: 20%;">On the way</li>-->
                                    <!--	<li class="section" style="width: 20%;">Delivered</li>-->
                                    <!--</ul>-->
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="orders_blk">
                        <div class="info_orders">

                          



                            <div class="row">
                                <input type="hidden" id="order_time" value="<?= $order[0]->date ?>">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-xs-12">
<?php
$date = date("M d-Y", strtotime($order[0]->date));
$time = date('h:i a', strtotime($order[0]->date));
?>
                                    <h4><?php echo strtoupper($this->lang->line("order_details")); ?></h4> 
                                    <p><?php echo $this->lang->line("order_id"); ?>: <?= $order[0]->orderno ?> (<?= $item_count ?>)</p>
                                    <p><?php echo $this->lang->line("order_date"); ?>: <?= $date ?> <?= $time ?></p>
                                    <p><?php echo $this->lang->line("total_amount"); ?>: <?php echo $this->lang->line("aed"); ?> <?= number_format($ord->total_price + $order_data[0]->service_charge, 2) ?></p>
                                    <p><?php echo $this->lang->line("mode_of_payment"); ?>:
<?php
if ($ord->mode_of_payment == 1) {
    echo $this->lang->line("credit_card");
} elseif ($ord->mode_of_payment == 2) {
    echo $this->lang->line("visa_checkout");
} else {
    echo $this->lang->line("cash_on_delivery");
}
?>
                                    </p>
                                        <?php if (!empty($orderDeliverDate)) { ?>
                                        <p><?php echo $this->lang->line("date_of_deliver"); ?>: <?= $orderDeliverDate ?></p>
                                    <?php } ?>
                                </div>

                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-xs-12">
<?php if ($order[0]->delivery_type != '2') { ?>
                                        <h4><?php echo strtoupper($this->lang->line("address")); ?></h4> 
                                        <p><?= $address_details[0]->address ?><br>

    <?= $address_details[0]->mobile_code ?>  <?= $address_details[0]->mobile_number ?>                    
                                        </p>
                                        <?php }if ($order[0]->delivery_type == '2') { ?>
                                        <h4>Pickup Address </h4> 
                                        <p><?= $vendor->area ?><br>

    <?= $vendor->mobile_code ?>  <?= $vendor->mobile_number ?>                    
                                        </p>
                                        <?php } ?>
                                </div>

                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-xs-12 manroredr">
                                    <h4><?php echo strtoupper($this->lang->line("manage_order")); ?></h4>
                                    <!--<p style="display: none;"><b><a href="#"><i class="fas fa-file-alt"></i>  REQUEST INVOICE</a></b></p>-->
                                    <p><b><a href="<?= base_url('contact_us') ?>"><i class="fas fa-question-circle"></i> <?php echo strtoupper($this->lang->line("need_help")); ?></a></b></p>
<?php
$datetime1 = strtotime("now");
$datetime2 = strtotime($order[0]->date);
$interval = abs($datetime2 - $datetime1);
$minutes = round($interval / 60);


if ($minutes < 3) {
    ?>
                                        <?php if (empty($order[0]->order_status)) { ?>
                                            <p><b><a href="javascript:void(0)" OrderId="<?= $order_id ?>" id="CancelledMyOrder"><i class="fas fa-ban"></i> <?php echo strtoupper($this->lang->line("cancel_order")); ?></a></b></p>
                                            <p><span id="timer"></span></p>
        <?php
    }
}
?>
                                </div>
                            </div>
                            
                            
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-12">
                                    <?php
                                    if ($order[0]->delivery_type != '2' && $vendor->store_type == '1') {
                                        $delivery_time = !empty($Restaurant->delivery_time) ? $Restaurant->delivery_time : '0';
                                        $distance = $order[0]->distance_ord; 
                                        $delvTimeexpt = $distance * RIDER_DELIVER_TIME_PER_KM;
                                        ?>
                                        <h4>Order Delivery In: <?= convertToHoursMins($delivery_time + $delvTimeexpt, '%02d hours %02d minutes') ?></h4>
                                    <?php }
                                    if(!empty($rider)){
                                        ?>
                                        <h5>Rider Details</h5>
                                        <p>
                                            Name : <?= $rider->name ?><br>
                                            Email: <?= $rider->email ?><br>
                                            Phone: +<?= $rider->mobile_code ?><?= $rider->mobile_number ?><br>
                                        </p>
                                            <?php
                                    }
                                    ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>       

            </div>
        </div>

    </div>
</div>
</div>
</div>
</div>
</section>
<style>
    .pops ul.progress-bar{
        background-color:#FFF !important;

    }
</style>
<?php
if (!empty($request_map[$order[0]->order_status])) {
    $status = $request_map[$order[0]->order_status];
} else {
    $status = 'pending';
}
?>
<script>
    var status = '<?php echo $status ?>';
    var store_type = '<?php echo $name[0]->store_type ?>';
    var delivery_type = '<?php echo $order[0]->delivery_type ?>';
    if (status == 'Order Cancelled')

    {
        var ProgressBarValue = '{"OrderStatus":"<?php echo $status; ?>","OrderNode":["<?php echo $this->lang->line('order_cancelled') ?>"]}';
    } else {

        if (delivery_type == '1') {

            var ProgressBarValue = '{"OrderStatus":"<?php echo $status; ?>","OrderNode":["<?php echo $this->lang->line('order_confirmed') ?>","<?php echo $this->lang->line('prepare_order') ?>","Ready for delivery","<?php echo $this->lang->line('order_on_the_way') ?>","<?php echo $this->lang->line('order_delivered') ?>"]}';

        } else {
            var ProgressBarValue = '{"OrderStatus":"<?php echo $status; ?>","OrderNode":["<?php echo $this->lang->line('order_confirmed') ?>","<?php echo $this->lang->line('prepare_order') ?>","Ready for pickup","<?php echo $this->lang->line('order_delivered') ?>"]}';
        }
    }

// var ProgressBarValue = '{"OrderStatus":"<?php echo $status; ?>","OrderNode":["Order Received","Order Confirmed","Assigned to deliveryboy","Order On the way","Order Delivered"]}'; 
</script>
