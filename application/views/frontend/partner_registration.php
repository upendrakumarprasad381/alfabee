<style>
    #loader {      
        display: none;      
        position: fixed;      
        z-index: 999;      
        height: 2em;      
        width: 2em;      
        overflow: show;      
        margin: auto;      
        top: 0;      
        left: 0;      
        bottom: 0;      
        right: 0;    
    }    
    #loader:before {      
        content: '';      
        display: block;      
        position: fixed;      
        top: 0;      
        left: 0;      
        width: 100%;      
        height: 100%;      
        background-color: rgba(0,0,0,0.3);    
    }
    .location_icon {
        padding: 12px;
        position: absolute;
        right: 24px;
        z-index: 5;
        border-radius: 1px;
        box-shadow: 0 1px 4px -1px rgba(0,0,0,.3);
        border-radius: 2px;
        background-color: #fff;
        bottom: 207px;
    }
    .locate {
        width: 16px;
        height: 16px;

    }
    .location_icon img {
        cursor:pointer;
    }
</style>
<section class="section __signwrap">
    <div class="container">
        <div class="inner_pg_blok">
            <div class="row">
                <div id="loader" style="display:none;"><img src="<?= base_url('images/loading.gif') ?>"/></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login_box">
                    <h3><?php echo $this->lang->line("become_a_partner"); ?></h3>
                    <p><?php echo $this->lang->line("IF_YOU_ARE_A_BUSINESS_OWNER"); ?></p> 
                    <form class="needs-validation row" novalidate>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("OWNER_NAME"); ?></label>
                            <input type="text" class="form-control" id="name" autocomplete="off"   placeholder="" required>
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("restaurant_name"); ?></label>
                            <input type="text" id="restaurant_name" class="form-control" autocomplete="off"   placeholder="" required >
                        </div>

                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("BUSINESS_ADDRESS"); ?></label>
                        
                            <textarea class="form-control" id="business_location"   rows="2" autocomplete="off"></textarea>
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("TYPE_OF_BUSINESS"); ?></label>
                            <select class="form-control" id="business_type">
                                <option value=""><?php echo $this->lang->line("CHOOSE_BUSINESS"); ?> </option>
                                <?= get_storetype() ?>
                                <!--   <option value="Grocery">Grocery </option>-->
                                <!--<option value="Restaurants">Restaurants</option> -->
                                <!--<option value="Medical Store">Medical Store</option> -->
                                <!--<option value="Gifts">Gifts</option>-->
                                <!--<option value="Dairy">Dairy</option>-->
                                <!--<option value="Others">Others</option>-->
                            </select>

                        </div>
                        <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12 cuisine" style="display:none">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("cuisine_type"); ?></label><span class="required">*</span>
                            <select class="form-control selectpicker" id="primary_cuisine_type" name="primary_cuisine_type[]" multiple data-live-search="true">
                                <?= get_cuisine() ?>
                            </select>
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("ARE_YOU_DELIVERING"); ?></label>
                            <select class="form-control" id="isDelivery">
                                <option value=""><?php echo $this->lang->line("CHOOSE_DELIVERING_OPTION"); ?></option>
                                <option value="1"><?php echo $this->lang->line("yes"); ?> </option>
                                <option value="0"><?php echo $this->lang->line("no"); ?></option> 
                            </select>
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 delivery_optionXXXX" style="display:none">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("delivery_charge"); ?></label>
                            <input type="number" id="service_charge" class="form-control" autocomplete="off"   placeholder="" required >
                        </div> 
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("DELIVERING_RADIUS"); ?></label>
                            <div class="form-group row __nubrline">
                                <input type="text" class="col-8 form-control _nodropfild" placeholder="" id="delivery_radius" autocomplete="off"> 
                                <input type="text" class="col " name="radius" id="radius" value="km" disabled="true">
                            </div>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputPassword1"><?php echo $this->lang->line("mobile_number"); ?></label>
                            <div class="form-group row __nubrline">

                                <input type="text" class="col " name="mobile_code" id="mobile_code" value="92" disabled="true">

                                <input type="text" class="col-8 form-control _nodropfild" placeholder="" id="mobile_number" autocomplete="off"> </div>
<!--<input type="number"  id="mobile_number" class="form-control"  autocomplete="off" placeholder=" " required>-->
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("email_id"); ?></label>
                            <input type="email" class="form-control" id="email_id" autocomplete="off"  placeholder="" required>
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("password"); ?></label>
                            <input type="password" id="password" class="form-control" autocomplete="off"   placeholder="" required >
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("confirm_password"); ?></label>
                            <input type="password" id="cpassword" class="form-control" autocomplete="off"   placeholder="" required >
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                            <span class="common_message"></span>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                            <button type="button" class="btn btn-warning float-right" id="SubmitVendor"><b><?php echo $this->lang->line("submit"); ?></b></button>
                        </div>
                    </form>
                </div> 
            </div>
        </div>
    </div>
</section>
<div id="business_location_map" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <input type="hidden" value="" id="isEditing">
            <div class="modal-header modal-alt-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $this->lang->line("location"); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
            <div class="modal-body">
                <div class="srch-by-txt">
                    <input type="hidden" id="lat" value="">
                    <input type="hidden" id="lng" value="">
                    <input type="text" id='business-location' value="" placeholder="<?php echo $this->lang->line("search_road_landmark"); ?>" autocomplete="off" class="form-control">
                </div>
                <div class="map-wrapper-inner" id="map-page">
                    <div id="google-maps-box">
                        <div class="map-icons location_icon"><img ng-src="<?= base_url() ?>images/locate-mp.svg" class="noDrag locate cursor" onclick="getLocation()" width="45" height="45" src="<?= base_url() ?>images/locate-mp.svg" title="Current Location"></div>
                        <div id="map" style="width:100%; height:300px;"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="button-wrap">
                    <button type="button" class="btn btn-success confirm_location" data-dismiss="modal"><?php echo $this->lang->line("CONFIRM"); ?></button>
                </div>
            </div>
        </div>
    </div>
</div>