<section class="section _spg __homely_foods">
		<div class="container">
			<div class="row _rpad ">
				 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pl-2 pr-2">
					<h2><?php echo $this->lang->line("party_orders");?></h2>
				 </div>
                <?php
                foreach($homely_food_users as $users)
                {
                    if($users->image!='')
                    {
                        $image = base_url().'uploads/vendor_images/'.$users->image;
                    }else{
                        $image=base_url('images/default_rest.png');
                    }
                ?>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center pl-2 pr-2">
					<a href="<?=base_url('search_details/'.base64_encode($users->id))?>"><div class="wd100 __wtbx __msims">
					<div class="__msims_img">
						<img class="img-fluid" src="<?= $image?>" />
					</div>
						<h4><?= $users->restaurant_name?></h4>
						<h6><?= $users->cuisine_name?></h6>
					</div>
					</a>
				</div>
				<?php
                }
                ?>
			</div>
		</div>
</section>