
<?php
$this->session->unset_userdata('CartData');
$this->session->unset_userdata('cart_count');

$session_arr = $this->session->userdata('UserLogin');

$Qry1 = "SELECT * FROM `orders` WHERE user_id='$session_arr->id' ORDER BY id DESC LIMIT 1";
$Array = $this->Database->select_qry_array($Qry1);
if (empty($Array)) {
    redirect(base_url());
}
$date = date('Y-m-d', strtotime($Array[0]->date));
$time = date('H:i', strtotime($Array[0]->date));

$datetime1 = strtotime("now");
$datetime2 = strtotime($Array[0]->date);
$store_type = !empty($Array[0]->store_type) ? $Array[0]->store_type : '';
$interval = abs($datetime2 - $datetime1);
$minutes = round($interval / 60);

if (!empty($Array)) {
    $Restaurant = GetRestaurantById($Array[0]->vendor_id);
    $Restaurant = !empty($Restaurant) ? $Restaurant[0] : '';
    $address_details = GetuseraddressBy($Array[0]->delivery_address);
    $vendor = GetusersById($Array[0]->vendor_id);
    $msgOrderDelv = "";
    $res = GetOrderSubmitResponce($Array[0]->id);
    $msgOrderDelv = !empty($res['msgOrderDelv']) ? $res['msgOrderDelv'] : '';
}
?>
<section class="section main_content_wrap __inner_nr dashboard">

    <div class="container">
        <div class="row">
            <div class="col-lg-12col-md-12 col-sm-12 col-xs-12">

                <div class="  __thanx">
                    <img src="https://food.granddubai.net/alfabee/images/check.png" />

                    <h1>Thank You <img src="https://food.granddubai.net/alfabee/images/hp.png" /></h1>
                    <input type="hidden" id="userid" value="<?= !empty($userdetails) ? $userdetails[0]->id : '' ?>">
                    <input type="hidden" id="order_time" value="<?= !empty($Array[0]->date) ? $Array[0]->date : '' ?>">



                    <?php
                    //  if(isset($order_id)){
                    //  echo '<h2 style="text-align:center;">Order #'.$order_id.'</h2>';
                    //  echo '<br>';echo '<br>';
                    echo '<h2 style="text-align:center;">' . $this->lang->line("order_placed_sucessfully") . '</h2>';
                    if (!empty($msgOrderDelv)) {
                        ?><h4 style="text-align:center;"><?= $msgOrderDelv ?></h4><?php
                    }
                    echo '<h4 style="text-align:center;">' . $this->lang->line("check_email") . '</h4>';


                    if ($minutes < 3 && $Array[0]->order_status != 5) {
                        echo '<h5 style="text-align:center;" id="timer"></h5>';
                        ?>
                        <h5 style="text-align:center;"><a class="__click_cancel" href="<?= base_url('order_history/' . base64_encode($Array[0]->id)) ?>">Click here to cancel this order</a></h5>
                        <?php
                    }
                    //  echo '<h4 style="text-align:center;" id="timer_show">Cancel your order in <span id="timer"></span></h4>';
                    //  }
                    ?>


                     
                        
                        <p style="font-size: 14px;margin-top: 10px;    text-align: left;margin-left: 29px;">You can pay by any of the following options. Once payment is made, you can share the transaction
details along with order number on our Whatsapp number 0316-5572935</p>
                                            
                                            <ul style="font-size: 14px;text-align: left;padding-right: 10px;padding-bottom: 15px;">
                                                <li> <img height="80" style="float: right;" src="<?= base_url('images/easy-paisa.png') ?>">Easy Paisa Account : 0316-5572935<br>Account Title: Aamer Shehzad </li>
                                                
                                                <li><img height="50" style="float: right;    margin-top: 19px;margin-right: -90px;" src="<?= base_url('images/jazz-cash.jpg') ?>"> Jazz Cash Account: 0316-5572935<br>Account Title: Aamer Shehzad </li>
                                                
                                                <li>Bank Transfer IBAN = PK40 BAHL 0083 0981 0094 5801<br>Account Title: ALFABEE<br>Bank Al Habib – Mandi Bahauddin Br.</li>
                                            </ul>

                </div>
            </div>
        </div>
    </div>


</section>