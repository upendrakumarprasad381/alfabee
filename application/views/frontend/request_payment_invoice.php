<?php
$segment2 = $this->uri->segment(2);
$order_id = base64_decode($segment2);
$session_user = $this->session->userdata('UserLogin');
$userdetails = GetUserDetails($session_user->id);
$order = GetPartyOrderDetailsById($order_id);
if(empty($order)){
    return false;
}
$item_count = count($order);
$order_data = GetPartyOrderById($order_id);

$Qry = "SELECT * FROM `restaurant_details` WHERE `vendor_id`=" . $order[0]->vendor_id;
$name = $this->Database->select_qry_array($Qry);

$saved_address = "SELECT * FROM `user_order_address` WHERE user_id=$session_user->id  ORDER BY id DESC LIMIT 1";
$address_details = $this->Database->select_qry_array($saved_address);

$oStatus=  GetPartyOrderStatusName($order[0]->order_status);

?>
<section class="section main_content_wrap __inner_nr dashboard">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                <div class="wd100">
                    <div class="wd100 _dsoutbox">
                        <?php include('includes/dleftMenu.php'); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">

                <div class="wd100 _dsoutbox orders_wrap cart_table_wrap">

                    <h3><a href="<?= base_url('my_profile') ?>"> <?php echo $this->lang->line("my_account"); ?></a>  
                        <i class="fas fa-chevron-right"></i> <?php echo $this->lang->line("my_party_orders"); ?>
                    </h3>
                    <div class="orders_blk">
                        <div class="orders_blk_topbra">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                                &nbsp;<span style="color: black;font-size: 15px;font-weight: 600;"><?= $name[0]->restaurant_name ?> - <?= $order[0]->party_orderno ?></span>
                            </div>  
                        </div>

                        <div class="clearfix"></div>
                        <div class="table-responsive">

                            <table  class="table"> 
                                <tr>
                                    <th align="left" valign="middle" scope="col"><?php echo $this->lang->line("items"); ?></th>
                                    <th align="left" valign="middle" scope="col"><?php echo $this->lang->line("special_request"); ?></th>
                                    <th align="center" valign="middle" scope="col" style="text-align:center;"><?php echo $this->lang->line("quantity"); ?></th>
                                    <th align="center" valign="middle" scope="col" style="text-align:center;"><?php echo $this->lang->line("price"); ?></th>
                                    <th align="center" valign="middle" scope="col" style="text-align:center;"><?php echo $this->lang->line("total"); ?></th>
                                </tr>
                                <?php
                                foreach ($order as $ord) {
                                    ?>
                                    <tr>
                                        <?php
                                        if ($this->session->userdata('language') == 'ar') {
                                            $align = 'right';
                                        } else {
                                            $align = 'left';
                                        }
                                        ?>
                                        <td align="<?php echo $align ?>" valign="top"><strong><?= $ord->product_name ?></strong>

           <!--<p>Barbeque Sauce, Ground Beef, Bold Bbq, Garlic Parmesan</p></td>-->
                                        <td align="left" valign="top"><?= $ord->special_request ?></td>
                                        <td align="center" valign="middle"><?= $ord->quantity ?></td>
                                        <td align="center" valign="middle"><?php echo $this->lang->line("aed"); ?> <?= number_format($ord->price, 2) ?></td>
                                        <td align="center" valign="middle"><?php echo $this->lang->line("aed"); ?> <?= number_format($ord->subtotal, 2) ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>

                                <tr>
                                    <td colspan="4" align="right" valign="top"> 
                                        <?php echo $this->lang->line("sub_total"); ?>
                                    </td>
                                    <td align="center" valign="middle"><?php echo $this->lang->line("aed"); ?> <?= number_format($order_data[0]->price, 2) ?></td>
                                </tr>
                                <!--<tr>-->
                                <!--  <td colspan="4" align="right" valign="top"><?php echo $this->lang->line("delivery_charge"); ?></td>-->
                                <!--  <td align="center" valign="middle"><?php echo $this->lang->line("aed"); ?> <?= number_format($name[0]->service_charge, 2) ?></td>-->
                                <!--</tr>-->
                                <tr>
                                    <td colspan="4" align="right" valign="top"><strong><?php echo $this->lang->line("total_amount"); ?></strong></td>
                                    <td align="center" valign="middle"><strong><?php echo $this->lang->line("aed"); ?> <?= number_format($ord->total_price, 2) ?></strong></td>
                                </tr>
                            </table>


                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 pops">   
                                <div class="progress-bar-wrapper">
                                    <!--<div class="status-bar" style="width: 80%;">-->
                                    <!--	<div class="current-status" style="transition: width 0s linear -1500ms;"></div>-->
                                    <!--</div>-->
                                    <!--<ul class="progress-bar">-->
                                    <!--	<li class="section" style="width: 20%;">Order Received</li>-->
                                    <!--	<li class="section" style="width: 20%;">Order confirmed</li>-->
                                    <!--	<li class="section" style="width: 20%;">Assigned to courier</li>-->
                                    <!--	<li class="section" style="width: 20%;">On the way</li>-->
                                    <!--	<li class="section" style="width: 20%;">Delivered</li>-->
                                    <!--</ul>-->
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="orders_blk" >
                        <div class="info_orders">
                            <div class="row">
                                <input type="hidden" id="order_time" value="<?= $order[0]->date ?>">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-xs-12">
                                    <?php
                                    $date = date("M d-Y", strtotime($order[0]->date));
                                    $time = date('h:i a', strtotime($order[0]->date));
                                    ?>
                                    <h4><?php echo strtoupper($this->lang->line("order_details")); ?></h4> 
                                    <p><?php echo $this->lang->line("order_id"); ?>: <?= $order[0]->party_orderno ?> (<?= $item_count ?>)</p>
                                    <p><?php echo $this->lang->line("order_date"); ?>: <?= $date ?> <?= $time ?></p>
                                    <p><?php echo $this->lang->line("total_amount"); ?>: <?php echo $this->lang->line("aed"); ?> <?= number_format($ord->total_price, 2) ?></p>
                                    <p><?php echo $this->lang->line("mode_of_payment"); ?>:
                                        <?php
                                        if ($ord->mode_of_payment == 1) {
                                            echo $this->lang->line("credit_card");
                                        } elseif ($ord->mode_of_payment == 2) {
                                            echo $this->lang->line("visa_checkout");
                                        } else {
                                            echo $this->lang->line("cash_on_delivery");
                                        }
                                        ?>
                                    </p>
                                </div>

                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-xs-12">
                                    <h4>Party Details</h4> 
                                    <p>No.of Guests :<?= $order[0]->no_of_people ?></p>
                                    <p>Party Date : <?= date('D, d M-Y', strtotime($order[0]->party_date)) ?></p>
                                    <p>Party Time : <?= date('h:i A', strtotime($order[0]->party_time)) ?></p>
                                    <p>Status : <span style="padding: 2px 10px;    background: cadetblue;"><?= $oStatus ?></span></p>


                                </div>

                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-xs-12 manroredr" style="display:none;">
                                    <h4><?php echo strtoupper($this->lang->line("manage_order")); ?></h4>
                                    <!--<p style="display: none;"><b><a href="#"><i class="fas fa-file-alt"></i>  REQUEST INVOICE</a></b></p>-->
                                    <p><b><a href="<?= base_url('contact_us') ?>"><i class="fas fa-question-circle"></i> <?php echo strtoupper($this->lang->line("need_help")); ?></a></b></p>
                                    <?php
                                    $datetime1 = strtotime("now");
                                    $datetime2 = strtotime($order[0]->date);
                                    $interval = abs($datetime2 - $datetime1);
                                    $minutes = round($interval / 60);
                                    if ($minutes < 3) {
                                        ?>
                                        <?php if (empty($order[0]->order_status)) { ?>
                                            <p><b><a href="javascript:void(0)" OrderId="<?= $order_id ?>" id="CancelledMyOrder"><i class="fas fa-ban"></i> <?php echo strtoupper($this->lang->line("cancel_order")); ?></a></b></p>
                                            <p><span id="timer"></span></p>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>       

            </div>
        </div>

    </div>
</div>
</div>
</div>
</div>
</section>
<style>
    .pops ul.progress-bar{
        background-color:#FFF !important;

    }
</style>
<?php
// if(!empty($request_map[$order[0]->order_status]))
// {
//     $status = $request_map[$order[0]->order_status];
// }else{
//     $status = 'pending';   
// }
?>
<script>
 
// var status = '<?php echo $status ?>';

// if(status=='Order Cancelled')

// {
// var ProgressBarValue = '{"OrderStatus":"<?php echo $status; ?>","OrderNode":["<?php echo $this->lang->line('order_cancelled') ?>"]}';
// }else{
//      ProgressBarValue = '{"OrderStatus":"<?php echo $status; ?>","OrderNode":["<?php echo $this->lang->line('order_confirmed') ?>","<?php echo $this->lang->line('prepare_order') ?>","<?php echo $this->lang->line('order_on_the_way') ?>","<?php echo $this->lang->line('order_delivered') ?>"]}';
// }

// var ProgressBarValue = '{"OrderStatus":"<?php echo $status; ?>","OrderNode":["Order Received","Order Confirmed","Assigned to deliveryboy","Order On the way","Order Delivered"]}'; 
</script>
