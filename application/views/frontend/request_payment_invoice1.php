<?php
$segment2 =$this->uri->segment(2); 
$order_id=base64_decode($segment2);
$session_user= $this->session->userdata('UserLogin');
$userdetails=GetUserDetails($session_user->id);
$order = GetPartyOrderById($order_id);

$item_count = count($order);
$order_data = GetOrderByOrderId($order_id);

$Qry = "SELECT * FROM `restaurant_details` WHERE `vendor_id`=".$order[0]->vendor_id;
$name = $this->Database->select_qry_array($Qry);

?>
<section class="section main_content_wrap __inner_nr dashboard">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
					<div class="wd100">
						<div class="wd100 _dsoutbox">
							<?php include('includes/dleftMenu.php'); ?>
						</div>
					</div>
				</div>
				<div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
				 
					<div class="wd100 _dsoutbox orders_wrap cart_table_wrap">
					 
                <h3><a href="my_orders.php"> <?php echo $this->lang->line("my_account");?> </a>  
                    <i class="fas fa-chevron-right"></i><?php echo $this->lang->line("my_party_orders");?>
                </h3>
                <div class="orders_blk">
                    
                    <div class="orders_blk_topbra">
                        <input type="hidden" value="<?= $order[0]->id?>" id="party_id">
                       
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                            &nbsp;<span style="color: black;font-size: 15px;font-weight: 600;"><?= $name[0]->restaurant_name?> - <?= $order[0]->party_orderno?></span>
                        </div>  
                    </div>

                    <div class="clearfix"></div>
                    <div class="details">
                         <table  class="table"> 
                                 
                                  
                                 
                                  <tr>
                                    <td colspan="" align="left" valign="top"> 
                                    <?php echo $this->lang->line("occasion");?>
                                    </td>
                                    <td align="left" valign="left"><?= GetNameById($order[0]->occasion_id,'occasion','occasion_name') ?></td>
                                  </tr>
                                  <tr>
                                    <td colspan="" align="left" valign="top"> 
                                    <?php echo $this->lang->line("party_date");?>
                                    </td>
                                    <td align="left" valign="left"><?= date('D, d M-Y', strtotime($order[0]->party_date)) ?></td>
                                  </tr>
                                  <tr>
                                    <td colspan="" align="left" valign="top"> 
                                    <?php echo $this->lang->line("party_time");?>
                                    </td>
                                    <td align="left" valign="left"><?= date('h:i A', strtotime($order[0]->party_time)) ?></td>
                                  </tr>
                                  <tr>
                                    <td colspan="" align="left" valign="top"><?php echo $this->lang->line("no_of_guests");?></td>
                                   <td align="left" valign="left"><?php echo $this->lang->line("adult");?> - <?= $order[0]->no_of_adult?>, <?php echo $this->lang->line("children")?> -<?= $order[0]->no_of_children?>  </td>
                                  </tr>
                                  <tr>
                                    <td colspan="" align="left" valign="top"> 
                                    <?php echo $this->lang->line("party_venue");?>
                                    </td>
                                    <td align="left" valign="left"><?= $order[0]->party_venue ?></td>
                                  </tr>
                                  <tr>
                                    <td colspan="" align="left" valign="top"> 
                                    <?php echo $this->lang->line("cuisines");?>
                                    </td>
                                    <td align="left" valign="left">
                                        <?php
                                        $cuisine = explode(',',$order[0]->cuisine_id);
                                        foreach($cuisine as $cus){
                                            $Qry = "SELECT * FROM `cuisine` WHERE cuisine_id='$cus' ";
                                            $CuisineArray = $this->Database->select_qry_array($Qry);
                                            $cuisine_name[] = $CuisineArray[0]->cuisine_name;
                                        }
                                        echo implode(',',$cuisine_name);
                                        ?>
                                    </td>
                                  </tr>
                                  
                                </table>
                    </div>
                    <?php
                    if($order[0]->price!=0)
                    {
                    ?>
                    
                    <div class="party_order">
                        <div class="orders_blk_topbra">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                            &nbsp;<span style="color: black;font-size: 15px;font-weight: 600;"><?php echo $this->lang->line("payment_information");?></span>
                        </div> 
                        </div>
                            <div class="table-responsive">
                                
                                <table  class="table"> 
                                 
                                  
                                 
                                  <tr>
                                    <td colspan="" align="left" valign="top"> 
                                    <?php echo $this->lang->line("sub_total");?>
                                    </td>
                                    <td align="left" valign="left"><?php echo $this->lang->line("aed");?> <?= number_format($order[0]->price,2)?></td>
                                  </tr>
                                  <tr>
                                    <td colspan="" align="left" valign="top"> 
                                    <?php echo $this->lang->line("tax");?>
                                    </td>
                                    <td align="left" valign="left"><?php echo $this->lang->line("aed");?> 0</td>
                                  </tr>
                                  <tr>
                                    <td colspan="" align="left" valign="top"> 
                                    <?php echo $this->lang->line("delivery_charge");?>
                                    </td>
                                    <td align="left" valign="left"><?php echo $this->lang->line("aed");?> 0</td>
                                  </tr>
                                  <tr>
                                    <td colspan="" align="left" valign="top"><strong><?php echo $this->lang->line("total_amount");?></strong></td>
                                    <td ><strong><?php echo $this->lang->line("aed");?> <?= number_format($order[0]->price,2)?></strong></td>
                                  </tr>
                                  
                                </table>
        
                                
                                
        
                            </div>
                        
                        
        
                        
                            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        				<div class="wd100 order_summary __bbeere __ssrequests">
        
        					<div class="wd100">
        						<h3><?php echo $this->lang->line("select_payment_method");?></h3>
        					</div>
        
        					<div class="row">
        					
        
        
        
        						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        							<article class="card __pyawrapbx" style="border-radius: 0;">
        								<div class="card-body ">
        
        									<ul class="nav bg-light nav-pills rounded nav-fill mb-3" role="tablist">
        										<li class="nav-item">
        											<a class="nav-link" data-toggle="pill" href="#nav-tab-card">
        												<i class="fa fa-credit-card"></i><?php echo $this->lang->line("credit_card");?></a></li>
        										<li class="nav-item">
        											<a class="nav-link" data-toggle="pill" href="#nav-tab-paypal">
        												<i class="fab fa-cc-visa"></i><?php echo $this->lang->line("visa_checkout");?></a></li>
        										<li class="nav-item">
        											<a class="nav-link" data-toggle="pill" href="#nav-tab-bank">
        												<i class="far fa-money-bill-alt"></i><?php echo $this->lang->line("cash");?></a></li>
        									</ul>
        
        									<div class="tab-content">
        										<div class="tab-pane fade show" id="nav-tab-card">
        
        											<form role="form">
        												<div class="form-group">
        													<label for="username"><?php echo $this->lang->line("full_name");?><?php echo $this->lang->line("on_card");?></label>
        													<input type="text" class="form-control" name="username" placeholder="" required="">
        												</div> <!-- form-group.// -->
        
        												<div class="form-group">
        													<label for="cardNumber"><?php echo $this->lang->line("card_no");?></label>
        													<div class="input-group">
        														<input type="text" class="form-control" name="cardNumber" placeholder="">
        														<div class="input-group-append">
        															<span class="input-group-text text-muted">
        																<i class="fab fa-cc-visa"></i>   <i class="fab fa-cc-amex"></i>  
        																<i class="fab fa-cc-mastercard"></i>
        															</span>
        														</div>
        													</div>
        												</div> <!-- form-group.// -->
        
        												<div class="row">
        													<div class="col-sm-8">
        														<div class="form-group">
        															<label><span class="hidden-xs"><?php echo $this->lang->line("expiration");?></span> </label>
        															<div class="input-group">
        																<input type="number" class="form-control" placeholder="MM" name="">
        																<input type="number" class="form-control" placeholder="YY" name="">
        															</div>
        														</div>
        													</div>
        													<div class="col-sm-4">
        														<div class="form-group">
        															<label data-toggle="tooltip" title="" data-original-title="3 digits code on back side of the card"><?php echo $this->lang->line("cvv");?><i class="fa fa-question-circle"></i></label>
        															<input type="number" class="form-control" required="">
        														</div> <!-- form-group.// -->
        													</div>
        												</div> <!-- row.// -->
        												<button class="subscribe btn btn-primary btn-block" type="button"  style="border-radius: 0;"><?php echo $this->lang->line("confirm");?></button>
        											</form>
        										</div> <!-- tab-pane.// -->
        										<div class="tab-pane fade" id="nav-tab-paypal">
        											<p>
        													
        You can now pay safely online with fewer clicks by using Visa Checkout. <br/>
        Please click the Visa Checkout button below to proceed with your payment.</p>
        											  
        										</div>
        										<div class="tab-pane fade" id="nav-tab-bank">
        											<button style="text-align:center" class="subscribe btn btn-success pay_now_request" type="button"  style="border-radius: 0;"><?php echo $this->lang->line("pay_now");?></button>
        										</div> <!-- tab-pane.// -->
        									</div> <!-- tab-content .// -->
        
        								</div> <!-- card-body.// -->
        							</article> <!-- card.// -->
        						</div>
        
                           
         
        
        					</div>
        				</div>
        			</div>
                     </div>
                     <?php
                    }
                    ?>
        </div>
        </div>
    </div>
			 
				
		</div>
		</div>
</section>

