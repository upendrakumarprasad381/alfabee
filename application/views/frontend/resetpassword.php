<section class="section main_content_wrap __inner_nr dashboard">
<div class="container">
		<div class="row">
			<div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
				<div class="wd100">
					<div class="wd100 _dsoutbox">
						<div class="row pl-2 pr-2 pb-3">
						    <input type="hidden" id="userid" value="<?=$id?>">
						 	 <div class="col-lg-6 col-md-6 col-sm-12">
                             	<h4 class="pb-1">Reset Password</h4> 
							   <div class="form-group">
                                    <input type="password" class="form-control" value="" name="password" id="password" placeholder="Enter your new password" autocomplete="off"> 
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" value="" name="cpassword" id="cpassword" placeholder="Confirm password" autocomplete="off"> 
                                </div>
								 
								<span class="common_message"></span>
								<div class="form-group">
									<button type="button" class="btn btn-primary float-right pl-5 pr-5 border-radius-0" id="resetpassword">Save</button>
                                </div>
							</div>		
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>