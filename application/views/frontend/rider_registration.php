<style>
    #loader_rider {      
        display: none;      
        position: fixed;      
        z-index: 999;      
        height: 2em;      
        width: 2em;      
        overflow: show;      
        margin: auto;      
        top: 0;      
        left: 0;      
        bottom: 0;      
        right: 0;    
    }    
    #loader_rider:before {      
        content: '';      
        display: block;      
        position: fixed;      
        top: 0;      
        left: 0;      
        width: 100%;      
        height: 100%;      
        background-color: rgba(0,0,0,0.3);    
    }
</style>
<section class="section __signwrap">
    <div class="container">
        <div class="inner_pg_blok">
            <div class="row">
                <div id="loader_rider" style="display:none;"><img src="<?= base_url('images/loading.gif') ?>"/></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login_box">
                    <h3><?php echo $this->lang->line("become_a_rider"); ?></h3> 

                    <p><?php echo $this->lang->line("IF_YOU_HAVE_A_BYKE"); ?></p>
                    <form class="needs-validation row" >


                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("full_name"); ?></label>
                            <input type="text" class="form-control" id="rider_name" autocomplete="off"   placeholder="" required>
                        </div>


                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("FATHER_NAME"); ?></label>
                            <input type="text" class="form-control" id="father_name" autocomplete="off"   placeholder="" required>

                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("location"); ?></label>
                           
                            <textarea class="form-control" id="business_location"   rows="2" autocomplete="off"></textarea>
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1">NIC #</label>
                            <input type="text" class="form-control" id="nic" autocomplete="off"   placeholder="" required>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("DELIVERING_RADIUS"); ?></label>
                            <div class="form-group row __nubrline">
                                <input type="text" class="col-8 form-control _nodropfild" placeholder="" id="delivery_radius" autocomplete="off"> 
                                <input type="text" class="col " name="radius" id="radius" value="km" disabled="true">
                            </div>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("mobile_number"); ?></label>
                            <div class="form-group row __nubrline">

                                <input type="text" class="col " name="mobile_code" id="mobile_code" value="92" disabled="true">

                                <input type="text" class="col-8 form-control _nodropfild" placeholder="" id="phone_number" autocomplete="off"> </div>
<!--<input type="number" class="form-control" id="phone_number"   placeholder="" autocomplete="off" required>-->

                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("email_id"); ?></label>
                            <input type="text" class="form-control" id="email_address" autocomplete="off"  placeholder="" required>
                        </div>



                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("BIKE_TYPE"); ?></label>
                            <select class="form-control" id="bike_type">
                                <option value=""><?php echo $this->lang->line("CHOISE_BIKE_TYPE"); ?> </option>
                                <option value="Motor Bike"><?php echo $this->lang->line("MOTOT_BIKE"); ?> </option>
                                <option value="Bicycle"><?php echo $this->lang->line("BICYCLE"); ?></option> 
                                <option value="Loader Bike"><?php echo $this->lang->line("LOADER_BYKE"); ?></option> 
                            </select>

                        </div>


                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("ARE_YOU_18_YEAR"); ?></label>
                            <select class="form-control" id="age">
                                <option value="1"><?php echo $this->lang->line("yes"); ?> </option>
                                <option value="0"><?php echo $this->lang->line("no"); ?></option> 
                            </select>

                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line("DO_YOU_HAVE_LEARING_PERMIT"); ?></label>
                            <select class="form-control" id="isLicense">
                                <option value="1"><?php echo $this->lang->line("yes"); ?> </option>
                                <option value="0"><?php echo $this->lang->line("no"); ?></option> 
                            </select>

                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                            <span class="common_msg"></span>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <button type="button" class="btn btn-warning float-right" id="SubmitRider"><b><?php echo $this->lang->line("submit"); ?></b></button>
                        </div>
                    </form>


                </div> 


            </div>
        </div>
    </div>
</section>
<div id="business_location_map" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <input type="hidden" value="" id="isEditing">
            <div class="modal-header modal-alt-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $this->lang->line("location"); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
            <div class="modal-body">
                <div class="srch-by-txt">
                    <input type="hidden" id="lat" value="">
                    <input type="hidden" id="lng" value="">
                    <input type="text" id='business-location' value="" placeholder="<?php echo $this->lang->line("search_road_landmark"); ?>" autocomplete="off" class="form-control">
                </div>
                <div class="map-wrapper-inner" id="map-page">
                    <div id="google-maps-box">
                        <!--<div class="map-icons location_icon"><img ng-src="<?= base_url() ?>images/locate-mp.svg" class="noDrag locate cursor" onclick="getLocation()" width="45" height="45" src="<?= base_url() ?>images/locate-mp.svg" title="Current Location"></div>-->
                        <div id="map" style="width:100%; height:300px;"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="button-wrap">
                    <button type="button" class="btn btn-success confirm_location" data-dismiss="modal"><?php echo $this->lang->line("CONFIRM"); ?></button>
                </div>
            </div>
        </div>
    </div>
</div>