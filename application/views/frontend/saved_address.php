<?php
    $user = GetSessionArrayUser();
    $address = GetAllAddress($user->id);
  
?>
<section class="section main_content_wrap __inner_nr dashboard">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
					<div class="wd100">
						<div class="wd100 _dsoutbox">
							<?php include('includes/dleftMenu.php'); ?>
						</div>
					</div>
				</div>
				<div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
					<div class="wd100">
						<div class="wd100 _dsoutbox otext __my_address">
							<h4><?php echo $this->lang->line("my_address");?> <?php if(count($address)>0){?><a href="#" data-toggle="modal" data-target="#map_panel" onclick="showAddressPopup(false)" class="__add_more"><i class="fas fa-plus-circle"></i> <?php echo $this->lang->line("add_address");?></a><?php } ?></h4>
						 
							<?php
							if(count($address)>0)
							{
							foreach($address as $addrs)
							{
							 ?>
						
							<div class="wd100 __lis">
								<div class="row __listwp">
									<div class="col-2">
										<?php echo $this->lang->line("address_name");?>
									</div>
									<div class="col">
										<?php
										if($addrs->address_label == 1)
										{
										    $address_name = $addrs->building;
										}elseif($addrs->address_label == 2)
										{
										    $address_name = $addrs->house;
										}else{
										    $address_name = $addrs->office;
										}
										echo $address_name;
										?>
									</div>
								</div>

								<div class="row __listwp">
									<div class="col-2">
										<?php echo $this->lang->line("address");?>  
									</div>
									<div class="col">
										<?= $addrs->address?>, <?= $addrs->apartment_no?>,<?= $addrs->floor?>,<?= $addrs->street?>
									</div>
								</div>

								<div class="row __listwp" >
									<div class="col-2">
										<?php echo $this->lang->line("mobile_number");?>
									</div>
									<div class="col">
									    <?= $addrs->mobile_code?> <?= $addrs->mobile_number?>
									</div>
								</div>

								<div class="__edit">
                                                                    <a href="" data-toggle="modal" style="display: none;" data-target="#map_panel"  onClick="showAddressPopup(true,this)" data-address="<?= $addrs->id?>"><i class="far fa-edit"></i></a>
									<a href="" onClick = "delAddr(event);"><i class="far fa-trash-alt"></i></a>
								</div>
							</div>
							<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenter" aria-modal="true">
                        	    <input type="hidden" value="" id="del_address">
                        	<div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                        		<div class="modal-content">
                        		    <div class="modal-header">
                        				
                        				<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                        			</div>
                        			<div class="modal-body">
                        		
                                        <h3><?php echo $this->lang->line("are_you_sure_want_to_delete");?></h3>
                                        <br>
                                    
                                    
                        			</div>
                        			<div class="modal-footer">
                        			    <button class="btn btn-success" onclick="ok_delete(<?= $addrs->id?>)"><?php echo $this->lang->line("ok");?></button>
                                        <button class="btn btn-default" onclick="cancel()"><?php echo $this->lang->line("cancel");?></button>
                                
                              </div>
                        		</div>
                        	</div>
                        </div>
							<?php
							}
							}else{
							?>
							<section class="text-center">
							    <img src="<?= base_url('images/addresses-empy.svg')?>" width="60" alt="no addresses available">
							    <p><?php echo $this->lang->line("no_address_to_display");?></p>
							    <button class="btn btn-success" data-toggle="modal" data-target="#map_panel" onclick="showAddressPopup(false)"><?php echo $this->lang->line("add_address");?></button>
							</section><br>
							<?php
							}
							?>
						 
					</div>
				</div>
			<div id="map_panel" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <input type="hidden" value="" id="isEditing">
                    <div class="modal-header modal-alt-header">
                        <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $this->lang->line("add_new_address");?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>
                    <div class="modal-body">
                        <div class="srch-by-txt">
                            <input type="text" id='store-location' value="" placeholder="<?php echo $this->lang->line("search_road_landmark");?>" autocomplete="off" class="form-control">
                        </div>
                        <div class="map-wrapper-inner" id="map-page">
                            <div id="google-maps-box">
                                <div id="map" style="width:100%; height:300px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="button-wrap">
                            <!--<button type="button" class="btn secondary_button" id="view_address"><i class="fa fa-address-card" aria-hidden="true"></i> View Address Fields</button>-->
                            <button type="button" class="btn btn-success confirm_location" data-dismiss="modal"><?php echo $this->lang->line("CONFIRM");?></button>
                        </div>
                    </div>
                </div>
            </div>
         </div>
         
         <div class="__addadresspop modal fade bd-example-modal-lg" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<h5 class="modal-title address_title" id="exampleModalLongTitle"><?php echo $this->lang->line("add_new_address");?></h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									  <span aria-hidden="true">&times;</span>
									</button>
								  </div>
									
								  <div class="modal-body">  
									  
									  <div class="row">
										  <input type="hidden" value="" id="content">
										   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											   
											  <!-- <div class="__map wd100">-->
												 <!--  <div class="  form-group">-->
												 <!--<label class="wd100" for="exampleFormControlSelect1">Search Road or Landmark</label>-->
												 <!-- <input type="email" class="form-control" placeholder="Search...." id="store-map-location">-->
												  <!--<input type="text" class="form-control area" aria-describedby="Area" id="pac-input">-->
												 <!--  </div>-->
												 <!--  <div id="map"></div>-->
													<!--<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d10205.12794366965!2d55.339414167405394!3d25.268648208207992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sae!4v1582186793959!5m2!1sen!2sae" width="100%" height="150" frameborder="0" style="border:0;" allowfullscreen=""></iframe>-->
											  <!-- 	</div>-->
										  </div>
										   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											   <h5><?php echo $this->lang->line("contact_details");?></h5>
										  </div>
											   
										  <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

											   <div class="form-group row __nubrline">
												<label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("mobile_number");?></label>
												<input type="text" class="form-control col _nodropcode" name="mobile_code" value="+92" disabled="true" id="mobile_code">
													<input type="text" class="col-8 form-control _nodropfild" placeholder="" id="mobile_number" value="" >
												</div>
										 </div>
										 <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
											 <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("land_no_optional");?></label>
											  <input type="text" class="form-control" placeholder="" id="landphone_no" value="">
										 </div>
									 	  
									     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											   <h5><?php echo $this->lang->line("address_details");?></h5>
										  </div>
										  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
										      <input type="hidden" value="" id="location-lat">
										      <input type="hidden" value="" id="location-lng">
											 <label class="wd100" for="exampleInputEmail1"><?php echo $this->lang->line("location");?></label>
											 <input type="text" class="form-control location" aria-describedby="Area" id="location" value="" readonly="true">
										  </div>
										  
										  <!--<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group">-->
											 <!--<label class="wd100" for="exampleFormControlSelect1">City</label>-->
											 <!--<input type="text" class="form-control city" aria-describedby="NaCityme" id="locality" value="" disabled="true"> -->
										  <!--</div>-->
										  
										  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group">
											 <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("type");?></label>
											  <select class="form-control col _nodropcode" id="building_type">
											       
													<option value="1"><?php echo $this->lang->line("apartment");?></option>
													<option value="2"><?php echo $this->lang->line("house");?></option>
													<option value="3"><?php echo $this->lang->line("office");?></option> 
												</select>
										  </div>
 	   
										  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group">
											 <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("street");?></label>
											  <input type="text" class="form-control" placeholder="" id="street" value="">
										  </div>
										
										  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group" id="building">
											 <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("building");?></label>
											  <input type="text" class="form-control" placeholder="" id="building_name" value="">
										  </div>
										  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group" id="apartment">
											 <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("apartment_no");?></label>
											  <input type="text" class="form-control" placeholder="" id="apartment_no" value="">
										  </div>
										  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group" style="display:none;" id="office">
											 <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("office");?></label>
											  <input type="text" class="form-control" placeholder="" id="office_name">
										  </div>
										  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group" style="display:none;" id="house">
											 <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("house");?></label>
											  <input type="text" class="form-control" placeholder="" id="house_name">
										  </div>
										  
									      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group" id="floor">
											 <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("floor");?></label>
											  <input type="email" class="form-control" placeholder="" id="floor_no" value="">
										  </div>
										  
										  <div class="col-lg-8 col-md-6 col-sm-12 col-xs-12 form-group">
											 <label class="wd100" for="exampleFormControlSelect1"><?php echo $this->lang->line("additional_directions");?></label>
											 <textarea class="form-control" id="exampleFormControlTextarea1" rows="1" id="additional_direction" value=""></textarea>
										  </div>
								 		  
									  </div>
  		  
									  <span class="common_message"></span>
								</div>
 
								  <div class="modal-footer"> 
								    <button type="button" class="btn secondary_button" id="show_map"><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $this->lang->line("view_map");?></button>
									<button type="button" class="btn btn-primary" id="save_address"><?php echo $this->lang->line("save_address");?></button>
								  </div>
									
							  </div>
							</div>

 
							</div>
	
			</div>
		</div>
		</div>
</section>