<?php
error_reporting(0);
$segment2 = base64_decode($this->uri->segment(2));
$categories = GetRestaurantCategory($segment2);
$party_categories = GetRestaurantPartyCategory($segment2);
$get_restaurant_details = GetRestaurantDetails($segment2);
$rest_location = $this->session->userdata('rest_location');
$vendorAr = GetusersById($segment2);
$Qry = "SELECT * FROM promo_code WHERE promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE() AND promo_code.user_type=1 AND promo_code.status=0 AND promo_code.archive=0 AND FIND_IN_SET($segment2,promo_code.restaurant_id)";
$sql = $this->Database->select_qry_array($Qry);

$session_cart = $this->session->userdata('CartData');

$session_cartPRCODE = !empty($session_cart) && is_array($session_cart) ? $session_cart : [];
for ($i = 0; $i < count($session_cartPRCODE); $i++) {
    $dfty = $session_cartPRCODE[$i];
    unset($session_cartPRCODE[$i]['promocode_id']);
    unset($session_cartPRCODE[$i]['discount_amt']);
    unset($session_cartPRCODE[$i]['maximum_discount']);
}
$this->session->set_userdata('CartData', $session_cartPRCODE);

if (!empty($vendorAr->archive)) {
    die(json_encode(array("code" => 200, 'response' => array('status' => false, 'message' => 'Shop is inactive'))));
}
if ($vendorAr->status != '1') {
    die(json_encode(array("code" => 200, 'response' => array('status' => false, 'message' => 'Shop is inactive'))));
}

$lan = getsystemlanguage();
$partyOrdAct = !empty($session_cart[0]['party']) ? 1 : '';

$session_arr = $this->session->userdata('UserLogin');

$user_id = !empty($session_arr->id) ? $session_arr->id : '';
$RedirectUrl = base_url('checkout');
$OnclickUrl = empty($session_arr) ? "LoginAndCallBack('$RedirectUrl')" : "window.location = '$RedirectUrl'";

$Redirect_Url = base_url('party_order');
$Onclick_Url = empty($session_arr) ? "LoginAndCallBack('$Redirect_Url')" : "window.location = '$Redirect_Url'";

$Redirect_Url1 = base_url('search_details/' . $this->uri->segment(2));
$Onclick_Url1 = empty($session_arr) ? "LoginAndCallBack('$Redirect_Url1')" : "window.location = '$Redirect_Url1'";

$occasion = GetOccasion();
$get_cuisine = GetCuisineByRestaurant($segment2);

$Qry1 = "SELECT * FROM `restaurant_banner` WHERE vendor_id='$segment2'";
$Array = $this->Database->select_qry_array($Qry1);
$bannerList = $Array;
$banner = isset($Array[0]->image) ? $Array[0]->image : '';
?>
<style>
    .add_to_cart{
        cursor: pointer;
    }
    .sub_cat{
        cursor: pointer;
    }

    .secondary_button{
        border: 1px solid #00a53c;
        color: #00a53c;
    }
    .__outofstock{
        width: 75px;
        height: 75px;
        position: absolute;
        left: 0;
        /* right: 0; */
        background: #00000091;
        margin: auto;
        text-align: center;
        padding-top: 30px;
        text-transform: uppercase;
        color: #fff;
        font-weight: bold;
        /* border-radius: 50%;*/
    }
    .have_options{
        line-height: 1.2rem;
        display: inherit;
        font-size: 1rem;
        margin-top: 4px;
        color: rgb(244, 162, 102);
    }
    .addedlist7353{
        background: #fecd08;
    }
</style>
<div class="wd100 breadcrumb_wrap">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>"><?php echo $this->lang->line("home"); ?></a></li>
                <?php
                if ($get_restaurant_details[0]->location != '') {
                    $rest_name = $get_restaurant_details[0]->location;
                    $this->session->set_userdata('location', $rest_name);
                    $this->session->set_userdata('sub_locality', $rest_name);
                    $this->session->set_userdata('locality', $get_restaurant_details[0]->city);
                    $this->session->set_userdata('administrative_area_level_1', $get_restaurant_details[0]->city);
                    $this->session->set_userdata('latitude', $get_restaurant_details[0]->latitude);
                    $this->session->set_userdata('longitude', $get_restaurant_details[0]->longitude);
                    $this->session->set_userdata('store_type', $get_restaurant_details[0]->store_type);
                } else {
                    $rest_name1 = $get_restaurant_details[0]->area;
                    $res_name2 = explode('-', $rest_name1);
                    $rest_name = $res_name2[0];

                    $this->session->set_userdata('location', $rest_name);
                    $this->session->set_userdata('sub_locality', $rest_name);
                    $this->session->set_userdata('locality', !empty($res_name2[1]) ? $res_name2[1] : '');
                    $this->session->set_userdata('administrative_area_level_1', !empty($res_name2[1]) ? $res_name2[1] : '');
                    $this->session->set_userdata('latitude', $get_restaurant_details[0]->latitude);
                    $this->session->set_userdata('longitude', $get_restaurant_details[0]->longitude);
                    $this->session->set_userdata('store_type', $get_restaurant_details[0]->store_type);
                }
                $location = $rest_name;
// if(!empty($rest_location['location']))
// {
//     $location = $rest_location['location'];
// }else{
//     $location = $rest_name;
// }
                ?>
                <li class="breadcrumb-item active"><a href="<?= base_url('search_result') ?>"><?= $location ?></a></li>
                <!--<li class="breadcrumb-item active"><a href="<?= base_url('search_result?location=' . $rest_name) ?>"><?= $rest_name ?></a></li>-->

<!--<li class="breadcrumb-item active"><a href="<?= base_url('search_result?location=' . $rest_name) ?>"><?= $rest_name ?></a></li>-->
                <li class="breadcrumb-item active" aria-current="page"><?= $get_restaurant_details[0]->restaurant_name ?></li>
                <input type="hidden" value="<?= $location ?>" id="location">
                <input type="hidden" value="<?= $get_restaurant_details[0]->latitude ?>" id="latitude">
                <input type="hidden" value="<?= $get_restaurant_details[0]->longitude ?>" id="longitude">
                <input type="hidden" value="<?= $segment2 ?>" id="vendor_id">
                <input type="hidden" value="<?= $get_restaurant_details[0]->store_type ?>" id="store_type">
            </ol>
        </nav>
    </div>
</div>
<section class="section __scsearchwp __search_details">
    <div class="container">
        <div class="row">

            <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
                <div class="wd100 __gdwp __dicrpwp">
                    <div class="media">
                        <?php
                        if (!empty($get_restaurant_details[0]->image)) {
                            $image = base_url() . 'uploads/vendor_images/' . $get_restaurant_details[0]->image;
                        } else {
                            $image = base_url(DEFAULT_LOGO_RESTAURANT);
                        }
                        ?>
                        <a href="#"><img class="mr-4 ml-4 __gdimg" src="<?= $image ?>"></a>
                        <div class="media-body __gdtx">
                            <h3>
                                <?php
                                if ($lan == 'ar' && !empty($get_restaurant_details[0]->restaurant_name_ar)) {
                                    echo $get_restaurant_details[0]->restaurant_name_ar;
                                } else {
                                    echo $get_restaurant_details[0]->restaurant_name;
                                }
                                ?>

                            </h3>
                            <?php
                            $qry = "SELECT COUNT(user_feedback.status) AS total_reviews,AVG(rating) as rating  FROM `user_feedback` LEFT JOIN users ON users.id=user_feedback.user_id WHERE vendor_id='$segment2' AND user_feedback.archive=0 AND user_feedback.status=0 ORDER BY user_feedback.id desc";
                            $Array = $this->Database->select_qry_array($qry);
                            ?>
                            <div class="wd100 __star_reviews">
                                <!--<div class="__star"> <i class="fa fa-star"></i> </div>-->
                                <div class="_reviews"> (<?= $Array[0]->total_reviews ?> <?php echo $this->lang->line("reviews"); ?>) </div>
                                <div style="display: block;" class="__star"> <i class="fa fa-star"></i> <?= round($Array[0]->rating, 1) ?></div> 
                                <!--<div class="_reviews"> (711 reviews) </div> -->
                            </div>
                            <?php
                            $storeDel = GetstoretypeBy($get_restaurant_details[0]->store_type);
                            if ($get_restaurant_details[0]->store_type == '1') {
                                $cus = $get_restaurant_details[0]->cuisine_name;
                            } else {
                                if ($lan == 'ar' && !empty($storeDel->store_type_ar)) {
                                    echo $storeDel->store_type_ar;
                                } else {
                                    echo $storeDel->store_type;
                                }
                            }
                            ?>








                            <div class="wd100 __gd_subtag"><?= $cus ?></div>

                            <!--                            <div class="__subinfo" style="margin-left:0px;">
                                                            <ul>
                                                                <li style="display:none;">Service: <?= DecimalAmount($get_restaurant_details[0]->service_charge) ?></li> 
                                                                <li>
                            <?php
                            if ($get_restaurant_details[0]->delivery_time != '') {
                                ?>
                                                                        
                                                                                                                    We deliver in: <?= $get_restaurant_details[0]->delivery_time ?>
                                <?php
                            }
                            ?>
                                                                </li> 
                                                                <li>Min.Order: <?php echo $this->lang->line("aed"); ?> <?= DecimalAmount($get_restaurant_details[0]->min_amount) ?></li>
                                                            </ul>
                                                        </div>-->

                            <div class="wd100 __gd_subtag __delocation"> 
                                <a href="#"><?= str_replace('-', ',', $get_restaurant_details[0]->area) ?></a>
                            </div>

                            <div class="__subinfo" style="margin-left:0px;">
                                <ul>
                                    <?php if ($get_restaurant_details[0]->opening_time != '00:00:00') { ?>
                                        <li ><dirv style="    float: left;"><?php echo $this->lang->line("opening_time"); ?></dirv>:  <span><?= date('h:i A', strtotime($get_restaurant_details[0]->opening_time)) ?> - <?= date('h:i A', strtotime($get_restaurant_details[0]->closing_time)) ?></span></li> 
                                    <?php }if ($get_restaurant_details[0]->delivery_hours_et != '00:00:00') { ?>
                                        <li ><dirv style="    float: left;"><?php echo $this->lang->line("delivery_timing"); ?></dirv> : <span><?= date('h:i A', strtotime($get_restaurant_details[0]->delivery_hours_st)) ?> - <?= date('h:i A', strtotime($get_restaurant_details[0]->delivery_hours_et)) ?></span></li> 
                                    <?php } ?>
                                </ul>

                            </div>

                            <div class="__pay_icon"> <img src="https://food.granddubai.net/alfabee/images/pay-.jpg">  </div>








                            <div class="wd100 __info" style="display:none;"> 
                                <a href="#" data-toggle="modal" data-target="#exampleModal_Info"><img src="https://food.granddubai.net/alfabee/images/info.png"> Restaurant info </a> 


                                <!-- Modal -->
                                <div class="modal fade  __popup__info" id="exampleModal_Info" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">

                                        <div class="modal-content">

                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Burger King</h5> 

                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>

                                            <div class="modal-body">

                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            if (!empty($sql)) {
                                $rest_id = explode(',', $sql[0]->restaurant_id);

                                if (in_array($segment2, $rest_id)) {

                                    foreach ($sql as $s) {
                                        ?>									

                                        <div class="wd100 __off" > 

                                            <a href="#" data-toggle="modal" data-target="#exampleModal_off">
                                                <img src="https://food.granddubai.net/alfabee/images/off.png">
                                                <?= $s->discount ?>% off <br/> <small>T&Cs apply</small> 
                                            </a> 


                                            <!-- Modal -->
                                            <div class="modal fade  __popup__off" id="exampleModal_off" tabindex="-2" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">

                                                    <div class="modal-content">

                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">T&Cs apply</h5>

                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>

                                                        <div class="modal-body">

                                                            <p><?= $s->discount ?>% off on orders. 
                                                            </p> 
                                                            <?php
                                                            if ($s->purchase_amount != 0) {
                                                                ?>
                                                                <p>For order above <?= $s->purchase_amount ?> </p>
                                                                <?php
                                                            }
                                                            ?>
                                                            <p>use code <?= $s->promo_code ?></p>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>								
                                        <?php
                                    }
                                }
                            }
                            ?>



                        </div>
                    </div>
                </div> 
            </div>   

            <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12 __subimgbnr">  
                <div class="wd100 __ddpgsld">
                    <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
                        <!--<ol class="carousel-indicators">-->
                        <!--	<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>-->
                        <!--	<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>-->
                        <!--	<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>-->
                        <!--</ol>-->
                        <?php
                        if (!empty($bannerList)) {

                            $flagrr = 0;
                            for ($i = 0; $i < count($bannerList); $i++) {
                                $d = $bannerList[$i];
                                $filenff = 'uploads/banner_images/' . $d->image;
                                if (is_file(HOME_DIR . $filenff)) {
                                    ?>
                                    <div class="carousel-item <?= empty($flagrr) ? 'active' : '' ?>" dd><img src="<?= base_url($filenff) ?>" class="d-block w-100"> </div>
                                    <?php
                                    $flagrr++;
                                }
                            }
                            ?>

                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
                        <?php } else { ?>
                            <div class="carousel-item active" dd><img src="https://food.granddubai.net/alfabee/images/poimg.jpg" class="d-block w-100"> </div>
                        <?php } ?>

                    </div>
                </div>
<!--<img class="img-fluid" src="https://food.granddubai.net/alfabee/images/poimg.jpg">-->
            </div>   			     



            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 mt-4  ">




                <div class="wd100 radius10" style="display: none;"> 
                    <div class="wd100 __ddpgsld">
                        <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            </ol>
                            <?php
                            if (isset($banner) && $banner != "") {
                                $image = explode(",", $banner);
                                ?>
                                <div class="carousel-inner">
                                    <?php
                                    for ($i = 0; $i < count($image); $i++) {
                                        if ($i == 0) {
                                            $class = "active";
                                        } else {
                                            $class = '';
                                        }
                                        ?>
                                        <div class="carousel-item <?= $class ?>"><img src="<?= base_url() ?>uploads/banner_images/<?= $image[$i] ?>" class="d-block w-100"> </div>
                                    <?php } ?>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
                            <?php } else { ?>
                                <div class="carousel-item active"><img src="<?= base_url() ?>images/ft2.jpg" class="d-block w-100"> </div>
                            <?php } ?>

                        </div>
                    </div>
                    <div class="wd100 __gdwp __dicrpwp">
                        <div class="media">
                            <?php
                            if (!empty($get_restaurant_details[0]->image)) {
                                $image = base_url() . 'uploads/vendor_images/' . $get_restaurant_details[0]->image;
                            } else {
                                $image = base_url(DEFAULT_LOGO_RESTAURANT);
                            }
//$image = 'uploads/vendor_images/'.$get_restaurant_details[0]->image;
                            if ($this->session->userdata('language') == 'ar') {
                                if ($get_restaurant_details[0]->cuisine_name_ar != '') {
                                    $cuisin = $get_restaurant_details[0]->cuisine_name_ar;
                                } else {
                                    $cuisin = $get_restaurant_details[0]->cuisine_name;
                                }
                            } else {
                                $cuisin = $get_restaurant_details[0]->cuisine_name;
                            }
                            ?>
                            <a href="#"><img class="mr-4 ml-4 __gdimg" src="<?= $image ?>"></a>
                            <div class="media-body __gdtx">
                                <h3><?= $get_restaurant_details[0]->restaurant_name ?></h3>
                                <?php
                                if ($get_restaurant_details[0]->store_type == '1') {
                                    $cus = $cuisin;
                                } else {
                                    $cus = GetNameById($get_restaurant_details[0]->store_type, 'store_type', 'store_type');
                                }
                                ?>
                                <div class="wd100 __gd_subtag"><?= $cus ?></div>
                                <?php
                                $qry = "SELECT count(id) as total_reviews,AVG(rating) as rating FROM user_feedback WHERE vendor_id='$segment2'";
                                $Array = $this->Database->select_qry_array($qry);
                                $rating = !empty($Array[0]->rating) ? number_format($Array[0]->rating, 1) : 0;
                                ?>
                                <div class="wd100 __star_reviews">
                                        <!--<div class="__star"> <i class="fa fa-star"></i> <?= $rating ?></div>-->
                                        <!--<div class="_reviews"> (<?= $Array[0]->total_reviews ?> reviews) </div>-->
                                    <div class="__subinfo" style="margin-left:0px;">
                                        <ul>
                                                <!--<li><?= $get_restaurant_details[0]->delivery_time ?></li>-->
        <!--<li><?php echo $this->lang->line("service"); ?>: <?= DecimalAmount($get_restaurant_details[0]->service_charge) ?></li>-->
                                            <li>Min. Order: <?php echo $this->lang->line("aed"); ?> <?= DecimalAmount($get_restaurant_details[0]->min_amount) ?></li>
                                        </ul>
                                    </div>
                                </div>
                                <?php
                                if (!empty($sql)) {
                                    $rest_id = explode(',', $sql[0]->restaurant_id);
                                    if (in_array($segment2, $rest_id)) {
                                        foreach ($sql as $s) {
                                            ?>
                                            <div dddddrr class="wd100 __offer">
                                                <!--Super Saver - Get 50% off on all... <a class="__ofer_more" href="#">More</a>-->
                                                <?php echo $this->lang->line("super_saver"); ?> - Get <?= $s->discount ?>% off. use code <?= $s->promo_code ?>
                                            </div>
                                            <?php
                                        }
                                    }
                                }
                                ?>

                                <div class="__pay_icon"> <img src="<?= base_url() ?>images/pay-.jpg"> </div>



                            </div>
                        </div>
                    </div>
                </div>



                <div class="wd100 __mtbwp  ">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" id="tab11">
                            <a class="nav-link <?= empty($partyOrdAct) ? 'active' : '' ?>" id="menu-tab" data-toggle="tab" href="#menu" role="tab" aria-controls="menu" aria-selected="true">
                                <div class="tbimg"> </div> <?php echo $this->lang->line("menu"); ?></a>
                        </li>
                        <?php
                        if ($get_restaurant_details[0]->party_order == 1) {
                            ?>
                            <li class="nav-item" id="tab12">
                                <a class="nav-link <?= !empty($partyOrdAct) ? 'active' : '' ?>" id="partyOrder-tab" data-toggle="tab" href="#partyOrder" role="tab" aria-controls="partyOrder" aria-selected="true">
                                    <div class="tbimg"> </div> <?php echo $this->lang->line("party_order"); ?></a>
                            </li>
                            <?php
                        }
                        ?>
                        <li class="nav-item">
                            <a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">
                                <div class="tbimg"> </div><?php echo $this->lang->line("reviews"); ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="false">
                                <div class="tbimg"> </div> <?php echo $this->lang->line("overview"); ?></a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade  <?= empty($partyOrdAct) ? 'active show' : '' ?>" id="menu" role="tabpanel" aria-labelledby="menu-tab">
                            <div class="row">
                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                    <div class="wd100 __menulttb get_category">
                                        <h5><?php echo $this->lang->line("categories"); ?></h5>
                                        <ul>
                                            <?php
                                            foreach ($categories as $cat) {
                                                if ($this->session->userdata('language') == 'ar') {
                                                    if ($cat->category_name_ar != '') {
                                                        $category = $cat->category_name_ar;
                                                    } else {
                                                        $category = $cat->category_name;
                                                    }
                                                } else {
                                                    $category = $cat->category_name;
                                                }
                                                ?>
                                                                                                                                <!--<li class="actv"><a href="#"><?= $cat->category_name; ?></a></li>-->
                                                <li id="cat-list-righ-<?= $cat->category_id ?>"><a href="#div_section_<?= $cat->category_id ?>" catId="<?= $cat->category_id ?>" class="scrollLink"><?= $category; ?></a></li>

                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 __menu_listvw">
                                    <div class="wd100 __ltserach ">
                                        <div class="form-group has-search eydhhdgege"> 
                                            <span class="fa fa-search form-control-feedback search-icon"></span>
                                            <!--<span class="delete-icon" ><i class="fas fa-times"></i></span>-->
                                            <input type="text" class="form-control" placeholder="Search Item" id="searchiteams" ddid="search_menu_item"> </div>
                                    </div>
                                    <div class="wd100 __accordion_cm" id="get_id"> 
                                        <div class="panel-group yeundgdw3" id="accordion" role="tablist" aria-multiselectable="true">

                                            <?php
                                            if (count($categories) > 0) {
                                                ?>
                            <!--                                                <table width="100%">
                                                                                <tbody id="myTable">-->
                                                <?php
                                                $i = 1;
                                                foreach ($categories as $k => $menu_cat) {

//                                                    echo '<pre>';
//                                                    print_r($CategoryArray); 
//                                                    echo '</pre>';
                                                    // print_r($CategoryArray);die();
                                                    //  $Qry = "SELECT * FROM `menu_list` LEFT JOIN promo_code ON (promo_code.offer_addedby=menu_list.vendor_id AND promo_code.date_from<=CURDATE() AND promo_code.date_to>=CURDATE()) WHERE menu_list.vendor_id='$segment2' AND `category_id`='$menu_cat->category_id' AND menu_list.archive=0";
                                                    //  print_r($Qry);
                                                    //  $CategoryArray = $this->Database->select_qry_array($Qry);
                                                    //  print_r($CategoryArray);die();
                                                    ?>
                                                        <!--                                                        <tr>
                                                                                                                    <td>-->
                                                    <div class="panel panel-default" id="mainPannel<?= $i ?>" RRRR>
                                                        <div class="panel-heading <?= $i == '1' ? 'active ' : '' ?>" role="tab" id="headingOne">
                                                            <h4 class="panel-title">
                                                                <a id="div_section_<?= $menu_cat->category_id ?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $i ?>" aria-expanded="true" aria-controls="collapse<?= $i ?>">
                                                                    <?php
                                                                    if ($lan == 'ar' && !empty($menu_cat->category_name_ar)) {
                                                                        $menu_cat->category_name = $menu_cat->category_name_ar;
                                                                    }
                                                                    ?>                                                                 
                                                                    <?= $menu_cat->category_name; ?> 

                                                                    <img id="imgloader<?= $i ?>" style="width: 45px;height: 17px;" src="https://wpamelia.com/wp-content/uploads/2018/11/ezgif-2-6d0b072c3d3f.gif">
                                                                </a>
                                                            </h4> 
                                                        </div>


                                                        <div id="collapse<?= $i ?>" vendorId="<?= $segment2 ?>" uniqid="<?= $i ?>" catId="<?= $menu_cat->category_id ?>" class="panel-collapse loadmenuajax collapse <?= $i == '1' ? 'in  show  ' : '' ?> " role="tabpanel" aria-labelledby="headingOne">
                                                            <div class="panel-body">
                                                                <?php ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--                                                            </td>
                                                                                                    </tr>-->
                                                    <?php
                                                    $i++;
                                                }
                                                ?>
                                                <!--                                                    </tbody> 
                                                                                                </table>-->
                                                <?php
                                            } else {
                                                ?>
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body">
                                                            <h3><?php echo $this->lang->line("no_results_found"); ?></h3>
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php
                                            }
                                            ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade <?= !empty($partyOrdAct) ? 'active show' : '' ?>" id="partyOrder" role="tabpanel" aria-labelledby="partyOrder-tab">

                            <input type="hidden" name="vendor_id" value="<?= $segment2 ?>" id="vendor_id">
                            <input type="hidden" name="user_id" value="<?= $user_id ?>" id="user_id">
                            <div class="row">
                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                    <div class="wd100 __menulttb get_menucategory">
                                        <h5><?php echo $this->lang->line("categories"); ?></h5>
                                        <ul>
                                            <?php
                                            foreach ($party_categories as $cat) {
                                                if ($this->session->userdata('language') == 'ar') {
                                                    if ($cat->category_name_ar != '') {
                                                        $category = $cat->category_name_ar;
                                                    } else {
                                                        $category = $cat->category_name;
                                                    }
                                                } else {
                                                    $category = $cat->category_name;
                                                }
                                                ?>
                                                                                                                                <!--<li class="actv"><a href="#"><?= $cat->category_name; ?></a></li>-->
                                                <li><a href="#div_section_<?= $cat->category_id ?>" class="scrollLink"><?= $category; ?></a></li>

                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 __menu_listvw">
                                    <div class="wd100 __ltserach">
                                        <div class="form-group has-search"> 
                                            <span class="fa fa-search form-control-feedback search-icon"></span>
                                            <span class="delete-icon" onClick="clearRestSearchText()" style="display:none;"><i class="fas fa-times"></i></span>
                                            <input type="text" class="form-control" placeholder="Search Item" id="search_partymenu_item"> </div>
                                    </div>
                                    <div class="wd100 __accordion_cm" id="get_menuid">
                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                                            <?php
                                            if (count($party_categories) > 0) {
                                                $i = 1;
                                                foreach ($party_categories as $k => $menu_cat) {
                                                    $CategoryArray = listPartyMenuByRestaurant($segment2, $menu_cat->category_id);
                                                    ?>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading active" role="tab" id="headingOne">
                                                            <h4 class="panel-title">
                                                                <a id="div_section_<?= $menu_cat->category_id ?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $i ?>" aria-expanded="true" aria-controls="collapse<?= $i ?>">
                                                                    <?= $menu_cat->category_name; ?>
                                                                </a>
                                                            </h4> 
                                                        </div>


                                                        <div id="collapse<?= $i ?>" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                                                            <div class="panel-body">
                                                                <?php
                                                                foreach ($CategoryArray as $menu_list) {

                                                                    if ($this->session->userdata('language') == 'ar') {
                                                                        if ($menu_list->menu_name_ar != '') {
                                                                            $menu_name = $menu_list->menu_name_ar;
                                                                        } else {
                                                                            $menu_name = $menu_list->menu_name;
                                                                        }
                                                                    } else {
                                                                        $menu_name = $menu_list->menu_name;
                                                                    }

                                                                    $ConditionArray = array('menu_id' => $menu_list->id);

                                                                    $image = 'uploads/party_menu/' . $menu_list->image;


                                                                    $opening_time = date('H:i', strtotime($get_restaurant_details[0]->opening_time));
                                                                    $closing_time = date('H:i', strtotime($get_restaurant_details[0]->closing_time));
                                                                    $currentTime = date('H:i', time());
                                                                    if ($currentTime > $opening_time && $get_restaurant_details[0]->busy_status == 1) {
                                                                        $busy = 'data-busy=1';
                                                                    } else {
                                                                        $busy = '';
                                                                    }


                                                                    if ($currentTime < $opening_time) {
                                                                        $close = 'data-close=1';
                                                                    } else {
                                                                        $close = '';
                                                                    }
                                                                    ?>
                                                                    <div class="wd100 __itm_crtwp">
                                                                        <?php
                                                                        if ($get_restaurant_details[0]->store_type != 1) {
                                                                            if ($menu_list->stock == 0) {
                                                                                $style = "pointer-events: none";
                                                                            } else {
                                                                                $style = '';
                                                                            }
                                                                        } else {
                                                                            $style = '';
                                                                        }


                                                                        $arr2 = array();
                                                                        $class = '';
                                                                        if (!empty($session_cart) && $session_cart[0]['party'] == 1) {

                                                                            for ($i = 0; $i < count($session_cart); $i++) {
                                                                                array_push($arr2, $session_cart[$i]['menu_id']);
                                                                            }
                                                                            $menu_val = (int) trim($menu_list->id);

                                                                            if (in_array($menu_val, $arr2)) {

                                                                                foreach ($session_cart as $d) {
                                                                                    if ($d['menu_id'] == $menu_val) {

                                                                                        $class = '';
                                                                                    }
                                                                                }
                                                                            } else {
                                                                                $class = 'add_to_cart';
                                                                            }
                                                                        } else {
                                                                            $class = 'add_to_cart';
                                                                        }
                                                                        ?>
                                                                        <div class="media <?= $class ?>" style="<?= $style ?>" data-category="<?= $menu_list->category_id; ?>" data-party="1" data-id="<?= $menu_list->id; ?>" data-choice="0"  data-vendor="<?= $get_restaurant_details[0]->vendor_id ?>" data-sessionparty="<?= !empty($session_cart) ? $session_cart[0]['party'] : '' ?>" data-sessionvendor="<?= !empty($session_cart) ? $session_cart[0]['vendor_id'] : '' ?>" <?= $busy ?> <?= $close ?>> 

                                                                            <?php
                                                                            if ($get_restaurant_details[0]->store_type != 1) {
                                                                                if ($menu_list->stock == 0) {
                                                                                    $style = "pointer-events: none";
                                                                                    ?>

                                                                                    <div class="__outofstock">
                                                                                        Out of Stock	
                                                                                    </div>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                            <?php
                                                                            if (isset($menu_list->image) && $menu_list->image != '') {
                                                                                ?>
                                                                                <img data-toggle="tooltip" data-placement="top" title="Tooltip on top" class="__itm_crtimg" src="<?= base_url($image); ?>">
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <img class="__itm_crtimg" src="<?= base_url(DEFAULT_LOGO_MENU); ?>">
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                            <div class="media-body">
                                                                                <h3><?= $menu_name; ?></h3>
                                                                                <div class="wd100 __ctdcrp">
                                                                                    <p><?= $menu_list->description; ?></p>
                                                                                </div>
                                                                                <?php
                                                                                $menu_id = explode(',', $menu_list->menu_id);
                                                                                if ($menu_list->price != 0) {
                                                                                    //  if($menu_list->deal_id!=0)
                                                                                    if (in_array($menu_list->id, $menu_id)) {
                                                                                        $discount_price = DecimalAmount($menu_list->price) * $menu_list->discount / 100;
                                                                                        $final_price = DecimalAmount($menu_list->price) - $discount_price;
                                                                                        ?>
                                                                                        <div dddd="" class="wd100 __offer">
                                                                                            <?php
                                                                                            $save = DecimalAmount($menu_list->price) - DecimalAmount($final_price);
                                                                                            ?>
                                                                                            <p>Save <?php echo $this->lang->line("aed"); ?> <?= DecimalAmount($save) ?></p>
                                                                                        </div>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                            <div class="col-4 _cartcm">
                                                                                <div class="row">
                                                                                    <div class="col p-0">
                                                                                        <?php
                                                                                        $menu_id = explode(',', $menu_list->menu_id);
                                                                                        if ($menu_list->price != 0) {
                                                                                            //  if($menu_list->deal_id!=0)
                                                                                            if (in_array($menu_list->id, $menu_id)) {
                                                                                                $discount_price = DecimalAmount($menu_list->price) * $menu_list->discount / 100;
                                                                                                $final_price = DecimalAmount($menu_list->price) - $discount_price;
                                                                                                ?>
                                                                                                <div class="wd100 _cartpris" style="text-decoration: line-through"> <?php echo $this->lang->line("aed"); ?> <?= DecimalAmount($menu_list->price); ?> </div>
                                                                                                <div class="wd100 _cartpris"> <?php echo $this->lang->line("aed"); ?> <?= DecimalAmount($final_price); ?> </div>
                                                                                                <?php
                                                                                            } else {
                                                                                                $final_price = DecimalAmount($menu_list->price);
                                                                                                ?>
                                                                                                <div class="wd100 _cartpris"> <?php echo $this->lang->line("aed"); ?> <?= DecimalAmount($final_price); ?> </div>
                                                                                                <?php
                                                                                            }
                                                                                        } else {
                                                                                            ?>
                                                                                            <div class="wd100 _cartpris" class="option_product" ><?php echo $this->lang->line("price_on_selection"); ?></div>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                <!--<div class="wd100 __star"> <i class="fa fa-star"></i> 4.4 </div>-->
                                                                                    </div>
                                                                                    <div class="col-5 p-0">
                                                                                        <?php
                                                                                        $arr3 = array();
                                                                                        if (!empty($session_cart) && $session_cart[0]['party'] == 1) {
                                                                                            $arr3 = array();
                                                                                            for ($i = 0; $i < count($session_cart); $i++) {
                                                                                                array_push($arr3, $session_cart[$i]['menu_id']);
                                                                                            }
                                                                                            $menu_val = (int) trim($menu_list->id);

                                                                                            if (in_array($menu_val, $arr3)) {
                                                                                                foreach ($session_cart as $d) {
                                                                                                    if ($d['menu_id'] == $menu_val) {
                                                                                                        ?>

                                                                                                        <div class="  __pqty">
                                                                                                            <section class="qty-update-section incart b-a ng-scope">
                                                                                                                <input type="hidden" value="<?= $get_restaurant_details[0]->store_type ?>" id="store_type">
                                                                                                                <button type="button" class="btn btn-sm b-r" RRR onclick="updateQty(this)" data-id="<?= $d['menu_id'] ?>" data-cart="<?= $d['cart_id'] ?>"  data-type="minus" data-value="<?= $d['quantity'] ?>">
                                                                                                                    <i class="fa fa-minus orange"></i>
                                                                                                                </button>
                                                                                                                <span class="f-11" data-id="<?= $d['menu_id'] ?>" id="quantity"><b><?= $d['quantity'] ?></b></span>
                                                                                                                <button type="button" class="btn btn-sm b-l" YYY onclick="updateQty(this)" data-id="<?= $d['menu_id'] ?>" data-cart="<?= $d['cart_id'] ?>"  data-type="plus" data-value="<?= $d['quantity'] ?>">
                                                                                                                    <i class="fa fa-plus orange"></i>
                                                                                                                </button>
                                                                                    <!--<button type="button" class="btn btn-sm b-r"> <i class="fa fa-minus orange"></i> </button> <span class="f-11"><b class="ng-binding">1</b></span>-->
                                                                                    <!--<button type="button" class="btn btn-sm b-l"> <i class="fa fa-plus orange"></i> </button>-->
                                                                                                            </section>
                                                                                                        </div>
                                                                                                        <?php
                                                                                                    }
                                                                                                }
                                                                                            } else {
                                                                                                ?>

                                                                                                <div class="__crtbtn"  data-category="<?= $menu_list->category_id; ?>" data-id="<?= $menu_list->id; ?> "  data-vendor="<?= $get_restaurant_details[0]->vendor_id ?>" data-choice="0" data-sessionparty="<?= !empty($session_cart) ? $session_cart[0]['party'] : '' ?>" data-sessionvendor="<?= !empty($session_cart) ? $session_cart[0]['vendor_id'] : '' ?>" <?= $busy ?> <?= $close ?>>  Add  <i class="fas fa-plus"></i> </div>
                                                                                                <?php
                                                                                            }
                                                                                        } else {
                                                                                            ?>
                                                                                            <div class="__crtbtn"  data-category="<?= $menu_list->category_id; ?>" data-id="<?= $menu_list->id; ?> "  data-vendor="<?= $get_restaurant_details[0]->vendor_id ?>" data-choice="0" data-sessionparty="<?= !empty($session_cart) ? $session_cart[0]['party'] : '' ?>" data-sessionvendor="<?= !empty($session_cart) ? $session_cart[0]['vendor_id'] : '' ?>" <?= $busy ?> <?= $close ?>>  Add  <i class="fas fa-plus"></i> </div>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $i++;
                                                }
                                            } else {
                                                ?>
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body">
                                                            <h3><?php echo $this->lang->line("no_results_found"); ?></h3>
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php
                                            }
                                            ?>

                                        </div>
                                    </div>
                                </div>
                                <!--</div>-->
                            </div>

                            <form id="partyOrder">
                                <div class="row" style="display:none;">	 
                                    <div class="form-group col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line("occasion"); ?></label><span class="required">*</span>
                                        <select class="form-control" id="occasion">
                                            <option value=""><?php echo $this->lang->line("select_occasion"); ?></option>
                                            <?php
                                            foreach ($occasion as $occ) {

                                                if ($this->session->userdata('language') == 'ar') {
                                                    if ($occ->occasion_name_ar != '') {
                                                        $occasion = $occ->occasion_name_ar;
                                                    } else {
                                                        $occasion = $occ->occasion_name;
                                                    }
                                                } else {
                                                    $occasion = $occ->occasion_name;
                                                }
                                                ?>
                                                <option value="<?= $occ->id ?>"><?= $occasion ?></option>
                                                <?php
                                            }
                                            ?>

                                        </select>
                                        <span class="error_occasion"></span>
                                    </div>
                                    <div class="form-group col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line("party_date"); ?></label><span class="required">*</span>
                                        <input type="text" class="form-control party_date" aria-describedby="" id="datepicker" placeholder="<?php echo $this->lang->line("party_date"); ?>" autocomplete="off"> 
                                        <span class="error_date"></span>
                                    </div>

                                    <div class="form-group col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line("party_time"); ?></label><span class="required">*</span>

                                        <!--<div class="row">-->

                                        <!--    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" >-->
                                        <!--	<label></label>-->
                                        <input type="text" class="form-control party_time" aria-describedby="Area" id="datetimepicker3" placeholder="<?php echo $this->lang->line("time"); ?>"> 

                                        <div style="color:red;margin-top: 4px;text-align:right;">*<?php echo $this->lang->line("24_hrs_format"); ?></div>
                                        <span class="error_time"></span>
                                        <!--</div>-->
                                        <!--</div>-->


                                    </div>
                                    <div class="form-group col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line("cuisines"); ?></label><span class="required">*</span>
                                        <select class="form-control selectpicker"  id="cuisine" name="cuisine[]" multiple data-live-search="true">
                                            <?php
                                            foreach ($get_cuisine as $cuisine) {
                                                if ($this->session->userdata('language') == 'ar') {
                                                    if ($cuisine->cuisine_name_ar != '') {
                                                        $cuis = $cuisine->cuisine_name_ar;
                                                    } else {
                                                        $cuis = $cuisine->cuisine_name;
                                                    }
                                                } else {
                                                    $cuis = $cuisine->cuisine_name;
                                                }
                                                ?>
                                                <option value="<?= $cuisine->cuisine_id ?>"><?= $cuis ?></option>
                                                <?php
                                            }
                                            ?>
<!--<input type="text" class="form-control" aria-describedby="" id="cuisine"> -->
                                        </select>
                                        <span class="error_cuisine"></span>
                                    </div>
                                    <div class="form-group col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line("no_of_guests"); ?> </label><span class="required">*</span>

                                        <div class="row">
                                            <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                <?php echo $this->lang->line("adult"); ?> : <input type="number" class="form-control area" aria-describedby="Area" id="adult_num" placeholder="<?php echo $this->lang->line("no_of_adult"); ?>"> 
                                                <span class="error_adult"></span>
                                            </div>
                                            <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                <?php echo $this->lang->line("children"); ?> : <input type="number" class="form-control area" aria-describedby="Area" id="children_num" placeholder="<?php echo $this->lang->line("no_of_children"); ?>"> 
                                                <div style="color:red;margin-top: 4px;text-align:right;">*<?php echo $this->lang->line("below_14_yrs"); ?></div>
                                                <span class="error_child"></span>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                        <input type="hidden" value="" id="lat">
                                        <input type="hidden" value="" id="lng">
                                        <input type="hidden" value="" id="locality">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line("party_venue"); ?></label><span class="required">*</span>
                                        <div class="row">

                                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                                <label></label>
                                                <input type="text" class="form-control" aria-describedby="" id="venue"> 
                                                <span class="error_venue"></span>
                                            </div></div></div>

                                </div>
                            </form>
                            <div class="row" style="display:none;">
                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <button type="button" class="btn btn-success btn-block partyOrderSubmit" url="<?= $Onclick_Url ?>" onClick=""><?php echo $this->lang->line("submit"); ?></button>
                                </div>
                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <span class="message"></span>
                                </div>
                            </div>

                            <!--/</div>-->
                        </div>
                        <div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">

                            <div class="w100 __reviews_listv">



                                <div class="wd100 __reviews_forms">
                                    <h5><?php echo $this->lang->line("reviews"); ?></h5>
                                    <div class="starrating risingstar d-flex justify-content-center flex-row-reverse float-left	">
                                        <input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="5 star">5</label>
                                        <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="4 star">4</label>
                                        <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="3 star">3</label>
                                        <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="2 star">2</label>
                                        <input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="1 star">1</label>
                                        <br>

                                    </div>
                                    <div class="wd100">
                                        <span class="error_rating"></span>
                                    </div>
                                    <div class="wd100">
                                        <div class="form-group">
                                            <label for="exampleFormControlTextarea1"><?php echo $this->lang->line("reviews"); ?></label>
                                            <textarea class="form-control comments" id="exampleFormControlTextarea1" rows="3"></textarea>
                                            <span class="error_comments"></span>
                                        </div>
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary float-right submit_feedback" url="<?= $Onclick_Url1 ?>" onClick=""><?php echo $this->lang->line("submit"); ?></button>
                                        </div>
                                    </div>




                                </div>	
                                <?php
                                $Qry1 = "SELECT *  FROM `user_feedback` LEFT JOIN users ON users.id=user_feedback.user_id WHERE vendor_id='$segment2' AND user_feedback.archive=0 AND user_feedback.status=0 ORDER BY user_feedback.id desc";
                                $Array1 = $this->Database->select_qry_array($Qry1);
                                ?>

                                <h5 class="text-left">(<?= count($Array1) ?>) <?php echo $this->lang->line("reviews"); ?></h5>

                                <?php
                                foreach ($Array1 as $arr) {

                                    if (isset($arr->image) && $arr->image != '') {
                                        $image = base_url() . 'uploads/user_images/' . $arr->image;
                                    } else {
                                        $image = base_url('images/profile-default.png');
                                    }
                                    ?>

                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-2 pl-2 pr-2 __reimg"> <img src="<?= $image ?>" class="img img-rounded img-fluid" /> </div>
                                                <div class="col-md-10 pl-2 pr-2">
                                                    <div class="wd100 mb-2"> 
                                                        <?php
                                                        $stars = (int) $arr->rating;
                                                        $count = 1;
                                                        for ($i = 1; $i <= 5; $i++) {
                                                            if ($stars >= $count) {
                                                                $color = "#ff9900";
                                                            } else {
                                                                $color = "";
                                                            }
                                                            $count++;
                                                            ?>
                                                            <span class="float-left">
                                                                <i class="fa fa-star" style="color:<?= $color ?>"></i>
                                                            </span> 
                                                            <?php
                                                        }
                                                        ?>


                                                        <div class="float-left __rwname"><?= !empty($arr->name) ? $arr->name : '' ?></div>
                                                        <div class="float-right  w100 text-secondary text-center __redatim"><?= !empty($arr->inserted_on) ? date('d M Y, h:i A ', strtotime($arr->inserted_on)) : '' ?></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <p><?= $arr->comments ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>


                            </div>

                        </div>
                        <div class="tab-pane fade" id="overview" role="tabpanel" aria-labelledby="overview-tab">
                            <div class="wd100 __overviewwrp">
                                <h4><?= $get_restaurant_details[0]->restaurant_name ?></h4>
                                <div class="row">
                                    <div class="col-4 __hdtext"> Min. Order </div>
                                    <div class="col"> <?php echo $this->lang->line("aed"); ?> <?= DecimalAmount($get_restaurant_details[0]->min_amount) ?> </div>
                                </div>
                                <div class="row">
                                    <div class="col-4 __hdtext"> <?php echo $this->lang->line("working_hours"); ?> </div>
                                    <?php
                                    $opening_time = date('h:i a ', strtotime($get_restaurant_details[0]->opening_time));
                                    $closing_time = date('h:i a ', strtotime($get_restaurant_details[0]->closing_time));
                                    ?>
                                    <div class="col"> <?= $opening_time ?> - <?= $closing_time ?> </div>
                                </div>
                                <!--<div class="row">-->
                                <!--	<div class="col-4 __hdtext"> Delivery Time </div>-->
                                <!--	<div class="col"> <?= $get_restaurant_details[0]->delivery_time ?> </div>-->
                                <!--</div>-->
                                <?php
                                if ($get_restaurant_details[0]->isDelivery == 1 && $get_restaurant_details[0]->delivery_time_start != '00:00:00' && $get_restaurant_details[0]->delivery_time_ends != '00:00:00') {
                                    ?>

                                    <div class="row">
                                        <div class="col-4 __hdtext"> <?php echo $this->lang->line("delivery_timing"); ?> </div>
                                        <?php
                                        $start_time = date('h:i a ', strtotime($get_restaurant_details[0]->delivery_time_start));
                                        $ends_time = date('h:i a ', strtotime($get_restaurant_details[0]->delivery_time_ends));
                                        ?>
                                        <div class="col"> <?= $start_time ?> - <?= $ends_time ?> </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="row" style="display:none">
                                    <div class="col-4 __hdtext"> <?php echo $this->lang->line("delivery_charge"); ?> </div>
                                    <div class="col"> <?php echo $this->lang->line("aed"); ?> <?= DecimalAmount($get_restaurant_details[0]->service_charge) ?>  </div>
                                </div>
                                <!--<div class="row">-->
                                <!--	<div class="col-4 __hdtext"> Pre-Order </div>-->
                                <!--	<div class="col">-->
                                <?php
//  if($get_restaurant_details[0]->pre_order==1)
//  {
//      echo 'Yes';
//  }else{
//      echo 'No';
//  }
                                ?>
                                <!--	</div>-->
                                <!--</div>-->
                                <div class="row">
                                    <div class="col-4 __hdtext"> <?php echo $this->lang->line("payment"); ?> </div>
                                    <div class="col">
                                        <div class="__pay_icon__over"> <img src="<?= base_url('images/pay-.jpg') ?>"> </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <!--<div class="col-4 __hdtext"> Rating </div>-->
                                    <!--<div class="col">-->
                                    <!--	<div class="wd100 __star_reviews">-->
                                    <!--		<div class="__star"> <i class="fa fa-star"></i> <?= $rating ?> </div>-->
                                    <!--	</div>-->
                                    <!--</div>-->
                                </div>
                                <?php
                                if ($get_restaurant_details[0]->store_type == 1) {
                                    ?>

                                    <div class="row">
                                        <div class="col-4 __hdtext"> <?php echo $this->lang->line("cuisines"); ?> </div>
                                        <?php
                                        // 		echo '<pre>';
                                        // 		print_r($get_restaurant_details[0]);
                                        ?>
                                        <div class="col"> <?= $get_restaurant_details[0]->cuisine_name ?> </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="row">
                                    <div class="col-4 __hdtext"> Description </div>
                                    <div class="col">
                                        <div class="__pay_icon__over"> <?= $get_restaurant_details[0]->about ?> </div>
                                    </div>
                                </div> 


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 mt-4  __cartsummy_wrp">
                <?php
//  if ($get_restaurant_details[0]->store_type == 1 && $get_restaurant_details[0]->table_booking == 1) {

                if (!empty($partyOrdAct)) {
                    ?>

                    <div class="wd100 __reservation" style="display: none;">
                        <input type="hidden" value="<?= $get_restaurant_details[0]->table_capacity ?>" id="table_capacity">
                        <h3>Make a reservation</h3>

                        <div class="wd100">

                            <div class="form-group">
                                <label for="exampleFormControlSelect1">No.of Guests</label>
                                <input type="text" class="form-control check_reservation_date " id="no_of_people" placeholder="" autocomplete="off">
                                <!--<select class="form-control" id="no_of_people">-->
                                <!--  <option value="1">1</option>-->
                                <!--  <option value="2">2</option>-->
                                <!--  <option value="3">3</option>-->
                                <!--  <option value="4">4</option>-->
                                <!--  <option value="5">5</option>-->
                                <!--</select>-->
                            </div>

                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <input type="text" class="form-control check_reservation_time" id="reservation_date" placeholder="Date" autocomplete="off" value="<?= date('Y-m-d'); ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">  
                                    <div class="form-group">
                                        <label>Time</label>
                                        <?php
                                        $start = $get_restaurant_details[0]->table_booking_opening_time; //you can write here 00:00:00 but not need to it
                                        $end = $get_restaurant_details[0]->table_booking_closing_time;

                                        $tStart = strtotime($start);
                                        $tEnd = strtotime($end);
                                        $tNow = $tStart;
                                        echo '<select class="form-control check_reservation_time" name="reservation_time" id="reservation_time">';
                                        echo '<option value="">Select Time</option>';
                                        while ($tNow <= $tEnd) {
                                            echo '<option value="' . date("H:i", $tNow) . '">' . date("h:i A", $tNow) . '</option>';
                                            $tNow = strtotime('+30 minutes', $tNow);
                                        }
                                        echo '</select>';
                                        ?>
                                        <!--<select class="form-control" id="reservation_time">-->
                                        <!--        <option value="12:00">12:00 AM</option>-->
                                        <!--        <option value="12:30">12:30 AM</option>-->
                                        <!--        <option value="1:00">1:00 AM</option>-->
                                        <!--        <option value="1:30">1:30 AM</option>-->
                                        <!--        <option value="2:00">2:00 AM</option>-->
                                        <!--        <option value="2:30">2:30 AM</option>-->
                                        <!--        <option value="3:00">3:00 AM</option>-->
                                        <!--        <option value="3:30">3:30 AM</option>-->
                                        <!--        <option value="4:00">4:00 AM</option>-->
                                        <!--        <option value="4:30">4:30 AM</option>-->
                                        <!--        <option value="5:00">5:00 AM</option>-->
                                        <!--        <option value="5:30">5:30 AM</option>-->
                                        <!--        <option value="6:00">6:00 AM</option>-->
                                        <!--        <option value="6:30">6:30 AM</option>-->
                                        <!--        <option value="7:00">7:00 AM</option>-->
                                        <!--        <option value="7:30">7:30 AM</option>-->
                                        <!--        <option value="8:00">8:00 AM</option>-->
                                        <!--        <option value="8:30">8:30 AM</option>-->
                                        <!--        <option value="9:00">9:00 AM</option>-->
                                        <!--        <option value="9:30">9:30 AM</option>-->
                                        <!--        <option value="10:00">10:00 AM</option>-->
                                        <!--        <option value="10:30">10:30 AM</option>-->
                                        <!--        <option value="11:00">11:00 AM</option>-->
                                        <!--        <option value="11:30">11:30 AM</option>-->
                                        <!--        <option value="12:00">12:00 PM</option>-->
                                        <!--        <option value="12:30">12:30 PM</option>-->
                                        <!--        <option value="13:00">1:00 PM</option>-->
                                        <!--        <option value="13:30">1:30 PM</option>-->
                                        <!--        <option value="14:00">2:00 PM</option>-->
                                        <!--        <option value="14:30">2:30 PM</option>-->
                                        <!--        <option value="15:00">3:00 PM</option>-->
                                        <!--        <option value="15:30">3:30 PM</option>-->
                                        <!--        <option value="16:00">4:00 PM</option>-->
                                        <!--        <option value="16:30">4:30 PM</option>-->
                                        <!--        <option value="17:00">5:00 PM</option>-->
                                        <!--        <option value="17:30">5:30 PM</option>-->
                                        <!--        <option value="18:00">6:00 PM</option>-->
                                        <!--        <option value="18:30">6:30 PM</option>-->
                                        <!--        <option value="19:00">7:00 PM</option>-->
                                        <!--        <option value="19:30">7:30 PM</option>-->
                                        <!--        <option value="20:00">8:00 PM</option>-->
                                        <!--        <option value="20:30">8:30 PM</option>-->
                                        <!--        <option value="21:00">9:00 PM</option>-->
                                        <!--        <option value="21:30">9:30 PM</option>-->
                                        <!--        <option value="22:00">10:00 PM</option>-->
                                        <!--        <option value="22:30">10:30 PM</option>-->
                                        <!--        <option value="23:00">11:00 PM</option>-->
                                        <!--        <option value="23:30">11:30 PM</option>-->
                                        <!--    </select>-->
                                        <!--<input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Time">-->
                                    </div>
                                </div>  
                            </div>
                            <i class="fa fa-info-circle hidden-xs error_reservation" data-toggle="tooltip" title="" aria-describedby="ui-id-1" style="display:none;"></i>
                            <span class="message"></span>
                            <button type="button" class="btn btn-warning btn-block book_table" url="<?= $Onclick_Url1 ?>" onClick="">Book Now</button>


                            <div class="wd100 __reservation_resut" style="display:none;">
                                <h4>Select a time:</h4>

                                <div class="__time __active">
                                    5:00 PM
                                </div>

                                <div class="__time  ">
                                    5:00 PM
                                </div>


                                <div class="__time  ">
                                    5:00 PM
                                </div>


                                <div class="__time  ">
                                    5:00 PM
                                </div>




                            </div>


                        </div>



                    </div>
                    <?php
                }
                ?>














                <addedMenulist>
                    <?php
                    // cart auto list form here update by upendra start
                    $_REQUEST['restaurantId'] = $segment2;
                    getmycartHTMLWebsite();
                    // cart auto list form here update by upendra End
                    ?>
                </addedmenulist>






















                <div class="wd100"> <small class="float-right adstexthd"><?php echo $this->lang->line("sponsored_popular"); ?></small>
                    <?php
                    $Qry = "SELECT * FROM `ads` WHERE status=0 and archive=0 ORDER BY position";
                    $Array = $this->Database->select_qry_array($Qry);
                    foreach ($Array as $dat) {
                        ?>
                        <div class="wd100 adsrt __wtbx mb-2">
                            <a href="javascript:void(0)"><img class="img-fluid" src="<?= base_url('uploads/ads/' . $dat->image) ?>"></a>
                        </div>
                        <?php
                    }
                    ?>
                    <!--<div class="wd100 adsrt __wtbx mb-2">-->
                    <!--	<a href="javascript:void(0)"><img class="img-fluid" src="<?= base_url() ?>images/ads2.jpg"></a>-->
                    <!--</div>-->
                </div>
            </div>


            <div class="__accordion_cm __addadresspop __adonespop modal fade bd-example-modal-lg show" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-modal="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content ">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $this->lang->line("add_item_choices"); ?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                        </div>
                        <div class="modal-body" id="options_menu">

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModal" aria-modal="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content __ctboxpopup">
                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                        </div>

                        <div class="modal-body ">
                            <br>
                            <?php
                            if (!empty($session_cart)) {
                                $Qry = "SELECT * FROM `restaurant_details` WHERE `vendor_id`='" . $session_cart[0]['vendor_id'] . "'";
                                $name = $this->Database->select_qry_array($Qry);
                                ?>
                                <p><?php echo $this->lang->line("items_in_cart"); ?><b><?= $name[0]->restaurant_name ?> </b> </p>

                                <p><?php echo $this->lang->line("clear_cart"); ?></p>
                                <br>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="check_confirmation"><?php echo $this->lang->line("yes"); ?></button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line("no"); ?></button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="PartyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModal" aria-modal="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content __ctboxpopup">
                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                        </div>

                        <div class="modal-body ">
                            <br>
                            <?php
                            if (!empty($session_cart)) {
                                if ($session_cart[0]['party'] == 0) {
                                    ?>
                                    <p>There are items in your cart from online order .</p><p>You can't add items from Party order </p>
                                    <?php
                                } else {
                                    ?>
                                    <p>There are items in your cart from Party order .</p><p>You can't add items from Online order </p>
                                    <?php
                                }
                                ?>

                                <p><?php echo $this->lang->line("clear_cart"); ?></p>
                                <br>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="check_confirmation"><?php echo $this->lang->line("yes"); ?></button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line("no"); ?></button>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <input type="hidden" value="<?= $get_restaurant_details[0]->store_type ?>" id="store_category">	
    <div class="modal fade" id="closeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenter" aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content __ctboxpopup">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
                <div class="modal-body">
                    <br/>
                    <p style="
                       text-align: center;
                       "><span><?php echo $this->lang->line("sorry"); ?></span>, <?= $get_restaurant_details[0]->restaurant_name ?> <?php echo $this->lang->line("restaurant_closed"); ?></p>
                    <br/>


                </div>
                <div class="modal-footer">
                    <?php
                    $store = GetNameById($get_restaurant_details[0]->store_type, 'store_type', 'store_type');
                    ?>
                    <!--<button class="btn btn-success" onclick="cancel_popup()"><?= $store ?> Nearby</button>-->
                    <button class="btn btn-success" onclick="ok_alert()"><?= $store ?> Nearby</button>
                    <button class="btn btn-default __btncontinue" onclick="ok()"><?php echo $this->lang->line("continue"); ?></button>

                </div>
            </div>
        </div> 
    </div>

    <div class="modal fade" id="busyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenter" aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content __ctboxpopup">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
                <div class="modal-body">
                    <br/>
                    <p style="
                       text-align: center;
                       "><span><?php echo $this->lang->line("sorry"); ?></span>, <?= $get_restaurant_details[0]->restaurant_name ?> <?php echo $this->lang->line("restaurant_busy"); ?></p>
                    <br/>


                </div>
                <div class="modal-footer">
                    <?php
                    $store = GetNameById($get_restaurant_details[0]->store_type, 'store_type', 'store_type');
                    ?>
                    <!--<button class="btn btn-success" onclick="cancel_popup()"><?= $store ?> Nearby</button>-->
                    <button class="btn btn-success" onclick="ok_alert()"><?= $store ?> Nearby</button>
                    <button class="btn btn-default __btncontinue" onclick="ok()"><?php echo $this->lang->line("continue"); ?></button>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deliveryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenter" aria-modal="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
                <div class="modal-body">
                    <?php
                    $start_time = date('h:i a', strtotime($get_restaurant_details[0]->delivery_time_start));
                    $ends_time = date('h:i a', strtotime($get_restaurant_details[0]->delivery_time_ends));
                    $currentTime = date('H:i', time());
                    ?>
                    <p><span><?php echo $this->lang->line("sorry"); ?></span>, <?= $get_restaurant_details[0]->restaurant_name ?> <?php echo $this->lang->line("no_delivery"); ?></p>
                    <p><?php echo $this->lang->line("delivery_available_from"); ?> <?= $start_time ?> - <?= $ends_time ?></p>
                    <p><?php echo $this->lang->line("get_order_once_delivery_available"); ?></p>
                    <br>


                </div>
                <div class="modal-footer">
                    <button class="btn btn-success proceed" url="<?= $OnclickUrl ?>" onClick=""><?php echo $this->lang->line("proceed"); ?></button>
                    <button class="btn btn-default" id="deliveryModal" onclick="cancel_alert(this)"><?php echo $this->lang->line("cancel"); ?></button>

                </div>
            </div>
        </div>
    </div>



    <div id="party_venue_map" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <input type="hidden" value="" id="isEditing">
                <div class="modal-header modal-alt-header">
                    <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $this->lang->line("party_venue"); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
                <div class="modal-body">
                    <div class="srch-by-txt">
                        <input type="text" id='store-location' value="" placeholder="<?php echo $this->lang->line("search_road_landmark"); ?>" autocomplete="off" class="form-control">
                    </div>
                    <div class="map-wrapper-inner" id="map-page">
                        <div id="google-maps-box">
                            <div id="map" style="width:100%; height:300px;"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="button-wrap">
                        <button type="button" class="btn btn-success confirm_cuisine" data-dismiss="modal">Done</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .eydhhdgege{
        //display: none;
    }
    .popover-body img{
        width: 100%;
        object-fit:cover;

    }
</style>