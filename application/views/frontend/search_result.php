<?php
// $get_cuisine = GetCuisine();
// $get_cuisine_count = GetCuisineCount($_GET['location']);
$location = $location;
$lan=  getsystemlanguage();
?>
<style>
    ._reviews{
        display: none;
    }
    .wd100 .__subinfo ul{
        display: none;
    }
    .__gdwp {
        padding: 7px 0 !important;
    }
    .__radio_sly .show_all {
        display: none;
    }
    .__radio_sly .__ct{
        display: none;
    }
</style>
<div class="wd100 breadcrumb_wrap">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>"><?php echo $this->lang->line("home"); ?></a></li>
                <li class="breadcrumb-item" ><?= $location ?></li>
                <!--<li class="breadcrumb-item active" aria-current="page">Toyota Prado 3.0 T-DSL 2019 TX.L FOR EXPORT LIMITED STOCK</li>-->
            </ol>
        </nav>
    </div>
</div>
<section class="section __scsearchwp">
    <div class="container">
        <div class="wd100 __tpiner_bnr">


            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <?php
                $banner = '';
                $paginate = '';
                $Qry_img = "SELECT * FROM `sponsored_ad` WHERE status=0 and archive=0";
                $Img_Array = $this->Database->select_qry_array($Qry_img);
                $i=0;
                for ($i = 0; $i < count($Img_Array); $i++) {

                    if ($i == 0) {
                        $class = "active";
                    } else {
                        $class = '';
                    }

                    $paginate = $paginate . '<li data-target="#carouselExampleIndicators" data-slide-to="' . $i . '" class="' . $class . '"></li>';
                    $banner = $banner . ' <div class="carousel-item ' . $class . '"> <img class="d-block w-100" src="' . base_url('uploads/sponsored/' . $Img_Array[$i]->image) . '"></div>';
                }
                ?>
                <ol class="carousel-indicators">
                    <?= $paginate ?>
                    <li data-target="#carouselExampleIndicators" class="<?= empty($i) ? 'active' : '' ?>" data-slide-to="<?= $i ?>"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="<?= $i + 1 ?>"></li>
                    <?php if ($store_type == '1') { ?>
                        <li data-target="#carouselExampleIndicators" data-slide-to="<?= $i + 2 ?>"></li>
                    <?php } ?>
                    <!--<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>-->
                    <!--<li data-target="#carouselExampleIndicators" data-slide-to="3"></li>-->
                </ol>
                <div class="carousel-inner">
                    <?= $banner ?>
                    <div class="carousel-item <?= empty($i) ? 'active' : '' ?>"> <a href="<?= base_url('search_result?bannerType=offers') ?>"><img class="d-block w-100" src="<?= base_url('images/top_inner_banner1.png') ?>"></a></div>
                    <div class="carousel-item "> <a href="<?= base_url('search_result?bannerType=fast-delivery') ?>"><img class="d-block w-100" src="<?= base_url('images/top_inner_banner2.png') ?>"></a></div>
                    <?php if ($store_type == '1') { ?>
                        <div class="carousel-item "> <a href="<?= base_url('search_result?bannerType=party-orders') ?>"><img class="d-block w-100" src="<?= base_url('images/top_inner_banner3.png') ?>"></a></div>
                    <?php } ?>



                </div>

                <!--<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">-->
                       <!--<span class="carousel-control-prev-icon" aria-hidden="true"></span>-->
                       <!--<span class="sr-only">Previous</span>-->
                <!-- </a>-->
                <!-- <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">-->
                       <!--<span class="carousel-control-next-icon" aria-hidden="true"></span>-->
                       <!--<span class="sr-only">Next</span>-->
                <!-- </a>-->
            </div>













        </div>
        <div class="row">


            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input type="hidden" value="<?= $latitude ?>" id="latitude">
                <input type="hidden" value="<?= $longitude ?>" id="longitude">
                <input type="hidden" value="<?= $location ?>" id="location">
                <input type="hidden" value="<?= $locality ?>" id="locality">
                <input type="hidden" value="<?= $store_type ?>" id="store_type">
                <?php
                $store_name = GetNameById($store_type, 'store_type', 'store_type');
              $storedel=  GetstoretypeBy($store_type);
                ?>
                <h1 style="margin-top: 15px;margin-bottom: 15px;"><?= $store_name; ?> <?= !empty($store_name) ? 'in' : '' ?> <?= $location ?></h1> 











            </div>
            <?php
            $array['location'] = $location;
            $array['latitude'] = $latitude;
            $array['longitude'] = $longitude;
            $array['locality'] = $locality;

            $this->session->set_userdata('rest_location', $array);
            ?>
            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                <div class="filter_tog">
                    <div class="filter_tog_text"><?php echo $this->lang->line("filter"); ?></div>
                    <div class="filter_tog_bar">
                        <i></i>
                        <i></i>
                        <i></i>
                    </div>
                </div>

                <div class="sidebar-item left_filtterbox">
                    <div class="make-me-sticky">

                        <div class="wd100 __wtbx">
                            <div class="wd100 __ltserach">

                                <div class="has-search">
                                    <span class="fa fa-search form-control-feedback search-icon"></span>
                                    <span class="delete-icon" onClick="clearRestSearchText()" style="display:none;"><i class="fas fa-times"></i></span>
                                    <input type="text" class="form-control search" placeholder="<?php echo $this->lang->line("filter_name"); ?>" id="restSearchBoxXXX" autocomplete="off">
                                </div>
                            </div>

                            <?php
                            if ($store_type == 1) {
                                ?>

                                <div class="wd100 __sort_by __radio_sly">
                                    <h5>Filter By</h5>
                                    <ul class=" " style="display: none;">
                                        <li>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" id="book_table_filter" name="book_table_filter" class="form-check-input" value="">Book a Table</label>
                                            </div>
                                        </li>
                                    </ul>
                                    <hr />
                                </div>

                                <div class="wd100 __sort_by __radio_sly search">
                                    <h5><?php echo $this->lang->line("cuisines"); ?></h5>
                                    <ul class="scrollbar singlecheckbox">

                                        <?php
                                        $get_cuisine = GetCuisineCount($array['locality'], $array['latitude'], $array['longitude']);
                                        foreach ($get_cuisine as $cuisine) {
                                            if ($this->session->userdata('language') == 'ar') {
                                                if ($cuisine->cuisine_name_ar != '') {
                                                    $cuis = $cuisine->cuisine_name_ar;
                                                } else {
                                                    $cuis = $cuisine->cuisine_name;
                                                }
                                            } else {
                                                $cuis = $cuisine->cuisine_name;
                                            }
                                            //  $count = GetCuisineCount($array['location'],$array['latitude'],$cuisine->cuisine_id);
                                            // print_r($count);
                                            ?>
                                            <li>
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input type="checkbox" class="form-check-input" name="cuisinetype" id="cuisinetype" value="<?= $cuisine->cuisine_id ?>" cuis="<?= $cuis ?>"><?= $cuis ?> <span class="__ct"><?= !empty($cuisine->count) ? $cuisine->count : '0' ?></span></label>
                                                            <!--<input type="radio" class="form-check-input" name="cuisinetype" id="cuisinetype" value="<?= $cuisine->cuisine_id ?>"><?= $cuis ?> <span class="__ct"><?= !empty($cuisine->count) ? $cuisine->count : '0' ?></span></label>-->
                                                </div>
                                            </li>
                                            <?php
                                        }
                                        ?>

                                    </ul> <a href="#" class="show_all" data-toggle="modal" data-target="#exampleModal"><?php echo $this->lang->line("show_all"); ?></a>
                                    <hr /> </div>
                                <div class="wd100 __sort_by __radio_sly search_div">
                                </div>
                                <?php
                            }
                            ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 __ftlistwp">


                <div class="wd100 __ltserach __misrch">


                    <!-- Another variation with a button -->
                    <div class="input-group">
                        <input type="hidden" id="latitudeTemp">
                        <input type="hidden" id="longitudeTemp">

                        <input type="text" id="business-location" class="form-control" placeholder="<?php echo $this->lang->line("search_another_location"); ?>">
                        <div class="input-group-append">
                            <button id="locationchageBtnclick" class="btn btn-warning" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>


                <div class="wd100 __wtbx">




                    <div class="wd100 __flrtp">
                        <ul>
                            <?php
                            $filter = !empty($_REQUEST['filter']) ? $_REQUEST['filter'] : '';
                            ?>
                            <li class="__flrtphd"><?php echo $this->lang->line("sort_by"); ?>:</li>
                            <li><a class="<?= empty($filter) ? 'actv' : '' ?>" href="<?= base_url('search_result') ?>" ><?php echo $this->lang->line("recommended"); ?> </a></li>
                            <li><a class="<?= $filter == '1' ? 'actv' : '' ?>" href="<?= base_url('search_result?filter=1') ?>" ><?php echo $this->lang->line("newest"); ?> </a></li>
                            <li><a style="display: none;" class="" href="" onClick="changeSortCriteria('3', event)"><?php echo $this->lang->line("a_to_z"); ?></a></li>
                            <li><a style="display: none;" class="" href="" onClick="changeSortCriteria('4', event)"><?php echo $this->lang->line("min_order_amt"); ?></a></li>
                            <!--<li><a class="" href="" onClick="changeSortCriteria('5',event)"><?php echo $this->lang->line("fastest_delivery"); ?></a></li>-->
                            <!--<li><a class="" href="" onClick="changeSortCriteria('rating',event)">Rating</a></li>-->
                        </ul>
                    </div>
                    <div class="location">
                        <table>
                            <?php
                            if (count($rest_details) > 0) {
                                foreach ($rest_details as $details) {

                                    // $Qry1 = "SELECT * FROM `delivery_timings` left join cities on cities.city_id=delivery_timings.cities WHERE vendor_id=$details->vendor_id AND cities.city_name LIKE CONCAT('%', '".trim($array['locality'])."' ,'%')";
                                    $Qry1 = "SELECT  delivery_timings.*, GROUP_CONCAT(city_name) AS city_name FROM  delivery_timings 
                                        INNER JOIN cities ON FIND_IN_SET(city_id, cities) > 0 WHERE delivery_timings.vendor_id = '$details->vendor_id' AND cities.city_name LIKE CONCAT('%', '" . trim($array['locality']) . "' ,'%') 
                                        GROUP BY cities,delivery_timings.id";

                                    // print_r($Qry1);

                                    $Array = $this->Database->select_qry_array($Qry1);
                                    //  print_r($Array);
                                    foreach ($Array as $dat) {
                                        $start_time = date('H:i', strtotime($dat->delivery_from));
                                        $end_time = date('H:i', strtotime($dat->delivery_to));
                                        $currentTime = date('H:i', time());
                                        //print_r($currentTime);
                                        if ($start_time != '00:00:00' && $end_time != '00:00:00') {
                                            // if (($end_time < $start_time && ($currentTime <= $start_time && $currentTime >= $end_time)) || 
                                            // ($end_time > $start_time && ($currentTime <= $start_time || $currentTime >= $end_time)) ){
                                            //     $delivery = 0;
                                            // }else{
                                            //     $delivery = 1;
                                            // }
                                            if ($currentTime > $start_time && $currentTime < $end_time) {
                                                $delivery = 1;
                                            } elseif ($currentTime < $start_time && $currentTime < $end_time) {
                                                $delivery = 1;
                                            } else {
                                                $delivery = 0;
                                            }
                                        }
                                        // print_r($delivery);
                                        if ($delivery == 1) {
                                            $delivery_time = $this->lang->line("same_day");
                                        } else {
                                            $delivery_time = $this->lang->line("next_day");
                                        }
                                    }
                                    // print_r($delivery_time);
                                    ?>
                                    <tr>
                                        <td>
                                            <div class="wd100 __gdwp">
                                                <div class="media">
                                                    <?php
                                                    if (!empty($details->image)) {
                                                        $image = base_url() . 'uploads/vendor_images/' . $details->image;
                                                    } else {
                                                        $image = base_url(DEFAULT_LOGO_RESTAURANT);
                                                    }
                                                    ?>
                                                    <a href="#">
                                                        <?php
                                                        $opening_time = date('H:i A', strtotime($details->opening_time));
                                                        $closing_time = date('H:i A', strtotime($details->closing_time));
                                                        $currentTime = date('H:i A', time());

                                                        // if ($currentTime < $opening_time ) {
                                                        if (($currentTime <= $opening_time) || ($closing_time < $opening_time && ($currentTime <= $opening_time && $currentTime >= $closing_time)) ||
                                                                ($closing_time > $opening_time && ($currentTime <= $opening_time || $currentTime >= $closing_time))) {
                                                            ?>
                                                            <div class="__closed">
                                                                Closed	
                                                            </div>
                                                            <?php
                                                        }

                                                        if ($currentTime > $opening_time && $details->busy_status == 1) {
                                                            ?>
                                                            <div class="__busy">
                                                                Busy	
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($this->session->userdata('language') == 'ar') {
                                                            if ($details->cuisine_name_ar != '') {
                                                                $cuisin = $details->cuisine_name_ar;
                                                            } else {
                                                                $cuisin = $details->cuisine_name;
                                                            }
                                                        } else {
                                                            $cuisin = $details->cuisine_name;
                                                        }
                                                        if ($this->session->userdata('language') == 'ar') {
                                                            if ($details->restaurant_name_ar != '') {
                                                                $rest_name = $details->restaurant_name_ar;
                                                            } else {
                                                                $rest_name = $details->restaurant_name;
                                                            }
                                                        } else {
                                                            $rest_name = $details->restaurant_name;
                                                        }
                                                        ?>
                                                        <img class="mr-4 ml-4 __gdimg" src="<?= $image; ?>"></a>
                                                        <!--<span class="logo-overlay">-->
                                                        <!--    <img ng-src="https://www.talabat.com/images/talabat/tag_closed_en.png" class="status-img" src="https://www.talabat.com/images/talabat/tag_closed_en.png">-->
                                                    <!--</span>-->
                                                    <div class="media-body __gdtx">
                                                        <a href="<?= base_url('search_details/' . base64_encode($details->id)); ?>">
                                                            <h3><?= $rest_name ?></h3>
                                                            <?php
                                                            if ($details->store_type == '1') {
                                                                $cus = $cuisin;
                                                            } else {
                                                                $cus = GetNameById($details->store_type, 'store_type', 'store_type');
                                                            }
                                                            ?>

                                                            <div class="wd100 __gd_subtag"><?= $cus ?></div>
                                                            <div class="wd100 __star_reviews">
                                                                <?php
                                                                $qry = "SELECT count(id) as total_reviews,AVG(rating) as rating FROM user_feedback WHERE vendor_id='$details->vendor_id'";
                                                                $Array = $this->Database->select_qry_array($qry);
                                                                $rating = !empty($Array[0]->rating) ? number_format($Array[0]->rating, 1) : 0;
                                                                ?>
                                                                <div class="__star"> <i class="fa fa-star"></i> <?= $rating ?> </div>

                                                                <div class="_reviews"> (<?= $Array[0]->total_reviews ?> reviews) </div>
                                                            </div>
                                                            <div class="wd100 __subinfo">
                                                                <ul>
                                                                    <?php
                                                                    if ($details->isDelivery == 1) {
                                                                        ?>

                                                                        <li style="display:none;"><?php echo $this->lang->line("delivery"); ?> : <?= !empty($delivery_time) ? $delivery_time : '' ?></li>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <li><?= $details->delivery_time ?></li>
                                                                    <li style="display:none;"><?php echo $this->lang->line("delivery_fee"); ?>: <?= $details->service_charge != 0 ? number_format($details->service_charge, 2) : $this->lang->line('free') ?></li>
                                                                    <li><?php echo $this->lang->line("minimum_order"); ?>: <?= $details->min_amount ?></li>
                                                                </ul>
                                                            </div>

                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <div class="wd100 __gdwp">
                                    <div class="media">
                                        <div class="media-body __gdtx">
                                            <h3 class="empty_result">
                                                <?php if($lan=='ar' && !empty($storedel->if_nodatafound)){
                                                    echo $storedel->if_nodatafound;
                                                }else{ ?>
                                                Oh We are Sorry ! No <?= $store_name; ?> found
                                                <?php } ?>
                                            </h3> 
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </table>
                    </div>
                    <div class="cuisine_type"></div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"> <small class="float-right adstexthd"><?php echo $this->lang->line("sponsored_popular"); ?></small>
                <?php
                $Qry = "SELECT * FROM `ads` WHERE status=0 and archive=0 ORDER BY position";
                $Array = $this->Database->select_qry_array($Qry);
                foreach ($Array as $dat) {
                    ?>
                    <div class="wd100 adsrt __wtbx mb-2">
                        <a href="javascript:void(0)"><img class="img-fluid" src="<?= base_url('uploads/ads/' . $dat->image) ?>"></a>
                    </div>
                    <?php
                }
                ?>
                <!--<div class="wd100 adsrt __wtbx mb-2">-->
                <!--	<a href="javascript:void(0)"><img class="img-fluid" src="images/ads2.jpg"></a>-->
                <!--</div>-->
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><?php echo $this->lang->line("all_cuisines"); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="options_menu">
                    <div class="wd100 __sort_by __radio_sly">
                        <div class="row">


                            <?php
                            $get_cuisine = GetCuisineCount($array['locality'], $array['latitude'], $array['longitude']);

                            foreach ($get_cuisine as $cuisine) {
                                // 	foreach($get_cuisine as $cuisine)
                                // 	{
                                ?>
                                <div class="col-sm-5 col-md-4">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="cuisinetype_popup" id="cuisinetype_popup" value="<?= $cuisine->cuisine_id ?>"><?= $cuisine->cuisine_name ?> <span class="__ct">(<?= !empty($cuisine->count) ? $cuisine->count : '0' ?>)</span></label>
                                                <!--<input type="radio" class="form-check-input" name="cuisinetype_popup" id="cuisinetype_popup" value="<?= $cuisine->cuisine_id ?>"><?= $cuisine->cuisine_name ?> <span class="__ct">(<?= !empty($cuisine->count) ? $cuisine->count : '0' ?>)</span></label>-->
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                        </div>

                    </div>

                </div>
                <div class="modal-body" id="options_menu_popup">
                </div>
                <div class="modal-footer">
                    <div class="button-wrap">
                        <button type="button" class="btn btn-success confirm_location" data-dismiss="modal"><?php echo $this->lang->line("CONFIRM"); ?></button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>