<style>
    #loader {      
        display: none;      
        position: fixed;      
        z-index: 999;      
        height: 2em;      
        width: 2em;      
        overflow: show;      
        margin: auto;      
        top: 0;      
        left: 0;      
        bottom: 0;      
        right: 0;    
   }    
   #loader:before {      
        content: '';      
        display: block;      
        position: fixed;      
        top: 0;      
        left: 0;      
        width: 100%;      
        height: 100%;      
        background-color: rgba(0,0,0,0.3);    
   }
</style>
<section class="section __signwrap">
		<div class="container">
			<div class="inner_pg_blok">
				<div class="row">
				    <div id="loader" style="display:none;"><img src="<?= base_url('images/loading.gif')?>"/></div>
					<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 login_box">
						<h3><?php echo $this->lang->line("login");?></h3>
						<form>
							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo $this->lang->line("email");?></label><span class="required">*</span>
								<input type="email" class="form-control" aria-describedby="Name" id="user_email" autocomplete="off"> </div>
							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo $this->lang->line("password");?></label><span class="required">*</span>
								<input type="password" class="form-control" aria-describedby="Name" id="user_password" autocomplete="off"> 
						 	</div>
							
									
							<div class="row">
							<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group  tc_condition">
								<!--<label><input type="checkbox" name="condition" id="condition"> Keep Me logged in</label>-->
							</div>
							<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group ">
							 	<a href="<?=base_url('forgot_password')?>" class="forgot_a"><?php echo $this->lang->line("forgot_password");?></a>
							</div>
							</div>
							<span class="login_message"></span>
							
				 			<div class="form-group">
								<button type="button" class="btn btn-warning float-right pl-5 pr-5 border-radius-0" id="login_submit"><?php echo $this->lang->line("submit");?></button>
							</div>
						</form>
						<div class="social_login_wrap">
							<h5><?php echo $this->lang->line("social_media_signup");?></h5>
                                                        <a href="<?= base_url('Login/Facebook'); ?>" >
								<div class="social_fb" > <img src="<?= base_url()?>images/facebook.png">
									<!--Sign up with Facebook-->
								</div>
							</a>
							<a href="<?= base_url('Login/Google'); ?>" class="btn_login_gg_link st_login_social_link" data-channel="google">
								<div class="social_fb gogole"> <img src="<?= base_url()?>images/search.png">
									<!--Sign up with Google-->
								</div>
							</a>
							<!--<a  class="btn_login_gg_link st_login_social_link" data-channel="google" href="Frontend/linkedinlogin?oauth_init=1">-->
							<!--    <div class="social_fb linkedin">-->
							<!--        <img src="images/linkedin.png">-->
							<!--Sign up with Linkedin-->
							<!--    </div>-->
							<!--</a>-->
						</div>
					</div>
					<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 register_box">
						<h3><?php echo $this->lang->line("register");?></h3>
						<form class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
								<label for="exampleInputEmail1"><?php echo $this->lang->line("full_name");?></label><span class="required">*</span>
								<input type="text" class="form-control" aria-describedby="Name" id="name" autocomplete="off"> </div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
								<label for="exampleInputEmail1"><?php echo $this->lang->line("email");?></label><span class="required">*</span>
								<input type="text" class="form-control" aria-describedby="Name" id="email" autocomplete="off"> </div>
						
						
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
								<label for="exampleInputEmail1"><?php echo $this->lang->line("phone_number");?></label><span class="required">*</span>
								<div class="form-group row __nubrline">
								    
								    <input type="text" class="col " name="mobile_code" id="mobile_code" value="92" disabled="true">
					 
									<input type="text" class="col-8 form-control _nodropfild" placeholder="" id="phone" autocomplete="off"> </div>
							</div>
							
							
							
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
								<label for="exampleInputEmail1"><?php echo $this->lang->line("password");?></label><span class="required">*</span>
								<input type="password" class="form-control" aria-describedby="Name" id="password" autocomplete="off"> <span id="password_msg"></span> </div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
								<label for="exampleInputEmail1"><?php echo $this->lang->line("confirm_password");?></label><span class="required">*</span>
								<input type="password" class="form-control" aria-describedby="Name" id="cpassword" autocomplete="off"> </div>
						
							
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group  tc_condition">
								<label>
									<input type="checkbox" name="condition" id="condition"> <?php echo $this->lang->line("i_agree");?></label>
							        <br><span class="common_message"></span>
							</div>
							
							<span class="register_message"></span>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
								<button type="button" class="btn btn-warning float-right pl-5 pr-5 border-radius-0" id="register_submit"><?php echo $this->lang->line("register");?></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
</section>