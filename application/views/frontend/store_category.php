<?php
$type = base64_decode($this->uri->segment(2));
$storeList=GetstoretypeBy($type);

$Searchtag='Search for '.(!empty($storeList->store_type) ? $storeList->store_type : '');
if($type=='1'){
    $Searchtag=$Searchtag.' or a dish';
}

?>
<section class="section  __cathit __homely_foods">
    <div class="container">
        <div class="row _rpad ">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pl-2 pr-2">
                <!--<h2>Restaurants</h2>-->

                <div class="__ssbrplt">
                    <div class="__box_search_top">
                        <div class="form-group has-search">
                            <input type="hidden" value="<?= $type ?>" id="store_type">
                            <span class="fa fa-search form-control-feedback search-icon"></span>
                            <span class="delete-icon" onClick="clearRestSearchText()" style="display:none;"><i class="fas fa-times"></i></span>
                            <input type="text" class="form-control" placeholder="<?= $Searchtag ?>" id="restSearch">
                        </div>
                    </div>
                    <?php if ($type == '1') { ?>
                        <div class="__partyorder">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" id="partyclickbuttin"  name="parttyorder" class="form-check-input" value=""> Party Order</label>
                            </div>
                        </div>
                    <?php } ?>
                </div>  

                </h2>
            </div>
        </div>
        <div class="__narlist row _rpad ">
            <?php
            if (!empty($homely_food_users)) {
                foreach ($homely_food_users as $users) {
                    if ($users->image != '') {
                        $image = base_url() . 'uploads/vendor_images/' . $users->image;
                    } else {
                        $image = base_url(DEFAULT_LOGO_RESTAURANT);
                    }

                  
                    if ($this->session->userdata('language') == 'ar') {
                        if ($users->cuisine_name_ar != '') {
                            $cuis = $users->cuisine_name_ar;
                        } else {
                            $cuis = $users->cuisine_name;
                        }
                    } else {
                        $cuis = $users->cuisine_name;
                    }

                    if ($this->session->userdata('language') == 'ar') {
                        if ($users->restaurant_name_ar != '') {
                            $rest_name = $users->restaurant_name_ar;
                        } else {
                            $rest_name = $users->restaurant_name;
                        }
                    } else {
                        $rest_name = $users->restaurant_name;
                    }

                    if ($users->store_type == '1') {
                        $cus = $cuis;
                    } else {
                        $cus = GetNameById($users->store_type, 'store_type', 'store_type');
                    }
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center pl-2 pr-2">
                        <a href="<?= base_url('search_details/' . base64_encode($users->id)) ?>"><div class="wd100 __wtbx __msims">
                                <div class="__msims_img">
                                    <img class="img-fluid" src="<?= $image ?>" />
                                </div>
                                <h4><?= $rest_name ?></h4>
                                <h6><?= $cus ?></h6>
                                <h6>
                                    <?php if ($users->opening_time != '00:00:00') { ?>
                                        Opening Time: <?= date('h:i A', strtotime($users->opening_time)) ?> - <?= date('h:i A', strtotime($users->closing_time)) ?>
                                    <?php }if ($users->delivery_hours_et != '00:00:00') { ?>
                                       <br>Delivery Time: <?= date('h:i A', strtotime($users->delivery_hours_st)) ?> - <?= date('h:i A', strtotime($users->delivery_hours_et)) ?>
                                    <?php } ?>
                                    
                                </h6>
                            </div>
                        </a>
                    </div>
                    <?php
                }
            } else {
                ?>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center pl-2 pr-2">
                    <div class="__msims_img">
                        <h3>No results found</h3> 
                    </div>
                </div>
                <?php
            }
            ?>
        </div>

        <div class="__search_result row _rpad" style="display:none;">

        </div>	
    </div>
</section>