<section class="section __sccontacts _spg __addurfood">
		<div class="container">
		  	
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
					<h2>Suggest a HomeBusiness</h2>
				 </div>
				
				 <div class="__form">
					 
				 <div class="row">
				 
					 
				<form class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
					
					<div class="row">	 
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("restaurant_name")?></label>
							<input type="text" class="form-control" id="name" placeholder="" autocomplete="off"> 
						</div>
						
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("location")?></label>
							<input type="text" class="form-control" id="location" placeholder="" autocomplete="off"> 
						</div>
					 	
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("contact_details")?></label>
							<input type="text" class="form-control" id="contact_details" placeholder="" autocomplete="off"> 
						</div>
					 	
					 	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<label for="exampleInputEmail1"><?php echo $this->lang->line("additional_comments")?></label>
							<textarea class="form-control area" id="additional_comments" placeholder=""></textarea>
							<!--<input type="text" class="form-control area" id="additional_comments" placeholder="<?php echo $this->lang->line("additional_comments")?>"> -->
						</div>
						
						<div class="col"> </div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
						    <span class="register_message common_message"></span>
						</div>
						<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 form-group">
						    <button type="button" class="btn btn-primary btn-block Suggest_HomeBusiness"><?php echo $this->lang->line("submit")?></button>
						</div>

						</div>
					</form>
					
		            </div>
		           </div>
		    </div>
	</section>