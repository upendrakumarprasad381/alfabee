<section class="section __sttcpg">
		<div class="container">
  
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
		<h2>Terms and Conditions  </h2>
		
		
		
		<p>  These Terms of  Service (&quot;<strong>Terms</strong>&quot;) are intended to inform you of your legal  rights and obligations regarding your access to and use of the websites and  mobile applications provided by &ldquo;AlfaBee&rdquo; (or &quot;<strong>us / we</strong>&quot;) (in  conjunction with &quot;<strong>Platforms</strong>&quot;). Please read these Terms  carefully. By entering or using the Platforms, you agree to these Terms and  conclude a legally binding contract with &ldquo;AlfaBee&rdquo; and / or its affiliates. You  may not use the Platforms if you do not accept the Terms or do not be bound by  the Terms. Your use of Platforms is your own risk, including the risk of  exposure to objectionable content, or other inappropriate content.<strong> </strong></p>
<p>Platforms can be  used by anyone over the age of 18. If you are under 18, you must get permission  from your parent (s) or legal guardian who will take responsibility for your  actions and any charges related to your use of Platforms and / or Purchase.<strong> </strong><br>
  &nbsp;&rdquo;AlfaBee&rdquo; may  amend or modify or update these Terms from time to time at its discretion. You  will be responsible for reviewing these Terms regularly and ensuring compliance  with these Terms. Your use of the Platforms after such amendment or alteration  will be deemed to be expressing  your  consent to the Amended / modified / revised Terms and be bound by those Amended  / Modified / Updated Terms.<br>
  </p>
<h4>Definitions</h4>
<h5>User.</h5>
<p>&quot;User (s)&quot;  or &quot;You&quot; or &quot;Yours&quot; refers to you, as a Platform user. User  is a person who accesses or uses the Platforms for the purpose of ordering,  sharing, displaying, hosting, publishing, creating, or uploading information or  views or images and includes other persons participating in the use of the  Platforms including unlimited user access to 'merchant business page' wanted  business listings or otherwise.</p>
<h5> 
  Content</h5>
<p> 
&quot;Content&quot; will include but not limited to, updates,  photos, images, audio, video, location data, nearby locations, and all other  types of information or data. &quot;Your Content&quot; or &quot;User  Content&quot; means content that you upload, share or transfer to, using or  linking to Platforms, such as likes, ratings, reviews, photos,messages, profile  information, or any other items you display publicly or displayed on your  account profile. &ldquo;AlfaBee&rdquo; Content&quot; means content created by &ldquo;AlfaBee&rdquo; and  made available in relation to Services including, but not limited to, visual  connectors, interactive features, graphics, design, integration, computer code,  products, software, integrated ratings, reports and other use of related data  in connection with the activities associated with your account and all other objects  and objects of the Platform other than your content and third party content.  &quot;External Company Content&quot; means content from third parties other  than &ldquo;AlfaBee&rdquo; or its users and is available on platforms.</p>
<h5> Vendor(s)/Establishment(s)</h5>
<p>&quot;Vendor&rdquo; means  the institutions listed on &ldquo;AlfaBee&rdquo; and any &ldquo;AlfaBee&rdquo; mobile or software  related applications.<strong></strong></p>
<h5>  User(s)</h5>
<p>  User is eligible to  use the Platforms only if they are legally able to enter into binding  agreements and are up to 18 years. He does not intend to sell or deliver  alcohol in any form, drugs and / or narcotics.<strong></strong><br>
The user (s) will  not share a unique password with anyone. Users have a responsibility to keep  the password secure and confidential. In the event of fraud or abuse of the  user account which is not the user's fault, the user accepts that all orders  placed under the user's &ldquo;AlfaBee&rdquo; account are solely the responsibility of the  user.</p>
<h5> 
  Deliveries</h5>
<p>  Your orders will be  shipped to only verified residential and commercial addresses. The delivery  times quoted are approximate and may vary depending on various factors that may  be beyond our control. Goods will be shipped to the address provided at the  time of ordering Goods. You will be responsible for providing accurate and  complete information at the time of ordering. Delivery costs will be reimbursed  to us even if the delivery is delayed.<br>
  In the event that  you do not agree to the delivery of the goods at the time they are ready to be  delivered or we may not deliver the Goods to you due to lack of proper instructions  or authorization it will be deemed to have been delivered to you. Therefore,  you will be responsible for any storage, delivery, insurance and shipping costs  and you will pay us in full for all associated costs.<br>
  In the unlikely  event that you receive the wrong items from us, you have the right to refuse  delivery of these Goods without being liable to pay for these Poor Goods.<br>
Assets will become  your property only after we have received all payments due in respect of goods  including delivery and tax costs, if any. User has agreed to thoroughly inspect  the goods before accepting delivery. Once the delivery has been accepted, we  will not take any responsibility for any Defects in the Goods.</p>
<h5>Special Deliveries</h5>
<p>The delivery charges mentioned in the app are applicable only for standard deliveries with maximum weight of 25kg. In case of any delivery order weighing more than 25Kg shall be treated as special delivery. The rates for special deliveries shall be advised per order before processing the order and the order shall be processed only once these charges are agreed.</p>

<h5>Restrictions</h5>
<p>  Without limiting the validity of these Terms, by  using the Platforms, you expressly agree not to post or transmit any content  (including reviews) or to participate in any activity which, in our sole  discretion:</p>


<ol>
  <li>Violation of our guidelines and policies;</li>
  <li>Is injurious, abusive, harassing, hostile, offensive,  defamatory, discriminatory, vulgar, profane, obscene, libelous, hateful,  threatening or otherwise objectionable, invasive of another's privacy, relating  or encouraging money laundering or gambling</li>
  <li>Creates true or false reviews that are intentionally  made, or do not refer to items and services, environment, or other attributes  of the business you are reviewing.</li>
  <li>Contains content that violates the standards of good  taste or standards of Platforms;</li>
  <li>Infringes any third party rights, including, but not  limited to, right of privacy, right of publicity, copyright, trademark, patent,  trade secret, or any other intellectual property or proprietary rights;</li>
  <li>Alleges others of illegal activity or describes  physical altercations.</li>
  <li>Any matter relating to health code violations that  requires reporting to the health department. </li>
  <li>Is it illegal, or infringing on any federal, state,  or local law (for example, by disclosing or trading internal information in  violation of security law);</li>
  <li>Attempts to impersonate another person or  organization;</li>
  <li>To  conceal or attempt to conceal the origin of your content, including but not  limited to: (i) posting your content under false or misleading name; or (ii)  concealing or attempting to conceal the IP address to which your content was sent;</li>
  <li>Creates  a deceptive advertisement or cause, or is the result of a conflict of interest;</li>
  <li>Whether  commercial in nature, including but not limited to spam, surveys, contests,  pyramid schemes, posts or reviews submitted or removed for the purpose of payment,  posts or reviews submitted or removed or at the request of the revised  business, or other promotional material;</li>
  <li>Submit  or suggest that your content is sponsored or approved in any way;</li>
  <li>Contains  non-English material or, in the case of products or services offered in foreign  languages, the appropriate language for those products or services;</li>
  <li>Lies,  misrepresents or obscures your association with another person or organization;</li>
  <li>Accesses  or uses another user's account without permission;</li>
  <li>Distributes  computer viruses or other code, files, or programs that interfere with, damage,  or reduce the performance of any computer software or hardware or electronic  communications equipment</li>
  <li>Interfers  with, interrupts, or disrupts the operation or use of any features of the  Services or servers or networks connected to platforms;</li>
  <li>&quot;Hacks&quot;  or unauthorized access to our personal or private records, records of another  user, or of another person;</li>
  <li>Violates  any contract or fraudulent relationship (for example, by disclosing the  personal or confidential information of your employer or client in violation of  any employment, consultation, or non-disclosure agreement);</li>
  <li>Dismiss,  change engineers, disassemble or otherwise attempt to obtain source code on  Platforms;</li>
  <li>Delete,  reduce, disable, damage or interfere with security-related features, or  features that impose restrictions on the use of Platforms;</li>
  <li>Controls  restrictions on any of the Robot Release topics in the Services, if any, or  bypasses or byputs other measures used to prevent or limit access to Platforms;</li>
  <li>Collects,  accesses, or stores personal information about other platform users;</li>
  <li>Posted  by bot;</li>
  <li>It  hurts children in every way;</li>
  <li>It  threatens the unity, integrity, defense, security or sovereignty of Pakistan,  friendly relations with foreign nations, or social order or causes the  promotion of any apparent crime or hinders the investigation of any crime or  defamation of any other nation;</li>
  <li>Alters,  copies, deletes or crawls, displays, publishes, licenses, sells, rents, leases,  borrows, transfers or sells other rights to Platforms or our content; or</li>
  <li>Attempts  to do any of the above.</li>
</ol>
 
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	 </div>
	 	 </div>
</section>


<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


