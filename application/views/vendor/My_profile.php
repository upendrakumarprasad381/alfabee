<?php
$Session = GetSessionArrayVendor();
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="<?=base_url('Vendor')?>"><?php echo $this->lang->line("home")?></a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span><?php echo $this->lang->line("update_profile")?></span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold"><?php echo $this->lang->line("update_profile")?></span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="UserId" value="<?= $Session->id ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("full_name")?></label>
                                    <div class="col-md-6">
                                        <input id="name" value="<?= isset($Session->name) ? $Session->name : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                <!--<div class="form-group">-->
                                <!--    <label class="control-label col-md-3">Last Name</label>-->
                                <!--    <div class="col-md-6">-->
                                <!--        <input id="last_name" value="<?= isset($Session->last_name) ? $Session->last_name : '' ?>" type="text" class="form-control">-->
                                <!--    </div>-->
                                <!--</div>-->
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("mobile")?></label>
                                    <div class="col-md-6">
                                       <input id="mobile_number" value="<?= isset($Session->mobile_number) ? $Session->mobile_number : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("email")?></label>
                                    <div class="col-md-6">
                                       <input id="email" value="<?= isset($Session->email) ? $Session->email : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("password")?></label>
                                    <div class="col-md-6">
                                       <input id="password" value="" type="password" class="form-control">
                                    </div>
                                </div>
                                <!--<div class="form-group">-->
                                <!--    <label class="control-label col-md-3">Cuisine Type</label>-->
                                <!--    <div class="col-md-6">-->
                                <!--       <select class="form-control selectpicker multiselect-box" data-live-search="true" id="cuisine_id" name="rest_cuisine[]" multiple>-->
                                <!--            <option value="">Select Cuisine</option>-->
                                <!--            <?=get_cuisine(isset($Cuisine->cuisine_id) ? $Cuisine->cuisine_id : '' )?>-->
                                <!--        </select>-->
                                <!--    </div>-->
                                <!--</div>-->
                                <!--<div class="form-group">-->
                                <!--    <label class="control-label col-md-3">Opening Time</label>-->
                                <!--    <div class="col-md-2">-->
                                <!--        <div class="input-group">-->
                                <!--            <input type="text" class="form-control timepicker timepicker-no-seconds" id="opening_time">-->
                                <!--            <span class="input-group-btn">-->
                                <!--                <button class="btn default" type="button">-->
                                <!--                    <i class="fa fa-clock-o"></i>-->
                                <!--                </button>-->
                                <!--            </span>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--    <label class="control-label col-md-2">Closing Time</label>-->
                                <!--    <div class="col-md-2">-->
                                <!--        <div class="input-group">-->
                                <!--            <input type="text" class="form-control timepicker timepicker-no-seconds" id="closing_time">-->
                                <!--            <span class="input-group-btn">-->
                                <!--                <button class="btn default" type="button">-->
                                <!--                    <i class="fa fa-clock-o"></i>-->
                                <!--                </button>-->
                                <!--            </span>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--</div>-->
                                
                                <!--<div class="form-group">-->
                                <!--    <label class="control-label col-md-3">Upload Image</label>-->
                                <!--    <div class="col-md-6">-->
                                <!--    <input type="file" class="custom-file-input" id="CommonImages" src=""><br>-->
                                <!--    <a target="_blank" href="<?=$Session->LogoFullURL?>">-->
                                <!--        <img src=" <?php if(isset($Session->image) && $Session->image!=""){ echo base_url().'uploads/vendor_images/'.$Session->image;} ?>" width="20%" id="ImagesEncode" OldImage='<?= !empty($Session->image) ? 'uploads/vendor_images/' . $Session->image : '' ?>'>-->
                                <!--    </a>-->
                                <!--    <?php if(isset($Session->image) && $Session->image!=""){ ?>-->
                                <!--        <br><a class="remove_image_common" path="uploads/vendor_images/" name="<?=isset($Session->image)?$Session->image:''?>" table="users" id="<?=$Session->id?>" type="">Remove</a>-->
                                <!--      <?php } ?>-->
                                <!--    </div>-->
                                <!--</div>-->
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="UpdateProfile" class="btn blue">
                                            <i class="fa fa-check"></i> <?php echo $this->lang->line("submit")?></button>
                                        <button type="button" onclick="window.location = '<?=base_url('vendor_dashboard')?>';" class="btn default"><?php echo $this->lang->line("cancel")?></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
     </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
