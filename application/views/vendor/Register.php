<html lang="en">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Register</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/bootstrap/css/'); ?>bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/fonts/font-awesome-4.7.0/css/'); ?>font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/fonts/Linearicons-Free-v1.0.0/'); ?>icon-font.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/fonts/iconic/css/'); ?>material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/animate/'); ?>animate.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/css-hamburgers/'); ?>hamburgers.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/animsition/css/'); ?>animsition.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/select2/'); ?>select2.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/daterangepicker/'); ?>daterangepicker.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/css/'); ?>util.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/css/'); ?>main.css">
    </head>
    <body style="background-color: #999999;">

        <div class="limiter">
            <div class="container-login100">
                <div class="login100-more" style="background-image: url('<?= base_url('assets/AdminLogin/') ?>images/bg-01.jpg');"></div>

                <div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">
                    <form class="login100-form1">
                        <span class="login100-form-title p-b-59">
                            HomeEats
                        </span>
                        <div style="width: 100%;text-align: center;" id="MessageDiv">
                           
                        </div>
                        <br>
                        <div class="wrap-input100 validate-input" data-validate="Firstname is required">
                            <span class="label-input100">First Name</span>
                            <input id="first_name" class="input100" type="text" name="first_name" placeholder="First Name">
                            <span class="focus-input100"></span>
                        </div>
                        
                        <div class="wrap-input100 validate-input" data-validate="Lastname is required">
                            <span class="label-input100">Last Name</span>
                            <input id="last_name" class="input100" type="text" name="last_name" placeholder="Last Name">
                            <span class="focus-input100"></span>
                        </div>
                        
                        <div class="wrap-input100 validate-input" data-validate="Restaurant Name is required">
                            <span class="label-input100">Restaurant Name</span>
                            <input id="restaurant_name" class="input100" type="text" name="restaurant_name" placeholder="Restaurant Name">
                            <span class="focus-input100"></span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate ="Email is required">
                            <span class="label-input100">Email address</span>
                            <input class="input100" type="email" id="email" name="email" placeholder="Email">
                            <span class="focus-input100"></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate ="Mobile number is required">
                            <span class="label-input100">Phone Number</span>
                            <input class="input100" type="text" id="phone" name="phone" placeholder="Mobile Number">
                            <span class="focus-input100"></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate ="City is required">
                            <span class="label-input100">City</span>
                            <input class="input100" type="text" id="city" name="city" placeholder="City">
                            <span class="focus-input100"></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate ="Area is required">
                            <span class="label-input100">Area</span>
                            <input class="input100" type="text" id="area" name="area" placeholder="Area">
                            <span class="focus-input100"></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate = "Street is required">
                            <span class="label-input100">Street</span>
                            <input class="input100" type="text" id="street" name="street" placeholder="Street">
                            <span class="focus-input100"></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate = "Password is required">
                            <span class="label-input100">Password</span>
                            <input class="input100" type="password" id="password" name="password" placeholder="Password">
                            <span class="focus-input100"></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate = "Password is required">
                            <span class="label-input100">Confirm Password</span>
                            <input class="input100" type="password" id="cpassword" name="cpassword" placeholder="Confirm Password">
                            <span class="focus-input100"></span>
                        </div>


                        <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <button type="button" class="login100-form-btn register_submit">
                                    Register   
                                </button>
                            </div>
                        </div>
                        <span class="register_message common_message"></span>
                    </form>
                </div>
            </div>
        </div>
        <script>
            var base_url = '<?= base_url() ?>';
            var segment2 = '<?= $this->uri->segment(2) ?>';
            var segment3 = '<?= $this->uri->segment(3) ?>';
            var segment4 = '<?=  $this->uri->segment(4) ?>';
            var controller = '<?= $this->router->fetch_class() ?>';
            var method = '<?= $this->router->fetch_method() ?>';
        </script>
        <script>var base_url = '<?= base_url() ?>';</script>
        <script src="<?=base_url()?>js/additional/jquery.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
        <script src="<?=base_url()?>js/additional/bootstrap.bundle.min.js"></script>
        <script src="<?=base_url()?>js/additional/app.js"></script>
        <script src="<?=base_url()?>js/vendor.js"></script>
    </body>
</html>