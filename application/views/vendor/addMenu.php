<?php
$Session = GetSessionArrayVendor();
$Qry = "SELECT promocode_id,title FROM promo_code WHERE offer_addedby=" . $Session->id;
$Array = $this->Database->select_qry_array($Qry);
?>
<style>
    .close_btn{
        margin-top:25px;
    }
    @media (max-width: 1520px) {
        .close_btn {
            margin-top: 0;
        }
    }
    .img-size-common {
        color: #f01818 !important;
        font-size: 66% !important;
        font-weight: 600 !important;
    }
    .up_tm_box {
        float: left;
        width: 79px;
        margin: 0 10px 10px 0;
        border: #ccc 1px solid;
        padding: 5px;
        text-align: center;
        font-size: 12px;
    }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">

                            <span class="caption-subject bold uppercase"><?= empty($Id) ? $this->lang->line("add_new") : $this->lang->line("update"); ?> <?php echo $this->lang->line("menu") ?></span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="Id" value="<?= $Id ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("category") ?></label>
                                    <div class="col-md-6" id="category_list">
                                        <input type="hidden" id="store_type" value="<?= $Session->store_type ?>">
                                        <select class="form-control selectpicker" data-live-search="true" id="category" onchange="addCategory()">
                                            <option value=""><?php echo $this->lang->line("select_category") ?></option>
                                            <!--<option value=""><php echo $this->lang->line("add_new_category") ?></option>-->
                                            <?php
                                            if (isset($Categories->category_id)) {
                                                $cat_id = $Categories->category_id;
                                            } else {
                                                $cat_id = '';
                                            }
                                            ?>
                                            <?= get_category($cat_id, $Session->store_type) ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="add_category">
                                    <label class="control-label col-md-3"></label>
                                    <div class="col-md-6" id="other_category"> 
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("menu") ?> <?php echo $this->lang->line("in_english") ?></label>
                                    <div class="col-md-6">
                                        <input value="<?= !empty($Categories->menu_name) ? $Categories->menu_name : '' ?>" id="menu_name" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("menu") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                    <div class="col-md-6">
                                        <input value="<?= !empty($Categories->menu_name_ar) ? $Categories->menu_name_ar : '' ?>" id="menu_name_ar" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Upload Menu Image</label>
                                    <div class="col-md-6">
                                        <input type="file" class="custom-file-input" id="CommonImages" src="">
                                    </div>
                                    <div class="col-md-3">

                                        <a target="_blank" href="<?php
                                        if (isset($Categories->image) && $Categories->image != "") {
                                            echo base_url() . 'uploads/menu/' . $Categories->image;
                                        }
                                        ?>">
                                            <img src=" <?php
                                            if (isset($Categories->image) && $Categories->image != "") {
                                                echo base_url() . 'uploads/menu/' . $Categories->image;
                                            }
                                            ?>" width="20%" id="ImagesEncode" OldImage='<?= !empty($Categories->image) ? 'uploads/menu/' . $Categories->image : '' ?>'>
                                        </a>
                                        <?php if (isset($Categories->image) && $Categories->image != "") { ?>
                                            <br><a class="remove_image_common" path="uploads/menu/" name="<?= isset($Categories->image) ? $Categories->image : '' ?>" table="menu_list" id="<?= $Id ?>" type="">Remove</a>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php
                                if (isset($Categories->product_images) && $Categories->product_images != "") {
                                    $image = explode(",", $Categories->product_images);
                                    ?>
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-8">
                                            <?php for ($i = 0; $i < count($image); $i++) { ?>
                                                <div class="up_tm_box">
                                                    <a target="_blank" href="<?php
                                                    if (isset($image[$i]) && $image[$i] != "") {
                                                        echo base_url() . 'uploads/product_images/' . $image[$i];
                                                    }
                                                    ?>">
                                                        <img src=" <?php
                                                        if (isset($image[$i]) && $image[$i] != "") {
                                                            echo base_url() . 'uploads/product_images/' . $image[$i];
                                                        }
                                                        ?>" id="ImagesEncode_<?= $i ?>" OldImage='<?= !empty($image[$i]) ? $image[$i] : '' ?>' class="more_images_cls img-responsive" newimage=''>
                                                    </a> 
                                                    <?php if (isset($image[$i]) && $image[$i] != "") { ?>
                                                        <a class="remove_image_common" path="uploads/product_images/" name="<?= isset($image[$i]) ? $image[$i] : '' ?>" table="menus" id="<?= $Id ?>" type="multiple" field="product_images">Remove</a>
                                                    <?php } ?>
                                                </div>   
                                            <?php } ?>

                                        </div>
                                    </div>

                                    <?php
                                }
                                ?> 
                                <div id="more_upload_section">
                                    <div class="form-group" style="display: none;">
                                        <label class="control-label col-md-3">Upload More Menu Images

                                        </label>
                                        <div class="col-md-6">
                                            <?php $rand = rand(); ?>
                                            <input type="file" class="custom-file-input MultipleImages" id="" refval="<?= $rand ?>" src="">
                                        </div>
                                        <div class="col-md-3">

                                            <a target="_blank" href="">
                                                <img src="" width="20%" id="ImagesEncode_<?= $rand ?>" OldImage='' class="more_images_cls" newimage=''>
                                            </a> 
                                            <button type="button" class="btn btn-info" id="add_menu_images" inputclassname="more_images_cls">
                                                <span class="glyphicon glyphicon-plus"></span> Add More
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("description") ?> <?php echo $this->lang->line("in_english") ?></label>
                                    <div class="col-md-6">
                                        <textarea class="form-control summernote" id="description" rows="10"><?= !empty($Categories->description) ? $Categories->description : '' ?></textarea>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("description") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                    <div class="col-md-6">
                                        <textarea class="form-control summernote" id="description_ar" rows="10"><?= !empty($Categories->description_ar) ? $Categories->description_ar : '' ?></textarea>  
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo $this->lang->line("price") ?></label>
                                    <div class="input-group col-md-6 ___arddv">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>
                                        </span>
                                        <input value="<?= !empty($Categories->price) ? $Categories->price : '' ?>" id="product_price" type="number" class="form-control col-md-3" autocomplete="off">

                                    </div>
                                </div>
                                <?php
                                if ($Session->store_type == 1) {
                                    ?>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo $this->lang->line("choices") ?></label>
                                        <div class="col-md-6 text-left pt-1">

                                            <input type="checkbox" id="choice" name="choice" value="1" <?php echo (isset($Categories->choice) && $Categories->choice == 1 ? 'checked="checked"' : ''); ?>>
                                            <label><?php echo $this->lang->line("yes") ?></label>

                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php
                                if ($Session->store_type != 1) {
                                    ?>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Available Stock</label>
                                        <div class="col-md-6">
                                            <input value="<?= !empty($Categories->stock) ? $Categories->stock : '' ?>" id="stock" type="number" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Max Purchase Quantity<small><br>(If zero user can purchase n number of quantity)</small></label>
                                        <div class="col-md-6">
                                            <input value="<?= !empty($Categories->max_purchase_qty) ? $Categories->max_purchase_qty : '0' ?>" id="max_purchase_qty" type="number" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>



                                <div class="form-group" style="display: <?= $Session->store_type == '3' ? 'block' : 'none' ?>">
                                    <label class="control-label col-md-3">Prescription required</label>
                                    <div class="col-md-6">
                                        <?php
                                        $prescriptionreq = !empty($Categories->prescriptionreq) ? $Categories->prescriptionreq : '';
                                        ?>
                                        <input type="checkbox" id="prescriptionrequired" <?= !empty($prescriptionreq) ? "checked" : '' ?> name="prescriptionrequired" value="1">
                                        <label>Yes</label>
                                    </div>
                                </div>





                                <div class="add_choices">
                                    <div class="form-group __latextrit" id="choice_for_yes">
                                        <label class="col-md-3 control-label"><?php echo $this->lang->line("size") ?></label>
                                        <div class="col-md-9">
                                            <div class="mt-repeater">
                                                <div data-repeater-list="group-b">
                                                    <?php
                                                    if (isset($Menu) && count($Menu) > 0) {
                                                        foreach ($Menu as $d) {
                                                            ?>
                                                            <div data-repeater-item class="row">
                                                                <div class="col-md-4">
                                                                    <label class="control-label"><?php echo $this->lang->line("size") ?>

                                                                    </label>
                                                                    <select class="form-control" name="size[]" id="size">
                                                                        <option value="" disabled><?php echo $this->lang->line("select_size") ?></option>
                                                                        <option value="Small" <?php if ($d->menu_size == 'Small') echo ' selected="selected"'; ?>><?php echo $this->lang->line("small") ?></option>
                                                                        <option value="Medium" <?php if ($d->menu_size == 'Medium') echo ' selected="selected"'; ?>><?php echo $this->lang->line("medium") ?></option>
                                                                        <option value="Large" <?php if ($d->menu_size == 'Large') echo ' selected="selected"'; ?>><?php echo $this->lang->line("large") ?></option>


                                                                        <option value="Half" <?php if ($d->menu_size == 'Half') echo ' selected="selected"'; ?>><?php echo $this->lang->line("Half") ?></option>
                                                                        <option value="Full" <?php if ($d->menu_size == 'Full') echo ' selected="selected"'; ?>><?php echo $this->lang->line("Full") ?></option>
                                                                    </select>
                                                                    <!--<input type="text" class="form-control" name="size[]" value="<?= !empty($d->menu_size) ? $d->menu_size : '' ?>" id="size"/> -->
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label class="control-label"><?php echo $this->lang->line("price") ?>
                                                                        <span class="img-size-common">(This will add along with Base Price)</span>
                                                                    </label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-btn">
                                                                            <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>
                                                                        </span>
                                                                        <input type="text"  class="form-control" name="price[]" value="<?= !empty($d->menu_price) ? $d->menu_price : '' ?>" id="price"/> </div>
                                                                    <input type="hidden"  class="form-control" name="size_id[]" value="<?= !empty($d->menu_size_id) ? $d->menu_size_id : '' ?>" id="size_id"/>
                                                                </div>

                                                                <div class="col-md-1 close_btn">
                                                                    <label class="control-label">&nbsp;</label>
                                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                                        <i class="fa fa-close"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                    }else {
                                                        ?>
                                                        <div data-repeater-item class="row">
                                                            <div class="col-md-4">
                                                                <label class="control-label"><?php echo $this->lang->line("size") ?>

                                                                </label>
                                                                <select class="form-control" name="size[]" id="size">
                                                                    <option value=""><?php echo $this->lang->line("select_size") ?></option>
                                                                    <option value="Small"><?php echo $this->lang->line("small") ?></option>
                                                                    <option value="Medium"><?php echo $this->lang->line("medium") ?></option>
                                                                    <option value="Large"><?php echo $this->lang->line("large") ?></option>

                                                                    <option value="Half"><?php echo $this->lang->line("Half") ?></option>
                                                                    <option value="Full"><?php echo $this->lang->line("Full") ?></option>
                                                                </select>
                                                            <!--<input type="text" class="form-control" name="size[]" value="<?= !empty($d->menu_size) ? $d->menu_size : '' ?>" id="size"/> -->
                                                            </div>


                                                            <div class="col-md-4">
                                                                <label class="control-label"><?php echo $this->lang->line("price") ?>
                                                                    <span class="img-size-common">(This will add along with Base Price)</span>
                                                                </label>
                                                                <div class="input-group">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>
                                                                    </span>
                                                                    <input type="text"  class="form-control col-md-3" name="price[]" value="<?= !empty($d->menu_price) ? $d->menu_price : '' ?>" id="price"/> </div>
                                                            </div>
                                                            <div class="col-md-1 close_btn">
                                                                <label class="control-label">&nbsp;</label>
                                                                <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                                    <i class="fa fa-close"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                                <hr>
                                                <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                                    <i class="fa fa-plus"></i> <?php echo $this->lang->line("add_size") ?></a>
                                                <br>
                                                <br> </div>


                                        </div>

                                    </div>

                                    <div class="form-group __latextrit" id="choice_for_yes_addon">
                                        <label class="col-md-3 control-label"><?php echo $this->lang->line("add_ons") ?></label>
                                        <div class="col-md-9">
                                            <div class="mt-repeater">
                                                <div data-repeater-list="group-b">
                                                    <?php
                                                    if (isset($Addon) && count($Addon) > 0) {

                                                        foreach ($Addon as $ad) {
                                                            ?>
                                                            <div data-repeater-item class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label"><?php echo $this->lang->line("add_ons_name") ?> <?php echo $this->lang->line("in_english") ?></label>
                                                                    <input type="text" class="form-control" name="add_on[]" value="<?= !empty($ad->add_on) ? $ad->add_on : '' ?>" id="add_on"/> 
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <label class="control-label"><?php echo $this->lang->line("add_ons_name") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                                                    <input type="text" class="form-control" name="addonar[]" value="<?= !empty($ad->add_on_ar) ? $ad->add_on_ar : '' ?>" id="addonar"/> 
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label class="control-label"><?php echo $this->lang->line("price") ?></label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-btn">
                                                                            <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>
                                                                        </span>
                                                                        <input type="text"  class="form-control" name="addons_price[]" value="<?= !empty($ad->addon_price) ? $ad->addon_price : '' ?>" id="addons_price"/> </div>
                                                                    <input type="hidden"  class="form-control" name="addon_id[]" value="<?= !empty($ad->menu_addon_id) ? $ad->menu_addon_id : '' ?>" id="addon_id"/>
                                                                </div>

                                                                <div class="col-md-1 close_btn">
                                                                    <label class="control-label">&nbsp;</label>
                                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                                        <i class="fa fa-close"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <div data-repeater-item class="mt-repeater-add">
                                                            <div class="col-md-5">
                                                                <label class="control-label"><?php echo $this->lang->line("add_ons_name") ?> <?php echo $this->lang->line("in_english") ?></label>
                                                                <input type="text" class="form-control" name="add_on[]" value="<?= !empty($ad->add_on) ? $ad->add_on : '' ?>" id="add_on"/> 
                                                            </div>
                                                            <div class="col-md-5">
                                                                <label class="control-label"><?php echo $this->lang->line("add_ons_name") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                                                <input type="text" class="form-control" name="addonar[]" value="<?= !empty($ad->add_on_ar) ? $ad->add_on_ar : '' ?>" id="addonar"/> 
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label class="control-label text-left"><?php echo $this->lang->line("price") ?></label>
                                                                <div class="input-group">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>
                                                                    </span>
                                                                    <input type="text"  class="form-control" name="addons_price[]" value="<?= !empty($ad->addon_price) ? $ad->addon_price : '' ?>" id="addons_price"/> </div>
                                                            </div>

                                                            <div class="col-md-1 close_btn">
                                                                <label class="control-label">&nbsp;</label>
                                                                <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                                    <i class="fa fa-close"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                                <hr>
                                                <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                                    <i class="fa fa-plus"></i> <?php echo $this->lang->line("add_addon") ?></a>
                                                <br>
                                                <br> </div>


                                        </div>

                                    </div>

                                    <div class="form-group __latextrit" id="choice_for_yes_topping">
                                        <label class="col-md-3 control-label"><?php echo $this->lang->line("topping") ?></label>
                                        <div class="col-md-9">
                                            <div class="mt-repeater">
                                                <div data-repeater-list="group-b">
                                                    <?php
                                                    if (isset($Topping) && count($Topping) > 0) {
                                                        foreach ($Topping as $top) {
                                                            ?>
                                                            <div data-repeater-item class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label"><?php echo $this->lang->line("topping_name") ?> <?php echo $this->lang->line("in_english") ?></label>
                                                                    <input type="text" class="form-control" name="topping[]" value="<?= !empty($top->topping) ? $top->topping : '' ?>" id="topping_name"/> </div>
                                                                <div class="col-md-5">
                                                                    <label class="control-label"><?php echo $this->lang->line("topping_name") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                                                    <input type="text" class="form-control" name="topping_ar[]" value="<?= !empty($top->topping_ar) ? $top->topping_ar : '' ?>" id="toppingname_ar"/> </div>
                                                                <div class="col-md-4">
                                                                    <label class="control-label"><?php echo $this->lang->line("price") ?></label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-btn">
                                                                            <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>
                                                                        </span>
                                                                        <input type="text"  class="form-control" name="topping_price[]" value="<?= !empty($top->topping_price) ? $top->topping_price : '' ?>" id="topping_price"/> </div>
                                                                    <input type="hidden"  class="form-control" name="topping_id[]" value="<?= !empty($top->menu_topping_id) ? $top->menu_topping_id : '' ?>" id="topping_id"/>
                                                                </div>

                                                                <div class="col-md-1 close_btn">
                                                                    <label class="control-label">&nbsp;</label>
                                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                                        <i class="fa fa-close"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <div data-repeater-item class="row">
                                                            <div class="col-md-5">
                                                                <label class="control-label"><?php echo $this->lang->line("topping_name") ?> <?php echo $this->lang->line("in_english") ?></label>
                                                                <input type="text" class="form-control" name="topping[]" value="<?= !empty($top->topping) ? $top->topping : '' ?>" id="topping_name"/> </div>
                                                            <div class="col-md-5">
                                                                <label class="control-label"><?php echo $this->lang->line("topping_name") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                                                <input type="text" class="form-control" name="topping_ar[]" value="<?= !empty($top->topping_ar) ? $top->topping_ar : '' ?>" id="toppingname_ar"/> </div>

                                                            <div class="col-md-4">
                                                                <label class="control-label"><?php echo $this->lang->line("price") ?></label>
                                                                <div class="input-group">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>
                                                                    </span>
                                                                    <input type="text"  class="form-control" name="topping_price[]" value="<?= !empty($top->topping_price) ? $top->topping_price : '' ?>" id="topping_price"/> </div>
                                                            </div>

                                                            <div class="col-md-1 close_btn">
                                                                <label class="control-label">&nbsp;</label>
                                                                <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                                    <i class="fa fa-close"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                                <hr>
                                                <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                                    <i class="fa fa-plus"></i> <?php echo $this->lang->line("add_topping") ?></a>
                                                <br>
                                                <br> </div>


                                        </div>

                                    </div>

                                    <div class="form-group __latextrit" id="choice_for_yes_drink" style="display:none;">
                                        <label class="col-md-3 control-label"><?php echo $this->lang->line("drink") ?></label>
                                        <div class="col-md-9">
                                            <div class="mt-repeater">
                                                <div data-repeater-list="group-b">
                                                    <?php
                                                    if (isset($Drink) && count($Drink) > 0) {
                                                        foreach ($Drink as $drink) {
                                                            ?>
                                                            <div data-repeater-item class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label"><?php echo $this->lang->line("drink_name") ?> <?php echo $this->lang->line("in_english") ?></label>
                                                                    <input type="text" class="form-control" name="drink[]" value="<?= !empty($drink->drink_name) ? $drink->drink_name : '' ?>" id="drink_name"/> </div>
                                                                <div class="col-md-5">
                                                                    <label class="control-label"><?php echo $this->lang->line("drink_name") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                                                    <input type="text" class="form-control" name="drink_ar[]" value="<?= !empty($drink->drink_name_ar) ? $drink->drink_name_ar : '' ?>" id="drinkname_ar"/> </div>

                                                                <div class="col-md-4">
                                                                    <label class="control-label"><?php echo $this->lang->line("price") ?></label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-btn">
                                                                            <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>
                                                                        </span>
                                                                        <input type="text"  class="form-control" name="drink_price[]" value="<?= !empty($drink->drink_price) ? $drink->drink_price : '' ?>" id="drink_price"/> </div>
                                                                </div>

                                                                <div class="col-md-1 close_btn">
                                                                    <label class="control-label">&nbsp;</label>
                                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                                        <i class="fa fa-close"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <div data-repeater-item class="row">
                                                            <div class="col-md-5">
                                                                <label class="control-label"><?php echo $this->lang->line("drink_name") ?> <?php echo $this->lang->line("in_english") ?></label>
                                                                <input type="text" class="form-control" name="drink[]" value="<?= !empty($drink->drink_name) ? $drink->drink_name : '' ?>" id="drink_name"/> </div>
                                                            <div class="col-md-5">
                                                                <label class="control-label"><?php echo $this->lang->line("drink_name") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                                                <input type="text" class="form-control" name="drink_ar[]" value="<?= !empty($drink->drink_name_ar) ? $drink->drink_name_ar : '' ?>" id="drinkname_ar"/> </div>

                                                            <div class="col-md-4">
                                                                <label class="control-label"><?php echo $this->lang->line("price") ?></label>
                                                                <div class="input-group">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>
                                                                    </span>
                                                                    <input type="text"  class="form-control" name="drink_price[]" value="<?= !empty($drink->drink_price) ? $drink->drink_price : '' ?>" id="drink_price"/> </div>
                                                            </div>

                                                            <div class="col-md-1 close_btn">
                                                                <label class="control-label">&nbsp;</label>
                                                                <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                                    <i class="fa fa-close"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                                <hr>
                                                <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                                    <i class="fa fa-plus"></i> <?php echo $this->lang->line("add_drink") ?></a>
                                                <br>
                                                <br> </div>


                                        </div>

                                    </div>

                                    <div class="form-group __latextrit" id="choice_for_yes_dips">
                                        <label class="col-md-3 control-label"><?php echo $this->lang->line("dips") ?></label>
                                        <div class="col-md-9">
                                            <div class="mt-repeater">
                                                <div data-repeater-list="group-b">
                                                    <?php
                                                    if (isset($Dips) && count($Dips) > 0) {
                                                        foreach ($Dips as $dips) {
                                                            ?>
                                                            <div data-repeater-item class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label"><?php echo $this->lang->line("dips") ?> <?php echo $this->lang->line("in_english") ?></label>
                                                                    <input type="text" class="form-control" name="dips[]" value="<?= !empty($dips->dips) ? $dips->dips : '' ?>" id="dips"/> </div>
                                                                <div class="col-md-5">
                                                                    <label class="control-label"><?php echo $this->lang->line("dips") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                                                    <input type="text" class="form-control" name="dips_ar[]" value="<?= !empty($dips->dips_ar) ? $dips->dips_ar : '' ?>" id="dip_ar"/> </div>

                                                                <div class="col-md-4">
                                                                    <label class="control-label"><?php echo $this->lang->line("price") ?></label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-btn">
                                                                            <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>
                                                                        </span>
                                                                        <input type="text"  class="form-control" name="dip_price[]" value="<?= !empty($dips->dip_price) ? $dips->dip_price : '' ?>" id="dip_price"/> </div>
                                                                    <input type="hidden"  class="form-control" name="dip_id[]" value="<?= !empty($dips->menu_dip_id) ? $dips->menu_dip_id : '' ?>" id="dip_id"/>
                                                                </div>

                                                                <div class="col-md-1 close_btn">
                                                                    <label class="control-label">&nbsp;</label>
                                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                                        <i class="fa fa-close"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <div data-repeater-item class="row">
                                                            <div class="col-md-5">
                                                                <label class="control-label"><?php echo $this->lang->line("dips") ?> <?php echo $this->lang->line("in_english") ?></label>
                                                                <input type="text" class="form-control" name="dips[]" value="<?= !empty($dips->dips) ? $dips->dips : '' ?>" id="dips"/> </div>
                                                            <div class="col-md-5">
                                                                <label class="control-label"><?php echo $this->lang->line("dips") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                                                <input type="text" class="form-control" name="dips_ar[]" value="<?= !empty($dips->dips_ar) ? $dips->dips_ar : '' ?>" id="dip_ar"/> </div>

                                                            <div class="col-md-4">
                                                                <label class="control-label"><?php echo $this->lang->line("price") ?></label>
                                                                <div class="input-group">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>
                                                                    </span>
                                                                    <input type="text"  class="form-control" name="dip_price[]" value="<?= !empty($dips->dip_price) ? $dips->dip_price : '' ?>" id="dip_price"/> </div>
                                                            </div>

                                                            <div class="col-md-1 close_btn">
                                                                <label class="control-label">&nbsp;</label>
                                                                <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                                    <i class="fa fa-close"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                                <hr>
                                                <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                                    <i class="fa fa-plus"></i> <?php echo $this->lang->line("add_dips") ?></a>
                                                <br>
                                                <br> </div>


                                        </div>

                                    </div>

                                    <div class="form-group __latextrit" id="choice_for_yes_side">
                                        <label class="col-md-3 control-label"><?php echo $this->lang->line("side") ?></label>
                                        <div class="col-md-9">
                                            <div class="mt-repeater">
                                                <div data-repeater-list="group-b">
                                                    <?php
                                                    if (isset($Side) && count($Side) > 0) {
                                                        foreach ($Side as $side) {
                                                            ?>
                                                            <div data-repeater-item class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label"><?php echo $this->lang->line("side") ?> <?php echo $this->lang->line("in_english") ?></label>
                                                                    <input type="text" class="form-control" name="side_dish_name[]" value="<?= !empty($side->side_dish) ? $side->side_dish : '' ?>" id="side_dish_name"/> </div>
                                                                <div class="col-md-5">
                                                                    <label class="control-label"><?php echo $this->lang->line("side") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                                                    <input type="text" class="form-control" name="side_dish_nam_ar[]" value="<?= !empty($side->side_dish_ar) ? $side->side_dish_ar : '' ?>" id="sidedish_name_ar"/> </div>

                                                                <div class="col-md-4">
                                                                    <label class="control-label"><?php echo $this->lang->line("price") ?></label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-btn">
                                                                            <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>
                                                                        </span>
                                                                        <input type="text"  class="form-control" name="side_dish_price[]" value="<?= !empty($side->side_dish_price) ? $side->side_dish_price : '' ?>" id="side_dish_price"/> </div>
                                                                    <input type="hidden"  class="form-control" name="side_dish_id[]" value="<?= !empty($side->menu_side_id) ? $side->menu_side_id : '' ?>" id="side_dish_id"/>
                                                                </div>

                                                                <div class="col-md-1 close_btn">
                                                                    <label class="control-label">&nbsp;</label>
                                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                                        <i class="fa fa-close"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <div data-repeater-item class="row">
                                                            <div class="col-md-5">
                                                                <label class="control-label"><?php echo $this->lang->line("side") ?> <?php echo $this->lang->line("in_english") ?></label>
                                                                <input type="text" class="form-control" name="side_dish_name[]" value="<?= !empty($side->side_dish) ? $side->side_dish : '' ?>" id="side_dish_name"/> </div>
                                                            <div class="col-md-5">
                                                                <label class="control-label"><?php echo $this->lang->line("side") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                                                <input type="text" class="form-control" name="side_dish_name_ar[]" value="<?= !empty($side->side_dish_ar) ? $side->side_dish_ar : '' ?>" id="sidedish_name_ar"/> </div>

                                                            <div class="col-md-4">
                                                                <label class="control-label"><?php echo $this->lang->line("price") ?></label>
                                                                <div class="input-group">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>
                                                                    </span>
                                                                    <input type="text"  class="form-control" name="side_dish_price[]" value="<?= !empty($side->side_dish_price) ? $side->side_dish_price : '' ?>" id="side_dish_price"/> </div>
                                                            </div>

                                                            <div class="col-md-1 close_btn">
                                                                <label class="control-label">&nbsp;</label>
                                                                <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                                    <i class="fa fa-close"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                                <hr>
                                                <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                                    <i class="fa fa-plus"></i><?php echo $this->lang->line("add_side") ?></a>
                                                <br>
                                                <br> </div>


                                        </div>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("status") ?></label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="Status">
                                            <option <?= isset($Categories->status) ? $Categories->status == 0 ? 'selected="selected"' : '' : '' ?> value="0"><?php echo $this->lang->line("active") ?></option>
                                            <option <?= isset($Categories->status) ? $Categories->status == 1 ? 'selected="selected"' : '' : '' ?> value="1"><?php echo $this->lang->line("inactive") ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" class="btn blue" id="SubmitMenu">
                                            <i class="fa fa-check"></i> <?php echo $this->lang->line("submit") ?></button>
                                        <button type="button" onclick="window.location = '<?= base_url('vendor/menu'); ?>';" class="btn default"><?php echo $this->lang->line("cancel") ?></button>
                                    </div>
                                </div>
                            </div>

                            <!-- END FORM-->
                    </div>
                    </form>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
