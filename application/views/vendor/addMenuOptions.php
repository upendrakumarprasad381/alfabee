<?php
$Session = GetSessionArrayVendor();
$Qry = "SELECT promocode_id,title FROM promo_code WHERE offer_addedby=".$Session->id;
$Array = $this->Database->select_qry_array($Qry);
?>
<style>
    .close_btn{
        margin-top:25px;
    }
    @media (max-width: 1520px) {
        .close_btn {
            margin-top: 0;
        }
    }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            
                            <span class="caption-subject bold uppercase"><?= empty($Id) ? $this->lang->line("add_new") : $this->lang->line("update"); ?> <?php echo $this->lang->line("menu") ?></span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="Id" value="">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("category") ?></label>
                                    <div class="col-md-6" id="category_list">
                                        <input type="hidden" id="store_type" value="<?= $Session->store_type?>">
                                        <select class="form-control selectpicker" data-live-search="true" id="category" onchange="addCategory()">
                                            <option value=""><?php echo $this->lang->line("select_category") ?></option>
                                            <option value=""><?php echo $this->lang->line("add_new_category") ?></option>
                                            <?php
                                                if(isset($Categories->category_id))
                                                {
                                                    $cat_id = $Categories->category_id;
                                                }else{
                                                    $cat_id = '';
                                                }
                                            ?>
                                            <?=get_category($cat_id,$Session->store_type)?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="add_category">
                                    <label class="control-label col-md-3"></label>
                                    <div class="col-md-6" id="other_category"> 
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("menu") ?> <?php echo $this->lang->line("in_english") ?></label>
                                    <div class="col-md-6">
                                        <input value="<?= !empty($Categories->menu_name) ? $Categories->menu_name : '' ?>" id="menu_name" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("menu") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                    <div class="col-md-6">
                                        <input value="<?= !empty($Categories->menu_name_ar) ? $Categories->menu_name_ar : '' ?>" id="menu_name_ar" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("description") ?> <?php echo $this->lang->line("in_english") ?></label>
                                    <div class="col-md-6">
                                        <textarea class="form-control summernote" id="description" rows="10"><?= !empty($Categories->description) ? $Categories->description : '' ?></textarea>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("description") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                    <div class="col-md-6">
                                        <textarea class="form-control summernote" id="description_ar" rows="10"><?= !empty($Categories->description_ar) ? $Categories->description_ar : '' ?></textarea>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("upload_image") ?> </label>
                                    <div class="col-md-6">
                                    <input type="file" class="custom-file-input" id="CommonImages" src="">
                                    </div>
                                    <div class="col-md-3">
                                        
                                    <a target="_blank" href="<?php if(isset($Categories->image) && $Categories->image!=""){ echo base_url().'uploads/menu/'.$Categories->image;} ?>">
                                        <img src=" <?php if(isset($Categories->image) && $Categories->image!=""){ echo base_url().'uploads/menu/'.$Categories->image;} ?>" width="20%" id="ImagesEncode" OldImage='<?= !empty($Categories->image) ? 'uploads/menu/' . $Categories->image : '' ?>'>
                                    </a>
                                    <?php if(isset($Categories->image) && $Categories->image!=""){ ?>
                                        <br><a class="remove_image_common" path="uploads/menu/" name="<?=isset($Categories->image)?$Categories->image:''?>" table="menu_list" id="<?=$Id?>" type="">Remove</a>
                                      <?php } ?>
                                    </div>
                                </div>
                
                                <?php
                                if($Session->store_type==1)
                                {
                                ?>
                                
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo $this->lang->line("choices") ?></label>
                                    <div class="col-md-6 text-left pt-1">
                                      
                                        <input type="checkbox" id="choice" name="choice" value="1" <?php echo (isset($Categories->choice) && $Categories->choice==1 ? 'checked="checked"' : '');?>>
                                        <label><?php echo $this->lang->line("yes") ?></label>
    
                                    </div>
                                </div>
                                <?php
                                }
                                ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo $this->lang->line("price") ?></label>
                                    <div class="input-group col-md-6 ___arddv">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>
                                        </span>
                                        <input value="<?= !empty($Categories->price) ? $Categories->price : '' ?>" id="product_price" type="text" class="form-control col-md-3" autocomplete="off">
                                   
                                    </div>
                                </div>
                                <?php
                                if($Session->store_type!=1)
                                {
                                ?>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Available Stock</label>
                                    <div class="col-md-6">
                                        <input value="<?= !empty($Categories->stock) ? $Categories->stock : '' ?>" id="stock" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <?php
                                }
                                ?>
                                <div class="add_choices">
                                    <div class="form-group __latextrit" id="choice_for_yes_addon">
                                        <label class="col-md-3 control-label"><?php echo $this->lang->line("add_ons") ?></label>
                                        <div class="col-md-9">
                                    <div class="mt-repeater">
                                                <div data-repeater-list="group-b" >
                                   <div data-repeater-item class="row">
                                                        <div class="col-md-5">
                                                            <label class="control-label">Add ons Heading <?php echo $this->lang->line("in_english") ?></label>
                                                            <input type="text" class="form-control" name="add_on[]" value="<?= !empty($ad->add_on) ? $ad->add_on : '' ?>" id="add_on"/> 
                                                        </div>
                                                        <div class="col-md-5">
                                                            <label class="control-label"><?php echo $this->lang->line("add_ons_name") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                                            <input type="text" class="form-control" name="add_on_ar[]" value="<?= !empty($ad->add_on_ar) ? $ad->add_on_ar : '' ?>" id="addon_ar"/> 
                                                        </div>
                                                        <!--<div class="col-md-4">-->
                                                        <!--    <label class="control-label text-left"><?php echo $this->lang->line("price") ?></label>-->
                                                        <!--    <div class="input-group">-->
                                                        <!--        <span class="input-group-btn">-->
                                                        <!--            <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>-->
                                                        <!--        </span>-->
                                                        <!--    <input type="text"  class="form-control" name="addon_price[]" value="<?= !empty($ad->addon_price) ? $ad->addon_price : '' ?>" id="addon_price"/> </div>-->
                                                        <!--</div>-->
                                                        
                                                        <div class="col-md-1 close_btn">
                                                            <label class="control-label">&nbsp;</label>
                                                            <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                                <i class="fa fa-close"></i>
                                                            </a>
                                                        </div>
                                                        
                                                      
                                                    </div>
                                                      <div data-repeater-list="group-b" >
                                   <div data-repeater-item1 class="row">
                                                        <div class="col-md-5">
                                                            <label class="control-label">Add ons Heading <?php echo $this->lang->line("in_english") ?></label>
                                                            <input type="text" class="form-control" name="add_on[]" value="<?= !empty($ad->add_on) ? $ad->add_on : '' ?>" id="add_on"/> 
                                                        </div>
                                                        <!--<div class="col-md-5">-->
                                                        <!--    <label class="control-label"><?php echo $this->lang->line("add_ons_name") ?> <?php echo $this->lang->line("in_arabic") ?></label>-->
                                                        <!--    <input type="text" class="form-control" name="add_on_ar[]" value="<?= !empty($ad->add_on_ar) ? $ad->add_on_ar : '' ?>" id="addon_ar"/> -->
                                                        <!--</div>-->
                                                        <!--<div class="col-md-4">-->
                                                        <!--    <label class="control-label text-left"><?php echo $this->lang->line("price") ?></label>-->
                                                        <!--    <div class="input-group">-->
                                                        <!--        <span class="input-group-btn">-->
                                                        <!--            <button class="btn default" type="button"><?php echo $this->lang->line("aed") ?></button>-->
                                                        <!--        </span>-->
                                                        <!--    <input type="text"  class="form-control" name="addon_price[]" value="<?= !empty($ad->addon_price) ? $ad->addon_price : '' ?>" id="addon_price"/> </div>-->
                                                        <!--</div>-->
                                                        
                                                        <div class="col-md-1 close_btn">
                                                            <label class="control-label">&nbsp;</label>
                                                            <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                                <i class="fa fa-close"></i>
                                                            </a>
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                    </div>
                                                    </div>
                                                    
                                                     <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add ">
                                                    <i class="fa fa-plus"></i> <?php echo $this->lang->line("add_addon") ?></a>
                                                    <hr>
                                                <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add ">
                                                    <i class="fa fa-plus"></i> <?php echo $this->lang->line("add_addon") ?></a>
                                                <br>
                                                <br> 
                                                    </div>
                                                    </div>
                                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("status") ?></label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="Status">
                                            <option <?= isset($Categories->status) ? $Categories->status == 0 ? 'selected="selected"' : '' : '' ?> value="0"><?php echo $this->lang->line("active") ?></option>
                                            <option <?= isset($Categories->status) ? $Categories->status == 1 ? 'selected="selected"' : '' : '' ?> value="1"><?php echo $this->lang->line("inactive") ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="button" class="btn blue" id="SubmitMenu">
                                                <i class="fa fa-check"></i> <?php echo $this->lang->line("submit") ?></button>
                                            <button type="button" onclick="window.location = '<?= base_url('vendor/menu'); ?>';" class="btn default"><?php echo $this->lang->line("cancel") ?></button>
                                        </div>
                                    </div>
                                </div>

                                <!-- END FORM-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>





        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
