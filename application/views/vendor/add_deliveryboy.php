<?php
$Session = GetSessionArrayVendor();
$userId = !empty($this->uri->segment(3)) ? base64_decode($this->uri->segment(3)) : '';
$cArray = GetCustomerArrayById($userId);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= empty($cArray['user_id']) ? 'Add New' : 'Update'; ?> Delivery Boy</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered" method="post">
                            <input type="hidden" id="customer_id" name="customer_id" value="<?= !empty($cArray['user_id']) ? $cArray['user_id'] : '' ?>">
                            <div class="form-body">
                                <?php
                                if (isset($_POST['add_deliveryboy'])) {
                                    $update = [];
                                    $customer_id = !empty($_POST['customer_id']) ? $_POST['customer_id'] : '';

                                    $update['name'] = $_POST['name'];
                                    $update['email'] = $_POST['email'];
                                    $update['mobile_number'] = $_POST['mobile_number'];
                                    $update['mobile_code'] = '92';
                                    $update['store_type'] = '0';
                                    $update['deliveryboy_added_by'] = $Session->id;
                                    $update['lastupdate'] = date('Y-m-d H:i:s');
                                    if (!empty($_POST['password'])) {
                                        $update['password'] = md5($_POST['password']);
                                    }
                                    $update['status'] = $_POST['status'];
                                    $update['archive'] = $update['status'];
                                    if (empty($customer_id)) {
                                        $qry = "SELECT * FROM `users` WHERE user_type=4 AND email LIKE '" . $update['email'] . "'";
                                        $checkEmail = $this->Database->select_qry_array($qry);
                                        if (empty($checkEmail)) {
                                            $update['user_type'] = 4;
                                            $this->Database->insert('users', $update);
                                            redirect(base_url('vendor/delivery_boy'));
                                        } else {
                                            $message = "Already registered this email";
                                        }
                                    } else {
                                        $cond = array('id' => $customer_id);
                                        $this->Database->update('users', $update, $cond);
                                        redirect(base_url('vendor/delivery_boy'));
                                    }
                                }
                                if (!empty($message)) {
                                    ?>
                                    <div class="form-group">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-8 alert alert-info" role="alert">
                                            <?= $message ?>
                                        </div>

                                    </div>
                                <?php } ?>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Name</label>
                                    <div class="col-md-4">
                                        <input id="name" name="name" value="<?= isset($cArray['full_name']) ? $cArray['full_name'] : '' ?>" type="text" class="form-control" required autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Email</label>
                                    <div class="col-md-4">
                                        <input id="email" name="email" value="<?= isset($cArray['email']) ? $cArray['email'] : '' ?>" type="email" class="form-control" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Phone</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <button id="mobile_code" name="mobile_code" value="<?= isset($cArray['mobile_code']) ? $cArray['mobile_code'] : '92' ?>" class="btn" type="button"><?= isset($cArray->mobile_code) ? $cArray->mobile_code : '+92' ?></button>

                                            </span>
                                            <input id="mobile_number" name="mobile_number" value="<?= isset($cArray['mobile_number']) ? $cArray['mobile_number'] : '' ?>" type="number" class="form-control" autocomplete="off" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Password</label>
                                    <div class="col-md-4">
                                        <input name="password" value="" type="text" class="form-control" autocomplete="off" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("status") ?></label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="status" name="status">
                                            <option <?= isset($cArray->status) ? $cArray->status == 0 ? 'selected="selected"' : '' : '' ?> value="0"><?php echo $this->lang->line("active") ?></option>
                                            <option <?= isset($cArray->status) ? $cArray->status == 1 ? 'selected="selected"' : '' : '' ?> value="1"><?php echo $this->lang->line("inactive") ?></option>
                                        </select>
                                    </div>
                                </div>





                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" name="add_deliveryboy" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <button type="button" onclick="window.location = '';" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
