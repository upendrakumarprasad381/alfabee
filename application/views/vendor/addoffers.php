<?php
$Id = !empty($this->uri->segment(4)) ? base64_decode($this->uri->segment(4)) : '';
$cArray = GetpromocodeBy($Id);

$Session = GetSessionArrayVendor();

?>
<style>
.img-size-common {
    color: #f01818 !important;
    font-size: 66% !important;
    font-weight: 600 !important;
}
.up_tm_box {
    float: left;
    width: 150px;
    margin: 0 10px 10px 0;
    border: #ccc 1px solid;
    padding: 5px;
    text-align: center;
    font-size: 12px;
}
</style>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= empty($cArray->promocode_id) ? $this->lang->line("add_new") : $this->lang->line("update"); ?> <?php echo $this->lang->line("offers")?></span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered" method="post" onsubmit="return false;">
                            <input type="hidden" id="Id" value="<?= !empty($cArray->promocode_id) ? $cArray->promocode_id : '' ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("offer_title")?> <?php echo $this->lang->line("in_english")?></label>
                                    <div class="col-md-4">
                                        <input id="offer_title" value="<?= !empty($cArray->title) ? $cArray->title : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("offer_title")?> <?php echo $this->lang->line("in_arabic")?></label>
                                    <div class="col-md-4">
                                        <input id="offer_title_ar" value="<?= !empty($cArray->title_ar) ? $cArray->title_ar : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                
                                
                                
                                
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3">Discount Type</label>
                                    <div class="col-md-4">
                                        <?php
                                        $discount_type=!empty($cArray->discount_type) ? $cArray->discount_type : '';
                                        ?>
                                        <select  id="discount_type" class="form-control" onchange="VENDRCOMN.vendordiscountTypechnage();">
                                            <option <?= empty($discount_type) ? 'selected' : '' ?> value="0">Percentage</option>
                                            <option <?= $discount_type=='1' ? 'selected' : '' ?> value="1">Fixed</option>
                                        </select>
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3" id="distypemsg"><?= empty($discount_type) ? 'Discount in %' : 'Enter amount' ?></label>
                                    <div class="col-md-4">
                                        <input type="number" value="<?= !empty($cArray->discount) ? $cArray->discount : '' ?>" id="discount" class="form-control">
                                    </div>
                                </div>
                                
                                

                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("date_from")?></label>
                                    <div class="col-md-4">
                                        <input id="date_from" value="<?= !empty($cArray->date_from) && $cArray->date_from != '0000-00-00' ? date('Y-m-d',  strtotime($cArray->date_from)) : '' ?>"  type="text" class="form-control datepicker" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("date_to")?></label>
                                    <div class="col-md-4">
                                        <input id="date_to" value="<?= !empty($cArray->date_to) && $cArray->date_to != '0000-00-00' ? date('Y-m-d',  strtotime($cArray->date_to)) : '' ?>" type="text" class="form-control datepicker1" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("menu")?></label>
                                    <div class="col-md-4">
                                        <select class="form-control selectpicker" data-live-search='true' multiple="multiple" id="menu_id"   >
                                           
                                            <?php
                                                $menu = GetMenudetails($Session->id);
                                                $menu_id = !empty($cArray->menu_id) ? explode(',', $cArray->menu_id) : [];
                                               
                                                for ($i = 0; $i < count($menu); $i++) {
                                                    $drt = $menu[$i];
                                                    ?> <option <?= in_array($drt->id, $menu_id) ? 'selected' : '' ?> value="<?= $drt->id ?>"><?= $drt->menu_name?></option><?php
                                                }
                                                ?>
                                                </select>
                                    </div>
                                </div>
                                
                           
                              
                               
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("status")?></label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="status" name="status">
                                            <option <?= isset($cArray->status) ? $cArray->status == 0 ? 'selected="selected"' : '' : '' ?> value="0"><?php echo $this->lang->line("active")?></option>
                                            <option <?= isset($cArray->status) ? $cArray->status == 1 ? 'selected="selected"' : '' : '' ?> value="1"><?php echo $this->lang->line("inactive")?></option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="button" class="btn blue" id="addoffers">
                                                <i class="fa fa-check"></i> <?php echo $this->lang->line("submit")?></button>
                                            <button type="button" onclick="window.location = '<?= base_url('vendor/offers'); ?>';" class="btn default"><?php echo $this->lang->line("cancel")?></button>
                                        </div>
                                    </div>
                                </div>

                                <!-- END FORM-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>





        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN CONTENT -->

