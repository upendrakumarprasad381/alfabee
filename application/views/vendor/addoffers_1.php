<?php
$promoId = !empty($_REQUEST['promoId']) ? base64_decode($_REQUEST['promoId']) : '';
$cArray = GetpromocodeBy($promoId);
$Session = GetSessionArrayVendor();

?>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= empty($cArray->promo_id) ? 'Add New' : 'Update'; ?> Offer</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered" method="post" onsubmit="return false;">
                            <input type="hidden" id="promo_id" value="<?= !empty($cArray->promo_id) ? $cArray->promo_id : '' ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Offer Code</label>
                                    <div class="col-md-4">
                                        <input id="promo_code" value="<?= !empty($cArray->promo_code) ? $cArray->promo_code : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Discount In %</label>
                                    <div class="col-md-4">
                                        <input type="number" value="<?= !empty($cArray->discount) ? $cArray->discount : '' ?>" id="discount" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Date From</label>
                                    <div class="col-md-4">
                                        <input id="date_from" value="<?= !empty($cArray->date_from) && $cArray->date_from != '0000-00-00' ? date('d-m-Y',  strtotime($cArray->date_from)) : '' ?>"  type="text" class="form-control datepicker" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Date To</label>
                                    <div class="col-md-4">
                                        <input id="date_to" value="<?= !empty($cArray->date_to) && $cArray->date_to != '0000-00-00' ? $cArray->date_to : '' ?>" type="text" class="form-control datepicker" autocomplete="off">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Menu</label>
                                    <div class="col-md-4">
                                        <select class="form-control selectpicker" data-live-search='true' multiple="multiple" id="menu_id">
                                            <option value="">Select</option>
                                            <?php
                                  
                                                $menu = GetMenudetails($Session->id);
                                                $city = !empty($cArray->menu_id) ? explode(',', $cArray->menu_id) : [];
                                                for ($i = 0; $i < count($menu); $i++) {
                                                    $drt = $menu[$i];
                                                    ?> <option <?= in_array($drt->id, $city) ? 'selected' : '' ?> value="<?= $drt->id ?>"><?= $drt->menu_name?></option><?php
                                                }
                                            
                                            ?>
                                        </select>
                                    </div>
                                </div>




                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="button" class="btn blue" id="addoffers">
                                                <i class="fa fa-check"></i> Submit</button>
                                            <button type="button" onclick="window.location = '<?= base_url('admin/boxtype'); ?>';" class="btn default">Cancel</button>
                                        </div>
                                    </div>
                                </div>

                                <!-- END FORM-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>





        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN CONTENT -->

