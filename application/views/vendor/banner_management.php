<?php
$Session = GetSessionArrayVendor();
$vendor_id = $Session->id;
$Qry = "SELECT * FROM `restaurant_banner` WHERE vendor_id=" . $vendor_id;
$Array = $this->Database->select_qry_array($Qry);
$banner = isset($Array[0]->image) ? $Array[0]->image : '';

if (isset($_POST['formsubmit'])) {
    $form = $_FILES['agreementform'];
    $FileName = uniqid() . '.' . pathinfo($form['name'], PATHINFO_EXTENSION);

    if (move_uploaded_file($form['tmp_name'], HOME_DIR . 'uploads/banner_images/' . $FileName)) {
        $insert = array(
            'vendor_id' => $vendor_id,
            'image' => $FileName,
            'status' => 0,
        );

        $this->Database->insert('restaurant_banner', $insert);
    }
}
?>
<style>
    .img-size-common {
        color: #f01818 !important;
        font-size: 66% !important;
        font-weight: 600 !important;
    }
    .up_tm_box {
        float: left;
        width: 150px;
        margin: 0 10px 10px 0;
        border: #ccc 1px solid;
        padding: 5px;
        text-align: center;
        font-size: 12px;
    }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="<?= base_url('Vendor') ?>"><?php echo $this->lang->line("home") ?></a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span><?php echo $this->lang->line("update_restaurant_banner") ?></span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold"><?php echo $this->lang->line("update_restaurant_banner") ?></span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
                            <div class="row">
                                <?php
                                $qry = "SELECT * FROM `restaurant_banner` WHERE vendor_id='$vendor_id'";
                                $image = $this->Database->select_qry_array($qry);
                                if (isset($image)) {
                                    ?>
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>

                                        <div class="col-md-8">
                                            <?php
                                            for ($i = 0; $i < count($image); $i++) {
                                                $d = $image[$i];
                                                $filenff = 'uploads/banner_images/' . $d->image;
                                                if (is_file(HOME_DIR . $filenff)) {
                                                    ?>
                                                    <div class="up_tm_box"  >
                                                        <a target="_blank" href="<?= base_url($filenff) ?>">
                                                            <img src="<?= base_url($filenff) ?>"   class="banner_images_cls img-responsive" newimage=''>
                                                        </a> 

                                                        <br><a href="javascript:void(0)" removefile="<?= HOME_DIR . $filenff ?>"  condjson='{"id":"<?= $d->id ?>"}' dbtable="restaurant_banner" class="autodelete"><?php echo $this->lang->line("remove") ?></a>

                                                    </div> 
                                                <?php }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                <?php }
                                ?> 
                                <div id="image_upload_section">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"><?php echo $this->lang->line("upload_image") ?>
                                            <span class="img-size-common"> height and width 715px * 210px</span>
                                        </label>
                                        <div class="col-md-6">
<?php $rand = rand(); ?>
                                            <input type="file" class="custom-file-input MultipleImages" name="agreementform" id="" required refval="<?= $rand ?>" src="">
                                        </div>
                                        <div class="col-md-3">

                                            <a target="_blank" href="" >
                                                <img src="" width="20%" id="ImagesEncode_<?= $rand ?>" OldImage='' class="banner_images_cls" newimage=''>
                                            </a> 
                                            <button type="button" style="display: none;" class="btn btn-info" id="add_more_images" inputclassname="banner_images_cls">
                                                <span class="glyphicon glyphicon-plus"></span> <?php echo $this->lang->line("add_more") ?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" id="SubmitBannerXXX" name="formsubmit" class="btn blue">
                                            <i class="fa fa-check"></i> <?php echo $this->lang->line("submit") ?></button>
                                        <button type="button" onclick="window.location = '';" class="btn default"><?php echo $this->lang->line("cancel") ?></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
