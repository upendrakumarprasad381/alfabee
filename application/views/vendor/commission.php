<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                        
                            <span class="caption-subject bold uppercase"><?php echo $this->lang->line("commission") ?></span>
                        </div>
                    </div>
                 
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="Id" value="<?= !empty($commission[0]->id)?$commission[0]->id:'' ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <!--<label class="control-label col-md-3">You are paying</label>-->
                                    <div class="col-md-12">
                                      <span class="col-md-2"><?php echo $this->lang->line("you_are_paying") ?></span> <span class="col-md-2"><input value="<?= !empty($commission[0]->percentage) ? number_format($commission[0]->percentage,2) : '' ?>" id="commission" type="text" class="" autocomplete="off" disabled>
                                     </span><span class="col-md-3"><?php echo $this->lang->line("%_commission_to_homeeats") ?></span>
                                    </div>
                                   
                                </div>
                                <!--<div class="form-group">-->
                                <!--    <label class="control-label col-md-3">Status</label>-->
                                <!--    <div class="col-md-4">-->
                                <!--        <select class="form-control" id="Status">-->
                                <!--            <option <?= isset($Categories->status) ? $Categories->status == 0 ? 'selected="selected"' : '' : '' ?> value="0">Active</option>-->
                                <!--            <option <?= isset($Categories->status) ? $Categories->status == 1 ? 'selected="selected"' : '' : '' ?> value="1">Inactive</option>-->
                                <!--        </select>-->
                                <!--    </div>-->
                                <!--</div>-->
                            </div>
                               



                                <!--<div class="form-actions">-->
                                <!--    <div class="row">-->
                                <!--        <div class="col-md-offset-3 col-md-9">-->
                                <!--            <button type="button" class="btn blue" id="SubmitCommission">-->
                                <!--                <i class="fa fa-check"></i> Submit</button>-->
                                <!--            <button type="button" onclick="window.location = '<?= base_url('vendor/commission'); ?>';" class="btn default">Cancel</button>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--</div>-->

                                <!-- END FORM-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>





        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
