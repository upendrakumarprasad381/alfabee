<?php
    $Session = GetSessionArrayVendor();
    $Qry = "SELECT restaurant_name,email,area,vendor_id,delivery_location,latitude,longitude FROM  users LEFT JOIN restaurant_details ON users.id=restaurant_details.vendor_id WHERE users.id= $Session->id";
    $rest_details = $this->Database->select_qry_array($Qry);
?>
<style>
    #floating-panel {
        position: absolute;
        top: 80px;
        left: 25%;
        z-index: 5;
        padding: 5px;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?php echo $this->lang->line("delivery_location") ?></span>
                        </div>
                    </div>
                    <form method="post" action="<?= base_url('vendor/delivery_location')?>">
                     <div class="form-body">
                                <div class="form-group">
                                    <input type="hidden" value="<?= $rest_details[0]->latitude?>" id="latitude">
                                    <input type="hidden" value="<?= $rest_details[0]->longitude?>" id="longitude">
                                    <input type="hidden" value="<?= $rest_details[0]->restaurant_name?>" id="restaurant_name">
                                    <input type="hidden" value="<?= $rest_details[0]->area?>" id="restaurant_streetaddress">
                                    <input type="hidden" value="<?= $rest_details[0]->vendor_id?>" id="vendor_id" name="vendor_id">
                                    <input type="hidden" name="delivery_area1" value="<?= $rest_details[0]->delivery_location?>" id="delivery_area1">
                        <div id="floating-panel"><input id="removeLine" type=button value="<?php echo $this->lang->line("remove_line") ?>"></div>
                        <div class="map_model" id="map" style="width:100%; height:450px;">
                        <!-- <div id="map_canvas" style="width:100%; height:450px;"></div> -->
                       
                        </div>
                        </div></div>
                        <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn blue" id="updateLocation">
                                                <i class="fa fa-check"></i> <?php echo $this->lang->line("submit") ?></button>

                                        </div>
                                    </div>
                                </div>
                    </form>
                </div>
                </div>
                </div>
                </div>
                </div>