
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?php echo $this->lang->line("feedback")?></span>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover commontableXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no")?></th>
                                    <th><?php echo $this->lang->line("customer_name")?></th>
                                    <th><?php echo $this->lang->line("rating")?></th>
                                    <th><?php echo $this->lang->line("comments")?></th>
                                    <th><?php echo $this->lang->line("added_date")?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($feedback); $i++) {
                                    $d = $feedback[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= GetNameById($d->user_id,'users','name')?></td>
                                        <td>
                                            <div class="rating">
                                                
                                                <?php
                                                $stars = (int)$d->rating;
                                                $count = 1;
                                                for($j = 1; $j <= 5; $j++){
                                                    if($stars >= $count){
                                                        $color = "#ff9900";
                                                    } else {
                                                        $color = "";
                                                    }
                                                    $count++;
                                                
                                                ?>
				                            <span class="float-left">
										        <i class="fa fa-star" style="color:<?= $color ?>"></i>
										    </span> 
				                                <?php
                                                }
                                                
                                                ?>
				                            </div>
                                        </td>
                                        <td><?= $d->comments?></td>
                                        <td><?= date('d M Y, h:i A ',strtotime($d->inserted_on))?></td>
                                       
                                    </tr>
                                <?php }
                        
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
