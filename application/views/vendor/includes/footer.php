</div>
<!-- END CONTAINER -->


<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> <?= date('Y') ?> &copy; AlfaBee &nbsp;&nbsp;&nbsp;
        <a target="_blank" href="https://alwafaagroup.com">Alwafaa Group</a> 

    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
</div>
<!-- BEGIN QUICK NAV -->
<div class="quick-nav-overlay"></div>
<!-- END QUICK NAV -->
<?php
$Controller = $this->router->fetch_class();
$Method = $this->router->fetch_method();
$segment3 = $this->uri->segment(3);
$segment2 = $this->uri->segment(2);
$sess_value = $this->session->userdata(ADMIN_LANGUAGE_ID);
?>
<script>
    var base_url = '<?= base_url(); ?>';
    var Controller = '<?= $Controller ?>';
    var Method = '<?= $Method ?>';
    var segment3 = '<?= $segment3 ?>';
    var segment2 = '<?= $segment2 ?>';
    var sessionValue = '<?= $sess_value ?>';
</script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?= base_url(); ?>assets/global/scripts/app.min.js" type="text/javascript"></script>

<script src="<?= base_url(); ?>js/additional/jquery-confirm.min.js"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<script src="<?= base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/pages/scripts/DatatableClass.js" type="text/javascript"></script> 
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>  
<script src="<?= base_url(); ?>assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
<!--<script src="<?= base_url() ?>assets/global/plugins/moment.min.js" type="text/javascript"></script>-->
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?= base_url(); ?>assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/lobibox-master/js/lobibox.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap-select.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery-repeater/jquery.repeater.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/pages/scripts/form-repeater.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/pages/scripts/table-datatables-editable.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>js/additional/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="<?= base_url(); ?>js/vendor.js"></script>
<script src="<?= base_url(); ?>js/vendornew.js"></script>
<?php
if ($segment2 == 'delivery_location') {
    ?>
    <script src="https://demo.softwarecompany.ae/home_eats/js/additional/delivery_location.js"></script>
    <?php
    if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'en' || empty($this->session->userdata(ADMIN_LANGUAGE_ID))) {
        ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&libraries=places,drawing&callback=initMap&language=en" async defer></script> 
        <?php
    } else {
        ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&libraries=places,drawing&callback=initMap&language=ar" async defer></script>
        <?php
    }
    ?>
    <?php
}
?>
<!-- END THEME LAYOUT SCRIPTS -->
<script>
    $(document).ready(function () {
        $('.commontable').DataTable();
        $('.common_table').DataTable({
            buttons: [
                {extend: "excel", className: "buttonsToHide"},
                {extend: "pdf", className: "buttonsToHide"},
            ]
        });
        $('.timepicker').timepicker({
              timeFormat: 'HH:mm',
                 showMeridian: false,
        });

        $('.repeater').repeater({
            // (Required if there is a nested repeater)
            // Specify the configuration of the nested repeaters.
            // Nested configuration follows the same format as the base configuration,
            // supporting options "defaultValues", "show", "hide", etc.
            // Nested repeaters additionally require a "selector" field.
            repeaters: [{
                    // (Required)
                    // Specify the jQuery selector for this nested repeater
                    selector: '.inner-repeater'
                }]
        });

    });

</script>

<?php
if ($segment2 == 'OrderDetails' || $segment2 == 'orders') {
    ?>

    <script>
        var order_date = document.getElementById('order_time').value;
        var oldDateObj = new Date(order_date);
        var newDateObj = new Date();
        newDateObj.setTime(oldDateObj.getTime() + (3 * 60 * 1000));

        var x = setInterval(function () {

            var now = new Date().getTime();

            var distance = newDateObj - now;

            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            element = document.getElementById("timer");
            if (element != null) {

                document.getElementById("timer").innerHTML = minutes + "m " + seconds + "s ";
            }
            if (distance < 0) {
                clearInterval(x);
                var a = document.getElementById("timer");
                if (a != null)
                {
                    a.style.display = "none";
                }
                var b = document.getElementById("time_left");
                if (b != null)
                {
                    b.style.display = "none";
                }
                if (a != null)
                {
                    setTimeout(function () {
                        window.location = "";
                    }, 1000);
                }
            }
        }, 1000);

        window.setInterval(function () {

            $('.timer').each(function () {
                var sl = $(this).attr('sl');

            });
        }, 1000);
    </script>
    <?php
}
?>
</body>

</html>
<script>
    
</script>