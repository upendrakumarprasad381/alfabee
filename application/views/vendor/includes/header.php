<?php
$session_arr = $this->session->userdata('Vendor');
$vendor_notification = notification_count($session_arr->id);
$get_all_vendor_notification = get_all_notification($session_arr->id);
$Qry = "SELECT * FROM `restaurant_details` WHERE vendor_id='$session_arr->id'";
$DetailsArray = $this->Database->select_qry_array($Qry);
?>
<!DOCTYPE html>
<html lang="en">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <title>Alfabee</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <?php
        if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'en' || empty($this->session->userdata(ADMIN_LANGUAGE_ID))) {
            ?>
            <link href="<?= base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="<?= base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
            <?php
        } else {
            ?>
            <link href="<?= base_url() ?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css" />
            <link href="<?= base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css" rel="stylesheet" type="text/css" />
            <?php
        }
        ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
        <!-- BEGIN THEME GLOBAL STYLES -->
        <?php
        if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'en' || empty($this->session->userdata(ADMIN_LANGUAGE_ID))) {
            ?>
            <link href="<?= base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
            <link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
            <?php
        } else {
            ?>
            <link href="<?= base_url() ?>assets/global/css/components-rtl.min.css" rel="stylesheet" id="style_components" type="text/css" />
            <link href="<?= base_url() ?>assets/global/css/plugins-rtl.min.css" rel="stylesheet" type="text/css" />
            <?php
        }
        ?>
        <!-- END THEME GLOBAL STYLES -->

        <link href="<?= base_url() ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <?php
        if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'en' || empty($this->session->userdata(ADMIN_LANGUAGE_ID))) {
            ?>
            <link href="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css"/>
            <?php
        } else {
            ?>
            <link href="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css" rel="stylesheet" type="text/css"/>
            <?php
        }
        ?>
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="<?= base_url() ?>css/jquery-confirm.min.css"> 
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />

        <!-- BEGIN THEME LAYOUT STYLES -->
        <?php
        if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'en' || empty($this->session->userdata(ADMIN_LANGUAGE_ID))) {
            ?>
            <!-- BEGIN THEME LAYOUT STYLES -->
            <link href="<?= base_url() ?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
            <link href="<?= base_url() ?>assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="<?= base_url() ?>assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME LAYOUT STYLES -->
            <?php
        } else {
            ?>
             <link href="<?= base_url() ?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
            <link href="<?= base_url() ?>assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="<?= base_url() ?>assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
<!--            <link href="<?= base_url() ?>assets/layouts/layout/css/layout-rtl.min.css" rel="stylesheet" type="text/css" />
            <link href="<?= base_url() ?>assets/layouts/layout/css/themes/darkblue-rtl.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="<?= base_url() ?>assets/layouts/layout/css/custom-rtl.min.css" rel="stylesheet" type="text/css" />-->
            <?php
        }
        ?>
        <!-- END THEME LAYOUT STYLES -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/lobibox-master/dist/css/lobibox.min.css"/>
        <link href="<?= base_url(); ?>assets/css/bootstrap-select.min.css" rel="stylesheet">
        <link href="<?= base_url(); ?>css/jquery-ui.css" rel="stylesheet">

        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

        <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>images/favicons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>images/favicons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>images/favicons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>images/favicons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>images/favicons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>images/favicons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>images/favicons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>images/favicons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>images/favicons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url() ?>images/favicons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>images/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>images/favicons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>images/favicons/favicon-16x16.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
        <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="<?= base_url('vendor_dashboard') ?>">

                            <img src="<?= base_url() ?>assets/layouts/layout/img/logo.png" alt="logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->

                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <li class="nav-item dropdown"> 
                                <div class="lang_adm" style="display:none;">
                                    <form method="POST" action="<?= base_url() ?>vendor/set_lang">
                                        <?php
                                        $url_value = $this->uri->segment(1);
                                        if (!empty($this->uri->segment(2))) {
                                            $url_value .= '/' . $this->uri->segment(2);
                                        }
                                        if (!empty($this->uri->segment(3))) {
                                            $url_value .= '/' . $this->uri->segment(3);
                                        }
                                        ?>
                                        <input type="hidden" id="url"  value="<?php echo $url_value; ?>" name='url'>  
                                        <select id="choose_lang" name="choose_lang" onchange="this.form.submit()">
                                            <option value="en" <?php if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'en') { ?>selected<?php } ?> >English</option>
                                            <option class="__ar" value="ar"  <?php if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'ar') { ?>selected<?php } ?>>ﺍﻟﻌﺮﺑﻴﺔ</option></select>

                                    </form>
                                </div>
                            </li>
                            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default"> <?= $vendor_notification[0]->total_notification ?> </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="external">
                                        <h3>
                                            <span class="bold"><?= $vendor_notification[0]->total_notification ?></span> <?php echo $this->lang->line("new_notifications") ?></h3>
                                        <!--<a href="page_user_profile_1.html">view all</a>-->
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                            <?php
                                            foreach ($get_all_vendor_notification as $notification) {
                                                if ($notification->inserted_on != '0000-00-00 00:00:00') {
                                                    $datas = get_timedifference($notification->inserted_on);
                                                } else {
                                                    $datas = '';
                                                }

                                                if ($notification->notification_type == NEW_USER) {
                                                    $class = 'fas fa-user-plus';
                                                } elseif ($notification->notification_type == NEW_RESTAURANT) {
                                                    $class = 'fas fa-store';
                                                } elseif ($notification->notification_type == NEW_ORDER) {
                                                    $class = 'fas fa-shopping-cart';
                                                } elseif ($notification->notification_type == PARTY_ORDER) {
                                                    $class = 'fas fa-shopping-cart';
                                                }
                                                ?>
                                                <li>
                                                    <a href="javascript:void(0);" onclick="readNotification(this)" data-id="<?= $notification->notification_id ?>" data-url="<?= $notification->redirect_url ?>">
                                                        <span class="time"><?= $datas ?></span>
                                                        <span class="details">
                                                            <span class="label label-sm label-icon label-success">
                                                                <i style="font-size: 15px;" class="<?= $class ?>"></i>
                                                            </span> <?= $notification->notification_message ?> </span>
                                                    </a>
                                                </li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </li>
                                </ul>
                            </li>


                            <?php
                            if (!empty($session_arr)) {

                                $vendordetails = GetUserDetails($session_arr->id);
                                if (isset($vendordetails[0]->image) && $vendordetails[0]->image != '') {
                                    $image = base_url() . 'uploads/vendor_images/' . $vendordetails[0]->image;
                                } else {
                                    $image = base_url('images/profile-default.png');
                                }
                                ?>
                                <li class="dropdown dropdown-user">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <img alt="" class="img-circle" src="<?= $image ?>" />
                                        <span class="username username-hide-on-mobile"><?= $session_arr->name ?></span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-default">
                                        <li>
                                            <a href="<?= base_url('vendor/profile'); ?>">
                                                <i class="icon-user"></i> <?php echo $this->lang->line("my_profile") ?></a>
                                        </li>
                                        <li>
                                            <a href="<?= base_url('vendorlogout'); ?>">
                                                <i class="icon-key"></i><?php echo $this->lang->line("logout") ?></a>
                                        </li>
                                    </ul>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->

                </div>
                <!-- END HEADER INNER -->
            </div>

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->




            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <div class="page-sidebar-wrapper">
                    <div class="page-sidebar navbar-collapse collapse">
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <li class="nav-item start liclass12 open">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="fas fa-store"></i>
                                    <span class="title">Business Details</span>
                                    <span class="arrow arrowclass12"></span>
                                </a>
                                <ul class="sub-menu ulclass12" style="display:block;">
                                    <li class="nav-item " id="12">
                                        <a href="<?= base_url() ?>vendor/storedetails" class="nav-link">
                                            <span class="title">Update Business Details</span>
                                        </a>
                                    </li>
                                    <!--<li class="nav-item " id="12">-->
                                    <!--    <a href="<?= base_url() ?>vendor/delivery_location" class="nav-link">-->
                                    <!--        <span class="title"><?php echo $this->lang->line("delivery_location") ?></span>-->
                                    <!--    </a>-->
                                    <!--</li>-->
                                    <!-- <li class="nav-item " id="12">-->
                                    <!--    <a href="<?= base_url() ?>vendor/commission" class="nav-link">-->
                                    <!--        <span class="title"><?php echo $this->lang->line("commission") ?></span>-->
                                    <!--    </a>-->
                                    <!--</li>-->
                                </ul>
                            </li>
                            <li class="nav-item start">
                                <a href="<?= base_url('vendor/menu'); ?>" class="nav-link nav-toggle">
                                    <i class="fas fa-box-open"></i>
                                    <span class="title"><?php echo $this->lang->line("menu") ?></span>
                                </a>
                            </li>
                            <?php
                            if (isset($DetailsArray[0]->party_order)) {
                                if ($DetailsArray[0]->party_order == 1) {
                                    ?>
                                    <li class="nav-item start">
                                        <a href="<?= base_url('vendor/party_menu'); ?>" class="nav-link nav-toggle">
                                            <i class="fas fa-box-open"></i>
                                            <span class="title">Party Order Menu</span>
                                        </a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                            <li class="nav-item start">
                                <a href="<?= base_url('vendor/most_selling'); ?>" class="nav-link nav-toggle">
                                    <i class="fas fa-shopping-bag"></i>
                                    <span class="title">Best Selling</span>
                                </a>
                            </li>
                            

                            <li class="nav-item start">
                                <a href="<?= base_url('vendor/orders'); ?>" class="nav-link nav-toggle">
                                    <i class="fas fa-shopping-cart"></i>
                                    <span class="title"><?php echo $this->lang->line("orders") ?></span>
                                </a>
                            </li>
                            <?php
                            if (isset($DetailsArray[0]->party_order)) {
                                if ($DetailsArray[0]->party_order == 1) {
                                    ?>

                                    <li class="nav-item start" >
                                        <a href="<?= base_url('vendor/partyorders'); ?>" class="nav-link nav-toggle">
                                            <i class="fas fa-shopping-cart"></i>
                                            <span class="title"><?php echo $this->lang->line("party_orders") ?></span>
                                        </a>
                                    </li>
                                    <?php
                                }
                            }

                            if (!empty($DetailsArray[0]->isDelivery)) {
                                ?>
                                <li class="nav-item start" >
                                    <a href="<?= base_url('vendor/delivery_boy'); ?>" class="nav-link nav-toggle">
                                        <i class="fas fa-truck"></i>
                                        <span class="title"><?php echo $this->lang->line("delivery_boy") ?></span>
                                    </a>
                                </li>
                            <?php } ?>
                            <li class="nav-item start">
                                <a href="<?= base_url('vendor/offers'); ?>" class="nav-link nav-toggle">
                                    <i class="fas fa-gift"></i>
                                    <span class="title"><?php echo $this->lang->line("offers") ?></span>
                                </a>
                            </li>


                            <li class="nav-item start">
                                <a href="<?= base_url('vendor/feedback'); ?>" class="nav-link nav-toggle">
                                    <i class="fas fa-comments"></i>
                                    <span class="title"><?php echo $this->lang->line("feedback") ?></span>
                                </a>
                            </li>
                            <li class="nav-item start">
                                <a href="<?= base_url('vendor/banner_management'); ?>" class="nav-link nav-toggle">
                                    <i class="fas fa-image"></i>
                                    <span class="title"><?php echo $this->lang->line("banner_mgmt") ?></span>
                                </a>
                            </li>
                            <li class="nav-item start">
                                <a href="<?= base_url('vendor/reservations'); ?>" class="nav-link nav-toggle">
                                    <i class="fa fa-book"></i>
                                    <span class="title">Reservations</span>
                                </a>
                            </li>
                            <li class="nav-item start">
                                <a href="<?= base_url('vendor/transactions'); ?>" class="nav-link nav-toggle">
                                    <i class="fas fa-coins"></i>
                                    <span class="title"><?php echo $this->lang->line("transactions") ?></span>
                                </a>
                            </li>
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->
                <!--<div class="alert alert-success alert_header" style="display:none;"> <strong>Success!</strong> <span id="success-message"></span> </div>-->
                <!--    <div class="alert alert-danger alert_header" style="display:none;"> <strong>Error!</strong> <span id="error-message"> </span> </div>-->