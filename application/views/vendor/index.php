<?php
$Session = GetSessionArrayVendor();

$orders=GetOrdersByVendorId($Session->id);
$count_order = count($orders);
$orders_only=GetOrdersOnlyByVendorId($Session->id);
$count_order_only = count($orders_only);
$menu = GetMenudetails($Session->id);
$count_menu = count($menu);
$party_order = GetPartyOrdersByVendorId($Session->id);
$count_party = count($party_order);
$offers = total_promocode($Session->id);

$ConditionArray = array('archive' => 0,'vendor_id' => $Session->id);
$Qry = "SELECT * FROM `user_feedback` WHERE archive=:archive AND vendor_id=:vendor_id ORDER BY id DESC";
$Array = $this->Database->select_qry_array($Qry, $ConditionArray);
$feedback_count = count($Array);


$todays = date("Y-m-d");
$oneWeek = date("Y-m-d", strtotime("-1 week"));

$order_deleved = ",(SELECT COUNT(id)  FROM `orders` WHERE archive=0 AND order_status IN (6) AND vendor_id='$Session->id') AS order_deleved";

$orderAmt = ",(SELECT (SUM(service_charge)+SUM(total_price))  FROM `orders` WHERE archive=0 AND order_status !=7 AND DATE(date) BETWEEN '$oneWeek' AND '$todays' AND vendor_id='$Session->id') AS order_amount_week";
$orderAmtYear = ",(SELECT (SUM(service_charge)+SUM(total_price))  FROM `orders` WHERE archive=0 AND order_status !=7 AND MONTH(date)='" . date('m') . "' AND YEAR(date)='" . date('Y') . "' AND vendor_id='$Session->id') AS order_amount_year";
$order_amount_total = ",(SELECT (SUM(service_charge)+SUM(total_price))  FROM `orders` WHERE archive=0  AND order_status !=7 AND vendor_id='$Session->id') AS order_amount_total";

$qry = "SELECT COUNT(id) AS party_order $orderAmt $orderAmtYear $order_amount_total $order_deleved FROM `party_order` WHERE archive=0";
$dArray = $this->Database->select_qry_array($qry);
$dArray = !empty($dArray) ? $dArray[0] : '';


?>
<style>
     .fasCd {
        opacity: 1 !important;
        line-height: 50px !important;
        margin-left: -5px !important;
        font-size: 50px !important;
    }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
           
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green" href="<?= base_url('vendor/menu')?>">
                    <div class="visual">
                        <i class="fasCd fa fa-cutlery"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value=""><?= $count_menu?></span>
                        </div>
                        <div class="desc"><?php echo $this->lang->line("menu") ?></div>
                    </div>
                </a>
                
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 red" href="<?= base_url('vendor/orders')?>">
                    <div class="visual">
                        <i class="fasCd fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value=""><?= $count_order_only?></span>
                        </div>
                        <div class="desc"> <?php echo $this->lang->line("orders") ?></div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="display:none;">
                <a class="dashboard-stat dashboard-stat-v2 purple" href="<?= base_url('vendor/partyorders')?>">
                    <div class="visual">
                        <i class="fasCd fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value=""><?= $count_party?></span>
                        </div>
                        <div class="desc"> <?php echo $this->lang->line("party_orders") ?> </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 blue" href="<?= base_url('vendor/offers')?>">
                    <div class="visual">
                        <i class="fasCd fas fa-percent"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value=""><?= $offers[0]->total_promocode?></span>
                        </div>
                        <div class="desc"> <?php echo $this->lang->line("offers") ?> </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a style="background: #8c7d30;" class="dashboard-stat dashboard-stat-v2 purple" href="<?= base_url('vendor/feedback')?>">
                    <div class="visual">
                        <i class="fasCd fa fa-comments-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value=""><?= $feedback_count?></span>
                        </div>
                        <div class="desc"> <?php echo $this->lang->line("feedback") ?> </div>
                    </div>
                </a>
            </div>
            
            
            
            
            
            
            
            
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a style="background: #9C27B0;" class="dashboard-stat dashboard-stat-v2 purple" href="<?= base_url('vendor/orders') ?>">
                    <div class="visual">
                        <i class="fasCd fa"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value=""><?= !empty($dArray->order_deleved) ? $dArray->order_deleved : '0' ?></span>
                        </div>
                        <div class="desc"> Order delivered </div>
                    </div>
                </a>
            </div>
             <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a style="background: #1665a3;" class="dashboard-stat dashboard-stat-v2 purple" href="<?= base_url('vendor/orders') ?>">
                    <div class="visual">
                        <i class="fasCd fa"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value=""><?= !empty($dArray->order_amount_week) ? 'PKR '.DecimalAmount($dArray->order_amount_week) : '0' ?></span>
                        </div>
                        <div class="desc"> Weekly Order Amount </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a style="background: #4CAF50;" class="dashboard-stat dashboard-stat-v2 purple" href="<?= base_url('vendor/orders') ?>">
                    <div class="visual">
                        <i class="fasCd fa"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value=""><?= !empty($dArray->order_amount_year) ? 'PKR '.DecimalAmount($dArray->order_amount_year) : '0' ?></span>
                        </div>
                        <div class="desc"> <?= date('F') ?> Order Amount </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a style="background: #795548;" class="dashboard-stat dashboard-stat-v2 purple" href="<?= base_url('vendor/orders') ?>">
                    <div class="visual">
                        <i class="fasCd fa"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value=""><?= !empty($dArray->order_amount_total) ? 'PKR '.DecimalAmount($dArray->order_amount_total) : '0' ?></span>
                        </div>
                        <div class="desc"> Total Order Amount </div>
                    </div>
                </a>
            </div>
            
            
            
            
            
            
            
            
        </div>
        
        
         <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Todays Order</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no") ?></th>
                                    <th><?php echo $this->lang->line("order_no") ?></th>
                                    <!--<th><php echo $this->lang->line("customer_name") ?></th>-->
                                    <th><?php echo $this->lang->line("price") ?></th>
                                    <th><?php echo $this->lang->line("payment_type") ?></th>
                                    <th><?php echo $this->lang->line("order_status") ?></th>
                                    <th><?php echo $this->lang->line("order_date") ?></th>
                                    <th><?php echo $this->lang->line("action") ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $Select = ',U.name,U.id as user_id,O.order_status,OS.status_color,OS.status_name';
                                $Join = 'LEFT JOIN users U ON U.id=O.user_id LEFT JOIN order_status OS ON OS.status_id=O.order_status';
                                $qry = "SELECT O.* $Select FROM `orders` O $Join WHERE 1 AND DATE(O.date)='" . date('Y-m-d') . "' AND vendor_id='$Session->id' AND O.archive=0 ORDER BY O.id DESC";
                                $order = $this->Database->select_qry_array($qry);


                                for ($i = 0; $i < count($order); $i++) {
                                    $d = $order[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->orderno ?></td>
                                        <!--<td><= $d->name ?></td>-->
                                        <td><?= $d->total_price + $d->service_charge ?></td>
                                        <td><?= GetPaymentType($d->mode_of_payment) ?></td>
                                        <td>
                                            <?php
                                            // $OrserStatusName = GetOrderStatusName($d->order_status);
                                            ?>
                                            <span class="label label-sm label-success" style="    background-color: <?= $d->status_color ?>;"><?= !empty($d->status_name) ? $d->status_name : 'Received' ?></span>
                                            <!--<span class="label label-sm label-success <?= $OrserStatusName ?>"><?= $OrserStatusName ?></span>-->
                                        </td>
                                        <td><?= date('D, d M-Y H:i', strtotime($d->date)) ?></td>
                                        <td>
                                            <a href="<?= base_url('Vendor/OrderDetails/' . base64_encode($d->id)) ?>" title="Order summary"><span class="label label-sm label-danger"><i class="fa fa-eye"></i></span></a>
                                        </td>
                                    </tr>
                                <?php } ?>

                                <?php if (empty($order)) {
                                    ?><tr>
                                        <td style="text-align: center;" colspan="9">no order found.</td>
                                    </tr><?php }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>
        
        
        
         <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Last 12 month Order List</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">

                        <div id="newchartdiv"></div>
                        <?php
                        $todays = date('Y-m-d');
                        $gAryrrr = [];
                        for ($i = 0; $i < 12; $i++) {
                            $date = date("Y-m-d", strtotime("-$i month", strtotime($todays)));
                            $month = date("m", strtotime($date));
                            $year = date("Y", strtotime($date));
                            $qry = "SELECT COUNT(id) AS total FROM `orders` WHERE MONTH(date)='" . $month . "' AND YEAR(date)='" . $year . "' AND vendor_id='$Session->id' AND archive=0";

                            $order = $this->Database->select_qry_array($qry);
                            $toto = !empty($order[0]->total) ? $order[0]->total : 0;

                            $gAryrrr[] = array('year' => date('M Y', strtotime($date)), 'income' => $toto);
                        }
                        
                        ?>

                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>


        <div class="row">
            <div class="col-md-12">


                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Receiving Total Order One Month Graph</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chartdiv"></div>	
                    </div>
                </div>



            </div>

        </div>
        
        
        
        
        
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->





<style>
    #chartdiv,#newchartdiv {
        width: 100%;
        height: 500px;
    }
    .dashboard-stat.purple .details .number{
            font-size: 17px !important;
    }
</style>

<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
<?php
$todays = date('Y-m-d');
$gAry = [];
for ($i = 0; $i < 30; $i++) {
    $date = date("Y-m-d", strtotime("-$i days", strtotime($todays)));
    $qry = "SELECT COUNT(id) AS total FROM `orders` WHERE DATE(date)='" . $date . "' AND vendor_id='$Session->id' AND archive=0";
    $order = $this->Database->select_qry_array($qry);
    $toto = !empty($order[0]->total) ? $order[0]->total : 0;

    $gAry[] = array('category' => date('d M Y', strtotime($date)), 'value' => $toto);
}
?>
<!-- Chart code -->
<script>
    am4core.ready(function () {

// Themes begin
        am4core.useTheme(am4themes_animated);
// Themes end

        var chart = am4core.create("chartdiv", am4charts.XYChart);



        chart.data = JSON.parse('<?= json_encode($gAry) ?>');
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.dataFields.category = "category";
        categoryAxis.renderer.minGridDistance = 15;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.renderer.grid.template.strokeDasharray = "1,3";
        categoryAxis.renderer.labels.template.rotation = -90;
        categoryAxis.renderer.labels.template.horizontalCenter = "left";
        categoryAxis.renderer.labels.template.location = 0.5;

        categoryAxis.renderer.labels.template.adapter.add("dx", function (dx, target) {
            return -target.maxRight / 2;
        })

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.tooltip.disabled = true;
        valueAxis.renderer.ticks.template.disabled = true;
        valueAxis.renderer.axisFills.template.disabled = true;

        var series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.categoryX = "category";
        series.dataFields.valueY = "value";
        series.tooltipText = "{valueY.value}";
        series.sequencedInterpolation = true;
        series.fillOpacity = 0;
        series.strokeOpacity = 1;
        series.strokeDashArray = "1,3";
        series.columns.template.width = 0.01;
        series.tooltip.pointerOrientation = "horizontal";

        var bullet = series.bullets.create(am4charts.CircleBullet);

        chart.cursor = new am4charts.XYCursor();

        chart.scrollbarX = new am4core.Scrollbar();
        chart.scrollbarY = new am4core.Scrollbar();


    }); // end am4core.ready()
</script>

<script>
    am4core.ready(function () {
// Themes begin
        am4core.useTheme(am4themes_animated);
// Themes end
// Create chart instance
        var chart = am4core.create("newchartdiv", am4charts.XYChart);
// Export
        chart.exporting.menu = new am4core.ExportMenu();
// Data for both series
        var data = JSON.parse('<?= json_encode($gAryrrr) ?>');;

        /* Create axes */
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "year";
        categoryAxis.renderer.minGridDistance = 30;

        /* Create value axis */
        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        /* Create series */
        var columnSeries = chart.series.push(new am4charts.ColumnSeries());
        columnSeries.name = "Income";
        columnSeries.dataFields.valueY = "income";
        columnSeries.dataFields.categoryX = "year";

        columnSeries.columns.template.tooltipText = "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]"
        columnSeries.columns.template.propertyFields.fillOpacity = "fillOpacity";
        columnSeries.columns.template.propertyFields.stroke = "stroke";
        columnSeries.columns.template.propertyFields.strokeWidth = "strokeWidth";
        columnSeries.columns.template.propertyFields.strokeDasharray = "columnDash";
        columnSeries.tooltip.label.textAlign = "middle";

        var lineSeries = chart.series.push(new am4charts.LineSeries());
        lineSeries.name = "Expenses";
        lineSeries.dataFields.valueY = "expenses";
        lineSeries.dataFields.categoryX = "year";

        lineSeries.stroke = am4core.color("#fdd400");
        lineSeries.strokeWidth = 3;
        lineSeries.propertyFields.strokeDasharray = "lineDash";
        lineSeries.tooltip.label.textAlign = "middle";

        var bullet = lineSeries.bullets.push(new am4charts.Bullet());
        bullet.fill = am4core.color("#fdd400"); // tooltips grab fill from parent by default
        bullet.tooltipText = "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]"
        var circle = bullet.createChild(am4core.Circle);
        circle.radius = 4;
        circle.fill = am4core.color("#fff");
        circle.strokeWidth = 3;
        chart.data = data;

    }); // end am4core.ready()
</script>
