<?php
$Session = GetSessionArrayVendor();
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?php echo $this->lang->line("menu_details") ?></span>
                            <a href="<?= base_url('vendor/menu/add-new'); ?>" class="btn btn-sm green small"> <?php echo $this->lang->line("add_new") ?>
                                <i class="fa fa-plus"></i>
                            </a>
                            
                            <a  target="_blank" href="<?= base_url('vendor/menucategory'); ?>" class="btn btn-sm green small"> Menu Category</a>
                            <?php
                            if($Session->store_type!='1'){
                               ?>
                            <a  href="<?= base_url('vendor/menuexcelupload'); ?>" class="btn btn-sm green small"> Upload From Excel</a>
                            <a  href="<?= base_url('vendor/exportmenu'); ?>" class="btn btn-sm red small"> Export Excel</a>       
                                   <?php 
                            }
                            ?>
                            
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover commontableXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no") ?></th>
                                    <th><?php echo $this->lang->line("category") ?></th>
                                    <th><?php echo $this->lang->line("menu") ?></th>
                                    <th><?php echo $this->lang->line("status") ?></th>
                                    <th><?php echo $this->lang->line("action") ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($Categories); $i++) {
                                    $d = $Categories[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?php $cat = GetCategoryById($d->category_id);
                                            echo $cat[0]->category_name;
                                        ?></td>
                                        <td><?= $d->menu_name ?></td>
                                        <td><?= $d->status == 0 ? '<span class="label label-sm label-success">'.$this->lang->line("active").'</span>' : '<span class="label label-sm label-danger">'.$this->lang->line("inactive").'</span>' ?></td>
                                        <td>
                                            <a href="<?= base_url('vendor/menu/add-new/' . base64_encode($d->id)) ?>"><span class="label label-sm label-success"><i class="fa fa-pencil" aria-hidden="true"></i></span></a> 
                                            <!--<a ref="javascript:void(0)" class="common_delete" Id="<= $d->id ?>" table="menu_list" redrurl="menu"><span class="label label-sm label-<= empty($d->archive) ? 'danger' : 'info' ?>"><= empty($d->archive) ? '<i class="fas fa-ban"></i>' : '<i class="far fa-check-circle"></i>' ?></span></a>-->
                                              <a href="javascript:void(0)" updatejson='{"archive":"1"}' menuId="<?= $d->id ?>" condjson='{"id":"<?= $d->id ?>"}' dbtable="menu_list" class="menudelete" title="Remove"><span class="label label-sm label-danger"><i class="fas fa-trash-alt"></i></span></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
