<?php
$Session = GetSessionArrayVendor();
$Categories = GetCategories($Session->id);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Menu Category</span>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover commontableXXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Category</th>
                                    <th>Position</th>
                                    <th>Status</th>
                                    <!--<th>Action</th>-->
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($Categories); $i++) {
                                    $d = $Categories[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?
                                            $cat = GetCategoryById($d->category_id);
                                            echo $cat[0]->category_name;
                                        ?></td>
                                        <td>
                                            <form>
                                                <input style="width:60px;" name="position" type="text" class="form-control position" autocomplete="off"  value=" <?=$d->position?>">
                                                <a class="btn default update_position" table="menu_list" tableid='<?=$d->id?>'  style="margin-top:-55px;margin-left:71px;">Update</a> 
                                                <a class="btn default reset_position" table="menu_list" tableid='<?=$d->id?>' style="margin-top: -56px;margin-left: 0px;">Reset</a>
                                             </form>
                                        </td>
                                        <td><?= $d->status == 0 ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Inactive</span>' ?></td>
                                        <!--<td>-->
                                        <!--    <a href="<?= base_url('vendor/menu/add-new/' . base64_encode($d->id)) ?>"><span class="label label-sm label-success"><i class="fa fa-pencil" aria-hidden="true"></i></span></a> -->
                                        <!--    <a ref="javascript:void(0)" class="common_delete" Id="<?= $d->id ?>" table="menu_list" redrurl="menu"><span class="label label-sm label-<?= empty($d->archive) ? 'danger' : 'info' ?>"><?= empty($d->archive) ? '<i class="fas fa-ban"></i>' : '<i class="far fa-check-circle"></i>' ?></span></a>-->
                                        <!--</td>-->
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
