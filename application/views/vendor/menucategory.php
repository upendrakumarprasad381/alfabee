<?php
$Session = GetSessionArrayVendor();
$catid = !empty($_REQUEST['catid']) ? base64_decode($_REQUEST['catid']) : '';

$cat=GetCategory($catid);
$cat=!empty($cat) ?$cat[0] : '';

?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <?php if (empty($catid)) { ?>
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <span class="caption-subject bold uppercase">Menu Category </span>
                            </div>
                        </div>
                        <input type="hidden" id="vendor_id" value="<?= $Session->id ?>">
                        <div class="portlet-body">

                            <div class="row">
                                <?php
                                $qry = "SELECT C.*,CP.ordercat_view FROM `category_details` C JOIN `menu_list` ML ON ML.`category_id`=C.`id` LEFT JOIN category_positions CP ON CP.category_id=ML.category_id AND CP.vendor_id=ML.vendor_id WHERE ML.vendor_id='$Session->id' AND C.archive=0 AND C.status=0 AND ML.archive=0 AND ML.status=0  GROUP BY ML.category_id ORDER BY CP.ordercat_view ASC";
                                $cArray = $this->Database->select_qry_array($qry);
                                $option = '';
                                for ($i = 0; $i < count($cArray); $i++) {
                                    $d = $cArray[$i];
                                    $option = $option . ' <li id="' . $d->id . '" >' . $d->category_name . ' <a href="' . base_url('vendor/menucategory?catid=' . base64_encode($d->id)) . '"><span class="label label-sm label-success">View Menu</span></a></li>';

                                    $sql = "SELECT P.posid  FROM `category_positions` P WHERE P.`vendor_id` = '$Session->id' AND P.category_id='$d->id'";
                                    $c44Array = $this->Database->select_qry_array($sql);
                                    $update['ordercat_view'] = $i;
                                    $update['vendor_id'] = $Session->id;
                                    $update['category_id'] = $d->id;
                                    if (empty($c44Array)) {
                                        $this->Database->insert('category_positions', $update);
                                    }
                                }
                                ?>
                                <div class="col-sm-12">
                                    <p><strong>Note: select and drag and drop to assign the order.</strong></p>
                                </div>
                                <div class="col-sm-12">
                                    <ul id="sortable">
                                        <?= $option ?>
                                    </ul>
                                </div>
                                <div class="col-sm-12">
                                    <button onclick="VENDRCOMN.menucategoryorderviewupdate();" type="button" class="btn btn-primary">Submit</button>
                                </div>

                            </div>

                        </div>
                    </div>
                <?php } else {
                    ?>
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <span class="caption-subject bold uppercase">Menu listing - <?= $cat->category_name ?></span>
                            </div>
                        </div>
                        <input type="hidden" id="vendor_id" value="<?= $Session->id ?>">
                        <div class="portlet-body">

                            <div class="row">
                                <?php
                                $option = '';
                                $sql = "SELECT * FROM `menu_list` WHERE archive=0 AND status=0 AND vendor_id='$Session->id' AND category_id='$catid' ORDER BY order_view ASC";
                                $c44Array = $this->Database->select_qry_array($sql);
                                for ($i = 0; $i < count($c44Array); $i++) {
                                    $d = $c44Array[$i];
                                    $option = $option . ' <li id="' . $d->id . '" >' . $d->menu_name . ' </li>';
                                }
                                ?>
                                <div class="col-sm-12">
                                    <p><strong>Note: select and drag and drop to assign the order.</strong></p>
                                </div>
                                <div class="col-sm-12">
                                    <ul id="sortable">
                                        <?= $option ?>
                                    </ul>
                                </div>
                                <div class="col-sm-12">
                                    <button onclick="VENDRCOMN.menulistorderviewupdate();" type="button" class="btn btn-primary">Submit</button>
                                </div>

                            </div>

                        </div>
                    </div>

                    <?php
                }
                ?>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<style>
    #sortable li{
        margin-bottom: 8px;
    }
</style>