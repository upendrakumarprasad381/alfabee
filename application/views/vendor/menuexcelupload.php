

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">

                            <span class="caption-subject bold uppercase">Upload Menu</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">

                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Upload Menu From excel <br>
                                        <small><a href="<?= base_url('images/menu-upload-excel-formate.xlsx') ?>">Download formate</a></small>
                                    </label>
                                    <div class="col-md-6">
                                        <input value="" id="UploadExcel" type="file" class="form-control" autocomplete="off">
                                    </div>
                                </div>


                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" class="btn blue" onclick="VENDRCOMN.uploadExcelMenu();" id="uploadExcelMenu">
                                            <i class="fa fa-check"></i> <?php echo $this->lang->line("submit") ?></button>
                                        <button type="button" onclick="window.location = '<?= base_url('vendor/menu'); ?>';" class="btn default"><?php echo $this->lang->line("cancel") ?></button>
                                    </div>
                                </div>
                            </div>

                            <!-- END FORM-->
                    </div>
                    </form>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
