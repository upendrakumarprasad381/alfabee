<?php
    $Qry = "SELECT * FROM `category_details` WHERE category_name LIKE 'Best Selling%'";
    $Array = $this->Database->select_qry_array($Qry);
    
    $Session = GetSessionArrayVendor();

   // echo $Session->id;exit;
    $get_category = "SELECT category_details.id, category_name FROM `menu_list` LEFT JOIN category_details ON category_details.id=menu_list.category_id WHERE vendor_id=".$Session->id ." AND category_details.archive=0 GROUP BY category_details.id";
    $Array1 = $this->Database->select_qry_array($get_category);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                        
                            <span class="caption-subject bold uppercase">Best Selling</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="Id" value="">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("category") ?></label>
                                    <div class="col-md-6" id="category_list">
                                        <input type="hidden" id="most_selling" value="<?= !empty($Array[0]->id) ? $Array[0]->id : ''?>">
                                        <input type="text" id="most_selling" class="form-control" value="<?= !empty($Array[0]->category_name) ? $Array[0]->category_name : ''?>" disabled="true">
                                    </div>
                                </div>
                                
                                    
                                <?php
                                $array_name = [];
                                $qry = "SELECT * FROM `most_selling` WHERE vendor_id=".$Session->id;
                                $array = $this->Database->select_qry_array($qry);
                                // if(!empty($array))
                                // {
                                    // $menu_list = explode(',',$array[0]->menu_id);
                                    foreach($array as $a)
                                    {
                                        array_push($array_name,$a->menu_id);
                                    }
                                    foreach($Array1 as $cat)
                                    {
                                        ?>
                                        <div class="form-group">
                                        <label class="control-label col-md-3"><?= $cat->category_name?></label>
                                        
                                        <?php
                                        $menu = "SELECT * FROM `menu_list` WHERE category_id =".$cat->id." AND vendor_id=".$Session->id ;
                                        // $menu = "SELECT * FROM `menu_list` LEFT JOIN category_details ON category_details.id =menu_list.category_id WHERE category_id =".$cat->id." AND vendor_id=".$Session->id ;
                                        $MenuArray = $this->Database->select_qry_array($menu);
                                    ?>
                                    <div class="col-md-6" id="category_list">
                                    <?php
                                        foreach($MenuArray as $k=>$menu)
                                        {
                                            if(in_array($menu->id,$array_name))
                                            {
                                            ?>
                                            <input type="checkbox" name="menu_list[]" id="menu_list" catid="<?= $cat->id ?>" value="<?= $menu->id?>" checked><label class="control-label"><?= $menu->menu_name?></label>
                                            <?php
                                            }else{
                                                ?>
                                            <input type="checkbox" name="menu_list[]" id="menu_list" catid="<?= $cat->id ?>" value="<?= $menu->id?>" ><label class="control-label"><?= $menu->menu_name?></label>
                                                <?php
                                            }
    
                                    ?>
                                    
                                    <?php
                                        }
                                    ?>
                                    </div>
                                    </div>
                                    <?php
                                    // }
                                }
                                ?>
                              
                               
                                
                            <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="button" class="btn blue" id="SubmitMostSelling">
                                                <i class="fa fa-check"></i> <?php echo $this->lang->line("submit") ?></button>
                                            <button type="button" onclick="window.location = '<?= base_url('vendor/menu'); ?>';" class="btn default"><?php echo $this->lang->line("cancel") ?></button>
                                        </div>
                                    </div>
                                </div>

                                <!-- END FORM-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>





        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
