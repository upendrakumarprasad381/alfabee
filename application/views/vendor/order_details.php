<?php
$Order = GetOrderByOrderId($orderid);
$PaymentType = GetPaymentType($Order[0]->mode_of_payment);
$UserArray = GetUserDetails($Order[0]->user_id);
;
$AddressArray = GetAddressArrayByAddressId($Order[0]->delivery_address);
$op = GetOrderDetailsByOrderId($orderid);
$Session = GetSessionArrayVendor();
$restautant_details = GetRestaurantById($Session->id);

$time = date('H:i', strtotime($Order[0]->date));

$StatusList = GetOrderStatusList(0);
$his = GetorderstatushistoryBy($orderid);
$singleStatus = json_decode(json_encode($his), true);
$singleStatus = array_column($singleStatus, 'status_id');


$get_restaurant_details = GetRestaurantDetails($Order[0]->vendor_id);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <input value="<?= $orderid ?>" type="hidden" id="order_id">
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <input type="hidden" value="<?= $Order[0]->user_id ?>" id="user_id">
                    <input type="hidden" value="<?= $Session->id ?>" id="vendor_id">
                    <input type="hidden" id="order_time" value="<?= !empty($Order[0]->date) ? $Order[0]->date : '' ?>">
                    <div class="portlet-title">
                        <div class="caption" style="padding: 0px;">
                            <?php
                            $Select = 'selected="selected"';
                            $UserProfile = 'uploads/user_images/' . $UserArray[0]->image;
                            if (is_file(HOME_DIR . $UserProfile)) {
                                $ImageHtml = '<img style="border-radius: 50% !important;width:32px" src="' . base_url() . $UserProfile . '" />';
                            } else {
                                $ImageHtml = '<i style="font-size: 36px;" class="fab fa-first-order"></i>';
                            }
                            echo $ImageHtml;
                            ?>
                            <span class="caption-subject font-dark sbold uppercase"> Order No - <?= $Order[0]->orderno ?>
                                <span class="hidden-xs">| <?= date('D d M-Y h:i A', strtotime($Order[0]->date)) ?></span>
                            </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="portlet blue-hoki box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i  class="fab fa-first-order"></i> <?php echo $this->lang->line("order_details") ?>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("order_no") ?>: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= $Order[0]->orderno ?>
                                                    </div>
                                                </div>


                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Order Type: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= $Order[0]->delivery_type == '1' ? 'Deliver' : 'Self Pickup' ?>
                                                    </div>
                                                </div>


                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("order_date_time") ?>: </div>
                                                    <div class="col-md-7 value">  <?= date('D d M-Y h:i A', strtotime($Order[0]->date)) ?></div>
                                                </div>
                                                <?php
                                                if (!empty($Order[0]->special_request)) {
                                                    ?>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("special_request") ?>: </div>
                                                        <div class="col-md-7 value"> <?= $Order[0]->special_request ?> </div>
                                                    </div>

                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                // if($Order[0]->order_status == ORDER_CONFIRMED)
                                                // {
                                                ?>
                                                <!--<div class="row static-info">-->
                                                <!--<div class="col-md-5 name"> <?php echo $this->lang->line("order_status") ?>: </div>-->
                                                <!--    <div class="col-md-7 value">-->
                                                <!--        <label for="id04" class="strong-label post-as">Waiting for admin to assign delivery boy</label>-->
                                                <!--    </div>-->
                                                <!--</div>-->

                                                <!--<div class="row static-info">-->
                                                <!--    <div class="col-md-5 name">Assign DeliveryBoy: </div>-->

                                                <!--    <div class="col-md-7 value">  -->
                                                <!--    <input type="hidden" value="<?= $orderid ?>" id="orderId">-->
                                                <!--        <select class="form-control" id="deliveryboy_id" orderId="<?= $orderid ?>">-->
                                                <!--            <option value="">select</option>-->
                                                <?php
// $listArray = GetdeliveryboyByVendor($Session->id);
// $selectedprov = !empty($Order[0]->deliveryboy_id) ? $Order[0]->deliveryboy_id : '';
// for ($i = 0; $i < count($listArray); $i++) {
//     $dmk = $listArray[$i];
                                                ?>  
                                                                <!--<option <?= $selectedprov == $dmk->id ? 'selected' : '' ?> value="<?= $dmk->id ?>"><?= $dmk->name ?></option>-->
                                                <?php
                                                // }
                                                ?>
                                                <!--        </select>-->
                                                <!--    </div>-->
                                                <!--</div>-->
                                                <?php
// }
                                                ?>
                                                <?php
                                                //  if($Order[0]->order_status == 0)
                                                //  {

                                                $datetime1 = strtotime("now");
                                                $datetime2 = strtotime($Order[0]->date);
                                                $interval = abs($datetime2 - $datetime1);
                                                $minutes = round($interval / 60);


                                                if ($minutes < 3 && ($Order[0]->order_status == 0 || $Order[0]->order_status != 5)) {
                                                    ?>
                                                    <div class="row static-info" id="time_left">
                                                        <div class="col-md-5 name"> Time Left to Cancel Order: </div>
                                                        <div class="col-md-7 value">
                                                            <span id="timer"></span>
                                                        </div>
                                                    </div>
                                                    <?php
                                                } else {
                                                    $studtdif = !empty($Order[0]->order_status) ? $Order[0]->order_status : '';
                                                    // if ($Order[0]->delivery_type == 1) {
                                                    ?>   

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("order_status") ?>: </div>
                                                        <div class="col-md-7 value">
                                                            <select class="form-control" id="change_status" style="">
                                                                <?php
                                                                if (empty($studtdif)) {
                                                                    ?>
                                                                    <option  disabled="" selected value="">Select</option> 
                                                                    <option msg="Are you sure this order want to confirm ?" title="Order Confirmed" value="1" >Order Confirmed</option>
                                                                    <option msg="Are you sure this order want to cancel ?" title="Order Cancel" value="7"  >Order Cancel</option>
                                                                    <?php
                                                                } else if ($studtdif == '7') {
                                                                    ?><option disabled selected value="7"  >Order Cancel</option><?php
                                                                } else if ($studtdif == '1') {
                                                                    ?>
                                                                    <option disabled selected value="1" >Order Confirmed</option>
                                                                    <option msg="Are you sure this order is preparing ?" title="Preparing Order" value="2"  >Preparing Order</option>
                                                                    <?php
                                                                } else if ($studtdif == '2') {
                                                                    if ($Order[0]->delivery_type == 1) {
                                                                        ?> 
                                                                        <option disabled selected value="2"  >Preparing Order</option>
                                                                        <option msg="Are you sure this order is ready for delivery ?" title="Ready for delivery" value="14" >Ready for delivery</option>
                                                                        <?php
                                                                    } else {
                                                                        ?> 
                                                                        <option disabled selected value="2"  >Preparing Order</option>
                                                                        <option msg="Are you sure this order is ready for pickup ?" title="Ready for pickup" value="8"  >Ready for pickup</option>
                                                                        <?php
                                                                    }
                                                                }  else if ($studtdif == '8') {
                                                                    ?>
                                                                    <option disabled selected value="8"  >Ready for pickup</option>
                                                                    <option msg="Are you sure this order has been delivered ?" title="Order Delivered" value="6"  >Order Delivered</option>
                                                                    <?php
                                                                } else {
                                                                    $statusId = GetorderstatusBy($Order[0]->order_status);
                                                                    $name4440 = !empty($statusId->status_name) ? $statusId->status_name : 'NA';
                                                                    ?><option disabled="" selected value="NANAN"  ><?= $name4440 ?></option><?php
                                                                }
                                                                ?>

                                                            </select>
                                                            
                                                          <?php /*  ?>
                                                            <select class="form-control" id="change_status22" style="margin-top: 10px;">
                                                                <?php
                                                                $dicREDYPICKDEL = '';
                                                                if (empty($restautant_details[0]->isDelivery) && $Order[0]->delivery_type == '1') {
                                                                    $dicREDYPICKDEL = 'disabled';
                                                                }

                                                                $disbleOption = '';
                                                                if ($studtdif == '7') {
                                                                    $disbleOption = 'disabled';
                                                                }
                                                                ?>
                                                                <option <?= $disbleOption ?> disabled="" value="">Select</option>                               
                                                                <option <?= $disbleOption ?> value="1" <?= $studtdif == 1 ? 'selected="selected"' : '' ?> <?= in_array('1', $singleStatus) ? 'disabled' : '' ?>>Order Confirmed</option>
                                                                <option <?= $disbleOption ?> value="2" <?= $studtdif == 2 ? 'selected="selected"' : '' ?> <?= in_array('2', $singleStatus) ? 'disabled' : '' ?>>Preparing Order</option>

                                                                <?php if ($Order[0]->delivery_type == 1) { ?>
                                                                    <option <?= $disbleOption ?> value="14" <?= $studtdif == 14 ? 'selected="selected"' : '' ?> <?= in_array('14', $singleStatus) ? 'disabled' : '' ?>>Ready for delivery</option>
                                                                    <option <?= $disbleOption ?> value="5" <?= $studtdif == 5 ? 'selected="selected"' : '' ?> <?= in_array('5', $singleStatus) ? 'disabled' : '' ?> <?= $dicREDYPICKDEL ?>>Order on the way</option>
                                                                <?php } else {
                                                                    ?> <option <?= $disbleOption ?> value="8" <?= $studtdif == 8 ? 'selected="selected"' : '' ?> <?= in_array('8', $singleStatus) ? 'disabled' : '' ?>>Ready for pickup</option><?php }
                                                                ?>
                                                                <option <?= $disbleOption ?> value="6" <?= $studtdif == 6 ? 'selected="selected"' : '' ?> <?= in_array('6', $singleStatus) ? 'disabled' : '' ?> <?= $dicREDYPICKDEL ?>>Order Delivered</option>
                                                                <option <?= $disbleOption ?> value="7" <?= $studtdif == 7 ? 'selected="selected"' : '' ?>  <?= !empty($studtdif) ? 'disabled' : '' ?> >Order Cancel</option>


                                                            </select>
                                                            <?php */ ?>
                                                        </div>
                                                    </div>
                                                   


                                                    <?php if ($Order[0]->delivery_type == '1' && $restautant_details[0]->isDelivery == 1) { ?>
                                                        <div class="row static-info">
                                                            <div class="col-md-5 name"> Assign delivery boys: </div>
                                                            <div class="col-md-7 value">
                                                                <select class="form-control" <?= empty($studtdif) || $studtdif == '7' ? 'disabled' : '' ?> id="assign_rider" onchange="VENDRCOMN.assignRiderbyVendor();">
                                                                    <option value="">Select</option>  
                                                                    <?php
                                                                    $assign_rider = isset($Order[0]->assign_rider) ? $Order[0]->assign_rider : '';
                                                                    $qry = "SELECT * FROM `users` WHERE user_type=4 AND deliveryboy_added_by='$Session->id' AND archive=0 ORDER BY id DESC";
                                                                    $crrrArray = $this->Database->select_qry_array($qry);
                                                                    for ($i = 0; $i < count($crrrArray); $i++) {
                                                                        $d = $crrrArray[$i];
                                                                        ?><option <?= $assign_rider == $d->id ? 'selected' : '' ?> value="<?= $d->id ?>"><?= $d->name ?></option>  <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php
                                                }
//  }
                                                ?>
                                                <?php
                                                if (!empty($Order->promo_code)) {
                                                    ?>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("promo_code") ?>: </div>
                                                        <div class="col-md-7 value"> <?= $Order->promo_code ?> </div>
                                                    </div>
                                                    <?php
                                                }
                                                if (!empty($Order[0]->cancellation_msg)) {
                                                    ?>
                                                    <div class="row static-info">
                                                        <?php if (!empty($Order[0]->cancellation_reason)) { ?>
                                                            <div class="col-md-5 name"> <?php echo $this->lang->line("cancel_reason") ?>: </div>
                                                            <div class="col-md-7 value"> <?= $Order[0]->cancellation_reason ?> </div>

                                                        <?php } ?>    
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("message") ?>: </div>
                                                        <div class="col-md-7 value"> <?= $Order[0]->cancellation_msg ?> </div>
                                                    </div>
                                                    <?php
                                                }

                                                if (!empty($Order[0]->scheduled_delivery_date) && $Order[0]->scheduled_delivery_date != '0000-00-00') {
                                                    ?>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> scheduled date: </div>
                                                        <div class="col-md-7 value">
                                                            <?= date('d/m/Y', strtotime($Order[0]->scheduled_delivery_date)) ?>
                                                            <?php
                                                            if (!empty($Order[0]->scheduled_delivery_time) && $Order[0]->scheduled_delivery_time != '00:00:00') {
                                                                echo date('h:i A', strtotime($Order[0]->scheduled_delivery_time));
                                                            } if (!empty($Order[0]->scheduled_deliveryend_time) && $Order[0]->scheduled_deliveryend_time != '00:00:00') {
                                                                echo ' - ' . date('h:i A', strtotime($Order[0]->scheduled_deliveryend_time));
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>

                                                    <?php
                                                }


                                                $fileName = "uploads/prescription/" . (!empty($Order[0]->upload_prescription) ? $Order[0]->upload_prescription : '');
                                                if (is_file(HOME_DIR . $fileName)) {
                                                    ?>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> Prescription document: </div>
                                                        <div class="col-md-7 value"> <a target="_blank" href="<?= base_url($fileName) ?>">Click here to download</a> </div>
                                                    </div>
                                                <?php } ?>


                                                <!--<div class="row static-info">-->
                                                <!--    <div class="col-md-5 name"> Last Update: </div>-->
                                                <!--    <div class="col-md-7 value"> -->
                                                <?php
// if (!empty($Order->LastUpdatedBy)) {
//     echo $Order->LastUpdatedBy . ' - ' . date('D, d M-Y h:i A', strtotime($Order->lastupdate));
// }
                                                ?>
                                                <!--        </div>-->
                                                <!--</div>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="portlet blue-hoki box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fas fa-user"></i><?php echo $this->lang->line("payment_information") ?></div>
                                            </div>
                                            <div class="portlet-body">


                                                <?php if ($Order[0]->delivery_type == '1') { ?>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> Distance </div>
                                                        <div class="col-md-7 value"> <?= $Order[0]->distance_ord ?> KM</div>
                                                    </div>
                                                <?php } ?>



                                                <div class="row static-info">
                                                    <div class="col-md-5 name"><?php echo $this->lang->line("payment_type") ?>: </div>
                                                    <div class="col-md-7 value"> <?= $PaymentType ?></div>
                                                </div>
                                                <?php
                                                if ($Order[0]->mode_of_payment == '1') {
                                                    ?>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("payment_id") ?>: </div>
                                                        <div class="col-md-7 value"> <?= $Order->payment_id ?></div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("sub_total") ?>: </div>
                                                    <div class="col-md-7 value" style="user-select: all;">PKR <?= number_format($Order[0]->price, 2) ?> </div>
                                                </div>
                                                <?php if (!empty($CanAmt)) { ?>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("cancelled_amt") ?>: </div>
                                                        <div class="col-md-7 value" style="user-select: all;color: red;">PKR <?= DecimalAmount($CanAmt) ?> </div>
                                                    </div>                                                
                                                <?php } ?>
                                                <?php if (!empty($Order[0]->discount)) { ?>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("discount") ?>: </div>
                                                        <div class="col-md-7 value" style="user-select: all;">PKR <?= number_format($Order[0]->discount, 2) ?>  </div>
                                                    </div>
                                                <?php } ?>
                                                <?php if (!empty($Order[0]->loyality_discount)) { ?>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> Loyality <?php echo $this->lang->line("discount") ?>: </div>
                                                        <div class="col-md-7 value" style="user-select: all;">PKR <?= number_format($Order[0]->loyality_discount, 2) ?>  </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="row static-info" >
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("delivery_fee") ?>: </div>
                                                    <div class="col-md-7 value"> <span style="color: green;"><?= !empty($Order[0]->service_charge) ? 'PKR ' . number_format($Order[0]->service_charge, 2) : 'Free' ?></span> </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("grand_total") ?>: </div>
                                                    <div class="col-md-7 value"> PKR <?= number_format($Order[0]->total_price + $Order[0]->service_charge, 2) ?> </div>
                                                </div>


                                                <?php if (!empty($Order[0]->special_request)) { ?>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> Special requests: </div>
                                                        <div class="col-md-7 value"> <?= $Order[0]->special_request ?> </div>
                                                    </div>
                                                <?php } ?>

                                                <br>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-md-12 col-sm-12" >
                                        <div class="portlet blue-hoki box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fas fa-user"></i><?php echo $this->lang->line("customer_information") ?> </div>

                                            </div>
                                            <div class="portlet-body">
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("customer_name") ?>: </div>
                                                    <div class="col-md-7 value"> <?= $UserArray[0]->name ?></div>
                                                </div>
                                                <!--                                                <div class="row static-info">
                                                                                                    <div class="col-md-5 name"> <php echo $this->lang->line("email") ?>: </div>
                                                                                                    <div class="col-md-7 value" style="user-select: all;"> <?= $UserArray[0]->email ?> </div>
                                                                                                </div>
                                                                                                <div class="row static-info">
                                                                                                    <div class="col-md-5 name"> <php echo $this->lang->line("phone_number") ?>: </div>
                                                                                                    <div class="col-md-7 value"> <a href="tel:<?= $UserArray[0]->mobile_code . $UserArray[0]->mobile_number ?>"><?= $UserArray[0]->mobile_code . $UserArray[0]->mobile_number ?></a> </div>
                                                                                                </div>
                                                -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                if (!empty($AddressArray)) {
                                    ?>

                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="portlet blue-hoki box">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="far fa-address-card"></i><?php echo $this->lang->line("delivery_address") ?>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("building_name_no") ?>: </div>
                                                        <div class="col-md-7 value"> <?= $AddressArray->building ?> </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("street") ?>: </div>
                                                        <div class="col-md-7 value"> <?= $AddressArray->street ?> </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("building_type") ?>: </div>
                                                        <div class="col-md-7 value"> 
                                                            <?php
                                                            if ($AddressArray->address_label == 1) {
                                                                $type = $this->lang->line("apartment");
                                                            } elseif ($AddressArray->address_label == 2) {
                                                                $type = $this->lang->line("house");
                                                            } else {
                                                                $type = $this->lang->line("office");
                                                            }
                                                            ?>
                                                            <?= $type ?></div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name"> <?php echo $this->lang->line("address") ?>: </div>
                                                        <div class="col-md-7 value"> <?= $AddressArray->address ?>, Mob -<?= $AddressArray->mobile_code . $AddressArray->mobile_number ?>   </div>
                                                    </div>
                                                    <?php
                                                    if ($AddressArray->additional_direction != 'undefined') {
                                                        ?>

                                                        <div class="row static-info">
                                                            <div class="col-md-5 name"> <?php echo $this->lang->line("additional_directions") ?>: </div>
                                                            <div class="col-md-7 value"> <?= $AddressArray->additional_direction ?> </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <?php
                                }
                                if ($Order[0]->order_status != 0) {
                                    if ($Order[0]->order_status != 1) {
                                        ?>
                                        <?php
                                        if ($Order[0]->deliveryboy_id != 0) {
                                            ?>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="portlet blue-hoki box">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                <i class="far fa-user"></i><?php echo $this->lang->line("delivery_boy") ?>
                                                            </div>
                                                        </div>



                                                        <div class="portlet-body">

                                                            <div class="row static-info">
                                                                <div class="col-md-5 name"> <?php echo $this->lang->line("delivery_boy") ?>: </div>
                                                                <div class="col-md-7 value"> 
                                                                    <input type="hidden" value="<?= $orderid ?>" id="orderId">
                                                                    <?php
                                                                    $User_Array = GetUserDetails($Order[0]->deliveryboy_id);

                                                                    if (!empty($User_Array)) {
                                                                        if ($User_Array[0]->deliveryboy_added_by == $Session->id) {
                                                                            $listArray = GetdeliveryboyByVendor($Session->id);
                                                                            if (count($listArray) > 0) {
                                                                                ?>
                                                                                <select class="form-control" id="deliveryboy_id" orderId="<?= $orderid ?>">
                                                                                    <option value="">select</option>
                                                                                    <?php
                                                                                    $selectedprov = !empty($Order[0]->deliveryboy_id) ? $Order[0]->deliveryboy_id : '';
                                                                                    for ($i = 0; $i < count($listArray); $i++) {
                                                                                        $dmk = $listArray[$i];
                                                                                        ?>  <option <?= $selectedprov == $dmk->id ? 'selected' : '' ?> value="<?= $dmk->id ?>"><?= $dmk->name ?></option><?php
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                                <?php
                                                                            }
                                                                        } else {
                                                                            $User_Array = GetUserDetails($Order[0]->deliveryboy_id);
                                                                            if (!empty($User_Array)) {
                                                                                echo "Delivery Boy '" . $User_Array[0]->name . "' assigned by Admin";
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $User_Array = GetUserDetails($Order[0]->deliveryboy_id);
                                                                        if (!empty($User_Array)) {
                                                                            echo "Delivery Boy '" . $User_Array[0]->name . "' assigned by Admin";
                                                                        }
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                    }
                                }
                                ?>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="portlet grey-cascade box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fas fa-shopping-cart"></i><?php echo $this->lang->line("order_details") ?>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th> <?php echo $this->lang->line("sl_no") ?> </th>
                                                                <th> <?php echo $this->lang->line("item_name") ?> </th>
                                                                <!--<th> Image </th>-->
                                                                <th> <?php echo $this->lang->line("price") ?> </th>
                                                                <!--<th> Size </th>-->
                                                                <th> <?php echo $this->lang->line("quantity") ?> </th>
                                                                <th> <?php echo $this->lang->line("total") ?> </th>
                                                                <!--<th> Action </th>-->
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            for ($i = 0; $i < count($op); $i++) {
                                                                $d = $op[$i];
                                                                ?>
                                                                <tr>
                                                                    <td><?= $i + 1; ?></td>
                                                                    <td>
                                                                        <?= ucwords($d->product_name) ?>
                                                                        <br>
                                                                        <?php
                                                                        if (isset($d->size) && !empty($d->size)) {
                                                                            echo 'Size';
                                                                            $size = explode(',', $d->size);
                                                                            echo '<ul class="fa-ul">';
                                                                            foreach ($size as $ad) {

                                                                                $Qry = "SELECT * FROM `menu_sizes` WHERE menu_size_id=$ad";
                                                                                $Array = $this->Database->select_qry_array($Qry);
                                                                                echo '<li class="fa fa-check">';
                                                                                echo $Array[0]->menu_size;
                                                                                echo '</li>';
                                                                            }
                                                                            echo '</ul>';
                                                                            echo '<br>';
                                                                        }
                                                                        if (isset($d->add_on) && !empty($d->add_on)) {
                                                                            echo 'Addon';
                                                                            $addon = explode(',', $d->add_on);
                                                                            echo '<ul class="fa-ul">';
                                                                            foreach ($addon as $ad) {

                                                                                $Qry = "SELECT * FROM `menu_addons` WHERE menu_addon_id=$ad";
                                                                                $Array = $this->Database->select_qry_array($Qry);
                                                                                echo '<li class="fa fa-check">';
                                                                                echo $Array[0]->add_on;
                                                                                echo '</li>';
                                                                            }
                                                                            echo '</ul>';
                                                                            echo '<br>';
                                                                        }
                                                                        if (isset($d->topping) && !empty($d->topping)) {
                                                                            echo 'Toppings';
                                                                            $topping = explode(',', $d->topping);
                                                                            echo '<ul class="fa-ul">';
                                                                            foreach ($topping as $ad) {

                                                                                $Qry = "SELECT * FROM `menu_topping` WHERE menu_topping_id=$ad";
                                                                                $Array = $this->Database->select_qry_array($Qry);
                                                                                echo '<li class="fa fa-check">';
                                                                                echo $Array[0]->topping;
                                                                                echo '</li>';
                                                                            }
                                                                            echo '</ul>';
                                                                            echo '<br>';
                                                                        }
                                                                        if (isset($d->drink) && !empty($d->drink)) {
                                                                            echo 'Drinks';
                                                                            $drink = explode(',', $d->drink);
                                                                            echo '<ul class="fa-ul">';
                                                                            foreach ($drink as $ad) {

                                                                                $Qry = "SELECT * FROM `menu_drink` WHERE menu_drink_id=$ad";
                                                                                $Array = $this->Database->select_qry_array($Qry);
                                                                                echo '<li class="fa fa-check">';
                                                                                echo $Array[0]->drink_name;
                                                                                echo '</li>';
                                                                            }
                                                                            echo '</ul>';
                                                                            echo '<br>';
                                                                        }
                                                                        if (isset($d->dip) && !empty($d->dip)) {
                                                                            echo 'Dip';
                                                                            $dip = explode(',', $d->dip);
                                                                            echo '<ul class="fa-ul">';
                                                                            foreach ($dip as $ad) {

                                                                                $Qry = "SELECT * FROM `menu_dips` WHERE menu_dip_id=$ad";
                                                                                $Array = $this->Database->select_qry_array($Qry);
                                                                                echo '<li class="fa fa-check">';
                                                                                echo $Array[0]->dips;
                                                                                echo '</li>';
                                                                            }
                                                                            echo '</ul>';
                                                                            echo '<br>';
                                                                        }
                                                                        if (isset($d->side) && !empty($d->side)) {
                                                                            echo 'Side';
                                                                            $side = explode(',', $d->side);
                                                                            echo '<ul class="fa-ul">';
                                                                            foreach ($side as $ad) {

                                                                                $Qry = "SELECT * FROM `menu_side` WHERE menu_side_id=$ad";
                                                                                $Array = $this->Database->select_qry_array($Qry);
                                                                                echo '<li class="fa fa-check">';
                                                                                echo $Array[0]->side_dish;
                                                                                echo '</li>';
                                                                            }
                                                                            echo '</ul>';
                                                                            echo '<br>';
                                                                        }
                                                                        ?>
                                                                    </td>

                                                                    <td><?= number_format($d->price, 2) ?></td>
                                                                    <!--<td><?= $d->product_size ?></td>-->
                                                                    <td><?= $d->quantity ?></td>
                                                                    <td><?= DecimalAmount($d->price * $d->quantity, 2) ?></td>
                                                                      <!--<td><?= $d->product_price * $d->product_quantity ?></td>-->

                                                                                                                                                                                                    <!--<td>-->
                                                                    <?php
                                                                    // if(empty($d->status)) { 
                                                                    ?>
                                                                                                                                                                                                    <!--<a OrderProductId="<?= $d->id ?>" href="javascript:void(0)"  title="User dashboard" class="RemoveOrderProducts">-->
                                                                                                                                                                                                    <!--<span class="label label-sm label-danger"><i class="far fa-trash-alt"></i></span>-->
                                                                    <!--</a>-->
                                                                    <?php
                                                                    // } else { 
                                                                    ?>
                                                                  <!--<a OrderProductId="<?= $d->id ?>" href="javascript:void(0)"  title="User dashboard" class="">-->
                                                                  <!--  <span class="label label-sm label-danger">Cancelled </span>-->
                                                                    <!--  </a>-->

                                                                    <?php
                                                                    // }
                                                                    ?>
                                                                    <!--</td>-->

                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                    <div class="col-sm-12">
                                                        <style>
                                                            .root {
                                                                padding: 1rem;
                                                                border-radius: 0;
                                                                box-shadow: 0 0 4px rgba(0, 0, 0, 0.12);
                                                                margin-bottom: 10px;
                                                            }


                                                            .order-track {
                                                                margin-top: 2rem;
                                                                padding: 0 1rem;
                                                                border-top: 1px dashed #2c3e50;
                                                                padding-top: 2.5rem;
                                                                display: flex;
                                                                flex-direction: column;
                                                            }
                                                            .order-track-step {
                                                                display: flex;
                                                                height: 7rem;
                                                            }
                                                            .order-track-step:last-child {
                                                                overflow: hidden;
                                                                height: 4rem;
                                                            }
                                                            .order-track-step:last-child .order-track-status span:last-of-type {
                                                                display: none;
                                                            }
                                                            .order-track-status {
                                                                margin-right: 1.5rem;
                                                                position: relative;
                                                            }
                                                            .order-track-status-dot {
                                                                display: block;
                                                                width: 2.2rem;
                                                                height: 2.2rem;
                                                                border-radius: 50% !important;
                                                                background: #95A5A6;
                                                            }
                                                            .order-track-status-line {
                                                                display: block;
                                                                margin: 0 auto;
                                                                width: 2px;
                                                                height: 7rem;
                                                                background:#822024;
                                                            }
                                                            .order-track-text-stat {
                                                                font-size: 1.3rem;
                                                                font-weight: 500;
                                                                margin-bottom: 3px;
                                                                margin-top: 0px;
                                                            }
                                                            .order-track-text-sub {
                                                                font-size: 1rem;
                                                                font-weight: 300;
                                                            }

                                                            .order-track {
                                                                transition: all .3s height 0.3s;
                                                                transform-origin: top center;
                                                            }
                                                        </style>
                                                        <?php
                                                        if (count($his) > 0) {
                                                            ?>
                                                            <section class="root">

                                                                <div class="order-track">
                                                                    <?php
                                                                    foreach ($his as $item) {
                                                                        ?>
                                                                        <div class="order-track-step">
                                                                            <div class="order-track-status">
                                                                                <span class="order-track-status-dot"></span>
                                                                                <span class="order-track-status-line"></span>
                                                                            </div>
                                                                            <div class="order-track-text">
                                                                                <p class="order-track-text-stat"><?= $item->status_name ?></p>
                                                                                <span class="order-track-text-sub"><?= date('d F,Y h:i A', strtotime($item->timestamp)) ?> </span>
                                                                            </div>
                                                                        </div>
                                                                    <?php } ?>

                                                                </div>
                                                            </section>  
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- End: life time stats -->
            </div>
        </div>


    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
