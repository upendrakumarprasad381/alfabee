<?php
$order_details=GetOrderDetailsByOrderId($orderid);

$order=GetOrderById($orderid);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="<?=base_url('vendor_dashboard')?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="<?=base_url('vendor/orders')?>">Orders</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Order Details</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <a href="<?=base_url('Admin/Orders')?>"><span class="caption-subject bold">Order Details-</span><span class="subtitle-ord"><b><?=$order[0]->orderno?></b></span></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                                                <thead>
                                                    <tr>
                                                        <th>Sl</th>
                                                        <th>Item</th>
                                                        
                                                        <th>Price</th>
                                                        <th>Quantity</th>
                                                        <th>Subtotal</th>
                                                        <th>Choices</th>
                                                        <th>Change Order Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                    if(!empty($order_details)){
                                                        for($i=0;$i<count($order_details);$i++){
                                                        $d=$order_details[$i];
                                                    ?>
                                                    <tr>
                                                        <td><?=$i+1?></td>
                                                        <td>
                                                            <?php
                                                            echo $d->product_name;
                                                            ?>
                                                        </td>
                                                        
                                                        <td><?=$d->price?></td>
                                                        <td><?=$d->quantity?></td>
                                                        <td><?=$d->subtotal?></td>
                                                        
                                                        <td>
                                                            <?php
                                                            if(isset($d->size) && !empty($d->size))
                                                            {
                                                                echo 'Size';
                                                                $size = explode(',',$d->size);
                                                                echo '<ul>';
                                                                foreach($size as $ad)
                                                                {
                                                                    
                                                                    $Qry = "SELECT * FROM `menu_sizes` WHERE menu_size_id=$ad";
                                                                    $Array = $this->Database->select_qry_array($Qry);
                                                                    echo '<li>';
                                                                    echo $Array[0]->menu_size;
                                                                    echo '</li>';
                                                                }
                                                                echo '</ul>';
                                                            }
                                                            if(isset($d->add_on) && !empty($d->add_on))
                                                            {
                                                                echo 'Addon';
                                                                $addon = explode(',',$d->add_on);
                                                                echo '<ul>';
                                                                foreach($addon as $ad)
                                                                {
                                                                    
                                                                    $Qry = "SELECT * FROM `menu_addons` WHERE menu_addon_id=$ad";
                                                                    $Array = $this->Database->select_qry_array($Qry);
                                                                    echo '<li>';
                                                                    echo $Array[0]->add_on;
                                                                    echo '</li>';
                                                                }
                                                                echo '</ul>';
                                                            }
                                                            if(isset($d->topping) && !empty($d->topping))
                                                            {
                                                                echo 'Toppings';
                                                                $topping = explode(',',$d->topping);
                                                                echo '<ul>';
                                                                foreach($topping as $ad)
                                                                {
                                                                    
                                                                    $Qry = "SELECT * FROM `menu_topping` WHERE menu_topping_id=$ad";
                                                                    $Array = $this->Database->select_qry_array($Qry);
                                                                    echo '<li>';
                                                                    echo $Array[0]->topping;
                                                                    echo '</li>';
                                                                }
                                                                echo '</ul>';
                                                            }
                                                            if(isset($d->drink) && !empty($d->drink))
                                                            {
                                                                echo 'Drinks';
                                                                $drink = explode(',',$d->drink);
                                                                echo '<ul>';
                                                                foreach($drink as $ad)
                                                                {
                                                                    
                                                                    $Qry = "SELECT * FROM `menu_drink` WHERE menu_drink_id=$ad";
                                                                    $Array = $this->Database->select_qry_array($Qry);
                                                                    echo '<li>';
                                                                    echo $Array[0]->drink_name;
                                                                    echo '</li>';
                                                                }
                                                                echo '</ul>';
                                                            }
                                                            if(isset($d->dip) && !empty($d->dip))
                                                            {
                                                                echo 'Dip';
                                                                $dip = explode(',',$d->dip);
                                                                echo '<ul>';
                                                                foreach($dip as $ad)
                                                                {
                                                                    
                                                                    $Qry = "SELECT * FROM `menu_dips` WHERE menu_dip_id=$ad";
                                                                    $Array = $this->Database->select_qry_array($Qry);
                                                                    echo '<li>';
                                                                    echo $Array[0]->dips;
                                                                    echo '</li>';
                                                                }
                                                                echo '</ul>';
                                                            }
                                                            if(isset($d->side) && !empty($d->side))
                                                            {
                                                                echo 'Side';
                                                                $side = explode(',',$d->side);
                                                                echo '<ul>';
                                                                foreach($side as $ad)
                                                                {
                                                                    
                                                                    $Qry = "SELECT * FROM `menu_side` WHERE menu_side_id=$ad";
                                                                    $Array = $this->Database->select_qry_array($Qry);
                                                                    echo '<li>';
                                                                    echo $Array[0]->side_dish;
                                                                    echo '</li>';
                                                                }
                                                                echo '</ul>';
                                                            }
                                                            ?>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <?php } } ?>   
                            </tbody>
                        </table>
                    </div>
                 </div>
                <!-- END EXAMPLE TABLE PORTLET-->
             </div>
        </div>
     </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->