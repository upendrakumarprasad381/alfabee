<style>
    .table-bordered td, .table-bordered th {
    text-align: center;
}
</style>
<?php
$order_details=GetOrderDetailsByOrderId($orderid);

$order=GetOrderById($orderid);
$user_details = GetUserDetails($order[0]->user_id);

?>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">


            <input type="hidden" id="order_id" value="<?= $orderid ?>">
            <div class="col-sm-12">
                <table class="table table-bordered table-hover">
                    <tr class="heading">
                        <th class="heading" colspan="4">Order details - <?= $order[0]->orderno ?> </th>
                    </tr>
                    <tr>
                        <th >Order No : </th>
                        <td ><?= $order[0]->orderno ?></td>
                        <th  >Date : </th>
                        <td  ><?= date('d/m/Y h:i:A', strtotime($order[0]->date)) ?></td>
                    </tr>
                    <tr>

                        <th  >Price : </th>
                        <td  ><?= number_format($order[0]->total_price,2) ?></td>
                        <th >Payment Type : </th>
                        <td ><?php
                        if($order[0]->mode_of_payment==3)
                        {
                            echo 'Cash on delivery';
                        }elseif($order[0]->mode_of_payment==1){
                            echo 'Credit Card';
                        }else{
                            echo '';
                        }
                        ?></td>
                    </tr>
                   
                    <tr>

                        <th  >&nbsp;</th>
                        <td  ></td>
                        <th  >&nbsp;</th>
                        <td  ></td>
                    </tr>
                    <tr>
                        <th colspan="4">Customer Details </th>
                    </tr> 
                    <tr>
                        <th >Name : </th>
                        <td ><?=GetNameById($order[0]->user_id,'users','name')?></td>
                        <th  >Email : </th>
                        <td  ><?= $user_details[0]->email ?></td>
                    </tr>
                    <tr>
                        <th >Phone : </th>
                        <td ><?= $user_details[0]->mobile_number ?></td>
                        <th  ></th>
                        <td  ></td>
                    </tr>
                    <tr>
                        <th colspan="4">Order Items </th>
                    </tr>
                    <tr>
                        <th >Item Name </th>
                        <th >Total </th>
                        <th  colspan="2">Choices</th>
                    </tr>
                    <?php
                    if(!empty($order_details)){
                    for($i=0;$i<count($order_details);$i++){
                    $d = $order_details[$i];
                    
                        ?>
                        <tr>
                            <td ><?= $d->product_name ?></td>
                            <td ><?= number_format($d->price,2) ?></td>
                            <td colspan="2">
                                 <?php
                                    if(isset($d->size) && !empty($d->size))
                                    {
                                        echo 'Size';
                                        $size = explode(',',$d->size);
                                        echo '<ul class="fa-ul">';
                                        foreach($size as $ad)
                                        {
                                            
                                            $Qry = "SELECT * FROM `menu_sizes` WHERE menu_size_id=$ad";
                                            $Array = $this->Database->select_qry_array($Qry);
                                            echo '<li class="fa fa-check">';
                                            echo $Array[0]->menu_size;
                                            echo '</li>';
                                        }
                                        echo '</ul>';
                                        echo '<br>';
                                    }
                                    if(isset($d->add_on) && !empty($d->add_on))
                                    {
                                        echo 'Addon';
                                        $addon = explode(',',$d->add_on);
                                        echo '<ul class="fa-ul">';
                                        foreach($addon as $ad)
                                        {
                                            
                                            $Qry = "SELECT * FROM `menu_addons` WHERE menu_addon_id=$ad";
                                            $Array = $this->Database->select_qry_array($Qry);
                                            echo '<li class="fa fa-check">';
                                            echo $Array[0]->add_on;
                                            echo '</li>';
                                        }
                                        echo '</ul>';
                                        echo '<br>';
                                    }
                                    if(isset($d->topping) && !empty($d->topping))
                                    {
                                        echo 'Toppings';
                                        $topping = explode(',',$d->topping);
                                        echo '<ul class="fa-ul">';
                                        foreach($topping as $ad)
                                        {
                                            
                                            $Qry = "SELECT * FROM `menu_topping` WHERE menu_topping_id=$ad";
                                            $Array = $this->Database->select_qry_array($Qry);
                                            echo '<li class="fa fa-check">';
                                            echo $Array[0]->topping;
                                            echo '</li>';
                                        }
                                        echo '</ul>';
                                        echo '<br>';
                                    }
                                    if(isset($d->drink) && !empty($d->drink))
                                    {
                                        echo 'Drinks';
                                        $drink = explode(',',$d->drink);
                                        echo '<ul class="fa-ul">';
                                        foreach($drink as $ad)
                                        {
                                            
                                            $Qry = "SELECT * FROM `menu_drink` WHERE menu_drink_id=$ad";
                                            $Array = $this->Database->select_qry_array($Qry);
                                            echo '<li class="fa fa-check">';
                                            echo $Array[0]->drink_name;
                                            echo '</li>';
                                        }
                                        echo '</ul>';
                                        echo '<br>';
                                    }
                                    if(isset($d->dip) && !empty($d->dip))
                                    {
                                        echo 'Dip';
                                        $dip = explode(',',$d->dip);
                                        echo '<ul class="fa-ul">';
                                        foreach($dip as $ad)
                                        {
                                            
                                            $Qry = "SELECT * FROM `menu_dips` WHERE menu_dip_id=$ad";
                                            $Array = $this->Database->select_qry_array($Qry);
                                            echo '<li class="fa fa-check">';
                                            echo $Array[0]->dips;
                                            echo '</li>';
                                        }
                                        echo '</ul>';
                                        echo '<br>';
                                    }
                                    if(isset($d->side) && !empty($d->side))
                                    {
                                        echo 'Side';
                                        $side = explode(',',$d->side);
                                        echo '<ul class="fa-ul">';
                                        foreach($side as $ad)
                                        {
                                            
                                            $Qry = "SELECT * FROM `menu_side` WHERE menu_side_id=$ad";
                                            $Array = $this->Database->select_qry_array($Qry);
                                            echo '<li class="fa fa-check">';
                                            echo $Array[0]->side_dish;
                                            echo '</li>';
                                        }
                                        echo '</ul>';
                                        echo '<br>';
                                    }
                                    ?>
                            </td>
                        </tr>
                       <?php } } ?>  
                       <?php
                       if($order[0]->order_status!=5)
                       {
                        ?>
                       
                       <tr>
                        <th colspan="4">Change Status </th>
                        </tr>
                        <?php
                       }
                       ?>
                       <?php if($order[0]->order_status==0)
                       {
                        ?>
                       
                       <tr>
                           <td>Change Status To</td>
                           <td colspan="2">
                               <div class="input-holder">
                                <div class="custom-radio">
                                    <input type="checkbox" class="filled-in manage-products-checkbox" name="order_status" id="order_status" value="<?php echo ORDER_RECEIVED ?>">
                                    <label for="id04" class="strong-label post-as">Order Received</label>
                                </div>
                            </div>
                           </td>
                           <td><button type="submit" id="change_status" class="add-bttn bg-color">Change Status</button></td>
                       </tr>
                       <?php
                       }else{
                           
                      
                       ?>
                       <tr>
                           <?php
                         
                           ?>
                            <?php
                                //$valid_status_to_changes = [ORDER_RECEIVED , ORDER_CONFIRMED , ASSIGNED_TO_DELIVERYBOY, ORDER_ON_THE_WAY, ORDER_DELIVERED];
                            if ( $order[0]->order_status == ORDER_RECEIVED) {
                            ?>
                           <td>Change Status To</td>
                           <td colspan="2">
                            <div class="input-holder">
                                <div class="custom-radio">
                                    <input type="checkbox" id="order_status" class="filled-in manage-products-checkbox" name="order_status" value="<?php echo ORDER_CONFIRMED ?>">
                                    <label for="id04" class="strong-label post-as">Order Confirmed</label>
                                </div>
                            </div>
                            </td>
                            <td><button type="submit" id="change_status" class="add-bttn bg-color">Change Status</button></td>
                            <?php
                            }elseif($order[0]->order_status == ORDER_CONFIRMED)
                            {
                            ?>
                            <td>Change Status To</td>
                            <td colspan="2">
                            <div class="input-holder">
                                <div class="custom-radio">
                                    <input type="checkbox" id="order_status" class="filled-in manage-products-checkbox" name="order_status" value="<?php echo ASSIGNED_TO_DELIVERYBOY ?>">
                                    <label for="id04" class="strong-label post-as">Assigned to delivery boy</label>
                                </div>
                            </div>
                            </td>
                            <td><button type="submit" id="change_status" class="add-bttn bg-color">Change Status</button></td>
                            <?php
                            }elseif($order[0]->order_status == ASSIGNED_TO_DELIVERYBOY)
                            {
                            ?>
                            <td>Change Status To</td>
                            <td colspan="2">
                            <div class="input-holder">
                                <div class="custom-radio">
                                    <input type="checkbox" id="order_status" class="filled-in manage-products-checkbox" name="order_status" value="<?php echo ORDER_ON_THE_WAY ?>">
                                    <label for="id04" class="strong-label post-as">Order on the way</label>
                                </div>
                            </div>
                            </td>
                            <td><button type="submit" id="change_status" class="add-bttn bg-color">Change Status</button></td>
                            <?php
                            }elseif($order[0]->order_status == ORDER_ON_THE_WAY){
                            ?>
                            <td>Change Status To</td>
                            <td colspan="2">
                            <div class="input-holder">
                                <div class="custom-radio">
                                    <input type="checkbox" id="order_status" class="filled-in manage-products-checkbox" name="order_status" value="<?php echo ORDER_DELIVERED ?>">
                                    <label for="id04" class="strong-label post-as">Order delivered</label>
                                </div>
                            </div>
                            </td>
                            <td><button type="submit" id="change_status" class="add-bttn bg-color">Change Status</button></td>
                            <?php
                            }
                            ?>
                           </td>
                       </tr>
                       <?php
                       }
                       ?>
                </table>
                
                 <div class="col-sm-12">
                <style>
                    .root {
                        padding: 1rem;
                        border-radius: 0;
                        box-shadow: 0 0 4px rgba(0, 0, 0, 0.12);
                        margin-bottom: 10px;
                    }
                   

                    .order-track {
                        margin-top: 2rem;
                        padding: 0 1rem;
                        border-top: 1px dashed #2c3e50;
                        padding-top: 2.5rem;
                        display: flex;
                        flex-direction: column;
                    }
                    .order-track-step {
                        display: flex;
                        height: 7rem;
                    }
                    .order-track-step:last-child {
                        overflow: hidden;
                        height: 4rem;
                    }
                    .order-track-step:last-child .order-track-status span:last-of-type {
                        display: none;
                    }
                    .order-track-status {
                        margin-right: 1.5rem;
                        position: relative;
                    }
                    .order-track-status-dot {
                        display: block;
                        width: 2.2rem;
                        height: 2.2rem;
                        border-radius: 50% !important;
                        background: #240772;
                    }
                    .order-track-status-line {
                        display: block;
                        margin: 0 auto;
                        width: 2px;
                        height: 7rem;
                        background:#240772;
                    }
                    .order-track-text-stat {
                        font-size: 1.3rem;
                        font-weight: 500;
                        margin-bottom: 3px;
                        margin-top: 0px;
                    }
                    .order-track-text-sub {
                        font-size: 1rem;
                        font-weight: 300;
                    }

                    .order-track {
                        transition: all .3s height 0.3s;
                        transform-origin: top center;
                    }
                </style>
                <?php
                $his = GetorderstatushistoryBy($orderid);
                if(count($his)>0)
                {
                ?>
                <section class="root">
                    
                    <div class="order-track">
                        <?php

                        foreach($his as $item){
                            if($item->status_id=='1'){
                                $status = 'Order Received';
                            }
                            else if($item->status_id=='2'){
                                $status = 'Order Confirmed';
                            }
                            else if($item->status_id=='3'){
                                $status = 'Assigned to deliveryboy';
                            }
                            else if($item->status_id=='4'){
                                $status = 'Order on the way';
                            }
                            else if($item->status_id=='5'){
                                $status = 'Order Delivered';
                            }
                           
                            ?>
                            <div class="order-track-step">
                                <div class="order-track-status">
                                    <span class="order-track-status-dot"></span>
                                    <span class="order-track-status-line"></span>
                                </div>
                                <div class="order-track-text">
                                    <p class="order-track-text-stat"><?= $status ?></p>
                                    <span class="order-track-text-sub"><?= date('d F,Y h:i A',  strtotime($item->timestamp)) ?> </span>
                                </div>
                            </div>
                        <?php } ?>
                       
                    </div>
                </section>  
                <?php
                }
                ?>
            </div>
            </div>
        </div>
    </div>
</div>