<?php
$Session = GetSessionArrayVendor();

$orders = GetOrdersByVendorId($Session->id);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?php echo $this->lang->line("orders") ?></span>

                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no") ?></th>
                                    <th><?php echo $this->lang->line("order_no") ?></th>
                                    <!--<th><php echo $this->lang->line("customer_name") ?></th>-->
                                    <th>Order <?php echo $this->lang->line("price") ?> In PKR</th>
                                    <!--<th>Payment Status</th>-->
                                    <th><?php echo $this->lang->line("status") ?></th>
                                    <th><?php echo $this->lang->line("order_date") ?></th>
                                    <!--<th>Delivery Time</th>-->
                                    <!--<th>Invoice</th>-->
                                    <th><?php echo $this->lang->line("action") ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($orders); $i++) {
                                    $d = $orders[$i];
                                    ?>
                                    <tr>
                                <input type="hidden" id="order_time" value="<?= !empty($d->date) ? $d->date : '' ?>">
                                <td><?= $i + 1; ?></td>

                                <td><?= $d->orderno ?></td>
                                <!--<td><= GetNameById($d->user_id, 'users', 'name') ?></td>-->
                                <td><?= number_format($d->total_price + $d->service_charge, 2) ?></td>

         <!--<td> -->
                                <?php
                                // if($d->status=="1"){
                                //  echo ' <span class="label label-sm label-success"> Paid </span>';  
                                // }else if($d->status=="2"){
                                //     echo ' <span class="label label-sm label-danger"> Cancelled </span>'; 
                                // }else{
                                //  echo '<span class="label label-sm label-warning"> Pending </span>';  
                                // }
                                ?>
                                <!--</td>-->
                                <!--<td>-->
                                <?php
                                // if($d->order_status=="1"){
                                //     echo 'Order Confirmed';
                                // }elseif($d->order_status=="2"){
                                //     echo 'Assign to deliver';
                                // }
                                // elseif($d->order_status=="3"){
                                //     echo 'Order on the way';
                                // }elseif($d->order_status=="4"){
                                //     echo 'Order delivered';
                                // }elseif($d->order_status=="5"){
                                //     echo 'Order Cancelled by user';
                                // }
                                // elseif($d->order_status=="6"){
                                //     echo 'Order Cancelled';
                                // }
                                // elseif($d->order_status=="4"){
                                //     echo 'Order on the way';
                                // }elseif($d->order_status=="5"){
                                //     echo 'Order delivered';
                                // }
                                // $datetime1 = strtotime("now");
                                // $datetime2 = strtotime($d->date);
                                // $interval  = abs($datetime2 - $datetime1);
                                // $minutes   = round($interval / 60);
                                // if($minutes<3 & $d->order_status!=5){
                                ?>
                                    <!--Time Left : <span id="timer"></span>-->
                                <?php
                                // }
                                ?>
                                <!--</td>-->
                                <td>
                                    <span class="label label-sm label-success" style="    background-color: <?= $d->status_color ?>;"><?= !empty($d->status_name) ? $d->status_name : 'Received' ?></span>
                                    <?php
                                    if ($d->order_status != '7') {
                                        $datetime1 = strtotime("now");
                                        $datetime2 = strtotime($d->date);
                                        $interval = abs($datetime2 - $datetime1);
                                        $minutes = round($interval / 60);
                                       // if ($minutes < 3 && ($d->order_status == 0 || $d->order_status != 5)) {
                                            ?>
                                    <!--</br>Time Left to cancel order by user : <span id="timeId<?= $i ?>" sl="<?= $i ?>" time="<?= date('H:i:s',  strtotime($d->date)) ?>" class="timer"><?= $minutes ?></span>-->
                                            <?php
                                       // }
                                    }
                                    ?>
                                </td>
                                <td><?= date('D, d M-Y h:i A', strtotime($d->date)) ?></td>
                               <!--<td>-->
                                <?php
                                // if($d->pre_order==1)
                                // {
                                //     echo date('d-m-Y h:i A',strtotime($d->delivery_time));
                                // }
                                ?>
                                <!--</td>-->
                                <!--<td></td>-->
                                <!--<td><?php
                                if ($d->status == "1") {
                                    echo date('d-m-Y', strtotime($d->payment_date));
                                }
                                ?></td>-->
                                <td>
                                    <a href="<?= base_url('Vendor/OrderDetails/' . base64_encode($d->id)) ?>"><span class="label label-sm label-success"><i class="fa fa-eye" aria-hidden="true" title="View"></i></span></a>
                                    <?php
                                    // if($d->order_status!="5")
                                    // {
                                    ?>
                                    <!--<a redrurl="Orders" Id="<?= $d->id ?>" title="Cancel Order" class="CancelOrder" table="orders">-->
                                    <!--      <span class="label label-sm label-danger" ><i class="fa fa-times"></i></span></a>-->
                                    <?php
                                    // }
                                    ?>
                                        <?php if ($d->invoice != "" && $d->order_status != '7') { ?>
                                                                                    <a href="<?= base_url('login/invoice?orderId='. base64_encode($d->id)) ?>" target="_blank"><i class="fas fa-print"></i></a>
                                        <a href="<?= base_url() ?>uploads/invoice/<?= $d->invoice ?>" target="_blank"><i class="fa fa-file" aria-hidden="true" title="<?php echo $this->lang->line("invoice") ?>"></i></a>
    <?php } ?>
                                </td>

                                </tr>
<?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
