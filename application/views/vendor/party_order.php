<?php
$Session = GetSessionArrayVendor();

$orders = GetPartyOrdersByVendorId($Session->id);

?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?php echo $this->lang->line("party_orders")?></span>
                            
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover commontableXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no")?></th>
                                    <th><?php echo $this->lang->line("party_order_id")?></th>
                                    <th><?php echo $this->lang->line("customer_name")?></th>
                                    <th><?php echo $this->lang->line("price")?></th>
                                    <th><?php echo $this->lang->line("order_date")?></th>
                                    <th><?php echo $this->lang->line("action")?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($orders); $i++) {
                                    $d = $orders[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td> 
                                       
                                        <td><?= $d->party_orderno ?></td>
                                        <td><?=GetNameById($d->user_id,'users','name')?></td>
                                        <td>PKR <?=number_format($d->total_price,2)?></td>
                                        <td><?=date('D, d M-Y h:i A',strtotime($d->date))?></td>
                                        <td>
                                        <a href="<?= base_url('Vendor/partyOrderDetails/' . base64_encode($d->id)) ?>"><span class="label label-sm label-success"><i class="fa fa-eye" aria-hidden="true"></i></span></a>
                                        </td>
                                        
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
