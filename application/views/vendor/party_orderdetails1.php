<style>
    .table-bordered td, .table-bordered th {
    text-align: center;
    }
    .heading th {
    background: #822024;
    color: #fff;
    
</style>
<?php

$order=GetPartyOrdersById($orderid);
$user_details = GetUserDetails($order[0]->user_id);

?>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">


            <input type="hidden" id="party_order_id" value="<?= $orderid ?>">
            <div class="col-sm-12">
                <table class="table table-bordered table-hover">
                    <tr class="heading">
                        <th class="heading" colspan="4">Party Order details - <?= $order[0]->party_orderno ?> </th>
                    </tr>
                    
                    <tr>
                        <th colspan="4" class="heading">Customer Details </th>
                    </tr> 
                    <tr>
                        <th >Name : </th>
                        <td ><?=GetNameById($order[0]->user_id,'users','name')?></td>
                        <th  >Email : </th>
                        <td  ><?= $user_details[0]->email ?></td>
                    </tr>
                    <tr>
                        <th >Phone : </th>
                        <td ><?= $user_details[0]->mobile_number ?></td>
                        <th  ></th>
                        <td  ></td>
                    </tr>
                    <tr>

                        <th >&nbsp;</th>
                        <td ></td>
                        <th>&nbsp;</th>
                        <td  ></td>
                    </tr>
                    <tr>
                        <th class="heading" colspan="4">Party details </th>
                    </tr>
                    <tr>
                        <th >Party Order No : </th>
                        <td ><?= $order[0]->party_orderno ?></td>
                        <th  >Requested Date : </th>
                        <td  ><?= date('d/m/Y h:i:A', strtotime($order[0]->inserted_on)) ?></td>
                    </tr>
                    <tr>
                        <th >Occasion: </th>
                        <td ><?=GetNameById($order[0]->occasion_id,'occasion','occasion_name')?></td>
                        <th  >Party Date : </th>
                        <td  ><?= date('d/m/Y h:i:A', strtotime($order[0]->party_date)) ?></td>
                    </tr>
                    <tr>
                        <th >No.of Adults: </th>
                        <td ><?= $order[0]->no_of_adult?></td>
                        <th  >No.of Childrens : </th>
                        <td  ><?= $order[0]->no_of_children ?></td>
                    </tr>
                     <tr>
                        <th >Party Time: </th>
                        <td ><?= date("H:i A", strtotime($order[0]->party_time));?></td>
                        <th  >Venue : </th>
                        <td  ><?= $order[0]->party_venue ?></td>
                    </tr>
                    <tr>
                        <th>Cuisine</th>
                        <td>
                            <?php
                                $cuisine = explode(',',$order[0]->cuisine_id);
                                foreach($cuisine as $cus){
                                    $Qry = "SELECT * FROM `cuisine` WHERE cuisine_id='$cus' ";
                                    $CuisineArray = $this->Database->select_qry_array($Qry);
                                    $cuisine_name[] = $CuisineArray[0]->cuisine_name;
                                }
                                echo implode(',',$cuisine_name);
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th  >&nbsp;</th>
                        <td  ></td>
                        <th  >&nbsp;</th>
                        <td  ></td>
                    </tr>
                    <tr>
                        <th colspan="4" class="heading">My Quotation</th>
                    </tr>
                    <tr>
                        <th >Occasion: </th>
                        <td ><?=GetNameById($order[0]->occasion_id,'occasion','occasion_name')?></td>
                        <td>
                            <div class="input-group col-md-8">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">PKR</button>
                                </span>
                                <input value="<?= !empty($order[0]->price) ? $order[0]->price : '' ?>" id="price" type="text" class="form-control col-md-3" autocomplete="off">
                                   
                            </div>
                        </td>
                        <td>
                            <button type="submit" name="quoteprice" id="quoteprice" class="btn btn-success">Quote Price</button>
                        </td>
                    </tr>
                   
                    <tr>

                        <th  >&nbsp;</th>
                        <td  ></td>
                        <th  >&nbsp;</th>
                        <td  ></td>
                    </tr>
                    
                     <?php
                       if($order[0]->mode_of_payment==3 && $order[0]->payment_status==2)
                       {
                        ?>
                        <tr>
                        <td>Payment Received</td>
                            <td colspan="2">
                            <div class="input-holder">
                                <div class="custom-radio">
                                    <input type="checkbox" id="payment_status" class="filled-in manage-products-checkbox" name="order_status" value="1">
                                    <label for="id04" class="strong-label post-as">Payment Received</label>
                                </div>
                            </div>
                            </td>
                            <td><button type="submit" id="party_order_status" class="btn btn-success">Payment Confirmation</button></td>
                            </tr>
                        <?php
                        
                       }
                       ?>
                   
                </table>
                
                               
            </div>
            </div>
        </div>
    </div>
</div>