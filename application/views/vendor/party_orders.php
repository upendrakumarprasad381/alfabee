<?php
$Session = GetSessionArrayVendor();

$orders = GetOrdersByVendorId($Session->id);

?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Party Orders</span>
                            
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover commontableXXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Order Date</th>
                                    <th>Order Id</th>
                                    <th>Customer Name</th>
                                    <th>Price</th>
                                    <!--<th>Payment Status</th>-->
                                    <th>Order Status</th>
                                    <th>Delivery Time</th>
                                    <th>Payment Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($orders); $i++) {
                                    $d = $orders[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?=date('d-m-Y',strtotime($d->date))?></td>
                                        <td><?= $d->orderno ?></td>
                                        <td><?=GetNameById($d->user_id,'users','name')?></td>
                                        <td>PKR <?=number_format($d->total_price,2)?></td>
                                        <!--<td> -->
                                            <?php
                                            // if($d->status=="1"){
                                            //  echo ' <span class="label label-sm label-success"> Paid </span>';  
                                            // }else if($d->status=="2"){
                                            //     echo ' <span class="label label-sm label-danger"> Cancelled </span>'; 
                                            // }else{
                                            //  echo '<span class="label label-sm label-warning"> Pending </span>';  
                                            // }
                                            ?>
                                        <!--</td>-->
                                        <td>
                                            <?php
                                            if($d->order_status=="1"){
                                                echo 'Order Received';
                                            }elseif($d->order_status=="2"){
                                                echo 'Order Received';
                                            }elseif($d->order_status=="3"){
                                                echo 'Assigned to deliveryboy';
                                            }elseif($d->order_status=="4"){
                                                echo 'Order on the way';
                                            }elseif($d->order_status=="5"){
                                                echo 'Order delivered';
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if($d->pre_order==1)
                                            {
                                                echo date('d-m-Y h:i A',strtotime($d->delivery_time));
                                            }
                                            ?>
                                        </td>
                                        <td><?php if($d->status=="1"){echo date('d-m-Y',strtotime($d->payment_date));}?></td>
                                        <td>
                                        <a href="<?= base_url('Vendor/OrderDetails/' . base64_encode($d->id)) ?>"><span class="label label-sm label-success"><i class="fa fa-eye" aria-hidden="true"></i></span></a>
                                        <!--<a redrurl="Orders" Id="<?= $d->id ?>" title="<?=$d->orderno?>" class="CancelOrder" table="orders">-->
                                        <!--      <span class="label label-sm label-danger" ><i class="fa fa-times"></i></span></a>-->
                                        </td>
                                        
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
