<?php

$session_arr = $this->session->userdata('Vendor');

$Qry = "SELECT * FROM `book_table` LEFT JOIN restaurant_details ON restaurant_details.vendor_id = book_table.vendor_id LEFT JOIN users on users.id=book_table.user_id WHERE book_table.vendor_id='$session_arr->id'";
$Array = $this->Database->select_qry_array($Qry);

?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Reservations</span>
                            
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover commontableXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no") ?></th>
                                    <th>Customer Name</th>
                                    <th>Customer Email</th>
                                    <th>Customer Mobile</th>
                                    <th>No.of People</th>
                                    <th>Booking Date</th>
                                    <th>Booking Time</th>
                                    <!--<th><?php echo $this->lang->line("status") ?></th>-->
                                    <!--<th><?php echo $this->lang->line("action") ?></th>-->
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($Array); $i++) {
                                    $d = $Array[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                    
                                        <td><?= $d->name ?></td>
                                        <td><?= $d->email ?></td>
                                        <td> <?= $d->mobile_number ?></td>
                                        <td><?= $d->no_of_people ?></td>
                                        <td><?=date('D, d M-Y',strtotime($d->reservation_date))?></td>
                                        <td><?=date('h:i A',strtotime($d->reservation_time))?></td>
                                        <!--<td><?= $d->status == 0 ? '<span class="label label-sm label-success">'.$this->lang->line("active").'</span>' : '<span class="label label-sm label-danger">'.$this->lang->line("inactive").'</span>' ?></td>-->
                                        <!--<td>-->
                                        <!--    <a redrurl="reservations" class="commonApprove" table="book_table" Id="<?= $d->id ?>"><span class="label label-sm label-success"><i class="fa fa-check" aria-hidden="true" title="Approve" ></i></span></a>-->
                                        <!--    <a redrurl="reservations" class="commonReject" table="book_table" Id="<?= $d->id ?>"><span class="label label-sm label-danger"><i class="fas fa-ban" title="Reject"></i></span></a>-->
                                        <!--</td>-->
                                        <!--<td><a title="user information" href="<?= base_url('Vendor/user_info/' . base64_encode($d->user_id)) ?>" title="User dashboard" ><span class="label label-sm label-success"><i class="fas fa-street-view" aria-hidden="true"></i></span></a></td>-->
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
