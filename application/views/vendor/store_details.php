<?php
$Session = GetSessionArrayVendor();

$ConditionArray = array('vendor_id' => $Session->id);
$Qry1 = "SELECT * FROM `restaurant_details` WHERE vendor_id=:vendor_id";
$Array1 = $this->Database->select_qry_array($Qry1, $ConditionArray);
$store = !empty($Array1) ? $Array1[0] : '';
$delivery = "SELECT * FROM `delivery_timings` WHERE vendor_id=:vendor_id";
$delivery_array = $this->Database->select_qry_array($delivery, $ConditionArray);

$Qry2 = "SELECT IFNULL(GROUP_CONCAT(restaurant_cuisine_type.cuisine_id SEPARATOR '|'), '') as cuisine_id," .
        "IFNULL(GROUP_CONCAT(cuisine.cuisine_name SEPARATOR '|'), '') as cuisine_name FROM `restaurant_cuisine_type` LEFT JOIN `cuisine` ON cuisine.cuisine_id=restaurant_cuisine_type.cuisine_id  WHERE vendor_id=:vendor_id";
$Array2 = $this->Database->select_qry_array($Qry2, $ConditionArray);
// echo '<pre>';
// print_r($Array2);die();

$category = array_combine(
        array_filter(explode("|", $Array2[0]->cuisine_id)), array_filter(explode("|", $Array2[0]->cuisine_name))
);

$Qry3 = "SELECT IFNULL(GROUP_CONCAT(cuisine.cuisine_name SEPARATOR '|'),'') as cuisine_name,GROUP_CONCAT(if(cuisine.cuisine_name_ar='',null,cuisine.cuisine_name_ar )SEPARATOR '|') as cuisine_name_ar," .
        "IFNULL(GROUP_CONCAT(cuisine.cuisine_id SEPARATOR '|'), '') as cuisine_id FROM `cuisine` WHERE status=0 AND archive=0";
$Array3 = $this->Database->select_qry_array($Qry3);

$selected_categories = explode('|', $Array3[0]->cuisine_id);
$selected_cuisine = explode('|', $Array3[0]->cuisine_name);

if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'ar') {
    if ($Array3[0]->cuisine_name_ar != '') {
        $cuisine_list = array_combine(
                array_filter(explode("|", $Array3[0]->cuisine_id)), array_filter(explode("|", $Array3[0]->cuisine_name_ar))
        );
    } else {
        $cuisine_list = array_combine(
                array_filter(explode("|", $Array3[0]->cuisine_id)), array_filter(explode("|", $Array3[0]->cuisine_name))
        );
    }
} else {
    $cuisine_list = array_combine(
            array_filter(explode("|", $Array3[0]->cuisine_id)), array_filter(explode("|", $Array3[0]->cuisine_name))
    );
}

$selected_cuisine_id = explode('|', $Array2[0]->cuisine_id);
?>
<style>
    .img-size-common {
        color: #f01818 !important;
        font-size: 70% !important;
        font-weight: 600 !important;
    }
    .heading th {
        background: #240772;
        color: #fff;
    }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="<?= base_url('Vendor') ?>"><?php echo $this->lang->line("home") ?></a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Update Business Details</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold">Update Business Details</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered" enctype="multipart/form-data">
                            <input type="hidden" id="UserId" value="<?= $Session->id ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("restaurant_name") ?> <?php echo $this->lang->line("in_english") ?></label>
                                    <div class="col-md-6">
                                        <input id="restaurant_name" value="<?= isset($Array1[0]->restaurant_name) ? $Array1[0]->restaurant_name : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("restaurant_name") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                    <div class="col-md-6">
                                        <input id="restaurant_name_ar" value="<?= isset($Array1[0]->restaurant_name_ar) ? $Array1[0]->restaurant_name_ar : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                <?php
                                if ($Session->store_type == 1) {
                                    ?>

                                    <div class="form-group">
                                        <label class="control-label col-md-3"><?php echo $this->lang->line("cuisine_type") ?></label>
                                        <div class="col-md-6">
                                            <select class="form-control selectpicker multiselect-box" data-live-search="true" id="cuisine_id" name="rest_cuisine[]" multiple>
                                                <?php
                                                foreach ($cuisine_list as $k => $array_name) {
                                                    ?>
                                                    <option value="<?php echo $k ?>" <?php echo in_array($k, $selected_cuisine_id) ? "selected" : "" ?>><?php echo $array_name ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("opening_time") ?></label>
                                    <div class="col-md-2">
                                        <div class="input-group">
                                            <input type="text" class="form-control timepicker timepicker-no-seconds" id="opening_time" value="<?php echo isset($Array1[0]->opening_time) ? $Array1[0]->opening_time : '' ?>">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-clock-o"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    <label class="control-label col-md-2"><?php echo $this->lang->line("closing_time") ?></label>
                                    <div class="col-md-2">
                                        <div class="input-group">
                                            <input type="text" class="form-control timepicker timepicker-no-seconds" id="closing_time" value="<?php echo isset($Array1[0]->closing_time) ? $Array1[0]->closing_time : '' ?>">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-clock-o"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                               
                                <div class="form-group" style="display: <?= $Array1[0]->isDelivery == '1' ? 'block' : 'none' ?>;">
                                    <label class="control-label col-md-3">Delivery hours start time</label>
                                    <div class="col-md-2">
                                        <div class="input-group">
                                            <input type="text" class="form-control timepicker timepicker-no-seconds" id="delivery_hours_st" value="<?php echo isset($Array1[0]->delivery_hours_st) ? $Array1[0]->delivery_hours_st : '' ?>">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-clock-o"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    <label class="control-label col-md-2">End Time</label>
                                    <div class="col-md-2">
                                        <div class="input-group">
                                            <input type="text" class="form-control timepicker timepicker-no-seconds" id="delivery_hours_et" value="<?php echo isset($Array1[0]->delivery_hours_et) ? $Array1[0]->delivery_hours_et : '' ?>">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-clock-o"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("description") ?> <?php echo $this->lang->line("in_english") ?></label>
                                    <div class="col-md-6">
                                        <textarea class="form-control summernote" id="description" rows="5"><?= (!empty($Array1[0]->about) && $Array1[0]->about != 'undefined') ? $Array1[0]->about : '' ?></textarea>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("description") ?> <?php echo $this->lang->line("in_arabic") ?></label>
                                    <div class="col-md-6">
                                        <textarea class="form-control summernote" id="description_ar" rows="5"><?= (!empty($Array1[0]->about_ar) && $Array1[0]->about_ar != 'undefined') ? $Array1[0]->about_ar : '' ?></textarea>  
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("commission_to_pay") ?><br>(<?php echo $this->lang->line("in_percentage") ?>)</label>
                                    <div class="col-md-6">
                                        <?php
                                        $commission = GetCommissionByVendor($Session->id);
                                        ?>
                                        <input id="commission" value="<?= isset($commission[0]->commission) ? $commission[0]->commission : '' ?>" type="text" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo $this->lang->line("delivery") ?></label>
                                    <div class="col-md-6">

                                        <input type="checkbox" id="isDelivery" name="isDelivery" value="<?= $Array1[0]->isDelivery ?>" <?php echo (isset($Array1[0]->isDelivery) && $Array1[0]->isDelivery == 1 ? 'checked="checked"' : ''); ?> disabled>
                                        <label><?php echo $this->lang->line("yes") ?></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Delivery Radius<br>
                                        <span class="img-size-common">(in km)</span>
                                    </label>
                                    <div class="col-md-6">
                                        <input id="delivery_radius" value="<?= isset($Session->delivery_radius) ? $Session->delivery_radius : '' ?>" type="number" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group" id="is_delivery" style="display:none;">
                                    <!--<div class="form-group __latextrit" id="choice_for_yes">-->
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("delivery_time") ?>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="mt-repeater">
                                            <div data-repeater-list="group-b">
                                                <?php
                                                if (isset($delivery_array) && count($delivery_array) > 0) {
                                                    foreach ($delivery_array as $d) {

                                                        $selected_cities_id = explode(',', $d->cities);
                                                        ?>
                                                        <div data-repeater-item class="row">
                                                            <div class="col-md-3">
                                                                <label class="control-label"><?php echo $this->lang->line("delivery_from") ?></label>
                                                                <input type="text" class="form-control timepicker timepicker-no-seconds" id="delivery_timepicker_from" name="delivery_timepicker_from[]" value="<?php echo isset($d->delivery_from) ? date('H:i A', strtotime($d->delivery_from)) : '' ?>">

                                                            </div>
                                                            <div class="col-md-3">
                                                                <label class="control-label"><?php echo $this->lang->line("delivery_to") ?></label>
                                                                <input type="text" class="form-control timepicker timepicker-no-seconds" id="delivery_timepicker_to" name="delivery_timepicker_to[]" value="<?php echo isset($d->delivery_to) ? date('H:i A', strtotime($d->delivery_to)) : '' ?>">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label class="control-label"><?php echo $this->lang->line("delivery_location") ?></label>
                                                                <select class="form-control selectpicker multiselect-box" data-live-search="true" id="emirates" name="emirates[]" multiple>
                                                                    <?php
                                                                    $Qry = "SELECT IFNULL(GROUP_CONCAT(city_id SEPARATOR '|'), '') as city_id,IFNULL(GROUP_CONCAT(city_name SEPARATOR '|'), '') as city_name,IFNULL(GROUP_CONCAT(city_name_ar SEPARATOR '|'), '') as city_name_ar FROM cities";
                                                                    $Arry = $this->Database->select_qry_array($Qry);
                                                                    if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'en' || empty($this->session->userdata(ADMIN_LANGUAGE_ID))) {
                                                                        $cities_list = array_combine(
                                                                                array_filter(explode("|", $Arry[0]->city_id)), array_filter(explode("|", $Arry[0]->city_name))
                                                                        );
                                                                    } else {
                                                                        $cities_list = array_combine(
                                                                                array_filter(explode("|", $Arry[0]->city_id)), array_filter(explode("|", $Arry[0]->city_name_ar))
                                                                        );
                                                                    }

                                                                    foreach ($cities_list as $a => $arry_name) {
                                                                        ?>
                                                                        <option value="<?php echo $a ?>" <?php echo in_array($a, $selected_cities_id) ? "selected" : "" ?>><?php echo $arry_name ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>

                                                            <div class="col-md-1 close_btn">
                                                                <label class="control-label">&nbsp;</label>
                                                                <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                                    <i class="fa fa-close"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <div data-repeater-item class="row">
                                                        <div class="col-md-3">
                                                            <label class="control-label"><?php echo $this->lang->line("delivery_from") ?></label>
                                                            <input type="text" class="form-control timepicker timepicker-no-seconds" id="delivery_timepicker_from" name="delivery_timepicker_from[]" value="">

                                                        </div>
                                                        <div class="col-md-3">
                                                            <label class="control-label"><?php echo $this->lang->line("delivery_to") ?></label>
                                                            <input type="text" class="form-control timepicker timepicker-no-seconds" id="delivery_timepicker_to" name="delivery_timepicker_to[]" value="">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label class="control-label"><?php echo $this->lang->line("delivery_location") ?></label>
                                                            <select class="form-control selectpicker multiselect-box " data-live-search="true" id="emirates" name="emirates[]" multiple>
                                                                <?php
                                                                $Qry_name = "SELECT * FROM cities";
                                                                $Arry_name = $this->Database->select_qry_array($Qry_name);
                                                                foreach ($Arry_name as $arr) {
                                                                    if ($this->session->userdata(ADMIN_LANGUAGE_ID) == 'en' || empty($this->session->userdata(ADMIN_LANGUAGE_ID))) {
                                                                        $city = $arr->city_name;
                                                                    } else {
                                                                        $city = $arr->city_name_ar;
                                                                    }
                                                                    ?>
                                                                    <option value="<?= $arr->city_id ?>"><?= $city ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>        
                                                        <div class="col-md-1 close_btn">
                                                            <label class="control-label">&nbsp;</label>
                                                            <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                                <i class="fa fa-close"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <hr>
                                            <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add add_more">
                                                <i class="fa fa-plus"></i> Add More</a>
                                            <br>
                                            <br> </div>
                                    </div>
                                    <!--</div>-->
                                    <!--<div class="col-md-2">-->

                                    <!--    <div class="input-group">-->
                                    <!--        <input type="text" class="form-control timepicker timepicker-no-seconds" id="delivery_time_start" value="<?php echo isset($Array1[0]->delivery_time_start) ? $Array1[0]->delivery_time_start : '' ?>">-->
                                    <!--        <span class="input-group-btn">-->
                                    <!--            <button class="btn default" type="button">-->
                                    <!--                <i class="fa fa-clock-o"></i>-->
                                    <!--            </button>-->
                                    <!--        </span>-->
                                    <!--    </div>-->
                                    <!-- </div>  -->
                                    <!-- <label class="control-label col-md-1"><?php echo $this->lang->line("to") ?></label>-->
                                    <!-- <div class="col-md-2">-->
                                    <!--    <div class="input-group">-->
                                    <!--        <input type="text" class="form-control timepicker timepicker-no-seconds" id="delivery_time_ends" value="<?php echo isset($Array1[0]->delivery_time_ends) ? $Array1[0]->delivery_time_ends : '' ?>">-->
                                    <!--        <span class="input-group-btn">-->
                                    <!--            <button class="btn default" type="button">-->
                                    <!--                <i class="fa fa-clock-o"></i>-->
                                    <!--            </button>-->
                                    <!--        </span>-->
                                    <!--    </div>-->
                                    <!--</div>-->

                                </div>

                                <!--<div class="form-group">-->
                                <!--    <label class="col-md-3 control-label">Pre-order Facility</label>-->
                                <!--    <div class="col-md-6">-->

<!--        <input type="checkbox" id="pre_order" name="pre_order" value="<?= $Array1[0]->pre_order ?>" <?php echo (isset($Array1[0]->pre_order) && $Array1[0]->pre_order == 1 ? 'checked="checked"' : ''); ?>>-->
                                <!--        <label>Yes</label>-->
                                <!--    </div>-->
                                <!--</div>-->
                                <?php
                                if ($Session->store_type == 1) {
                                    ?>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo $this->lang->line("party_order") ?></label>
                                        <div class="col-md-6">

                                            <input type="checkbox" id="party_order" name="party_order" value="<?= $Array1[0]->party_order ?>" <?php echo (isset($Array1[0]->party_order) && $Array1[0]->party_order == 1 ? 'checked="checked"' : ''); ?>>
                                            <label><?php echo $this->lang->line("yes") ?></label>
                                        </div>
                                    </div>

                                    <div class="form-group" style="display: none;">
                                        <label class="col-md-3 control-label">Table Reservation</label>
                                        <div class="col-md-6">

                                            <input type="checkbox" id="table_booking" name="table_booking" value="<?= $Array1[0]->table_booking ?>" <?php echo (isset($Array1[0]->table_booking) && $Array1[0]->table_booking == 1 ? 'checked="checked"' : ''); ?>>
                                            <label><?php echo $this->lang->line("yes") ?></label>
                                        </div>
                                    </div>








                                    <div class="form-group table_booking_div" style="display:none;">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-sm-6">
                                            <table class="table table-bordered table-hover">
                                                <tr class="heading">
                                                    <th class="heading" colspan="4">Table Capacity Details</th>
                                                </tr>

                                                <tr>
                                                    <th width="50%" colspan="2"> Max. Table Capacity</th>
                                                    <td width="50%" colspan="2"><input id="table_capacity" value="<?= isset($Array1[0]->table_capacity) ? $Array1[0]->table_capacity : '' ?>" type="number" class="form-control"></td>
                                                </tr>
                                                <tr class="heading">
                                                    <th class="heading" colspan="4">Table Booking Timing</th>
                                                </tr>

                                                <tr>
                                                    <th width="50%" colspan="2"> From </th>
                                                    <td width="50%" colspan="2">

                                                        <div class="input-group">
                                                            <input type="text" class="form-control timepicker timepicker-no-seconds" id="table_booking_opening_time" value="<?php echo isset($Array1[0]->table_booking_opening_time) ? $Array1[0]->table_booking_opening_time : '' ?>">
                                                            <span class="input-group-btn">
                                                                <button class="btn default" type="button">
                                                                    <i class="fa fa-clock-o"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th width="50%" colspan="2"> To </th>
                                                    <td width="50%" colspan="2">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control timepicker timepicker-no-seconds" id="table_booking_closing_time" value="<?php echo isset($Array1[0]->table_booking_closing_time) ? $Array1[0]->table_booking_closing_time : '' ?>">
                                                            <span class="input-group-btn">
                                                                <button class="btn default" type="button">
                                                                    <i class="fa fa-clock-o"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--<label class="col-md-3 control-label"></label>-->
                                        <!--<div class="col-md-6">-->
                                        <!--<div class="col-md-2">-->
                                        <!--    <label class="col-md-2">Table Capacity</label>-->
                                        <!--</div>-->
                                        <!--    <div class="col-md-4">-->
                                        <!--    <input id="table_capacity" value="<?= isset($Array1[0]->table_capacity) ? $Array1[0]->table_capacity : '' ?>" type="number" class="form-control">-->

                                        <!--    </div>-->
                                        <!--</div>-->
                                        <!--<div class="col-md-4">-->
                                        <!--    <label class="col-md-3 control-label">Table Booking Facility Available From</label>-->
                                        <!--<div class="col-md-2">-->
                                        <!--        <div class="input-group">-->
                                        <!--            <input type="text" class="form-control timepicker timepicker-no-seconds" id="table_booking_opening_time" value="<?php echo isset($Array1[0]->table_booking_opening_time) ? $Array1[0]->table_booking_opening_time : '' ?>">-->
                                        <!--            <span class="input-group-btn">-->
                                        <!--                <button class="btn default" type="button">-->
                                        <!--                    <i class="fa fa-clock-o"></i>-->
                                        <!--                </button>-->
                                        <!--            </span>-->
                                        <!--        </div>-->
                                        <!--    </div>-->

                                        <!--    <div class="col-md-4">-->
                                        <!--        <label class="control-label">&nbsp;</label>-->
                                        <!--        <div class="input-group">-->
                                        <!--            <input type="text" class="form-control timepicker timepicker-no-seconds" id="table_booking_closing_time" value="<?php echo isset($Array1[0]->table_booking_closing_time) ? $Array1[0]->table_booking_closing_time : '' ?>">-->
                                        <!--            <span class="input-group-btn">-->
                                        <!--                <button class="btn default" type="button">-->
                                        <!--                    <i class="fa fa-clock-o"></i>-->
                                        <!--                </button>-->
                                        <!--            </span>-->
                                        <!--        </div>-->
                                        <!--    </div>-->


                                        <!--</div>-->


                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo $this->lang->line("busy") ?></label>
                                    <div class="col-md-6">
                                        <?php
                                        $opening_time = date('H:i', strtotime($Array1[0]->opening_time));
                                        $closing_time = date('H:i', strtotime($Array1[0]->closing_time));
                                        $currentTime = date('H:i', time());

                                        if ($currentTime < $opening_time) {
                                            ?>
                                            <input type="checkbox" id="busy_status" name="busy_status" value="<?= $Array1[0]->busy_status ?>" <?php echo (isset($Array1[0]->busy_status) && $Array1[0]->busy_status == 1 ? 'checked="checked"' : ''); ?> disabled="true">
                                            <?php
                                        } else {
                                            ?>
                                            <input type="checkbox" id="busy_status" name="busy_status" value="<?= $Array1[0]->busy_status ?>" <?php echo (isset($Array1[0]->busy_status) && $Array1[0]->busy_status == 1 ? 'checked="checked"' : ''); ?>>
                                            <?php
                                        }
                                        ?>
                                        <label><?php echo $this->lang->line("yes") ?></label>
                                    </div>
                                </div>
                                <?php
//echo '<pre>';
//print_r($Array1);
//exit;
                                ?>



                                <div class="form-group">
                                    <label class="col-md-3 control-label">Self Pickup</label>
                                    <div class="col-md-6" style="    display: flex;">
                                        <div style="width: 200px;">
                                            <input type="checkbox" id="self_pickup" name="self_pickup" <?php echo (isset($Array1[0]->self_pickup) && $Array1[0]->self_pickup == 1 ? 'checked="checked"' : ''); ?> value="0">
                                            <label>Yes</label>
                                        </div>

                                        <div style="width: 200px;">
                                            <input type="checkbox" id="fastdelvry" name="fastdelvry" <?php echo (isset($Array1[0]->fastdelvry) && $Array1[0]->fastdelvry == 1 ? 'checked="checked"' : ''); ?> value="0">
                                            <label>Fast Delivery</label>
                                        </div>
                                    </div> 
                                </div>



                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("delivery_time") ?>
                                        <br><span class="img-size-common">(in mins)</span>
                                    </label>
                                    <div class="col-md-6">
                                        <input id="delivery_time" value="<?= isset($Array1[0]->delivery_time) ? $Array1[0]->delivery_time : '' ?>" type="number" disabled class="form-control">
                                    </div>
                                </div>
                                <div class="form-group" style="display: none;">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("delivery_fee") ?></label>
                                    <div class="col-md-6">
                                        <input id="service_charge" value="<?= isset($Array1[0]->service_charge) ? $Array1[0]->service_charge : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("minimum_amount") ?></label>
                                    <div class="col-md-6">
                                        <input id="min_amount" value="<?= isset($Array1[0]->min_amount) ? $Array1[0]->min_amount : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                
                                
                                
                                
                                
                                <?php
                                $prescriptionreq=!empty($Array1[0]->prescriptionreq) ? $Array1[0]->prescriptionreq : '';
                                ?>
                                <div class="form-group" style="display: <?= $Session->store_type=='3' ? 'none' : 'none'; ?>;">
                                    <label class="control-label col-md-3">Prescription is required</label>
                                    <div class="col-md-6">
                                        <input type="checkbox" id="prescriptionrequired" <?= !empty($prescriptionreq) ? "checked" : '' ?> name="prescriptionrequired" value="1">
                                       <label>Yes</label>
                                    </div>
                                </div>
                                
                                
                                
                                
                                
                                
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("business_logo") ?></label>
                                    <div class="col-md-6">
                                        <?php $rand = rand(); ?>
                                        <input type="file" class="custom-file-input MultipleImages form-control " id="" refval="<?= $rand ?>" src=""> 

                                        <div class="__imhtum">
                                            <a target="_blank" href="<?= $Session->LogoFullURL ?>">

                                                <img src=" <?php
                                                if (isset($Session->image) && $Session->image != "") {
                                                    echo base_url() . 'uploads/vendor_images/' . $Session->image;
                                                }
                                                ?>" width="100%" id="ImagesEncode_<?= $rand ?>" OldImage='<?= !empty($Session->image) ? 'uploads/vendor_images/' . $Session->image : '' ?>' class="business_logo">
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>

                                        <?php if (isset($Session->image) && $Session->image != "") { ?>
                                            <a class="remove_image_common " path="uploads/vendor_images/" name="<?= isset($Session->image) ? $Session->image : '' ?>" table="users" id="<?= $Session->id ?>" type=""><?php echo $this->lang->line("remove") ?></a>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group" style="display:none">
                                    <label class="control-label col-md-3"><?php echo $this->lang->line("trade_license") ?></label>
                                    <div class="col-md-6">
                                        <?php $rand = rand(); ?>
                                        <input type="file" class="custom-file-input MultipleImages form-control" id="" refval="<?= $rand ?>" src=""> 
                                        <div class="__imhtum">
                                            <a target="_blank" href="<?= $Session->LogoFullURL ?>">

                                                <img src=" <?php
                                                if (isset($Session->trade_license) && $Session->trade_license != "") {
                                                    echo base_url() . 'uploads/trade_license/' . $Session->trade_license;
                                                }
                                                ?>"  id="ImagesEncode_<?= $rand ?>" OldImage='<?= !empty($Session->trade_license) ? 'uploads/trade_license/' . $Session->trade_license : '' ?>' class="trade_license">
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>

                                        <?php if (isset($Session->trade_license) && $Session->trade_license != "") { ?>
                                            <a class="remove_image_common " path="uploads/trade_license/" name="<?= isset($Session->image) ? $Session->trade_license : '' ?>" table="users" id="<?= $Session->id ?>" type="" field="trade_license"><?php echo $this->lang->line("remove") ?></a>
                                        <?php } ?>
                                    </div>
                                </div>
                                <!--<div class="form-group">-->
                                <!--    <label class="control-label col-md-3">Upload Banner Image</label>-->
                                <!--    <div class="col-md-6">-->
                                <!--     <input type="file" name="files[]" id="files" multiple >-->
                                <!--    </div>-->
                                <!--</div>-->


                                <div class="row" style="display: <?php echo (isset($Array1[0]->isDelivery) && $Array1[0]->isDelivery == 1 ? 'block' : 'none'); ?>">
                                    <div class="col-sm-12">
                                        <div class="panel-group" style="padding: 10px;">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" href="#collapse1">Delivery Charges</a>
                                                    </h4>
                                                </div>
                                                <div id="collapse1" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <?php
                                                        $charges_type = isset($Array1[0]->charges_type) ? $Array1[0]->charges_type : '0';
                                                        ?>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Charges Type</label>
                                                            <div class="col-md-6">
                                                                <input type="radio" onclick="VENDRCOMN.updatedeleverycharges()" name="charges_type" <?= $charges_type == '0' ? 'checked' : '' ?>  value="0">
                                                                <label>Fixed</label>
                                                                <input type="radio" onclick="VENDRCOMN.updatedeleverycharges()" name="charges_type" <?= $charges_type == '1' ? 'checked' : '' ?> value="1">
                                                                <label>Flexible</label>
                                                            </div>
                                                        </div>
                                                        <div id="Flexiblecharges" style="display: <?= $charges_type == '1' ? 'block' : 'none' ?>">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">First Kilometre</label>
                                                                <div class="col-md-6">
                                                                    <input id="dlvry_chrge_frst_km" value="<?= !empty($store->dlvry_chrge_frst_km) ? $store->dlvry_chrge_frst_km : '' ?>" type="text" placeholder="Kilometre" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">First Kilometre Amount</label>
                                                                <div class="col-md-6">
                                                                    <input id="dlvry_chrge" value="<?= !empty($store->service_charge) ? $store->service_charge : '' ?>" type="text" placeholder="Amount" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Every per Kilometre charges</label>
                                                                <div class="col-md-6">
                                                                    <input id="dlvry_chrge_perkm" value="<?= !empty($store->dlvry_chrge_perkm) ? $store->dlvry_chrge_perkm : '' ?>" type="text" placeholder="Amount" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="Fixedcharges" style="display: <?= $charges_type == '0' ? 'block' : 'none' ?>">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Charges</label>
                                                                <div class="col-md-6">
                                                                    <input id="dlvry_chrge_ch" value="<?= !empty($store->service_charge) ? $store->service_charge : '' ?>" type="text" placeholder="Amount" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>












                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="UpdateStore" class="btn blue">
                                            <i class="fa fa-check"></i> <?php echo $this->lang->line("submit") ?></button>
                                        <button type="button" onclick="window.location = '<?= base_url('vendor_dashboard') ?>';" class="btn default"><?php echo $this->lang->line("cancel") ?></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
