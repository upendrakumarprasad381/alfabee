<?php
$Session = GetSessionArrayVendor();



?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"> <?php echo $this->lang->line("transactions")?></span>
                            
                        </div>
                    </div>
                    
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover common_table">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no") ?></th>
                                    <th>Order No</th>
<!--                                    <th>Vendor Name</th>-->
                                    <!--<th><php echo $this->lang->line("customer_name") ?></th>-->
                                    <th><?php echo $this->lang->line("mode_of_payment") ?></th>
                                    <th><?php echo $this->lang->line("order_date") ?></th>
                                    <th><?php echo $this->lang->line("amount") ?>  In PKR</th>
                                    <th>Service Charge  In PKR</th>

                                    <th>Vendor Earnings  In PKR</th>
                                    <th>Alfabee Earnings  In PKR</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                
                                 $select = ",U.name,RD.restaurant_name,UT.restaurant_earning,UT.homeeats_earning,RD.commission";
                                $join = " LEFT JOIN users U ON U.id=O.user_id LEFT JOIN restaurant_details RD ON RD.vendor_id=O.vendor_id LEFT JOIN user_transactions UT ON UT.order_id=O.id";
                                $Qry = "SELECT O.* $select FROM `orders` O $join WHERE  O.archive=0 AND O.vendor_id='$Session->id' AND O.order_status NOT IN(7) GROUP BY O.id $Cond  order by O.id desc";
                                $transactions = $this->Database->select_qry_array($Qry);

                                for ($i = 0; $i < count($transactions); $i++) {
                                    $d = $transactions[$i];
                                    $total = $d->total_price ; // + $d->service_charge
                                   $alfaBeeEar= ($total * $d->commission) / 100;
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->orderno; ?></td>
                                        <!--<td><= $d->restaurant_name ?> </td>--> 
                                        <!--<td><= $d->name ?></td>-->
                                        <td>
                                            <?php
                                            if ($d->transaction_type == 1) {
                                                echo 'Online';
                                            } else {
                                                echo 'Cash on delivery';
                                            }
                                            ?>
                                        </td>
                                        <td><?= date('d/m/Y', strtotime($d->date)) ?></td>
                                        <td><?= number_format($total, 2) ?></td>
                                        <td><?= number_format($d->service_charge, 2) ?></td>


                                        <td><?= number_format($total-$alfaBeeEar, 2) ?></td>
                                        <td><?= number_format($alfaBeeEar, 2) ?></td>

                                    </tr>
                                <?php } 
                                
                                
                                /*
                                for ($i = 0; $i < count($transactions); $i++) {
                                    $d = $transactions[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?=GetNameById($d->user_id,'users','name')?></td>
                                        <td>PKR <?=number_format($d->amount,2)?></td>
                                        <td>
                                            <?php
                                            if($d->transaction_type==1)
                                            {
                                                echo 'Online';
                                            }else{
                                                echo 'Cash on delivery';
                                            }
                                        ?>
                                        </td>
                                        <td><?=date('D, d M-Y h:i A',strtotime($d->timestamp))?></td>
                                        <td>PKR <?=number_format($d->restaurant_earning,2)?></td>
                                        <td>PKR <?=number_format($d->homeeats_earning,2)?></td>
                                        
                                    </tr>
                                <?php  } */
                                
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
