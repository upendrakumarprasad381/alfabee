<?php
if (isset($_POST['submit'])) {
    $username = !empty($_POST['username']) ? $_POST['username'] : '';
    $qry = "SELECT *  FROM `users` WHERE `email` LIKE '" . $username . "' AND user_type='2'";
    $dArray = $this->Database->select_qry_array($qry);
    if (!empty($dArray)) {
        if (!isset($_POST['otpcode'])) {
            $OtpCode = mt_rand(10000, 99999);
            $_POST['old_otp'] = $OtpCode;

            $html = '';
            $html = $html . "<p>Dear " . ucfirst($dArray[0]->name) . ',</p>';
            $html = $html . "<p>You have one new OTP Code: $OtpCode</p>";
            $html = emailTemplate($html);
            send_mail($dArray[0]->email, 'OTP Verification', $html);
            $message = 'Please verify your email address.';
        } else {

            if ($_POST['old_otp'] == $_POST['otpcode']) {
                $json = [];
                $json['password'] = md5($_POST['password']);
                $userId = $this->Database->update('users', $json, array('id' => $dArray[0]->id));

                header("Refresh:1; url=" . base_url('Login/Vendor'));

                $message = 'password updated successfully.';
            } else {
                $message = 'Invalid otp code.';
            }
        }
    } else {
        $message = 'Invalid email address.';
    }
}
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<div class="form-gap"></div>
<div class="container">
    <div class="row" style="margin-top: 100px;">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="text-center">
                        <h3><i class="fa fa-lock fa-4x"></i></h3>
                        <h2 class="text-center">Forgot Password?</h2>
                        <p>You can reset your password here.</p>
                        <div class="panel-body">

                            <form id="register-form" role="form" autocomplete="off" class="form" method="post">
                                <?php if (!empty($message)) { ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?= $message ?>
                                    </div>
                                <?php } ?>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                        <input name="username" id="username" value="<?= !empty($_POST['username']) ? $_POST['username'] : '' ?>" name="email" placeholder="email address" class="form-control"  type="email">
                                    </div>
                                </div>
                                <?php
                                if (isset($_POST['submit']) && !empty($dArray)) {
                                    ?>
                                    <input type="hidden" name="old_otp" fff="<?= $_POST['old_otp'] ?>" value="<?= !empty($_POST['old_otp']) ? $_POST['old_otp'] : '' ?>">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                            <input type="text" name="otpcode" class="form-control" value="<?= !empty($_POST['otpcode']) ? $_POST['otpcode'] : '' ?>" autocomplete="off" placeholder="OTP Code" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                            <input type="password" name="password" class="form-control" value="<?= !empty($_POST['password']) ? $_POST['password'] : '' ?>" autocomplete="off" placeholder="New password" required>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="form-group">
                                    <input  class="btn btn-lg btn-primary btn-block"  name="submit" value="Reset Password" type="submit">
                                </div>


                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>