
(function ($) {
    "use strict";

    /*==================================================================
     [ Focus Contact2 ]*/
    $('.input100').each(function () {
        $(this).on('blur', function () {
            if ($(this).val().trim() != "") {
                $(this).addClass('has-val');
            } else {
                $(this).removeClass('has-val');
            }
        })
    })


    /*==================================================================
     [ Validate after type ]*/
    $('.validate-input .input100').each(function () {
        $(this).on('blur', function () {
            if (validate(this) == false) {
                showValidate(this);
            } else {
                $(this).parent().addClass('true-validate');
            }
        })
    })

    /*==================================================================
     [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.login100-form-btn').on('click', function () {
        var check = true;

        for (var i = 0; i < input.length; i++) {
            if (validate(input[i]) == false) {
                showValidate(input[i]);
                check = false;
            }
        }
        if (check == true) {
            var UserName = $('#UserName').val();
            var Password = $('#Password').val();
            var data = {UserName: UserName, Password: Password};
            $('#MessageDiv').empty();
            $.ajax({
                url: base_url + "Login/CheckAdminLogin",
                type: 'POST',
                async: false,
                data: data,
                success: function (data) {
                    var json = jQuery.parseJSON(data);
                    if (json.status == 'error') {
                        var html = '<span class="label-input100" style="color: red;">' + json.msg + '</span>';
                        $('#MessageDiv').append(html);
                        return false;
                    } else if (json.status == 'success') {
                        var html = '<span class="label-input100" style="color: green;">' + json.msg + '</span>';
                        $('#MessageDiv').append(html);
                        window.location = json.RedirectURL;
                    }

                }
            });
            return false;
        }
        return check;

    });


    $('.validate-form .input100').each(function () {
        $(this).focus(function () {
            hideValidate(this);
            $(this).parent().removeClass('true-validate');
        });
    });

    function validate(input) {
        if ($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if ($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        } else {
            if ($(input).val().trim() == '') {
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }



})(jQuery);