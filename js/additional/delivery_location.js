 (function ($) {
    var lat = $("#latitude").val();
    var lng = $("#longitude").val();
     var DeliveryAreaType = "z";
      var initMap = function () {
        if(lat!='' && lng!=''){
            var mapCenter = new google.maps.LatLng(lat, lng);
        }else{
            var mapCenter = new google.maps.LatLng(25.276987,55.296249);
        }
        // var mapCenter = new google.maps.LatLng(25.276987,55.296249);
        var map_container = $('#map'),
            map = new google.maps.Map(map_container[0], {
                center: mapCenter,
                // center : new google.maps.LatLng(lat, lng),
                zoom: 15
            }),
            drawingManager = new google.maps.drawing.DrawingManager({
                drawingControlOptions: {
                    drawingModes: [
                        google.maps.drawing.OverlayType.POLYGON
                    ]
                },
                map: map,
                polygonOptions: {
                    editable: DeliveryAreaType == "z" ? true : false,
                    draggable: true
                }
            }),
            marker = new google.maps.Marker({
                position: mapCenter,
                // position : new google.maps.LatLng(lat, lng),
                map: map,
                title: $("#restaurant_name").val(),
                animation: google.maps.Animation.DROP

            }),
            input = $('#delivery_area1'),
            geocoder = new google.maps.Geocoder(),
            address = $('#restaurant_streetaddress').val(),
            title_input = $('#restaurant_name').val();
        if (!address && map_container.data('address3')) {
            address = map_container.data('address3');
        }
        if (address.length) {
            geocoder.geocode({address: address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                }
            });
        }
        var sourceObj = $("#restaurant_delivery_radius");
        var radiusVal = sourceObj.val();
        // if (radiusVal && document.getElementById('longitude').value && document.getElementById('latitude').value) {
        //     // Add the circle for this city to the map.
        //     var circle = new google.maps.Circle({
        //         strokeColor: '#FF0000',
        //         strokeOpacity: 0.8,
        //         strokeWeight: 2,
        //         fillColor: '#FF0000',
        //         fillOpacity: 0.35,
        //         map: map,
        //         center: new google.maps.LatLng(document.getElementById('latitude').value, document.getElementById('longitude').value),
        //         radius: (DistanceUnit == "Km" ? parseFloat(radiusVal * 1000) : parseFloat(radiusVal * 1609.34))
        //     });
        //     circle.drawing_type = "circle";
        //     circle.setOptions({editable:true,draggable:false});
        //     map.fitBounds(circle.getBounds());
        //     google.maps.event.addListener(circle, 'center_changed', function () {
        //         //console.log(circle);
        //     });
        //     google.maps.event.addListener(circle, 'radius_changed', function () {
        //         //console.log(circle.radius);
        //         var changedRadius = (DistanceUnit == "Km" ? parseFloat(circle.radius / 1000) : parseFloat(circle.radius / 1609.34)).toFixed(2);
        //         sourceObj.val(changedRadius);
        //         map.fitBounds(circle.getBounds());
        //     });
        //     google.maps.event.addListener(circle, 'click', function () {
        //         //console.log(circle);
        //     });
        // }
        if ($('#delivery_area1').val().length && DeliveryAreaType == "z") {
            var initPath1 = $.parseJSON($('#delivery_area1').val()),
                paths1 = [];
            for (var i = 0, l = initPath1.length; i < l; i++) {
                paths1.push(new google.maps.LatLng(initPath1[i][0], initPath1[i][1]));
            }
          
            var initPolygon1 = new google.maps.Polygon({
                paths: paths1,
                editable: true,
                map: map
            });
          //  map.fitBounds(initPolygon1.getBounds());
        }
   

        google.maps.event.addListener(drawingManager, 'polygoncomplete', function (polygon) {
            drawingManager.setDrawingMode(null);
            if (typeof initPolygon1 != 'undefined') {
                initPolygon1.setMap(null);
            }
            initPolygon1 = polygon;
        });
        
        
    $('#removeLine').click(function (e) {
        
        initPolygon1.setMap(null);
        });
    
    // $('#updateLocation').click(function (e) {
    $('#map').closest('form').on('submit', function (e) {
            if(DeliveryAreaType == "z") {
                if (typeof initPolygon1 != 'undefined') {
                    var vertices3 = initPolygon1.getPath(),
                        paths1 = [];
                    for (var i = 0; i < vertices3.getLength(); i++) {
                        var xy3 = vertices3.getAt(i);
                        var lat = Math.floor(xy3.lat()*1000+0.5)/1000; 
                        var lng = Math.floor(xy3.lng()*1000+0.5)/1000; 
                        paths1.push([lat, lng]);
                        // paths1.push([xy3.lat(), xy3.lng()]);
                    }
                    input.val(JSON.stringify(paths1));
                    $('[name="delivery_area1"]').val(JSON.stringify(paths1));
                    $('[name="vendor_id"]').val();
                }
                else {
                    e.preventDefault();
                    alert("Please select delivery zone");
                    return false;
                }
            }else{
                if(radiusVal==""){
                    e.preventDefault();
                    alert("Enter delivery radius more than 0");
                    return false;
                }
            }
        });
       
       
        
        
 }

  
    	window.initMap = initMap;
    
 })($);
   
        
 
  
// (function ($) {
  
// 	var map,
//     marker = false,
//     geocoder;

      
// 	var initMap = function () {
// 		var mapCenter = new google.maps.LatLng(25.276987,55.296249);

// 		map = new google.maps.Map(document.getElementById('map'), {
// 			center: mapCenter,
// 			zoom: 13,
// 			draggable: true
// 		});
// 		var polyOptions = {
//                     strokeWeight: 0,
//                     fillOpacity: 0.45,
//                     editable: true,
//                     draggable: true
//                 };

		
//                  drawingManager = new google.maps.drawing.DrawingManager({
//                     drawingMode: google.maps.drawing.OverlayType.POLYGON,
//                     markerOptions: {
//                         draggable: true
//                     },
//                     polylineOptions: {
//                         editable: true,
//                         draggable: true
//                     },
//                     rectangleOptions: polyOptions,
//                     circleOptions: polyOptions,
//                     polygonOptions: polyOptions,
//                     map: map
//                  });
//             marker = new google.maps.Marker({
// 			position: mapCenter,
// 			map: map,
// 			draggable: true
// 		});
		
// 		if ($('#delivery_area1').val().length) {
//             var initPath1 = $.parseJSON($('#delivery_area1').val()),
//                 paths1 = [];
//             for (var i = 0, l = initPath1.length; i < l; i++) {
//                 paths1.push(new google.maps.LatLng(initPath1[i][0], initPath1[i][1]));
//             }
//             var initPolygon1 = new google.maps.Polygon({
//                 paths: paths1,
//                 editable: true,
//                 map: map
//             });
//             map.fitBounds(initPolygon1.getBounds());
//         }
		
//         google.maps.event.addListener(drawingManager, 'polygoncomplete', function (polygon) {
//             drawingManager.setDrawingMode(null);
//             if (typeof initPolygon1 != 'undefined') {
//                 initPolygon1.setMap(null);
//             }
//             initPolygon1 = polygon;
//         });

// 	}


    



// 	window.initMap = initMap;

// })($);

