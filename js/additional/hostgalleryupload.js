Dropzone.autoDiscover = false;
var uploadedDropzone=[];
var avalTblG = 'host_property_gallery';
var host_idG = $('#property_id').val();
if (Method == 'addhostservice') {
    avalTblG = 'host_service_gallery';
    host_idG = $('#service_id').val();
} else if (Method == 'addhostretreat') {
    avalTblG = 'host_retreat_gallery';
    host_idG = $('#retreat_id').val();
}
var myDropzone = new Dropzone("div#uploadgallery", {
    url: base_url + "helper",
    addRemoveLinks: true,
    success: function (file, response) {
        try {
            var json = jQuery.parseJSON(response);
            $(file.previewTemplate).find('.dz-remove').attr('id', json.insertId);
            if (json.status == false) {
                alert(json.message);
                 $(file.previewElement).remove();
            }else{
                uploadedDropzone.push(json.insertId);
            }
        } catch (e) {

        }

    },
    init: function () {
        this.on("sending", function (file, xhr, formData) {
            formData.append("function", 'uploadgallery');
            formData.append("avalTbl", avalTblG);
            formData.append("host_id", host_idG);
        });
        this.on("removedfile", function (file) {
            console.log(file);
            var fileId = $(file.previewTemplate).find('.dz-remove').attr('id');
            var data = {function: 'removeuploadgallery', fileId: fileId, avalTblG: avalTblG, host_idG: host_idG};
            $.ajax({
                url: base_url + "Admin/Helper",
                data: data,
                type: 'POST',
                success: function (data) {

                }
            });
        });
    }
});
