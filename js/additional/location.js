var placeSearch, autocomplete;
      var componentForm = {
        locality: 'long_name',
        // street_number: 'short_name',
        // route: 'long_name',
        // locality: 'long_name',
        administrative_area_level_1: 'short_name',
        sublocality_level_1: 'short_name',
        // country: 'long_name',
        // postal_code: 'short_name'
      };
      
      

      function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('autocomplete')),
            {
                //  type: ['geocode'],
                //  type: ['(cities)'],
                 componentRestrictions: {country: 'ae'}
                
            }
                );
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        var place = autocomplete.getPlace();
        console.log(place);
        for (var component in componentForm) {
          document.getElementById(component).value = '';
        //   document.getElementById(component).disabled = true;
        }
        $('#sublocality_level_1').prop("disabled", false);
        $('#location_lat').val(place.geometry.location.lat());
        $('#location_lng').val(place.geometry.location.lng());
        $('.latitude').val(place.geometry.location.lat());
        $('.longitude').val(place.geometry.location.lng());
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
            var loc = place.formatted_address.split('-');
            var get_location = loc[0].toLowerCase();
            $('.get_location').val(get_location);
          }
        }
      }

      function geolocate() {
        $('.delete-icon').css('display','block');
        $('.pointer').css('display','none');
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
           
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }