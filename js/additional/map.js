(function ($) {
    
    var placeSearch, autocomplete;
      var componentForm = {
        locality: 'long_name',
        // street_number: 'short_name',
        // route: 'long_name',
        // locality: 'long_name',
        administrative_area_level_1: 'short_name',
        sublocality_level_1: 'short_name',
        // country: 'long_name',
        // postal_code: 'short_name'
      };
  
	var map,
    marker = false,
    geocoder;

      
	var initMap = function () {
		     var defaultLat = 25.276987;
        var defaultLong = 55.296249;
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {


                defaultLat = position.coords.latitude;
                defaultLong = position.coords.longitude;

 var mapCenter = new google.maps.LatLng(defaultLat, defaultLong);


		map = new google.maps.Map(document.getElementById('map'), {
			center: mapCenter,
			zoom: 13,
			draggable: true
		});

		$('#map_panel').on('hidden.bs.modal', function (e) {
			$("#location").val($("#store-location").val());
		});
		$('#map_panel').on('shown.bs.modal', function () {
			google.maps.event.trigger(map, "resize");
		});

		var input = document.getElementById('store-location');
		var autocomplete = new google.maps.places.Autocomplete(input);
		//autocomplete.setComponentRestrictions({'country': 'ae'});
		
		marker = new google.maps.Marker({
			position: mapCenter,
			map: map,
			draggable: true
		});
		
        google.maps.event.addListener(marker, 'dragend', function (event) {
             geocodePositionByAddress(marker.getPosition());
			markerLocation();
        });
		
		google.maps.event.addListener(autocomplete, 'place_changed', function () {
			
			var place = autocomplete.getPlace();
            latlng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
    
            map.setCenter(latlng);
    
            map.setZoom(13);
    
			marker.setPosition(latlng);
			
			markerLocation();
		});
		map.setCenter(mapCenter);
		
		var initPath1 = $.parseJSON($('#delivery_location').val());
                paths1 = [];
            for (var i = 0, l = initPath1.length; i < l; i++) {
                paths1.push(new google.maps.LatLng(initPath1[i][0], initPath1[i][1]));
            }
          
            var initPolygon1 = new google.maps.Polygon({
                paths: paths1,
                editable: false,
                map: map
            });
		
	});
        }

	}

	function markerLocation() {
		var currentLocation = marker.getPosition();
	

		$('#location-lat').val(currentLocation.lat()); //latitude
		$('#location-lng').val(currentLocation.lng()); //latitude
		var lat = Math.floor(currentLocation.lat()*100+0.5)/100; 
        var lng = Math.floor(currentLocation.lng()*1000+0.5)/1000;
		$('#location_lat').val(lat); //latitude
		$('#location_lng').val(lng); //latitude
		geocodePosition(currentLocation);
    }
    
    function geocodePosition(pos) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            latLng: pos
        }, function (responses) {
            var arr = [];
            if (responses && responses.length > 0) {
                for(i=0;i<responses.length;i++)
                {
                    if(responses[i].geometry.location_type=='APPROXIMATE')
                    {
                        var resp = responses[i].formatted_address;
                        arr.push(resp);

                        // document.getElementById('store-location').value = responses[0].formatted_address;
                        // document.getElementById('venue').value = responses[0].formatted_address;
                    }
                       
                        
                }
                var locality = arr[0].split("-");
                var array =[];
                if(locality.length<3){
                    for(i=0;i<responses.length;i++)
                    {

                        if(responses[i].geometry.location_type=='GEOMETRIC_CENTER')
                        {
                            var resp = responses[i].formatted_address;
                            // console.log(resp);
                            array.push(resp);
    
                            // document.getElementById('store-location').value = responses[0].formatted_address;
                            // document.getElementById('venue').value = responses[0].formatted_address;
                        }
                           
                            
                    }
                }
                
                
                if(locality.length<3 && array.length!=0){
                    var data_value = array[1];
            
                    if(data_value!=undefined)
                    {
                        var datas = data_value;
                    }else{
                        var datas = array[0];
                    }
                    var data_locality = datas.split("-");
                    var data = data_locality[1];
                }else{
                    var datas = arr[0];
                    var data_locality = arr[0].split("-");
                    var data = data_locality[1];
                }
                
                // document.getElementById('store-location').value = datas;
                document.getElementById('store-location').value = document.getElementById('store-location').value;
                document.getElementById('location').value = datas;
                // document.getElementById('location_area').value = datas;
                document.getElementById('location_area').value = document.getElementById('store-location').value;
                $('.location').val(datas);
                document.getElementById('locality').value = data;
                
                 google.maps.event.addListener(marker, 'dragend', function (event) {
                
        			document.getElementById('store-location').value =  datas;
                });
                
            } else {
               
                document.getElementById('store-location').value = '';
            }
        
            // if (responses && responses.length > 0) {
                
            //     document.getElementById('store-location').value = responses[0].formatted_address;
            //     document.getElementById('location').value = responses[0].formatted_address;
            //     document.getElementById('location_area').value = responses[0].formatted_address;
            //     $('.location').val(responses[0].formatted_address);
            // } else {
               
            //     document.getElementById('store-location').value = '';
            // }
        
        });
    }

    var pacContainerInitialized = false;
	$('#store-location').keypress(function () {
		if (!pacContainerInitialized) {
			$('.pac-container').css('z-index', '9999');
			pacContainerInitialized = true;
		}
	});

	window.initMap = initMap;

})($);
getLocation();
function getLocation()
{
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {

            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            lattt = position.coords.latitude
            // console.log(geolocation);exit;
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            // console.log(geolocation.lat);exit;
            latlng = new google.maps.LatLng(geolocation.lat, geolocation.lng),
                    // autocomplete.setBounds(circle.getBounds());
                    geocoder = new google.maps.Geocoder();
            geocoder.geocode({'latLng': latlng}, function (responses, status) {
                var arr = [];
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log(responses);
                    if (responses && responses.length > 0) {

                        for (i = 0; i < responses.length; i++)
                        {
//                            if (responses[i].geometry.location_type == 'APPROXIMATE')
//                            {
                            var resp = responses[i].formatted_address;
                            arr.push(resp);
                            //  }
                        }
                    }
                }

                var cur_loc = arr[0];
            
                $('#location_area,#store-location').val(cur_loc);
                //   document.getElementById('venue').value = locality;
              
                $('#lat').val(geolocation.lat); //latitude
                $('#lng').val(geolocation.lng); //latitude
                $('#location_lat,#location-lat').val(geolocation.lat); //latitude
                $('#location_lng,#location-lng').val(geolocation.lng); //latitude
            });
        });
    }
}
function geocodePositionByAddress(pos) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        latLng: pos
    }, function (responses) {
        if (responses && responses.length > 0) {
            var addressName = responses[0].formatted_address;
            $('#store-location').val(addressName);
        } else {
            return '';
        }
    });
}
/* $(document).ready(function () {
	initMap();
});
 */