(function ($) {
  
	var map,
    marker = false,
    geocoder;
	var initMap = function () {
		var mapCenter = new google.maps.LatLng(25.276987,55.296249);

		map = new google.maps.Map(document.getElementById('map'), {
			center: mapCenter,
			zoom: 13,
			draggable: true
		});

		$('#party_venue_map').on('hidden.bs.modal', function (e) {
			$("#location").val($("#store-location").val());
		});
		$('#party_venue_map').on('shown.bs.modal', function () {
			google.maps.event.trigger(map, "resize");
		});

		var input = document.getElementById('store-location');
		var autocomplete = new google.maps.places.Autocomplete(input);
		autocomplete.setComponentRestrictions({'country': 'ae'});
		
		marker = new google.maps.Marker({
			position: mapCenter,
			map: map,
			draggable: true
		});
		
        google.maps.event.addListener(marker, 'dragend', function (event) {
			markerLocation();
        });
		
		google.maps.event.addListener(autocomplete, 'place_changed', function () {
			
			var place = autocomplete.getPlace();
            latlng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
    
            map.setCenter(latlng);
    
            map.setZoom(13);
    
			marker.setPosition(latlng);
			
			markerLocation();
		});
		map.setCenter(mapCenter);

	}

	function markerLocation() {
		var currentLocation = marker.getPosition();

		$('#lat').val(currentLocation.lat()); //latitude
		$('#lng').val(currentLocation.lng()); //latitude
		$('#location_lat').val(currentLocation.lat()); //latitude
		$('#location_lng').val(currentLocation.lng()); //latitude
		geocodePosition(currentLocation);
    }
    
    function geocodePosition(pos) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            latLng: pos
        }, function (responses) {
            var arr = [];
            if (responses && responses.length > 0) {
                for(i=0;i<responses.length;i++)
                {
                    if(responses[i].geometry.location_type=='APPROXIMATE')
                    {
                        var resp = responses[i].formatted_address;
                        arr.push(resp);

                        // document.getElementById('store-location').value = responses[0].formatted_address;
                        // document.getElementById('venue').value = responses[0].formatted_address;
                    }
                       
                        
                }
                var locality = arr[0].split("-");
                var array =[];
                if(locality.length<3){
                    for(i=0;i<responses.length;i++)
                    {

                        if(responses[i].geometry.location_type=='GEOMETRIC_CENTER')
                        {
                            var resp = responses[i].formatted_address;
                            // console.log(resp);
                            array.push(resp);
    
                            // document.getElementById('store-location').value = responses[0].formatted_address;
                            // document.getElementById('venue').value = responses[0].formatted_address;
                        }
                           
                            
                    }
                }
                
                
    
                if(locality.length<3 && array.length!=0){
                    var data_value = array[1];
            
                    if(data_value!=undefined)
                    {
                        var datas = data_value;
                    }else{
                        var datas = array[0];
                    }
                    var data_locality = datas.split("-");
                    var data = data_locality[1];
                }else{
                    var datas = arr[0];
                    var data_locality = arr[0].split("-");
                    var data = data_locality[1];
                }
                document.getElementById('store-location').value = datas;
                // document.getElementById('venue').value = datas;
                document.getElementById('venue').value = document.getElementById('store-location').value;
                // document.getElementById('approx_location').value = datas;
                document.getElementById('locality').value = data;
                
                
            } else {
               
                document.getElementById('store-location').value = '';
            }
        });
    }

    var pacContainerInitialized = false;
	$('#store-location').keypress(function () {
		if (!pacContainerInitialized) {
			$('.pac-container').css('z-index', '9999');
			pacContainerInitialized = true;
		}
	});

	window.initMap = initMap;

})($);


/* $(document).ready(function () {
	initMap();
});
 */