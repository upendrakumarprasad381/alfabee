$(function () {

    var Obj = {dateFormat: 'yy-mm-dd', minDate: 0};
    $(".datepicker").datepicker(Obj);
    $(".datepicker1").datepicker(Obj);
    $(".datepicker2").datepicker({
        dateFormat: 'yy-mm-dd'
    });
});
$(document).ready(function () {

    $('.timepicker').timepicker({
           timeFormat: 'HH:mm',
                 showMeridian: false,
    });
});
if (Method == 'OrderDetails' || Method == 'party_orderDetails') {
    setTimeout(function () {
        window.location = '';
    }, 35000);
}
if (sessionValue == 'ar')
{
    var sure_want_to_delete = 'هل أنت متأكد أنك تريد الحذف؟'
    var want_to_submit = 'هل أنت متأكد من إرساله؟ ';
    var reject_user = 'هل أنت متأكد أنك تريد رفض هذا المستخدم؟';
    var submit = 'تأكيد';
    var cancel = 'إلغاء';
    var accept_user = 'هل أنت متأكد أنك تريد قبول هذا المستخدم؟';
    var want_to_update = 'هل تريد التحديث؟';
    var reset_position = 'هل تريد إعادة التعيين بالفعل؟'
    var delete_data = 'حذف!!';
} else {
    var sure_want_to_delete = 'Are you sure you want to delete this?';
    var want_to_submit = 'Are you sure want to submit ?';
    var reject_user = 'Are you sure you want to reject this user?'
    var submit = 'Submit';
    var cancel = 'Cancel';
    var accept_user = 'Are you sure you want to accept this user?';
    var want_to_update = 'are you sure want to update?';
    var reset_position = 'Do you want to reset position?';
    var delete_data = 'Delete!!';
}
function readNotification(obj) {
    var id = $(obj).data("id");
    var redirect_url = $(obj).data("url");
    var data = {function: 'updateNotification', id: id, redirect_url: redirect_url};
    $.ajax({
        method: "POST",
        url: base_url + "Admin/Helper",
        data: data,
        async: false,
        success: function (res) {
            var json = JSON.parse(res);
            if (json.status == 'success') {
                window.location = base_url + json.url;
            }

        },
        error: function (xhr) {
            alert("Error occured.please try again");
        }
    });
}
$('.remove_image_common').click(function () {

    var id = $(this).attr('id');
    var path = $(this).attr('path');
    var name = $(this).attr('name');
    var table = $(this).attr('table');
    var type = $(this).attr('type');
    if (table == "promo_code")
    {
        var status = 'offer_remove_image';
    } else {
        var status = 'common_remove_image';
    }

    var data = {
        function: status
        , id: id
        , path: path
        , name: name
        , table: table
        , type: type
    };

    $.confirm({
        title: 'Do you want to remove image?',
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Submit',
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/Helper"
                        , async: true
                        , type: 'POST'
                        , data: data
                        , success: function (data) {

                            var json = jQuery.parseJSON(data);
                            if (json.status == 'success') {
                                $('#success-message').html('Removed successfully');
                                $('.alert-success').show();
                                $('.alert-success').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                    window.location = '';
                                }, 2000);
                            } else {
                                $('.alert-danger').show();
                                $('#success-error').html('Try Again')
                                $('.alert-danger').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                }, 2000);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-danger',
                action: function () {
                }
            },
        }
    });
});

$("input[name='premium']").change(function () {
    var prem_val = $(this).val();
    var vendor_id = $('#vendor_id').val();
    var data = {function: 'updatePremium', premium: prem_val, vendor_id: vendor_id};
    $.ajax({
        url: base_url + "Admin/Helper",
        type: 'POST',
        data: data,
        async: false,
        success: function (data) {
            console.log(data);
            if (jQuery.parseJSON(data)) {
                var json = jQuery.parseJSON(data);
                if (json.status == 'success') {
                    setInterval('location.reload()', 1000);
                }
            }
        }
    });
});
$("#business_type").change(function () {
    var store_type = $('#business_type').val();
    var data = {store_type: store_type};
    $('#rest_id').empty();
    $.ajax({
        url: base_url + "Admin/getBusinessName",
        type: 'POST',
        data: data,
        dataType: 'json',
        // async: false,
        success: function (response) {

            var html = "";
            for (var i = 0; i < response.length; i++) {
                var dr = response[i];


                html += '<option value="' + dr.vendor_id + '">' + dr.restaurant_name + '</option>';

            }
            $('#rest_id').append(html);
            $('.selectpicker').selectpicker('refresh');
            // console.log(data);
            // if (jQuery.parseJSON(data)) {
            //     var json = jQuery.parseJSON(data);
            //     if (json.status == 'success') {
            //          setInterval('location.reload()', 1000); 
            //     }
            // }
        }
    });
});
$('.CustomerBlockUnblock').dblclick(function () {
    var CustomerId = $(this).attr('CustomerId');
    var Archive = $(this).attr('Archive');
    var data = {function: 'CustomerBlockUnblock', CustomerId: CustomerId, archive: Archive};
    $.ajax({
        url: base_url + "Admin/Helper",
        type: 'POST',
        data: data,
        async: false,
        success: function (data) {
            console.log(data);
            if (jQuery.parseJSON(data)) {
                var json = jQuery.parseJSON(data);
                if (json.status == 'success') {
                    window.location = '';
                }
            }
        }
    });
});
$(document).ready(function () {
    $('#SubmitCategory').click(function () {
        var json = '';
        json = json + '{';
        var Name = $('#Name');
        var store_type = $('#store_type');
        if (store_type.val() == '') {
            store_type.focus();
            store_type.css('border-color', 'red');
            return false;
        } else {
            store_type.css('border-color', '');
            json = json + '"store_type":"' + store_type.val() + '",';
        }
        if (Name.val() == '') {
            Name.focus();
            Name.css('border-color', 'red');
            return false;
        } else {
            Name.css('border-color', '');
            json = json + '"category_name":"' + Name.val() + '",';
        }
        json = json + '"category_name_ar":"' + $('#Name_ar').val() + '",';
        json = json + '"status":"' + $('#Status').val() + '",';
        json = json + '"Id":"' + $('#Id').val() + '"';
        json = json + '}';
        var data = {
            json: json
        };

        $.confirm({
            title: want_to_submit,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "admin/category/add-new",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {

                                    var json = jQuery.parseJSON(data);
                                    if (json.status == 'success') {
                                        $('#success-message').html(json.message);
                                        $('.alert-success').show();
                                        $('.alert-success').slideDown(500);
                                        setTimeout(function () {
                                            $('.alert-danger').slideUp(500);
                                            window.location = base_url + 'admin/category';
                                        }, 2000);

                                    } else {
                                        $('.alert-danger').show();
                                        $('#error-message').html(json.message)
                                        $('.alert-danger').slideDown(500);
                                        setTimeout(function () {
                                            $('.alert-danger').slideUp(500);
                                        }, 2000);
                                    }
                                } catch (e) {
                                    alert(e);
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });
    $('#SubmitStoreType').click(function () {
        var json = '';
        json = json + '{';
        var Name = $('#Name');
        if (Name.val() == '') {
            Name.focus();
            Name.css('border-color', 'red');
            return false;
        } else {
            Name.css('border-color', '');
            json = json + '"store_type":"' + Name.val() + '",';
        }
        json = json + '"store_type_ar":"' + $('#Name_ar').val() + '",';
        json = json + '"image":"' + $('.common_file').attr('file_name') + '",';


        json = json + '"if_nodatafound":"' + $('#if_nodatafound').val() + '",';
        json = json + '"if_shopclose":"' + $('#if_shopclose').val() + '",';
        json = json + '"if_shopbusy":"' + $('#if_shopbusy').val() + '",'; 



        json = json + '"status":"' + $('#Status').val() + '",';
        json = json + '"Id":"' + $('#Id').val() + '"';
        json = json + '}';
        var description = $('#description').val();
        var data = {
            json: json,
            description: description
        };

        $.confirm({
            title: want_to_submit,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "admin/store_type/add-new",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {

                                    var json = jQuery.parseJSON(data);
                                    if (json.status == 'success') {
                                        $('#success-message').html(json.message);
                                        $('.alert-success').show();
                                        $('.alert-success').slideDown(500);
                                        setTimeout(function () {
                                            $('.alert-danger').slideUp(500);
                                            window.location = base_url + 'admin/store_type';
                                        }, 2000);

                                    } else {
                                        $('.alert-danger').show();
                                        $('#error-message').html(json.message)
                                        $('.alert-danger').slideDown(500);
                                        setTimeout(function () {
                                            $('.alert-danger').slideUp(500);
                                        }, 2000);
                                    }
                                } catch (e) {
                                    alert(e);
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });

    $('#SubmitOccasion').click(function () {
        var json = '';
        json = json + '{';
        var Name = $('#Name');
        if (Name.val() == '') {
            Name.focus();
            Name.css('border-color', 'red');
            return false;
        } else {
            Name.css('border-color', '');
            json = json + '"occasion_name":"' + Name.val() + '",';
        }
        json = json + '"occasion_name_ar":"' + $('#Name_ar').val() + '",';
        json = json + '"status":"' + $('#Status').val() + '",';
        json = json + '"Id":"' + $('#Id').val() + '"';
        json = json + '}';
        var data = {
            json: json
        };

        $.confirm({
            title: want_to_submit,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "admin/occasion/add-new",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {

                                    var json = jQuery.parseJSON(data);
                                    if (json.status == 'success') {
                                        $('#success-message').html(json.message);
                                        $('.alert-success').show();
                                        $('.alert-success').slideDown(500);
                                        setTimeout(function () {
                                            $('.alert-danger').slideUp(500);
                                            window.location = base_url + 'admin/occasion';
                                        }, 2000);

                                    } else {
                                        $('.alert-danger').show();
                                        $('#error-message').html(json.message)
                                        $('.alert-danger').slideDown(500);
                                        setTimeout(function () {
                                            $('.alert-danger').slideUp(500);
                                        }, 2000);
                                    }
                                } catch (e) {
                                    alert(e);
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });

    $('#SubmitCuisine').click(function () {
        var json = '';
        json = json + '{';
        var cuisine_name = $('#cuisine_name');
        var cuisine_name_ar = $('#cuisine_name_ar');
        if (cuisine_name.val() == '') {
            cuisine_name.focus();
            cuisine_name.css('border-color', 'red');
            return false;
        } else {
            cuisine_name.css('border-color', '');
            json = json + '"cuisine_name":"' + cuisine_name.val() + '",';
        }
        json = json + '"cuisine_name_ar":"' + cuisine_name_ar.val() + '",';
        json = json + '"status":"' + $('#Status').val() + '",';
        json = json + '"Id":"' + $('#Id').val() + '"';
        json = json + '}';
        var data = {
            json: json
        };

        $.confirm({
            title: want_to_submit,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "admin/cuisine/add-new",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {

                                    var json = jQuery.parseJSON(data);
                                    if (json.status == 'success') {
                                        $('#success-message').html(json.message);
                                        $('.alert-success').show();
                                        $('.alert-success').slideDown(500);
                                        setTimeout(function () {
                                            $('.alert-danger').slideUp(500);
                                            window.location = base_url + 'admin/cuisine';
                                        }, 2000);

                                    } else {
                                        $('.alert-danger').show();
                                        $('#error-message').html(json.message)
                                        $('.alert-danger').slideDown(500);
                                        setTimeout(function () {
                                            $('.alert-danger').slideUp(500);
                                        }, 2000);
                                    }
                                } catch (e) {
                                    alert(e);
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });

    $('body').on('click', '.common_delete', function () {
        var Id = $(this).attr('Id');
        var message = $(this).attr('title');
        var table = $(this).attr('table');
        var url = $(this).attr('redrurl');
        var data = {
            function: 'Common_delete',
            Id: Id,
            table: table
        };

        $.confirm({
            content: sure_want_to_delete,
            title: delete_data,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/Helper",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {

                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    $('#success-message').html(json.message);
                                    $('.alert-success').show();
                                    $('.alert-success').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                        window.location = base_url + 'Admin/' + url;
                                    }, 2000);

                                } else {
                                    $('.alert-danger').show();
                                    $('#error-message').html(json.message)
                                    $('.alert-danger').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                    }, 2000);
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });
    $('body').on('click', '.autodelete', function () {
        var condjson = $(this).attr('condjson');
        var dbtable = $(this).attr('dbtable');
        var removefile = $(this).attr('removefile');
        var data = {function: 'autodelete', condjson: condjson, dbtable: dbtable, removefile: removefile};
        var cmessage = $(this).attr('cmessage') != undefined ? $(this).attr('cmessage') : 'Are you sure want to delete!.';
        $.confirm({
            title: cmessage,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/Helper",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == true) {
                                        window.location = '';
                                    }
                                } catch (e) {

                                }
                            }
                        });
                    }
                },
                close: {
                    text: cancel,
                    btnClass: 'btn-red',
                }
            }
        });
    });

    $('body').on('click', '.cuisine_delete', function () {
        var id = $(this).attr('id');
        var table = $(this).attr('table');
        var url = $(this).attr('redrurl');
        var data = {
            function: 'cuisine_delete',
            id: id,
            table: table
        };

        $.confirm({
            content: sure_want_to_delete,
            title: delete_data,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    action: function () {
                        $.ajax({
                            url: base_url + "admin/helper",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    $('#success-message').html(json.message);
                                    $('.alert-success').show();
                                    $('.alert-success').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                        window.location = base_url + 'admin/' + url;
                                    }, 2000);

                                } else {
                                    $('.alert-danger').show();
                                    $('#error-message').html(json.message)
                                    $('.alert-danger').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                    }, 2000);
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });
    $('body').on('click', '.commonReject', function () {

        var id = $(this).attr('id');
        var table = $(this).attr('table');
        var url = $(this).attr('redrurl');
        var data = {
            function: 'common_reject',
            id: id,
            table: table
        };
        $.confirm({
            content: reject_user,
            title: 'Rejected!!',
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    action: function () {
                        $("#loader").css('display', 'block');
                        $.ajax({
                            url: base_url + "admin/helper",
                            type: 'POST',
                            data: data,
                            beforeSend: function () {
                                $("#loader").css('display', 'block');

                            },
                            // async: false,
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    $("#loader").css('display', 'none');
                                    $('#success-message').html(json.message);
                                    $('.alert-success').show();
                                    $('.alert-success').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                        window.location = base_url + 'admin/' + url;
                                    }, 2000);

                                } else {
                                    $('.alert-danger').show();
                                    $('#error-message').html(json.message)
                                    $('.alert-danger').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                    }, 2000);
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });
    $('body').on('click', '.commonApprove', function () {
        var id = $(this).attr('id');
        var table = $(this).attr('table');
        var url = $(this).attr('redrurl');
        var data = {
            function: 'common_approve',
            id: id,
            table: table
        };
        $.confirm({
            content: accept_user,
            title: 'Approve!!',
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    action: function () {
                        $("#loader").css('display', 'block');
                        $.ajax({
                            url: base_url + "admin/helper",
                            type: 'POST',
                            data: data,
                            beforeSend: function () {
                                $("#loader").css('display', 'block');

                            },
                            // async: false,
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    $("#loader").css('display', 'none');
                                    $('#success-message').html(json.message);
                                    $('.alert-success').show();
                                    $('.alert-success').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                        window.location = base_url + 'admin/' + url;
                                    }, 2000);

                                } else {
                                    $('.alert-danger').show();
                                    $('#error-message').html(json.message)
                                    $('.alert-danger').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                    }, 2000);
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });


    $('.review_status').change(function () {

        var status = $(this).val();
        var feedback_id = $('#feedback_id').val();
        var data = {
            function: 'update_status',
            status: status,
            feedback_id: feedback_id
        };
        $.confirm({
            title: 'Do you want to update status?',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/Helper",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == true) {
                                        $('#success-message').html(json.message);
                                        $('.alert-success').show();
                                        $('.alert-success').slideDown(500);
                                        setTimeout(function () {
                                            $('.alert-danger').slideUp(500);
                                            window.location = base_url + 'admin/reviews';
                                        }, 2000);
                                    } else {
                                        $('.alert-danger').show();
                                        $('#error-message').html(json.message)
                                        $('.alert-danger').slideDown(500);
                                        setTimeout(function () {
                                            $('.alert-danger').slideUp(500);
                                        }, 2000);
                                    }
                                } catch (e) {
                                    alert(e)
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-danger',
                    action: function () {
                    }
                },
            }
        });
        // $.ajax({
        //       type:'POST',
        //       url:base_url + "admin/helper",
        //       data: data,
        //       async: false,
        //       success:function(data)
        //       {

        //           var json = jQuery.parseJSON(data);
        //         if(json.status==success)
        //         {
        //             $('.toggle').addClass('btn-primary');
        //             $('.toggle').removeClass('btn-default off');
        //         }else{
        //             $('.toggle').addClass('btn-default off');
        //             $('.toggle').removeClass('btn-primary');
        //         }
        //       }
        //     });
    });

    $('#addpromocode').click(function () {
        if ($('#promo_code').val() == '') {
            $('#promo_code').css('border-color', 'red');
            $('#promo_code').focus();
            return false;
        } else {
            $('#promo_code').css('border-color', '');
            var promo_code = $('#promo_code').val();
        }
        if ($('#offer_title').val() == '') {
            $('#offer_title').css('border-color', 'red');
            $('#offer_title').focus();
            return false;
        } else {
            $('#offer_title').css('border-color', '');
            var offer_title = $('#offer_title').val();
        }
        if ($('#discount').val() == '') {
            $('#discount').css('border-color', 'red');
            $('#discount').focus();
            return false;
        } else {
            $('#discount').css('border-color', '');
            var discount = $('#discount').val();
        }
        if ($('#date_from').val() == '') {
            $('#date_from').css('border-color', 'red');
            $('#date_from').focus();
            return false;
        } else {
            $('#date_from').css('border-color', '');
            var date_from = $('#date_from').val();
        }
        if ($('#date_to').val() == '') {
            $('#date_to').css('border-color', 'red');
            $('#date_to').focus();
            return false;
        } else {
            $('#date_to').css('border-color', '');
            var date_to = $('#date_to').val();
        }
        if ($('#business_type').val() == '') {
            $('#business_type').css('border-color', 'red');
            $('#business_type').focus();
            return false;
        } else {
            $('#business_type').css('border-color', '');
            var store_type = $('#business_type').val();
        }
        if ($('#rest_id').val() == '') {
            $('#rest_id').css('border-color', 'red');
            $('#rest_id').focus();
            return false;
        } else {
            $('#rest_id').css('border-color', '');

            var rest_id = $('#rest_id').val() != null ? $('#rest_id').val().join(',') : '';
        }
        var Id = $('#Id').val();
        var purchase_amount = $('#purchase_amount').val();
        var offer_title_ar = $('#offer_title_ar').val();
        var data = {
            Id: Id,
            promo_code: promo_code,
            offer_title: offer_title,
            offer_title_ar: offer_title_ar,
            store_type: store_type,
            discount: discount,
            date_from: date_from,
            date_to: date_to,
            rest_id: rest_id,
            purchase_amount: purchase_amount,
            maximum_discount: $('#maximum_discount').val(),
            discount_type: $('#discount_type').val(),
            discount_from: $('#discount_from').val(),
        };
        $.confirm({
            title: want_to_submit,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            data: data,
                            type: 'POST',
                            async: false,
                            url: base_url + "admin/promocode/add-new",
                            success: function (data) {


                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    window.location = base_url + 'admin/promocode';
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });

    $('body').on('change', '.common_file', function () {
        var form_name = $(this).attr('form_name');
        var fd = new FormData(document.getElementById(form_name));

        fd.append("function", 'common_upload_file_only');
        fd.append("id", this.id);
        fd.append("location", $(this).attr('location'));
        fd.append("delete_img", $(this).attr('prev_image'));
        $('.common_message_file').html('<img src="' + base_url + 'images/loaderline.gif" width="15%">');
        $.ajax({
            url: base_url + "Admin/helper"
            , type: "POST"
            , data: fd
            , processData: false
            , contentType: false
            , error: function (jqXHR) {
                alert('Errror')
            }
        }).done(function (data) {
            var json = jQuery.parseJSON(data);
            if (json.status == 'success') {
                $('.common_file').attr('file_name', json.file_name);
                $('.common_message_file').html("");
                if ($("#choose_file").length) {
                    $('#choose_file').text(json.orginal_file_name);
                }
            } else {
                $('.common_message_file').html("Invalid File");
                $('.common_message_file').css("color", "red");
            }
        });
        return false;
    });

    $('#SubmitNewHomeurl').click(function () {
        var json = '';
        json = json + '{';
        json = json + '"home_url":"' + $('#homeurl').val() + '",';
        json = json + '"archive":"' + $('#HomeStatus').val() + '",';
        json = json + '"home_image":"' + $('.common_file').attr('file_name') + '",';
        json = json + '"HomeBannerId":"' + $('#HomeBannerId').val() + '"';
        json = json + '}';
        var data = {json: json};
        $.confirm({
            title: want_to_submit,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/home_slider/add-new",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == 'success') {
                                        window.location = base_url + 'Admin/home_slider';
                                    } else {
                                        alert(json.message)
                                    }
                                } catch (e) {
                                    alert(e)
                                }

                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });

    });
    $('#SubmitSubSlider').click(function () {
        var json = '';
        json = json + '{';
        json = json + '"home_url":"' + $('#homeurl').val() + '",';
        json = json + '"archive":"' + $('#HomeStatus').val() + '",';
        json = json + '"home_image":"' + $('.common_file').attr('file_name') + '",';
        json = json + '"HomeBannerId":"' + $('#HomeBannerId').val() + '"';
        json = json + '}';
        var data = {json: json};
        $.confirm({
            title: want_to_submit,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/sub_slider/add-new",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == 'success') {
                                        window.location = base_url + 'Admin/sub_slider';
                                    } else {
                                        alert(json.message)
                                    }
                                } catch (e) {
                                    alert(e)
                                }

                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });

    });
    $('#SubmitSideBanner').click(function () {
        var json = '';
        json = json + '{';
        json = json + '"home_url":"' + $('#homeurl').val() + '",';
        json = json + '"archive":"' + $('#HomeStatus').val() + '",';
        json = json + '"home_image":"' + $('.common_file').attr('file_name') + '",';
        json = json + '"HomeBannerId":"' + $('#HomeBannerId').val() + '"';
        json = json + '}';
        var data = {json: json};
        $.confirm({
            title: want_to_submit,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/side_banner/add-new",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == 'success') {
                                        window.location = base_url + 'Admin/side_banner';
                                    } else {
                                        alert(json.message)
                                    }
                                } catch (e) {
                                    alert(e)
                                }

                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });

    });

    $('#SubmitHomeBanner').click(function () {
        var json = '';
        json = json + '{';
        json = json + '"archive":"' + $('#HomeStatus').val() + '",';
        json = json + '"image":"' + $('.common_file').attr('file_name') + '",';
        json = json + '"status":"' + $('#HomeStatus').val() + '",';
        json = json + '"Id":"' + $('#Id').val() + '"';
        json = json + '}';
        var data = {json: json};
        $.confirm({
            title: want_to_submit,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/home_page/add-new",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == 'success') {
                                        window.location = base_url + 'Admin/home_page';
                                    } else {
                                        alert(json.message)
                                    }
                                } catch (e) {
                                    alert(e)
                                }

                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });

    });
    $('#SubmitFilter').click(function () {
        var json = '';
        json = json + '{';
        json = json + '"name":"' + $('#name').val() + '",';
        json = json + '"archive":"' + $('#Status').val() + '",';
        json = json + '"image":"' + $('.common_file').attr('file_name') + '",';
        json = json + '"FilterId":"' + $('#FilterId').val() + '"';
        json = json + '}';
        var data = {json: json};
        $.confirm({
            title: want_to_submit,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/filter/add-new",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == 'success') {
                                        window.location = base_url + 'Admin/filter';
                                    } else {
                                        alert(json.message)
                                    }
                                } catch (e) {
                                    alert(e)
                                }

                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });

    });
    $('#SubmitCommission').click(function () {
        var json = '';
        json = json + '{';
        json = json + '"percentage":"' + $('#commission').val() + '",';
        json = json + '"store_type":"' + $('#store_type').val() + '",';
        json = json + '"Id":"' + $('#Id').val() + '",';
        json = json + '"status":"' + $('#Status').val() + '"';
        json = json + '}';
        var data = {json: json};

        $.confirm({
            title: want_to_submit,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/commission/add-new",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == 'success') {
                                        window.location = base_url + 'Admin/commission';
                                    } else {
                                        alert(json.message)
                                    }
                                } catch (e) {
                                    alert(e)
                                }

                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });

    });

    $('#SubmitSponsored').click(function () {
        var json = '';
        json = json + '{';
        json = json + '"archive":"' + $('#HomeStatus').val() + '",';
        json = json + '"status":"' + $('#HomeStatus').val() + '",';
        json = json + '"image":"' + $('.common_file').attr('file_name') + '",';
        json = json + '"Id":"' + $('#Id').val() + '"';
        json = json + '}';
        var data = {json: json};

        $.confirm({
            title: want_to_submit,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/sponsored_ad/add-new",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == 'success') {
                                        window.location = base_url + 'Admin/sponsored_ad';
                                    } else {
                                        alert(json.message)
                                    }
                                } catch (e) {
                                    alert(e)
                                }

                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });

    });
    $('#SubmitAds').click(function () {
        var json = '';
        json = json + '{';
        json = json + '"status":"' + $('#HomeStatus').val() + '",';
        json = json + '"image":"' + $('.common_file').attr('file_name') + '",';
        json = json + '"Id":"' + $('#Id').val() + '"';
        json = json + '}';
        var data = {json: json};
        $.confirm({
            title: want_to_submit,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/ads/add-new",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == 'success') {
                                        window.location = base_url + 'Admin/ads';
                                    } else {
                                        alert(json.message)
                                    }
                                } catch (e) {
                                    alert(e)
                                }

                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });

    });

    $('.update_position').click(function () {
        var position = $(this).closest('tr').find('.position').val();
        var tableid = $(this).attr('tableid');
        var table = $(this).attr('table');
        var store_type = $(this).attr('storetype');
        var data = {
            function: 'update_position'
            , id: tableid
            , table: table
            , position: position
            , store_type: store_type
        };
        $.confirm({
            title: want_to_update,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/Helper"
                            , async: true
                            , type: 'POST'
                            , data: data
                            , success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    $('#success-message').html(json.message);
                                    $('.alert-success').show();
                                    $('.alert-success').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                        window.location = '';
                                    }, 2000);
                                }
                                else {
                                    $('.alert-danger').show();
                                    $('#error-message').html(json.message)
                                    $('.alert-danger').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                        window.location = '';
                                    }, 2000);
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-danger',
                    action: function () {
                    }
                },
            }
        });
    });
    $('.reset_position').click(function () {
        var tableid = $(this).attr('tableid');
        var table = $(this).attr('table');
        var data = {
            function: 'reset_position'
            , id: tableid
            , table: table
        };
        $.confirm({
            title: reset_position,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/Helper"
                            , async: true
                            , type: 'POST'
                            , data: data
                            , success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    $('#success-message').html(json.message);
                                    $('.alert-success').show();
                                    $('.alert-success').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                        window.location = '';
                                    }, 2000);
                                }
                                else {
                                    $('.alert-danger').show();
                                    $('#error-message').html(json.message)
                                    $('.alert-danger').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                        window.location = '';
                                    }, 2000);
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-danger',
                    action: function () {
                    }
                },
            }
        });
    });

    $('#SubmitNewUser').click(function () {

        var json = '';
        json = json + '{';
        if ($('#UserName').val() == '') {
            $('#UserName').focus();
            $('#UserName').css('border-color', 'red');
            return false;
        } else {
            $('#UserName').css('border-color', '');
            json = json + '"name":"' + $('#UserName').val() + '",';
        }

        if ($('#UserEmail').val() == '') {
            $('#UserEmail').focus();
            $('#UserEmail').css('border-color', 'red');
            return false;
        } else {
            $('#UserEmail').css('border-color', '');
            json = json + '"email":"' + $('#UserEmail').val() + '",';
        }


        if ($('#UserPassword').val() == '') {
            // $('#UserPassword').focus();
            // $('#UserPassword').css('border-color', 'red');
            // return false;
        } else {
            $('#UserPassword').css('border-color', '');
            json = json + '"password":"' + $('#UserPassword').val() + '",';
        }
        json = json + '"mobile":"' + $('#mobile').val() + '",';
        json = json + '"LoginType":"' + $('#UserType').val() + '",';
        json = json + '"status":"' + $('#UserStatus').val() + '",';
        json = json + '"UserId":"' + $('#UserId').val() + '"';



        json = json + '}';
        var data = {json: json};
        var baseurl = $('#baseurl').val();
        $.confirm({
            title: want_to_submit,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/AddNewUser",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == 'success') {
                                        window.location = base_url + 'Admin/user_management';
                                    } else {
                                        alert(json.message)
                                    }
                                } catch (e) {
                                    alert(e)
                                }

                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });

    });
    $('#deliveryboy_id').on('change', function () {
        var orderId = $('#orderId').val();
        var deliveryboy_id = $('#deliveryboy_id').val();
        var user_id = $('#user_id').val();
        var vendor_id = $('#vendor_id').val();
        var login_id = $('#login_id').val();
        var data = {function: 'assign_deliveryboy', orderId: orderId, deliveryboy_id: deliveryboy_id, user_id: user_id, vendor_id: vendor_id, login_id: login_id};
        $.confirm({
            title: want_to_submit,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-green',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/Helper",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                if (jQuery.parseJSON(data)) {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == true) {
                                        window.location = '';
                                    } else {
                                        alert(json.message)
                                    }
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-warning',
                    action: function () {
                    }
                },
            }
        });
    });
    $('#change_status').click(function () {
        var order_status = $('#order_status').val();
        var order_id = $('#order_id').val();
        var user_id = $('#user_id').val();
        var vendor_id = $('#vendor_id').val();

        if ($('#payment_status').val() != undefined) {
            var payment_status = $('#payment_status').val();
        } else {
            var payment_status = ''
        }

        var data = {function: 'changeOrderStatus', order_status: order_status, order_id: order_id, payment_status: payment_status, user_id: user_id, vendor_id: vendor_id};

        $.ajax({
            url: base_url + "Admin/helper",
            type: 'POST',
            data: data,
            async: false,
            success: function (data) {

                setTimeout(function () {
                    $('.alert-danger').slideUp(500);
                    window.location = '';
                }, 1000);
            }
        });
    });

    $('#SubmitLoyality').click(function () {
        var json = '';
        json = json + '{';
        var point = $('#point');
        var point_amt = $('#point_amt');
        var point_limit_for_redeem = $('#point_limit_for_redeem');
        if (point.val() == '') {
            point.focus();
            point.css('border-color', 'red');
            return false;
        } else {
            point.css('border-color', '');
            json = json + '"point":"' + point.val() + '",';
        }
        if (point_amt.val() == '') {
            point_amt.focus();
            point_amt.css('border-color', 'red');
            return false;
        } else {
            point_amt.css('border-color', '');
            json = json + '"point_amt":"' + point_amt.val() + '",';
        }
        if (point_limit_for_redeem.val() == '') {
            point_limit_for_redeem.focus();
            point_limit_for_redeem.css('border-color', 'red');
            return false;
        } else {
            point_limit_for_redeem.css('border-color', '');
            json = json + '"point_limit_for_redeem":"' + point_limit_for_redeem.val() + '",';
        }
        json = json + '"status":"' + $('#Status').val() + '",';
        json = json + '"Id":"' + $('#Id').val() + '"';
        json = json + '}';
        var data = {
            json: json,
            redm_point: $('#redm_point').val(),
            redm_amt: $('#redm_amt').val(),
        };

        $.confirm({
            title: want_to_submit,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "admin/loyality_points/add-new",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {

                                    var json = jQuery.parseJSON(data);
                                    if (json.status == 'success') {
                                        $('#success-message').html(json.message);
                                        $('.alert-success').show();
                                        $('.alert-success').slideDown(500);
                                        setTimeout(function () {
                                            $('.alert-danger').slideUp(500);
                                            window.location = base_url + 'admin/loyality_points';
                                        }, 2000);

                                    } else {
                                        $('.alert-danger').show();
                                        $('#error-message').html(json.message)
                                        $('.alert-danger').slideDown(500);
                                        setTimeout(function () {
                                            $('.alert-danger').slideUp(500);
                                        }, 2000);
                                    }
                                } catch (e) {
                                    alert(e);
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: cancel,
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });

    $('#update_details').click(function () {
        var json = {};
        var user = {};
        json['charges_type'] = $("input:radio[name=charges_type]:checked").val();
        json['dlvry_chrge_frst_km'] = $('#dlvry_chrge_frst_km').val();
        json['service_charge'] = $('#dlvry_chrge').val();
        json['dlvry_chrge_perkm'] = $('#dlvry_chrge_perkm').val();
        if (json['charges_type'] == '0') {
            json['service_charge'] = $('#dlvry_chrge_ch').val();
        }
        json['commission'] = $('#commission').val();
        json['restaurant_name'] = $('#restaurant_name').val();
        json['restaurant_name'] = $('#restaurant_name').val();
        json['isDelivery'] = $('#isDelivery').is(':checked') ? '1' : '0';

        json['self_pickup'] = $('#self_pickup').is(':checked') ? '1' : '0';
        json['fastdelvry'] = $('#fastdelvry').is(':checked') ? '1' : '0';

        json['delivery_hours_st'] = $('#delivery_hours_st').val();
        json['delivery_hours_et'] = $('#delivery_hours_et').val();


        user['delivery_radius'] = $('#delivery_radius').val();
            user['free_delivery'] = $('#free_delivery').val();
        var vendor_id = $('#vendor_id').val();

 user['store_type'] = $('#store_type').val();
        user['area'] = $('#autocomplete').val();
        user['latitude'] = $('#lat').val();
        user['longitude'] = $('#lng').val();
        if (user['area'] == '' || user['latitude'] == '' || user['longitude'] == '') {
            alert('please select location');
            return false;
        }
        user['status'] = $('#status').val();


        var data = {function: 'updateDeliveyCharge', json: json, user: user, vendor_id: vendor_id};
        data['cuisine_type'] = $('#cuisine_type').val();
        $.ajax({
            url: base_url + "Admin/helper",
            type: 'POST',
            data: data,
            async: false,
            success: function (data) {
                if (jQuery.parseJSON(data)) {
                    var json = jQuery.parseJSON(data);
                    if (json.status == true) {
                        toastr.success(json.message, 'Success Alert', {timeOut: 5000})
                        // AlertHeader(json.message, json.status);
                        setTimeout(function () {
                            window.location = '';
                        }, 2000);
                    }
                }

                // setTimeout(function () {
                //     $('.alert-danger').slideUp(500);
                //     window.location = '';
                // }, 1000);
            }
        });

    })
    $('#business_location').click(function () {
        $('#business_location_map').modal('show');
    })


});
