if (location.protocol !== "https:") {
    location.protocol = "https:";
}
$(document).ready(function () {
    window.setInterval(function () {
        ADMINCMN.autochatNotifaction();
    }, 5000);
    window.setInterval(function () {
        ADMINCMN.headerAdminNotifaction();
    }, 10000);
  ADMINCMN.headerAdminNotifaction();
    if (segment2 == 'vendor_info' || segment2 == 'add_deliveryboy' || segment2 == 'citybyorder' || segment2 == 'orderlocation') {
        var input = document.getElementById('business-location');
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            $('#latitude').val(place.geometry.location.lat());
            $('#longitude').val(place.geometry.location.lng());
        });
    }
    $('#autocomplete').click(function () {
        $('#party_venue_map').modal('show');
    });
    $('body').on('click', '.autodeleteTemp', function () {
        var condjson = $(this).attr('condjson');
        var dbtable = $(this).attr('dbtable');
        var removefile = $(this).attr('removefile');
        var data = {function: 'autodelete', condjson: condjson, dbtable: dbtable, removefile: removefile};
        var cmessage = $(this).attr('cmessage') != undefined ? $(this).attr('cmessage') : 'Are you sure want to delete!.';
        $.confirm({
            title: cmessage,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: submit,
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/Helper",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == true) {
                                        window.location = '';
                                    }
                                } catch (e) {

                                }
                            }
                        });
                    }
                },
                close: {
                    text: cancel,
                    btnClass: 'btn-red',
                }
            }
        });
    });
    $('body').on('click', '.autoupdate', function () {
        var updatejson = $(this).attr('updatejson');
        var condjson = $(this).attr('condjson');
        var dbtable = $(this).attr('dbtable');
        var data = {function: 'autoupdate', updatejson: updatejson, condjson: condjson, dbtable: dbtable};
        var cmessage = $(this).attr('cmessage') != undefined ? $(this).attr('cmessage') : 'Are you sure want to delete!.';
        $.confirm({
            title: cmessage,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/Helper",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == true) {
                                        window.location = '';
                                    }
                                } catch (e) {

                                }
                            }
                        });
                    }
                },
                close: {
                    text: 'Cancel',
                    btnClass: 'btn-red',
                }
            }
        });
    });
    if (Method == 'vendor_position') {
        $(function () {
            $("#sortable").sortable({
                stop: function (e, ui) {
                    console.log($.map($(this).find('li'), function (el) {
                        return el.id + ' = ' + $(el).index();
                    }));
                }
            });

        });
    }
});
var ADMINCMN = (function () {
    var fn = {};
    fn.assignRiderbyVendor = function () {
        var riderId = $('#assign_rider').val();
        var orderId = $('#order_id').val();
        var data = {status: 'assignRiderbyVendor', riderId: riderId, orderId: orderId};
        data['commission_update'] = true;
        $.ajax({
            method: "POST",
            url: base_url + "Frontend/Helper",
            data: data,
            async: false,
            success: function (res) {
                var json = JSON.parse(res);
                alert(json.message);
                if (json.status == true) {

                } else {

                }
            },
            error: function (xhr) {
                alert("Error occured.please try again");
            }
        });
    }
    fn.deliveryboyUpdateCommission = function () {
        var cmamount = $('#cmamount').val();
        var dboyid = $('#dboyid').val();
        if ($('#cmamount').val() == '') {
            $('#cmamount').focus();
            return false;
        }
        var data = {function: 'deliveryboyUpdateCommission', cmamount: cmamount, dboyid: dboyid};
        $.ajax({
            method: "POST",
            url: base_url + "Admin/Helper",
            data: data,
            async: false,
            success: function (res) {
                var json = JSON.parse(res);
                if (json.status == true) {
                    alert(json.message);
                }
            },
            error: function (xhr) {
                alert("Error occured.please try again");
            }
        });
    }
    fn.updatedeleverycharges = function () {
        var charges_type = $("input:radio[name=charges_type]:checked").val();
        $('#Flexiblecharges,#Fixedcharges').hide();
        if (charges_type == '1') {
            $('#Flexiblecharges').show();
        } else if (charges_type == '0') {
            $('#Fixedcharges').show();
        }
    }
    fn.updateRiderCommission = function (e) {
        var dboyid = $(e).attr('dboyid');
        var rider_commission_id = $(e).val();

        var data = {status: 'updateRiderCommissionRRRR', dboyid: dboyid, rider_commission_id: rider_commission_id};
        $.ajax({
            method: "POST",
            url: base_url + "Frontend/Helper",
            data: data,
            async: false,
            success: function (res) {
                var json = JSON.parse(res);

                if (json.status == true) {
                    window.location = '';
                } else {

                }
            },
            error: function (xhr) {
                alert("Error occured.please try again");
            }
        });
    }
    fn.riderCommissingOpenModel = function (e) {
        var json = $(e).attr('json');
        $('#idsnamed,#com_title,#km,#commission,#additional_commission').val('');
        if (json != undefined && json != '') {
            json = jQuery.parseJSON(atob(json));
            $('#idsnamed').val(json.id);
            $('#com_title').val(json.com_title);
            $('#km').val(json.km);
            $('#commission').val(json.commission);
            $('#additional_commission').val(json.additional_commission);
            console.log(json);
        }
        $('#myModal').modal('show');
    }
    fn.vendororderviewupdate = function (e) {

        var orderview = [];
        $.map($('#sortable').find('li'), function (el) {
            var d = {};
            d['id'] = el.id;
            d['orderview'] = $(el).index();
            orderview.push(d);
            // console.log(el.id);
            // return el.id + ' = ' + $(el).index();
        })

        var data = {status: 'vendororderviewupdate', orderview: orderview};

        $.ajax({
            method: "POST",
            url: base_url + "Frontend/Helper",
            data: data,
            async: false,
            success: function (res) {
                var json = JSON.parse(res);
                if (json.status == true) {
                    window.location = '';
                } else {

                }
            },
            error: function (xhr) {
                alert("Error occured.please try again");
            }
        });
    }
    fn.autochatNotifaction = function () {
        var data = {status: 'autochatNotifaction', iddddp: 1};
        $.ajax({
            method: "POST",
            url: base_url + "Frontend/Helper",
            data: data,
            async: false,
            success: function (res) {
                var json = JSON.parse(res);
                if (json.status == true) {
                    //  console.log(json.list);
                    for (var i = 0; i < json.list.length; i++) {

                        var d = json.list[i];
                        $.confirm({
                            title: '<div class="newmessage">New message - ' + d.name + '!</div>',
                            content: d.message,
                            buttons: {
                                Ok: {
                                    text: 'Ok',
                                    'chat_id': d.chat_id,
                                    action: function (action) {
                                        var chat_id = action.chat_id;
                                        var data = {status: 'readyMychat', chat_id: chat_id};
                                        $.ajax({
                                            method: "POST",
                                            url: base_url + "Frontend/Helper",
                                            data: data,
                                            async: false,
                                            success: function (res) {
                                                var json = JSON.parse(res);
                                                if (json.status == true) {
                                                    if (Method != 'chat') {
                                                        window.location = base_url + 'admin/chat';
                                                    }
                                                    console.log('readed!');
                                                } else {

                                                }
                                            },
                                            error: function (xhr) {
                                                alert("Error occured.please try again");
                                            }
                                        });

                                    }
                                }

                            }
                        });
                    }
                    ADMINCMN.orderbrowsernotification(json.notifications);
                } else {

                }
            },
            error: function (xhr) {
                alert("Error occured.please try again");
            }
        });
    }
    fn.headerAdminNotifaction = function () {
        var data = {status: 'headerAdminNotifaction'};
        $.ajax({
            method: "POST",
            url: base_url + "Frontend/Helper",
            data: data,
            async: true,
            success: function (res) {
                var json = JSON.parse(res);
                if (json.status == true) {
                    $('#notilistdf').html(json.HTML);
                    $('headerNotifactionCount').html(json.countlist);
                } else {

                }
            },
            error: function (xhr) {
                alert("Error occured.please try again");
            }
        });
    }
    fn.admindiscountTypechnage = function () {
        var discount_type = $('#discount_type').val();
        if (discount_type == '1') {
            $('#distypemsg').html('Enter amount');
            $('#maxdisdiv').hide();
        } else {
            $('#distypemsg').html('Discount in %');
            $('#maxdisdiv').show();
        }
    }
    fn.orderbrowsernotification = function (notifications) {
        for (var i = 0; i < notifications.length; i++) {
            var d = notifications[i];
            ADMINCMN.notifyMe('New order -  ' + d.orderno, base_url + 'admin/OrderDetails/' + btoa(d.id));

        }
        //console.log(notifications);
    }
    fn.notifyMe = function (message, Url) {
        if (!("Notification" in window)) {
            return false;
        }
        else if (Notification.permission === "granted") {
            sendNotifactin(message, Url);
        }
        else if (Notification.permission !== "denied") {
            Notification.requestPermission().then(function (permission) {
                if (permission === "granted") {
                    sendNotifactin(message, Url);
                }
            });
        }
        function sendNotifactin(message, Url) {
            var notification = new Notification(message);
            notification.onclick = function (event) {
                event.preventDefault(); // prevent the browser from focusing the Notification's tab
                window.open(Url, '_blank');
            };

        }
    }
    return fn;
})();

