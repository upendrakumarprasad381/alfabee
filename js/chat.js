$(document).ready(function () {
    chat.init();
    $(".chat-input").click(function () {
        var sen = $('#sender_id').val();
        if (sen == '') {
            chat.getGuestUserEmail();
            return false;
        }
    });
});
var last_chat_id = 0;
var chat = (function () {
    var sender_id = $('#sender_id').val();
    var receiver_id = $('#receiver_id').val();
    var user_name = '';
    var IntervalId = '';
    var fn = {};
    fn.init = function () {
        chat.updatechatHistory();
        window.clearInterval(IntervalId);
        IntervalId = window.setInterval(function () {
            chat.updatechatHistory();
        }, 5000);
    }
    fn.opendialogueBox = function (e) {
        $('#chatboxdialogue').removeClass('chatbox-min');
        receiver_id = $(e).attr('receiverId');
        user_name = $(e).attr('user_name');
        $('#userchatName').html(user_name);
        last_chat_id = 0;
        $('.chat-messages').empty();
        $('#chatbord').show();
        chat.init();
    }, fn.closedialogueBox = function () {
        //$('#chatbord').hide();
        $('#chatboxdialogue').addClass('chatbox-min');
    }
    fn.updatechatHistory = function () {
        var user_name = $('#user_name').val();
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": base_url + "Services/chat_list",
            "method": "POST",
            "headers": {
                "user-agents": "com.alfabee.com",
                "authtoken": "Alwafaa123",
                "user-id": "50",
                "devicetoken": "5558",
                "cache-control": "no-cache",
                "postman-token": "7eb3b02c-006d-ec2f-f3d1-4ac331d9586b"
            },
            "data": "{\"sender_id\":\"" + sender_id + "\",\"receiver_id\":\"" + receiver_id + "\",\"chat_id\":\"" + last_chat_id + "\"}"
        }

        $.ajax(settings).done(function (response) {
            var json = response;
            // alert(last_chat_id)
            var chat_list = json.result.chat_list;
            for (var i = 0; i < chat_list.length; i++) {
                var d = chat_list[i];
                var html = '';
                if (d.sender_id != receiver_id) {
                    if (d.message_type == '1') {
                        html = html + ' <div class="message-box-holder">';
                        html = html + '   <div class="message-sender">'
                        html = html + ' <audio controls>';
                        html = html + '<source src="' + d.message + '" type="audio/mpeg">';
                        html = html + ' Your browser does not support the audio element.';
                        html = html + '</audio>';
                        html = html + ' </div>';
                        html = html + ' </div>';
                    } else {
                        html = html + '<div class="message-box-holder">';
                        html = html + '      <div class="message-box">' + d.message + '</div>';
                        html = html + ' </div>';
                    }
                } else {
                    if (last_chat_id != 0) {
                        $.confirm({
                            title: '<div class="newmessage">New message!</div>',
                            content: d.message,
                        });
                    }
                    html = html + ' <div class="message-box-holder">';
                    html = html + '   <div class="message-sender">' + user_name + '</div>';
                    html = html + '   <div class="message-box message-partner">' + d.message + '</div>';
                    html = html + ' </div>';
                }
                $('.chat-messages').append(html);
            }
            last_chat_id = json.result.last_chat_id > 0 ? json.result.last_chat_id : last_chat_id;
            if (chat_list.length) {
                chat.chatScrolldown();
            }
        });
    }
    fn.send_message = function () {
        var chatInput = $('.chat-input').val();
        if (chatInput != '') {
            if (sender_id == '') {
                chat.getGuestUserEmail();
                return false;
            }
            var html = '';
            html = html + '<div class="message-box-holder">';
            html = html + '      <div class="message-box">' + chatInput + '</div>';
            html = html + ' </div>';
            $('.chat-input').val('');
            $('.chat-messages').append(html);
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": base_url + "Services/send_message",
                "method": "POST",
                "headers": {
                    "user-agents": "com.alfabee.com",
                    "authtoken": "Alwafaa123",
                    "user-id": "50",
                    "devicetoken": "5558",
                    "cache-control": "no-cache",
                    "postman-token": "f66a2c51-d49e-e4a5-31a4-caf863a38f18"
                },
                "data": "{\"sender_id\":\"" + sender_id + "\",\"receiver_id\":\"" + receiver_id + "\",\"message\":\"" + chatInput + "\"}"
            }

            $.ajax(settings).done(function (response) {
                var json = response;
                last_chat_id = json.result.last_chat_id;
                chat.chatScrolldown();
            });
        }
    }
    fn.refreshUserlist = function () {
        var data = {function: 'refreshChatlist'};
        $.ajax({
            data: data,
            type: 'POST',
            async: true,
            url: base_url + "Admin/Helper",
            success: function (data) {
                $('.inbox_chat').html(data);
            }
        });
    }
    fn.autosend_message = function () {
        var key = window.event.keyCode;
        if (key === 13) {
            chat.send_message();
            return false;
        }
        else {
            return true;
        }
    }
    fn.chatScrolldown = function () {
        $('.chat-messages').scrollTop($('.chat-messages')[0].scrollHeight);
    }
    fn.getGuestUserEmail = function () {
        if ($('.chat-input').val() != '') {
            $('.chat-input').val('');
        }

        $.confirm({
            title: 'Enter your email id!',
            content: '' +
                    '<form action="" class="formName">' +
                    '<div class="form-group">' +
                    '<input type="text" placeholder="Email" class="name form-control" required />' +
                    '</div>' +
                    '</form>',
            buttons: {
                formSubmit: {
                    text: 'Submit',
                    btnClass: 'btn-blue',
                    action: function () {
                        var name = this.$content.find('.name').val();
                        if (!name) {
                            $.alert('provide a valid email');
                            return false;
                        }
                        if (!isValidEmail(name)) {
                            $.alert('provide a valid email');
                            return false;
                        }
                        var data = {status: 'getnewuserlistforchat', name: name};
                        $.ajax({
                            data: data,
                            type: 'POST',
                            async: true,
                            url: base_url + "Frontend/Helper",
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == true) {
                                        sender_id = json.user_id;
                                        $('#sender_id').val(json.user_id);
                                    }
                                } catch (e) {
                                    alertSimple('somting went wrong');
                                }
                            }
                        });
                    }
                },
                cancel: function () {
                    $('#sender_id').focus();
                },
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    }
    return fn;
})();
