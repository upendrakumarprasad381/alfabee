var CallBack = GetLoginCallBackURl();
if (sessionValue == 'ar')
{
    var submit = 'تأكيد';
    var cancel = 'إلغاء';
    var cancel_this_order = 'هل أنت متأكد من إلغاء الطلب؟';
    var why_cancel = 'لماذا الإلغاء';
    var order_took_to_long = 'استغرق الطلب وقتا طويلا ';
    var changed_my_mind = 'غيرت رأيي';
    var price_high = 'السعر مرتفع جدا';
    var found_somewhere_else = 'لقد وجدت أنها أرخص في مكان آخر';
    var other = 'شيء آخر';
    var other_reason = 'يرجى تحديد السبب إذا كان الآخرون';
    var enter_location = 'يرجى إدخال العنوان ';
    var fullname_required = 'الاسم الكامل (إجباري)';
    var businessname_required = 'اسم المشروع (إجباري)';
    var email_required = 'البريد الالكتروني (إجباري)';
    var phone_required = 'رقم الهاتف ( إجباري) ';
    var city_required = 'المدينة ( إجباري)';
    var area_required = 'المنطقة ( إجباري)';
    var street_required = 'اسم الشارع ( إجباري)';
    var password_required = 'رقم المرور ( إجباري)';
    var password_validation = 'أحرف على الاقل، على ان يتكون من أحرف وأرقام 8 ';
    var confirm_password = 'تأكيد كلمة المرور ';
    var password_match = 'كلمة المرور يجب أن تتطابق';
    var cusine_type = 'يرجى إختيار نوع المطبخ';
    var accept_terms = 'اقبل الشروط والأحكام '
    var mobile_validation = 'يجب أن يبدأ رقم الجوال بـ 5 وأن يتكون من 9 أرقام';
    var loction_required = 'العنوان ( إجباري)';
    var select_building = 'نوع المبنى ';
    var building_name_required = 'اسم المبنى ( إجباري) ';
    var apartment_name_required = 'رقم الشقة( إجباري) ';
    var house_no_required = 'رقم المنزل ( إجباري)';
    var office_name_required = 'اسم المكتب( إجباري)';
    var floor_required = 'رقم الطابق';
    var select_occasion = 'يرجى اختيار المناسبة '
    var select_party_date = 'يرجى تحديد تاريخ المناسبة '
    var no_of_adults = 'عدد البالغين ';
    var no_of_childrens = 'عدد الأطفال ';
    var select_party_time = 'يرجي اختيار وقت المناسبة ';
    var enter_venue = 'الرجاء إدخال المكان';
    var select_cuisine = 'يرجى إختيار نوع المطبخ';
    var add_new_address = 'أضف عنوان جديد ';
    var edit = 'تعديل ';
    var pinned_location_outside = 'للأسف، لا يوجد لدينا توصيل  في هذة المنطقة حاليا ';
    var select_rating = 'يرجى اختيار تقييم';
    var enter_comments = 'يرجى إضافة تعليق';
    var delivery_from = 'التسليم من';
    var delivery_to = 'التسليم إلى';
    var delivery_location = 'عنوان الاستلام ';
    var abudhabi = 'أبو ظبي';
    var ajman = 'عجمان';
    var dubai = 'دبي';
    var fujairah = 'الفجيرة';
    var ras_al_khaimah = 'رأس الخيمة';
    var sharjah = 'الشارقة';
    var umm_al_quwain = 'أم القيوين';
    var delivery = 'Please select delivery option';
} else {
    var submit = 'SUBMIT';
    var cancel = 'CANCEL';
    var cancel_this_order = 'Are you sure want to cancel this order ?';
    var why_cancel = 'Why Cancel';
    var order_took_to_long = 'The order took too long';
    var changed_my_mind = 'I changed my mind';
    var price_high = 'The price is too high';
    var found_somewhere_else = 'I found it cheaper somewhere else';
    var other = 'Other';
    var other_reason = 'Please specify the reason if others';
    var enter_location = 'Please enter a location';
    var fullname_required = 'Full Name Required';
    var businessname_required = 'Business Name Required';
    var email_required = 'Email Required';
    var phone_required = 'Phone number Required';
    var city_required = 'City Required';
    var area_required = 'Location Required';
    var street_required = 'Street name Required';
    var password_required = 'Password Required';
    var password_validation = 'Min 8 Characters,Atleast one letter,Atleast one number';
    var confirm_password = 'Confirm password required';
    var password_match = 'Both passwords must match';
    var cusine_type = 'Please select store type';
    var delivery = 'Please select delivery option';
    var accept_terms = 'Please accept terms & condition';
    var mobile_validation = 'Mobile number should consist of 10 digits long';
    var loction_required = 'Location required';
    var select_building = 'Please select building type';
    var building_name_required = 'Building Name Required';
    var apartment_name_required = 'Apartment Name Required';
    var house_no_required = 'House number required';
    var office_name_required = 'Office name required';
    var floor_required = 'Floor required';
    var select_occasion = 'Please select occasion'
    var select_party_date = 'Please select party date'
    var no_of_adults = 'Please enter no.of adults';
    var no_of_childrens = 'Please enter no.of childrens';
    var select_party_time = 'Please select party time';
    var enter_venue = 'Please enter venue';
    var select_cuisine = 'Please select cuisine';
    var add_new_address = 'Add New Address';
    var edit = 'Edit Address';
    var pinned_location_outside = 'Unfortunately, the pinned location is outside delivery zone.';
    var select_rating = 'Please select a rating';
    var enter_comments = 'Please enter comments';
    var delivery_from = 'Delivery From';
    var delivery_to = 'Delivery To';
    var delivery_location = 'Delivery Location';
    var abudhabi = 'Abu Dhabi';
    var ajman = 'Ajman';
    var dubai = 'Dubai';
    var fujairah = 'Fujairah';
    var ras_al_khaimah = 'Ras Al Khaimah';
    var sharjah = 'Sharjah';
    var umm_al_quwain = 'Umm Al Quwain';
}

function LoginAndCallBack(CallBackURL) {
    var Status = SetLoginCallBackURl(CallBackURL);
    sessionStorage.removeItem('vendor_id');
    if (Status == true) {
        window.location = base_url + 'login_signup/callback';
    }
}
function SetLoginCallBackURl(CallBackURL) {
    if (localStorage['CallBackURL'] = CallBackURL) { //assigning url to temporary session
        return true;
    }
}
function GetLoginCallBackURl() {
    return localStorage['CallBackURL'];
}
function isValidPassword(password) {
    return new RegExp('(?=.*[a-zA-Z])(?=.*\\d).{8,}').test(password);
}
function isValidEmail(email) {
    return /^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i.test(email) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(email);
}
function isValidMobile(mobile)
{
    var char_leng = mobile.length;
    if(char_leng < 10) {
        return false;
    } else {
    //return /^((?:3|4|5|6|7|9|10)[0-10]{7,})$/.test(mobile);
    return true;
    }
}

// function googleTranslateElementInit() {
//     new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ar'}, 'google_translate_element');
// }
if (Method == 'PaymentInvocie' || Method == 'orderHistory') {
    setTimeout(function () {
        window.location = '';
    }, 35000);
}
$('#mobile_number, #landphone_no, #phone, #phone_number,#delivery_radius,#no_of_people').on('input blur paste', function () {
    $(this).val($(this).val().replace(/\D/g, ''))
})

$('#datetimepicker3').datetimepicker({
    icons:
            {
                up: 'fa fa-angle-up',
                down: 'fa fa-angle-down'
            },
    format: 'HH:mm',
});

$("#datepicker").datepicker({
    minDate: 0,
    dateFormat: 'yy-mm-dd'
});

$("#reservation_date").datepicker({
    minDate: 0,
    dateFormat: 'yy-mm-dd'
});

$("#schedule_date").datepicker({
    minDate: 0,
    dateFormat: 'yy-mm-dd'
});
$("#party_date").datepicker({
    minDate: 0,
    dateFormat: 'yy-mm-dd'
});

$('.delivery_timepicker_from').datetimepicker({
    icons:
            {
                up: 'fa fa-angle-up',
                down: 'fa fa-angle-down'
            },
    format: "HH:mm a",
});
$('.delivery_timepicker_to').datetimepicker({
    icons:
            {
                up: 'fa fa-angle-up',
                down: 'fa fa-angle-down'
            },
    format: "HH:mm a",
});
$('#cuisine').selectpicker();
$('#mobile_number,#phone_number,#phone,#landphone_no').keyup(function () {
    var text = $(this).val();
    if (text.length > 12) {
        $(this).val(text.slice(0, -1));
    }
});
$('input.isDelivery').on('change', function () {
    $('input.isDelivery').not(this).prop('checked', false);
});
$('.isDelivery').on('change', function ()
{
    if ($('input.isDelivery:checked').val() == 1)
    {
        $('.delivery_option').show();
    } else {
        $('.delivery_option').hide();
    }
});
$('#business_type').on('change', function ()
{
    if ($('#business_type').val() == 1)
    {
        $('.cuisine').show();
    } else {
        $('.cuisine').hide();
    }
});

$('#isDelivery').on('change', function ()
{
    if ($('#isDelivery').val() == 1)
    {
        $('.delivery_option').show();
    } else {
        $('.delivery_option').hide();
    }
});

$('body').on('change', 'input[name="deliveryRadio"]', function () {
    if ($(this).val() == 1)
    {
        $('#delivery_address').show();
    } else {
        $('#delivery_address').hide();
    }
});

// $(document).ready(function(){
function add_more()
{
    var html = '';
    var uniqId = Math.floor((Math.random() * 1000000000) + 1);
    var leng = $('.Bankdetails').length;
    html = html + ' <div class="row add_copy">';
    html = html + ' <div class="form-group col-lg-3 col-md-12 col-sm-12 col-xs-12">';
    html = html + ' <label for="exampleInputEmail1">' + delivery_from + '</label>';
    html = html + ' <input type="text" class="form-control timepicker timepicker-no-seconds delivery_timepicker_from" id="delivery_timepicker_from" name="delivery_timepicker_from[]">';
    html = html + ' </div>';
    html = html + ' <div class="form-group col-lg-3 col-md-12 col-sm-12 col-xs-12">';
    html = html + ' <label for="exampleInputEmail1">' + delivery_to + '</label>';
    html = html + ' <input type="text" class="form-control timepicker timepicker-no-seconds delivery_timepicker_to" id="delivery_timepicker_to" name="delivery_timepicker_to[]">';
    html = html + '  </div>';
    html = html + ' <div class="form-group col-lg-3 col-md-12 col-sm-12 col-xs-12">';
    html = html + ' <label for="exampleInputEmail1">' + delivery_location + '</label>';
    html = html + '  <select class="form-control selectpicker emirates" id="emirates" name="emirates[]" multiple data-live-search="true">';
    html = html + ' <option value="1">' + abudhabi + '</option><option value="2">' + ajman + '</option><option value="3">' + dubai + '</option><option value="4">' + fujairah + '</option><option value="5">' + ras_al_khaimah + '</option><option value="6">' + sharjah + '</option><option value="7">' + umm_al_quwain + '</option>';
    html = html + ' </select>';
    html = html + ' </div>';
    html = html + ' <div class="form-group col-lg-1 col-md-12 col-sm-12 col-xs-12">';
    html = html + ' <label class="wd100 __tabnone">&nbsp;</label>';
    html = html + ' <button type="button" class="btn btn-danger btn-block remove"><i class="fa fa-minus-circle" aria-hidden="true"></i>';
    html = html + ' </button>';
    html = html + ' </div>';
    html = html + ' </div>';
    $('.add-more-copy').append(html);
    $('.delivery_timepicker_from').datetimepicker({
        icons:
                {
                    up: 'fa fa-angle-up',
                    down: 'fa fa-angle-down'
                },
        format: "HH:mm a",
    });
    $('.delivery_timepicker_to').datetimepicker({
        icons:
                {
                    up: 'fa fa-angle-up',
                    down: 'fa fa-angle-down'
                },
        format: "HH:mm a",
    });
    $('.selectpicker').selectpicker('refresh');
}


$("body").on("click", ".remove", function () {
    $(this).parents(".add_copy").remove();
});



// $('.FoodSubmit').on('click', function () {
$('#SubmitVendor').on('click', function () {
    var json = '';
    var json = json + '{';
    if ($('#name').val() == '') {
        $('#name').css('border-color', 'red');
        $('#name').focus();
        $('.common_message').css('color', 'red');
        $('.common_message').html(fullname_required);
        return false;
    } else {
        $('#name').css('border-color', '');
        $('.common_message').html('');
        json = json + '"name":"' + $('#name').val() + '",';
    }
    if ($('#restaurant_name').val() == '') {
        $('#restaurant_name').css('border-color', 'red');
        $('#restaurant_name').focus();
        $('.common_message').css('color', 'red');
        $('.common_message').html(businessname_required);
        return false;
    } else {
        $('#restaurant_name').css('border-color', '');
        $('.common_message').html('');
        json = json + '"restaurant_name":"' + $('#restaurant_name').val() + '",';
    }
    if ($('#business_location').val() == '') {
        $('#business_location').css('border-color', 'red');
        $('#business_location').focus();
        $('.common_message').css('color', 'red');
        $('.common_message').html(area_required);
        return false;
    } else {
        $('#business_location').css('border-color', '');
        $('.common_message').html('');
        json = json + '"area":"' + $('#business_location').val() + '",';
    }
    if ($('#business_type').val() == '') {
        $('#business_type').css('border-color', 'red');
        $('#business_type').focus();
        $('.common_message').css('color', 'red');
        $('.common_message').html(cusine_type);
        return false;
    } else {
        $('#business_type').css('border-color', '');
        $('.common_message').html('');
        json = json + '"store_type":"' + $('#business_type').val() + '",';
    }
    if ($('#isDelivery').val() == '') {
        $('#isDelivery').css('border-color', 'red');
        $('#isDelivery').focus();
        $('.common_message').css('color', 'red');
        $('.common_message').html(delivery);
        return false;
    } else {
        $('#isDelivery').css('border-color', '');
        $('.common_message').html('');
        json = json + '"isDelivery":"' + $('#isDelivery').val() + '",';
    }
    if ($('#delivery_radius').val() == '') {
        $('#delivery_radius').css('border-color', 'red');
        $('#delivery_radius').focus();
        $('.common_message').css('color', 'red');
        $('.common_message').html(delivery);
        return false;
    } else {
        $('#delivery_radius').css('border-color', '');
        $('.common_message').html('');
        json = json + '"delivery_radius":"' + $('#delivery_radius').val() + '",';
    }
    // json = json + '"delivery_radius":"' + $('#delivery_radius').val() + '",';
    if ($('#mobile_number').val() == '') {
        $('#mobile_number').css('border-color', 'red');
        $('#mobile_number').focus();
        $('.common_message').css('color', 'red');
        $('.common_message').html(phone_required);
        return false;
    } else {
        $('#mobile_number').css('border-color', '');
        $('.common_message').html('');
        json = json + '"mobile_number":"' + $('#mobile_number').val() + '",';
    }
    if ($('#email_id').val() == '') {
        $('#email_id').css('border-color', 'red');
        $('#email_id').focus();
        $('.common_message').css('color', 'red');
        $('.common_message').html(email_required);
        return false;
    } else {
        if (!isValidEmail($('#email_id').val())) {
            $('#email_id').focus();
            $('#email_id').css('border-color', 'red');
            return false;
        }
        // else {
        $('#email_id').css('border-color', '');
        $('.common_message').html('');
        json = json + '"email":"' + $('#email_id').val() + '",';
        // }
    }


    // if ($('#locality').val() == '') {
    //     $('#locality').css('border-color', 'red');
    //     $('#locality').focus();
    //     $('.register_message').css('color', 'red');
    //     $('.register_message').html(city_required);
    //     return false;
    // } else {
    //     $('#locality').css('border-color', '');
    //     $('.register_message').html('');
    //     // json = json + '"city":"' + $('#locality').val() + '",';
    //     json = json + '"city":"' + $( "#locality option:selected" ).text() + '",';
    // }
    // if ($('#venue').val() == '') {
    //     $('#venue').css('border-color', 'red');
    //     $('#vebue').focus();
    //     $('.register_message').css('color', 'red');
    //     $('.register_message').html(area_required);
    //     return false;
    // } else {
    //     $('#venue').css('border-color', '');
    //     $('.register_message').html('');
    //     json = json + '"area":"' + $('#venue').val() + '",';
    // }
    // if ($('#street').val() == '') {
    //     $('#street').css('border-color', 'red');
    //     $('#street').focus();
    //     $('.register_message').css('color', 'red');
    //     $('.register_message').html(street_required);
    //     return false;
    // } else {
    //     $('#street').css('border-color', '');
    //     $('.register_message').html('');
    //     json = json + '"street":"' + $('#street').val() + '",';
    // }
    if ($('#password').val() == '') {
        $('#password').css('border-color', 'red');
        $('#password').focus();
        $('.common_message').css('color', 'red');
        $('.common_message').html(password_required);
        return false;
    } else {
        if (!isValidPassword($('#password').val())) {
            $('#password').css('border-color', 'red');
            $('#password').focus();
            $('.common_message').css('color', 'red');
            $('.common_message').html(password_validation);
            return false;
        } else {
            $('#password').css('border-color', '');
            json = json + '"password":"' + $('#password').val() + '",';
            $('.common_message').html('');
        }
    }
    if ($('#cpassword').val() == '') {
        $('#cpassword').css('border-color', 'red');
        $('#cpassword').focus();
        $('.common_message').css('color', 'red');
        $('.common_message').html(confirm_password);
        return false;
    } else {
        $('#cpassword').css('border-color', '');
        $('.common_message').html('');
    }
    if ($('#password').val() != $('#cpassword').val()) {
        $('#cpassword').css('border-color', 'red');
        $('#cpassword').focus();
        $('.common_message').css('color', 'red');
        $('.common_message').html(password_match);
        return false;
    } else {
        $('#cpassword').css('border-color', '');
        $('.common_message').html('');
    }

    // if ($('#primary_cuisine_type').val() == '') {
    //     $('#primary_cuisine_type').css('border-color', 'red');
    //     $('#primary_cuisine_type').focus();
    //     $('.register_message').css('color', 'red');
    //     $('.register_message').html(cusine_type);
    //     return false;
    // } else {
    //     $('#primary_cuisine_type').css('border-color', '');
    //     $('.register_message').html('');
    //     json = json + '"primary_cuisine_type":"' + $('#primary_cuisine_type').val() + '",';
    // }
    json = json + '"primary_cuisine_type":"' + $('#primary_cuisine_type').val() + '",';
    json = json + '"service_charge":"' + $('#service_charge').val() + '",';
    // if ($('.about').val() == '') {
    //     $('.about').css('border-color', 'red');
    //     $('.about').focus();
    //     $('.register_message').css('color', 'red');
    //     $('.register_message').html('Description Required');
    //     return false;
    // } else {
    //     $('.about').css('border-color', '');
    //     $('.register_message').html('');
    //     json = json + '"about":"' + $('.about').val() + '",';
    // }
    // var LogoImages = $(".business_logo").map(function () {
    //     return $(this).attr('newimage')
    // }).get();
    // var LicenseImages = $(".trade_license").map(function () {
    //     return $(this).attr('newimage')
    // }).get();

    // // json = json + '"owner":"' + $('.owner').val() + '",';
    // var english = /^[A-Za-z0-9]*$/;
    // if($('#sublocality_level_1').val()!='' && english.test($('#sublocality_level_1').val()))
    // {
    //     json = json + '"location":"' + $('#sublocality_level_1').val() + '",';
    // }else{
    //     json = json + '"location":"' + $('.get_location').val() + '",';
    // }
    // var delivery_from = $('input[id^="delivery_timepicker_from"]').map(function() { return $(this).val(); }).get();
    // var delivery_to = $('input[id^="delivery_timepicker_to"]').map(function() { return $(this).val(); }).get();
    // // var emirate = $('select[id^="emirate"]').map(function() { return $(this).val(); }).get();

    // var emirate = $('select[id^="emirate"]').map(function() { return {
    //     value: $(this).val()
    // }
    // }).get();


    // json = json + '"approx_location":"' + $('#approx_location').val() + '",';
    json = json + '"latitude":"' + $('#lat').val() + '",';
    json = json + '"longitude":"' + $('#lng').val() + '",';
    // json = json + '"isDelivery":"' + $('input.isDelivery:checked').val() + '",';
    // json = json + '"LogoImages":"' +LogoImages + '",';
    // json = json + '"LicenseImages":"' + LicenseImages + '"';
    // json = json + '"about":"' + $('.about').val() + '"';
    json = json + '"isDelivery":"' + $('#isDelivery').val() + '"';
    json = json + '}';

    var data = {
        json: json,
        // delivery_from: delivery_from,
        // delivery_to: delivery_to,
        // emirate:emirate,


    };
// console.log(data);exit;
    $("#loader").css('display', 'block');

    $.ajax({
        url: base_url + "Register/loginregistration/vendor_register",
        type: 'POST',
        // async: false,
        data: data,
        // data: "json=" + encodeURIComponent(json),
        beforeSend: function () {
            $("#loader").css('display', 'block');

        },
        success: function (response) {

            try {
                //   $("#loader").css('display','block');
                var resp = jQuery.parseJSON(response);

                if (resp.status == 'success') {
                    $("#loader").css('display', 'none');
                    // toastr.success(resp.message, 'Success Alert', {timeOut: 5000})
                    $('.common_message').css('color', 'green');
                    $('.common_message').html(resp.message);
                    setTimeout(function () {
                        // window.location= base_url+'Login/Vendor';
                        window.location = base_url;
                    }, 3000);
                } else {
                    $("#loader").css('display', 'none');
                    // $(".loader").css('display','none');
                    $('.common_message').css('color', 'red');
                    $('.common_message').html(resp.message);
                    toastr.error(resp.message, 'Error Alert', {timeOut: 5000})
                }
            } catch (e) {
                alert(e.message)
            }
        }
    });
    // $("#loader").css('display','none');
});

$('#SubmitRider').on('click', function () {

    if ($('#rider_name').val() == '') {
        $('#rider_name').css('border-color', 'red');
        $('#rider_name').focus();
        return false;
    } else {
        $('#rider_name').css('border-color', '');
        var rider_name = $('#rider_name').val();
    }

    var father_name = $('#father_name').val();
    if ($('#business_location').val() == '') {
        $('#business_location').css('border-color', 'red');
        $('#business_location').focus();
        $('.common_message').css('color', 'red');
        $('.common_message').html(area_required);
        return false;
    } else {
        $('#business_location').css('border-color', '');
        $('.common_message').html('');
        var area = $('#business_location').val();
    }
    if ($('#nic').val() == '') {
        $('#nic').css('border-color', 'red');
        $('#nic').focus();
        return false;
    } else {
        $('#nic').css('border-color', '');
        var nic = $('#nic').val();
    }
    if ($('#delivery_radius').val() == '') {
        $('#delivery_radius').css('border-color', 'red');
        $('#delivery_radius').focus();
        $('.common_message').css('color', 'red');
        $('.common_message').html(delivery);
        return false;
    } else {
        $('#delivery_radius').css('border-color', '');
        $('.common_message').html('');
        var delivery_radius = $('#delivery_radius').val();
    }

    if ($('#phone_number').val() == '') {
        $('#phone_number').css('border-color', 'red');
        $('#phone_number').focus();
        return false;
    } else {
        $('#phone_number').css('border-color', '');
        var phone_number = $('#phone_number').val();
    }
    if ($('#email_address').val() == '') {
        $('#email_address').css('border-color', 'red');
        $('#email_address').focus();
        return false;
    } else {
        if (!isValidEmail($('#email_address').val())) {
            $('#email_address').focus();
            $('#email_address').css('border-color', 'red');
            return false;
        }
        $('#email_address').css('border-color', '');
        var email_address = $('#email_address').val();
    }

    if ($('#bike_type').val() == '') {
        $('#bike_type').css('border-color', 'red');
        $('#bike_type').focus();
        return false;
    } else {
        $('#bike_type').css('border-color', '');
        var bike_type = $('#bike_type').val();
    }
    if ($('#age').val() == '') {
        $('#age').css('border-color', 'red');
        $('#age').focus();
        return false;
    } else {
        $('#age').css('border-color', '');
        var age = $('#age').val();
    }
    if ($('#isLicense').val() == '') {
        $('#isLicense').css('border-color', 'red');
        $('#isLicense').focus();
        return false;
    } else {
        $('#isLicense').css('border-color', '');
        var license = $('#isLicense').val();
    }
    var latitude = $('#lat').val();
    var longitude = $('#lng').val();

    var data = {rider_name: rider_name, father_name: father_name, area: area, latitude: latitude, longitude: longitude, nic: nic, mobile_number: phone_number, bike_type: bike_type, age: age, license: license, email_id: email_address, delivery_radius: delivery_radius};

    $("#loader_rider").css('display', 'block');
    $.ajax({
        url: base_url + "Register/loginregistration/rider_register",
        type: 'POST',
        // async: false,
        data: data,
        beforeSend: function () {
            $("#loader_rider").css('display', 'block');

        },
        success: function (data) {
            try {
                var json = jQuery.parseJSON(data);
                if (json.status == 'success') {
                    $("#loader_rider").css('display', 'none');

                    $('#exampleModal2').modal('hide');
                    // toastr.success(json.message, 'Success Alert', {timeOut: 5000})
                    $('.common_msg').css('color', 'green');
                    $('.common_msg').html(json.message);
                    setTimeout(function () {
                        // window.location= base_url+'Login/Vendor';
                        window.location = base_url;
                    }, 3000);
                } else {
                    $("#loader_rider").css('display', 'none');
                    //   toastr.error(json.message, 'Error Alert', {timeOut: 5000})
                    $('.common_msg').css('color', 'red');
                    $('.common_msg').html(json.message);
                }
            } catch (e) {
                alert(e)
            }
        }
    });
});
$(document).on('change', '.MultipleImages', function () {
    var refval = $(this).attr('refval');
    var fileTypes = ['jpg', 'jpeg', 'png', 'gif', 'jfif'];
    if (this.files && this.files[0]) {
        var extension = this.files[0].name.split('.').pop().toLowerCase(),
                isSuccess = fileTypes.indexOf(extension) > -1;
        var fileName = this.files[0].name;
        if (isSuccess) {
            var FR = new FileReader();
            FR.addEventListener("load", function (e) {
                document.getElementById('ImagesEncode_' + refval).src = e.target.result;
                $('#ImagesEncode_' + refval).attr('newimage', e.target.result);
                $('#ImagesEncode_' + refval).show();
            });
            FR.readAsDataURL(this.files[0]);
        } else {
            alert('Invalid File')
        }
    }
});

$('#register_submit').on('click', function () {
    var json = '';
    var json = json + '{';
    if ($('#name').val() == '') {
        $('#name').css('border-color', 'red');
        $('#name').focus();
        return false;
    } else {
        $('#name').css('border-color', '');
        json = json + '"name":"' + $('#name').val() + '",';
    }
    if ($('#email').val() == '') {
        $('#email').css('border-color', 'red');
        $('#email').focus();
        return false;
    } else {
        if (!isValidEmail($('#email').val())) {
            $('#email').focus();
            $('#email').css('border-color', 'red');
            return false;
        } else {
            $('#email').css('border-color', '');
            json = json + '"email":"' + $('#email').val() + '",';
        }
    }
    if ($('#phone').val() == '') {
        $('#phone').css('border-color', 'red');
        $('#phone').focus();
        return false;
    } else {
        $('#phone').css('border-color', '');
        json = json + '"mobile_number":"' + $('#phone').val() + '",';
    }
    // json = json + '"job_title":"' + $('#job_title').val() + '",';
    if ($('#password').val() == '') {
        $('#password').css('border-color', 'red');
        $('.register_message').css('color', 'red');
        $('#password').focus();
        $('.register_message').html('Password is required');
        return false;
    } else {
        if (!isValidPassword($('#password').val())) {
            $('#password').css('border-color', 'red');
            $('.register_message').css('color', 'red');
            $('.register_message').html(password_validation);
            return false;
        } else {
            $('#password').css('border-color', '');
            json = json + '"password":"' + $('#password').val() + '",';
            $('.register_message').html('');
        }
    }
    if ($('#cpassword').val() == '') {
        $('#cpassword').css('border-color', 'red');
        $('#cpassword').focus();
        $('.register_message').css('color', 'red');
        $('.register_message').html(confirm_password);
        return false;
    } else {
        $('#cpassword').css('border-color', '');
    }
    if ($('#password').val() != $('#cpassword').val()) {
        $('#cpassword').css('border-color', 'red');
        $('#cpassword').focus();
        $('.register_message').css('color', 'red');
        $('.register_message').html(password_match);
        return false;
    } else {
        $('#cpassword').css('border-color', '');
        $('.register_message').html('');
    }

    if ($('#condition').prop("checked") == false) {
        $('.common_message').css('color', 'red');
        $('.common_message').html(accept_terms);
        return false;
    } else if ($('#condition').prop("checked") == true) {
        $('.common_message').html('');
    }

    json = json + '"mobile_code":"' + $('#mobile_code').val() + '"';
    json = json + '}';
    var data = {
        json: json,
    };
    $("#loader").css('display', 'block');
    $.ajax({
        url: base_url + "Frontend/signUp/register",
        type: 'POST',
        //async: false,
        // data: "json=" + encodeURIComponent(json),
        data: data,
        beforeSend: function () {
            $("#loader").css('display', 'block');

        },
        success: function (data) {
            // console.log(data);exit;
            try {
                var json = jQuery.parseJSON(data);
                if (json.status == 'success') {
                    $("#loader").css('display', 'none');
                    $('.register_message').css('color', 'green');
                    $('.register_message').html(json.message);
                    setTimeout(function () {
                        window.location = base_url;
                    }, 2000);
                } else {
                    $("#loader").css('display', 'none');
                    $('.register_message').css('color', 'red');
                    $('.register_message').html(json.message);
                }
            } catch (e) {
                alert(e)
            }
        }
    });
});
$('#login_submit').on('click', function () {
    if ($('#user_email').val() == '') {
        $('#user_email').css('border-color', 'red');
        $('#user_email').focus();
        return false;
    } else {
        $('#user_email').css('border-color', '');
        var email = $('#user_email').val();
    }
    if ($('#user_password').val() == '') {
        $('#user_password').css('border-color', 'red');
        $('#user_password').focus();
        return false;
    } else {
        $('#user_password').css('border-color', '');
        var password = $('#user_password').val();
    }
    var data = {email: email, password: password};
    $.ajax({
        url: base_url + "Frontend/signUp/login",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            $('.login_submit').html('Submit');
            try {
                var json = jQuery.parseJSON(data);
                if (json.status == 'success') {
                    $('.login_message').css('color', 'green');
                    $('.login_message').html(json.message);
                    var vendor_id = sessionStorage.getItem("vendor_id");
                    setTimeout(function () {
                        if (vendor_id == null || vendor_id == 'undefined')
                        {
                            window.location = base_url;
                            if (segment2 == 'callback') {
                                window.location = CallBack;
                            }
                        }
                        else
                        {
                            window.location = base_url + 'search_details/' + btoa(vendor_id);
                        }
                    }, 2000);

                } else {
                    $('.login_message').css('color', 'red');
                    $('.login_message').html(json.message);
                }
            } catch (e) {
                alert(e)
            }
        }
    });
});
$('#UpdateProfile').on('click', function () {
    if ($('#name').val() == '') {
        $('#name').css('border-color', 'red');
        $('#name').focus();
        return false;
    } else {
        $('#name').css('border-color', '');
        var name = $('#name').val();
    }

    if ($('#phone').val() == '') {
        $('#phone').css('border-color', 'red');
        $('#phone').focus();
        return false;
    } else {
        $('#phone').css('border-color', '');
        var mobile_number = $('#phone').val();
    }
    // if ($('#country_id').val() == '') {
    //     $('#country_id').css('border-color', 'red');
    //     $('#country_id').focus();
    //     return false;
    // } else {
    //     $('#country_id').css('border-color', '');
    //     var country_id = $('#country_id').val();
    // }
    if ($('#gender').val() == '') {
        $('#gender').css('border-color', 'red');
        $('#gender').focus();
        return false;
    } else {
        $('#gender').css('border-color', '');
        var gender = $('#gender').val();
    }
    var id = $('#userid').val();
    var json = {id: id, name: name, gender: gender, mobile_number: mobile_number};
    $.ajax({
        url: base_url + "Frontend/myProfile/update",
        type: 'POST',
        async: false,
        data: json,
        success: function (resp) {
            try {
                var json = jQuery.parseJSON(resp);
                if (json.status == 'success') {
                    $('.profile_message').css('color', 'green');
                    $('.profile_message').html(json.message);
                    setTimeout(function () {
                        window.location = base_url + 'my_profile';
                    }, 2000);
                } else {
                    $('.profile_message').css('color', 'red');
                    $('.profile_message').html(json.message);
                }
            } catch (e) {
                alert(e)
            }
        }
    });

});
$('#chooseimage').click(function () {
    $('#profile_dashboard').click();
});
if (window.history.replaceState) {
    window.history.replaceState(null, null, window.location.href);
}

$('body').on('change', '.change_profile_image', function () {
    var input = this;
    var url = $(this).val();
    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
    if (input.files && input.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#chooseimage').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
    var form_name = $(this).attr('form_name');
    var fd = new FormData(document.getElementById(form_name));
    fd.append("file_id", $(this).attr('file_id'));
    fd.append("status", 'change_profile_image');
    fd.append("id", this.id);
    fd.append("delete_img", $(this).attr('prev_image'));
    $.ajax({
        url: base_url + "Frontend/Helper"
        , type: "POST"
        , data: fd
        , processData: false
        , contentType: false
        , success: function (data) {
            try {
                var json = jQuery.parseJSON(data);
                if (json.status == 'success') {
                    setTimeout(function () {
                        window.location = base_url + 'my_profile';
                    }, 1);
                } else {
                    $('.profile_message').css('color', 'red');
                    $('.profile_message').html(json.message);
                }
            } catch (e) {
                alert(e)
            }
        }
    });
});
$("#search_restaurant,#search_restaurant22").click(function () {
    var searchtype = $(this).attr('searchtype');
    $('.searchtype').val(searchtype);
    var english = /^[A-Za-z0-9]*$/;

    if ($('#sublocality_level_1').val() != '') //&& english.test($('#sublocality_level_1').val())
    {
        var location = $('#sublocality_level_1').val();
    } else {
        var location = $('.get_location').val();
    }
    if (location == '')
    {
        $('#party_venue_map').modal('show');
        // $('#error_msg').html(enter_location);
        // $('#error_msg').css('color', 'red');
        return false;
    }

    $("#search_location").submit();
});
$('#autocomplete').click(function () {
    $('#party_venue_map').modal('show');
});
$('#business_location').click(function () {
    $('#business_location_map').modal('show');
})
$("#store_type").change(function () {
    var type = $("#store_type option:selected").text();
    if (type == 'Restaurant') {
        $(".cuisine").show();
    } else {
        $(".cuisine").hide();
    }
});
function searchrestaurant() {
    var latitude = $('.latitude').val();
    var longitude = $('.longitude').val();
    var english = /^[A-Za-z0-9]*$/;

    if ($('#sublocality_level_1').val() != '' && english.test($('#sublocality_level_1').val()))
    {
        var location = $('#sublocality_level_1').val();
    } else {
        var location = $('.get_location').val();
    }

    var data = {location: location, latitude: latitude, longitude: longitude};
    if (location != '')
    {
        $.ajax({
            url: base_url + "search_result",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                // $('.location').hide();
                // $('.cuisine_type').html(data);
            }
        });
        // var query = '';

        // query = query + 'location=' + location;

        // query = 'search_result?' + query;
        // query = base_url + query;

        // window.location = query;

    } else {
        $('#error_msg').html('Please enter a location');
        $('#error_msg').css('color', 'red');
    }
}
// function searchrestaurant() {
//     // var location = $('#location_name').val();
//     // var location = $('#autocomplete').val();
//     var english = /^[A-Za-z0-9]*$/;

//     if($('#sublocality_level_1').val()!='' && english.test($('#sublocality_level_1').val()))
//     {
//         var location = $('#sublocality_level_1').val();
//     }else{
//         var location = $('.get_location').val();
//     }
//     if(location!='')
//     {
//         var query = '';

//         query = query + 'location=' + location;

//         query = 'search_result?' + query;
//         query = base_url + query;

//         window.location = query;
//     // var string = location.split('-');
//     // if(location!='')
//     // {
//     //     var query = '';

//     //     location = string != undefined ? string[0] : '';

//     //     query = query + 'location=' + string[0];

//     //     query = 'search_result?' + query;
//     //     query = base_url + query;

//     //     window.location = query;
//     }else{
//         $('#error_msg').html('Please enter a location');
//         $('#error_msg').css('color', 'red');
//     }
// }

// $('body').on('click', 'input[name="book_table_filter"]', function() {
$("#book_table_filter").on('change', function () {
    if ($(this).is(':checked')) {
        $(this).attr('value', 1);
        var table_booking = $(this).val();
        var rest = $('#restSearchBox').val();
        var loc_value = $('#location').val();
        var latitude = $('#latitude').val();
        var longitude = $('#longitude').val();
        var locality = $('#locality').val();
        var store_type = $('#store_type').val();
        var data = {status: 'bookingFacility', table_booking: table_booking, rest: rest, location: loc_value, store_type: store_type, latitude: latitude, longitude: longitude, locality: locality};

        $.ajax({
            url: base_url + "Frontend/Helper",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                var json = jQuery.parseJSON(data);
                $('.location').hide();
                $('.cuisine_type').html(json.data1);
                $('.search').hide();
                $('.search_div').html(json.data2);
                $('#options_menu').hide();
                $('#options_menu_popup').html(json.data3);
            }
        });
    } else {
        $(this).attr('value', 0);
        clearRestSearchText();
    }


});

// $('body').on('change', 'input[name="cuisinetype"]', function() {
$('body').on('click', 'input[name="cuisinetype"]', function () {
//   
//    // var cuisine_type = $(this).val();
//    var cuisine_type = $('input[id="cuisinetype"]:checked').map(function () {
//        return this.value;
//    }).get().join(",");
//
//    // var url = window.location.href;
//    // var arguments = url.split('?')[1].split('=');
//    // var loc_value = decodeURIComponent(arguments[1])
//    var loc_value = $('#location').val();
//    var rest = $('#restSearchBox').val();
//    var latitude = $('#latitude').val();
//    var longitude = $('#longitude').val();
//    var locality = $('#locality').val();
//    var store_type = $('#store_type').val();
//    var table_booking = $('#book_table_filter').val();
//    var data = {status: 'cuisineType', cuisine_type: cuisine_type, table_booking: table_booking, location: loc_value, store_type: store_type, rest: rest, latitude: latitude, longitude: longitude, locality: locality};
//
//    $.ajax({
//        url: base_url + "Frontend/Helper",
//        type: 'POST',
//        async: false,
//        data: data,
//        success: function (data) {
//            $('.location').hide();
//            $('.cuisine_type').html(data);
//        }
//    });
});

// $('body').on('change', 'input[name="cuisinetype_popup"]', function() {
$('body').on('click', 'input[name="cuisinetype_popup"]', function () {
    // var cuisine_type = $(this).val();
    var cuisine_type = $('input:checkbox:checked').map(function () {
        return this.value;
    }).get().join(",");
    // var url = window.location.href;
    // var arguments = url.split('?')[1].split('=');
    // var loc_value = decodeURIComponent(arguments[1]);
    var loc_value = $('#location').val();
    var rest = $('#restSearchBox').val();
    var latitude = $('#latitude').val();
    var longitude = $('#longitude').val();
    var locality = $('#locality').val();
    var store_type = $('#store_type').val();
    var table_booking = $('#book_table_filter').val();
    var data = {status: 'cuisineType', cuisine_type: cuisine_type, table_booking: table_booking, location: loc_value, store_type: store_type, rest: rest, latitude: latitude, longitude: longitude, locality: locality};

    $.ajax({
        url: base_url + "Frontend/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            // $('#exampleModal').modal('hide');
            $('.location').hide();
            $('.cuisine_type').html(data);
            var cus = cuisine_type.split(',');
            $.each(cus, function (index, value) {

                $("input[value='" + value + "']").prop('checked', true);
            });
            // $("input[value='" + cuisine_type + "']").prop('checked', true);
        }
    });
});
$('.confirm_cuisine').click(function () {
    $('#exampleModal').modal('hide');
})
$('body').on('click', '#check_confirmation', function () {
    $.ajax({
        url: base_url + "Frontend/cartProductCheck",
        method: "POST",
        async: false,
        success: function (resp) {
            $('#confirmModal').modal("hide");
            setTimeout(function () {
                window.location = '';
            }, 1000);
        }
    });

});
$('body').on('click', '.checkoutbtn', function () {

    if ($('#delivery_time').val() == 1)
    {
        var url = $(this).attr('url');
        $('.checkoutbtn').attr('onClick', url);
        $(".checkoutbtn").trigger("onclick");
    } else {
        $('#deliveryModal').modal('show');
    }


});
$('.proceed').click(function () {

    var url = $(this).attr('url');
    $('.proceed').attr('onClick', url);
    $(".proceed").trigger("onclick");
});

$('.book_table').click(function () {

//    var url = $(this).attr('url');
//    $('.book_table').attr('onClick', url);
//    $(".book_table").trigger("onclick");
});
function cancel_alert(e)
{
    var id = e.id;
    $("#" + id).modal('hide');
}
function ok_alert()
{
    // var loc = $('#location').val();
    // window.location.href = base_url+'search_result';

    var store_category = $('#store_category').val();
    window.location.href = base_url + 'store_category/' + btoa(store_category);

}
function delAddr(e)
{
    e.preventDefault();
    $('#deleteModal').modal('show');
}
function showSelectAddressPopup()
{
    $('#selectAddressModal').show();
}
function addressSelected()
{
    var selected_address = $('#selected_address').val();
    var data = {status: 'updateAddressStatus', address_id: selected_address};
    $.ajax({
        url: base_url + "Frontend/Helper",
        method: "POST",
        async: false,
        data: data,
        success: function (resp) {
            setTimeout(function () {
                window.location = '';
            }, 1000);
        }

    });

}
function ok_delete(id)
{
    var data = {status: 'delete_address', address_id: id};
    $.ajax({
        url: base_url + "Frontend/Helper",
        method: "POST",
        async: false,
        data: data,
        success: function (resp) {
            setTimeout(function () {
                window.location = '';
            }, 1000);
        }

    });
}

function ok()
{
    $("#closeModal").modal('hide');
    $("#busyModal").modal('hide');
    $('.sub_cat').attr("data-status", 1);
    $('.add_to_cart').attr("data-status", 1);
    var vendor_id = $('.sub_cat').data("vendor");
    var session_vendor_id = $('.sub_cat').data("sessionvendor");
    if (vendor_id != session_vendor_id && session_vendor_id != 0)
    {
        if (session_vendor_id != undefined || session_vendor_id == 0)
        {
            $("#confirmModal").modal('show');
        }
    }

}
function cancel_popup()
{
    $("#closeModal").modal('hide');
    $("#busyModal").modal('hide');
    $('#deleteModal').modal('hide');
    $('.sub_cat').attr("data-status", 1);
    $('.add_to_cart').attr("data-status", 1);
    var vendor_id = $('.sub_cat').data("vendor");
    var session_vendor_id = $('.sub_cat').data("sessionvendor");
    if (vendor_id != session_vendor_id && session_vendor_id != 0)
    {
        if (session_vendor_id != undefined || session_vendor_id == 0)
        {
            $("#confirmModal").modal('show');
        }
    }


}
$('.cart_remove_popup').click(function () {
    var Id = $(this).data('id');
    $(".modal-body #menu_val").val(Id);
    $('#cartPopupModal_' + Id).modal('show');
})
$('body').on('click', '.sub_cat', function () {
    var menu_id = $(this).data("id");
    if ($('#addedBtnId' + menu_id).css('display') == 'block') {
        return false;
    }
    var category_id = $(this).data("category");
    var vendor_id = $(this).data("vendor");
    var session_vendor_id = $(this).data("sessionvendor");
    var close = $(this).data("close");
    var busy = $(this).data("busy");
    var status = $(this).data("status");
    if (status == undefined && (close != undefined || busy != undefined))
    {
        if (close == 1) {
            $("#closeModal").modal('show');
            $("#confirmModal").modal('hide');
        } else if (busy == 1)
        {
            $("#busyModal").modal('show');
            $("#confirmModal").modal('hide');
        } else if (vendor_id != session_vendor_id && session_vendor_id != '')
        {
            // $("#confirmModal").modal('show');
            $("#busyModal").modal('hide');
        }
    }
    else {
        if (vendor_id != session_vendor_id && session_vendor_id != '')
        {
            $("#confirmModal").modal('show');
            // $("#busyModal").modal('hide');
        }
        else {

            var data = {status: 'get_menu_options', menu_id: menu_id, category_id: category_id, vendor_id: vendor_id};
            $.ajax({
                url: base_url + "Frontend/Helper",
                method: "POST",
                async: false,
                data: data,
                success: function (resp) {
                    $('#options_menu').html(resp);
                    $('#exampleModal').modal("show");
                }

            });
        }
    }
});

$("a.scrollLink").click(function (event) {
    event.preventDefault();
    var catId = $(this).attr('catId');

    if (catId != undefined) {
        $('#div_section_' + catId).click();
    }
    $("html, body").animate({scrollTop: $($(this).attr("href")).offset().top}, 500);
});
$("#search_menu_item").on("keyup", function () {
    $('.search-icon').css('display', 'none');
    $('.delete-icon').css('display', 'block');
    var menu = $(this).val().toLowerCase();
    var vendor_id = $('#vendor_id').val();
    var data = {status: 'get_menu_search', menu: menu, vendor_id: vendor_id};
    $.ajax({
        url: base_url + "Frontend/Helper",
        method: "POST",
        async: false,
        data: data,
        success: function (resp) {
            var json = jQuery.parseJSON(resp);
            $('.get_category ul').html(json.data1);
            $('#get_id').html(json.data2);

        }
    });
});

$("#search_partymenu_item").on("keyup", function () {
    $('.search-icon').css('display', 'none');
    $('.delete-icon').css('display', 'block');
    var menu = $(this).val().toLowerCase();
    var vendor_id = $('#vendor_id').val();
    var data = {status: 'get_partymenu_search', menu: menu, vendor_id: vendor_id};
    $.ajax({
        url: base_url + "Frontend/Helper",
        method: "POST",
        async: false,
        data: data,
        success: function (resp) {
            var json = jQuery.parseJSON(resp);
            $('.get_menucategory ul').html(json.data1);
            $('#get_menuid').html(json.data2);


        }
    });
});
$('#search_menu_item').keyup(function (e) {
    if (e.keyCode == 8) {
        location.reload();
    }
})
$('#search_partymenu_item').keyup(function (e) {
    if (e.keyCode == 8) {
        location.reload();
    }
})

$("#restSearchBox").on("keyup", function () {
    $('.search-icon').css('display', 'none');
    $('.delete-icon').css('display', 'block');
    var rest = $(this).val().toLowerCase();
    var location = $('#location').val();
    var latitude = $('#latitude').val();
    var longitude = $('#longitude').val();
    var locality = $('#locality').val();
    var store_type = $('#store_type').val();
    // var cusine_type = $("input[name='cuisinetype']:checked").val();
    var cuisine_type = $('input:checkbox:checked').map(function () {
        return this.value;
    }).get().join(",");
    var table_booking = $('#book_table_filter').val();
    var data = {status: 'get_restaurant_search', rest: rest, table_booking: table_booking, location: location, cuisine_type: cuisine_type, store_type: store_type, latitude: latitude, longitude: longitude, locality: locality};

    $.ajax({
        url: base_url + "Frontend/Helper",
        method: "POST",
        async: false,
        data: data,
        success: function (resp) {
            // console.log(resp);exit;
            var json = jQuery.parseJSON(resp);
            $('.location').hide();
            $('.cuisine_type').html(json.data1);
            $('.search').hide();
            $('.search_div').html(json.data2);
            $('#options_menu').hide();
            $('#options_menu_popup').html(json.data3);

        }
    });
});
$('#partyclickbuttin').change(function () {
    $("#restSearch").keyup();
});
$("#restSearch").on("keyup", function () {
    $('.search-icon').css('display', 'none');
    $('.delete-icon').css('display', 'block');
    var parttyorder = $('input[name=parttyorder]:checked').val();
    parttyorder = parttyorder == undefined ? 0 : 1;

    var rest = $(this).val().toLowerCase();
    var store_type = $('#store_type').val();
    var data = {status: 'restaurant_search', rest: rest, store_type: store_type, parttyorder: parttyorder};

    $.ajax({
        url: base_url + "Frontend/Helper",
        method: "POST",
        async: false,
        data: data,
        success: function (resp) {
            $('.__narlist').hide();
            $('.__search_result').show();
            $('.__search_result').html(resp);

        }
    });
});
function clearRestSearchText()
{
    var pathname = window.location.pathname;
    var current_url = pathname.split('/');

    if (current_url[2] == 'homely_foods' || current_url[2] == '' || current_url[2] == 'search_details')
    {
        setTimeout(function () {
            window.location = '';
        }, 1000);
    } else {
        $('#restSearchBox').val('');
        var location = $('#location').val();
        var latitude = $('#latitude').val();
        var longitude = $('#longitude').val();
        var store_type = $('#store_type').val();
        var data = {status: 'clear_search', location: location, latitude: latitude, longitude: longitude, store_type};
        $.ajax({
            url: base_url + "Frontend/Helper",
            method: "POST",
            async: false,
            data: data,
            success: function (resp) {

                $('.location').hide();
                $('.cuisine_type').html(resp);


                setInterval('location.reload()', 1000);
            }
        });
    }
    //  setTimeout(function () {
    //     window.location = '';
    // }, 1000);
}
$('#change_password').on('click', function () {
    var json = '';
    var json = json + '{';
    // json = json + '"old_password":"' + $('#old_password').val() + '",';
    json = json + '"password":"' + $('#password').val() + '",';
    if ($('#password').val() != '') {
        if (!isValidPassword($('#password').val())) {
            $('#password').css('border-color', 'red');
            $('#password_msg').css('color', 'red');
            $('#password_msg').html(password_validation);
            return false;
        } else {
            $('#password').css('border-color', '');
            $('#password_msg').html('');
        }
    }
    if ($('#cpassword').val() == '') {
        $('#cpassword').css('border-color', 'red');
        $('#cpassword').focus();
        return false;
    } else {
        $('#cpassword').css('border-color', '');
    }
    if ($('#password').val() != $('#cpassword').val()) {
        $('#cpassword').css('border-color', 'red');
        $('#cpassword').focus();
        $('#password_msg').css('color', 'red');
        $('#password_msg').html(password_match);
        return false;
    } else {
        $('#cpassword').css('border-color', '');
        $('#password_msg').html('');
    }
    json = json + '"id":"' + $('#userid').val() + '"';
    json = json + '}';
    $("#loader").css('display', 'block');
    $.ajax({
        url: base_url + "Frontend/changePassword/update",
        type: 'POST',
        // async: false,
        data: "json=" + encodeURIComponent(json),
        beforeSend: function () {
            $("#loader").css('display', 'block');

        },
        success: function (resp) {
            try {
                var json = jQuery.parseJSON(resp);
                if (json.status == 'success') {
                    $("#loader").css('display', 'none');
                    $('#password_msg').css('color', 'green');
                    $('#password_msg').html(json.message);
                    setTimeout(function () {
                        window.location = base_url + 'change_password';
                    }, 2000);
                } else {
                    $('#password_msg').css('color', 'red');
                    $('#password_msg').html(json.message);
                }
            } catch (e) {
                alert(e)
            }
        }
    });
});
$('#forgotpassword').on('click', function () {
    var email = $('#email').val();
    if ($('#email').val() === '') {
        $('#email').css('border-color', 'red');
        return false;
    } else {
        if (!isValidEmail($('#email').val())) {
            $('#email').focus();
            $('#email').css('border-color', 'red');
            return false;
        } else {
            $('#email').css('border-color', '');
        }
    }
    $.ajax({
        url: base_url + "Frontend/forgotPassword/update"
        , async: true
        , type: 'POST'
        , data: "email=" + email
        , success: function (resp) {
            var json = jQuery.parseJSON(resp);
            try {
                var json = jQuery.parseJSON(resp);
                if (json.status == 'success') {
                    $('.common_message').css('color', 'green');
                    $('.common_message').html(json.message);
                    setTimeout(function () {
                        window.location = base_url + 'login_signup';
                    }, 2000);
                } else {
                    $('.common_message').css('color', 'green');
                    $('.common_message').html(json.message);
                }
            } catch (e) {
                alert(e)
            }

        }
    });
});
$('#resetpassword').on('click', function () {
    var id = $('#userid').val();
    var password = $('#password').val();
    if ($('#password').val() === '') {
        $('#password').css('border-color', 'red');
        return false;
    } else {
        if (!isValidPassword($('#password').val())) {
            $('#password').css('border-color', 'red');
            $('.common_message').css('color', 'red');
            $('.common_message').html(password_validation);
            return false;
        } else {
            $('#password').css('border-color', '');
            $('.common_message').html('');
        }
    }
    if ($('#cpassword').val() === '') {
        $('#cpassword').css('border-color', 'red');
        return false;
    } else {
        if ($('#cpassword').val() != $('#password').val()) {
            $('#cpassword').css('border-color', 'red');
            $('.common_message').css('color', 'red');
            $('.common_message').html(password_match);
            return false;
        } else {
            $('#cpassword').css('border-color', '');
            $('.common_message').html('');
        }
    }
    $.ajax({
        url: base_url + "Frontend/resetpassword/update"
        , async: true
        , type: 'POST'
        , data: "password=" + password + "&id=" + id
        , success: function (data) {
            var json = jQuery.parseJSON(data);
            try {
                var json = jQuery.parseJSON(data);
                if (json.status == 'success') {
                    $('.common_message').css('color', 'green');
                    $('.common_message').html(json.message);
                    setTimeout(function () {
                        window.location = base_url + 'login_signup';
                    }, 2000);
                } else {
                    $('.common_message').css('color', 'red');
                    $('.common_message').html(json.message);
                }
            } catch (e) {
                alert(e)
            }
        }
    });
});

$(document).on("click", ".add_to_cart", function () {
    // var get_session_data = sessionStorage.getItem("options");
    var get_session_data = localStorage.getItem("options");
    var choice = $(this).data("choice");
    var vendor_id = $(this).data("vendor");
    var session_vendor_id = $(this).data("sessionvendor");
    var close = $(this).data("close");
    var busy = $(this).data("busy");
    var status = $(this).data("status");
    var party = $(this).data("party");
    var session_party = $(this).data("sessionparty");


    if (status == undefined && (close != undefined || busy != undefined))
    {

        if (close == 1) {
            $("#closeModal").modal('show');
            $("#confirmModal").modal('hide');
        } else if (busy == 1)
        {
            $("#busyModal").modal('show');
            $("#confirmModal").modal('hide');
        } else if (vendor_id != session_vendor_id && session_vendor_id != '')
        {
            // $("#confirmModal").modal('show');
            $("#busyModal").modal('hide');
        }
        else if (party != session_party && session_party != '')
        {
            // $("#confirmModal").modal('show');
            $("#busyModal").modal('hide');
        }
    }

    else {
        if (vendor_id != session_vendor_id && session_vendor_id != '')
        {
            $("#confirmModal").modal('show');
        } else if (party != session_party && session_party !== '')
        {

            $("#PartyModal").modal('show');
        }
        else {

            if (choice == 0)
            {
                var menu_id = $(this).data("id");
                var category = $(this).data("category");
                var vendor_id = $(this).data("vendor");
                var location = $('#location').val();
                var latitude = $('#latitude').val();
                var longitude = $('#longitude').val();
                var data = {status: 'AddToCart', choice: choice, party: party, menu_id: menu_id, category: category, vendor_id: vendor_id, location: location, latitude: latitude, longitude: longitude};

                $.ajax({
                    url: base_url + "Frontend/Helper",
                    type: 'POST',
                    async: false,
                    data: data,
                    success: function (data) {
                        //  console.log(data);exit;
                        $('.cart_summary').html('');
                        $('.promo_code').hide('');
                        $('.total').hide('');
                        // $('.cart_summary').html(data);
                        menuapplyBtn(menu_id);
                        loadmyCartIteam();

                        var cart_idFromYourCart = $('.cart_id-cart-list-' + menu_id).val();
                        $('.menu-class-' + menu_id + ' .btn-sm').attr('data-cart', cart_idFromYourCart);
                        $('.cart_id-menu-list-' + menu_id).val(cart_idFromYourCart);
                        sessionStorage.removeItem('options');
                    }
                });
            } else {
                // $("input:radio").each(function(){
                $('input[name="customRadio"]').each(function () {

                    var name = $(this).attr("name");

                    if ($("input:radio[name=" + name + "]:checked").length == 0) {
                        $('#' + name).attr("src", base_url + "images/redtick.svg");
                        exit;
                        // throw new Error("Something went badly wrong!");
                    } else {
                        $('#' + name).attr("src", base_url + "images/success-circle.svg");
                    }
                });
                $('input[name="customOptions"]').each(function () {

                    var name = $(this).attr("name");

                    if ($("input:radio[name=" + name + "]:checked").length == 0) {
                        $('#' + name).attr("src", base_url + "images/redtick.svg");
                        exit;
                        // throw new Error("Something went badly wrong!");
                    } else {
                        $('#' + name).attr("src", base_url + "images/success-circle.svg");
                    }
                });


                var options_id = $(this).data("options");
                // var product_price = $(this).data("price");
                var product_price = $('#menu_tot_price').val();
                var menu_id = $(this).data("id");
                var category = $(this).data("category");
                var vendor_id = $(this).data("vendor");
                var quantity = $('.qty').text();
                var location = $('#location').val();
                var latitude = $('#latitude').val();
                var longitude = $('#longitude').val();

                var data = {status: 'AddToCart', choice: choice, party: party, menu_id: menu_id, options_id: options_id, price: product_price, vendor_id: vendor_id, category: category, quantity: quantity, options_data: get_session_data, location: location, latitude: latitude, longitude: longitude};
                // console.log(data);exit;
                $.ajax({
                    url: base_url + "Frontend/Helper",
                    type: 'POST',
                    async: false,
                    data: data,
                    success: function (data) {
                        $('#exampleModal').modal("hide");
                        $('.cart_summary').html('');
                        $('.promo_code').hide('');
                        $('.total').hide('');
                        menuapplyBtn(menu_id);
                        loadmyCartIteam();


                        var cart_idFromYourCart = $('.cart_id-cart-list-' + menu_id).val();
                        $('.menu-class-' + menu_id + ' .btn-sm').attr('data-cart', cart_idFromYourCart);
                        $('.cart_id-menu-list-' + menu_id).val(cart_idFromYourCart);
                        //$('.cart_summary').html(data);
                        setTimeout(function () {

                            //  window.location = '';
                        }, 1000);

                        sessionStorage.removeItem('options');
                    }
                });

            }
        }
    }


});
$('body').on('click', '.remove_cart_product', function () {
    var id = $(this).data("id");
    var menu_id = id;
    var cart_id = $(this).data("cart");
    var data = {status: 'RemoveFromCart', id: menu_id, cart_id: cart_id};
    $.ajax({
        url: base_url + "Frontend/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            $('menubtnaddid' + menu_id).show();
            $('#addedBtnId' + menu_id).hide();

            setTimeout(function () {
                $('#menuIdse' + menu_id).addClass('add_to_cart');
                //alert("Hello");
            }, 250);


            loadmyCartIteam();
            return false;

        }
    });



});

function check_cart() {
    $.ajax({
        url: base_url + "frontend/check_cart"
        , success: function (data) {
            if (data != "0") {
                var json = jQuery.parseJSON(data);
                $('.cart_count').show();
                $('.cart_count').html(json.count);
            } else {
                $('.cart_count').css('display', 'none');
            }
        }
    })
}
$(document).ready(function () {

    check_cart();
    localStorage.removeItem('options');
});



$('body').on('change', 'input[name="customRadio"]', function () {
    var div_id = $(this).attr('name');
    $('#' + div_id).attr("src", base_url + "images/success-circle.svg");

    // sessionStorage.removeItem('options');
    var menu_price = $(this).val();
    // console.log(menu_price);
    var class_name = $(this).attr('class');
    var ret = class_name.split(" ");
    var qty = $('.qty').text();
    // var menu_price = menuPrice * qty;
    var dataPrice = $(this).data("price");
    var itemPrice = $('#tot_price').val();
    var options_price = $('#options_price').val();
    var item_name = $(this).data("name");
    var id = $(this).attr('id');

    var get_id = id.match(/[a-z]+|\d+/ig);

    if (menu_price != '0.00' || menu_price != '0')
    {

        var price = parseFloat(dataPrice) + parseFloat(menu_price) + parseFloat(options_price);
        $('#menu_tot_price').val(price);

        // if($('input[type="checkbox"]').is(':checked'))
        // {
        //     var get_addon_values = sessionStorage.getItem("addon_values");
        // }else{
        //     var get_addon_values = sessionStorage.removeItem('addon_values');
        // }
        // if(get_addon_values!=undefined)
        // {
        //     var prod_price = (parseInt(get_addon_values)+parseInt(menu_price)+parseInt(dataPrice));
        //     $('#menu_tot_price').val(prod_price);
        // }else{
        //     $('#menu_tot_price').val(dataPrice);
        // }

    } else {


        $('#menu_tot_price').val(dataPrice);

        //   if($('input[type="checkbox"]').is(':checked'))
        //     {
        //         var get_addon_values = sessionStorage.getItem("addon_values");
        //     }else{
        //         var get_addon_values = sessionStorage.removeItem('addon_values');
        //     }
        //     //console.log(get_addon_values);
        //     if(get_addon_values!=undefined)
        //     {
        //         var prod_price = (parseInt(get_addon_values)+parseInt(menu_price));
        //         $('#menu_tot_price').val(prod_price);
        //     }else{
        //         $('#menu_tot_price').val(dataPrice);
        //     }
    }
    var discount = $('#discount').val();

    if (itemPrice == 0 || itemPrice == undefined)
    {
        if (discount != 0)
        {
            $('.item_discount_price').css('display', 'block');
            $('.item_price span').css('text-decoration', 'line-through');
        }
        var updated_price = (menu_price * qty).toFixed(2);

        var discounted_price = updated_price * discount;
        var dis_price = updated_price - discounted_price;
        var discount_price = dis_price.toFixed(2);
        $('.item_price span').html('Price PKR ' + updated_price);
        $('.item_discount_price span').html('Price PKR ' + discount_price);

        var product_price = updated_price;
        if (discount != 0)
        {
            $('#menu_tot_price').val(discount_price);
        } else {
            $('#menu_tot_price').val(menu_price);
        }

        $('#tot_price').val(product_price);
    } else {

        if (menu_price != '0.00' || menu_price != '0')
        {
            // console.log(get_addon_values);
            // if(get_addon_values!=undefined)
            // {
            //     var item_price = $('#menu_tot_price').val();

            //     var price  = item_price;
            // }else{
            //      var item_price = $('#menu_tot_price').val();
            //     //  var price  = (parseInt(menu_price) + parseInt(item_price));
            //     // var price  = (parseInt(menu_price) + parseInt(dataPrice));
            //     var price  = (parseInt(menu_price) + parseInt(dataPrice));


            // }


            // var price  = (parseInt(menu_price)+parseInt(item_price));

            // $('#menu_tot_price').val(price);
            // var price = $('#menu_tot_price').val();
            // var price  = (parseInt(item_price) + parseInt(dataPrice));
            // $('#menu_tot_price').val(price);
            var update_price = $('#menu_tot_price').val();
            var price_update = parseInt(update_price) * qty;
            $('#tot_price').val(price_update);
            var product_price = price_update.toFixed(2);
            var discounted_price = product_price * discount;
            var dis_price = product_price - discounted_price;
            var discount_price = dis_price.toFixed(2);
            if (discount != 0)
            {
                $('#menu_tot_price').val(discount_price);
            } else {
                $('#menu_tot_price').val(price);
            }
            $('.item_price span').html('Price PKR ' + product_price);
            $('.item_discount_price span').html('Price PKR ' + discount_price);
        } else {
            if (get_addon_values != undefined)
            {
                var item_price = $('#menu_tot_price').val();

                var price = item_price;
            } else {
                var item_price = $('#menu_tot_price').val();
                var price = (parseInt(menu_price) + parseInt(item_price));
            }

            // var price  = (parseInt(menu_price)+parseInt(item_price));

            // $('#menu_tot_price').val(price);


            $('#menu_tot_price').val(price);
            var update_price = $('#menu_tot_price').val();
            var price_update = parseInt(update_price) * qty;
            $('#tot_price').val(price_update);
            var product_price = price_update.toFixed(2);
            var discounted_price = product_price * discount;
            var dis_price = product_price - discounted_price;
            var discount_price = dis_price.toFixed(2);
            if (discount != 0)
            {
                $('#menu_tot_price').val(discount_price);
            } else {
                $('#menu_tot_price').val(price);
            }
            $('.item_price span').html('Price PKR ' + product_price);
            $('.item_discount_price span').html('Price PKR ' + discount_price);
        }
    }

    var test_arr = JSON.parse(localStorage.getItem("options"));
    if (test_arr != null)
    {

        var result = test_arr.filter(function (v, i) {
            if (v.type === 'size') {
                test_arr.splice(i, 1)
            }
        });
        localStorage.setItem("options1", JSON.stringify(test_arr));

        var test_arr1 = JSON.parse(localStorage.getItem("options1"));
        test_arr1.push({type: $(this).data("type"),
            name: $(this).data('name'),
            id: get_id[1], });

        localStorage.setItem("options1", JSON.stringify(test_arr1));

        var test_arr2 = JSON.parse(localStorage.getItem("options1"));
        localStorage.setItem("options", JSON.stringify(test_arr2));
        // var stored = test_arr2;

    } else {
        var array = [];

        array.push({type: $(this).data("type"),
            name: $(this).data('name'),
            id: get_id[1], });
        localStorage.setItem("options", JSON.stringify(array));

    }
    $('#' + ret[1]).html(item_name);
    $('#' + ret[1]).attr('data-id', get_id[1]);
    // $('.selected_item').html(item_name);
    $('#add_tocart').attr("data-options", get_id[1]);
    $('#add_tocart').attr('data-price', product_price);
    $('#add_tocart').attr('data-quantity', $('.qty').text());
});


function myFunction(obj) {
    var class_name = $(obj).attr('class');
    var ret = class_name.split(" ");

    var val = [];
    var array = [];
    var product_price = [];
    var menu_price1 = [];
    var ischecked = $(obj).prop("checked");

    if (ischecked == true) {
        $('.' + ret[1] + ':checked').each(function (i) {
            val[i] = $(this).data("name");
        });

        $("input:checked").each(function () {
            var id = $(this).attr('id');
            var get_id = id.match(/[a-z]+|\d+/ig);
            array.push({
                type: $(this).data("type"),
                name: $(this).data('name'),
                id: get_id[1],
            });
        });
        localStorage.setItem("options", JSON.stringify(array));
        // sessionStorage.setItem("options", JSON.stringify(array));
        var itemPrice = $('#tot_price').val();
        var options_price = $('#options_price').val();
        // // var item_price = $(obj).data('price');
        var item_price = $('#menu_tot_price').val();
        var menu_price = $(obj).val();
        var qty = $('.qty').text();
        if (itemPrice == 0)
        {
            $('.item_price').html('Price PKR ' + menu_price);
            product_price = menu_price;
        } else {
            //     // var price  = (parseFloat(menu_price)+parseFloat(item_price));
            // var price  = (parseFloat(menu_price)+parseFloat(itemPrice));
            var price = (parseFloat(menu_price) + parseFloat(item_price));
            var price1 = parseFloat(options_price) + parseFloat(menu_price);
            $('#options_price').val(price1);
            $('#menu_tot_price').val(price);
            var update_price = $('#menu_tot_price').val();
            //     //   var price_update  = parseFloat(update_price);     
            var price_update = parseFloat(update_price) * qty;
            //     // $('.'+ret[1]).attr('data-price', price);
            //     $('#tot_price').val(price_update);
            // product_price = price_update.toFixed(2);
            $('#tot_price').val(price);
            // product_price = price.toFixed(2);
            product_price = price_update.toFixed(2);

        }
        // var a = sessionStorage.getItem("addon_values");
        // console.log(a);
    } else {
        $('.' + ret[1] + ':checked').each(function (i) {
            val[i] = $(this).data("name");
        });
        $("input:checked").each(function () {
            var id = $(this).attr('id');
            var get_id = id.match(/[a-z]+|\d+/ig);
            array.push({
                type: $(this).data("type"),
                name: $(this).data('name'),
                id: get_id[1],
            });
        });
        localStorage.setItem("options", JSON.stringify(array));

        var itemPrice = $('#tot_price').val();
        var options_price = $('#options_price').val();
        var menu_price = $(obj).val();
        var item_price = $('#menu_tot_price').val();
        var qty = $('.qty').text();
        if (itemPrice == 0)
        {
            $('.item_price').html('Price PKR ' + menu_price);
            product_price[i] = menu_price[i];
        } else {
            // var price  = parseFloat(itemPrice)-parseFloat(menu_price);
            // var price  = parseFloat(item_price)-parseFloat(menu_price);
            var price = parseFloat(item_price) - parseFloat(menu_price);
            var price1 = parseFloat(options_price) - parseFloat(menu_price);
            //     // $('.'+ret[1]).attr('data-price', price);
            $('#options_price').val(price1);
            $('#menu_tot_price').val(price);
            //     $('#tot_price').val(price);
            //     product_price = price.toFixed(2);
            //      sessionStorage.removeItem('addon_values');   
            var update_price = $('#menu_tot_price').val();
            var price_update = parseFloat(update_price) * qty;
            $('#tot_price').val(price);
            product_price = price_update.toFixed(2);
            // product_price = price.toFixed(2);
        }
        // var a = sessionStorage.getItem("addon_values");
        // console.log(a);
    }

    sessionStorage.setItem("addon_values", price1);

    $('.item_price span').html('Price PKR ' + product_price);
    $('#' + ret[1]).html(val.join(', '));
    $('#add_tocart').attr('data-price', product_price);


}
function updateQty(e) {

    var type = $(e).data('type');
    var menu_id = $(e).data('id');
    var quantity = $('.cartlistqty qtymenu-' + menu_id).text();

    quantity = $.isNumeric(quantity) ? quantity : '1';
    var store_type = $('#store_type').val();
    var cart_id = $(e).data('cart');


    if (quantity == '1' && type == 'minus') {
        $('#menuremoveId-' + menu_id).click();
        return false;
    }

    var data = {menu_id: menu_id, type: type, quantity: quantity, store_type: store_type, cart_id: cart_id};

    if (quantity > 0)
    {

        $.ajax({
            url: base_url + "frontend/UpdateQuantity",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {

                var json = jQuery.parseJSON(data);
                if (json.status == 'error')
                {
                    toastr.error(json.message, 'Error Alert', {timeOut: 5000});
                    return false;
                }
                if (json.status == 'success') {
                    // check_cart();
                    $('qtymenu-' + menu_id).html(json.quantityUpdated);
                    loadmyCartIteam();
                }

                //   $('.__cart_droplist').show();
                //   $('.__cart_droplist').css('display','block');
                //   $('#cart_popup').show();
                //   $(".__cartsummy").load(" .__cartsummy");
                //   $(".cart_icon").load(" .cart_icon");

            }
        })
    }

}

function menuapplyBtn(menu_id) {
    $('menubtnaddid' + menu_id).hide();
    $('#addedBtnId' + menu_id).show();
    $('#menuIdse' + menu_id).removeClass('add_to_cart');
    $('qtymenu-' + menu_id).html('1');
}
function loadmyCartIteam() {
    var restaurantId = atob(segment2);
    var data = {status: 'getmycartHTMLWebsite', encode: true, restaurantId: restaurantId};
    $.ajax({
        url: base_url + "Frontend/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            try {
                var json = jQuery.parseJSON(data);
                if (json.status == true) {
                    $('addedMenulist').html(json.HTML);

//                    $('html, body').animate({
//                        scrollTop: $("addedMenulist").offset().top
//                    }, 2000);
                } else {

                }
            } catch (e) {
                alert(e.message)
            }
        }
    });


}
$('.cart_icon').click(function (e) {
    e.preventDefault();
    $('.__cart_droplist').show();
});

$('#building_type').change(function () {
    var type = this.value;
    ;
    if (type == 2) {
        $('#building').hide();
        $('#apartment').hide();
        $('#house').show();
        $('#floor').hide();
        $('#office').hide();
    } else if (type == 3)
    {
        $('#apartment').hide();
        $('#building').show();
        $('#house').hide();
        $('#floor').show();
        $('#office').show();
    } else {
        $('#apartment').show();
        $('#building').show();
        $('#house').hide();
        $('#floor').show();
        $('#office').hide();
    }
})
$('#save_address').on('click', function () {
    var json = '';
    var json = json + '{';
    if ($('#mobile_number').val() == '') {
        $('#mobile_number').css('border-color', 'red');
        $('#mobile_number').focus();
        $('.common_message').css('color', 'red');
        $('.common_message').html(phone_required);
        return false;
    } else {
        if (!isValidMobile($('#mobile_number').val()))
        {
            $('#mobile_number').css('border-color', 'red');
            $('.common_message').css('color', 'red');
            $('.common_message').html(mobile_validation);
            return false;
        } else {
            $('#mobile_number').css('border-color', '');
            json = json + '"mobile_number":"' + $('#mobile_number').val() + '",';
            $('.common_message').html('');
        }

    }
    if (window.location.pathname == '/alfabee/saved_address')
    {
        if ($('.location').val() == '') {
            $('.location').css('border-color', 'red');
            $('.location').focus();
            $('.common_message').css('color', 'red');
            $('.common_message').html(location_required);
            return false;
        } else {
            $('.location').css('border-color', '');
            json = json + '"address":"' + $('.location').val() + '",';
        }
    } else {
        if ($('#location_area').val() == '') {
            $('#location_area').css('border-color', 'red');
            $('#location_area').focus();
            $('.common_message').css('color', 'red');
            $('.common_message').html(location_required);
            return false;
        } else {
            $('#location_area').css('border-color', '');
            if ($('#location_area').val() != undefined) {
                json = json + '"address":"' + $('#location_area').val() + '",';
            } else {
                json = json + '"address":"' + $('#location').val() + '",';
            }
        }
    }

    if ($('#street').val() == '') {
        $('#street').css('border-color', 'red');
        $('#street').focus();
        $('.common_message').css('color', 'red');
        $('.common_message').html(street_required);
        return false;
    } else {
        $('#street').css('border-color', '');
        json = json + '"street":"' + $('#street').val() + '",';
    }

    if ($('#building_type').val() == '') {
        $('#building_type').css('border-color', 'red');
        $('#building_type').focus();
        $('.common_message').css('color', 'red');
        $('.common_message').html(select_building);
        return false;
    } else {
        $('#building_type').css('border-color', '');
        json = json + '"address_label":"' + $('#building_type').val() + '",';
    }
    if ($('#building_type').val() != 2)
    {
        if ($('#building_name').val() == '') {
            $('#building_name').css('border-color', 'red');
            $('#building_name').focus();
            $('.common_message').css('color', 'red');
            $('.common_message').html(building_name_required);
            return false;
        } else {
            $('#building_name').css('border-color', '');
            json = json + '"building":"' + $('#building_name').val() + '",';
        }
    }
    if ($('#building_type').val() == 1)
    {
        if ($('#apartment_no').val() == '') {
            $('#apartment_no').css('border-color', 'red');
            $('#apartment_no').focus();
            $('.common_message').css('color', 'red');
            $('.common_message').html(apartment_name_required);
            return false;
        } else {
            $('#apartment_no').css('border-color', '');
            json = json + '"apartment_no":"' + $('#apartment_no').val() + '",';
        }
    }
    if ($('#building_type').val() == 2)
    {
        if ($('#house_name').val() == '') {
            $('#house_name').css('border-color', 'red');
            $('#house_name').focus();
            $('.common_message').css('color', 'red');
            $('.common_message').html(house_no_required);
            return false;
        } else {
            $('#house_name').css('border-color', '');
            json = json + '"house":"' + $('#house_name').val() + '",';
        }
    }
    if ($('#building_type').val() == 3)
    {
        if ($('#office_name').val() == '') {
            $('#office_name').css('border-color', 'red');
            $('#office_name').focus();
            $('.common_message').css('color', 'red');
            $('.common_message').html(office_name_required);
            return false;
        } else {
            $('#office_name').css('border-color', '');
            json = json + '"office":"' + $('#office_name').val() + '",';
        }
    }
    if ($('#building_type').val() != 2)
    {
        if ($('#floor_no').val() == '') {
            $('#floor_no').css('border-color', 'red');
            $('#floor_no').focus();
            $('.common_message').css('color', 'red');
            $('.common_message').html(floor_required);
            return false;
        } else {
            $('#floor_no').css('border-color', '');
            json = json + '"floor":"' + $('#floor_no').val() + '",';
        }
    }
    if ($('#landphone_no').val() != '') {
        var land_no = $('#landphone_no').val();
    } else {
        var land_no = 0;
    }
    json = json + '"mobile_code":"' + $('#mobile_code').val() + '",';
    json = json + '"landphone_no":"' + land_no + '",';
    json = json + '"latitude":"' + $('#location-lat').val() + '",';
    json = json + '"longitude":"' + $('#location-lng').val() + '",';
    if ($('#additional_direction').val() != 'undefined')
    {
        var additional = $('#additional_direction').val();
    } else {
        var additional = '';
    }
    json = json + '"additional_direction":"' + additional + '"';

    json = json + '}';
    var content = $('#content').val();
    var address_id = $(this).data("address");
    var data = {json: json, content: content, address_id: address_id};

    $.ajax({
        url: base_url + "Frontend/saveAddress",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {

            try {
                var json = jQuery.parseJSON(data);
                if (json.status == 'success') {
                    $('#exampleModalCenter').modal('hide');
                    // $('.common_message').css('color', 'green');
                    // $('.common_message').html(json.message);
                    setTimeout(function () {
                        window.location = '';
                    }, 1000);
                } else {
                    $('.common_message').css('color', 'red');
                    $('.common_message').html(json.message);
                    $('#exampleModalCenter').modal('show');
                }
            } catch (e) {
                alert(e)
            }
        }
    });
});
$(document).ready(function () {
    var radio_selected = $('input[name=inlineRadioOptions]:checked').val();
    if (radio_selected == 0) {
        $('#now').show();
    } else {
        $('#preorder').show();
    }

    $("[name=inlineRadioOptions]").change(function () {

        var radio_selected = $('input[name=inlineRadioOptions]:checked').val();
        if (radio_selected == 0) {
            $('#now').show();
            $('#preorder').hide();
        } else {
            $('#preorder').show();
            $('#now').hide();
        }

    });

    var pathname = window.location.pathname;
    var url = pathname.split('/');
    if (url[2] == 'search_details')
    {
        $('.cart_icon').hide();
    } else {
        $('.cart_icon').show();
    }
});
$('.place_order').click(function () {

    var party_or_not = $('#party_or_not').val();
    var store_type = $('#store_type').val();
    if (party_or_not != 1)
    {
        if ($('#address_id').val() != undefined)
        {
            var busy_status = $('#busy_status').val();
            var closed_status = $('#closed_status').val();
            var prescriptionreq = $('#prescriptionreq').val();
            if (busy_status == '1') {
                $('#busyModalAlert').modal('show');
                return false
            } else if (closed_status == '1') {
                $('#busyModalAlert').modal('show');
                return false
            }


            var url = $(this).attr('url');
            var radio_selected = $('input[name=inlineRadioOptions]:checked').val();

            var delivery_type = $('input[name=deliveryRadio]:checked').val();

            var special_request = $('.special_request').val();
            var schedule_delivery_date = $('#schedule_date').val();
            var schedule_delivery_time = $('#schedule_time').val();
            var dlcharges = $('#service_charge_amouttt').val();
            if (store_type != '1') {
                if ($('#schedule_time').val() == '') {
                    alertSimple('please choose schedule time.');
                    return false;
                }
            }
            if (radio_selected != '')
            {
                var pre_order = $('#pre_order').val();
            } else {
                var pre_order = '';
            }
            if (delivery_type == 1)
            {
                var address_id = $('#address_id').val();
            } else {
                var address_id = '';
            }
            var dayu = {delivery_time: radio_selected, special_request: special_request, pre_order: pre_order, address_id: address_id, schedule_delivery_date: schedule_delivery_date, schedule_delivery_time: schedule_delivery_time, delivery_type: delivery_type, dlcharges: dlcharges};
            dayu['store_type'] = store_type;
            if (store_type != '1') {
                dayu['schedule_date'] = $('#schedule_date').val();
                dayu['schedule_date_time'] = $('#schedule_time').val();
                dayu['scheduled_deliveryend_time'] = $('#schedule_time option:selected').attr('endtime');
            }
            dayu['upload_prescription'] = $('#UploadExcel').attr('oldfile');
            if (dayu['upload_prescription'] == '' && store_type == '3' && prescriptionreq == '1') {
                alertSimple('Prescription file is required');
                return false;
            }



            $("#loader").css('display', 'block');
            $.ajax({
                url: url,
                type: 'POST',
                // async: false,
                data: dayu,
                beforeSend: function () {
                    $("#loader").css('display', 'block');

                },
                success: function (data) {
                    $("#loader").css('display', 'block');
                    try {
                        $("#loader").css('display', 'block');
                        var resp = jQuery.parseJSON(data);
                        if (resp.status == false && resp.type == '1') {
                            $("#loader").css('display', 'none');
                            $('#busyModalAlert').modal('show');
                        } else if (resp.status == 'success') {
                            $("#loader").css('display', 'none');
                            window.location = base_url + resp.redirect_url;
                        }
                    } catch (e) {
                        alert(e.message)
                    }
                    // $("#loader").css('display','block');
                    // var resp = jQuery.parseJSON(data);
                    // if(resp.status=='success')
                    // {
                    //     $("#loader").css('display','none');
                    //     window.location= base_url+resp.redirect_url;
                    // }

                }
            });

        } else {
            $("#map_panel").modal("show");
        }
    } else {
        var party_time_end = $('#party_time_end').val();
        if ($('#no_of_people').val() == '') {
            $('#no_of_people').css('border-color', 'red');
            $('#no_of_people').focus();
            return false;
        } else {
            $('#no_of_people').css('border-color', '');
            var no_of_people = $('#no_of_people').val();
        }
        if ($('#party_date').val() == '') {
            $('#party_date').css('border-color', 'red');
            $('#party_date').focus();
            return false;
        } else {
            $('#party_date').css('border-color', '');
            var party_date = $('#party_date').val();
        }
        if ($('#party_time').val() == '') {
            $('#party_time').css('border-color', 'red');
            $('#party_time').focus();
            return false;
        } else {
            $('#party_time').css('border-color', '');
            var party_time = $('#party_time').val();
        }
        if ($('#party_time_end').val() == '') {
            $('#party_time_end').css('border-color', 'red');
            $('#party_time_end').focus();
            return false;
        } else {
            $('#party_time_end').css('border-color', '');

        }
        var busy_status = $('#busy_status').val();
        var closed_status = $('#closed_status').val();
//        if (busy_status == '1')
//        { 
//            $('#busyModalAlert').modal('show');
//        } else if (closed_status == '1')
//        {
//            $('#busyModalAlert').modal('show');
//        }
//        else {

        var url = $(this).attr('url');
        var special_request = $('.special_request').val();
        var data = {special_request: special_request, no_of_people: no_of_people, party_date: party_date, party_time: party_time, party_time_end: party_time_end};
        data['upload_prescription'] = $('#UploadExcel').attr('oldfile');
        $("#loader").css('display', 'block');
        $.ajax({
            url: url,
            type: 'POST',
            // async: false,
            data: data,
            beforeSend: function () {
                $("#loader").css('display', 'block');

            },
            success: function (data) {
                $("#loader").css('display', 'block');
                try {
                    $("#loader").css('display', 'block');
                    var resp = jQuery.parseJSON(data);
                    if (resp.status == false && resp.type == '1') {
                        $("#loader").css('display', 'none');
                        $('#busyModalAlert').modal('show');
                    } else if (resp.status == 'success') {
                        $("#loader").css('display', 'none');
                        window.location = base_url + resp.redirect_url;
                    }
                } catch (e) {
                    alert(e.message)
                }

            }
        });
        // }

    }
});

$('.drop_close').click(function () {
    $('.__cart_droplist').hide();
    event.stopPropagation();
})
$(document).ready(function (e) {
    if (window.location.hash == '#_=_' || window.location.hash == '#') {
        window.location.hash = ''; // for older browsers, leaves a # behind
        history.pushState('', document.title, window.location.pathname); // nice and clean
    }
});
$('.partyOrderSubmit').on('click', function () {

    if ($('#occasion').val() == '') {
        $('#occasion').css('border-color', 'red');
        $('.error_occasion').css('color', 'red');
        $('.error_occasion').html(select_occasion);
        $('#occasion').focus();
        return false;
    } else {
        $('#occasion').css('border-color', '');
        $('.error_occasion').html('');
        var occasion = $('#occasion').val();
    }
    if ($('.party_date').val() == '') {
        $('.party_date').css('border-color', 'red');
        $('.error_date').css('color', 'red');
        $('.error_date').html(select_party_date);
        $('.party_date').focus();
        return false;
    } else {
        $('.party_date').css('border-color', '');
        $('.error_date').html('');
        var party_date = $('.party_date').val();
    }
    if ($('#adult_num').val() == '') {
        $('#adult_num').css('border-color', 'red');
        $('.error_adult').css('color', 'red');
        $('.error_adult').html(no_of_adults);
        $('#adult_num').focus();
        return false;
    } else {
        $('#adult_num').css('border-color', '');
        $('.error_adult').html('');
        var no_of_adult = $('#adult_num').val();
    }
    if ($('#children_num').val() == '') {
        $('#children_num').css('border-color', 'red');
        $('.error_child').css('color', 'red');
        $('.error_child').html(no_of_childrens);
        $('#children_num').focus();
        return false;
    } else {
        $('#children_num').css('border-color', '');
        $('.error_child').html('');
        var no_of_children = $('#children_num').val();
    }
    if ($('.party_time').val() == '') {
        $('.party_time').css('border-color', 'red');
        $('.error_time').css('color', 'red');
        $('.error_time').html(select_party_time);
        $('.party_time').focus();
        return false;
    } else {
        $('.party_time').css('border-color', '');
        $('.error_time').html('');
        var party_time = $('.party_time').val();
    }
    if ($('#venue').val() == '') {
        $('#venue').css('border-color', 'red');
        $('.error_venue').css('color', 'red');
        $('.error_venue').html(enter_venue);
        $('#venue').focus();
        return false;
    } else {
        $('#venue').css('border-color', '');
        $('.error_venue').html('');
        var party_venue = $('#venue').val();
    }
    if ($('#cuisine').val() == '') {
        $('#cuisine').css('border-color', 'red');
        $('.error_cuisine').css('color', 'red');
        $('.error_cuisine').html(select_cuisine);
        $('#cuisine').focus();
        return false;
    } else {
        $('#cuisine').css('border-color', '');
        $('.error_cuisine').html('');
        var cuisine = $('#cuisine').val();
    }
    var cuisine_id = cuisine != null ? cuisine.join(',') : '';
    var user_id = $('#user_id').val();
    var vendor_id = $('#vendor_id').val();
    var latitude = $('#lat').val();
    var longitude = $('#lng').val();
    var data = {
        occasion_id: occasion,
        party_date: party_date,
        no_of_adult: no_of_adult,
        no_of_children: no_of_children,
        party_time: party_time,
        party_venue: party_venue,
        cuisine_id: cuisine_id,
        user_id: user_id,
        vendor_id: vendor_id,
        latitude: latitude,
        longitude: longitude
    };

    $.ajax({
        url: base_url + "party_order",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            try {
                var json = jQuery.parseJSON(data);

                if (json.status == 'success') {
                    $('.message').css('color', 'green');
                    $('.message').html(json.message);
                    setTimeout(function () {
                        window.location = '';
                    }, 2000);


                } else if (json.status == 'error' && json.redirecturl != 'undefined') {
                    sessionStorage.setItem("vendor_id", vendor_id);
                    window.location.href = base_url + json.redirecturl;

                } else {

                    $('.message').css('color', 'red');
                    $('.message').html(json.message);
                }


            } catch (e) {
                alert(e)
            }
        }
    });


});
$('.pay_now_request').click(function () {

    var party_id = $('#party_id').val();
    var data = {status: 'requestPayment', party_id: party_id};
    $.ajax({
        url: base_url + "Frontend/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            try {
                var json = jQuery.parseJSON(data);

                if (json.status == 'success') {

                    setTimeout(function () {
                        window.location = base_url + 'my_party_orders';
                    }, 2000);
                }

            } catch (e) {
                alert(e)
            }
        }
    });
});
function changeSortCriteria(obj, e)
{
    e.preventDefault();
    var location = $('#location').val();
    var latitude = $('#latitude').val();
    var longitude = $('#longitude').val();
    var locality = $('#locality').val();
    var store_type = $('#store_type').val();
    // var cuisine_type = $("input[name='cuisinetype']:checked").val();
    var cuisine_type = $('input:checkbox:checked').map(function () {
        return this.value;
    }).get().join(",");
    var rest = $('#restSearchBox').val();
    $('a').removeClass('actv');
    e.target.className = "actv";

    window.location = base_url + 'search_result?filter=' + obj;
    return false;
    var data = {status: 'filter_search', filter: obj, location: location, cuisine_type: cuisine_type, store_type: store_type, rest: rest, latitude: latitude, longitude: longitude, locality: locality};
    $(this).addClass("actv");
    $.ajax({
        url: base_url + "Frontend/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            $('.location').hide();
            $('.cuisine_type').html(data);
            // $('.location').html(data);

        }
    });
}
$("#add_address, #show_map").on("click", function () {
    $("#map_panel").modal("show");
    $('#exampleModalCenter').modal('hide');
});
function showAddressPopup(e, d) {
    if (e == false)
    {
        $('.modal-title').text(add_new_address);
        $('#isEditing').val(0);
        $('.confirm_location').attr("data-address", '');
    } else {
        $('.modal-title').text(edit);
        $('#isEditing').val(1);
        var address_id = d.getAttribute("data-address")
        $('.confirm_location').attr("data-address", address_id);
    }
}
function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

$('.confirm_location').on('click', function () {
    var delivery_radius = $('#delivery_radius').val();
    var pathname = window.location.pathname;
    var url = pathname.split('/');
    if (url[2] == 'checkout')
    {
        var store_lat = $('#store_lat').val();
        var store_lng = $('#store_lng').val();
        var delv_lat = $('#location_lat').val();
        var delv_lng = $('#location_lng').val();

        var R = 6371; // Radius of the earth in km
        var dLat = deg2rad(delv_lat - store_lat);  // deg2rad below
        var dLon = deg2rad(delv_lng - store_lng);
        var a =
                Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(deg2rad(store_lat)) * Math.cos(deg2rad(delv_lat)) *
                Math.sin(dLon / 2) * Math.sin(dLon / 2)
                ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var distance = R * c; // Distance in km
//        if (distance > delivery_radius)
//        {
//            alert('Sorry,we dont deliver here');
//            $("#map_panel").modal("show");
//        } else {
        $('#exampleModalCenter').modal('show');
        $("#map_panel").modal("hide");
        var isEditing = $('#isEditing').val();
        if (isEditing == 0)
        {
            $('.modal-title').text(add_new_address);
            $('#mobile_number').val('');
            $('#landphone_no').val('');
            $('.location').val('');
            $('#building_name').val('');
            $('#apartment_no').val('');
            $('#floor_no').val('');
            $('#office_name').val('');
            $('#additional_address').val('');
        } else {
            $('.modal-title').text(edit);
            var address_id = $(this).data("address");

            // var address_id = $('#address_id').val();

            var data = {status: 'OrderAddress', address_id: address_id};
            $.ajax({
                url: base_url + "Frontend/Helper",
                type: 'POST',
                async: false,
                data: data,
                success: function (data) {
                    var json = jQuery.parseJSON(data);

                    $('#mobile_number').val(json[0]['mobile_number']);
                    if (json[0]['landphone_no'] != 0)
                    {
                        var land = json[0]['landphone_no'];
                    } else {
                        var land = '';
                    }
                    $('#landphone_no').val(land);
                    // $('#area').val(json[0]['address']);
                    if (json[0]['building'] != '')
                    {
                        $('#building_name').val(json[0]['building']);
                    }
                    if (json[0]['apartment_no'] != '')
                    {
                        $('#apartment_no').val(json[0]['apartment_no']);

                    }
                    if (json[0]['floor_no'] != '')
                    {
                        $('#floor_no').val(json[0]['floor']);

                    }
                    if (json[0]['office'] != '')
                    {
                        $('#office_name').val(json[0]['office']);

                    }
                    if (json[0]['house'] != '')
                    {
                        $('#house_name').val(json[0]['house']);

                    }
                    if (json[0]['additional_address'] != '')
                    {
                        $('#additional_address').val(json[0]['additional_address']);

                    }
                    $('#building_type').val(json[0]['address_label']).attr("selected", "selected");
                    $('#content').val('edit');
                    var type = json[0]['address_label'];
                    if (type == 2) {
                        $('#building').hide();
                        $('#apartment').hide();
                        $('#house').show();
                        $('#floor').hide();
                        $('#office').hide();
                    } else if (type == 3)
                    {
                        $('#apartment').hide();
                        $('#building').show();
                        $('#house').hide();
                        $('#floor').show();
                        $('#office').show();
                    } else {
                        $('#apartment').show();
                        $('#building').show();
                        $('#house').hide();
                        $('#floor').show();
                        $('#office').hide();
                    }
                    $('#save_address').attr("data-address", address_id);
                }
            });

        }
        // }

    } else {
        $('#exampleModalCenter').modal('show');
        $("#map_panel").modal("hide");
        var isEditing = $('#isEditing').val();
        if (isEditing == 0)
        {
            $('.modal-title').text(add_new_address);
            $('#mobile_number').val('');
            $('#landphone_no').val('');
            $('.location').val('');
            $('#building_name').val('');
            $('#apartment_no').val('');
            $('#floor_no').val('');
            $('#office_name').val('');
            $('#additional_address').val('');
        } else {
            $('.modal-title').text(edit);
            var address_id = $(this).data("address");

            // var address_id = $('#address_id').val();

            var data = {status: 'OrderAddress', address_id: address_id};
            $.ajax({
                url: base_url + "Frontend/Helper",
                type: 'POST',
                async: false,
                data: data,
                success: function (data) {
                    var json = jQuery.parseJSON(data);

                    $('#mobile_number').val(json[0]['mobile_number']);
                    if (json[0]['landphone_no'] != 0)
                    {
                        var land = json[0]['landphone_no'];
                    } else {
                        var land = '';
                    }
                    $('#landphone_no').val(land);
                    // $('#area').val(json[0]['address']);
                    if (json[0]['building'] != '')
                    {
                        $('#building_name').val(json[0]['building']);
                    }
                    if (json[0]['apartment_no'] != '')
                    {
                        $('#apartment_no').val(json[0]['apartment_no']);

                    }
                    if (json[0]['floor_no'] != '')
                    {
                        $('#floor_no').val(json[0]['floor']);

                    }
                    if (json[0]['office'] != '')
                    {
                        $('#office_name').val(json[0]['office']);

                    }
                    if (json[0]['house'] != '')
                    {
                        $('#house_name').val(json[0]['house']);

                    }
                    if (json[0]['additional_address'] != '')
                    {
                        $('#additional_address').val(json[0]['additional_address']);

                    }
                    $('#building_type').val(json[0]['address_label']).attr("selected", "selected");
                    $('#content').val('edit');
                    var type = json[0]['address_label'];
                    if (type == 2) {
                        $('#building').hide();
                        $('#apartment').hide();
                        $('#house').show();
                        $('#floor').hide();
                        $('#office').hide();
                    } else if (type == 3)
                    {
                        $('#apartment').hide();
                        $('#building').show();
                        $('#house').hide();
                        $('#floor').show();
                        $('#office').show();
                    } else {
                        $('#apartment').show();
                        $('#building').show();
                        $('#house').hide();
                        $('#floor').show();
                        $('#office').hide();
                    }
                    $('#save_address').attr("data-address", address_id);
                }
            });

        }
    }

    // console.log('d');exit;
    // {
    //     var delivery_location = JSON.parse($('#delivery_location').val());

    //     var lat = parseFloat($('#location_lat').val());
    //     var lng = parseFloat($('#location_lng').val());

    //     // var found = false;
    //     // for (var i = 0; i < delivery_location.length; i++) {

    //     //     // This if statement depends on the format of your array
    //     //     if (delivery_location[i][0] == lat && delivery_location[i][1] == lng) {
    //     //         found = true;   // Found it
    //     //     }
    //     // }
    //     //   var x = point[0], y = point[1];
    //     //console.log(delivery_location.length);exit;
    //     var inside = false;
    //     for (var i = 0, j = delivery_location.length - 1; i < delivery_location.length; j = i++) {
    //         var xi = delivery_location[i][0], yi = delivery_location[i][1];
    //         var xj = delivery_location[j][0], yj = delivery_location[j][1];

    //         var intersect = ((yi > lng) != (yj > lng))
    //             && (lat < (xj - xi) * (lng - yi) / (yj - yi) + xi);
    //         if (intersect) inside = !inside;
    //     }

    //     if(inside)
    //     {
    //         $('#exampleModalCenter').modal('show');
    //         $("#map_panel").modal("hide");
    //         var isEditing = $('#isEditing').val();
    //         if(isEditing==0)
    //         {
    //             $('.modal-title').text(add_new_address);
    //             $('#mobile_number').val('');
    //             $('#landphone_no').val('');
    //             $('.location').val('');
    //             $('#building_name').val('');
    //             $('#apartment_no').val('');
    //             $('#floor_no').val('');
    //             $('#office_name').val('');
    //             $('#additional_address').val('');
    //         }else{
    //             $('.modal-title').text(edit);
    //             var address_id = $(this).data("address");

    //             // var address_id = $('#address_id').val();

    //             var data = {status: 'OrderAddress', address_id: address_id};
    //             $.ajax({
    //                 url: base_url + "Frontend/Helper",
    //                 type: 'POST',
    //                 async: false,
    //                 data: data,
    //                 success: function (data) {
    //                     var json = jQuery.parseJSON(data);

    //                     $('#mobile_number').val(json[0]['mobile_number']);
    //                     if(json[0]['landphone_no']!=0)
    //                     {
    //                         var land = json[0]['landphone_no'];
    //                     }else{
    //                         var land = '';
    //                     }
    //                     $('#landphone_no').val(land);
    //                     // $('#area').val(json[0]['address']);
    //                     if(json[0]['building']!='')
    //                     {
    //                         $('#building_name').val(json[0]['building']);
    //                     }
    //                     if(json[0]['apartment_no']!='')
    //                     {
    //                         $('#apartment_no').val(json[0]['apartment_no']);

    //                     }
    //                     if(json[0]['floor_no']!='')
    //                     {
    //                         $('#floor_no').val(json[0]['floor']);

    //                     }
    //                     if(json[0]['office']!='')
    //                     {
    //                         $('#office_name').val(json[0]['office']);

    //                     }
    //                     if(json[0]['house']!='')
    //                     {
    //                         $('#house_name').val(json[0]['house']);

    //                     }
    //                     if(json[0]['additional_address']!='')
    //                     {
    //                         $('#additional_address').val(json[0]['additional_address']);

    //                     }
    //                     $('#building_type').val(json[0]['address_label']).attr("selected", "selected");
    //                     $('#content').val('edit');
    //                     var type = json[0]['address_label'];
    //                     if(type==2){
    //                         $('#building').hide();
    //                         $('#apartment').hide();
    //                         $('#house').show();
    //                         $('#floor').hide();
    //                         $('#office').hide();
    //                     }else if(type==3)
    //                     {
    //                         $('#apartment').hide();
    //                         $('#building').show();
    //                         $('#house').hide();
    //                         $('#floor').show();
    //                         $('#office').show();
    //                     }else{
    //                         $('#apartment').show();
    //                         $('#building').show();
    //                         $('#house').hide();
    //                         $('#floor').show();
    //                         $('#office').hide();
    //                     }
    //                     $('#save_address').attr("data-address",address_id);
    //                 }
    //             });

    //         }
    //     }else{
    //         alert(pinned_location_outside);
    //         $("#map_panel").modal("show");
    //     }
    // }else{

    // }
})

$('.submit_feedback').on('click', function () {

    if ($('input[name="rating"]:checked').length == 0) {
        $('.error_rating').css('color', 'red');
        $('.error_rating').html(select_rating);
        return false;
    } else {
        $('.error_rating').html('');
        var rating = $("input[name='rating']:checked").val();
    }
    if ($('.comments').val() == '') {
        $('.comments').css('border-color', 'red');
        $('.error_comments').css('color', 'red');
        $('.error_comments').html(enter_comments);
        $('.comments').focus();
        return false;
    } else {
        $('.comments').css('border-color', '');
        $('.error_comments').html('');
        var comments = $('.comments').val();
    }
    var vendor_id = $('#vendor_id').val();
    var data = {vendor_id: vendor_id, rating: rating, comments: comments};

    $.ajax({
        url: base_url + "Frontend/userFeedback",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            try {
                var json = jQuery.parseJSON(data);

                if (json.status == 'success') {
                    $('.message').css('color', 'green');
                    $('.message').html(json.message);
                    setTimeout(function () {
                        window.location = '';
                    }, 2000);


                } else if (json.status == 'error' && json.redirecturl != 'undefined') {

                    window.location.href = base_url + json.redirecturl;

                } else {

                    $('.message').css('color', 'red');
                    $('.message').html(json.message);
                }


            } catch (e) {
                alert(e)
            }
        }
    });

});

$('#venue').click(function () {
    $('#party_venue_map').modal('show');
})

$('#apply_couponcode').click(function (e) {
    e.preventDefault();

    var promocode = $('#promo_code').val();
    var rest_id = $('#rest_id').val();
    var total_amouttt = $('#total_amouttt').val();
    var data = {status: 'ApplyPromoCode', promo_code: promocode, rest_id: rest_id, total_amouttt: total_amouttt};
    data['service_charge'] = $('#service_charge_amouttt').val();
    $.ajax({
        url: base_url + "Frontend/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {

            try {
                var json = jQuery.parseJSON(data);
                if (json.status == 'success') {
                    $('.message').css('color', 'green');
                    $('.message').html(json.message);
                    $('#promocode').hide();
                    $('#apply_promocode').show();
                    setTimeout(function () {
                        window.location = '';
                    }, 1000);


                } else {
                    $('.message').css('color', 'red');
                    $('.message').html(json.message);
                }


            } catch (e) {
                alert(e)
            }
        }
    });
});
$('#remove_promocode').click(function (e) {
    e.preventDefault();

    var promocode_id = $('#promocode_id').val();

    var data = {status: 'RemovePromoCode', promocode_id: promocode_id};
    $.ajax({
        url: base_url + "Frontend/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {

            try {
                var json = jQuery.parseJSON(data);
                if (json.status == 'success') {
                    $('.message').css('color', 'green');
                    $('.message').html(json.message);
                    $('#promocode').show();
                    $('#apply_promocode').hide();
                    setTimeout(function () {
                        window.location = '';
                    }, 1000);


                } else {
                    $('.message').css('color', 'red');
                    $('.message').html(json.message);
                }


            } catch (e) {
                alert(e)
            }
        }
    });
});
$('#remove_promocode').click(function (e) {
    e.preventDefault();
});
$('#CancelledMyOrder').click(function () {
    var OrderId = $(this).attr('orderid');
    var data = {status: 'CancelledMyOrder', OrderId: OrderId};


    $.confirm({
        title: cancel_this_order,
        type: 'red',
        typeAnimated: true,
        content: '' +
                '<form action="" class="formName" id="formName">' +
                '<div class="form-group">' +
                '<label for="comment">' + why_cancel + ':</label><br/>' +
                '<input type="radio" name="WritenQuery" id="WritenQuery" value="The order took too long" /> ' + order_took_to_long + ' <br/>' +
                '<input type="radio" name="WritenQuery" id="WritenQuery" value="I changed my mind" /> ' + changed_my_mind + '<br/>' +
                '<input type="radio" name="WritenQuery" id="WritenQuery" value="The price is too high" /> ' + price_high + '<br/>' +
                '<input type="radio" name="WritenQuery" id="WritenQuery" value="I found it cheaper somewhere else" /> ' + found_somewhere_else + ' <br/>' +
                '<input type="radio" name="WritenQuery" id="WritenQuery" value="Other" /> ' + other + '<br/>' +
                other_reason + ' <textarea class="form-control WritenQuery" rows="5" id="WritenQuery"></textarea>' +
                '</div>' +
                '</form>',
        buttons: {
            confirm: {
                text: submit,
                btnClass: 'btn-success',
                action: function () {
                    var WritenQueryop = this.$content.find($('input[name=WritenQuery]:checked', '#formName')).val();
                    var WritenQuery = '';
                    if (WritenQueryop == 'Other') {
                        var WritenQuery = this.$content.find('.WritenQuery').val();
                    }
                    data['WritenQuery'] = WritenQuery;
                    data['WritenQueryOp'] = WritenQueryop;

                    $.ajax({
                        url: base_url + "Frontend/Helper",
                        type: 'POST',
                        async: false,
                        data: data,
                        success: function (data) {
                            if (jQuery.parseJSON(data)) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    toastr.error(json.message, 'Error Alert', {timeOut: 5000})
                                    // AlertHeader(json.message, json.status);
                                    setTimeout(function () {
                                        window.location = '';
                                    }, 4000);
                                }
                            }
                        }
                    });

                }
            },
            cancel: {
                text: cancel,
                btnClass: 'btn-warning',
                action: function () {
                }
            },
        }
    });
});
$('.ContactUs').click(function () {
    if ($('#contact_name').val() == '') {
        $('#contact_name').css('border-color', 'red');
        $('#contact_name').focus();
        return false;
    } else {
        $('#contact_name').css('border-color', '');
        var contact_name = $('#contact_name').val();
    }
    if ($('#contact_email').val() == '') {
        $('#contact_email').css('border-color', 'red');
        $('#contact_email').focus();
        return false;
    } else {
        $('#contact_email').css('border-color', '');
        var contact_email = $('#contact_email').val();
    }
    if ($('#contact_mobile').val() == '') {
        $('#contact_mobile').css('border-color', 'red');
        $('#contact_mobile').focus();
        return false;
    } else {
        $('#contact_mobile').css('border-color', '');
        var contact_mobile = $('#contact_mobile').val();
    }
    if ($('#contact_email').val() == '') {
        $('#contact_email').css('border-color', 'red');
        $('#contact_email').focus();
        return false;
    } else {
        $('#contact_email').css('border-color', '');
        var contact_email = $('#contact_email').val();
    }
    if ($('#contact_message').val() == '') {
        $('#contact_message').css('border-color', 'red');
        $('#contact_message').focus();
        return false;
    } else {
        $('#contact_message').css('border-color', '');
        var contact_message = $('#contact_message').val();
    }
    var data = {
        name: contact_name,
        email: contact_email,
        mobile_number: contact_mobile,
        message: contact_message
    };

    $.ajax({
        url: base_url + "Frontend/contact_us",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {

            try {
                var resp = jQuery.parseJSON(data);

                if (resp.status == 'success') {
                    $('.register_message').css('color', 'green');
                    $('.register_message').html(resp.message);
                    setTimeout(function () {
                        // window.location= base_url+'Login/Vendor';
                        window.location = base_url;
                    }, 3000);
                } else {
                    $('.register_message').css('color', 'red');
                    $('.register_message').html(resp.message);
                }
            } catch (e) {
                alert(e.message)
            }
        }
    });
});

$('.Suggest_HomeBusiness').click(function () {
    if ($('#name').val() == '') {
        $('#name').css('border-color', 'red');
        $('#name').focus();
        return false;
    } else {
        $('#name').css('border-color', '');
        var name = $('#name').val();
    }
    if ($('#location').val() == '') {
        $('#location').css('border-color', 'red');
        $('#location').focus();
        return false;
    } else {
        $('#location').css('border-color', '');
        var location = $('#location').val();
    }
    var additional_comments = $('#additional_comments').val();
    var contact_details = $('#contact_details').val();
    var data = {
        name: name,
        location: location,
        contact_details: contact_details,
        additional_comments: additional_comments
    };

    $.ajax({
        url: base_url + "Frontend/suggestHomeBusiness",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {

            try {
                var resp = jQuery.parseJSON(data);

                if (resp.status == 'success') {
                    $('.register_message').css('color', 'green');
                    $('.register_message').html(resp.message);
                    // setTimeout(function () {
                    //     // window.location= base_url+'Login/Vendor';
                    //      window.location= base_url;
                    // }, 3000);
                } else {
                    $('.register_message').css('color', 'red');
                    $('.register_message').html(resp.message);
                }
            } catch (e) {
                alert(e.message)
            }
        }
    });
});
// $('#schedule_date').change(function(){
//     $( "#schedule" ).load(window.location.href + " #schedule" );
// });
$('.check_time, .check_date').change(function () {
    var schedule_delivery = $('#schedule_delivery').val();
    if (schedule_delivery == 0)
    {
        var party_date = $('#party_date').val();
        var party_time = $('#party_time').val();
        var data = {
            status: 'checkTime',
            party_date: party_date,
            party_time: party_time,
        };
        data['party_time_end'] = $('#party_time_end').val();
    } else {
        var party_date = $('#schedule_date').val();
        var party_time = $('#schedule_time').val();
        var data = {
            status: 'checkTime',
            party_date: party_date,
            party_time: party_time,
        };
        data['party_time_end'] = $('#schedule_time option:selected').attr('endtime');

    }
    data['resturent_id'] = $('#resturent_id').val();

    $.ajax({
        url: base_url + "Frontend/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {

            try {
                var resp = jQuery.parseJSON(data);

                if (resp.status == 'success')
                {
                    $('.party_error').hide();
                    $(".place_order").attr("disabled", false);
                    $(".place_order").css("cursor", "pointer");
                } else {
                    $('.party_error').show();
                    $('.party_error').css('color', 'red');
                    // $('.party_error').html(resp.message);
//                    $(".place_order").attr("disabled", true);
//                    $(".place_order").css("cursor", "default");
                }
                COMN.get_time_slot();
            } catch (e) {
                alert(e.message)
            }
        }
    });
});
$('.check_reservation_date, .check_reservation_time').change(function () {

    var vendor_id = $('#vendor_id').val();
    var table_capacity = $('#table_capacity').val();
    var no_of_people = $('#no_of_people').val();
    var reservation_date = $('#reservation_date').val();
    var reservation_time = $('#reservation_time').val();
    var data = {
        status: 'checkTableAvailability',
        vendor_id: vendor_id,
        table_capacity: table_capacity,
        no_of_people: no_of_people,
        reservation_date: reservation_date,
        reservation_time: reservation_time,
    };

    $.ajax({
        url: base_url + "Frontend/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {

            try {
                var resp = jQuery.parseJSON(data);

                if (resp.status == 'success')
                {
                    $('.error_reservation').hide();
                    $(".book_table").attr("disabled", false);
                    $(".book_table").css("cursor", "pointer");
                } else {
                    $('.error_reservation').show();
                    $('.error_reservation').css('color', 'red');
                    $('.error_reservation').html(resp.message);
                    $(".book_table").attr("disabled", true);
                    $(".book_table").css("cursor", "default");
                }
            } catch (e) {
                alert(e.message)
            }
        }
    });
})
$('.book_table').click(function () {
    if ($('#no_of_people').val() == '') {
        $('#no_of_people').css('border-color', 'red');
        $('#no_of_people').focus();
        return false;
    } else {
        $('#no_of_people').css('border-color', '');
        var no_of_people = $('#no_of_people').val();
    }
    if ($('#reservation_date').val() == '') {
        $('#reservation_date').css('border-color', 'red');
        $('#reservation_date').focus();
        return false;
    } else {
        $('#reservation_date').css('border-color', '');
        var reservation_date = $('#reservation_date').val();
    }
    if ($('#reservation_time').val() == '') {
        $('#reservation_time').css('border-color', 'red');
        $('#reservation_time').focus();
        return false;
    } else {
        $('#reservation_time').css('border-color', '');
        var reservation_time = $('#reservation_time').val();
    }

    var user_id = $('#user_id').val();
    var vendor_id = $('#vendor_id').val();
    var data = {
        user_id: user_id,
        vendor_id: vendor_id,
        no_of_people: no_of_people,
        reservation_date: reservation_date,
        reservation_time: reservation_time,
    };

    $.ajax({
        url: base_url + "Frontend/reservations",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {

            try {
                var json = jQuery.parseJSON(data);

                if (json.status == 'success') {
                    $('.message').css('color', 'green');
                    $('.message').html(json.message);
                    setTimeout(function () {
                        window.location = '';
                    }, 3000);


                } else if (json.status == 'error' && json.redirecturl != 'undefined') {

                    window.location.href = base_url + json.redirecturl;

                } else {
                    // $('.message').css('color', 'red');
                    // $('.message').html(json.message);
                    $('.error_reservation').show();
                    $('.error_reservation').css('color', 'red');
                    $('.error_reservation').html(json.message);
                }
            } catch (e) {
                alert(e.message)
            }
        }
    });
});

function ajaxpost(form, url) {
    var res = '';
    var xhr = new XMLHttpRequest();
    xhr.open('POST', base_url + url, false);
    xhr.onload = function () {
        res = xhr.responseText;
    };
    xhr.send(form);
    return res;
}