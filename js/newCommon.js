if (location.protocol !== "https:") {
    location.protocol = "https:";
}
var params = new window.URLSearchParams(window.location.search);
$(document).ready(function () {
    if (Method == 'searchResult') {
        var input = document.getElementById('business-location');
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            $('#latitudeTemp').val(place.geometry.location.lat());
            $('#longitudeTemp').val(place.geometry.location.lng());
        });
        $('#locationchageBtnclick').click(function () {
            var locName = $('#business-location').val();
            var latitude = $('#latitudeTemp').val();
            var longitude = $('#longitudeTemp').val();
            if (latitude == '' || longitude == '') {
                $('#business-location').focus();
                return false;
            }
            var data = {status: 'locationchageBtnclick', locName: locName};
            data['latitude'] = latitude;
            data['longitude'] = longitude;
            $.ajax({
                method: "POST",
                url: base_url + "Frontend/Helper",
                data: data,
                async: false,
                success: function (res) {
                    var json = JSON.parse(res);
                    if (json.status == true) {
                        window.location = '';
                    } else {

                    }
                },
                error: function (xhr) {
                    alert("Error occured.please try again");
                }
            });

        })
    }
if (Method == 'checkout') {
        COMN.get_time_slot();
        setTimeout(function(){
          window.location='';
        }, 60000);

    }
    COMN.popOver();
    // popovers initialization - on click
    $('[data-toggle="popover-click"]').popover({
        html: true,
        trigger: 'click',
        placement: 'bottom',
        content: function () {
            return '<img  src="' + $(this).data('img') + '" />';
        }
    });
    COMN.loadMenuajaxbyVendor();

//    $("#searchiteams").on("keyup", function () {
//        var value = $(this).val().toLowerCase();
//        $("#myTable tr").filter(function () {
//            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
//        });
//    });


    $('#searchiteams').keyup(function () {


        COMN.loadMenuajaxbyVendor();
    });
});
$('#myTab .nav-item').click(function () {
    var tabId = $(this).attr('id');
    // $('.checkoutbtn').show();
    if (tabId == 'tab12' || tabId == 'tab11') {
        sessionStorage.setItem("lasttabClick", tabId);
        //    $('.checkoutbtn').hide();
    }

});

$('body').on('keyup', '.search', function () {
    var val = $(this).val();
    $('table tbody tr').hide();
    var trs = $('table tbody tr').filter(function (d) {
        return $(this).text().toLowerCase().indexOf(val) != -1;
    });
    trs.show();
});
$('body').on('click', ".singlecheckbox input:checkbox", function () {
    var cuis = $(this).attr('cuis');
    var $box = $(this);

    if ($box.is(":checked")) {
        $('#restSearchBoxXXX').val(cuis.toLowerCase());

    } else {
        $('#restSearchBoxXXX').val('');
    }
    $('#restSearchBoxXXX').keyup();

    if ($box.is(":checked")) {
        var group = "input:checkbox[name='" + $box.attr("name") + "']";
        $(group).prop("checked", false);
        $box.prop("checked", true);
    } else {
        $box.prop("checked", false);
    }
});
if (Method == 'search_details') {
    var lasttabClick = sessionStorage.getItem("lasttabClick");
    if (lasttabClick != null) {
        $('#' + lasttabClick + ' a').click();
    }
}
if (params.get('partyorder') == '1') {
    $('#partyclickbuttin').click();
}

var COMN = (function () {
    var fn = {};
    fn.deliveryRadio1Chnage = function () {
        var delivery = $('input[name="deliveryRadio"]:checked').val();
        var data = {status: 'deliveryRadio1Chnage', delivery: delivery};
        $.ajax({
            method: "POST",
            url: base_url + "Frontend/Helper",
            data: data,
            async: false,
            success: function (res) {
                var json = JSON.parse(res);

                if (json.status == true) {
                    window.location = '';
                } else {

                }
            },
            error: function (xhr) {
                alert("Error occured.please try again");
            }
        });
    }
    fn.validationLoyalityPoints = function () {
        var total_amouttt = $('#total_amouttt').val();
        var loyalityAmt = $('#loyalityAmt').val();
        if ($('#loyalityAmt').val() == '') {
            $('#loyalityAmt').focus();
            return false;
        }
        var data = {status: 'validationLoyalityPoints', loyalityAmt: loyalityAmt, total_amouttt: total_amouttt};
        $.ajax({
            method: "POST",
            url: base_url + "Frontend/Helper",
            data: data,
            async: false,
            success: function (res) {
                var json = JSON.parse(res);
                alertSimple(json.response.message);
                if (json.response.status == true) {
                    window.location = '';
                } else {

                }
            },
            error: function (xhr) {
                alert("Error occured.please try again");
            }
        });
    }
    fn.removeLoyalityPoints = function () {
        var data = {status: 'removeLoyalityPoints'};
        $.ajax({
            method: "POST",
            url: base_url + "Frontend/Helper",
            data: data,
            async: false,
            success: function (res) {
                var json = JSON.parse(res);
                alert(json.message);
                if (json.status == true) {
                    window.location = '';
                } else {

                }
            },
            error: function (xhr) {
                alert("Error occured.please try again");
            }
        });
    }
    fn.setHomeAutouserAddress = function (e) {
        var latitude = $(e).attr('lat');
        var longitude = $(e).attr('long');
        var address = $(e).attr('adddres');
        $('#sublocality_level_1,#autocomplete').val(address);
        $('.latitude').val(latitude);
        $('.longitude').val(longitude);
        $('#party_venue_map').modal('hide');
    }
    fn.loadMenuajaxbyVendor = function () {
        var lastIndex = 0;
        var search = $('#searchiteams').val();
        $('.loadmenuajax').each(function () {
            var catId = $(this).attr('catId');
            var vendorId = $(this).attr('vendorId');
            var uniqid = $(this).attr('uniqid');
            var data = {status: 'loadFrontendMenulistHTML', catId: catId, vendorId: vendorId, search: search};
            setTimeout(function () {
                lastIndex++;
                $.ajax({
                    method: "POST",
                    url: base_url + "Frontend/Helper",
                    data: data,
                    async: true,
                    success: function (res) {
                        if (res != '') {
                            $('#collapse' + uniqid).html(res);
                            $('#imgloader' + uniqid).hide();
                            // $('#div_section_' + catId).click();
                            if ($('.loadmenuajax').length == lastIndex) {
                                COMN.popOver();
                            }
                            $('#mainPannel' + uniqid).show();
                            $('#cat-list-righ-' + catId).show();
                        } else {

                            $('#mainPannel' + uniqid).hide();
                            $('#cat-list-righ-' + catId).hide();
                        }
                    },
                    error: function (xhr) {
                        alert("Error occured.please try again");
                    }
                });
            }, 500);




        });
    }
    fn.popOver = function () {
        $('[data-toggle="popover-hover"]').popover({
            html: true,
            trigger: 'hover',
            placement: 'bottom',
            content: function () {
                return '<img  src="' + $(this).data('img') + '" />';
            }
        });
    }
    fn.chnageWebsitelanguage = function (e) {
        var choose_lang = $(e).attr('lan');
        var data = {status: 'removeLoyalityPoints', choose_lang: choose_lang};
        $.ajax({
            method: "POST",
            url: base_url + "Frontend/set_lang",
            data: data,
            async: false,
            success: function (res) {
                var json = JSON.parse(res);

                if (json.status == true) {
                    window.location = '';
                } else {

                }
            },
            error: function (xhr) {
                alert("Error occured.please try again");
            }
        });
    }
    fn.uploadquotationBillRequestIcon = function () {
        $('#UploadExcel').click();
    }
    fn.uploadquotationBillRequest = function () {
        var formData = new FormData();
        formData.append('status', 'uploadquotationBillRequest');


        var fileInput = document.querySelector('#UploadExcel');
        formData.append('UploadExcel', fileInput.files[0]);
        formData.append('oldfile', $('#UploadExcel').attr('oldfile'));

        if (fileInput.files[0] == undefined) {
            $('#UploadExcel').focus();
            return false;
        }
        var json = jQuery.parseJSON(ajaxpost(formData, 'Frontend/helper'));
        alertSimple(json.message);
        if (json.status == true) {
            $('#UploadExcel').attr('oldfile', json.file_path);
            // window.location = '';
        }
    }
    fn.get_time_slot = function () {
        var schedule_date = $('#schedule_date').val();
        var vendor_id = $('#resturent_id').val();
        var order_type = $('#order_type').val();
        order_type = order_type == '2' ? '1' : '0';
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": base_url + "Services/get_time_slot",
            "method": "POST",
            "headers": {
                "user-agents": "com.alfabee.com",
                "authtoken": "Alwafaa123",
                "content-type": "application/json",
                "cache-control": "no-cache",
                "postman-token": "1d0eb8df-c3b9-9dfb-6186-da041de355ef"
            },
            "processData": false,
            "data": '{"date":"' + schedule_date + '","user_type":"2","vendor_id":"' + vendor_id + '","order_type":"' + order_type + '"}'
        }

        $.ajax(settings).done(function (json) {
            try {

                if (json.response.status == true) {
                    var option = ' <option value="">Select Time</option>';
                    var times = json.result.times;
                    for (var i = 0; i < times.length; i++) {
                        var d = times[i];
                        option = option + '<option endTime="' + d.time_to + '" value="' + d.time_from + '">' + d.time_from + ' - ' + d.time_to + '</option>';
                    }
                    $('#schedule_time').html(option);
                    console.log(times)
                }
            } catch (e) {

            }

        });
    }
    return fn;
})();
function alertSimple(message) {
    $.dialog({
        title: false,
        content: message,
    });
}