$(function () {
    // var Obj = {dateFormat: 'dd-mm-yy'};
    var Obj = {dateFormat: 'yy-mm-dd', minDate: 0};
    $(".datepicker").datepicker(Obj);
    $(".datepicker1").datepicker(Obj);
});
if (Method == 'OrderDetails' || Method == 'partyOrderDetails') {
    setTimeout(function () {
        window.location = '';
    }, 35000);
}
if (sessionValue == 'ar')
{
    var sure_want_to_delete = 'هل أنت متأكد أنك تريد الحذف؟'
    var want_to_submit = 'هل أنت متأكد من إرساله؟ ';
    var reject_user = 'هل أنت متأكد أنك تريد رفض هذا المستخدم؟';
    var submit = 'تأكيد';
    var cancel = 'إلغاء';
    var accept_user = 'هل أنت متأكد أنك تريد قبول هذا المستخدم؟';
    var want_to_update = 'هل تريد التحديث؟';
    var reset_position = 'هل تريد إعادة التعيين بالفعل؟'
    var delete_data = 'حذف!!';
    var cancel_order = 'هل أنت متأكد من إلغاء الطلب؟';
    var cancel_date = 'إلغاء!!';
    var remove_image = 'هل تريد إزالة الصورة؟';
    var upload_image = 'يرجى تحميل صورة ';
} else {
    var sure_want_to_delete = 'Are you sure you want to delete this?';
    var want_to_submit = 'Are you sure want to submit ?';
    var reject_user = 'Are you sure you want to reject this user?'
    var submit = 'Submit';
    var cancel = 'Cancel';
    var accept_user = 'Are you sure you want to accept this user?';
    var want_to_update = 'are you sure want to update?';
    var reset_position = 'Do you want to reset position?';
    var delete_data = 'Delete!!';
    var cancel_order = 'Are you sure you want to cancel this order ?';
    var cancel_data = 'Cancel!!';
    var remove_image = 'Do you want to remove image?';
    var upload_image = 'Upload Image';
}
function readNotification(obj) {
    var id = $(obj).data("id");
    var redirect_url = $(obj).data("url");
    var data = {status: 'updateNotification', id: id, redirect_url: redirect_url};
    $.ajax({
        method: "POST",
        url: base_url + "Vendor/Helper",
        data: data,
        async: false,
        success: function (res) {
            var json = JSON.parse(res);
            if (json.status == 'success') {
                window.location = base_url + json.url;
            }

        },
        error: function (xhr) {
            alert("Error occured.please try again");
        }
    });
}
$('.CancelOrder').on('click', function () {
    var Id = $(this).attr('id');
    var message = $(this).attr('title');
    var table = $(this).attr('table');
    var url = $(this).attr('redrurl');
    var data = {
        status: 'CancelOrder',
        Id: Id,
        table: table
    };
    $.confirm({
        content: cancel_order,
        title: cancel_data,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: submit,
                action: function () {
                    $.ajax({
                        url: base_url + "Vendor/Helper",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {

                            var json = jQuery.parseJSON(data);
                            if (json.status == true) {
                                $('#success-message').html(json.message);
                                $('.alert-success').show();
                                $('.alert-success').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                    window.location = base_url + 'Vendor/' + url;
                                }, 2000);

                            } else {
                                $('.alert-danger').show();
                                $('#error-message').html(json.message)
                                $('.alert-danger').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                }, 2000);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: cancel,
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});
function isValidEmail(email) {
    return /^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i.test(email) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(email);
}
function isValidPassword(password) {
    return new RegExp('(?=.*[a-zA-Z])(?=.*\\d).{8,}').test(password);
}
$('#add_menu_images').click(function () {
    var classname = $(this).attr('inputclassname');

    var rand = Math.floor((Math.random() * 1000) + 1);
    $('#more_upload_section').append('<div class="form-group div_img_' + rand + '"><label class="control-label col-md-3">Upload Image</label><div class="col-md-6"><input type="file" class="custom-file-input MultipleImages" id="" refval="' + rand + '" src=""></div><div class="col-md-3"><a target="_blank" href=""><img src="" width="20%" id="ImagesEncode_' + rand + '" OldImage="" class="' + classname + '" newimage=""></a><a class="remove_image_upload" id="' + rand + '" ><span class="glyphicon glyphicon-trash"></span></a></div></div>');
});

$('.register_submit').on('click', function () {
    var json = '';
    var json = json + '{';
    if ($('#first_name').val() == '') {
        $('#first_name').css('border-color', 'red');
        $('#first_name').focus();
        return false;
    } else {
        $('#first_name').css('border-color', '');
        json = json + '"first_name":"' + $('#first_name').val() + '",';
    }
    if ($('#last_name').val() == '') {
        $('#last_name').css('border-color', 'red');
        $('#last_name').focus();
        return false;
    } else {
        $('#last_name').css('border-color', '');
        json = json + '"last_name":"' + $('#last_name').val() + '",';
    }
    if ($('#restaurant_name').val() == '') {
        $('#restaurant_name').css('border-color', 'red');
        $('#restaurant_name').focus();
        return false;
    } else {
        $('#restaurant_name').css('border-color', '');
        json = json + '"restaurant_name":"' + $('#restaurant_name').val() + '",';
    }
    if ($('#email').val() == '') {
        $('#email').css('border-color', 'red');
        $('#email').focus();
        return false;
    } else {
        if (!isValidEmail($('#email').val())) {
            $('#email').focus();
            $('#email').css('border-color', 'red');
            return false;
        } else {
            $('#email').css('border-color', '');
            json = json + '"email":"' + $('#email').val() + '",';
        }
    }
    if ($('#phone').val() == '') {
        $('#phone').css('border-color', 'red');
        $('#phone').focus();
        return false;
    } else {
        $('#phone').css('border-color', '');
        json = json + '"mobile_number":"' + $('#phone').val() + '",';
    }

    if ($('#city').val() == '') {
        $('#city').css('border-color', 'red');
        $('#city').focus();
        return false;
    } else {
        $('#city').css('border-color', '');
        json = json + '"city":"' + $('#city').val() + '",';
    }
    if ($('#area').val() == '') {
        $('#area').css('border-color', 'red');
        $('#area').focus();
        return false;
    } else {
        $('#area').css('border-color', '');
        json = json + '"area":"' + $('#area').val() + '",';
    }
    if ($('#street').val() == '') {
        $('#street').css('border-color', 'red');
        $('#street').focus();
        return false;
    } else {
        $('#street').css('border-color', '');
        json = json + '"street":"' + $('#street').val() + '",';
    }
    if ($('#password').val() == '') {
        $('#password').css('border-color', 'red');
        $('#password').focus();
        return false;
    } else {
        // if (!isValidPassword($('#password').val())) {
        if ($('#password').val() == '') {
            $('#password').css('border-color', 'red');
            $('#password').focus();
            // $('.register_message').css('color', 'red');
            // $('.register_message').html('Min 8 Characters,Atleast one letter,Atleast one number');
            return false;
        } else {
            $('#password').css('border-color', '');
            json = json + '"password":"' + $('#password').val() + '",';
            // $('.register_message').html('');
        }
    }
    if ($('#cpassword').val() == '') {
        $('#cpassword').css('border-color', 'red');
        $('#cpassword').focus();
        return false;
    } else {
        $('#cpassword').css('border-color', '');
    }
    if ($('#password').val() != $('#cpassword').val()) {
        $('#cpassword').css('border-color', 'red');
        $('#cpassword').focus();
        $('.register_message').css('color', 'red');
        $('.register_message').html('Both passwords must match');
        return false;
    } else {
        $('#cpassword').css('border-color', '');
        $('.register_message').html('');
    }
    json = json + '"approx_location":"' + $('#approx_location').val() + '"';
    json = json + '}';
    // if (grecaptcha.getResponse() == "") {
    //     $('.register_message').html('Captcha Required');
    //     $('.register_message').css('color', 'red');
    //     return false;
    // } else {
    //     $('.register_message').html('');
    // }
    $('.register_submit').html('<i class="fa fa-spinner fa-spin"></i> Register');
    $.ajax({
        url: base_url + "Register/loginregistration/vendor_register",
        type: 'POST',
        async: false,
        data: "json=" + encodeURIComponent(json),
        success: function (response) {
            try {
                var resp = jQuery.parseJSON(response);

                if (resp.status == 'success') {
                    $('.register_message').css('color', 'green');
                    $('.register_message').html(json.message);
                    setTimeout(function () {
                        window.location = base_url + 'Login/Vendor';
                    }, 2000);
                } else {
                    $('.register_message').css('color', 'red');
                    $('.register_message').html(json.message);
                }
            } catch (e) {
                alert(e.message)
            }
        }
    });
});

$('#UpdateProfile').click(function () {
    // if ($('#first_name').val() == '') {
    //     $('#first_name').css('border-color', 'red');
    //     $('#first_name').focus();
    //     return false;
    // } else {
    //     $('#first_name').css('border-color', '');
    //     var first_name = $('#first_name').val();
    // }
    if ($('#name').val() == '') {
        $('#name').css('border-color', 'red');
        $('#name').focus();
        return false;
    } else {
        $('#name').css('border-color', '');
        var name = $('#name').val();
    }
    if ($('#mobile_number').val() == '') {
        $('#mobile_number').css('border-color', 'red');
        $('#mobile_number').focus();
        return false;
    } else {
        $('#mobile_number').css('border-color', '');
        var mobile_number = $('#mobile_number').val();
    }
    if ($('#email').val() == '') {
        $('#email').css('border-color', 'red');
        $('#email').focus();
        return false;
    } else {
        $('#email').css('border-color', '');
        var email = $('#email').val();
    }

    var Id = $('#UserId').val();
    var password = $('#password').val();
    // var OldImage = $('#ImagesEncode').attr('OldImage');
    // var VendorImage = $('#ImagesEncode').attr('newimage');
    var data = {
        Id: Id,
        // first_name: first_name,
        name: name,
        mobile_number: mobile_number,
        email: email,
        // VendorImage: VendorImage,
        // OldImage: OldImage,
        password: password,
    };

    $.confirm({
        title: want_to_submit,
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: submit,
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "vendor/profile/update",
                        type: 'POST',
                        async: false,
                        data: data,
                        success: function (data) {
                            try {
                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    $('#success-message').html(json.message);
                                    $('.alert-success').show();
                                    $('.alert-success').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                        window.location = base_url + 'vendor/profile';
                                    }, 2000);

                                } else {
                                    $('.alert-danger').show();
                                    $('#error-message').html(json.message)
                                    $('.alert-danger').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                    }, 2000);
                                }
                            } catch (e) {
                                alert(e)
                            }
                        }
                    });
                }
            },
            cancel: {
                text: cancel,
                btnClass: 'btn-danger',
                action: function () {
                }
            },
        }
    });
});
// $( document ).ready(function() {
//     if($('#isDelivery').val()=='1')
//     {
//         $('#is_delivery').show();
//     }else{
//         $('#is_delivery').hide();
//     }
// });
// $("#isDelivery").on('change', function() {
//   if ($(this).is(':checked')) {
//     $(this).attr('value', 1);
//     $('#is_delivery').show();
//   } else {
//     $(this).attr('value', 0);
//     $('#is_delivery').hide();
//     $('#delivery_time_ends').val('');
//     $('#delivery_time_start').val('');
//   }


// });
$("#pre_order").on('change', function () {
    if ($(this).is(':checked')) {
        $(this).attr('value', 1);
    } else {
        $(this).attr('value', 0);
    }


});
$("#party_order").on('change', function () {
    if ($(this).is(':checked')) {
        $(this).attr('value', 1);
    } else {
        $(this).attr('value', 0);
    }


});
$("#busy_status").on('change', function () {
    if ($(this).is(':checked')) {
        $(this).attr('value', 1);
    } else {
        $(this).attr('value', 0);
    }


});

$("#table_booking").on('change', function () {
    if ($(this).is(':checked')) {
        $(this).attr('value', 1);
    } else {
        $(this).attr('value', 0);
    }


});

$('body').on('click', '.commonReject', function () {

    var id = $(this).attr('id');
    var table = $(this).attr('table');
    var url = $(this).attr('redrurl');
    var data = {
        function: 'common_reject',
        id: id,
        table: table
    };
    $.confirm({
        content: "Are you sure you want to reject this reservation?",
        title: 'Rejected!!',
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                action: function () {
                    $.ajax({
                        url: base_url + "vendor/helper",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            var json = jQuery.parseJSON(data);
                            if (json.status == true) {
                                $('#success-message').html(json.message);
                                $('.alert-success').show();
                                $('.alert-success').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                    window.location = base_url + 'admin/' + url;
                                }, 2000);

                            } else {
                                $('.alert-danger').show();
                                $('#error-message').html(json.message)
                                $('.alert-danger').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                }, 2000);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});
$('body').on('click', '.commonApprove', function () {
    var id = $(this).attr('id');
    var table = $(this).attr('table');
    var url = $(this).attr('redrurl');
    var data = {
        function: 'common_approve',
        id: id,
        table: table
    };
    $.confirm({
        content: "Are you sure you want to accept this reservation?",
        title: 'Approved!!',
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                action: function () {
                    $.ajax({
                        url: base_url + "vendor/helper",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            var json = jQuery.parseJSON(data);
                            if (json.status == true) {
                                $('#success-message').html(json.message);
                                $('.alert-success').show();
                                $('.alert-success').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                    window.location = base_url + 'admin/' + url;
                                }, 2000);

                            } else {
                                $('.alert-danger').show();
                                $('#error-message').html(json.message)
                                $('.alert-danger').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                }, 2000);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});
$('#UpdateStore').click(function () {
    var json = {};
    if ($('#restaurant_name').val() == '') {
        $('#restaurant_name').css('border-color', 'red');
        $('#restaurant_name').focus();
        return false;
    } else {
        $('#restaurant_name').css('border-color', '');
        var restaurant_name = $('#restaurant_name').val();
    }
    if ($('#cuisine_id').val() == '') {
        $('#cuisine_id').css('border-color', 'red');
        $('#cuisine_id').focus();
        return false;
    } else {
        $('#cuisine_id').css('border-color', '');
        var cuisine_id = $('#cuisine_id').val();
    }
    if ($('#opening_time').val() == '') {
        $('#opening_time').css('border-color', 'red');
        $('#opening_time').focus();
        return false;
    } else {
        $('#opening_time').css('border-color', '');
        var opening_time = $('#opening_time').val();
    }
    if ($('#closing_time').val() == '') {
        $('#closing_time').css('border-color', 'red');
        $('#closing_time').focus();
        return false;
    } else {
        $('#closing_time').css('border-color', '');
        var closing_time = $('#closing_time').val();
    }
    // if ($('#delivery_time').val() == '') {
    //     $('#delivery_time').css('border-color', 'red');
    //     $('#delivery_time').focus();
    //     return false;
    // } else {
    // $('#delivery_time').css('border-color', '');
    var delivery_time = $('#delivery_time').val();
    // }
    var restaurant_name_ar = $('#restaurant_name_ar').val();
    var Id = $('#UserId').val();
    var service_charge = $('#service_charge').val();
    var isDelivery = $('#isDelivery').is(':checked') ? '1' : '0';
    var self_pickup = $('#self_pickup').is(':checked') ? '1' : '0';
    var delivery_radius = $('#delivery_radius').val();
    // var pre_order = $('#pre_order').val();
    var party_order = $('#party_order').val();
    var busy_status = $('#busy_status').val();
    var min_amount = $('#min_amount').val();
    var about = $('#description').val();
    var about_ar = $('#description_ar').val();
    var delivery_time_start = $('#delivery_time_start').val();
    var delivery_time_ends = $('#delivery_time_ends').val();
    var LogoImages = $(".business_logo").attr('newimage');
    var LicenseImages = $(".trade_license").attr('newimage');

    var LogoOldImage = $('.business_logo').attr('OldImage');
    var LicenseOldImage = $('.trade_license').attr('OldImage');

    var table_booking = $('#table_booking').val();
    var table_capacity = $('#table_capacity').val();
    var table_booking_opening_time = $('#table_booking_opening_time').val();
    var table_booking_closing_time = $('#table_booking_closing_time').val();

    var delivery_from = $('input[id^="delivery_timepicker_from"]').map(function () {
        return $(this).val();
    }).get();
    var delivery_to = $('input[id^="delivery_timepicker_to"]').map(function () {
        return $(this).val();
    }).get();
    // var emirate = $('select[id^="emirate"]').map(function() { return $(this).val(); }).get();

    var emirate = $('select[id^="emirate"]').map(function () {
        return {
            value: $(this).val()
        }
    }).get();
    json['charges_type'] = $("input:radio[name=charges_type]:checked").val();
    json['dlvry_chrge_frst_km'] = $('#dlvry_chrge_frst_km').val();
    json['service_charge'] = $('#dlvry_chrge').val();
    json['dlvry_chrge_perkm'] = $('#dlvry_chrge_perkm').val();
    if (json['charges_type'] == '0') {
        json['service_charge'] = $('#dlvry_chrge_ch').val();
    }
    json['fastdelvry'] = $('#fastdelvry').is(':checked') ? '1' : '0';
    json['prescriptionreq'] = $('#prescriptionrequired').is(':checked') ? '1' : '0';
    var data = {
        Id: Id,
        restaurant_name: restaurant_name,
        restaurant_name_ar: restaurant_name_ar,
        cuisine_id: cuisine_id,
        opening_time: opening_time,
        closing_time: closing_time,
        delivery_time: delivery_time,
        service_charge: service_charge,
        min_amount: min_amount,
        about: about,
        about_ar: about_ar,
        isDelivery: isDelivery,
        delivery_radius: delivery_radius,
        // pre_order:pre_order,
        party_order: party_order,
        busy_status: busy_status,
        delivery_time_start: delivery_time_start,
        delivery_time_ends: delivery_time_ends,
        LogoImages: LogoImages,
        LicenseImages: LicenseImages,
        LogoOldImage: LogoOldImage,
        LicenseOldImage: LicenseOldImage,
        delivery_from: delivery_from,
        delivery_to: delivery_to,
        emirate: emirate,
        table_booking: table_booking,
        table_capacity: table_capacity,
        table_booking_opening_time: table_booking_opening_time,
        table_booking_closing_time: table_booking_closing_time,
        json: json,
        delivery_hours_st: $('#delivery_hours_st').val(),
        delivery_hours_et: $('#delivery_hours_et').val(),
        // form_data:form_data
    };
    data['self_pickup'] = self_pickup;

    $.confirm({
        title: want_to_submit,
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: submit,
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "vendor/storeDetails/update",
                        type: 'POST',
                        async: false,
                        data: data,
                        success: function (data) {
                            try {
                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    $('#success-message').html(json.message);
                                    $('.alert-success').show();
                                    $('.alert-success').slideDown(500);
                                    toastr.success(json.message, 'Success Alert', {timeOut: 5000})
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                        window.location = base_url + 'vendor/storedetails';
                                    }, 2000);

                                } else {
                                    $('.alert-danger').show();
                                    $('#error-message').html(json.message)
                                    $('.alert-danger').slideDown(500);
                                    toastr.error(json.message, 'Error Alert', {timeOut: 5000})
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                    }, 2000);
                                }
                            } catch (e) {
                                alert(e)
                            }
                        }
                    });
                }
            },
            cancel: {
                text: cancel,
                btnClass: 'btn-danger',
                action: function () {
                }
            },
        }
    });
});
$(document).ready(function () {

    $('.mt-repeater-add').click(function () {
        $('.timepicker').timepicker();
        $('.selectpicker').selectpicker('refresh');
    });
});

$('.login_vendor').on('click', function () {

    if ($('#UserName').val() == '') {
        $('#UserName').css('border-color', 'red');
        $('#UserName').focus();
        return false;
    } else {
        $('#UserName').css('border-color', '');
        var email = $('#UserName').val();
    }
    if ($('#Password').val() == '') {
        $('#Password').css('border-color', 'red');
        $('#Password').focus();
        return false;
    } else {
        $('#Password').css('border-color', '');
        var password = $('#Password').val();
    }
    var data = {UserName: email, Password: password};

    // $('.login_vendor').html('<i class="fa fa-spinner fa-spin"></i> Submit');
    $.ajax({
        url: base_url + "Login/CheckVendorLogin",
        type: 'POST',
        async: false,
        data: data,
        success: function (resp) {
            // $('.login_vendor').html('Submit');
            try {
                var json = jQuery.parseJSON(resp);
                if (json.status == 'success') {
                    $('.login_message').css('color', 'green');
                    $('.login_message').html(json.msg);
                    setTimeout(function () {
                        window.location = json.RedirectURL;
                        // if (segment2 == 'callback') {
                        //     window.location = CallBack;
                        // }
                    }, 2000);
                } else {
                    $('.login_message').css('color', 'red');
                    $('.login_message').html(json.msg);
                }
            } catch (e) {
                alert(e.message)
            }
        }
    });

});
$(document).on('change', '.MultipleImages', function () {
    var refval = $(this).attr('refval');
    var fileTypes = ['jpg', 'jpeg', 'png', 'gif', 'jfif'];
    if (this.files && this.files[0]) {
        var extension = this.files[0].name.split('.').pop().toLowerCase(),
                isSuccess = fileTypes.indexOf(extension) > -1;
        var fileName = this.files[0].name;
        if (isSuccess) {
            var FR = new FileReader();
            FR.addEventListener("load", function (e) {
                document.getElementById('ImagesEncode_' + refval).src = e.target.result;
                $('#ImagesEncode_' + refval).attr('newimage', e.target.result);
                $('#ImagesEncode_' + refval).show();
            });
            FR.readAsDataURL(this.files[0]);
        } else {
            alert('Invalid File')
        }
    }
});
$('#CommonImages').change(function () {
    var fileTypes = ['jpg', 'jpeg', 'png'];
    if (this.files && this.files[0]) {
        var extension = this.files[0].name.split('.').pop().toLowerCase(),
                isSuccess = fileTypes.indexOf(extension) > -1;
        var fileName = this.files[0].name;
        if (isSuccess) {
            var FR = new FileReader();
            FR.addEventListener("load", function (e) {
                document.getElementById('ImagesEncode').src = e.target.result;
                $('#ImagesEncode').attr('newimage', e.target.result);
                $('#ImagesEncode').show();
            });
            FR.readAsDataURL(this.files[0]);
        } else {
            alert('Invalid File')
        }
    }
});
$('#add_category').hide();
function addCategory() {
    var id = $('#category').val();
    if (id == '')
    {
        $('#add_category').show();
        $('#other_category').html("<input type='text' placeholder ='New category' name ='category_name' class='form-control' id='category_name' autocomplete='off'>");
    }
    else {
        $('#add_category').hide();
        $('#other_category').html("");
    }
}
$('#SubmitMenu').click(function () {
    var json = '';
    json = json + '{';
    var category = $('#category');
    var category_name = $('#category_name');
    if (category_name.val() == '' || category_name.val() == undefined)
    {
        if (category.val() == '') {
            category.focus();
            category.css('border-color', 'red');
            return false;
        } else {
            category.css('border-color', '');
            json = json + '"category_id":"' + category.val() + '",';
        }
    } else {
        if (category_name.val() == '') {
            category_name.focus();
            category_name.css('border-color', 'red');
            return false;
        } else {
            category_name.css('border-color', '');
            json = json + '"category_name":"' + category_name.val() + '",';
        }
    }

    var menu_name = $('#menu_name');
    var menu_name_ar = $('#menu_name_ar');
    if (menu_name.val() == '') {
        menu_name.focus();
        menu_name.css('border-color', 'red');
        return false;
    } else {
        menu_name.css('border-color', '');
        json = json + '"menu_name":"' + menu_name.val() + '",';
    }
    json = json + '"menu_name_ar":"' + menu_name_ar.val() + '",';
    var store_type = $('#store_type');
    json = json + '"store_type":"' + store_type.val() + '",';
    // var product_size = $('input[id^="size"]').map(function() {
    //   return this.value;
    // }).get();
    var product_size = $('select[id^="size"]').map(function () {
        return $(this).val();
    }).get();

    // var product_price = $('select[id^="price"]').map(function() {
    //   return this.value;
    // }).get();
    var product_price = $('input[id^="price"]').map(function () {
        return this.value;
    }).get();
    var size_id = $('input[id^="size_id"]').map(function () {
        return this.value;
    }).get();
    var add_on = $('input[id^="add_on"]').map(function () {
        return this.value;
    }).get();

    var add_on_ar = $('input[id^="addonar"]').map(function () {
        return this.value;
    }).get();

    var addon_price = $('input[id^="addons_price"]').map(function () {
        return this.value;
    }).get();
    var addon_id = $('input[id^="addon_id"]').map(function () {
        return this.value;
    }).get();

    var topping_name = $('input[id^="topping_name"]').map(function () {
        return this.value;
    }).get();

    var toppingname_ar = $('input[id^="toppingname_ar"]').map(function () {
        return this.value;
    }).get();

    var topping_price = $('input[id^="topping_price"]').map(function () {
        return this.value;
    }).get();
    var topping_id = $('input[id^="topping_id"]').map(function () {
        return this.value;
    }).get();
    // var drink_name = $('input[id^="drink_name"]').map(function() {
    //   return this.value;
    // }).get();
    // var drinkname_ar = $('input[id^="drinkname_ar"]').map(function() {
    //   return this.value;
    // }).get();

    // var drink_price = $('input[id^="drink_price"]').map(function() {
    //   return this.value;
    // }).get();

    var dip = $('input[id^="dips"]').map(function () {
        return this.value;
    }).get();
    var dip_ar = $('input[id^="dip_ar"]').map(function () {
        return this.value;
    }).get();

    var dip_price = $('input[id^="dip_price"]').map(function () {
        return this.value;
    }).get();
    var dip_id = $('input[id^="dip_id"]').map(function () {
        return this.value;
    }).get();

    var side_dish = $('input[id^="side_dish_name"]').map(function () {
        return this.value;
    }).get();

    var sidedish_ar = $('input[id^="sidedish_name_ar"]').map(function () {
        return this.value;
    }).get();


    var side_dish_price = $('input[id^="side_dish_price"]').map(function () {
        return this.value;
    }).get();

    var side_dish_id = $('input[id^="side_dish_id"]').map(function () {
        return this.value;
    }).get();

    if ($('input[name="choice"]:checked').val() == 1)
    {
        var choice = 1;
    } else {
        var choice = 0;
    }

    if ($('#stock').val() != undefined && $('#stock').val() != '')
    {
        var stock = $('#stock').val();
    } else {
        var stock = 0;
    }
    var OldImage = $('#ImagesEncode').attr('OldImage');
    var Images = $('#ImagesEncode').attr('newimage');
    var menu_price = $('#product_price').val();
    var choice = choice;
    // json = json + '"deal_id":"' + $('#offers').val() + '",';
    // json = json + '"description":"' + $('#description').val() + '",';
    // json = json + '"description_ar":"' + $('#description_ar').val() + '",';
    json = json + '"stock":"' + stock + '",';
    json = json + '"status":"' + $('#Status').val() + '",';
    json = json + '"max_purchase_qty":"' + $('#max_purchase_qty').val() + '",';
    json = json + '"Id":"' + $('#Id').val() + '"';
    json = json + '}';

    if ($('#product_price').val() == '') {
        $('#product_price').focus();
        return false;
    }

    var description = $('#description').val();
    var description_ar = $('#description_ar').val();

    var data = {
        json: json,
        Images: Images,
        OldImage: OldImage,
        description: description,
        description_ar: description_ar,
        size: product_size,
        price: product_price,
        product_price: menu_price,
        size_id: size_id,
        addon: add_on,
        add_on_ar: add_on_ar,
        addon_price: addon_price,
        addon_id: addon_id,
        choice: choice,
        topping: topping_name,
        topping_ar: toppingname_ar,
        topping_price: topping_price,
        topping_id: topping_id,
        // drink_name:drink_name,
        // drink_name_ar:drinkname_ar,
        // drink_price:drink_price,
        dip: dip,
        dip_ar: dip_ar,
        dip_price: dip_price,
        dip_id: dip_id,
        side_dish: side_dish,
        side_dish_ar: sidedish_ar,
        side_dish_price: side_dish_price,
        side_dish_id: side_dish_id,
        prescriptionreq: $('#prescriptionrequired').is(':checked') ? '1' : '0',
    };

    // console.log(data);exit;
    $.confirm({
        title: want_to_submit,
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: submit,
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "vendor/menu/add-new",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            try {

                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    $('#success-message').html(json.message);
                                    $('.alert-success').show();
                                    $('.alert-success').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                        window.location = base_url + 'vendor/menu';
                                    }, 2000);

                                } else {
                                    $('.alert-danger').show();
                                    $('#error-message').html(json.message)
                                    $('.alert-danger').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                    }, 2000);
                                }
                            } catch (e) {
                                alert(e);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: cancel,
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});
// $('#SubmitMenu').click(function () {
//     var json = '';
//     json = json + '{';
//     var category = $('#category');
//     var category_name = $('#category_name');
//     if (category_name.val() == '' || category_name.val() == undefined)
//     {
//         if (category.val() == '') {
//             category.focus();
//             category.css('border-color', 'red');
//             return false;
//         } else {
//             category.css('border-color', '');
//             json = json + '"category_id":"' + category.val() + '",';
//         }
//     } else {
//         if (category_name.val() == '') {
//             category_name.focus();
//             category_name.css('border-color', 'red');
//             return false;
//         } else {
//             category_name.css('border-color', '');
//             json = json + '"category_name":"' + category_name.val() + '",';
//         }
//     }

//     var menu_name = $('#menu_name');
//     var menu_name_ar = $('#menu_name_ar');
//     if (menu_name.val() == '') {
//         menu_name.focus();
//         menu_name.css('border-color', 'red');
//         return false;
//     } else {
//         menu_name.css('border-color', '');
//         json = json + '"menu_name":"' + menu_name.val() + '",';
//     }
//     json = json + '"menu_name_ar":"' + menu_name_ar.val() + '",';
//     var store_type = $('#store_type');
//     json = json + '"store_type":"' + store_type.val() + '",';
//     // var product_size = $('input[id^="size"]').map(function() {
//     //   return this.value;
//     // }).get();
//     var product_size = $('select[id^="size"]').map(function () {
//         return $(this).val();
//     }).get();

//     // var product_price = $('select[id^="price"]').map(function() {
//     //   return this.value;
//     // }).get();
//     var product_price = $('input[id^="price"]').map(function () {
//         return this.value;
//     }).get();
//     var size_id = $('input[id^="size_id"]').map(function () {
//         return this.value;
//     }).get();
//     var add_on = $('input[id^="add_on"]').map(function () {
//         return this.value;
//     }).get();

//     var add_on_ar = $('input[id^="addonar"]').map(function () {
//         return this.value;
//     }).get();

//     var addon_price = $('input[id^="addons_price"]').map(function () {
//         return this.value;
//     }).get();
//     var addon_id = $('input[id^="addon_id"]').map(function () {
//         return this.value;
//     }).get();

//     var topping_name = $('input[id^="topping_name"]').map(function () {
//         return this.value;
//     }).get();

//     var toppingname_ar = $('input[id^="toppingname_ar"]').map(function () {
//         return this.value;
//     }).get();

//     var topping_price = $('input[id^="topping_price"]').map(function () {
//         return this.value;
//     }).get();
//     var topping_id = $('input[id^="topping_id"]').map(function () {
//         return this.value;
//     }).get();
//     // var drink_name = $('input[id^="drink_name"]').map(function() {
//     //   return this.value;
//     // }).get();
//     // var drinkname_ar = $('input[id^="drinkname_ar"]').map(function() {
//     //   return this.value;
//     // }).get();

//     // var drink_price = $('input[id^="drink_price"]').map(function() {
//     //   return this.value;
//     // }).get();

//     var dip = $('input[id^="dips"]').map(function () {
//         return this.value;
//     }).get();
//     var dip_ar = $('input[id^="dip_ar"]').map(function () {
//         return this.value;
//     }).get();

//     var dip_price = $('input[id^="dip_price"]').map(function () {
//         return this.value;
//     }).get();
//     var dip_id = $('input[id^="dip_id"]').map(function () {
//         return this.value;
//     }).get();

//     var side_dish = $('input[id^="side_dish_name"]').map(function () {
//         return this.value;
//     }).get();

//     var sidedish_ar = $('input[id^="sidedish_name_ar"]').map(function () {
//         return this.value;
//     }).get();


//     var side_dish_price = $('input[id^="side_dish_price"]').map(function () {
//         return this.value;
//     }).get();

//     var side_dish_id = $('input[id^="side_dish_id"]').map(function () {
//         return this.value;
//     }).get();

//     if ($('input[name="choice"]:checked').val() == 1)
//     {
//         var choice = 1;
//     } else {
//         var choice = 0;
//     }

//     if ($('#stock').val() != undefined && $('#stock').val() != '')
//     {
//         var stock = $('#stock').val();
//     } else {
//         var stock = 0;
//     }

//     var NewImages = $(".more_images_cls").map(function () {
//         return $(this).attr('newimage')
//     }).get();

//     var Old_Image = $(".more_images_cls").map(function () {
//         return $(this).attr('OldImage')
//     }).get();
//     var OldImage = $('#ImagesEncode').attr('OldImage');
//     var Images = $('#ImagesEncode').attr('newimage');
//     var menu_price = $('#product_price').val();
//     var choice = choice;
//     // json = json + '"deal_id":"' + $('#offers').val() + '",';
//     json = json + '"description":"' + $('#description').val() + '",';
//     json = json + '"description_ar":"' + $('#description_ar').val() + '",';
//     json = json + '"stock":"' + stock + '",';
//     json = json + '"status":"' + $('#Status').val() + '",';
//     json = json + '"Id":"' + $('#Id').val() + '"';
//     json = json + '}';

//     var data = {
//         json: json,
//         Images: Images,
//         OldImage: OldImage,
//         size: product_size,
//         price: product_price,
//         product_price: menu_price,
//         size_id: size_id,
//         addon: add_on,
//         add_on_ar: add_on_ar,
//         addon_price: addon_price,
//         addon_id: addon_id,
//         choice: choice,
//         topping: topping_name,
//         topping_ar: toppingname_ar,
//         topping_price: topping_price,
//         topping_id: topping_id,
//         // drink_name:drink_name,
//         // drink_name_ar:drinkname_ar,
//         // drink_price:drink_price,
//         dip: dip,
//         dip_ar: dip_ar,
//         dip_price: dip_price,
//         dip_id: dip_id,
//         side_dish: side_dish,
//         side_dish_ar: sidedish_ar,
//         side_dish_price: side_dish_price,
//         side_dish_id: side_dish_id,
//         NewImages: NewImages,
//         Old_Image: Old_Image


//     };
//     // console.log(data);exit;
//     $.confirm({
//         title: want_to_submit,
//         content: false,
//         type: 'green',
//         typeAnimated: true,
//         buttons: {
//             confirm: {
//                 text: submit,
//                 btnClass: 'btn-success',
//                 action: function () {
//                     $.ajax({
//                         url: base_url + "vendor/menu/add-new",
//                         type: 'POST',
//                         data: data,
//                         async: false,
//                         success: function (data) {
//                             try {

//                                 var json = jQuery.parseJSON(data);
//                                 if (json.status == 'success') {
//                                     $('#success-message').html(json.message);
//                                     $('.alert-success').show();
//                                     $('.alert-success').slideDown(500);
//                                     setTimeout(function () {
//                                         $('.alert-danger').slideUp(500);
//                                         window.location = base_url + 'vendor/menu';
//                                     }, 2000);

//                                 } else {
//                                     $('.alert-danger').show();
//                                     $('#error-message').html(json.message)
//                                     $('.alert-danger').slideDown(500);
//                                     setTimeout(function () {
//                                         $('.alert-danger').slideUp(500);
//                                     }, 2000);
//                                 }
//                             } catch (e) {
//                                 alert(e);
//                             }
//                         }
//                     });
//                 }
//             },
//             cancel: {
//                 text: cancel,
//                 btnClass: 'btn-info',
//                 action: function () {
//                 }
//             },
//         }
//     });
// });
$('#SubmitPartyMenu').click(function () {
    var json = '';
    json = json + '{';
    var category = $('#category');
    var category_name = $('#category_name');
    if (category_name.val() == '' || category_name.val() == undefined)
    {
        if (category.val() == '') {
            category.focus();
            category.css('border-color', 'red');
            return false;
        } else {
            category.css('border-color', '');
            json = json + '"category_id":"' + category.val() + '",';
        }
    } else {
        if (category_name.val() == '') {
            category_name.focus();
            category_name.css('border-color', 'red');
            return false;
        } else {
            category_name.css('border-color', '');
            json = json + '"category_name":"' + category_name.val() + '",';
        }
    }

    var menu_name = $('#menu_name');
    var menu_name_ar = $('#menu_name_ar');
    if (menu_name.val() == '') {
        menu_name.focus();
        menu_name.css('border-color', 'red');
        return false;
    } else {
        menu_name.css('border-color', '');
        json = json + '"menu_name":"' + menu_name.val() + '",';
    }
    json = json + '"menu_name_ar":"' + menu_name_ar.val() + '",';
    var store_type = $('#store_type');
    json = json + '"store_type":"' + store_type.val() + '",';

    var OldImage = $('#ImagesEncode').attr('OldImage');
    var Images = $('#ImagesEncode').attr('newimage');
    var menu_price = $('#product_price').val();

    json = json + '"description":"' + $('#description').val() + '",';
    json = json + '"description_ar":"' + $('#description_ar').val() + '",';
    json = json + '"status":"' + $('#Status').val() + '",';
    json = json + '"Id":"' + $('#Id').val() + '"';
    json = json + '}';

    var data = {
        json: json,
        Images: Images,
        OldImage: OldImage,
        product_price: menu_price,
    };
    $.confirm({
        title: want_to_submit,
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: submit,
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "vendor/party_menu/add-new",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            try {

                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    $('#success-message').html(json.message);
                                    $('.alert-success').show();
                                    $('.alert-success').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                        window.location = base_url + 'vendor/party_menu';
                                    }, 2000);

                                } else {
                                    $('.alert-danger').show();
                                    $('#error-message').html(json.message)
                                    $('.alert-danger').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                    }, 2000);
                                }
                            } catch (e) {
                                alert(e);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: cancel,
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

$('body').on('click', '.common_delete', function () {

    var id = $(this).attr('id');
    var table = $(this).attr('table');
    var url = $(this).attr('redrurl');
    var data = {
        status: 'common_delete',
        Id: id,
        table: table
    };

    $.confirm({
        content: sure_want_to_delete,
        title: delete_data,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: submit,
                action: function () {
                    $.ajax({
                        url: base_url + "vendor/helper",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            var json = jQuery.parseJSON(data);
                            if (json.status == true) {
                                $('#success-message').html(json.message);
                                $('.alert-success').show();
                                $('.alert-success').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                    window.location = base_url + 'vendor/' + url;
                                }, 2000);

                            } else {
                                $('.alert-danger').show();
                                $('#error-message').html(json.message)
                                $('.alert-danger').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                }, 2000);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: cancel,
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});


$('.remove_image_common').click(function () {
    var id = $(this).attr('id');
    var path = $(this).attr('path');
    var name = $(this).attr('name');
    var table = $(this).attr('table');
    var type = $(this).attr('type');
    if (table == "promo_code")
    {
        var status = 'offer_remove_image';
    } else {
        var status = 'common_remove_image';
    }

    var data = {
        status: status
        , id: id
        , path: path
        , name: name
        , table: table
        , type: type
    };
    $.confirm({
        title: remove_image,
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: submit,
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "vendor/Helper"
                        , async: true
                        , type: 'POST'
                        , data: data
                        , success: function (data) {
                            var json = jQuery.parseJSON(data);
                            if (json.status == 'success') {
                                $('#success-message').html('Removed successfully');
                                $('.alert-success').show();
                                $('.alert-success').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                    window.location = '';
                                }, 2000);
                            } else {
                                $('.alert-danger').show();
                                $('#success-error').html('Try Again')
                                $('.alert-danger').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                }, 2000);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: cancel,
                btnClass: 'btn-danger',
                action: function () {
                }
            },
        }
    });
});

$('.add_choices').hide();

$('input[name="choice"]').click(function () {
    if ($(this).prop("checked") == true) {
        $('.add_choices').show();
    } else if ($(this).prop("checked") == false) {
        $('.add_choices').hide();
    }
});

if ($('input[name="choice"]:checkbox').is(':checked')) {
    $('.add_choices').show();
} else {
    $('.add_choices').hide();
}

$('input[name="table_booking"]').click(function () {
    if ($(this).prop("checked") == true) {
        $('.table_booking_div').show();
    } else if ($(this).prop("checked") == false) {
        $('.table_booking_div').hide();
    }
});

if ($('input[name="table_booking"]:checkbox').is(':checked')) {
    $('.table_booking_div').show();
} else {
    $('.table_booking_div').hide();
}
$('#change_partyorder_status').change(function () {
    var order_status = $('#change_partyorder_status').val();
    var order_id = $('#party_order_id').val();
    var user_id = $('#user_id').val();
    var vendor_id = $('#vendor_id').val();


    var data = {status: 'changePartyOrderStatus', order_status: order_status, order_id: order_id, user_id: user_id, vendor_id: vendor_id};

    $.ajax({
        url: base_url + "vendor/helper",
        type: 'POST',
        data: data,
        async: false,
        success: function (data) {

            setTimeout(function () {
                $('.alert-danger').slideUp(500);
                window.location = '';
            }, 1000);
        }
    });
});
$('#change_order_status').click(function () {
    var order_status = $('#order_status').val();
    var order_id = $('#order_id').val();
    var user_id = $('#user_id').val();
    var vendor_id = $('#vendor_id').val();

    if ($('#payment_status').val() != undefined) {
        var payment_status = $('#payment_status').val();
    } else {
        var payment_status = ''
    }

    var data = {status: 'changeOrderStatus', order_status: order_status, order_id: order_id, payment_status: payment_status, user_id: user_id, vendor_id: vendor_id};
    $.ajax({
        url: base_url + "vendor/helper",
        type: 'POST',
        data: data,
        async: false,
        success: function (data) {

            setTimeout(function () {
                $('.alert-danger').slideUp(500);
                window.location = '';
            }, 1000);
        }
    });
});
$('#change_status').change(function () {
    var order_status = $('#change_status').val();
    var order_id = $('#order_id').val();
    var user_id = $('#user_id').val();
    var vendor_id = $('#vendor_id').val();

    var msg = $('option:selected', this).attr('msg');
    var title = $('option:selected', this).attr('title');

    if ($('#payment_status').val() != undefined) {
        var payment_status = $('#payment_status').val();
    } else {
        var payment_status = ''
    }

    var data = {status: 'changeOrderStatus', order_status: order_status, order_id: order_id, payment_status: payment_status, user_id: user_id, vendor_id: vendor_id};


    $.confirm({
        title: title,
        content: msg,
        type: order_status == '7' ? 'red' : 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Yes',
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "vendor/helper",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {

                            setTimeout(function () {
                                $('.alert-danger').slideUp(500);
                                window.location = '';
                            }, 1000);
                        }
                    });
                }
            },
            cancel: {
                text: 'cancel',
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });

});
$('#party_order_status').click(function () {
    var payment_status = $('#payment_status').val();
    var partyorder_id = $('#party_order_id').val();



    var data = {status: 'changePartyOrderStatus', partyorder_id: partyorder_id, payment_status: payment_status};

    $.ajax({
        url: base_url + "vendor/helper",
        type: 'POST',
        data: data,
        async: false,
        success: function (data) {

            setTimeout(function () {
                $('.alert-danger').slideUp(500);
                window.location = '';
            }, 1000);
        }
    });
});

$('#addoffers').click(function () {
    if ($('#offer_title').val() == '') {
        $('#offer_title').css('border-color', 'red');
        $('#offer_title').focus();
        return false;
    } else {
        $('#offer_title').css('border-color', '');
        var offer_title = $('#offer_title').val();
    }
    if ($('#discount').val() == '') {
        $('#discount').css('border-color', 'red');
        $('#discount').focus();
        return false;
    } else {
        $('#discount').css('border-color', '');
        var discount = $('#discount').val();
    }
    if ($('#date_from').val() == '') {
        $('#date_from').css('border-color', 'red');
        $('#date_from').focus();
        return false;
    } else {
        $('#date_from').css('border-color', '');
        var date_from = $('#date_from').val();
    }
    if ($('#date_to').val() == '') {
        $('#date_to').css('border-color', 'red');
        $('#date_to').focus();
        return false;
    } else {
        $('#date_to').css('border-color', '');
        var date_to = $('#date_to').val();
    }
    if ($('#menu_id').val() == '') {
        $('#menu_id').css('border-color', 'red');
        $('#menu_id').focus();
        return false;
    } else {
        $('#menu_id').css('border-color', '');
        var menu_id = $('#menu_id').val() != null ? $('#menu_id').val().join(',') : '';
    }
    //  var OfferImages = $(".offer_images_cls").map(function () {
    //     return $(this).attr('newimage')
    // }).get();
    //  var OfferImages = $(".offer_images_cls").attr('newimage');
    //     return $(this).attr('newimage')
    // }).get();
    // var OldImage = $(".offer_images_cls").map(function () {
    //     return $(this).attr('OldImage')
    // }).get();
    //  var OldImage = $(".offer_images_cls").attr('OldImage')
    //     return $(this).attr('OldImage')
    // }).get();
    var offer_title_ar = $('#offer_title_ar').val();
    var status = $('#status').val();
    var Id = $('#Id').val();
    var data = {
        Id: Id,
        title: offer_title,
        title_ar: offer_title_ar,
        discount: discount,
        date_from: date_from,
        date_to: date_to,
        menu_id: menu_id,
        status: status,
        discount_type: $('#discount_type').val(),
        // OfferImages: OfferImages,
        // OldImage: OldImage,
    };

    $.confirm({
        title: want_to_submit,
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: submit,
                btnClass: 'btn-success',
                action: function () {

                    $.ajax({
                        data: data,
                        type: 'POST',
                        async: false,
                        url: base_url + "vendor/offers/add-new",
                        success: function (data) {
                            //   console.log(data);exit;
                            var json = jQuery.parseJSON(data);
                            if (json.status == true) {
                                window.location = base_url + 'vendor/offers';
                            }
                        }
                    });
                }
            },
            cancel: {
                text: cancel,
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

$('body').on('click', '.autodelete', function () {
    var condjson = $(this).attr('condjson');
    var dbtable = $(this).attr('dbtable');
    var removefile = $(this).attr('removefile');
    var data = {status: 'autodelete', condjson: condjson, dbtable: dbtable, removefile: removefile};
    var cmessage = $(this).attr('cmessage') != undefined ? $(this).attr('cmessage') : sure_want_to_delete;
    $.confirm({
        title: cmessage,
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            tryAgain: {
                text: submit,
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "Vendor/Helper",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            try {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    window.location = '';
                                }
                            } catch (e) {

                            }
                        }
                    });
                }
            },
            close: {
                text: cancel,
                btnClass: 'btn-red',
            }
        }
    });
});
$('#quoteprice').click(function () {
    var price = $('#price').val();
    var party_id = $('#party_order_id').val();
    var data = {status: 'updateQuotedPrice', price: price, party_id: party_id};

    $.ajax({
        url: base_url + "vendor/helper",
        type: 'POST',
        data: data,
        async: false,
        success: function (data) {
            setTimeout(function () {
                $('.alert-danger').slideUp(500);
                window.location = base_url + 'vendor/partyorders';
            }, 2000);
        }
    });
});
$('#SubmitMostSelling').click(function () {
    var checked = []
    $("input[name='menu_list[]']:checked").each(function ()
    {
        checked.push(parseInt($(this).val()));
    });
    var menu_list = checked != null ? checked.join(',') : '';
    // var menu_list = checked != null ? checked : '';
    var category_id = $('#most_selling').val();
    var data = {status: 'mostSelling', menu_list: menu_list, category_id: category_id};
    $.ajax({
        url: base_url + "vendor/helper",
        type: 'POST',
        data: data,
        async: false,
        success: function (data) {

            setTimeout(function () {
                window.location = '';
            }, 2000);
        }
    });
});
$('#add_more_images').click(function () {
    var classname = $(this).attr('inputclassname');
    var rand = Math.floor((Math.random() * 1000) + 1);
    $('#image_upload_section').append('<div class="form-group div_img_' + rand + '"><label class="control-label col-md-3">' + upload_image + '</label><div class="col-md-6"><input type="file" class="custom-file-input MultipleImages" id="" refval="' + rand + '" src=""></div><div class="col-md-3"><a target="_blank" href=""><img src="" width="20%" id="ImagesEncode_' + rand + '" OldImage="" class="' + classname + '" newimage=""></a><a class="remove_image_upload" id="' + rand + '" ><span class="glyphicon glyphicon-trash"></span></a></div></div>');
});
$(document).on('click', '.remove_image_upload', function () {
    var id = $(this).attr('id');
    $('.div_img_' + id).remove();
});
$('#SubmitBanner').click(function () {
    var Images = $(".banner_images_cls").map(function () {
        return $(this).attr('newimage')
    }).get();
    var OldImage = $(".banner_images_cls").map(function () {
        return $(this).attr('OldImage')
    }).get();
    var data = {
        Images: Images,
        OldImage: OldImage,
    };

    $.confirm({
        title: want_to_submit,
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: submit,
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "Vendor/banner_management",
                        type: 'POST',
                        async: false,
                        data: data,
                        success: function (data) {

                            try {
                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    $('#success-message').html(json.message);
                                    $('.alert-success').show();
                                    $('.alert-success').slideDown(500);
                                    setTimeout(function () {
                                        window.location = '';
                                    }, 1000);

                                } else {
                                    $('.alert-danger').show();
                                    $('#error-message').html(json.message)
                                    $('.alert-danger').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                    }, 2000);
                                }
                            } catch (e) {
                                alert(e)
                            }
                        }
                    });
                }
            },
            cancel: {
                text: cancel,
                btnClass: 'btn-danger',
                action: function () {
                }
            },
        }
    });
});


$('.update_position').click(function () {
    var position = $(this).closest('tr').find('.position').val();
    var tableid = $(this).attr('tableid');
    var table = $(this).attr('table');
    var data = {
        function: 'update_position'
        , id: tableid
        , table: table
        , position: position
    };
    $.confirm({
        title: want_to_update,
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: submit,
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/Helper"
                        , async: true
                        , type: 'POST'
                        , data: data
                        , success: function (data) {
                            var json = jQuery.parseJSON(data);
                            if (json.status == 'success') {
                                $('#success-message').html(json.message);
                                $('.alert-success').show();
                                $('.alert-success').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                    window.location = '';
                                }, 2000);
                            }
                            else {
                                $('.alert-danger').show();
                                $('#error-message').html(json.message)
                                $('.alert-danger').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                    window.location = '';
                                }, 2000);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: cancel,
                btnClass: 'btn-danger',
                action: function () {
                }
            },
        }
    });
});
$('.reset_position').click(function () {
    var tableid = $(this).attr('tableid');
    var table = $(this).attr('table');
    var data = {
        function: 'reset_position'
        , id: tableid
        , table: table
    };
    $.confirm({
        title: reset_position,
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: submit,
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/Helper"
                        , async: true
                        , type: 'POST'
                        , data: data
                        , success: function (data) {
                            var json = jQuery.parseJSON(data);
                            if (json.status == 'success') {
                                $('#success-message').html(json.message);
                                $('.alert-success').show();
                                $('.alert-success').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                    window.location = '';
                                }, 2000);
                            }
                            else {
                                $('.alert-danger').show();
                                $('#error-message').html(json.message)
                                $('.alert-danger').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                    window.location = '';
                                }, 2000);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: cancel,
                btnClass: 'btn-danger',
                action: function () {
                }
            },
        }
    });
});


$('#send_request_to_thirdparty').click(function () {
    var order_id = $('#order_id').val();
    var user_id = $('#user_id').val();
    var vendor_id = $('#vendor_id').val();
    var data = {order_id: order_id, user_id: user_id, vendor_id: vendor_id};
    $.ajax({
        url: base_url + "vendor/sendReqstToAirdtod",
        type: 'POST',
        data: data,
        async: false,
        success: function (data) {
            console.log(data);
            exit;
            setTimeout(function () {
                $('.alert-danger').slideUp(500);
                window.location = '';
            }, 1000);
        }
    });
});

$('#deliveryboy_id').on('change', function () {
    var orderId = $('#orderId').val();
    var deliveryboy_id = $('#deliveryboy_id').val();
    var user_id = $('#user_id').val();
    var vendor_id = $('#vendor_id').val();
    var data = {status: 'assign_deliveryboy', orderId: orderId, deliveryboy_id: deliveryboy_id, user_id: user_id, vendor_id: vendor_id};
    $.confirm({
        title: want_to_submit,
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: submit,
                btnClass: 'btn-green',
                action: function () {
                    $.ajax({
                        url: base_url + "vendor/Helper",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            if (jQuery.parseJSON(data)) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    window.location = '';
                                } else {
                                    alert(json.message)
                                }
                            }
                        }
                    });
                }
            },
            cancel: {
                text: cancel,
                btnClass: 'btn-warning',
                action: function () {
                }
            },
        }
    });
});
function alertSimple() {
    $.dialog({
        title: false,
        content: 'Simple modal!',
    });
}
function ajaxpost(form, url) {
    var res = '';
    var xhr = new XMLHttpRequest();
    xhr.open('POST', base_url + url, false);
    xhr.onload = function () {
        res = xhr.responseText;
    };
    xhr.send(form);
    return res;
}


