if (location.protocol !== "https:") {
    location.protocol = "https:";
}
$(document).ready(function () {
    window.setInterval(function () {
        VENDRCOMN.autochatNotifaction();
    }, 5000);
    $('body').on('click', '.autoupdate', function () {
        var updatejson = $(this).attr('updatejson');
        var condjson = $(this).attr('condjson');
        var dbtable = $(this).attr('dbtable');
        var data = {status: 'autoupdate', updatejson: updatejson, condjson: condjson, dbtable: dbtable};
        var cmessage = $(this).attr('cmessage') != undefined ? $(this).attr('cmessage') : 'Are you sure want to delete!.';
        $.confirm({
            title: cmessage,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "vendor/Helper",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == true) {
                                        window.location = '';
                                    }
                                } catch (e) {

                                }
                            }
                        });
                    }
                },
                close: {
                    text: 'Cancel',
                    btnClass: 'btn-red',
                }
            }
        });
    });
    
     $('body').on('click', '.menudelete', function () {
        var updatejson = $(this).attr('updatejson');
        var condjson = $(this).attr('condjson');
        var dbtable = $(this).attr('dbtable');
        var data = {status: 'menudelete', updatejson: updatejson, condjson: condjson, dbtable: dbtable};
        var cmessage = $(this).attr('cmessage') != undefined ? $(this).attr('cmessage') : 'Are you sure want to delete!.';
     data['menuId']=$(this).attr('menuId');
         $.confirm({
            title: cmessage,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "vendor/Helper",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == true) {
                                        window.location = '';
                                    }
                                } catch (e) {

                                }
                            }
                        });
                    }
                },
                close: {
                    text: 'Cancel',
                    btnClass: 'btn-red',
                }
            }
        });
    });
});
if (Method == 'menucategory') {
    $(function () {
        $("#sortable").sortable({
            stop: function (e, ui) {
                console.log($.map($(this).find('li'), function (el) {
                    return el.id + ' = ' + $(el).index();
                }));
            }
        });

    });
}
var VENDRCOMN = (function () {
    var fn = {};
    fn.assignRiderbyVendor = function () {
        var riderId = $('#assign_rider').val();
        var orderId = $('#order_id').val();
        var data = {status: 'assignRiderbyVendor', riderId: riderId, orderId: orderId};
        $.ajax({
            method: "POST",
            url: base_url + "Frontend/Helper",
            data: data,
            async: false,
            success: function (res) {
                var json = JSON.parse(res);
                alertSimple(json.message);
                if (json.status == true) {

                } else {

                }
            },
            error: function (xhr) {
                alert("Error occured.please try again");
            }
        });
    }
    fn.updatedeleverycharges = function () {
        var charges_type = $("input:radio[name=charges_type]:checked").val();
        $('#Flexiblecharges,#Fixedcharges').hide();
        if (charges_type == '1') {
            $('#Flexiblecharges').show();
        } else if (charges_type == '0') {
            $('#Fixedcharges').show();
        }
    }
    fn.menucategoryorderviewupdate = function (e) {
        var vendorId = $('#vendor_id').val();
        var orderview = [];
        $.map($('#sortable').find('li'), function (el) {
            var d = {};
            d['cartId'] = el.id;
            d['orderview'] = $(el).index();
            orderview.push(d);
            // console.log(el.id);
            // return el.id + ' = ' + $(el).index();
        })

        var data = {status: 'menucategoryorderviewupdate', vendorId: vendorId, orderview: orderview};
        data['positions'] = $(e).val();
        data['aftercate'] = $('option:selected', e).attr('aftercate');
        data['currentpos'] = $(e).attr('currentpos');
        $.ajax({
            method: "POST",
            url: base_url + "Frontend/Helper",
            data: data,
            async: false,
            success: function (res) {
                var json = JSON.parse(res);
                if (json.status == true) {
                    window.location = '';
                } else {

                }
            },
            error: function (xhr) {
                alert("Error occured.please try again");
            }
        });
    }
    fn.menulistorderviewupdate = function (e) {
        var vendorId = $('#vendor_id').val();
        var orderview = [];
        $.map($('#sortable').find('li'), function (el) {
            var d = {};
            d['id'] = el.id;
            d['orderview'] = $(el).index();
            orderview.push(d);
            // console.log(el.id);
            // return el.id + ' = ' + $(el).index();
        })

        var data = {status: 'menulistorderviewupdate', vendorId: vendorId, orderview: orderview};

        $.ajax({
            method: "POST",
            url: base_url + "Frontend/Helper",
            data: data,
            async: false,
            success: function (res) {
                var json = JSON.parse(res);
                if (json.status == true) {
                    window.location = '';
                } else {

                }
            },
            error: function (xhr) {
                alert("Error occured.please try again");
            }
        });
    }
    fn.vendordiscountTypechnage = function () {
        var discount_type = $('#discount_type').val();
        if (discount_type == '1') {
            $('#distypemsg').html('Enter amount');

        } else {
            $('#distypemsg').html('Discount in %');

        }
    }
    fn.uploadExcelMenu = function () {
        var formData = new FormData();
        formData.append('status', 'uploadExcelMenu');


        var fileInput = document.querySelector('#UploadExcel');
        formData.append('UploadExcel', fileInput.files[0]);


        if (fileInput.files[0] == undefined) {
            $('#UploadExcel').focus();
            return false;
        }
        $.confirm({
            title: 'Are you sure want to submit ?',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        var json = jQuery.parseJSON(ajaxpost(formData, 'vendor/helper'));
                        alertSimple(json.message);
                        if (json.status == true) {

                            window.location = json.RedUrl;
                        }
                    }
                },
                close: {
                    text: 'Cancel',
                    btnClass: 'btn-red',
                }
            }
        });

    }
    fn.autochatNotifaction = function () {
        var data = {status: 'autochatNotifactionvendor',tyri:1};
        $.ajax({
            method: "POST",
            url: base_url + "Frontend/Helper",
            data: data,
            async: false,
            success: function (res) {
                var json = JSON.parse(res);
                if (json.status == true) {
                    console.log(json.notifications);
                    VENDRCOMN.orderbrowsernotification(json.notifications);
                } else {

                }
            },
            error: function (xhr) {
                alert("Error occured.please try again");
            }
        });
    }
    fn.orderbrowsernotification = function (notifications) {
        for (var i = 0; i < notifications.length; i++) {
            var d = notifications[i];

            VENDRCOMN.notifyMe('New order -  ' + d.orderno, base_url + 'vendor/OrderDetails/' + btoa(d.id));

        }

    }
    fn.notifyMe = function (message, Url) {
        if (!("Notification" in window)) {
            return false;
        }
        else if (Notification.permission === "granted") {
            sendNotifactin(message, Url);
        }
        else if (Notification.permission !== "denied") {
            Notification.requestPermission().then(function (permission) {
                if (permission === "granted") {
                    sendNotifactin(message, Url);
                }
            });
        }
        function sendNotifactin(message, Url) {
            var notification = new Notification(message);
            notification.onclick = function (event) {
                event.preventDefault(); // prevent the browser from focusing the Notification's tab
                window.open(Url, '_blank');
            };

        }
    }
    return fn;
})();